function twoDigit(n) {
	return (n < 10 ? '0' : '') + n;
};

function formatDate(date) {
	var datePart = date.match(/\d+/g),
	year = datePart[0].substring(2);
	month = datePart[1];
	day = datePart[2];
	return day+'-'+month+'-'+year;
}

function formatTimestamps() {		
	var today = new Date();
	var currentDate = today.getFullYear() + "-" + twoDigit(today.getMonth() + 1) + "-" + twoDigit(today.getDate());
	var timestamps = document.getElementsByClassName("timestamp");
	for (i = 0; i < timestamps.length; i++) {
		ts = timestamps[i];
		value = ts.innerHTML;					
		dateValue = value.substring(0,value.indexOf('T'));
		timeValue = value.substring(value.indexOf('T') + 1);
		if (dateValue === currentDate) {						
			ts.innerHTML = timeValue;
		} else {
			ts.innerHTML = formatDate(dateValue) + " " + timeValue;
		};
	};
};

function formatNumericValues() {
	var numericElements = document.getElementsByClassName("numeric");
	for (i = 0; i < numericElements.length; i++) {
		var ne = numericElements[i];
		ne.textContent = ne.textContent.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
	};
}


function loadXMLDoc(filename) {
	if (window.ActiveXObject) {
		 xhttp = new ActiveXObject("Msxml2.XMLHTTP");
	} else {
		 xhttp = new XMLHttpRequest();
	}
	xhttp.open("GET", filename, false);
	xhttp.send("");
	return xhttp.responseXML;
}

function reloadContent() {		
	var xml = loadXMLDoc("H2XState.xml");		
	if (document.implementation && document.implementation.createDocument) {
		xsltProcessor = new XSLTProcessor();		
		try {
			container = document.getElementById('container')
			xsltProcessor.importStylesheet(xsl);
			resultDocument = xsltProcessor.transformToFragment(xml, document);			
			container.innerHTML = "";
			container.appendChild(resultDocument);
			formatTimestamps();
			formatNumericValues();
		}		
		catch(err) {
			console.error(err);
		} 
	}						
}

function checkLastUpdate() {	
	var value = lastUpdate();
	var currentTime = new Date();
	var up = true;
	var body = document.getElementsByTagName('body')[0];
	if (lastUpdateValue !== value) {		
		lastUpdateValue = value;
		lastUpdateTime = new Date();
	} else if (!lastUpdateTime || (currentTime-lastUpdateTime) >= 5000) {
		up = false;
	}	
	if (up) {
		body.setAttribute('class','up');
	} else {
		body.setAttribute('class','down');
	}
}

function lastUpdate() {
	return document.getElementById('last-update').textContent;
}

const xsl = loadXMLDoc("H2XStateMonitorStyleSheet.xsl");
reloadContent();
var lastUpdateValue = lastUpdate();
var lastUpdateTime;
checkLastUpdate();

setInterval(function(){
	reloadContent();
	checkLastUpdate();
}, 1000);