<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
	<xsl:output method="html" version="1.0" encoding="utf-8" indent="yes"/>
		
	<xsl:template match="H2XState">
		<div id="top">
			<div id="general">
				<xsl:apply-templates select="General"/>
			</div>
			<h1>H<sub>2</sub>X – HLA 2 XML Convertor</h1>
			<table>
				<td>
					<xsl:apply-templates select="RTIState"/>
				</td>
				<td>
					<xsl:apply-templates select="OtherSideState"/>
				</td>
				<td>
					<xsl:apply-templates select="Errors"/>
				</td>
				<td>
					<xsl:apply-templates select="Warnings"/>
				</td>
			</table>
		</div>
		<div id="bottom">
			<xsl:apply-templates select="Throughput"/>
			<br/>
			<xsl:apply-templates select="Objects"/>
			<br/>
			<xsl:apply-templates select="Interactions"/>
		</div>
	</xsl:template>
	
	
	<!-- Top section -->
	
	<xsl:template match="General">
		Version: <xsl:apply-templates select="@Version"/>&#160;
		Generated: <xsl:apply-templates select="@Generated"/>&#160;
		Up since: <xsl:apply-templates select="@UpSince"/>&#160;
		Last update: <span id="last-update"><xsl:apply-templates select="@LastUpdate"/></span>&#160;
		Status:
		<span id="up-status">
			<xsl:call-template name="Status-Indicator">
				<xsl:with-param name="Status" select="'Up'"/>
			</xsl:call-template>
		</span>
		<span id="down-status">
			<xsl:call-template name="Status-Indicator">
				<xsl:with-param name="Status" select="'Down'"/>
			</xsl:call-template>
		</span>
    </xsl:template>
		
	<xsl:template match="RTIState">
		<h3>RTI State</h3>
		<xsl:call-template name="State"/>
    </xsl:template>
	
	<xsl:template match="OtherSideState">
		<h3>Other Side State</h3>
        <xsl:call-template name="State"/>
    </xsl:template>
	
	<xsl:template name="State">
		<xsl:apply-templates select="@Status"/>
		<xsl:apply-templates select="@Heartbeat"/>
		&#160;&#160;&#160;
		Since: <xsl:apply-templates select="@Since"/>
		<br/>		
		Mode: <xsl:apply-templates select="@Mode"/>		
		<br/>
		<xsl:if test="name()='OtherSideState' and @Mode!='Playback'">
			Receive Connection: <xsl:apply-templates select="@ReceiveStatus"/>		
			<br/>
			Send Connection: <xsl:apply-templates select="@SendStatus"/>		
			<br/>
		</xsl:if>
		Description: <xsl:apply-templates select="@Description"/>		
	</xsl:template>
	
	<xsl:template match="Errors|Warnings">
		<h3><xsl:value-of select="name()"/></h3>
		Count: <xsl:apply-templates select="@Count"/><br/>		
		<xsl:if test="@Count &gt; 0">
			<b><u>Last <xsl:value-of select="substring(name(), 1, string-length(name()) - 1)"/></u></b>
			<br/>
			Time: <xsl:apply-templates select="Last/@Time"/>
			<br/>
			Module: <xsl:apply-templates select="Last/@Module"/>
			<br/>
			Type: <xsl:apply-templates select="Last/@Type"/>
			<br/>
			Description: <xsl:apply-templates select="Last/@Description"/>
		</xsl:if>
    </xsl:template>
	
	<xsl:template match="@Status|@Heartbeat">
		<xsl:value-of select="name()"/>: 
		<xsl:choose>
			<xsl:when test="../@Mode='Playback'">
				<span class="attribute-value">
					<xsl:value-of select="."/>
				</span>
			</xsl:when>
			<xsl:otherwise>
				<xsl:call-template name="Status-Indicator">
					<xsl:with-param name="Status" select="."/>
				</xsl:call-template>
			</xsl:otherwise>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template match="@ReceiveStatus|@SendStatus">
		<xsl:call-template name="Status-Indicator">
			<xsl:with-param name="Status" select="."/>
		</xsl:call-template>
	</xsl:template>
	
	<xsl:template name="Status-Indicator">
		<xsl:param name="Status"/>
		<span class="attribute-value">
			<xsl:value-of select="$Status"/>
		</span>
		<xsl:choose>
			<xsl:when test="$Status='Up' or $Status='On'">
				<img src="img/ball_green.png"/>
			</xsl:when>
			<xsl:when test="$Status='Down' or $Status='Off'">
				<img src="img/ball_red.png"/>
			</xsl:when>
		</xsl:choose>
	</xsl:template>
	
	<xsl:template match="@Description">		
		<div class="description">
			<br/>
			<span class="attribute-value">
				<xsl:value-of select="."/>
			</span>
		</div>
	</xsl:template>
	
	<xsl:template match="@*">
		<span class="attribute-value">
			<xsl:value-of select="."/>
		</span>
	</xsl:template>
	
	<!-- Bottom section -->	
	
	<xsl:template match="Objects|Interactions|Throughput">
		<xsl:variable name="colspan">
			<xsl:if test="name(.)='Objects'">6</xsl:if>
			<xsl:if test="name(.)='Interactions'">3</xsl:if>
			<xsl:if test="name(.)='Throughput'">2</xsl:if>
		</xsl:variable>
		<h3><xsl:value-of select="name(.)"/></h3>
		<table>
			<thead>
				<tr class="column-header-row">
					<th></th>
					<th colspan="{$colspan}">Received</th>
					<th colspan="{$colspan}">Sent</th>
				</tr>
				<tr>					
					<th>
						<xsl:choose>
						<xsl:when test="name(.)!='Throughput'">
							<xsl:value-of select="substring(name(.), 1, string-length(name(.)) - 1)"/>
						</xsl:when>
						<xsl:otherwise>
							<xsl:attribute name="class">clear-header</xsl:attribute>
						</xsl:otherwise>
						</xsl:choose>
					</th>
					<xsl:call-template name="Table-Headers"/>
					<xsl:call-template name="Table-Headers"/>
				</tr>
			</thead>
			<tbody>
				<xsl:choose>
					<xsl:when test="name(.)='Throughput'">
						<th class="row-header">Messages/Sec.</th>
						<xsl:apply-templates select="Received"/>
						<xsl:apply-templates select="Sent"/>
					</xsl:when>
					<xsl:otherwise>
						<xsl:apply-templates/>
					</xsl:otherwise>
				</xsl:choose>
			</tbody>
		</table>
	</xsl:template>
	
	<xsl:template name="Table-Headers">
		<xsl:if test="name(.)='Objects'">
			<th class="left-cell">Unique Ids</th>
			<th>Unique Since</th>
			<th>Updates</th>
			<th>Deletes</th>
			<th>First</th>
			<th>Last</th>
		</xsl:if>
		<xsl:if test="name(.)='Interactions'">
			<th class="left-cell">Count</th>
			<th>First</th>
			<th>Last</th>
		</xsl:if>
		<xsl:if test="name(.)='Throughput'">
			<th class="left-cell">Current</th>
			<th>Max</th>
		</xsl:if>
	</xsl:template>
	
	<xsl:template match="Object|Interaction">
		<tr>
			<th class="row-header">
				<xsl:value-of select="@Name"/>
			</th>
			<xsl:apply-templates select="Received"/>
			<xsl:apply-templates select="Sent"/>
		</tr>
	</xsl:template>
	
	<xsl:template match="Object//Received|Object//Sent">
		<td class="left-cell"><xsl:apply-templates select="@UniqueIds"/></td>
		<td><xsl:apply-templates select="@UniqueSince"/></td>
		<td><xsl:apply-templates select="@Updates"/></td>
		<td><xsl:apply-templates select="@Deletes"/></td>
		<td><xsl:apply-templates select="@First"/></td>
		<td><xsl:apply-templates select="@Last"/></td>
	</xsl:template>
	
	<xsl:template match="Interaction//Received|Interaction//Sent">
		<td class="left-cell"><xsl:apply-templates select="@Count"/></td>
		<td><xsl:apply-templates select="@First"/></td>
		<td><xsl:apply-templates select="@Last"/></td>
	</xsl:template>
	
	<xsl:template match="Throughput//Received|Throughput//Sent">
		<td class="left-cell"><xsl:apply-templates select="@Current"/></td>
		<td><xsl:apply-templates select="@Max"/></td>
	</xsl:template>
	
	<!-- Attribute formatting -->
	
	<xsl:template match="@Generated|@LastUpdate|@UpSince|@Since|@Time|@UniqueSince|@First|@Last">
		<span class="timestamp attribute-value">
			<xsl:value-of select="."/>
		</span>
	</xsl:template>
	
	<xsl:template match="@Count|@UniqueIds|@Updates|@Deletes|@Current|@Max">
		<span class="attribute-value numeric">
			<xsl:value-of select="."/>
		</span>
	</xsl:template>
	
</xsl:stylesheet>