/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAAMPHIBIOUSVEHICLEMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLAAMPHIBIOUSVEHICLEMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaAmphibiousVehicle.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaAmphibiousVehicle instances.
    */
    class HlaAmphibiousVehicleManagerListener {

    public:

        LIBAPI virtual ~HlaAmphibiousVehicleManagerListener() {}

        /**
        * This method is called when a new HlaAmphibiousVehicle instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param amphibiousVehicle the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaAmphibiousVehicleDiscovered(HlaAmphibiousVehiclePtr amphibiousVehicle, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaAmphibiousVehicle instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param amphibiousVehicle the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaAmphibiousVehicleInitialized(HlaAmphibiousVehiclePtr amphibiousVehicle, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaAmphibiousVehicleManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param amphibiousVehicle the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaAmphibiousVehicleInInterest(HlaAmphibiousVehiclePtr amphibiousVehicle, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaAmphibiousVehicleManagerListener instance goes out of interest.
        *
        * @param amphibiousVehicle the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaAmphibiousVehicleOutOfInterest(HlaAmphibiousVehiclePtr amphibiousVehicle, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaAmphibiousVehicle instance is deleted.
        *
        * @param amphibiousVehicle the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaAmphibiousVehicleDeleted(HlaAmphibiousVehiclePtr amphibiousVehicle, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaAmphibiousVehicleManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaAmphibiousVehicleManagerListener::Adapter : public HlaAmphibiousVehicleManagerListener {

    public:
        LIBAPI virtual void hlaAmphibiousVehicleDiscovered(HlaAmphibiousVehiclePtr amphibiousVehicle, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaAmphibiousVehicleInitialized(HlaAmphibiousVehiclePtr amphibiousVehicle, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaAmphibiousVehicleInInterest(HlaAmphibiousVehiclePtr amphibiousVehicle, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaAmphibiousVehicleOutOfInterest(HlaAmphibiousVehiclePtr amphibiousVehicle, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaAmphibiousVehicleDeleted(HlaAmphibiousVehiclePtr amphibiousVehicle, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
