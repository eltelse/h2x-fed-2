/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAJAMMERBEAMMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLAJAMMERBEAMMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaJammerBeam.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaJammerBeam instances.
    */
    class HlaJammerBeamManagerListener {

    public:

        LIBAPI virtual ~HlaJammerBeamManagerListener() {}

        /**
        * This method is called when a new HlaJammerBeam instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param jammerBeam the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaJammerBeamDiscovered(HlaJammerBeamPtr jammerBeam, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaJammerBeam instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param jammerBeam the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaJammerBeamInitialized(HlaJammerBeamPtr jammerBeam, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaJammerBeamManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param jammerBeam the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaJammerBeamInInterest(HlaJammerBeamPtr jammerBeam, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaJammerBeamManagerListener instance goes out of interest.
        *
        * @param jammerBeam the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaJammerBeamOutOfInterest(HlaJammerBeamPtr jammerBeam, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaJammerBeam instance is deleted.
        *
        * @param jammerBeam the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaJammerBeamDeleted(HlaJammerBeamPtr jammerBeam, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaJammerBeamManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaJammerBeamManagerListener::Adapter : public HlaJammerBeamManagerListener {

    public:
        LIBAPI virtual void hlaJammerBeamDiscovered(HlaJammerBeamPtr jammerBeam, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaJammerBeamInitialized(HlaJammerBeamPtr jammerBeam, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaJammerBeamInInterest(HlaJammerBeamPtr jammerBeam, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaJammerBeamOutOfInterest(HlaJammerBeamPtr jammerBeam, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaJammerBeamDeleted(HlaJammerBeamPtr jammerBeam, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
