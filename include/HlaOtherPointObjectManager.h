/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAOTHERPOINTOBJECTMANAGER_H
#define DEVELOPER_STUDIO_HLAOTHERPOINTOBJECTMANAGER_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <DevStudio/datatypes/DamageStatusEnum.h>
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EnvironmentObjectTypeStruct.h>
#include <DevStudio/datatypes/ForceIdentifierEnum.h>
#include <DevStudio/datatypes/OrientationStruct.h>
#include <DevStudio/datatypes/WorldLocationStruct.h>
#include <string>

#include <list>
#include <vector>

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaOtherPointObjectManagerListener.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaException.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

   /**
   * Manager used to manage HlaOtherPointObjects.
   */
   class HlaOtherPointObjectManager : private boost::noncopyable {

   public:

      LIBAPI virtual ~HlaOtherPointObjectManager() {}

      /**
      * Gets a list of all HlaOtherPointObjects within interest, both local and remote.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of HlaOtherPointObjects
      */
      LIBAPI virtual std::list<HlaOtherPointObjectPtr> getHlaOtherPointObjects() = 0;

      /**
      * Gets a list of all HlaOtherPointObjects, both local and remote.
      * HlaOtherPointObjects not within interest are also included.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of all HlaOtherPointObjects
      */
      LIBAPI virtual std::list<HlaOtherPointObjectPtr> getAllHlaOtherPointObjects() = 0;

      /**
      * Gets a list of local HlaOtherPointObjects within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of local HlaOtherPointObjects
      */
      LIBAPI virtual std::list<HlaOtherPointObjectPtr> getLocalHlaOtherPointObjects() = 0;

      /**
      * Gets a list of remote HlaOtherPointObjects within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of remote HlaOtherPointObjects
      */
      LIBAPI virtual std::list<HlaOtherPointObjectPtr> getRemoteHlaOtherPointObjects() = 0;

      /**
      * Find a HlaOtherPointObject with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> of the HlaOtherPointObject to find
      *
      * @return the specified HlaOtherPointObject, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaOtherPointObjectPtr getOtherPointObjectByHlaInstanceName(const std::wstring& hlaInstanceName) = 0;

      /**
      * Find a HlaOtherPointObject with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param encodedHlaInstanceHandle the encoded <code>HLA instance handle</code> of the HlaOtherPointObject to find
      *
      * @return the specified HlaOtherPointObject, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaOtherPointObjectPtr getOtherPointObjectByHlaInstanceHandle(const std::vector<char>& encodedHlaInstanceHandle) = 0;

      /**
      * Creates a new HlaOtherPointObject, and sets all <i>Create</i> attributes.
      *
      * @return Returns a new local HlaOtherPointObject.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaOtherPointObjectPtr createLocalHlaOtherPointObject(
      ) THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Creates a new HlaOtherPointObject with a specified <code>HLA instance name</code>, and sets all
      * <i>Create</i> attributes.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> to register for this instance
      *
      * @return Returns a new local HlaOtherPointObject.
      *
      * @throws HlaIllegalInstanceNameException if the name is illegal
      * @throws HlaInstanceNameInUseException if the name is already in use
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaOtherPointObjectPtr createLocalHlaOtherPointObject(
         const std::wstring& hlaInstanceName
      ) THROW_SPEC (HlaIllegalInstanceNameException, HlaInstanceNameInUseException,
                    HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaOtherPointObject and removes it from the federation.
      *
      * @param otherPointObject The HlaOtherPointObject to delete
      *
      * @return the HlaOtherPointObject deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaOtherPointObjectPtr deleteLocalHlaOtherPointObject(HlaOtherPointObjectPtr otherPointObject)
          THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                      HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaOtherPointObject and removes it from the federation.
      *
      * @param otherPointObject The HlaOtherPointObject to delete
      * @param timestamp timestamp when the instance was removed.
      *
      * @return the HlaOtherPointObject deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaOtherPointObjectPtr deleteLocalHlaOtherPointObject(HlaOtherPointObjectPtr otherPointObject, HlaTimeStampPtr timestamp)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaOtherPointObject and removes it from the federation.
      *
      * @param otherPointObject The HlaOtherPointObject to delete
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaOtherPointObject deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaOtherPointObjectPtr deleteLocalHlaOtherPointObject(HlaOtherPointObjectPtr otherPointObject, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaOtherPointObject and removes it from the federation.
      *
      * @param otherPointObject The HlaOtherPointObject to delete
      * @param timestamp timestamp when the instance was removed.
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaOtherPointObject deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaOtherPointObjectPtr deleteLocalHlaOtherPointObject(HlaOtherPointObjectPtr otherPointObject, HlaTimeStampPtr timestamp, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Add a HlaOtherPointObject manager listener.
      *
      * @param managerListener listener to add
      */
      LIBAPI virtual void addHlaOtherPointObjectManagerListener(HlaOtherPointObjectManagerListenerPtr managerListener) = 0;

      /**
      * Removed a HlaOtherPointObject manager listener.
      *
      * @param managerListener listener to remove
      */
      LIBAPI virtual void removeHlaOtherPointObjectManagerListener(HlaOtherPointObjectManagerListenerPtr managerListener) = 0;

     /**
      * Add a listener that will automatically be added to all instances of HlaOtherPointObject (Both local and remote).
      * The listener is notified when any attribute of an instance of HlaOtherPointObject is updated.
      * The listener is also called when an interaction is sent to an instance of HlaOtherPointObject.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param listener The listener to add.
      */
      LIBAPI virtual void addHlaOtherPointObjectDefaultInstanceListener(HlaOtherPointObjectListenerPtr listener) = 0;

     /**
      * Remove a listener from the set of listeners that are automatically added to new instances of HlaOtherPointObject.
      * Note: The listener will not be removed from already existing instances of HlaOtherPointObject.
      * This method is idempotent.
      *
      * @param listener The listener to remove.
      */
      LIBAPI virtual void removeHlaOtherPointObjectDefaultInstanceListener(HlaOtherPointObjectListenerPtr listener) = 0;

     /**
      * Add a valueListener that will automatically be added to all instances of HlaOtherPointObject (Both local and remote).
      * The  Valuelistener is notified when any attribute of an instance of HlaOtherPointObject is updated.
      * Note that the attribute values that are notified are not guaranteed to
      * be the latest available values for that attribute.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to add.
      */
      LIBAPI virtual void addHlaOtherPointObjectDefaultInstanceValueListener(HlaOtherPointObjectValueListenerPtr valueListener) = 0;

     /**
      * Remove a valueListener from the set of listeners that are automatically added to new instances of HlaOtherPointObject.
      * Note: The valueListener will not be removed from already existing instances of HlaOtherPointObject.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to remove.
      */
      LIBAPI virtual void removeHlaOtherPointObjectDefaultInstanceValueListener(HlaOtherPointObjectValueListenerPtr valueListener) = 0;

     /**
      * Enables or disables the HlaOtherPointObject manager.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaOtherPointObject manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      *
      * @param enabled <code>true</code> to enable the HlaOtherPointObject manager, otherwise <code>false</code>
      */
      LIBAPI virtual void setEnabled(bool enabled) = 0;

     /**
      * Enables the HlaOtherPointObject manager if it is avalable in the FOM at runtime.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaOtherPointObject manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      */
      LIBAPI virtual void setEnabledIfAvailableInFom() = 0;

     /**
      * Check if the HlaOtherPointObject manager is actually enabled when connected.
      * An HlaOtherPointObject manager may be disabled with the <code>setEnabled(false)</code> function, or it may be configured
      * as <i>if available in the FOM</i> by the <code>setEnabledIfAvailableInFom()</code> function and
      * then not available in the FOM at runtime.
      * <p>
      * This method should only be used when connected, it will always return <code>false</code>
      * when not connected.
      *
      * @return <code>true</code> if connected and the HlaOtherPointObject manager is enabled.
      */
      LIBAPI virtual bool isEnabled() = 0;
   };
}
#endif
