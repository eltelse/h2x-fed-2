/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAENVIRONMENTPROCESSVALUELISTENER_H
#define DEVELOPER_STUDIO_HLAENVIRONMENTPROCESSVALUELISTENER_H

#include <memory>

#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EnvironmentModelTypeEnum.h>
#include <DevStudio/datatypes/EnvironmentRecStruct.h>
#include <DevStudio/datatypes/EnvironmentRecStructArray.h>
#include <DevStudio/datatypes/EnvironmentTypeStruct.h>
#include <vector>

#include <DevStudio/HlaLogicalTime.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaEnvironmentProcessAttributes.h>    

namespace DevStudio {

   /**
   * Listener for updates of attributes, with the new updated values.  
   */
   class HlaEnvironmentProcessValueListener {

   public:

      LIBAPI virtual ~HlaEnvironmentProcessValueListener() {}
    
      /**
      * This method is called when the attribute <code>processIdentifier</code> is updated.
      * <br>Description from the FOM: <i>Identifies which process issued the environmental process update</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @param environmentProcess The object which is updated.
      * @param processIdentifier The new value of the attribute in this update
      * @param validOldProcessIdentifier True if oldProcessIdentifier contains a valid value
      * @param oldProcessIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void processIdentifierUpdated(HlaEnvironmentProcessPtr environmentProcess, EntityIdentifierStruct processIdentifier, bool validOldProcessIdentifier, EntityIdentifierStruct oldProcessIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>type</code> is updated.
      * <br>Description from the FOM: <i>Identifies the environmental process type</i>
      * <br>Description of the data type from the FOM: <i>Record specifying the kind of environment, the domain and any extra information necessary for describing the environmental entity</i>
      *
      * @param environmentProcess The object which is updated.
      * @param type The new value of the attribute in this update
      * @param validOldType True if oldType contains a valid value
      * @param oldType The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void typeUpdated(HlaEnvironmentProcessPtr environmentProcess, EnvironmentTypeStruct type, bool validOldType, EnvironmentTypeStruct oldType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>modelType</code> is updated.
      * <br>Description from the FOM: <i>Identifies the model used for generating this environmental process update ; defined in 1278.1a as being exercise specific</i>
      * <br>Description of the data type from the FOM: <i>Environment process model type</i>
      *
      * @param environmentProcess The object which is updated.
      * @param modelType The new value of the attribute in this update
      * @param validOldModelType True if oldModelType contains a valid value
      * @param oldModelType The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void modelTypeUpdated(HlaEnvironmentProcessPtr environmentProcess, EnvironmentModelTypeEnum::EnvironmentModelTypeEnum modelType, bool validOldModelType, EnvironmentModelTypeEnum::EnvironmentModelTypeEnum oldModelType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>environmentProcessActive</code> is updated.
      * <br>Description from the FOM: <i>Specifies the status of the environmental process (active or inactive)</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param environmentProcess The object which is updated.
      * @param environmentProcessActive The new value of the attribute in this update
      * @param validOldEnvironmentProcessActive True if oldEnvironmentProcessActive contains a valid value
      * @param oldEnvironmentProcessActive The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void environmentProcessActiveUpdated(HlaEnvironmentProcessPtr environmentProcess, bool environmentProcessActive, bool validOldEnvironmentProcessActive, bool oldEnvironmentProcessActive, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>sequenceNumber</code> is updated.
      * <br>Description from the FOM: <i>Optional, used to support update sequencing ; if not used, set to EP_NO_SEQUENCE ; when used, set to zero for each exercise and incremented by one for each update sent</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param environmentProcess The object which is updated.
      * @param sequenceNumber The new value of the attribute in this update
      * @param validOldSequenceNumber True if oldSequenceNumber contains a valid value
      * @param oldSequenceNumber The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void sequenceNumberUpdated(HlaEnvironmentProcessPtr environmentProcess, unsigned short sequenceNumber, bool validOldSequenceNumber, unsigned short oldSequenceNumber, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>environmentRecData</code> is updated.
      * <br>Description from the FOM: <i>Specifies environment records</i>
      * <br>Description of the data type from the FOM: <i>Specifies environment records as a collection of geometry and state records</i>
      *
      * @param environmentProcess The object which is updated.
      * @param environmentRecData The new value of the attribute in this update
      * @param validOldEnvironmentRecData True if oldEnvironmentRecData contains a valid value
      * @param oldEnvironmentRecData The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void environmentRecDataUpdated(HlaEnvironmentProcessPtr environmentProcess, std::vector<DevStudio::EnvironmentRecStruct > environmentRecData, bool validOldEnvironmentRecData, std::vector<DevStudio::EnvironmentRecStruct > oldEnvironmentRecData, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
      /**
      * This method is called when the producing federate of an attribute is changed.
      * Only available when using HLA Evolved.
      *
      * @param environmentProcess The object which is updated.
      * @param attribute The attribute that now has a new producing federate
      * @param oldProducingFederate The federate handle of the old producing federate
      * @param newProducingFederate The federate handle of the new producing federate
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void producingFederateUpdated(HlaEnvironmentProcessPtr environmentProcess, HlaEnvironmentProcessAttributes::Attribute attribute, HlaFederateIdPtr oldProducingFederate, HlaFederateIdPtr newProducingFederate, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;

      class Adapter;
   };

   /**
   * An adapter class that implements the HlaEnvironmentProcessValueListener interface with empty methods.
   * It might be used as a base class for a listener.
   */
   class HlaEnvironmentProcessValueListener::Adapter : public HlaEnvironmentProcessValueListener {

   public:

      LIBAPI virtual void processIdentifierUpdated(HlaEnvironmentProcessPtr environmentProcess, EntityIdentifierStruct processIdentifier, bool validOldProcessIdentifier, EntityIdentifierStruct oldProcessIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void typeUpdated(HlaEnvironmentProcessPtr environmentProcess, EnvironmentTypeStruct type, bool validOldType, EnvironmentTypeStruct oldType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void modelTypeUpdated(HlaEnvironmentProcessPtr environmentProcess, EnvironmentModelTypeEnum::EnvironmentModelTypeEnum modelType, bool validOldModelType, EnvironmentModelTypeEnum::EnvironmentModelTypeEnum oldModelType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void environmentProcessActiveUpdated(HlaEnvironmentProcessPtr environmentProcess, bool environmentProcessActive, bool validOldEnvironmentProcessActive, bool oldEnvironmentProcessActive, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void sequenceNumberUpdated(HlaEnvironmentProcessPtr environmentProcess, unsigned short sequenceNumber, bool validOldSequenceNumber, unsigned short oldSequenceNumber, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void environmentRecDataUpdated(HlaEnvironmentProcessPtr environmentProcess, std::vector<DevStudio::EnvironmentRecStruct > environmentRecData, bool validOldEnvironmentRecData, std::vector<DevStudio::EnvironmentRecStruct > oldEnvironmentRecData, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
      LIBAPI virtual void producingFederateUpdated(HlaEnvironmentProcessPtr environmentProcess, HlaEnvironmentProcessAttributes::Attribute attribute, HlaFederateIdPtr oldProducingFederate, HlaFederateIdPtr newProducingFederate, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
   };

}
#endif
