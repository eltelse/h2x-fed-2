/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAMINEFIELDMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLAMINEFIELDMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaMinefield.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaMinefield instances.
    */
    class HlaMinefieldManagerListener {

    public:

        LIBAPI virtual ~HlaMinefieldManagerListener() {}

        /**
        * This method is called when a new HlaMinefield instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param minefield the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaMinefieldDiscovered(HlaMinefieldPtr minefield, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaMinefield instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param minefield the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaMinefieldInitialized(HlaMinefieldPtr minefield, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaMinefieldManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param minefield the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaMinefieldInInterest(HlaMinefieldPtr minefield, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaMinefieldManagerListener instance goes out of interest.
        *
        * @param minefield the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaMinefieldOutOfInterest(HlaMinefieldPtr minefield, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaMinefield instance is deleted.
        *
        * @param minefield the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaMinefieldDeleted(HlaMinefieldPtr minefield, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaMinefieldManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaMinefieldManagerListener::Adapter : public HlaMinefieldManagerListener {

    public:
        LIBAPI virtual void hlaMinefieldDiscovered(HlaMinefieldPtr minefield, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaMinefieldInitialized(HlaMinefieldPtr minefield, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaMinefieldInInterest(HlaMinefieldPtr minefield, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaMinefieldOutOfInterest(HlaMinefieldPtr minefield, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaMinefieldDeleted(HlaMinefieldPtr minefield, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
