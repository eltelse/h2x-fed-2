/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLASOVIETIFFMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLASOVIETIFFMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaSovietIFF.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaSovietIFF instances.
    */
    class HlaSovietIFFManagerListener {

    public:

        LIBAPI virtual ~HlaSovietIFFManagerListener() {}

        /**
        * This method is called when a new HlaSovietIFF instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param sovietIFF the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaSovietIFFDiscovered(HlaSovietIFFPtr sovietIFF, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaSovietIFF instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param sovietIFF the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaSovietIFFInitialized(HlaSovietIFFPtr sovietIFF, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaSovietIFFManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param sovietIFF the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaSovietIFFInInterest(HlaSovietIFFPtr sovietIFF, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaSovietIFFManagerListener instance goes out of interest.
        *
        * @param sovietIFF the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaSovietIFFOutOfInterest(HlaSovietIFFPtr sovietIFF, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaSovietIFF instance is deleted.
        *
        * @param sovietIFF the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaSovietIFFDeleted(HlaSovietIFFPtr sovietIFF, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaSovietIFFManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaSovietIFFManagerListener::Adapter : public HlaSovietIFFManagerListener {

    public:
        LIBAPI virtual void hlaSovietIFFDiscovered(HlaSovietIFFPtr sovietIFF, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaSovietIFFInitialized(HlaSovietIFFPtr sovietIFF, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaSovietIFFInInterest(HlaSovietIFFPtr sovietIFF, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaSovietIFFOutOfInterest(HlaSovietIFFPtr sovietIFF, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaSovietIFFDeleted(HlaSovietIFFPtr sovietIFF, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
