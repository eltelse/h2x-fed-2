/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLASOVIETIFFTRANSPONDERMANAGER_H
#define DEVELOPER_STUDIO_HLASOVIETIFFTRANSPONDERMANAGER_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EventIdentifierStruct.h>
#include <DevStudio/datatypes/FundamentalParameterDataStruct.h>
#include <DevStudio/datatypes/FundamentalParameterDataStructLengthlessArray.h>
#include <DevStudio/datatypes/IffOperationalParameter1Enum.h>
#include <DevStudio/datatypes/IffOperationalParameter2Enum.h>
#include <DevStudio/datatypes/IffSystemModeEnum.h>
#include <DevStudio/datatypes/IffSystemNameEnum.h>
#include <DevStudio/datatypes/IffSystemTypeEnum.h>
#include <DevStudio/datatypes/RelativePositionStruct.h>
#include <string>
#include <vector>

#include <list>
#include <vector>

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaSovietIFFTransponderManagerListener.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaException.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

   /**
   * Manager used to manage HlaSovietIFFTransponders.
   */
   class HlaSovietIFFTransponderManager : private boost::noncopyable {

   public:

      LIBAPI virtual ~HlaSovietIFFTransponderManager() {}

      /**
      * Gets a list of all HlaSovietIFFTransponders within interest, both local and remote.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of HlaSovietIFFTransponders
      */
      LIBAPI virtual std::list<HlaSovietIFFTransponderPtr> getHlaSovietIFFTransponders() = 0;

      /**
      * Gets a list of all HlaSovietIFFTransponders, both local and remote.
      * HlaSovietIFFTransponders not within interest are also included.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of all HlaSovietIFFTransponders
      */
      LIBAPI virtual std::list<HlaSovietIFFTransponderPtr> getAllHlaSovietIFFTransponders() = 0;

      /**
      * Gets a list of local HlaSovietIFFTransponders within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of local HlaSovietIFFTransponders
      */
      LIBAPI virtual std::list<HlaSovietIFFTransponderPtr> getLocalHlaSovietIFFTransponders() = 0;

      /**
      * Gets a list of remote HlaSovietIFFTransponders within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of remote HlaSovietIFFTransponders
      */
      LIBAPI virtual std::list<HlaSovietIFFTransponderPtr> getRemoteHlaSovietIFFTransponders() = 0;

      /**
      * Find a HlaSovietIFFTransponder with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> of the HlaSovietIFFTransponder to find
      *
      * @return the specified HlaSovietIFFTransponder, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaSovietIFFTransponderPtr getSovietIFFTransponderByHlaInstanceName(const std::wstring& hlaInstanceName) = 0;

      /**
      * Find a HlaSovietIFFTransponder with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param encodedHlaInstanceHandle the encoded <code>HLA instance handle</code> of the HlaSovietIFFTransponder to find
      *
      * @return the specified HlaSovietIFFTransponder, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaSovietIFFTransponderPtr getSovietIFFTransponderByHlaInstanceHandle(const std::vector<char>& encodedHlaInstanceHandle) = 0;

      /**
      * Creates a new HlaSovietIFFTransponder, and sets all <i>Create</i> attributes.
      *
      * @return Returns a new local HlaSovietIFFTransponder.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaSovietIFFTransponderPtr createLocalHlaSovietIFFTransponder(
      ) THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Creates a new HlaSovietIFFTransponder with a specified <code>HLA instance name</code>, and sets all
      * <i>Create</i> attributes.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> to register for this instance
      *
      * @return Returns a new local HlaSovietIFFTransponder.
      *
      * @throws HlaIllegalInstanceNameException if the name is illegal
      * @throws HlaInstanceNameInUseException if the name is already in use
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaSovietIFFTransponderPtr createLocalHlaSovietIFFTransponder(
         const std::wstring& hlaInstanceName
      ) THROW_SPEC (HlaIllegalInstanceNameException, HlaInstanceNameInUseException,
                    HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaSovietIFFTransponder and removes it from the federation.
      *
      * @param sovietIFFTransponder The HlaSovietIFFTransponder to delete
      *
      * @return the HlaSovietIFFTransponder deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaSovietIFFTransponderPtr deleteLocalHlaSovietIFFTransponder(HlaSovietIFFTransponderPtr sovietIFFTransponder)
          THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                      HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaSovietIFFTransponder and removes it from the federation.
      *
      * @param sovietIFFTransponder The HlaSovietIFFTransponder to delete
      * @param timestamp timestamp when the instance was removed.
      *
      * @return the HlaSovietIFFTransponder deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaSovietIFFTransponderPtr deleteLocalHlaSovietIFFTransponder(HlaSovietIFFTransponderPtr sovietIFFTransponder, HlaTimeStampPtr timestamp)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaSovietIFFTransponder and removes it from the federation.
      *
      * @param sovietIFFTransponder The HlaSovietIFFTransponder to delete
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaSovietIFFTransponder deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaSovietIFFTransponderPtr deleteLocalHlaSovietIFFTransponder(HlaSovietIFFTransponderPtr sovietIFFTransponder, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaSovietIFFTransponder and removes it from the federation.
      *
      * @param sovietIFFTransponder The HlaSovietIFFTransponder to delete
      * @param timestamp timestamp when the instance was removed.
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaSovietIFFTransponder deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaSovietIFFTransponderPtr deleteLocalHlaSovietIFFTransponder(HlaSovietIFFTransponderPtr sovietIFFTransponder, HlaTimeStampPtr timestamp, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Add a HlaSovietIFFTransponder manager listener.
      *
      * @param managerListener listener to add
      */
      LIBAPI virtual void addHlaSovietIFFTransponderManagerListener(HlaSovietIFFTransponderManagerListenerPtr managerListener) = 0;

      /**
      * Removed a HlaSovietIFFTransponder manager listener.
      *
      * @param managerListener listener to remove
      */
      LIBAPI virtual void removeHlaSovietIFFTransponderManagerListener(HlaSovietIFFTransponderManagerListenerPtr managerListener) = 0;

     /**
      * Add a listener that will automatically be added to all instances of HlaSovietIFFTransponder (Both local and remote).
      * The listener is notified when any attribute of an instance of HlaSovietIFFTransponder is updated.
      * The listener is also called when an interaction is sent to an instance of HlaSovietIFFTransponder.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param listener The listener to add.
      */
      LIBAPI virtual void addHlaSovietIFFTransponderDefaultInstanceListener(HlaSovietIFFTransponderListenerPtr listener) = 0;

     /**
      * Remove a listener from the set of listeners that are automatically added to new instances of HlaSovietIFFTransponder.
      * Note: The listener will not be removed from already existing instances of HlaSovietIFFTransponder.
      * This method is idempotent.
      *
      * @param listener The listener to remove.
      */
      LIBAPI virtual void removeHlaSovietIFFTransponderDefaultInstanceListener(HlaSovietIFFTransponderListenerPtr listener) = 0;

     /**
      * Add a valueListener that will automatically be added to all instances of HlaSovietIFFTransponder (Both local and remote).
      * The  Valuelistener is notified when any attribute of an instance of HlaSovietIFFTransponder is updated.
      * Note that the attribute values that are notified are not guaranteed to
      * be the latest available values for that attribute.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to add.
      */
      LIBAPI virtual void addHlaSovietIFFTransponderDefaultInstanceValueListener(HlaSovietIFFTransponderValueListenerPtr valueListener) = 0;

     /**
      * Remove a valueListener from the set of listeners that are automatically added to new instances of HlaSovietIFFTransponder.
      * Note: The valueListener will not be removed from already existing instances of HlaSovietIFFTransponder.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to remove.
      */
      LIBAPI virtual void removeHlaSovietIFFTransponderDefaultInstanceValueListener(HlaSovietIFFTransponderValueListenerPtr valueListener) = 0;

     /**
      * Enables or disables the HlaSovietIFFTransponder manager.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaSovietIFFTransponder manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      *
      * @param enabled <code>true</code> to enable the HlaSovietIFFTransponder manager, otherwise <code>false</code>
      */
      LIBAPI virtual void setEnabled(bool enabled) = 0;

     /**
      * Enables the HlaSovietIFFTransponder manager if it is avalable in the FOM at runtime.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaSovietIFFTransponder manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      */
      LIBAPI virtual void setEnabledIfAvailableInFom() = 0;

     /**
      * Check if the HlaSovietIFFTransponder manager is actually enabled when connected.
      * An HlaSovietIFFTransponder manager may be disabled with the <code>setEnabled(false)</code> function, or it may be configured
      * as <i>if available in the FOM</i> by the <code>setEnabledIfAvailableInFom()</code> function and
      * then not available in the FOM at runtime.
      * <p>
      * This method should only be used when connected, it will always return <code>false</code>
      * when not connected.
      *
      * @return <code>true</code> if connected and the HlaSovietIFFTransponder manager is enabled.
      */
      LIBAPI virtual bool isEnabled() = 0;
   };
}
#endif
