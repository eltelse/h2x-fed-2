/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLABURSTPOINTOBJECTMANAGER_H
#define DEVELOPER_STUDIO_HLABURSTPOINTOBJECTMANAGER_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <DevStudio/datatypes/ChemicalContentEnum.h>
#include <DevStudio/datatypes/DamageStatusEnum.h>
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EnvironmentObjectTypeStruct.h>
#include <DevStudio/datatypes/ForceIdentifierEnum.h>
#include <DevStudio/datatypes/OrientationStruct.h>
#include <DevStudio/datatypes/WorldLocationStruct.h>
#include <string>

#include <list>
#include <vector>

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaBurstPointObjectManagerListener.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaException.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

   /**
   * Manager used to manage HlaBurstPointObjects.
   */
   class HlaBurstPointObjectManager : private boost::noncopyable {

   public:

      LIBAPI virtual ~HlaBurstPointObjectManager() {}

      /**
      * Gets a list of all HlaBurstPointObjects within interest, both local and remote.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of HlaBurstPointObjects
      */
      LIBAPI virtual std::list<HlaBurstPointObjectPtr> getHlaBurstPointObjects() = 0;

      /**
      * Gets a list of all HlaBurstPointObjects, both local and remote.
      * HlaBurstPointObjects not within interest are also included.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of all HlaBurstPointObjects
      */
      LIBAPI virtual std::list<HlaBurstPointObjectPtr> getAllHlaBurstPointObjects() = 0;

      /**
      * Gets a list of local HlaBurstPointObjects within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of local HlaBurstPointObjects
      */
      LIBAPI virtual std::list<HlaBurstPointObjectPtr> getLocalHlaBurstPointObjects() = 0;

      /**
      * Gets a list of remote HlaBurstPointObjects within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of remote HlaBurstPointObjects
      */
      LIBAPI virtual std::list<HlaBurstPointObjectPtr> getRemoteHlaBurstPointObjects() = 0;

      /**
      * Find a HlaBurstPointObject with the given <code>percentOpacity</code>.
      * If more than one HlaBurstPointObject with the given <code>percentOpacity</code> is present, one of
      * them is returned, it is undefined which. If none is found, <code>null</code> is returned.
      * <p>
      * percentOpacity was selected as a <i>Lookup</i> attribute.
      *
      * @param percentOpacity The percentOpacity that looks up which HlaBurstPointObject to return.
      *
      * @return an HlaBurstPointObject, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaBurstPointObjectPtr getBurstPointObjectByPercentOpacity(unsigned int percentOpacity) = 0;

      /**
      * Find a HlaBurstPointObject with the given <code>cylinderSize</code>.
      * If more than one HlaBurstPointObject with the given <code>cylinderSize</code> is present, one of
      * them is returned, it is undefined which. If none is found, <code>null</code> is returned.
      * <p>
      * cylinderSize was selected as a <i>Lookup</i> attribute.
      *
      * @param cylinderSize The cylinderSize that looks up which HlaBurstPointObject to return.
      *
      * @return an HlaBurstPointObject, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaBurstPointObjectPtr getBurstPointObjectByCylinderSize(unsigned int cylinderSize) = 0;

      /**
      * Find a HlaBurstPointObject with the given <code>cylinderHeight</code>.
      * If more than one HlaBurstPointObject with the given <code>cylinderHeight</code> is present, one of
      * them is returned, it is undefined which. If none is found, <code>null</code> is returned.
      * <p>
      * cylinderHeight was selected as a <i>Lookup</i> attribute.
      *
      * @param cylinderHeight The cylinderHeight that looks up which HlaBurstPointObject to return.
      *
      * @return an HlaBurstPointObject, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaBurstPointObjectPtr getBurstPointObjectByCylinderHeight(unsigned int cylinderHeight) = 0;

      /**
      * Find a HlaBurstPointObject with the given <code>numberOfBursts</code>.
      * If more than one HlaBurstPointObject with the given <code>numberOfBursts</code> is present, one of
      * them is returned, it is undefined which. If none is found, <code>null</code> is returned.
      * <p>
      * numberOfBursts was selected as a <i>Lookup</i> attribute.
      *
      * @param numberOfBursts The numberOfBursts that looks up which HlaBurstPointObject to return.
      *
      * @return an HlaBurstPointObject, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaBurstPointObjectPtr getBurstPointObjectByNumberOfBursts(unsigned int numberOfBursts) = 0;

      /**
      * Find a HlaBurstPointObject with the given <code>chemicalContent</code>.
      * If more than one HlaBurstPointObject with the given <code>chemicalContent</code> is present, one of
      * them is returned, it is undefined which. If none is found, <code>null</code> is returned.
      * <p>
      * chemicalContent was selected as a <i>Lookup</i> attribute.
      *
      * @param chemicalContent The chemicalContent that looks up which HlaBurstPointObject to return.
      *
      * @return an HlaBurstPointObject, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaBurstPointObjectPtr getBurstPointObjectByChemicalContent(ChemicalContentEnum::ChemicalContentEnum chemicalContent) = 0;

      /**
      * Find a HlaBurstPointObject with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> of the HlaBurstPointObject to find
      *
      * @return the specified HlaBurstPointObject, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaBurstPointObjectPtr getBurstPointObjectByHlaInstanceName(const std::wstring& hlaInstanceName) = 0;

      /**
      * Find a HlaBurstPointObject with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param encodedHlaInstanceHandle the encoded <code>HLA instance handle</code> of the HlaBurstPointObject to find
      *
      * @return the specified HlaBurstPointObject, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaBurstPointObjectPtr getBurstPointObjectByHlaInstanceHandle(const std::vector<char>& encodedHlaInstanceHandle) = 0;

      /**
      * Creates a new HlaBurstPointObject, and sets all <i>Create</i> attributes.
      *
      * @return Returns a new local HlaBurstPointObject.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaBurstPointObjectPtr createLocalHlaBurstPointObject(
      ) THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Creates a new HlaBurstPointObject with a specified <code>HLA instance name</code>, and sets all
      * <i>Create</i> attributes.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> to register for this instance
      *
      * @return Returns a new local HlaBurstPointObject.
      *
      * @throws HlaIllegalInstanceNameException if the name is illegal
      * @throws HlaInstanceNameInUseException if the name is already in use
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaBurstPointObjectPtr createLocalHlaBurstPointObject(
         const std::wstring& hlaInstanceName
      ) THROW_SPEC (HlaIllegalInstanceNameException, HlaInstanceNameInUseException,
                    HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaBurstPointObject and removes it from the federation.
      *
      * @param burstPointObject The HlaBurstPointObject to delete
      *
      * @return the HlaBurstPointObject deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaBurstPointObjectPtr deleteLocalHlaBurstPointObject(HlaBurstPointObjectPtr burstPointObject)
          THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                      HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaBurstPointObject and removes it from the federation.
      *
      * @param burstPointObject The HlaBurstPointObject to delete
      * @param timestamp timestamp when the instance was removed.
      *
      * @return the HlaBurstPointObject deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaBurstPointObjectPtr deleteLocalHlaBurstPointObject(HlaBurstPointObjectPtr burstPointObject, HlaTimeStampPtr timestamp)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaBurstPointObject and removes it from the federation.
      *
      * @param burstPointObject The HlaBurstPointObject to delete
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaBurstPointObject deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaBurstPointObjectPtr deleteLocalHlaBurstPointObject(HlaBurstPointObjectPtr burstPointObject, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaBurstPointObject and removes it from the federation.
      *
      * @param burstPointObject The HlaBurstPointObject to delete
      * @param timestamp timestamp when the instance was removed.
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaBurstPointObject deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaBurstPointObjectPtr deleteLocalHlaBurstPointObject(HlaBurstPointObjectPtr burstPointObject, HlaTimeStampPtr timestamp, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Add a HlaBurstPointObject manager listener.
      *
      * @param managerListener listener to add
      */
      LIBAPI virtual void addHlaBurstPointObjectManagerListener(HlaBurstPointObjectManagerListenerPtr managerListener) = 0;

      /**
      * Removed a HlaBurstPointObject manager listener.
      *
      * @param managerListener listener to remove
      */
      LIBAPI virtual void removeHlaBurstPointObjectManagerListener(HlaBurstPointObjectManagerListenerPtr managerListener) = 0;

     /**
      * Add a listener that will automatically be added to all instances of HlaBurstPointObject (Both local and remote).
      * The listener is notified when any attribute of an instance of HlaBurstPointObject is updated.
      * The listener is also called when an interaction is sent to an instance of HlaBurstPointObject.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param listener The listener to add.
      */
      LIBAPI virtual void addHlaBurstPointObjectDefaultInstanceListener(HlaBurstPointObjectListenerPtr listener) = 0;

     /**
      * Remove a listener from the set of listeners that are automatically added to new instances of HlaBurstPointObject.
      * Note: The listener will not be removed from already existing instances of HlaBurstPointObject.
      * This method is idempotent.
      *
      * @param listener The listener to remove.
      */
      LIBAPI virtual void removeHlaBurstPointObjectDefaultInstanceListener(HlaBurstPointObjectListenerPtr listener) = 0;

     /**
      * Add a valueListener that will automatically be added to all instances of HlaBurstPointObject (Both local and remote).
      * The  Valuelistener is notified when any attribute of an instance of HlaBurstPointObject is updated.
      * Note that the attribute values that are notified are not guaranteed to
      * be the latest available values for that attribute.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to add.
      */
      LIBAPI virtual void addHlaBurstPointObjectDefaultInstanceValueListener(HlaBurstPointObjectValueListenerPtr valueListener) = 0;

     /**
      * Remove a valueListener from the set of listeners that are automatically added to new instances of HlaBurstPointObject.
      * Note: The valueListener will not be removed from already existing instances of HlaBurstPointObject.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to remove.
      */
      LIBAPI virtual void removeHlaBurstPointObjectDefaultInstanceValueListener(HlaBurstPointObjectValueListenerPtr valueListener) = 0;

     /**
      * Enables or disables the HlaBurstPointObject manager.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaBurstPointObject manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      *
      * @param enabled <code>true</code> to enable the HlaBurstPointObject manager, otherwise <code>false</code>
      */
      LIBAPI virtual void setEnabled(bool enabled) = 0;

     /**
      * Enables the HlaBurstPointObject manager if it is avalable in the FOM at runtime.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaBurstPointObject manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      */
      LIBAPI virtual void setEnabledIfAvailableInFom() = 0;

     /**
      * Check if the HlaBurstPointObject manager is actually enabled when connected.
      * An HlaBurstPointObject manager may be disabled with the <code>setEnabled(false)</code> function, or it may be configured
      * as <i>if available in the FOM</i> by the <code>setEnabledIfAvailableInFom()</code> function and
      * then not available in the FOM at runtime.
      * <p>
      * This method should only be used when connected, it will always return <code>false</code>
      * when not connected.
      *
      * @return <code>true</code> if connected and the HlaBurstPointObject manager is enabled.
      */
      LIBAPI virtual bool isEnabled() = 0;
   };
}
#endif
