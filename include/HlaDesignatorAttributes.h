/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLADESIGNATORATTRIBUTES_H
#define DEVELOPER_STUDIO_HLADESIGNATORATTRIBUTES_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <string>
#include <vector>
#include <utility>
    
#include <boost/noncopyable.hpp>
    
#include <DevStudio/datatypes/AccelerationVectorStruct.h>
#include <DevStudio/datatypes/DeadReckoningAlgorithmEnum.h>
#include <DevStudio/datatypes/DesignatorCodeEnum.h>
#include <DevStudio/datatypes/DesignatorCodeNameEnum.h>
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/RelativePositionStruct.h>
#include <DevStudio/datatypes/WorldLocationStruct.h>
#include <string>

#include <DevStudio/HlaException.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaEmbeddedSystemAttributes.h>
#include <DevStudio/HlaTimeStamped.h>

namespace DevStudio {

   /**
   * Interface used to represent all attributes for an object instance.
   */
   class HlaDesignatorAttributes : public HlaEmbeddedSystemAttributes {
   public:


     /**
      * An enumeration of the attributes of an HlaDesignator
      *
      *<table summary="All attributes">
      * <tr><td><b>Enum constant</b></td><td><b>C++ name</b></td><td><b>FOM name</b></td></tr>
      * <tr><td>CODE_NAME</td><td>codeName</td><td><code>CodeName</code></td></tr>
      * <tr><td>DESIGNATED_OBJECT_IDENTIFIER</td><td>designatedObjectIdentifier</td><td><code>DesignatedObjectIdentifier</code></td></tr>
      * <tr><td>DESIGNATOR_CODE</td><td>designatorCode</td><td><code>DesignatorCode</code></td></tr>
      * <tr><td>DESIGNATOR_EMISSION_WAVELENGTH</td><td>designatorEmissionWavelength</td><td><code>DesignatorEmissionWavelength</code></td></tr>
      * <tr><td>DESIGNATOR_OUTPUT_POWER</td><td>designatorOutputPower</td><td><code>DesignatorOutputPower</code></td></tr>
      * <tr><td>DESIGNATOR_SPOT_LOCATION</td><td>designatorSpotLocation</td><td><code>DesignatorSpotLocation</code></td></tr>
      * <tr><td>DEAD_RECKONING_ALGORITHM</td><td>deadReckoningAlgorithm</td><td><code>DeadReckoningAlgorithm</code></td></tr>
      * <tr><td>RELATIVE_SPOT_LOCATION</td><td>relativeSpotLocation</td><td><code>RelativeSpotLocation</code></td></tr>
      * <tr><td>SPOT_LINEAR_ACCELERATION_VECTOR</td><td>spotLinearAccelerationVector</td><td><code>SpotLinearAccelerationVector</code></td></tr>
      * <tr><td>ENTITY_IDENTIFIER</td><td>entityIdentifier</td><td><code>EntityIdentifier</code></td></tr>
      * <tr><td>HOST_OBJECT_IDENTIFIER</td><td>hostObjectIdentifier</td><td><code>HostObjectIdentifier</code></td></tr>
      * <tr><td>RELATIVE_POSITION</td><td>relativePosition</td><td><code>RelativePosition</code></td></tr>
      *</table>
      */
      enum Attribute {
        /**
        * codeName (FOM name: <code>CodeName</code>).
        * <br>Description from the FOM: <i>The code name of the designator system.</i>
        */
         CODE_NAME,

        /**
        * designatedObjectIdentifier (FOM name: <code>DesignatedObjectIdentifier</code>).
        * <br>Description from the FOM: <i>The object instance ID of the entity that is currently being designated (if any).</i>
        */
         DESIGNATED_OBJECT_IDENTIFIER,

        /**
        * designatorCode (FOM name: <code>DesignatorCode</code>).
        * <br>Description from the FOM: <i>The designator code being used by the designating entity.</i>
        */
         DESIGNATOR_CODE,

        /**
        * designatorEmissionWavelength (FOM name: <code>DesignatorEmissionWavelength</code>).
        * <br>Description from the FOM: <i>The wavelength of the designator system.</i>
        */
         DESIGNATOR_EMISSION_WAVELENGTH,

        /**
        * designatorOutputPower (FOM name: <code>DesignatorOutputPower</code>).
        * <br>Description from the FOM: <i>The output power of the designator system.</i>
        */
         DESIGNATOR_OUTPUT_POWER,

        /**
        * designatorSpotLocation (FOM name: <code>DesignatorSpotLocation</code>).
        * <br>Description from the FOM: <i>The location, in the world coordinate system, of the designator spot.</i>
        */
         DESIGNATOR_SPOT_LOCATION,

        /**
        * deadReckoningAlgorithm (FOM name: <code>DeadReckoningAlgorithm</code>).
        * <br>Description from the FOM: <i>Dead reckoning algorithm used by the issuing object.</i>
        */
         DEAD_RECKONING_ALGORITHM,

        /**
        * relativeSpotLocation (FOM name: <code>RelativeSpotLocation</code>).
        * <br>Description from the FOM: <i>The location of the designator spot, relative to the object being designated (if any).</i>
        */
         RELATIVE_SPOT_LOCATION,

        /**
        * spotLinearAccelerationVector (FOM name: <code>SpotLinearAccelerationVector</code>).
        * <br>Description from the FOM: <i>The rate of change in linear velocity of the designator spot over time.</i>
        */
         SPOT_LINEAR_ACCELERATION_VECTOR,

        /**
        * entityIdentifier (FOM name: <code>EntityIdentifier</code>).
        * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
        */
         ENTITY_IDENTIFIER,

        /**
        * hostObjectIdentifier (FOM name: <code>HostObjectIdentifier</code>).
        * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
        */
         HOST_OBJECT_IDENTIFIER,

        /**
        * relativePosition (FOM name: <code>RelativePosition</code>).
        * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
        */
         RELATIVE_POSITION
      };

     /**
      * Gets the name of the attribute.
      *
      * @return The name of the attribute. An empty string will be returned if the attribute does not exist.
      */
      LIBAPI virtual std::wstring getName(Attribute attribute);

     /**
      * Finds the attribute specified in the parameter attributeName.
      * The found enumeration will be stored in the parameter attribute.
      *
      * @return true if the attribute was found, false otherwise.
      */
      LIBAPI virtual bool find(Attribute& attribute, std::wstring attributeName);

      LIBAPI virtual ~HlaDesignatorAttributes() {}
    
      /**
      * Returns true if the <code>codeName</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The code name of the designator system.</i>
      *
      * @return true if <code>codeName</code> is available.
      */
      LIBAPI virtual bool hasCodeName() = 0;

      /**
      * Gets the value of the <code>codeName</code> attribute.
      *
      * <br>Description from the FOM: <i>The code name of the designator system.</i>
      * <br>Description of the data type from the FOM: <i>Designator code name</i>
      *
      * @return the <code>codeName</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::DesignatorCodeNameEnum::DesignatorCodeNameEnum getCodeName()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>codeName</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The code name of the designator system.</i>
      * <br>Description of the data type from the FOM: <i>Designator code name</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>codeName</code> attribute.
      */
      LIBAPI virtual DevStudio::DesignatorCodeNameEnum::DesignatorCodeNameEnum getCodeName(DevStudio::DesignatorCodeNameEnum::DesignatorCodeNameEnum defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>codeName</code> attribute.
      * <br>Description from the FOM: <i>The code name of the designator system.</i>
      * <br>Description of the data type from the FOM: <i>Designator code name</i>
      *
      * @return the time stamped <code>codeName</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::DesignatorCodeNameEnum::DesignatorCodeNameEnum > getCodeNameTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>designatedObjectIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The object instance ID of the entity that is currently being designated (if any).</i>
      *
      * @return true if <code>designatedObjectIdentifier</code> is available.
      */
      LIBAPI virtual bool hasDesignatedObjectIdentifier() = 0;

      /**
      * Gets the value of the <code>designatedObjectIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>The object instance ID of the entity that is currently being designated (if any).</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @return the <code>designatedObjectIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::string getDesignatedObjectIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>designatedObjectIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The object instance ID of the entity that is currently being designated (if any).</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>designatedObjectIdentifier</code> attribute.
      */
      LIBAPI virtual std::string getDesignatedObjectIdentifier(std::string defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>designatedObjectIdentifier</code> attribute.
      * <br>Description from the FOM: <i>The object instance ID of the entity that is currently being designated (if any).</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @return the time stamped <code>designatedObjectIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::string > getDesignatedObjectIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>designatorCode</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The designator code being used by the designating entity.</i>
      *
      * @return true if <code>designatorCode</code> is available.
      */
      LIBAPI virtual bool hasDesignatorCode() = 0;

      /**
      * Gets the value of the <code>designatorCode</code> attribute.
      *
      * <br>Description from the FOM: <i>The designator code being used by the designating entity.</i>
      * <br>Description of the data type from the FOM: <i>Designator code</i>
      *
      * @return the <code>designatorCode</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::DesignatorCodeEnum::DesignatorCodeEnum getDesignatorCode()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>designatorCode</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The designator code being used by the designating entity.</i>
      * <br>Description of the data type from the FOM: <i>Designator code</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>designatorCode</code> attribute.
      */
      LIBAPI virtual DevStudio::DesignatorCodeEnum::DesignatorCodeEnum getDesignatorCode(DevStudio::DesignatorCodeEnum::DesignatorCodeEnum defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>designatorCode</code> attribute.
      * <br>Description from the FOM: <i>The designator code being used by the designating entity.</i>
      * <br>Description of the data type from the FOM: <i>Designator code</i>
      *
      * @return the time stamped <code>designatorCode</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::DesignatorCodeEnum::DesignatorCodeEnum > getDesignatorCodeTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>designatorEmissionWavelength</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The wavelength of the designator system.</i>
      *
      * @return true if <code>designatorEmissionWavelength</code> is available.
      */
      LIBAPI virtual bool hasDesignatorEmissionWavelength() = 0;

      /**
      * Gets the value of the <code>designatorEmissionWavelength</code> attribute.
      *
      * <br>Description from the FOM: <i>The wavelength of the designator system.</i>
      * <br>Description of the data type from the FOM: <i>Wavelength expressed in micrometer. [unit: micron, resolution: NA, accuracy: perfect]</i>
      *
      * @return the <code>designatorEmissionWavelength</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual float getDesignatorEmissionWavelength()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>designatorEmissionWavelength</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The wavelength of the designator system.</i>
      * <br>Description of the data type from the FOM: <i>Wavelength expressed in micrometer. [unit: micron, resolution: NA, accuracy: perfect]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>designatorEmissionWavelength</code> attribute.
      */
      LIBAPI virtual float getDesignatorEmissionWavelength(float defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>designatorEmissionWavelength</code> attribute.
      * <br>Description from the FOM: <i>The wavelength of the designator system.</i>
      * <br>Description of the data type from the FOM: <i>Wavelength expressed in micrometer. [unit: micron, resolution: NA, accuracy: perfect]</i>
      *
      * @return the time stamped <code>designatorEmissionWavelength</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< float > getDesignatorEmissionWavelengthTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>designatorOutputPower</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The output power of the designator system.</i>
      *
      * @return true if <code>designatorOutputPower</code> is available.
      */
      LIBAPI virtual bool hasDesignatorOutputPower() = 0;

      /**
      * Gets the value of the <code>designatorOutputPower</code> attribute.
      *
      * <br>Description from the FOM: <i>The output power of the designator system.</i>
      * <br>Description of the data type from the FOM: <i>The unit of power is the watt (W), which is equal to one joule per second. [unit: watt (W), resolution: NA, accuracy: perfect]</i>
      *
      * @return the <code>designatorOutputPower</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual float getDesignatorOutputPower()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>designatorOutputPower</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The output power of the designator system.</i>
      * <br>Description of the data type from the FOM: <i>The unit of power is the watt (W), which is equal to one joule per second. [unit: watt (W), resolution: NA, accuracy: perfect]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>designatorOutputPower</code> attribute.
      */
      LIBAPI virtual float getDesignatorOutputPower(float defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>designatorOutputPower</code> attribute.
      * <br>Description from the FOM: <i>The output power of the designator system.</i>
      * <br>Description of the data type from the FOM: <i>The unit of power is the watt (W), which is equal to one joule per second. [unit: watt (W), resolution: NA, accuracy: perfect]</i>
      *
      * @return the time stamped <code>designatorOutputPower</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< float > getDesignatorOutputPowerTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>designatorSpotLocation</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The location, in the world coordinate system, of the designator spot.</i>
      *
      * @return true if <code>designatorSpotLocation</code> is available.
      */
      LIBAPI virtual bool hasDesignatorSpotLocation() = 0;

      /**
      * Gets the value of the <code>designatorSpotLocation</code> attribute.
      *
      * <br>Description from the FOM: <i>The location, in the world coordinate system, of the designator spot.</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @return the <code>designatorSpotLocation</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::WorldLocationStruct getDesignatorSpotLocation()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>designatorSpotLocation</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The location, in the world coordinate system, of the designator spot.</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>designatorSpotLocation</code> attribute.
      */
      LIBAPI virtual DevStudio::WorldLocationStruct getDesignatorSpotLocation(DevStudio::WorldLocationStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>designatorSpotLocation</code> attribute.
      * <br>Description from the FOM: <i>The location, in the world coordinate system, of the designator spot.</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @return the time stamped <code>designatorSpotLocation</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::WorldLocationStruct > getDesignatorSpotLocationTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>deadReckoningAlgorithm</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Dead reckoning algorithm used by the issuing object.</i>
      *
      * @return true if <code>deadReckoningAlgorithm</code> is available.
      */
      LIBAPI virtual bool hasDeadReckoningAlgorithm() = 0;

      /**
      * Gets the value of the <code>deadReckoningAlgorithm</code> attribute.
      *
      * <br>Description from the FOM: <i>Dead reckoning algorithm used by the issuing object.</i>
      * <br>Description of the data type from the FOM: <i>Dead-reckoning algorithm</i>
      *
      * @return the <code>deadReckoningAlgorithm</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::DeadReckoningAlgorithmEnum::DeadReckoningAlgorithmEnum getDeadReckoningAlgorithm()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>deadReckoningAlgorithm</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Dead reckoning algorithm used by the issuing object.</i>
      * <br>Description of the data type from the FOM: <i>Dead-reckoning algorithm</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>deadReckoningAlgorithm</code> attribute.
      */
      LIBAPI virtual DevStudio::DeadReckoningAlgorithmEnum::DeadReckoningAlgorithmEnum getDeadReckoningAlgorithm(DevStudio::DeadReckoningAlgorithmEnum::DeadReckoningAlgorithmEnum defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>deadReckoningAlgorithm</code> attribute.
      * <br>Description from the FOM: <i>Dead reckoning algorithm used by the issuing object.</i>
      * <br>Description of the data type from the FOM: <i>Dead-reckoning algorithm</i>
      *
      * @return the time stamped <code>deadReckoningAlgorithm</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::DeadReckoningAlgorithmEnum::DeadReckoningAlgorithmEnum > getDeadReckoningAlgorithmTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>relativeSpotLocation</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The location of the designator spot, relative to the object being designated (if any).</i>
      *
      * @return true if <code>relativeSpotLocation</code> is available.
      */
      LIBAPI virtual bool hasRelativeSpotLocation() = 0;

      /**
      * Gets the value of the <code>relativeSpotLocation</code> attribute.
      *
      * <br>Description from the FOM: <i>The location of the designator spot, relative to the object being designated (if any).</i>
      * <br>Description of the data type from the FOM: <i>Relative position in right-handed Cartesian coordinates.</i>
      *
      * @return the <code>relativeSpotLocation</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::RelativePositionStruct getRelativeSpotLocation()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>relativeSpotLocation</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The location of the designator spot, relative to the object being designated (if any).</i>
      * <br>Description of the data type from the FOM: <i>Relative position in right-handed Cartesian coordinates.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>relativeSpotLocation</code> attribute.
      */
      LIBAPI virtual DevStudio::RelativePositionStruct getRelativeSpotLocation(DevStudio::RelativePositionStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>relativeSpotLocation</code> attribute.
      * <br>Description from the FOM: <i>The location of the designator spot, relative to the object being designated (if any).</i>
      * <br>Description of the data type from the FOM: <i>Relative position in right-handed Cartesian coordinates.</i>
      *
      * @return the time stamped <code>relativeSpotLocation</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::RelativePositionStruct > getRelativeSpotLocationTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>spotLinearAccelerationVector</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The rate of change in linear velocity of the designator spot over time.</i>
      *
      * @return true if <code>spotLinearAccelerationVector</code> is available.
      */
      LIBAPI virtual bool hasSpotLinearAccelerationVector() = 0;

      /**
      * Gets the value of the <code>spotLinearAccelerationVector</code> attribute.
      *
      * <br>Description from the FOM: <i>The rate of change in linear velocity of the designator spot over time.</i>
      * <br>Description of the data type from the FOM: <i>The magnitude of the change in linear velocity over time.</i>
      *
      * @return the <code>spotLinearAccelerationVector</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::AccelerationVectorStruct getSpotLinearAccelerationVector()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>spotLinearAccelerationVector</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The rate of change in linear velocity of the designator spot over time.</i>
      * <br>Description of the data type from the FOM: <i>The magnitude of the change in linear velocity over time.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>spotLinearAccelerationVector</code> attribute.
      */
      LIBAPI virtual DevStudio::AccelerationVectorStruct getSpotLinearAccelerationVector(DevStudio::AccelerationVectorStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>spotLinearAccelerationVector</code> attribute.
      * <br>Description from the FOM: <i>The rate of change in linear velocity of the designator spot over time.</i>
      * <br>Description of the data type from the FOM: <i>The magnitude of the change in linear velocity over time.</i>
      *
      * @return the time stamped <code>spotLinearAccelerationVector</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::AccelerationVectorStruct > getSpotLinearAccelerationVectorTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>entityIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
      *
      * @return true if <code>entityIdentifier</code> is available.
      */
      LIBAPI virtual bool hasEntityIdentifier() = 0;

      /**
      * Gets the value of the <code>entityIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @return the <code>entityIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::EntityIdentifierStruct getEntityIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>entityIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>entityIdentifier</code> attribute.
      */
      LIBAPI virtual DevStudio::EntityIdentifierStruct getEntityIdentifier(DevStudio::EntityIdentifierStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>entityIdentifier</code> attribute.
      * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @return the time stamped <code>entityIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::EntityIdentifierStruct > getEntityIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>hostObjectIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
      *
      * @return true if <code>hostObjectIdentifier</code> is available.
      */
      LIBAPI virtual bool hasHostObjectIdentifier() = 0;

      /**
      * Gets the value of the <code>hostObjectIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @return the <code>hostObjectIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::string getHostObjectIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>hostObjectIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>hostObjectIdentifier</code> attribute.
      */
      LIBAPI virtual std::string getHostObjectIdentifier(std::string defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>hostObjectIdentifier</code> attribute.
      * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @return the time stamped <code>hostObjectIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::string > getHostObjectIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>relativePosition</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
      *
      * @return true if <code>relativePosition</code> is available.
      */
      LIBAPI virtual bool hasRelativePosition() = 0;

      /**
      * Gets the value of the <code>relativePosition</code> attribute.
      *
      * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
      * <br>Description of the data type from the FOM: <i>Relative position in right-handed Cartesian coordinates.</i>
      *
      * @return the <code>relativePosition</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::RelativePositionStruct getRelativePosition()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>relativePosition</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
      * <br>Description of the data type from the FOM: <i>Relative position in right-handed Cartesian coordinates.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>relativePosition</code> attribute.
      */
      LIBAPI virtual DevStudio::RelativePositionStruct getRelativePosition(DevStudio::RelativePositionStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>relativePosition</code> attribute.
      * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
      * <br>Description of the data type from the FOM: <i>Relative position in right-handed Cartesian coordinates.</i>
      *
      * @return the time stamped <code>relativePosition</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::RelativePositionStruct > getRelativePositionTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
   };
}
#endif
