/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLASENSORLISTENER_H
#define DEVELOPER_STUDIO_HLASENSORLISTENER_H

#include <set>


#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaSensorAttributes.h>

namespace DevStudio {
   /**
   * Listener for updates of attributes.  
   */
   class HlaSensorListener {
   public:

      LIBAPI virtual ~HlaSensorListener() {}

      /**
      * This method is called when a HLA <code>reflectAttributeValueUpdate</code> is received for an remote object,
      * or a set of attributes is updated on a local object.
      *
      * @param sensor The sensor which this update corresponds to.
      * @param attributes The set of attributes that are updated.
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time the update was initiated.
      */
      LIBAPI virtual void attributesUpdated(HlaSensorPtr sensor, std::set<HlaSensorAttributes::Attribute> &attributes, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

      class Adapter;
   };

  /**
  * An adapter class that implements the HlaSensorListener interface with empty methods.
  * It might be used as a base class for a listener.
  */
  class HlaSensorListener::Adapter : public HlaSensorListener {

  public:

     LIBAPI virtual void attributesUpdated(HlaSensorPtr sensor, std::set<HlaSensorAttributes::Attribute> &attributes, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
    };

}
#endif
