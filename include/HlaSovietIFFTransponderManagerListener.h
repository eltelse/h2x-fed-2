/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLASOVIETIFFTRANSPONDERMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLASOVIETIFFTRANSPONDERMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaSovietIFFTransponder.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaSovietIFFTransponder instances.
    */
    class HlaSovietIFFTransponderManagerListener {

    public:

        LIBAPI virtual ~HlaSovietIFFTransponderManagerListener() {}

        /**
        * This method is called when a new HlaSovietIFFTransponder instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param sovietIFFTransponder the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaSovietIFFTransponderDiscovered(HlaSovietIFFTransponderPtr sovietIFFTransponder, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaSovietIFFTransponder instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param sovietIFFTransponder the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaSovietIFFTransponderInitialized(HlaSovietIFFTransponderPtr sovietIFFTransponder, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaSovietIFFTransponderManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param sovietIFFTransponder the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaSovietIFFTransponderInInterest(HlaSovietIFFTransponderPtr sovietIFFTransponder, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaSovietIFFTransponderManagerListener instance goes out of interest.
        *
        * @param sovietIFFTransponder the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaSovietIFFTransponderOutOfInterest(HlaSovietIFFTransponderPtr sovietIFFTransponder, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaSovietIFFTransponder instance is deleted.
        *
        * @param sovietIFFTransponder the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaSovietIFFTransponderDeleted(HlaSovietIFFTransponderPtr sovietIFFTransponder, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaSovietIFFTransponderManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaSovietIFFTransponderManagerListener::Adapter : public HlaSovietIFFTransponderManagerListener {

    public:
        LIBAPI virtual void hlaSovietIFFTransponderDiscovered(HlaSovietIFFTransponderPtr sovietIFFTransponder, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaSovietIFFTransponderInitialized(HlaSovietIFFTransponderPtr sovietIFFTransponder, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaSovietIFFTransponderInInterest(HlaSovietIFFTransponderPtr sovietIFFTransponder, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaSovietIFFTransponderOutOfInterest(HlaSovietIFFTransponderPtr sovietIFFTransponder, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaSovietIFFTransponderDeleted(HlaSovietIFFTransponderPtr sovietIFFTransponder, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
