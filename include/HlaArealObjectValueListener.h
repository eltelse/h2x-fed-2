/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAAREALOBJECTVALUELISTENER_H
#define DEVELOPER_STUDIO_HLAAREALOBJECTVALUELISTENER_H

#include <memory>

#include <DevStudio/datatypes/DamageStatusEnum.h>
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EnvironmentObjectTypeStruct.h>
#include <DevStudio/datatypes/ForceIdentifierEnum.h>
#include <DevStudio/datatypes/WorldLocationStruct.h>
#include <DevStudio/datatypes/WorldLocationStructLengthlessArray.h>
#include <string>
#include <vector>

#include <DevStudio/HlaLogicalTime.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaArealObjectAttributes.h>    

namespace DevStudio {

   /**
   * Listener for updates of attributes, with the new updated values.  
   */
   class HlaArealObjectValueListener {

   public:

      LIBAPI virtual ~HlaArealObjectValueListener() {}
    
      /**
      * This method is called when the attribute <code>pointsData</code> is updated.
      * <br>Description from the FOM: <i>Specifies the physical location (a collection of points) of the object</i>
      * <br>Description of the data type from the FOM: <i>Dynamic array of WorldLocationStruct elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
      *
      * @param arealObject The object which is updated.
      * @param pointsData The new value of the attribute in this update
      * @param validOldPointsData True if oldPointsData contains a valid value
      * @param oldPointsData The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void pointsDataUpdated(HlaArealObjectPtr arealObject, std::vector<DevStudio::WorldLocationStruct > pointsData, bool validOldPointsData, std::vector<DevStudio::WorldLocationStruct > oldPointsData, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>percentComplete</code> is updated.
      * <br>Description from the FOM: <i>Specifies the percent completion of the object</i>
      * <br>Description of the data type from the FOM: <i>Percentage [unit: percent (%), resolution: 1, accuracy: perfect]</i>
      *
      * @param arealObject The object which is updated.
      * @param percentComplete The new value of the attribute in this update
      * @param validOldPercentComplete True if oldPercentComplete contains a valid value
      * @param oldPercentComplete The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void percentCompleteUpdated(HlaArealObjectPtr arealObject, unsigned int percentComplete, bool validOldPercentComplete, unsigned int oldPercentComplete, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>damagedAppearance</code> is updated.
      * <br>Description from the FOM: <i>Specifies the damaged appearance of the object</i>
      * <br>Description of the data type from the FOM: <i>Damaged appearance</i>
      *
      * @param arealObject The object which is updated.
      * @param damagedAppearance The new value of the attribute in this update
      * @param validOldDamagedAppearance True if oldDamagedAppearance contains a valid value
      * @param oldDamagedAppearance The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void damagedAppearanceUpdated(HlaArealObjectPtr arealObject, DamageStatusEnum::DamageStatusEnum damagedAppearance, bool validOldDamagedAppearance, DamageStatusEnum::DamageStatusEnum oldDamagedAppearance, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>objectPreDistributed</code> is updated.
      * <br>Description from the FOM: <i>Specifies whether or not the object was created before the start of the exercise</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param arealObject The object which is updated.
      * @param objectPreDistributed The new value of the attribute in this update
      * @param validOldObjectPreDistributed True if oldObjectPreDistributed contains a valid value
      * @param oldObjectPreDistributed The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void objectPreDistributedUpdated(HlaArealObjectPtr arealObject, bool objectPreDistributed, bool validOldObjectPreDistributed, bool oldObjectPreDistributed, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>deactivated</code> is updated.
      * <br>Description from the FOM: <i>Specifies whether or not the object has been deactivated (it has ceased to exist in the synthetic environment)</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param arealObject The object which is updated.
      * @param deactivated The new value of the attribute in this update
      * @param validOldDeactivated True if oldDeactivated contains a valid value
      * @param oldDeactivated The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void deactivatedUpdated(HlaArealObjectPtr arealObject, bool deactivated, bool validOldDeactivated, bool oldDeactivated, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>smoking</code> is updated.
      * <br>Description from the FOM: <i>Specifies whether or not the object is smoking (creating a smoke plume)</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param arealObject The object which is updated.
      * @param smoking The new value of the attribute in this update
      * @param validOldSmoking True if oldSmoking contains a valid value
      * @param oldSmoking The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void smokingUpdated(HlaArealObjectPtr arealObject, bool smoking, bool validOldSmoking, bool oldSmoking, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>flaming</code> is updated.
      * <br>Description from the FOM: <i>Specifies whether or not the object is aflame</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param arealObject The object which is updated.
      * @param flaming The new value of the attribute in this update
      * @param validOldFlaming True if oldFlaming contains a valid value
      * @param oldFlaming The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void flamingUpdated(HlaArealObjectPtr arealObject, bool flaming, bool validOldFlaming, bool oldFlaming, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>objectIdentifier</code> is updated.
      * <br>Description from the FOM: <i>Identifies this EnvironmentObject instance (point, linear or areal)</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @param arealObject The object which is updated.
      * @param objectIdentifier The new value of the attribute in this update
      * @param validOldObjectIdentifier True if oldObjectIdentifier contains a valid value
      * @param oldObjectIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void objectIdentifierUpdated(HlaArealObjectPtr arealObject, EntityIdentifierStruct objectIdentifier, bool validOldObjectIdentifier, EntityIdentifierStruct oldObjectIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>referencedObjectIdentifier</code> is updated.
      * <br>Description from the FOM: <i>Identifies the Synthetic Environment object instance to which this EnvironmentObject instance is associated</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @param arealObject The object which is updated.
      * @param referencedObjectIdentifier The new value of the attribute in this update
      * @param validOldReferencedObjectIdentifier True if oldReferencedObjectIdentifier contains a valid value
      * @param oldReferencedObjectIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void referencedObjectIdentifierUpdated(HlaArealObjectPtr arealObject, std::string referencedObjectIdentifier, bool validOldReferencedObjectIdentifier, std::string oldReferencedObjectIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>forceIdentifier</code> is updated.
      * <br>Description from the FOM: <i>Identifies the force that created or modified this EnvironmentObject instance</i>
      * <br>Description of the data type from the FOM: <i>Force ID</i>
      *
      * @param arealObject The object which is updated.
      * @param forceIdentifier The new value of the attribute in this update
      * @param validOldForceIdentifier True if oldForceIdentifier contains a valid value
      * @param oldForceIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void forceIdentifierUpdated(HlaArealObjectPtr arealObject, ForceIdentifierEnum::ForceIdentifierEnum forceIdentifier, bool validOldForceIdentifier, ForceIdentifierEnum::ForceIdentifierEnum oldForceIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>objectType</code> is updated.
      * <br>Description from the FOM: <i>Identifies the type of this EnvironmentObject instance</i>
      * <br>Description of the data type from the FOM: <i>Record specifying the domain, the kind and the specific identification of the environment object</i>
      *
      * @param arealObject The object which is updated.
      * @param objectType The new value of the attribute in this update
      * @param validOldObjectType True if oldObjectType contains a valid value
      * @param oldObjectType The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void objectTypeUpdated(HlaArealObjectPtr arealObject, EnvironmentObjectTypeStruct objectType, bool validOldObjectType, EnvironmentObjectTypeStruct oldObjectType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
      /**
      * This method is called when the producing federate of an attribute is changed.
      * Only available when using HLA Evolved.
      *
      * @param arealObject The object which is updated.
      * @param attribute The attribute that now has a new producing federate
      * @param oldProducingFederate The federate handle of the old producing federate
      * @param newProducingFederate The federate handle of the new producing federate
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void producingFederateUpdated(HlaArealObjectPtr arealObject, HlaArealObjectAttributes::Attribute attribute, HlaFederateIdPtr oldProducingFederate, HlaFederateIdPtr newProducingFederate, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;

      class Adapter;
   };

   /**
   * An adapter class that implements the HlaArealObjectValueListener interface with empty methods.
   * It might be used as a base class for a listener.
   */
   class HlaArealObjectValueListener::Adapter : public HlaArealObjectValueListener {

   public:

      LIBAPI virtual void pointsDataUpdated(HlaArealObjectPtr arealObject, std::vector<DevStudio::WorldLocationStruct > pointsData, bool validOldPointsData, std::vector<DevStudio::WorldLocationStruct > oldPointsData, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void percentCompleteUpdated(HlaArealObjectPtr arealObject, unsigned int percentComplete, bool validOldPercentComplete, unsigned int oldPercentComplete, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void damagedAppearanceUpdated(HlaArealObjectPtr arealObject, DamageStatusEnum::DamageStatusEnum damagedAppearance, bool validOldDamagedAppearance, DamageStatusEnum::DamageStatusEnum oldDamagedAppearance, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void objectPreDistributedUpdated(HlaArealObjectPtr arealObject, bool objectPreDistributed, bool validOldObjectPreDistributed, bool oldObjectPreDistributed, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void deactivatedUpdated(HlaArealObjectPtr arealObject, bool deactivated, bool validOldDeactivated, bool oldDeactivated, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void smokingUpdated(HlaArealObjectPtr arealObject, bool smoking, bool validOldSmoking, bool oldSmoking, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void flamingUpdated(HlaArealObjectPtr arealObject, bool flaming, bool validOldFlaming, bool oldFlaming, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void objectIdentifierUpdated(HlaArealObjectPtr arealObject, EntityIdentifierStruct objectIdentifier, bool validOldObjectIdentifier, EntityIdentifierStruct oldObjectIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void referencedObjectIdentifierUpdated(HlaArealObjectPtr arealObject, std::string referencedObjectIdentifier, bool validOldReferencedObjectIdentifier, std::string oldReferencedObjectIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void forceIdentifierUpdated(HlaArealObjectPtr arealObject, ForceIdentifierEnum::ForceIdentifierEnum forceIdentifier, bool validOldForceIdentifier, ForceIdentifierEnum::ForceIdentifierEnum oldForceIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void objectTypeUpdated(HlaArealObjectPtr arealObject, EnvironmentObjectTypeStruct objectType, bool validOldObjectType, EnvironmentObjectTypeStruct oldObjectType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
      LIBAPI virtual void producingFederateUpdated(HlaArealObjectPtr arealObject, HlaArealObjectAttributes::Attribute attribute, HlaFederateIdPtr oldProducingFederate, HlaFederateIdPtr newProducingFederate, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
   };

}
#endif
