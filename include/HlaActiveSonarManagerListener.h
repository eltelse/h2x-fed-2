/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAACTIVESONARMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLAACTIVESONARMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaActiveSonar.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaActiveSonar instances.
    */
    class HlaActiveSonarManagerListener {

    public:

        LIBAPI virtual ~HlaActiveSonarManagerListener() {}

        /**
        * This method is called when a new HlaActiveSonar instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param activeSonar the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaActiveSonarDiscovered(HlaActiveSonarPtr activeSonar, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaActiveSonar instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param activeSonar the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaActiveSonarInitialized(HlaActiveSonarPtr activeSonar, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaActiveSonarManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param activeSonar the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaActiveSonarInInterest(HlaActiveSonarPtr activeSonar, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaActiveSonarManagerListener instance goes out of interest.
        *
        * @param activeSonar the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaActiveSonarOutOfInterest(HlaActiveSonarPtr activeSonar, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaActiveSonar instance is deleted.
        *
        * @param activeSonar the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaActiveSonarDeleted(HlaActiveSonarPtr activeSonar, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaActiveSonarManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaActiveSonarManagerListener::Adapter : public HlaActiveSonarManagerListener {

    public:
        LIBAPI virtual void hlaActiveSonarDiscovered(HlaActiveSonarPtr activeSonar, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaActiveSonarInitialized(HlaActiveSonarPtr activeSonar, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaActiveSonarInInterest(HlaActiveSonarPtr activeSonar, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaActiveSonarOutOfInterest(HlaActiveSonarPtr activeSonar, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaActiveSonarDeleted(HlaActiveSonarPtr activeSonar, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
