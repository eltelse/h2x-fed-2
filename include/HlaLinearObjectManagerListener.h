/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLALINEAROBJECTMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLALINEAROBJECTMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaLinearObject.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaLinearObject instances.
    */
    class HlaLinearObjectManagerListener {

    public:

        LIBAPI virtual ~HlaLinearObjectManagerListener() {}

        /**
        * This method is called when a new HlaLinearObject instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param linearObject the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaLinearObjectDiscovered(HlaLinearObjectPtr linearObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaLinearObject instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param linearObject the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaLinearObjectInitialized(HlaLinearObjectPtr linearObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaLinearObjectManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param linearObject the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaLinearObjectInInterest(HlaLinearObjectPtr linearObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaLinearObjectManagerListener instance goes out of interest.
        *
        * @param linearObject the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaLinearObjectOutOfInterest(HlaLinearObjectPtr linearObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaLinearObject instance is deleted.
        *
        * @param linearObject the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaLinearObjectDeleted(HlaLinearObjectPtr linearObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaLinearObjectManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaLinearObjectManagerListener::Adapter : public HlaLinearObjectManagerListener {

    public:
        LIBAPI virtual void hlaLinearObjectDiscovered(HlaLinearObjectPtr linearObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaLinearObjectInitialized(HlaLinearObjectPtr linearObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaLinearObjectInInterest(HlaLinearObjectPtr linearObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaLinearObjectOutOfInterest(HlaLinearObjectPtr linearObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaLinearObjectDeleted(HlaLinearObjectPtr linearObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
