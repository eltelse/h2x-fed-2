/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLASOVIETIFFLISTENER_H
#define DEVELOPER_STUDIO_HLASOVIETIFFLISTENER_H

#include <set>


#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaSovietIFFAttributes.h>

namespace DevStudio {
   /**
   * Listener for updates of attributes.  
   */
   class HlaSovietIFFListener {
   public:

      LIBAPI virtual ~HlaSovietIFFListener() {}

      /**
      * This method is called when a HLA <code>reflectAttributeValueUpdate</code> is received for an remote object,
      * or a set of attributes is updated on a local object.
      *
      * @param sovietIFF The sovietIFF which this update corresponds to.
      * @param attributes The set of attributes that are updated.
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time the update was initiated.
      */
      LIBAPI virtual void attributesUpdated(HlaSovietIFFPtr sovietIFF, std::set<HlaSovietIFFAttributes::Attribute> &attributes, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

      class Adapter;
   };

  /**
  * An adapter class that implements the HlaSovietIFFListener interface with empty methods.
  * It might be used as a base class for a listener.
  */
  class HlaSovietIFFListener::Adapter : public HlaSovietIFFListener {

  public:

     LIBAPI virtual void attributesUpdated(HlaSovietIFFPtr sovietIFF, std::set<HlaSovietIFFAttributes::Attribute> &attributes, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
    };

}
#endif
