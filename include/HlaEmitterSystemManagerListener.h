/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAEMITTERSYSTEMMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLAEMITTERSYSTEMMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaEmitterSystem.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaEmitterSystem instances.
    */
    class HlaEmitterSystemManagerListener {

    public:

        LIBAPI virtual ~HlaEmitterSystemManagerListener() {}

        /**
        * This method is called when a new HlaEmitterSystem instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param emitterSystem the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaEmitterSystemDiscovered(HlaEmitterSystemPtr emitterSystem, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaEmitterSystem instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param emitterSystem the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaEmitterSystemInitialized(HlaEmitterSystemPtr emitterSystem, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaEmitterSystemManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param emitterSystem the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaEmitterSystemInInterest(HlaEmitterSystemPtr emitterSystem, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaEmitterSystemManagerListener instance goes out of interest.
        *
        * @param emitterSystem the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaEmitterSystemOutOfInterest(HlaEmitterSystemPtr emitterSystem, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaEmitterSystem instance is deleted.
        *
        * @param emitterSystem the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaEmitterSystemDeleted(HlaEmitterSystemPtr emitterSystem, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaEmitterSystemManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaEmitterSystemManagerListener::Adapter : public HlaEmitterSystemManagerListener {

    public:
        LIBAPI virtual void hlaEmitterSystemDiscovered(HlaEmitterSystemPtr emitterSystem, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaEmitterSystemInitialized(HlaEmitterSystemPtr emitterSystem, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaEmitterSystemInInterest(HlaEmitterSystemPtr emitterSystem, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaEmitterSystemOutOfInterest(HlaEmitterSystemPtr emitterSystem, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaEmitterSystemDeleted(HlaEmitterSystemPtr emitterSystem, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
