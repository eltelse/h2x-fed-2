/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLARADIOTRANSMITTERVALUELISTENER_H
#define DEVELOPER_STUDIO_HLARADIOTRANSMITTERVALUELISTENER_H

#include <memory>

#include <DevStudio/datatypes/AntennaPatternVariantStruct.h>
#include <DevStudio/datatypes/AntennaPatternVariantStructLengthlessArray.h>
#include <DevStudio/datatypes/CryptographicModeEnum.h>
#include <DevStudio/datatypes/CryptographicSystemTypeEnum.h>
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/RFModulationSystemTypeEnum.h>
#include <DevStudio/datatypes/RFModulationTypeVariantStruct.h>
#include <DevStudio/datatypes/RadioInputSourceEnum.h>
#include <DevStudio/datatypes/RadioTypeStruct.h>
#include <DevStudio/datatypes/RelativePositionStruct.h>
#include <DevStudio/datatypes/SpreadSpectrumVariantStruct.h>
#include <DevStudio/datatypes/TransmitterOperationalStatusEnum.h>
#include <DevStudio/datatypes/WorldLocationStruct.h>
#include <string>
#include <vector>

#include <DevStudio/HlaLogicalTime.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaRadioTransmitterAttributes.h>    

namespace DevStudio {

   /**
   * Listener for updates of attributes, with the new updated values.  
   */
   class HlaRadioTransmitterValueListener {

   public:

      LIBAPI virtual ~HlaRadioTransmitterValueListener() {}
    
      /**
      * This method is called when the attribute <code>antennaPatternData</code> is updated.
      * <br>Description from the FOM: <i>The radiation pattern of the radio's antenna.</i>
      * <br>Description of the data type from the FOM: <i>Represents an antenna's radiation pattern, its orientation in space, and the polarization of the radiation.</i>
      *
      * @param radioTransmitter The object which is updated.
      * @param antennaPatternData The new value of the attribute in this update
      * @param validOldAntennaPatternData True if oldAntennaPatternData contains a valid value
      * @param oldAntennaPatternData The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void antennaPatternDataUpdated(HlaRadioTransmitterPtr radioTransmitter, std::vector<DevStudio::AntennaPatternVariantStruct > antennaPatternData, bool validOldAntennaPatternData, std::vector<DevStudio::AntennaPatternVariantStruct > oldAntennaPatternData, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>cryptographicMode</code> is updated.
      * <br>Description from the FOM: <i>The mode of the cryptographic system.</i>
      * <br>Description of the data type from the FOM: <i>Represents the encryption mode of a cryptographic system.</i>
      *
      * @param radioTransmitter The object which is updated.
      * @param cryptographicMode The new value of the attribute in this update
      * @param validOldCryptographicMode True if oldCryptographicMode contains a valid value
      * @param oldCryptographicMode The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void cryptographicModeUpdated(HlaRadioTransmitterPtr radioTransmitter, CryptographicModeEnum::CryptographicModeEnum cryptographicMode, bool validOldCryptographicMode, CryptographicModeEnum::CryptographicModeEnum oldCryptographicMode, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>cryptoSystem</code> is updated.
      * <br>Description from the FOM: <i>The type of cryptographic equipment in use.</i>
      * <br>Description of the data type from the FOM: <i>Identifies the type of cryptographic equipment</i>
      *
      * @param radioTransmitter The object which is updated.
      * @param cryptoSystem The new value of the attribute in this update
      * @param validOldCryptoSystem True if oldCryptoSystem contains a valid value
      * @param oldCryptoSystem The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void cryptoSystemUpdated(HlaRadioTransmitterPtr radioTransmitter, CryptographicSystemTypeEnum::CryptographicSystemTypeEnum cryptoSystem, bool validOldCryptoSystem, CryptographicSystemTypeEnum::CryptographicSystemTypeEnum oldCryptoSystem, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>encryptionKeyIdentifier</code> is updated.
      * <br>Description from the FOM: <i>The identification of the key used to encrypt the radio signals being transmitted. The transmitter and receiver should be considered to be using the same key if these numbers match.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param radioTransmitter The object which is updated.
      * @param encryptionKeyIdentifier The new value of the attribute in this update
      * @param validOldEncryptionKeyIdentifier True if oldEncryptionKeyIdentifier contains a valid value
      * @param oldEncryptionKeyIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void encryptionKeyIdentifierUpdated(HlaRadioTransmitterPtr radioTransmitter, unsigned short encryptionKeyIdentifier, bool validOldEncryptionKeyIdentifier, unsigned short oldEncryptionKeyIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>frequency</code> is updated.
      * <br>Description from the FOM: <i>The center frequency of the radio transmissions.</i>
      * <br>Description of the data type from the FOM: <i>Frequency of a radio transmission, in hertz. [unit: hertz (Hz), resolution: NA, accuracy: NA]</i>
      *
      * @param radioTransmitter The object which is updated.
      * @param frequency The new value of the attribute in this update
      * @param validOldFrequency True if oldFrequency contains a valid value
      * @param oldFrequency The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void frequencyUpdated(HlaRadioTransmitterPtr radioTransmitter, unsigned long long frequency, bool validOldFrequency, unsigned long long oldFrequency, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>frequencyBandwidth</code> is updated.
      * <br>Description from the FOM: <i>The bandpass of the radio transmissions.</i>
      * <br>Description of the data type from the FOM: <i>Frequency, based on SI derived unit hertz, unit symbol Hz. [unit: hertz (Hz), resolution: NA, accuracy: NA]</i>
      *
      * @param radioTransmitter The object which is updated.
      * @param frequencyBandwidth The new value of the attribute in this update
      * @param validOldFrequencyBandwidth True if oldFrequencyBandwidth contains a valid value
      * @param oldFrequencyBandwidth The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void frequencyBandwidthUpdated(HlaRadioTransmitterPtr radioTransmitter, float frequencyBandwidth, bool validOldFrequencyBandwidth, float oldFrequencyBandwidth, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>radioIndex</code> is updated.
      * <br>Description from the FOM: <i>A number that uniquely identifies this radio from others on the host.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param radioTransmitter The object which is updated.
      * @param radioIndex The new value of the attribute in this update
      * @param validOldRadioIndex True if oldRadioIndex contains a valid value
      * @param oldRadioIndex The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void radioIndexUpdated(HlaRadioTransmitterPtr radioTransmitter, unsigned short radioIndex, bool validOldRadioIndex, unsigned short oldRadioIndex, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>radioInputSource</code> is updated.
      * <br>Description from the FOM: <i>Specifies which position or data port provided the input for the transmission.</i>
      * <br>Description of the data type from the FOM: <i>Radio input source</i>
      *
      * @param radioTransmitter The object which is updated.
      * @param radioInputSource The new value of the attribute in this update
      * @param validOldRadioInputSource True if oldRadioInputSource contains a valid value
      * @param oldRadioInputSource The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void radioInputSourceUpdated(HlaRadioTransmitterPtr radioTransmitter, RadioInputSourceEnum::RadioInputSourceEnum radioInputSource, bool validOldRadioInputSource, RadioInputSourceEnum::RadioInputSourceEnum oldRadioInputSource, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>radioSystemType</code> is updated.
      * <br>Description from the FOM: <i>The type of radio transmitter.</i>
      * <br>Description of the data type from the FOM: <i>Specifies the type of a radio.</i>
      *
      * @param radioTransmitter The object which is updated.
      * @param radioSystemType The new value of the attribute in this update
      * @param validOldRadioSystemType True if oldRadioSystemType contains a valid value
      * @param oldRadioSystemType The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void radioSystemTypeUpdated(HlaRadioTransmitterPtr radioTransmitter, RadioTypeStruct radioSystemType, bool validOldRadioSystemType, RadioTypeStruct oldRadioSystemType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>rFModulationSystemType</code> is updated.
      * <br>Description from the FOM: <i>The radio system type associated with this transmitter.</i>
      * <br>Description of the data type from the FOM: <i>The radio system type associated with this transmitter.</i>
      *
      * @param radioTransmitter The object which is updated.
      * @param rFModulationSystemType The new value of the attribute in this update
      * @param validOldRFModulationSystemType True if oldRFModulationSystemType contains a valid value
      * @param oldRFModulationSystemType The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void rFModulationSystemTypeUpdated(HlaRadioTransmitterPtr radioTransmitter, RFModulationSystemTypeEnum::RFModulationSystemTypeEnum rFModulationSystemType, bool validOldRFModulationSystemType, RFModulationSystemTypeEnum::RFModulationSystemTypeEnum oldRFModulationSystemType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>rFModulationType</code> is updated.
      * <br>Description from the FOM: <i>Classification of the modulation type.</i>
      * <br>Description of the data type from the FOM: <i>Specifies the major modulation type as well as certain detailed information specific to the type.</i>
      *
      * @param radioTransmitter The object which is updated.
      * @param rFModulationType The new value of the attribute in this update
      * @param validOldRFModulationType True if oldRFModulationType contains a valid value
      * @param oldRFModulationType The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void rFModulationTypeUpdated(HlaRadioTransmitterPtr radioTransmitter, RFModulationTypeVariantStruct rFModulationType, bool validOldRFModulationType, RFModulationTypeVariantStruct oldRFModulationType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>spreadSpectrum</code> is updated.
      * <br>Description from the FOM: <i>Describes the spread spectrum characteristics of the transmission, such as frequency hopping or other spread spectrum transmission modes.</i>
      * <br>Description of the data type from the FOM: <i>Identifies the actual spread spectrum technique in use.</i>
      *
      * @param radioTransmitter The object which is updated.
      * @param spreadSpectrum The new value of the attribute in this update
      * @param validOldSpreadSpectrum True if oldSpreadSpectrum contains a valid value
      * @param oldSpreadSpectrum The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void spreadSpectrumUpdated(HlaRadioTransmitterPtr radioTransmitter, SpreadSpectrumVariantStruct spreadSpectrum, bool validOldSpreadSpectrum, SpreadSpectrumVariantStruct oldSpreadSpectrum, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>streamTag</code> is updated.
      * <br>Description from the FOM: <i>A globally unique identifier for the associated audio stream</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^64-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param radioTransmitter The object which is updated.
      * @param streamTag The new value of the attribute in this update
      * @param validOldStreamTag True if oldStreamTag contains a valid value
      * @param oldStreamTag The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void streamTagUpdated(HlaRadioTransmitterPtr radioTransmitter, unsigned long long streamTag, bool validOldStreamTag, unsigned long long oldStreamTag, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>timeHopInUse</code> is updated.
      * <br>Description from the FOM: <i>Whether the radio is using time hopping or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param radioTransmitter The object which is updated.
      * @param timeHopInUse The new value of the attribute in this update
      * @param validOldTimeHopInUse True if oldTimeHopInUse contains a valid value
      * @param oldTimeHopInUse The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void timeHopInUseUpdated(HlaRadioTransmitterPtr radioTransmitter, bool timeHopInUse, bool validOldTimeHopInUse, bool oldTimeHopInUse, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>transmittedPower</code> is updated.
      * <br>Description from the FOM: <i>The average transmitted power.</i>
      * <br>Description of the data type from the FOM: <i>Power ratio in decibels (dB) of a measured power referenced to 1 milliwatt (mW). [unit: decibel milliwatt (dBm), resolution: NA, accuracy: perfect]</i>
      *
      * @param radioTransmitter The object which is updated.
      * @param transmittedPower The new value of the attribute in this update
      * @param validOldTransmittedPower True if oldTransmittedPower contains a valid value
      * @param oldTransmittedPower The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void transmittedPowerUpdated(HlaRadioTransmitterPtr radioTransmitter, float transmittedPower, bool validOldTransmittedPower, float oldTransmittedPower, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>transmitterOperationalStatus</code> is updated.
      * <br>Description from the FOM: <i>The current operational state of the radio transmitter.</i>
      * <br>Description of the data type from the FOM: <i>The current operational state of a radio transmitter.</i>
      *
      * @param radioTransmitter The object which is updated.
      * @param transmitterOperationalStatus The new value of the attribute in this update
      * @param validOldTransmitterOperationalStatus True if oldTransmitterOperationalStatus contains a valid value
      * @param oldTransmitterOperationalStatus The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void transmitterOperationalStatusUpdated(HlaRadioTransmitterPtr radioTransmitter, TransmitterOperationalStatusEnum::TransmitterOperationalStatusEnum transmitterOperationalStatus, bool validOldTransmitterOperationalStatus, TransmitterOperationalStatusEnum::TransmitterOperationalStatusEnum oldTransmitterOperationalStatus, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>worldLocation</code> is updated.
      * <br>Description from the FOM: <i>The location of the antenna in the world coordinate system.</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @param radioTransmitter The object which is updated.
      * @param worldLocation The new value of the attribute in this update
      * @param validOldWorldLocation True if oldWorldLocation contains a valid value
      * @param oldWorldLocation The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void worldLocationUpdated(HlaRadioTransmitterPtr radioTransmitter, WorldLocationStruct worldLocation, bool validOldWorldLocation, WorldLocationStruct oldWorldLocation, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>entityIdentifier</code> is updated.
      * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @param radioTransmitter The object which is updated.
      * @param entityIdentifier The new value of the attribute in this update
      * @param validOldEntityIdentifier True if oldEntityIdentifier contains a valid value
      * @param oldEntityIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void entityIdentifierUpdated(HlaRadioTransmitterPtr radioTransmitter, EntityIdentifierStruct entityIdentifier, bool validOldEntityIdentifier, EntityIdentifierStruct oldEntityIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>hostObjectIdentifier</code> is updated.
      * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @param radioTransmitter The object which is updated.
      * @param hostObjectIdentifier The new value of the attribute in this update
      * @param validOldHostObjectIdentifier True if oldHostObjectIdentifier contains a valid value
      * @param oldHostObjectIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void hostObjectIdentifierUpdated(HlaRadioTransmitterPtr radioTransmitter, std::string hostObjectIdentifier, bool validOldHostObjectIdentifier, std::string oldHostObjectIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>relativePosition</code> is updated.
      * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
      * <br>Description of the data type from the FOM: <i>Relative position in right-handed Cartesian coordinates.</i>
      *
      * @param radioTransmitter The object which is updated.
      * @param relativePosition The new value of the attribute in this update
      * @param validOldRelativePosition True if oldRelativePosition contains a valid value
      * @param oldRelativePosition The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void relativePositionUpdated(HlaRadioTransmitterPtr radioTransmitter, RelativePositionStruct relativePosition, bool validOldRelativePosition, RelativePositionStruct oldRelativePosition, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
      /**
      * This method is called when the producing federate of an attribute is changed.
      * Only available when using HLA Evolved.
      *
      * @param radioTransmitter The object which is updated.
      * @param attribute The attribute that now has a new producing federate
      * @param oldProducingFederate The federate handle of the old producing federate
      * @param newProducingFederate The federate handle of the new producing federate
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void producingFederateUpdated(HlaRadioTransmitterPtr radioTransmitter, HlaRadioTransmitterAttributes::Attribute attribute, HlaFederateIdPtr oldProducingFederate, HlaFederateIdPtr newProducingFederate, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;

      class Adapter;
   };

   /**
   * An adapter class that implements the HlaRadioTransmitterValueListener interface with empty methods.
   * It might be used as a base class for a listener.
   */
   class HlaRadioTransmitterValueListener::Adapter : public HlaRadioTransmitterValueListener {

   public:

      LIBAPI virtual void antennaPatternDataUpdated(HlaRadioTransmitterPtr radioTransmitter, std::vector<DevStudio::AntennaPatternVariantStruct > antennaPatternData, bool validOldAntennaPatternData, std::vector<DevStudio::AntennaPatternVariantStruct > oldAntennaPatternData, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void cryptographicModeUpdated(HlaRadioTransmitterPtr radioTransmitter, CryptographicModeEnum::CryptographicModeEnum cryptographicMode, bool validOldCryptographicMode, CryptographicModeEnum::CryptographicModeEnum oldCryptographicMode, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void cryptoSystemUpdated(HlaRadioTransmitterPtr radioTransmitter, CryptographicSystemTypeEnum::CryptographicSystemTypeEnum cryptoSystem, bool validOldCryptoSystem, CryptographicSystemTypeEnum::CryptographicSystemTypeEnum oldCryptoSystem, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void encryptionKeyIdentifierUpdated(HlaRadioTransmitterPtr radioTransmitter, unsigned short encryptionKeyIdentifier, bool validOldEncryptionKeyIdentifier, unsigned short oldEncryptionKeyIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void frequencyUpdated(HlaRadioTransmitterPtr radioTransmitter, unsigned long long frequency, bool validOldFrequency, unsigned long long oldFrequency, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void frequencyBandwidthUpdated(HlaRadioTransmitterPtr radioTransmitter, float frequencyBandwidth, bool validOldFrequencyBandwidth, float oldFrequencyBandwidth, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void radioIndexUpdated(HlaRadioTransmitterPtr radioTransmitter, unsigned short radioIndex, bool validOldRadioIndex, unsigned short oldRadioIndex, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void radioInputSourceUpdated(HlaRadioTransmitterPtr radioTransmitter, RadioInputSourceEnum::RadioInputSourceEnum radioInputSource, bool validOldRadioInputSource, RadioInputSourceEnum::RadioInputSourceEnum oldRadioInputSource, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void radioSystemTypeUpdated(HlaRadioTransmitterPtr radioTransmitter, RadioTypeStruct radioSystemType, bool validOldRadioSystemType, RadioTypeStruct oldRadioSystemType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void rFModulationSystemTypeUpdated(HlaRadioTransmitterPtr radioTransmitter, RFModulationSystemTypeEnum::RFModulationSystemTypeEnum rFModulationSystemType, bool validOldRFModulationSystemType, RFModulationSystemTypeEnum::RFModulationSystemTypeEnum oldRFModulationSystemType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void rFModulationTypeUpdated(HlaRadioTransmitterPtr radioTransmitter, RFModulationTypeVariantStruct rFModulationType, bool validOldRFModulationType, RFModulationTypeVariantStruct oldRFModulationType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void spreadSpectrumUpdated(HlaRadioTransmitterPtr radioTransmitter, SpreadSpectrumVariantStruct spreadSpectrum, bool validOldSpreadSpectrum, SpreadSpectrumVariantStruct oldSpreadSpectrum, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void streamTagUpdated(HlaRadioTransmitterPtr radioTransmitter, unsigned long long streamTag, bool validOldStreamTag, unsigned long long oldStreamTag, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void timeHopInUseUpdated(HlaRadioTransmitterPtr radioTransmitter, bool timeHopInUse, bool validOldTimeHopInUse, bool oldTimeHopInUse, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void transmittedPowerUpdated(HlaRadioTransmitterPtr radioTransmitter, float transmittedPower, bool validOldTransmittedPower, float oldTransmittedPower, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void transmitterOperationalStatusUpdated(HlaRadioTransmitterPtr radioTransmitter, TransmitterOperationalStatusEnum::TransmitterOperationalStatusEnum transmitterOperationalStatus, bool validOldTransmitterOperationalStatus, TransmitterOperationalStatusEnum::TransmitterOperationalStatusEnum oldTransmitterOperationalStatus, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void worldLocationUpdated(HlaRadioTransmitterPtr radioTransmitter, WorldLocationStruct worldLocation, bool validOldWorldLocation, WorldLocationStruct oldWorldLocation, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void entityIdentifierUpdated(HlaRadioTransmitterPtr radioTransmitter, EntityIdentifierStruct entityIdentifier, bool validOldEntityIdentifier, EntityIdentifierStruct oldEntityIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void hostObjectIdentifierUpdated(HlaRadioTransmitterPtr radioTransmitter, std::string hostObjectIdentifier, bool validOldHostObjectIdentifier, std::string oldHostObjectIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void relativePositionUpdated(HlaRadioTransmitterPtr radioTransmitter, RelativePositionStruct relativePosition, bool validOldRelativePosition, RelativePositionStruct oldRelativePosition, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
      LIBAPI virtual void producingFederateUpdated(HlaRadioTransmitterPtr radioTransmitter, HlaRadioTransmitterAttributes::Attribute attribute, HlaFederateIdPtr oldProducingFederate, HlaFederateIdPtr newProducingFederate, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
   };

}
#endif
