/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLANATOIFFATTRIBUTES_H
#define DEVELOPER_STUDIO_HLANATOIFFATTRIBUTES_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <string>
#include <vector>
#include <utility>
    
#include <boost/noncopyable.hpp>
    
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EventIdentifierStruct.h>
#include <DevStudio/datatypes/FundamentalParameterDataStruct.h>
#include <DevStudio/datatypes/FundamentalParameterDataStructLengthlessArray.h>
#include <DevStudio/datatypes/IffOperationalParameter1Enum.h>
#include <DevStudio/datatypes/IffOperationalParameter2Enum.h>
#include <DevStudio/datatypes/IffSystemModeEnum.h>
#include <DevStudio/datatypes/IffSystemNameEnum.h>
#include <DevStudio/datatypes/IffSystemTypeEnum.h>
#include <DevStudio/datatypes/RelativePositionStruct.h>
#include <string>
#include <vector>

#include <DevStudio/HlaException.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaIFFAttributes.h>
#include <DevStudio/HlaTimeStamped.h>

namespace DevStudio {

   /**
   * Interface used to represent all attributes for an object instance.
   */
   class HlaNatoIFFAttributes : public HlaIFFAttributes {
   public:


     /**
      * An enumeration of the attributes of an HlaNatoIFF
      *
      *<table summary="All attributes">
      * <tr><td><b>Enum constant</b></td><td><b>C++ name</b></td><td><b>FOM name</b></td></tr>
      * <tr><td>BEAM_AZIMUTH_CENTER</td><td>beamAzimuthCenter</td><td><code>BeamAzimuthCenter</code></td></tr>
      * <tr><td>BEAM_AZIMUTH_SWEEP</td><td>beamAzimuthSweep</td><td><code>BeamAzimuthSweep</code></td></tr>
      * <tr><td>BEAM_ELEVATION_CENTER</td><td>beamElevationCenter</td><td><code>BeamElevationCenter</code></td></tr>
      * <tr><td>BEAM_ELEVATION_SWEEP</td><td>beamElevationSweep</td><td><code>BeamElevationSweep</code></td></tr>
      * <tr><td>BEAM_SWEEP_SYNC</td><td>beamSweepSync</td><td><code>BeamSweepSync</code></td></tr>
      * <tr><td>EVENT_IDENTIFIER</td><td>eventIdentifier</td><td><code>EventIdentifier</code></td></tr>
      * <tr><td>FUNDAMENTAL_PARAMETER_DATA</td><td>fundamentalParameterData</td><td><code>FundamentalParameterData</code></td></tr>
      * <tr><td>LAYER2DATA_AVAILABLE</td><td>layer2DataAvailable</td><td><code>Layer2DataAvailable</code></td></tr>
      * <tr><td>SECONDARY_OPERATIONAL_DATA_PARAMETER1</td><td>secondaryOperationalDataParameter1</td><td><code>SecondaryOperationalDataParameter1</code></td></tr>
      * <tr><td>SECONDARY_OPERATIONAL_DATA_PARAMETER2</td><td>secondaryOperationalDataParameter2</td><td><code>SecondaryOperationalDataParameter2</code></td></tr>
      * <tr><td>SYSTEM_MODE</td><td>systemMode</td><td><code>SystemMode</code></td></tr>
      * <tr><td>SYSTEM_NAME</td><td>systemName</td><td><code>SystemName</code></td></tr>
      * <tr><td>SYSTEM_TYPE</td><td>systemType</td><td><code>SystemType</code></td></tr>
      * <tr><td>SYSTEM_IS_ON</td><td>systemIsOn</td><td><code>SystemIsOn</code></td></tr>
      * <tr><td>SYSTEM_IS_OPERATIONAL</td><td>systemIsOperational</td><td><code>SystemIsOperational</code></td></tr>
      * <tr><td>ENTITY_IDENTIFIER</td><td>entityIdentifier</td><td><code>EntityIdentifier</code></td></tr>
      * <tr><td>HOST_OBJECT_IDENTIFIER</td><td>hostObjectIdentifier</td><td><code>HostObjectIdentifier</code></td></tr>
      * <tr><td>RELATIVE_POSITION</td><td>relativePosition</td><td><code>RelativePosition</code></td></tr>
      *</table>
      */
      enum Attribute {
        /**
        * beamAzimuthCenter (FOM name: <code>BeamAzimuthCenter</code>).
        * <br>Description from the FOM: <i>The azimuth center of the IFF beam's scan volume relative to the IFF system.</i>
        */
         BEAM_AZIMUTH_CENTER,

        /**
        * beamAzimuthSweep (FOM name: <code>BeamAzimuthSweep</code>).
        * <br>Description from the FOM: <i>The azimuth half-angle of the IFF beam's scan volume relative to the IFF system.</i>
        */
         BEAM_AZIMUTH_SWEEP,

        /**
        * beamElevationCenter (FOM name: <code>BeamElevationCenter</code>).
        * <br>Description from the FOM: <i>The elevation center of the IFF beam's scan volume relative to the IFF system.</i>
        */
         BEAM_ELEVATION_CENTER,

        /**
        * beamElevationSweep (FOM name: <code>BeamElevationSweep</code>).
        * <br>Description from the FOM: <i>The elevation half-angle of the IFF beam's scan volume relative to the IFF system.</i>
        */
         BEAM_ELEVATION_SWEEP,

        /**
        * beamSweepSync (FOM name: <code>BeamSweepSync</code>).
        * <br>Description from the FOM: <i>The percentage of time a scan is through its pattern from its origin.</i>
        */
         BEAM_SWEEP_SYNC,

        /**
        * eventIdentifier (FOM name: <code>EventIdentifier</code>).
        * <br>Description from the FOM: <i>Used to associate related events.</i>
        */
         EVENT_IDENTIFIER,

        /**
        * fundamentalParameterData (FOM name: <code>FundamentalParameterData</code>).
        * <br>Description from the FOM: <i>The fundamental energy radiation characteristics of the IFF/ATC/NAVAIDS system.</i>
        */
         FUNDAMENTAL_PARAMETER_DATA,

        /**
        * layer2DataAvailable (FOM name: <code>Layer2DataAvailable</code>).
        * <br>Description from the FOM: <i>Specifies if level 2 data is available for this IFF system. If level 2 data is available then the BeamAzimuthCenter, BeamAzimuthSweep, BeamElevationCenter, BeamElevationSweep, BeamSweepSync, FundamentalParameterData, SecondaryOperationalDataParameter1, and SecondaryOperationalDataParameter2 attributes shall be generated.</i>
        */
         LAYER2DATA_AVAILABLE,

        /**
        * secondaryOperationalDataParameter1 (FOM name: <code>SecondaryOperationalDataParameter1</code>).
        * <br>Description from the FOM: <i>Additional characteristics of the IFF/ATC/NAVAIDS emitting system.</i>
        */
         SECONDARY_OPERATIONAL_DATA_PARAMETER1,

        /**
        * secondaryOperationalDataParameter2 (FOM name: <code>SecondaryOperationalDataParameter2</code>).
        * <br>Description from the FOM: <i>Additional characteristics of the IFF/ATC/NAVAIDS emitting system.</i>
        */
         SECONDARY_OPERATIONAL_DATA_PARAMETER2,

        /**
        * systemMode (FOM name: <code>SystemMode</code>).
        * <br>Description from the FOM: <i>Mode of operation.</i>
        */
         SYSTEM_MODE,

        /**
        * systemName (FOM name: <code>SystemName</code>).
        * <br>Description from the FOM: <i>Particular named type of the IFF system in use.</i>
        */
         SYSTEM_NAME,

        /**
        * systemType (FOM name: <code>SystemType</code>).
        * <br>Description from the FOM: <i>General type of IFF system in use.</i>
        */
         SYSTEM_TYPE,

        /**
        * systemIsOn (FOM name: <code>SystemIsOn</code>).
        * <br>Description from the FOM: <i>Whether or not the system is on.</i>
        */
         SYSTEM_IS_ON,

        /**
        * systemIsOperational (FOM name: <code>SystemIsOperational</code>).
        * <br>Description from the FOM: <i>Whether or not the system is operational.</i>
        */
         SYSTEM_IS_OPERATIONAL,

        /**
        * entityIdentifier (FOM name: <code>EntityIdentifier</code>).
        * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
        */
         ENTITY_IDENTIFIER,

        /**
        * hostObjectIdentifier (FOM name: <code>HostObjectIdentifier</code>).
        * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
        */
         HOST_OBJECT_IDENTIFIER,

        /**
        * relativePosition (FOM name: <code>RelativePosition</code>).
        * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
        */
         RELATIVE_POSITION
      };

     /**
      * Gets the name of the attribute.
      *
      * @return The name of the attribute. An empty string will be returned if the attribute does not exist.
      */
      LIBAPI virtual std::wstring getName(Attribute attribute);

     /**
      * Finds the attribute specified in the parameter attributeName.
      * The found enumeration will be stored in the parameter attribute.
      *
      * @return true if the attribute was found, false otherwise.
      */
      LIBAPI virtual bool find(Attribute& attribute, std::wstring attributeName);

      LIBAPI virtual ~HlaNatoIFFAttributes() {}
    
      /**
      * Returns true if the <code>beamAzimuthCenter</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The azimuth center of the IFF beam's scan volume relative to the IFF system.</i>
      *
      * @return true if <code>beamAzimuthCenter</code> is available.
      */
      LIBAPI virtual bool hasBeamAzimuthCenter() = 0;

      /**
      * Gets the value of the <code>beamAzimuthCenter</code> attribute.
      *
      * <br>Description from the FOM: <i>The azimuth center of the IFF beam's scan volume relative to the IFF system.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @return the <code>beamAzimuthCenter</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual float getBeamAzimuthCenter()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>beamAzimuthCenter</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The azimuth center of the IFF beam's scan volume relative to the IFF system.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>beamAzimuthCenter</code> attribute.
      */
      LIBAPI virtual float getBeamAzimuthCenter(float defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>beamAzimuthCenter</code> attribute.
      * <br>Description from the FOM: <i>The azimuth center of the IFF beam's scan volume relative to the IFF system.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @return the time stamped <code>beamAzimuthCenter</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< float > getBeamAzimuthCenterTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>beamAzimuthSweep</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The azimuth half-angle of the IFF beam's scan volume relative to the IFF system.</i>
      *
      * @return true if <code>beamAzimuthSweep</code> is available.
      */
      LIBAPI virtual bool hasBeamAzimuthSweep() = 0;

      /**
      * Gets the value of the <code>beamAzimuthSweep</code> attribute.
      *
      * <br>Description from the FOM: <i>The azimuth half-angle of the IFF beam's scan volume relative to the IFF system.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @return the <code>beamAzimuthSweep</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual float getBeamAzimuthSweep()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>beamAzimuthSweep</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The azimuth half-angle of the IFF beam's scan volume relative to the IFF system.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>beamAzimuthSweep</code> attribute.
      */
      LIBAPI virtual float getBeamAzimuthSweep(float defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>beamAzimuthSweep</code> attribute.
      * <br>Description from the FOM: <i>The azimuth half-angle of the IFF beam's scan volume relative to the IFF system.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @return the time stamped <code>beamAzimuthSweep</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< float > getBeamAzimuthSweepTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>beamElevationCenter</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The elevation center of the IFF beam's scan volume relative to the IFF system.</i>
      *
      * @return true if <code>beamElevationCenter</code> is available.
      */
      LIBAPI virtual bool hasBeamElevationCenter() = 0;

      /**
      * Gets the value of the <code>beamElevationCenter</code> attribute.
      *
      * <br>Description from the FOM: <i>The elevation center of the IFF beam's scan volume relative to the IFF system.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @return the <code>beamElevationCenter</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual float getBeamElevationCenter()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>beamElevationCenter</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The elevation center of the IFF beam's scan volume relative to the IFF system.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>beamElevationCenter</code> attribute.
      */
      LIBAPI virtual float getBeamElevationCenter(float defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>beamElevationCenter</code> attribute.
      * <br>Description from the FOM: <i>The elevation center of the IFF beam's scan volume relative to the IFF system.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @return the time stamped <code>beamElevationCenter</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< float > getBeamElevationCenterTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>beamElevationSweep</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The elevation half-angle of the IFF beam's scan volume relative to the IFF system.</i>
      *
      * @return true if <code>beamElevationSweep</code> is available.
      */
      LIBAPI virtual bool hasBeamElevationSweep() = 0;

      /**
      * Gets the value of the <code>beamElevationSweep</code> attribute.
      *
      * <br>Description from the FOM: <i>The elevation half-angle of the IFF beam's scan volume relative to the IFF system.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @return the <code>beamElevationSweep</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual float getBeamElevationSweep()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>beamElevationSweep</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The elevation half-angle of the IFF beam's scan volume relative to the IFF system.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>beamElevationSweep</code> attribute.
      */
      LIBAPI virtual float getBeamElevationSweep(float defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>beamElevationSweep</code> attribute.
      * <br>Description from the FOM: <i>The elevation half-angle of the IFF beam's scan volume relative to the IFF system.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @return the time stamped <code>beamElevationSweep</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< float > getBeamElevationSweepTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>beamSweepSync</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The percentage of time a scan is through its pattern from its origin.</i>
      *
      * @return true if <code>beamSweepSync</code> is available.
      */
      LIBAPI virtual bool hasBeamSweepSync() = 0;

      /**
      * Gets the value of the <code>beamSweepSync</code> attribute.
      *
      * <br>Description from the FOM: <i>The percentage of time a scan is through its pattern from its origin.</i>
      * <br>Description of the data type from the FOM: <i>Percentage [unit: percent (%), resolution: NA, accuracy: NA]</i>
      *
      * @return the <code>beamSweepSync</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual float getBeamSweepSync()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>beamSweepSync</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The percentage of time a scan is through its pattern from its origin.</i>
      * <br>Description of the data type from the FOM: <i>Percentage [unit: percent (%), resolution: NA, accuracy: NA]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>beamSweepSync</code> attribute.
      */
      LIBAPI virtual float getBeamSweepSync(float defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>beamSweepSync</code> attribute.
      * <br>Description from the FOM: <i>The percentage of time a scan is through its pattern from its origin.</i>
      * <br>Description of the data type from the FOM: <i>Percentage [unit: percent (%), resolution: NA, accuracy: NA]</i>
      *
      * @return the time stamped <code>beamSweepSync</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< float > getBeamSweepSyncTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>eventIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Used to associate related events.</i>
      *
      * @return true if <code>eventIdentifier</code> is available.
      */
      LIBAPI virtual bool hasEventIdentifier() = 0;

      /**
      * Gets the value of the <code>eventIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>Used to associate related events.</i>
      * <br>Description of the data type from the FOM: <i>Identification of an event. Based on the Event Identifier record as specified in IEEE 1278.1-1995 section 5.2.18.</i>
      *
      * @return the <code>eventIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::EventIdentifierStruct getEventIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>eventIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Used to associate related events.</i>
      * <br>Description of the data type from the FOM: <i>Identification of an event. Based on the Event Identifier record as specified in IEEE 1278.1-1995 section 5.2.18.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>eventIdentifier</code> attribute.
      */
      LIBAPI virtual DevStudio::EventIdentifierStruct getEventIdentifier(DevStudio::EventIdentifierStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>eventIdentifier</code> attribute.
      * <br>Description from the FOM: <i>Used to associate related events.</i>
      * <br>Description of the data type from the FOM: <i>Identification of an event. Based on the Event Identifier record as specified in IEEE 1278.1-1995 section 5.2.18.</i>
      *
      * @return the time stamped <code>eventIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::EventIdentifierStruct > getEventIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>fundamentalParameterData</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The fundamental energy radiation characteristics of the IFF/ATC/NAVAIDS system.</i>
      *
      * @return true if <code>fundamentalParameterData</code> is available.
      */
      LIBAPI virtual bool hasFundamentalParameterData() = 0;

      /**
      * Gets the value of the <code>fundamentalParameterData</code> attribute.
      *
      * <br>Description from the FOM: <i>The fundamental energy radiation characteristics of the IFF/ATC/NAVAIDS system.</i>
      * <br>Description of the data type from the FOM: <i>Array of Fundamental Parameter Data records.</i>
      *
      * @return the <code>fundamentalParameterData</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::vector<DevStudio::FundamentalParameterDataStruct > getFundamentalParameterData()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>fundamentalParameterData</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The fundamental energy radiation characteristics of the IFF/ATC/NAVAIDS system.</i>
      * <br>Description of the data type from the FOM: <i>Array of Fundamental Parameter Data records.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>fundamentalParameterData</code> attribute.
      */
      LIBAPI virtual std::vector<DevStudio::FundamentalParameterDataStruct > getFundamentalParameterData(std::vector<DevStudio::FundamentalParameterDataStruct > defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>fundamentalParameterData</code> attribute.
      * <br>Description from the FOM: <i>The fundamental energy radiation characteristics of the IFF/ATC/NAVAIDS system.</i>
      * <br>Description of the data type from the FOM: <i>Array of Fundamental Parameter Data records.</i>
      *
      * @return the time stamped <code>fundamentalParameterData</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::vector<DevStudio::FundamentalParameterDataStruct > > getFundamentalParameterDataTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>layer2DataAvailable</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies if level 2 data is available for this IFF system. If level 2 data is available then the BeamAzimuthCenter, BeamAzimuthSweep, BeamElevationCenter, BeamElevationSweep, BeamSweepSync, FundamentalParameterData, SecondaryOperationalDataParameter1, and SecondaryOperationalDataParameter2 attributes shall be generated.</i>
      *
      * @return true if <code>layer2DataAvailable</code> is available.
      */
      LIBAPI virtual bool hasLayer2DataAvailable() = 0;

      /**
      * Gets the value of the <code>layer2DataAvailable</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies if level 2 data is available for this IFF system. If level 2 data is available then the BeamAzimuthCenter, BeamAzimuthSweep, BeamElevationCenter, BeamElevationSweep, BeamSweepSync, FundamentalParameterData, SecondaryOperationalDataParameter1, and SecondaryOperationalDataParameter2 attributes shall be generated.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>layer2DataAvailable</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual bool getLayer2DataAvailable()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>layer2DataAvailable</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies if level 2 data is available for this IFF system. If level 2 data is available then the BeamAzimuthCenter, BeamAzimuthSweep, BeamElevationCenter, BeamElevationSweep, BeamSweepSync, FundamentalParameterData, SecondaryOperationalDataParameter1, and SecondaryOperationalDataParameter2 attributes shall be generated.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>layer2DataAvailable</code> attribute.
      */
      LIBAPI virtual bool getLayer2DataAvailable(bool defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>layer2DataAvailable</code> attribute.
      * <br>Description from the FOM: <i>Specifies if level 2 data is available for this IFF system. If level 2 data is available then the BeamAzimuthCenter, BeamAzimuthSweep, BeamElevationCenter, BeamElevationSweep, BeamSweepSync, FundamentalParameterData, SecondaryOperationalDataParameter1, and SecondaryOperationalDataParameter2 attributes shall be generated.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>layer2DataAvailable</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< bool > getLayer2DataAvailableTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>secondaryOperationalDataParameter1</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Additional characteristics of the IFF/ATC/NAVAIDS emitting system.</i>
      *
      * @return true if <code>secondaryOperationalDataParameter1</code> is available.
      */
      LIBAPI virtual bool hasSecondaryOperationalDataParameter1() = 0;

      /**
      * Gets the value of the <code>secondaryOperationalDataParameter1</code> attribute.
      *
      * <br>Description from the FOM: <i>Additional characteristics of the IFF/ATC/NAVAIDS emitting system.</i>
      * <br>Description of the data type from the FOM: <i>IFF operational parameter 1</i>
      *
      * @return the <code>secondaryOperationalDataParameter1</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::IffOperationalParameter1Enum::IffOperationalParameter1Enum getSecondaryOperationalDataParameter1()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>secondaryOperationalDataParameter1</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Additional characteristics of the IFF/ATC/NAVAIDS emitting system.</i>
      * <br>Description of the data type from the FOM: <i>IFF operational parameter 1</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>secondaryOperationalDataParameter1</code> attribute.
      */
      LIBAPI virtual DevStudio::IffOperationalParameter1Enum::IffOperationalParameter1Enum getSecondaryOperationalDataParameter1(DevStudio::IffOperationalParameter1Enum::IffOperationalParameter1Enum defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>secondaryOperationalDataParameter1</code> attribute.
      * <br>Description from the FOM: <i>Additional characteristics of the IFF/ATC/NAVAIDS emitting system.</i>
      * <br>Description of the data type from the FOM: <i>IFF operational parameter 1</i>
      *
      * @return the time stamped <code>secondaryOperationalDataParameter1</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::IffOperationalParameter1Enum::IffOperationalParameter1Enum > getSecondaryOperationalDataParameter1TimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>secondaryOperationalDataParameter2</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Additional characteristics of the IFF/ATC/NAVAIDS emitting system.</i>
      *
      * @return true if <code>secondaryOperationalDataParameter2</code> is available.
      */
      LIBAPI virtual bool hasSecondaryOperationalDataParameter2() = 0;

      /**
      * Gets the value of the <code>secondaryOperationalDataParameter2</code> attribute.
      *
      * <br>Description from the FOM: <i>Additional characteristics of the IFF/ATC/NAVAIDS emitting system.</i>
      * <br>Description of the data type from the FOM: <i>IFF operational parameter 2</i>
      *
      * @return the <code>secondaryOperationalDataParameter2</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::IffOperationalParameter2Enum::IffOperationalParameter2Enum getSecondaryOperationalDataParameter2()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>secondaryOperationalDataParameter2</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Additional characteristics of the IFF/ATC/NAVAIDS emitting system.</i>
      * <br>Description of the data type from the FOM: <i>IFF operational parameter 2</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>secondaryOperationalDataParameter2</code> attribute.
      */
      LIBAPI virtual DevStudio::IffOperationalParameter2Enum::IffOperationalParameter2Enum getSecondaryOperationalDataParameter2(DevStudio::IffOperationalParameter2Enum::IffOperationalParameter2Enum defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>secondaryOperationalDataParameter2</code> attribute.
      * <br>Description from the FOM: <i>Additional characteristics of the IFF/ATC/NAVAIDS emitting system.</i>
      * <br>Description of the data type from the FOM: <i>IFF operational parameter 2</i>
      *
      * @return the time stamped <code>secondaryOperationalDataParameter2</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::IffOperationalParameter2Enum::IffOperationalParameter2Enum > getSecondaryOperationalDataParameter2TimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>systemMode</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Mode of operation.</i>
      *
      * @return true if <code>systemMode</code> is available.
      */
      LIBAPI virtual bool hasSystemMode() = 0;

      /**
      * Gets the value of the <code>systemMode</code> attribute.
      *
      * <br>Description from the FOM: <i>Mode of operation.</i>
      * <br>Description of the data type from the FOM: <i>IFF system mode</i>
      *
      * @return the <code>systemMode</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::IffSystemModeEnum::IffSystemModeEnum getSystemMode()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>systemMode</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Mode of operation.</i>
      * <br>Description of the data type from the FOM: <i>IFF system mode</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>systemMode</code> attribute.
      */
      LIBAPI virtual DevStudio::IffSystemModeEnum::IffSystemModeEnum getSystemMode(DevStudio::IffSystemModeEnum::IffSystemModeEnum defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>systemMode</code> attribute.
      * <br>Description from the FOM: <i>Mode of operation.</i>
      * <br>Description of the data type from the FOM: <i>IFF system mode</i>
      *
      * @return the time stamped <code>systemMode</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::IffSystemModeEnum::IffSystemModeEnum > getSystemModeTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>systemName</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Particular named type of the IFF system in use.</i>
      *
      * @return true if <code>systemName</code> is available.
      */
      LIBAPI virtual bool hasSystemName() = 0;

      /**
      * Gets the value of the <code>systemName</code> attribute.
      *
      * <br>Description from the FOM: <i>Particular named type of the IFF system in use.</i>
      * <br>Description of the data type from the FOM: <i>IFF system name</i>
      *
      * @return the <code>systemName</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::IffSystemNameEnum::IffSystemNameEnum getSystemName()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>systemName</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Particular named type of the IFF system in use.</i>
      * <br>Description of the data type from the FOM: <i>IFF system name</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>systemName</code> attribute.
      */
      LIBAPI virtual DevStudio::IffSystemNameEnum::IffSystemNameEnum getSystemName(DevStudio::IffSystemNameEnum::IffSystemNameEnum defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>systemName</code> attribute.
      * <br>Description from the FOM: <i>Particular named type of the IFF system in use.</i>
      * <br>Description of the data type from the FOM: <i>IFF system name</i>
      *
      * @return the time stamped <code>systemName</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::IffSystemNameEnum::IffSystemNameEnum > getSystemNameTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>systemType</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>General type of IFF system in use.</i>
      *
      * @return true if <code>systemType</code> is available.
      */
      LIBAPI virtual bool hasSystemType() = 0;

      /**
      * Gets the value of the <code>systemType</code> attribute.
      *
      * <br>Description from the FOM: <i>General type of IFF system in use.</i>
      * <br>Description of the data type from the FOM: <i>IFF system type</i>
      *
      * @return the <code>systemType</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::IffSystemTypeEnum::IffSystemTypeEnum getSystemType()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>systemType</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>General type of IFF system in use.</i>
      * <br>Description of the data type from the FOM: <i>IFF system type</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>systemType</code> attribute.
      */
      LIBAPI virtual DevStudio::IffSystemTypeEnum::IffSystemTypeEnum getSystemType(DevStudio::IffSystemTypeEnum::IffSystemTypeEnum defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>systemType</code> attribute.
      * <br>Description from the FOM: <i>General type of IFF system in use.</i>
      * <br>Description of the data type from the FOM: <i>IFF system type</i>
      *
      * @return the time stamped <code>systemType</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::IffSystemTypeEnum::IffSystemTypeEnum > getSystemTypeTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>systemIsOn</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Whether or not the system is on.</i>
      *
      * @return true if <code>systemIsOn</code> is available.
      */
      LIBAPI virtual bool hasSystemIsOn() = 0;

      /**
      * Gets the value of the <code>systemIsOn</code> attribute.
      *
      * <br>Description from the FOM: <i>Whether or not the system is on.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>systemIsOn</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual bool getSystemIsOn()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>systemIsOn</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Whether or not the system is on.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>systemIsOn</code> attribute.
      */
      LIBAPI virtual bool getSystemIsOn(bool defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>systemIsOn</code> attribute.
      * <br>Description from the FOM: <i>Whether or not the system is on.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>systemIsOn</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< bool > getSystemIsOnTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>systemIsOperational</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Whether or not the system is operational.</i>
      *
      * @return true if <code>systemIsOperational</code> is available.
      */
      LIBAPI virtual bool hasSystemIsOperational() = 0;

      /**
      * Gets the value of the <code>systemIsOperational</code> attribute.
      *
      * <br>Description from the FOM: <i>Whether or not the system is operational.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>systemIsOperational</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual bool getSystemIsOperational()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>systemIsOperational</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Whether or not the system is operational.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>systemIsOperational</code> attribute.
      */
      LIBAPI virtual bool getSystemIsOperational(bool defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>systemIsOperational</code> attribute.
      * <br>Description from the FOM: <i>Whether or not the system is operational.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>systemIsOperational</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< bool > getSystemIsOperationalTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>entityIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
      *
      * @return true if <code>entityIdentifier</code> is available.
      */
      LIBAPI virtual bool hasEntityIdentifier() = 0;

      /**
      * Gets the value of the <code>entityIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @return the <code>entityIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::EntityIdentifierStruct getEntityIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>entityIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>entityIdentifier</code> attribute.
      */
      LIBAPI virtual DevStudio::EntityIdentifierStruct getEntityIdentifier(DevStudio::EntityIdentifierStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>entityIdentifier</code> attribute.
      * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @return the time stamped <code>entityIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::EntityIdentifierStruct > getEntityIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>hostObjectIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
      *
      * @return true if <code>hostObjectIdentifier</code> is available.
      */
      LIBAPI virtual bool hasHostObjectIdentifier() = 0;

      /**
      * Gets the value of the <code>hostObjectIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @return the <code>hostObjectIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::string getHostObjectIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>hostObjectIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>hostObjectIdentifier</code> attribute.
      */
      LIBAPI virtual std::string getHostObjectIdentifier(std::string defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>hostObjectIdentifier</code> attribute.
      * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @return the time stamped <code>hostObjectIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::string > getHostObjectIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>relativePosition</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
      *
      * @return true if <code>relativePosition</code> is available.
      */
      LIBAPI virtual bool hasRelativePosition() = 0;

      /**
      * Gets the value of the <code>relativePosition</code> attribute.
      *
      * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
      * <br>Description of the data type from the FOM: <i>Relative position in right-handed Cartesian coordinates.</i>
      *
      * @return the <code>relativePosition</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::RelativePositionStruct getRelativePosition()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>relativePosition</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
      * <br>Description of the data type from the FOM: <i>Relative position in right-handed Cartesian coordinates.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>relativePosition</code> attribute.
      */
      LIBAPI virtual DevStudio::RelativePositionStruct getRelativePosition(DevStudio::RelativePositionStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>relativePosition</code> attribute.
      * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
      * <br>Description of the data type from the FOM: <i>Relative position in right-handed Cartesian coordinates.</i>
      *
      * @return the time stamped <code>relativePosition</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::RelativePositionStruct > getRelativePositionTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
   };
}
#endif
