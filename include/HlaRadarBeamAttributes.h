/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLARADARBEAMATTRIBUTES_H
#define DEVELOPER_STUDIO_HLARADARBEAMATTRIBUTES_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <string>
#include <vector>
#include <utility>
    
#include <boost/noncopyable.hpp>
    
#include <DevStudio/datatypes/BeamFunctionCodeEnum.h>
#include <DevStudio/datatypes/EventIdentifierStruct.h>
#include <DevStudio/datatypes/RTIobjectIdArray.h>
#include <string>
#include <vector>

#include <DevStudio/HlaException.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaEmitterBeamAttributes.h>
#include <DevStudio/HlaTimeStamped.h>

namespace DevStudio {

   /**
   * Interface used to represent all attributes for an object instance.
   */
   class HlaRadarBeamAttributes : public HlaEmitterBeamAttributes {
   public:


     /**
      * An enumeration of the attributes of an HlaRadarBeam
      *
      *<table summary="All attributes">
      * <tr><td><b>Enum constant</b></td><td><b>C++ name</b></td><td><b>FOM name</b></td></tr>
      * <tr><td>HIGH_DENSITY_TRACK</td><td>highDensityTrack</td><td><code>HighDensityTrack</code></td></tr>
      * <tr><td>TRACK_OBJECT_IDENTIFIERS</td><td>trackObjectIdentifiers</td><td><code>TrackObjectIdentifiers</code></td></tr>
      * <tr><td>BEAM_AZIMUTH_CENTER</td><td>beamAzimuthCenter</td><td><code>BeamAzimuthCenter</code></td></tr>
      * <tr><td>BEAM_AZIMUTH_SWEEP</td><td>beamAzimuthSweep</td><td><code>BeamAzimuthSweep</code></td></tr>
      * <tr><td>BEAM_ELEVATION_CENTER</td><td>beamElevationCenter</td><td><code>BeamElevationCenter</code></td></tr>
      * <tr><td>BEAM_ELEVATION_SWEEP</td><td>beamElevationSweep</td><td><code>BeamElevationSweep</code></td></tr>
      * <tr><td>BEAM_FUNCTION_CODE</td><td>beamFunctionCode</td><td><code>BeamFunctionCode</code></td></tr>
      * <tr><td>BEAM_IDENTIFIER</td><td>beamIdentifier</td><td><code>BeamIdentifier</code></td></tr>
      * <tr><td>BEAM_PARAMETER_INDEX</td><td>beamParameterIndex</td><td><code>BeamParameterIndex</code></td></tr>
      * <tr><td>EFFECTIVE_RADIATED_POWER</td><td>effectiveRadiatedPower</td><td><code>EffectiveRadiatedPower</code></td></tr>
      * <tr><td>EMISSION_FREQUENCY</td><td>emissionFrequency</td><td><code>EmissionFrequency</code></td></tr>
      * <tr><td>EMITTER_SYSTEM_IDENTIFIER</td><td>emitterSystemIdentifier</td><td><code>EmitterSystemIdentifier</code></td></tr>
      * <tr><td>EVENT_IDENTIFIER</td><td>eventIdentifier</td><td><code>EventIdentifier</code></td></tr>
      * <tr><td>FREQUENCY_RANGE</td><td>frequencyRange</td><td><code>FrequencyRange</code></td></tr>
      * <tr><td>PULSE_REPETITION_FREQUENCY</td><td>pulseRepetitionFrequency</td><td><code>PulseRepetitionFrequency</code></td></tr>
      * <tr><td>PULSE_WIDTH</td><td>pulseWidth</td><td><code>PulseWidth</code></td></tr>
      * <tr><td>SWEEP_SYNCH</td><td>sweepSynch</td><td><code>SweepSynch</code></td></tr>
      *</table>
      */
      enum Attribute {
        /**
        * highDensityTrack (FOM name: <code>HighDensityTrack</code>).
        * <br>Description from the FOM: <i>When TRUE the receiving simulation can assume that all targets that are in the scan pattern of the radar beam are being tracked</i>
        */
         HIGH_DENSITY_TRACK,

        /**
        * trackObjectIdentifiers (FOM name: <code>TrackObjectIdentifiers</code>).
        * <br>Description from the FOM: <i>Identification of the entities being tracked.</i>
        */
         TRACK_OBJECT_IDENTIFIERS,

        /**
        * beamAzimuthCenter (FOM name: <code>BeamAzimuthCenter</code>).
        * <br>Description from the FOM: <i>The azimuth center of the emitter beam's scan volume relative to the emitter system.</i>
        */
         BEAM_AZIMUTH_CENTER,

        /**
        * beamAzimuthSweep (FOM name: <code>BeamAzimuthSweep</code>).
        * <br>Description from the FOM: <i>The azimuth half-angle of the emitter beam's scan volume relative to the emitter system.</i>
        */
         BEAM_AZIMUTH_SWEEP,

        /**
        * beamElevationCenter (FOM name: <code>BeamElevationCenter</code>).
        * <br>Description from the FOM: <i>The elevation center of the emitter beam's scan volume relative to the emitter system.</i>
        */
         BEAM_ELEVATION_CENTER,

        /**
        * beamElevationSweep (FOM name: <code>BeamElevationSweep</code>).
        * <br>Description from the FOM: <i>The elevation half-angle of the emitter beam's scan volume relative to the emitter system.</i>
        */
         BEAM_ELEVATION_SWEEP,

        /**
        * beamFunctionCode (FOM name: <code>BeamFunctionCode</code>).
        * <br>Description from the FOM: <i>The function of the emitter beam.</i>
        */
         BEAM_FUNCTION_CODE,

        /**
        * beamIdentifier (FOM name: <code>BeamIdentifier</code>).
        * <br>Description from the FOM: <i>The identification of the emitter beam (must be unique on the emitter system).</i>
        */
         BEAM_IDENTIFIER,

        /**
        * beamParameterIndex (FOM name: <code>BeamParameterIndex</code>).
        * <br>Description from the FOM: <i>The index, into the federation specific emissions database, of the current operating mode of the emitter beam.</i>
        */
         BEAM_PARAMETER_INDEX,

        /**
        * effectiveRadiatedPower (FOM name: <code>EffectiveRadiatedPower</code>).
        * <br>Description from the FOM: <i>The effective radiated power of the emitter beam.</i>
        */
         EFFECTIVE_RADIATED_POWER,

        /**
        * emissionFrequency (FOM name: <code>EmissionFrequency</code>).
        * <br>Description from the FOM: <i>The center frequency of the emitter beam.</i>
        */
         EMISSION_FREQUENCY,

        /**
        * emitterSystemIdentifier (FOM name: <code>EmitterSystemIdentifier</code>).
        * <br>Description from the FOM: <i>The identification of the emitter system that is generating this emitter beam.</i>
        */
         EMITTER_SYSTEM_IDENTIFIER,

        /**
        * eventIdentifier (FOM name: <code>EventIdentifier</code>).
        * <br>Description from the FOM: <i>The EventIdentifier is used by the generating federate to associate related events. The event number shall start at one at the beginning of the exercise, and be incremented by one for each event.</i>
        */
         EVENT_IDENTIFIER,

        /**
        * frequencyRange (FOM name: <code>FrequencyRange</code>).
        * <br>Description from the FOM: <i>The bandwidth of the frequencies covered by the emitter beam.</i>
        */
         FREQUENCY_RANGE,

        /**
        * pulseRepetitionFrequency (FOM name: <code>PulseRepetitionFrequency</code>).
        * <br>Description from the FOM: <i>The Pulse Repetition Frequency of the emitter beam.</i>
        */
         PULSE_REPETITION_FREQUENCY,

        /**
        * pulseWidth (FOM name: <code>PulseWidth</code>).
        * <br>Description from the FOM: <i>The pulse width of the emitter beam.</i>
        */
         PULSE_WIDTH,

        /**
        * sweepSynch (FOM name: <code>SweepSynch</code>).
        * <br>Description from the FOM: <i>The percentage of time a scan is through its pattern from its origin.</i>
        */
         SWEEP_SYNCH
      };

     /**
      * Gets the name of the attribute.
      *
      * @return The name of the attribute. An empty string will be returned if the attribute does not exist.
      */
      LIBAPI virtual std::wstring getName(Attribute attribute);

     /**
      * Finds the attribute specified in the parameter attributeName.
      * The found enumeration will be stored in the parameter attribute.
      *
      * @return true if the attribute was found, false otherwise.
      */
      LIBAPI virtual bool find(Attribute& attribute, std::wstring attributeName);

      LIBAPI virtual ~HlaRadarBeamAttributes() {}
    
      /**
      * Returns true if the <code>highDensityTrack</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>When TRUE the receiving simulation can assume that all targets that are in the scan pattern of the radar beam are being tracked</i>
      *
      * @return true if <code>highDensityTrack</code> is available.
      */
      LIBAPI virtual bool hasHighDensityTrack() = 0;

      /**
      * Gets the value of the <code>highDensityTrack</code> attribute.
      *
      * <br>Description from the FOM: <i>When TRUE the receiving simulation can assume that all targets that are in the scan pattern of the radar beam are being tracked</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>highDensityTrack</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual bool getHighDensityTrack()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>highDensityTrack</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>When TRUE the receiving simulation can assume that all targets that are in the scan pattern of the radar beam are being tracked</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>highDensityTrack</code> attribute.
      */
      LIBAPI virtual bool getHighDensityTrack(bool defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>highDensityTrack</code> attribute.
      * <br>Description from the FOM: <i>When TRUE the receiving simulation can assume that all targets that are in the scan pattern of the radar beam are being tracked</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>highDensityTrack</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< bool > getHighDensityTrackTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>trackObjectIdentifiers</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Identification of the entities being tracked.</i>
      *
      * @return true if <code>trackObjectIdentifiers</code> is available.
      */
      LIBAPI virtual bool hasTrackObjectIdentifiers() = 0;

      /**
      * Gets the value of the <code>trackObjectIdentifiers</code> attribute.
      *
      * <br>Description from the FOM: <i>Identification of the entities being tracked.</i>
      * <br>Description of the data type from the FOM: <i>Set of ID's of registered object instances.</i>
      *
      * @return the <code>trackObjectIdentifiers</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::vector<std::string > getTrackObjectIdentifiers()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>trackObjectIdentifiers</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Identification of the entities being tracked.</i>
      * <br>Description of the data type from the FOM: <i>Set of ID's of registered object instances.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>trackObjectIdentifiers</code> attribute.
      */
      LIBAPI virtual std::vector<std::string > getTrackObjectIdentifiers(std::vector<std::string > defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>trackObjectIdentifiers</code> attribute.
      * <br>Description from the FOM: <i>Identification of the entities being tracked.</i>
      * <br>Description of the data type from the FOM: <i>Set of ID's of registered object instances.</i>
      *
      * @return the time stamped <code>trackObjectIdentifiers</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::vector<std::string > > getTrackObjectIdentifiersTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>beamAzimuthCenter</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The azimuth center of the emitter beam's scan volume relative to the emitter system.</i>
      *
      * @return true if <code>beamAzimuthCenter</code> is available.
      */
      LIBAPI virtual bool hasBeamAzimuthCenter() = 0;

      /**
      * Gets the value of the <code>beamAzimuthCenter</code> attribute.
      *
      * <br>Description from the FOM: <i>The azimuth center of the emitter beam's scan volume relative to the emitter system.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @return the <code>beamAzimuthCenter</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual float getBeamAzimuthCenter()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>beamAzimuthCenter</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The azimuth center of the emitter beam's scan volume relative to the emitter system.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>beamAzimuthCenter</code> attribute.
      */
      LIBAPI virtual float getBeamAzimuthCenter(float defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>beamAzimuthCenter</code> attribute.
      * <br>Description from the FOM: <i>The azimuth center of the emitter beam's scan volume relative to the emitter system.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @return the time stamped <code>beamAzimuthCenter</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< float > getBeamAzimuthCenterTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>beamAzimuthSweep</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The azimuth half-angle of the emitter beam's scan volume relative to the emitter system.</i>
      *
      * @return true if <code>beamAzimuthSweep</code> is available.
      */
      LIBAPI virtual bool hasBeamAzimuthSweep() = 0;

      /**
      * Gets the value of the <code>beamAzimuthSweep</code> attribute.
      *
      * <br>Description from the FOM: <i>The azimuth half-angle of the emitter beam's scan volume relative to the emitter system.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @return the <code>beamAzimuthSweep</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual float getBeamAzimuthSweep()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>beamAzimuthSweep</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The azimuth half-angle of the emitter beam's scan volume relative to the emitter system.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>beamAzimuthSweep</code> attribute.
      */
      LIBAPI virtual float getBeamAzimuthSweep(float defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>beamAzimuthSweep</code> attribute.
      * <br>Description from the FOM: <i>The azimuth half-angle of the emitter beam's scan volume relative to the emitter system.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @return the time stamped <code>beamAzimuthSweep</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< float > getBeamAzimuthSweepTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>beamElevationCenter</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The elevation center of the emitter beam's scan volume relative to the emitter system.</i>
      *
      * @return true if <code>beamElevationCenter</code> is available.
      */
      LIBAPI virtual bool hasBeamElevationCenter() = 0;

      /**
      * Gets the value of the <code>beamElevationCenter</code> attribute.
      *
      * <br>Description from the FOM: <i>The elevation center of the emitter beam's scan volume relative to the emitter system.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @return the <code>beamElevationCenter</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual float getBeamElevationCenter()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>beamElevationCenter</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The elevation center of the emitter beam's scan volume relative to the emitter system.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>beamElevationCenter</code> attribute.
      */
      LIBAPI virtual float getBeamElevationCenter(float defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>beamElevationCenter</code> attribute.
      * <br>Description from the FOM: <i>The elevation center of the emitter beam's scan volume relative to the emitter system.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @return the time stamped <code>beamElevationCenter</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< float > getBeamElevationCenterTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>beamElevationSweep</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The elevation half-angle of the emitter beam's scan volume relative to the emitter system.</i>
      *
      * @return true if <code>beamElevationSweep</code> is available.
      */
      LIBAPI virtual bool hasBeamElevationSweep() = 0;

      /**
      * Gets the value of the <code>beamElevationSweep</code> attribute.
      *
      * <br>Description from the FOM: <i>The elevation half-angle of the emitter beam's scan volume relative to the emitter system.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @return the <code>beamElevationSweep</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual float getBeamElevationSweep()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>beamElevationSweep</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The elevation half-angle of the emitter beam's scan volume relative to the emitter system.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>beamElevationSweep</code> attribute.
      */
      LIBAPI virtual float getBeamElevationSweep(float defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>beamElevationSweep</code> attribute.
      * <br>Description from the FOM: <i>The elevation half-angle of the emitter beam's scan volume relative to the emitter system.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @return the time stamped <code>beamElevationSweep</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< float > getBeamElevationSweepTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>beamFunctionCode</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The function of the emitter beam.</i>
      *
      * @return true if <code>beamFunctionCode</code> is available.
      */
      LIBAPI virtual bool hasBeamFunctionCode() = 0;

      /**
      * Gets the value of the <code>beamFunctionCode</code> attribute.
      *
      * <br>Description from the FOM: <i>The function of the emitter beam.</i>
      * <br>Description of the data type from the FOM: <i>Beam function</i>
      *
      * @return the <code>beamFunctionCode</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::BeamFunctionCodeEnum::BeamFunctionCodeEnum getBeamFunctionCode()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>beamFunctionCode</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The function of the emitter beam.</i>
      * <br>Description of the data type from the FOM: <i>Beam function</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>beamFunctionCode</code> attribute.
      */
      LIBAPI virtual DevStudio::BeamFunctionCodeEnum::BeamFunctionCodeEnum getBeamFunctionCode(DevStudio::BeamFunctionCodeEnum::BeamFunctionCodeEnum defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>beamFunctionCode</code> attribute.
      * <br>Description from the FOM: <i>The function of the emitter beam.</i>
      * <br>Description of the data type from the FOM: <i>Beam function</i>
      *
      * @return the time stamped <code>beamFunctionCode</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::BeamFunctionCodeEnum::BeamFunctionCodeEnum > getBeamFunctionCodeTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>beamIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The identification of the emitter beam (must be unique on the emitter system).</i>
      *
      * @return true if <code>beamIdentifier</code> is available.
      */
      LIBAPI virtual bool hasBeamIdentifier() = 0;

      /**
      * Gets the value of the <code>beamIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>The identification of the emitter beam (must be unique on the emitter system).</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the <code>beamIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual char getBeamIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>beamIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The identification of the emitter beam (must be unique on the emitter system).</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>beamIdentifier</code> attribute.
      */
      LIBAPI virtual char getBeamIdentifier(char defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>beamIdentifier</code> attribute.
      * <br>Description from the FOM: <i>The identification of the emitter beam (must be unique on the emitter system).</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the time stamped <code>beamIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< char > getBeamIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>beamParameterIndex</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The index, into the federation specific emissions database, of the current operating mode of the emitter beam.</i>
      *
      * @return true if <code>beamParameterIndex</code> is available.
      */
      LIBAPI virtual bool hasBeamParameterIndex() = 0;

      /**
      * Gets the value of the <code>beamParameterIndex</code> attribute.
      *
      * <br>Description from the FOM: <i>The index, into the federation specific emissions database, of the current operating mode of the emitter beam.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the <code>beamParameterIndex</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual unsigned short getBeamParameterIndex()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>beamParameterIndex</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The index, into the federation specific emissions database, of the current operating mode of the emitter beam.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>beamParameterIndex</code> attribute.
      */
      LIBAPI virtual unsigned short getBeamParameterIndex(unsigned short defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>beamParameterIndex</code> attribute.
      * <br>Description from the FOM: <i>The index, into the federation specific emissions database, of the current operating mode of the emitter beam.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the time stamped <code>beamParameterIndex</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< unsigned short > getBeamParameterIndexTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>effectiveRadiatedPower</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The effective radiated power of the emitter beam.</i>
      *
      * @return true if <code>effectiveRadiatedPower</code> is available.
      */
      LIBAPI virtual bool hasEffectiveRadiatedPower() = 0;

      /**
      * Gets the value of the <code>effectiveRadiatedPower</code> attribute.
      *
      * <br>Description from the FOM: <i>The effective radiated power of the emitter beam.</i>
      * <br>Description of the data type from the FOM: <i>Power ratio in decibels (dB) of a measured power referenced to 1 milliwatt (mW). [unit: decibel milliwatt (dBm), resolution: NA, accuracy: perfect]</i>
      *
      * @return the <code>effectiveRadiatedPower</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual float getEffectiveRadiatedPower()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>effectiveRadiatedPower</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The effective radiated power of the emitter beam.</i>
      * <br>Description of the data type from the FOM: <i>Power ratio in decibels (dB) of a measured power referenced to 1 milliwatt (mW). [unit: decibel milliwatt (dBm), resolution: NA, accuracy: perfect]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>effectiveRadiatedPower</code> attribute.
      */
      LIBAPI virtual float getEffectiveRadiatedPower(float defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>effectiveRadiatedPower</code> attribute.
      * <br>Description from the FOM: <i>The effective radiated power of the emitter beam.</i>
      * <br>Description of the data type from the FOM: <i>Power ratio in decibels (dB) of a measured power referenced to 1 milliwatt (mW). [unit: decibel milliwatt (dBm), resolution: NA, accuracy: perfect]</i>
      *
      * @return the time stamped <code>effectiveRadiatedPower</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< float > getEffectiveRadiatedPowerTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>emissionFrequency</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The center frequency of the emitter beam.</i>
      *
      * @return true if <code>emissionFrequency</code> is available.
      */
      LIBAPI virtual bool hasEmissionFrequency() = 0;

      /**
      * Gets the value of the <code>emissionFrequency</code> attribute.
      *
      * <br>Description from the FOM: <i>The center frequency of the emitter beam.</i>
      * <br>Description of the data type from the FOM: <i>Frequency, based on SI derived unit hertz, unit symbol Hz. [unit: hertz (Hz), resolution: NA, accuracy: NA]</i>
      *
      * @return the <code>emissionFrequency</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual float getEmissionFrequency()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>emissionFrequency</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The center frequency of the emitter beam.</i>
      * <br>Description of the data type from the FOM: <i>Frequency, based on SI derived unit hertz, unit symbol Hz. [unit: hertz (Hz), resolution: NA, accuracy: NA]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>emissionFrequency</code> attribute.
      */
      LIBAPI virtual float getEmissionFrequency(float defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>emissionFrequency</code> attribute.
      * <br>Description from the FOM: <i>The center frequency of the emitter beam.</i>
      * <br>Description of the data type from the FOM: <i>Frequency, based on SI derived unit hertz, unit symbol Hz. [unit: hertz (Hz), resolution: NA, accuracy: NA]</i>
      *
      * @return the time stamped <code>emissionFrequency</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< float > getEmissionFrequencyTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>emitterSystemIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The identification of the emitter system that is generating this emitter beam.</i>
      *
      * @return true if <code>emitterSystemIdentifier</code> is available.
      */
      LIBAPI virtual bool hasEmitterSystemIdentifier() = 0;

      /**
      * Gets the value of the <code>emitterSystemIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>The identification of the emitter system that is generating this emitter beam.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @return the <code>emitterSystemIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::string getEmitterSystemIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>emitterSystemIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The identification of the emitter system that is generating this emitter beam.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>emitterSystemIdentifier</code> attribute.
      */
      LIBAPI virtual std::string getEmitterSystemIdentifier(std::string defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>emitterSystemIdentifier</code> attribute.
      * <br>Description from the FOM: <i>The identification of the emitter system that is generating this emitter beam.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @return the time stamped <code>emitterSystemIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::string > getEmitterSystemIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>eventIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The EventIdentifier is used by the generating federate to associate related events. The event number shall start at one at the beginning of the exercise, and be incremented by one for each event.</i>
      *
      * @return true if <code>eventIdentifier</code> is available.
      */
      LIBAPI virtual bool hasEventIdentifier() = 0;

      /**
      * Gets the value of the <code>eventIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>The EventIdentifier is used by the generating federate to associate related events. The event number shall start at one at the beginning of the exercise, and be incremented by one for each event.</i>
      * <br>Description of the data type from the FOM: <i>Identification of an event. Based on the Event Identifier record as specified in IEEE 1278.1-1995 section 5.2.18.</i>
      *
      * @return the <code>eventIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::EventIdentifierStruct getEventIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>eventIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The EventIdentifier is used by the generating federate to associate related events. The event number shall start at one at the beginning of the exercise, and be incremented by one for each event.</i>
      * <br>Description of the data type from the FOM: <i>Identification of an event. Based on the Event Identifier record as specified in IEEE 1278.1-1995 section 5.2.18.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>eventIdentifier</code> attribute.
      */
      LIBAPI virtual DevStudio::EventIdentifierStruct getEventIdentifier(DevStudio::EventIdentifierStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>eventIdentifier</code> attribute.
      * <br>Description from the FOM: <i>The EventIdentifier is used by the generating federate to associate related events. The event number shall start at one at the beginning of the exercise, and be incremented by one for each event.</i>
      * <br>Description of the data type from the FOM: <i>Identification of an event. Based on the Event Identifier record as specified in IEEE 1278.1-1995 section 5.2.18.</i>
      *
      * @return the time stamped <code>eventIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::EventIdentifierStruct > getEventIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>frequencyRange</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The bandwidth of the frequencies covered by the emitter beam.</i>
      *
      * @return true if <code>frequencyRange</code> is available.
      */
      LIBAPI virtual bool hasFrequencyRange() = 0;

      /**
      * Gets the value of the <code>frequencyRange</code> attribute.
      *
      * <br>Description from the FOM: <i>The bandwidth of the frequencies covered by the emitter beam.</i>
      * <br>Description of the data type from the FOM: <i>Frequency, based on SI derived unit hertz, unit symbol Hz. [unit: hertz (Hz), resolution: NA, accuracy: NA]</i>
      *
      * @return the <code>frequencyRange</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual float getFrequencyRange()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>frequencyRange</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The bandwidth of the frequencies covered by the emitter beam.</i>
      * <br>Description of the data type from the FOM: <i>Frequency, based on SI derived unit hertz, unit symbol Hz. [unit: hertz (Hz), resolution: NA, accuracy: NA]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>frequencyRange</code> attribute.
      */
      LIBAPI virtual float getFrequencyRange(float defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>frequencyRange</code> attribute.
      * <br>Description from the FOM: <i>The bandwidth of the frequencies covered by the emitter beam.</i>
      * <br>Description of the data type from the FOM: <i>Frequency, based on SI derived unit hertz, unit symbol Hz. [unit: hertz (Hz), resolution: NA, accuracy: NA]</i>
      *
      * @return the time stamped <code>frequencyRange</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< float > getFrequencyRangeTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>pulseRepetitionFrequency</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The Pulse Repetition Frequency of the emitter beam.</i>
      *
      * @return true if <code>pulseRepetitionFrequency</code> is available.
      */
      LIBAPI virtual bool hasPulseRepetitionFrequency() = 0;

      /**
      * Gets the value of the <code>pulseRepetitionFrequency</code> attribute.
      *
      * <br>Description from the FOM: <i>The Pulse Repetition Frequency of the emitter beam.</i>
      * <br>Description of the data type from the FOM: <i>Frequency, based on SI derived unit hertz, unit symbol Hz. [unit: hertz (Hz), resolution: NA, accuracy: NA]</i>
      *
      * @return the <code>pulseRepetitionFrequency</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual float getPulseRepetitionFrequency()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>pulseRepetitionFrequency</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The Pulse Repetition Frequency of the emitter beam.</i>
      * <br>Description of the data type from the FOM: <i>Frequency, based on SI derived unit hertz, unit symbol Hz. [unit: hertz (Hz), resolution: NA, accuracy: NA]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>pulseRepetitionFrequency</code> attribute.
      */
      LIBAPI virtual float getPulseRepetitionFrequency(float defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>pulseRepetitionFrequency</code> attribute.
      * <br>Description from the FOM: <i>The Pulse Repetition Frequency of the emitter beam.</i>
      * <br>Description of the data type from the FOM: <i>Frequency, based on SI derived unit hertz, unit symbol Hz. [unit: hertz (Hz), resolution: NA, accuracy: NA]</i>
      *
      * @return the time stamped <code>pulseRepetitionFrequency</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< float > getPulseRepetitionFrequencyTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>pulseWidth</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The pulse width of the emitter beam.</i>
      *
      * @return true if <code>pulseWidth</code> is available.
      */
      LIBAPI virtual bool hasPulseWidth() = 0;

      /**
      * Gets the value of the <code>pulseWidth</code> attribute.
      *
      * <br>Description from the FOM: <i>The pulse width of the emitter beam.</i>
      * <br>Description of the data type from the FOM: <i>Time, based on SI base unit second, expressed in microsecond, unit symbol μs. [unit: microsecond, resolution: NA, accuracy: NA]</i>
      *
      * @return the <code>pulseWidth</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual float getPulseWidth()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>pulseWidth</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The pulse width of the emitter beam.</i>
      * <br>Description of the data type from the FOM: <i>Time, based on SI base unit second, expressed in microsecond, unit symbol μs. [unit: microsecond, resolution: NA, accuracy: NA]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>pulseWidth</code> attribute.
      */
      LIBAPI virtual float getPulseWidth(float defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>pulseWidth</code> attribute.
      * <br>Description from the FOM: <i>The pulse width of the emitter beam.</i>
      * <br>Description of the data type from the FOM: <i>Time, based on SI base unit second, expressed in microsecond, unit symbol μs. [unit: microsecond, resolution: NA, accuracy: NA]</i>
      *
      * @return the time stamped <code>pulseWidth</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< float > getPulseWidthTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>sweepSynch</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The percentage of time a scan is through its pattern from its origin.</i>
      *
      * @return true if <code>sweepSynch</code> is available.
      */
      LIBAPI virtual bool hasSweepSynch() = 0;

      /**
      * Gets the value of the <code>sweepSynch</code> attribute.
      *
      * <br>Description from the FOM: <i>The percentage of time a scan is through its pattern from its origin.</i>
      * <br>Description of the data type from the FOM: <i>Percentage [unit: percent (%), resolution: NA, accuracy: NA]</i>
      *
      * @return the <code>sweepSynch</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual float getSweepSynch()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>sweepSynch</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The percentage of time a scan is through its pattern from its origin.</i>
      * <br>Description of the data type from the FOM: <i>Percentage [unit: percent (%), resolution: NA, accuracy: NA]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>sweepSynch</code> attribute.
      */
      LIBAPI virtual float getSweepSynch(float defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>sweepSynch</code> attribute.
      * <br>Description from the FOM: <i>The percentage of time a scan is through its pattern from its origin.</i>
      * <br>Description of the data type from the FOM: <i>Percentage [unit: percent (%), resolution: NA, accuracy: NA]</i>
      *
      * @return the time stamped <code>sweepSynch</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< float > getSweepSynchTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
   };
}
#endif
