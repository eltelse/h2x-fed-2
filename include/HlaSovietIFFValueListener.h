/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLASOVIETIFFVALUELISTENER_H
#define DEVELOPER_STUDIO_HLASOVIETIFFVALUELISTENER_H

#include <memory>

#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EventIdentifierStruct.h>
#include <DevStudio/datatypes/FundamentalParameterDataStruct.h>
#include <DevStudio/datatypes/FundamentalParameterDataStructLengthlessArray.h>
#include <DevStudio/datatypes/IffOperationalParameter1Enum.h>
#include <DevStudio/datatypes/IffOperationalParameter2Enum.h>
#include <DevStudio/datatypes/IffSystemModeEnum.h>
#include <DevStudio/datatypes/IffSystemNameEnum.h>
#include <DevStudio/datatypes/IffSystemTypeEnum.h>
#include <DevStudio/datatypes/RelativePositionStruct.h>
#include <string>
#include <vector>

#include <DevStudio/HlaLogicalTime.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaSovietIFFAttributes.h>    

namespace DevStudio {

   /**
   * Listener for updates of attributes, with the new updated values.  
   */
   class HlaSovietIFFValueListener {

   public:

      LIBAPI virtual ~HlaSovietIFFValueListener() {}
    
      /**
      * This method is called when the attribute <code>beamAzimuthCenter</code> is updated.
      * <br>Description from the FOM: <i>The azimuth center of the IFF beam's scan volume relative to the IFF system.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @param sovietIFF The object which is updated.
      * @param beamAzimuthCenter The new value of the attribute in this update
      * @param validOldBeamAzimuthCenter True if oldBeamAzimuthCenter contains a valid value
      * @param oldBeamAzimuthCenter The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void beamAzimuthCenterUpdated(HlaSovietIFFPtr sovietIFF, float beamAzimuthCenter, bool validOldBeamAzimuthCenter, float oldBeamAzimuthCenter, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>beamAzimuthSweep</code> is updated.
      * <br>Description from the FOM: <i>The azimuth half-angle of the IFF beam's scan volume relative to the IFF system.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @param sovietIFF The object which is updated.
      * @param beamAzimuthSweep The new value of the attribute in this update
      * @param validOldBeamAzimuthSweep True if oldBeamAzimuthSweep contains a valid value
      * @param oldBeamAzimuthSweep The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void beamAzimuthSweepUpdated(HlaSovietIFFPtr sovietIFF, float beamAzimuthSweep, bool validOldBeamAzimuthSweep, float oldBeamAzimuthSweep, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>beamElevationCenter</code> is updated.
      * <br>Description from the FOM: <i>The elevation center of the IFF beam's scan volume relative to the IFF system.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @param sovietIFF The object which is updated.
      * @param beamElevationCenter The new value of the attribute in this update
      * @param validOldBeamElevationCenter True if oldBeamElevationCenter contains a valid value
      * @param oldBeamElevationCenter The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void beamElevationCenterUpdated(HlaSovietIFFPtr sovietIFF, float beamElevationCenter, bool validOldBeamElevationCenter, float oldBeamElevationCenter, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>beamElevationSweep</code> is updated.
      * <br>Description from the FOM: <i>The elevation half-angle of the IFF beam's scan volume relative to the IFF system.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @param sovietIFF The object which is updated.
      * @param beamElevationSweep The new value of the attribute in this update
      * @param validOldBeamElevationSweep True if oldBeamElevationSweep contains a valid value
      * @param oldBeamElevationSweep The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void beamElevationSweepUpdated(HlaSovietIFFPtr sovietIFF, float beamElevationSweep, bool validOldBeamElevationSweep, float oldBeamElevationSweep, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>beamSweepSync</code> is updated.
      * <br>Description from the FOM: <i>The percentage of time a scan is through its pattern from its origin.</i>
      * <br>Description of the data type from the FOM: <i>Percentage [unit: percent (%), resolution: NA, accuracy: NA]</i>
      *
      * @param sovietIFF The object which is updated.
      * @param beamSweepSync The new value of the attribute in this update
      * @param validOldBeamSweepSync True if oldBeamSweepSync contains a valid value
      * @param oldBeamSweepSync The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void beamSweepSyncUpdated(HlaSovietIFFPtr sovietIFF, float beamSweepSync, bool validOldBeamSweepSync, float oldBeamSweepSync, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>eventIdentifier</code> is updated.
      * <br>Description from the FOM: <i>Used to associate related events.</i>
      * <br>Description of the data type from the FOM: <i>Identification of an event. Based on the Event Identifier record as specified in IEEE 1278.1-1995 section 5.2.18.</i>
      *
      * @param sovietIFF The object which is updated.
      * @param eventIdentifier The new value of the attribute in this update
      * @param validOldEventIdentifier True if oldEventIdentifier contains a valid value
      * @param oldEventIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void eventIdentifierUpdated(HlaSovietIFFPtr sovietIFF, EventIdentifierStruct eventIdentifier, bool validOldEventIdentifier, EventIdentifierStruct oldEventIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>fundamentalParameterData</code> is updated.
      * <br>Description from the FOM: <i>The fundamental energy radiation characteristics of the IFF/ATC/NAVAIDS system.</i>
      * <br>Description of the data type from the FOM: <i>Array of Fundamental Parameter Data records.</i>
      *
      * @param sovietIFF The object which is updated.
      * @param fundamentalParameterData The new value of the attribute in this update
      * @param validOldFundamentalParameterData True if oldFundamentalParameterData contains a valid value
      * @param oldFundamentalParameterData The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void fundamentalParameterDataUpdated(HlaSovietIFFPtr sovietIFF, std::vector<DevStudio::FundamentalParameterDataStruct > fundamentalParameterData, bool validOldFundamentalParameterData, std::vector<DevStudio::FundamentalParameterDataStruct > oldFundamentalParameterData, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>layer2DataAvailable</code> is updated.
      * <br>Description from the FOM: <i>Specifies if level 2 data is available for this IFF system. If level 2 data is available then the BeamAzimuthCenter, BeamAzimuthSweep, BeamElevationCenter, BeamElevationSweep, BeamSweepSync, FundamentalParameterData, SecondaryOperationalDataParameter1, and SecondaryOperationalDataParameter2 attributes shall be generated.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param sovietIFF The object which is updated.
      * @param layer2DataAvailable The new value of the attribute in this update
      * @param validOldLayer2DataAvailable True if oldLayer2DataAvailable contains a valid value
      * @param oldLayer2DataAvailable The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void layer2DataAvailableUpdated(HlaSovietIFFPtr sovietIFF, bool layer2DataAvailable, bool validOldLayer2DataAvailable, bool oldLayer2DataAvailable, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>secondaryOperationalDataParameter1</code> is updated.
      * <br>Description from the FOM: <i>Additional characteristics of the IFF/ATC/NAVAIDS emitting system.</i>
      * <br>Description of the data type from the FOM: <i>IFF operational parameter 1</i>
      *
      * @param sovietIFF The object which is updated.
      * @param secondaryOperationalDataParameter1 The new value of the attribute in this update
      * @param validOldSecondaryOperationalDataParameter1 True if oldSecondaryOperationalDataParameter1 contains a valid value
      * @param oldSecondaryOperationalDataParameter1 The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void secondaryOperationalDataParameter1Updated(HlaSovietIFFPtr sovietIFF, IffOperationalParameter1Enum::IffOperationalParameter1Enum secondaryOperationalDataParameter1, bool validOldSecondaryOperationalDataParameter1, IffOperationalParameter1Enum::IffOperationalParameter1Enum oldSecondaryOperationalDataParameter1, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>secondaryOperationalDataParameter2</code> is updated.
      * <br>Description from the FOM: <i>Additional characteristics of the IFF/ATC/NAVAIDS emitting system.</i>
      * <br>Description of the data type from the FOM: <i>IFF operational parameter 2</i>
      *
      * @param sovietIFF The object which is updated.
      * @param secondaryOperationalDataParameter2 The new value of the attribute in this update
      * @param validOldSecondaryOperationalDataParameter2 True if oldSecondaryOperationalDataParameter2 contains a valid value
      * @param oldSecondaryOperationalDataParameter2 The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void secondaryOperationalDataParameter2Updated(HlaSovietIFFPtr sovietIFF, IffOperationalParameter2Enum::IffOperationalParameter2Enum secondaryOperationalDataParameter2, bool validOldSecondaryOperationalDataParameter2, IffOperationalParameter2Enum::IffOperationalParameter2Enum oldSecondaryOperationalDataParameter2, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>systemMode</code> is updated.
      * <br>Description from the FOM: <i>Mode of operation.</i>
      * <br>Description of the data type from the FOM: <i>IFF system mode</i>
      *
      * @param sovietIFF The object which is updated.
      * @param systemMode The new value of the attribute in this update
      * @param validOldSystemMode True if oldSystemMode contains a valid value
      * @param oldSystemMode The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void systemModeUpdated(HlaSovietIFFPtr sovietIFF, IffSystemModeEnum::IffSystemModeEnum systemMode, bool validOldSystemMode, IffSystemModeEnum::IffSystemModeEnum oldSystemMode, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>systemName</code> is updated.
      * <br>Description from the FOM: <i>Particular named type of the IFF system in use.</i>
      * <br>Description of the data type from the FOM: <i>IFF system name</i>
      *
      * @param sovietIFF The object which is updated.
      * @param systemName The new value of the attribute in this update
      * @param validOldSystemName True if oldSystemName contains a valid value
      * @param oldSystemName The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void systemNameUpdated(HlaSovietIFFPtr sovietIFF, IffSystemNameEnum::IffSystemNameEnum systemName, bool validOldSystemName, IffSystemNameEnum::IffSystemNameEnum oldSystemName, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>systemType</code> is updated.
      * <br>Description from the FOM: <i>General type of IFF system in use.</i>
      * <br>Description of the data type from the FOM: <i>IFF system type</i>
      *
      * @param sovietIFF The object which is updated.
      * @param systemType The new value of the attribute in this update
      * @param validOldSystemType True if oldSystemType contains a valid value
      * @param oldSystemType The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void systemTypeUpdated(HlaSovietIFFPtr sovietIFF, IffSystemTypeEnum::IffSystemTypeEnum systemType, bool validOldSystemType, IffSystemTypeEnum::IffSystemTypeEnum oldSystemType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>systemIsOn</code> is updated.
      * <br>Description from the FOM: <i>Whether or not the system is on.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param sovietIFF The object which is updated.
      * @param systemIsOn The new value of the attribute in this update
      * @param validOldSystemIsOn True if oldSystemIsOn contains a valid value
      * @param oldSystemIsOn The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void systemIsOnUpdated(HlaSovietIFFPtr sovietIFF, bool systemIsOn, bool validOldSystemIsOn, bool oldSystemIsOn, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>systemIsOperational</code> is updated.
      * <br>Description from the FOM: <i>Whether or not the system is operational.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param sovietIFF The object which is updated.
      * @param systemIsOperational The new value of the attribute in this update
      * @param validOldSystemIsOperational True if oldSystemIsOperational contains a valid value
      * @param oldSystemIsOperational The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void systemIsOperationalUpdated(HlaSovietIFFPtr sovietIFF, bool systemIsOperational, bool validOldSystemIsOperational, bool oldSystemIsOperational, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>entityIdentifier</code> is updated.
      * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @param sovietIFF The object which is updated.
      * @param entityIdentifier The new value of the attribute in this update
      * @param validOldEntityIdentifier True if oldEntityIdentifier contains a valid value
      * @param oldEntityIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void entityIdentifierUpdated(HlaSovietIFFPtr sovietIFF, EntityIdentifierStruct entityIdentifier, bool validOldEntityIdentifier, EntityIdentifierStruct oldEntityIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>hostObjectIdentifier</code> is updated.
      * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @param sovietIFF The object which is updated.
      * @param hostObjectIdentifier The new value of the attribute in this update
      * @param validOldHostObjectIdentifier True if oldHostObjectIdentifier contains a valid value
      * @param oldHostObjectIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void hostObjectIdentifierUpdated(HlaSovietIFFPtr sovietIFF, std::string hostObjectIdentifier, bool validOldHostObjectIdentifier, std::string oldHostObjectIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>relativePosition</code> is updated.
      * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
      * <br>Description of the data type from the FOM: <i>Relative position in right-handed Cartesian coordinates.</i>
      *
      * @param sovietIFF The object which is updated.
      * @param relativePosition The new value of the attribute in this update
      * @param validOldRelativePosition True if oldRelativePosition contains a valid value
      * @param oldRelativePosition The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void relativePositionUpdated(HlaSovietIFFPtr sovietIFF, RelativePositionStruct relativePosition, bool validOldRelativePosition, RelativePositionStruct oldRelativePosition, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
      /**
      * This method is called when the producing federate of an attribute is changed.
      * Only available when using HLA Evolved.
      *
      * @param sovietIFF The object which is updated.
      * @param attribute The attribute that now has a new producing federate
      * @param oldProducingFederate The federate handle of the old producing federate
      * @param newProducingFederate The federate handle of the new producing federate
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void producingFederateUpdated(HlaSovietIFFPtr sovietIFF, HlaSovietIFFAttributes::Attribute attribute, HlaFederateIdPtr oldProducingFederate, HlaFederateIdPtr newProducingFederate, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;

      class Adapter;
   };

   /**
   * An adapter class that implements the HlaSovietIFFValueListener interface with empty methods.
   * It might be used as a base class for a listener.
   */
   class HlaSovietIFFValueListener::Adapter : public HlaSovietIFFValueListener {

   public:

      LIBAPI virtual void beamAzimuthCenterUpdated(HlaSovietIFFPtr sovietIFF, float beamAzimuthCenter, bool validOldBeamAzimuthCenter, float oldBeamAzimuthCenter, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void beamAzimuthSweepUpdated(HlaSovietIFFPtr sovietIFF, float beamAzimuthSweep, bool validOldBeamAzimuthSweep, float oldBeamAzimuthSweep, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void beamElevationCenterUpdated(HlaSovietIFFPtr sovietIFF, float beamElevationCenter, bool validOldBeamElevationCenter, float oldBeamElevationCenter, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void beamElevationSweepUpdated(HlaSovietIFFPtr sovietIFF, float beamElevationSweep, bool validOldBeamElevationSweep, float oldBeamElevationSweep, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void beamSweepSyncUpdated(HlaSovietIFFPtr sovietIFF, float beamSweepSync, bool validOldBeamSweepSync, float oldBeamSweepSync, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void eventIdentifierUpdated(HlaSovietIFFPtr sovietIFF, EventIdentifierStruct eventIdentifier, bool validOldEventIdentifier, EventIdentifierStruct oldEventIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void fundamentalParameterDataUpdated(HlaSovietIFFPtr sovietIFF, std::vector<DevStudio::FundamentalParameterDataStruct > fundamentalParameterData, bool validOldFundamentalParameterData, std::vector<DevStudio::FundamentalParameterDataStruct > oldFundamentalParameterData, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void layer2DataAvailableUpdated(HlaSovietIFFPtr sovietIFF, bool layer2DataAvailable, bool validOldLayer2DataAvailable, bool oldLayer2DataAvailable, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void secondaryOperationalDataParameter1Updated(HlaSovietIFFPtr sovietIFF, IffOperationalParameter1Enum::IffOperationalParameter1Enum secondaryOperationalDataParameter1, bool validOldSecondaryOperationalDataParameter1, IffOperationalParameter1Enum::IffOperationalParameter1Enum oldSecondaryOperationalDataParameter1, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void secondaryOperationalDataParameter2Updated(HlaSovietIFFPtr sovietIFF, IffOperationalParameter2Enum::IffOperationalParameter2Enum secondaryOperationalDataParameter2, bool validOldSecondaryOperationalDataParameter2, IffOperationalParameter2Enum::IffOperationalParameter2Enum oldSecondaryOperationalDataParameter2, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void systemModeUpdated(HlaSovietIFFPtr sovietIFF, IffSystemModeEnum::IffSystemModeEnum systemMode, bool validOldSystemMode, IffSystemModeEnum::IffSystemModeEnum oldSystemMode, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void systemNameUpdated(HlaSovietIFFPtr sovietIFF, IffSystemNameEnum::IffSystemNameEnum systemName, bool validOldSystemName, IffSystemNameEnum::IffSystemNameEnum oldSystemName, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void systemTypeUpdated(HlaSovietIFFPtr sovietIFF, IffSystemTypeEnum::IffSystemTypeEnum systemType, bool validOldSystemType, IffSystemTypeEnum::IffSystemTypeEnum oldSystemType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void systemIsOnUpdated(HlaSovietIFFPtr sovietIFF, bool systemIsOn, bool validOldSystemIsOn, bool oldSystemIsOn, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void systemIsOperationalUpdated(HlaSovietIFFPtr sovietIFF, bool systemIsOperational, bool validOldSystemIsOperational, bool oldSystemIsOperational, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void entityIdentifierUpdated(HlaSovietIFFPtr sovietIFF, EntityIdentifierStruct entityIdentifier, bool validOldEntityIdentifier, EntityIdentifierStruct oldEntityIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void hostObjectIdentifierUpdated(HlaSovietIFFPtr sovietIFF, std::string hostObjectIdentifier, bool validOldHostObjectIdentifier, std::string oldHostObjectIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void relativePositionUpdated(HlaSovietIFFPtr sovietIFF, RelativePositionStruct relativePosition, bool validOldRelativePosition, RelativePositionStruct oldRelativePosition, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
      LIBAPI virtual void producingFederateUpdated(HlaSovietIFFPtr sovietIFF, HlaSovietIFFAttributes::Attribute attribute, HlaFederateIdPtr oldProducingFederate, HlaFederateIdPtr newProducingFederate, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
   };

}
#endif
