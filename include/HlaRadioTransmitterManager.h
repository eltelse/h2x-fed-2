/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLARADIOTRANSMITTERMANAGER_H
#define DEVELOPER_STUDIO_HLARADIOTRANSMITTERMANAGER_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <DevStudio/datatypes/AntennaPatternVariantStruct.h>
#include <DevStudio/datatypes/AntennaPatternVariantStructLengthlessArray.h>
#include <DevStudio/datatypes/CryptographicModeEnum.h>
#include <DevStudio/datatypes/CryptographicSystemTypeEnum.h>
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/RFModulationSystemTypeEnum.h>
#include <DevStudio/datatypes/RFModulationTypeVariantStruct.h>
#include <DevStudio/datatypes/RadioInputSourceEnum.h>
#include <DevStudio/datatypes/RadioTypeStruct.h>
#include <DevStudio/datatypes/RelativePositionStruct.h>
#include <DevStudio/datatypes/SpreadSpectrumVariantStruct.h>
#include <DevStudio/datatypes/TransmitterOperationalStatusEnum.h>
#include <DevStudio/datatypes/WorldLocationStruct.h>
#include <string>
#include <vector>

#include <list>
#include <vector>

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaRadioTransmitterManagerListener.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaException.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

   /**
   * Manager used to manage HlaRadioTransmitters.
   */
   class HlaRadioTransmitterManager : private boost::noncopyable {

   public:

      LIBAPI virtual ~HlaRadioTransmitterManager() {}

      /**
      * Gets a list of all HlaRadioTransmitters within interest, both local and remote.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of HlaRadioTransmitters
      */
      LIBAPI virtual std::list<HlaRadioTransmitterPtr> getHlaRadioTransmitters() = 0;

      /**
      * Gets a list of all HlaRadioTransmitters, both local and remote.
      * HlaRadioTransmitters not within interest are also included.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of all HlaRadioTransmitters
      */
      LIBAPI virtual std::list<HlaRadioTransmitterPtr> getAllHlaRadioTransmitters() = 0;

      /**
      * Gets a list of local HlaRadioTransmitters within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of local HlaRadioTransmitters
      */
      LIBAPI virtual std::list<HlaRadioTransmitterPtr> getLocalHlaRadioTransmitters() = 0;

      /**
      * Gets a list of remote HlaRadioTransmitters within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of remote HlaRadioTransmitters
      */
      LIBAPI virtual std::list<HlaRadioTransmitterPtr> getRemoteHlaRadioTransmitters() = 0;

      /**
      * Find a HlaRadioTransmitter with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> of the HlaRadioTransmitter to find
      *
      * @return the specified HlaRadioTransmitter, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaRadioTransmitterPtr getRadioTransmitterByHlaInstanceName(const std::wstring& hlaInstanceName) = 0;

      /**
      * Find a HlaRadioTransmitter with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param encodedHlaInstanceHandle the encoded <code>HLA instance handle</code> of the HlaRadioTransmitter to find
      *
      * @return the specified HlaRadioTransmitter, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaRadioTransmitterPtr getRadioTransmitterByHlaInstanceHandle(const std::vector<char>& encodedHlaInstanceHandle) = 0;

      /**
      * Creates a new HlaRadioTransmitter, and sets all <i>Create</i> attributes.
      *
      * @return Returns a new local HlaRadioTransmitter.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaRadioTransmitterPtr createLocalHlaRadioTransmitter(
      ) THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Creates a new HlaRadioTransmitter with a specified <code>HLA instance name</code>, and sets all
      * <i>Create</i> attributes.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> to register for this instance
      *
      * @return Returns a new local HlaRadioTransmitter.
      *
      * @throws HlaIllegalInstanceNameException if the name is illegal
      * @throws HlaInstanceNameInUseException if the name is already in use
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaRadioTransmitterPtr createLocalHlaRadioTransmitter(
         const std::wstring& hlaInstanceName
      ) THROW_SPEC (HlaIllegalInstanceNameException, HlaInstanceNameInUseException,
                    HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaRadioTransmitter and removes it from the federation.
      *
      * @param radioTransmitter The HlaRadioTransmitter to delete
      *
      * @return the HlaRadioTransmitter deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaRadioTransmitterPtr deleteLocalHlaRadioTransmitter(HlaRadioTransmitterPtr radioTransmitter)
          THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                      HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaRadioTransmitter and removes it from the federation.
      *
      * @param radioTransmitter The HlaRadioTransmitter to delete
      * @param timestamp timestamp when the instance was removed.
      *
      * @return the HlaRadioTransmitter deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaRadioTransmitterPtr deleteLocalHlaRadioTransmitter(HlaRadioTransmitterPtr radioTransmitter, HlaTimeStampPtr timestamp)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaRadioTransmitter and removes it from the federation.
      *
      * @param radioTransmitter The HlaRadioTransmitter to delete
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaRadioTransmitter deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaRadioTransmitterPtr deleteLocalHlaRadioTransmitter(HlaRadioTransmitterPtr radioTransmitter, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaRadioTransmitter and removes it from the federation.
      *
      * @param radioTransmitter The HlaRadioTransmitter to delete
      * @param timestamp timestamp when the instance was removed.
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaRadioTransmitter deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaRadioTransmitterPtr deleteLocalHlaRadioTransmitter(HlaRadioTransmitterPtr radioTransmitter, HlaTimeStampPtr timestamp, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Add a HlaRadioTransmitter manager listener.
      *
      * @param managerListener listener to add
      */
      LIBAPI virtual void addHlaRadioTransmitterManagerListener(HlaRadioTransmitterManagerListenerPtr managerListener) = 0;

      /**
      * Removed a HlaRadioTransmitter manager listener.
      *
      * @param managerListener listener to remove
      */
      LIBAPI virtual void removeHlaRadioTransmitterManagerListener(HlaRadioTransmitterManagerListenerPtr managerListener) = 0;

     /**
      * Add a listener that will automatically be added to all instances of HlaRadioTransmitter (Both local and remote).
      * The listener is notified when any attribute of an instance of HlaRadioTransmitter is updated.
      * The listener is also called when an interaction is sent to an instance of HlaRadioTransmitter.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param listener The listener to add.
      */
      LIBAPI virtual void addHlaRadioTransmitterDefaultInstanceListener(HlaRadioTransmitterListenerPtr listener) = 0;

     /**
      * Remove a listener from the set of listeners that are automatically added to new instances of HlaRadioTransmitter.
      * Note: The listener will not be removed from already existing instances of HlaRadioTransmitter.
      * This method is idempotent.
      *
      * @param listener The listener to remove.
      */
      LIBAPI virtual void removeHlaRadioTransmitterDefaultInstanceListener(HlaRadioTransmitterListenerPtr listener) = 0;

     /**
      * Add a valueListener that will automatically be added to all instances of HlaRadioTransmitter (Both local and remote).
      * The  Valuelistener is notified when any attribute of an instance of HlaRadioTransmitter is updated.
      * Note that the attribute values that are notified are not guaranteed to
      * be the latest available values for that attribute.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to add.
      */
      LIBAPI virtual void addHlaRadioTransmitterDefaultInstanceValueListener(HlaRadioTransmitterValueListenerPtr valueListener) = 0;

     /**
      * Remove a valueListener from the set of listeners that are automatically added to new instances of HlaRadioTransmitter.
      * Note: The valueListener will not be removed from already existing instances of HlaRadioTransmitter.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to remove.
      */
      LIBAPI virtual void removeHlaRadioTransmitterDefaultInstanceValueListener(HlaRadioTransmitterValueListenerPtr valueListener) = 0;

     /**
      * Enables or disables the HlaRadioTransmitter manager.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaRadioTransmitter manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      *
      * @param enabled <code>true</code> to enable the HlaRadioTransmitter manager, otherwise <code>false</code>
      */
      LIBAPI virtual void setEnabled(bool enabled) = 0;

     /**
      * Enables the HlaRadioTransmitter manager if it is avalable in the FOM at runtime.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaRadioTransmitter manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      */
      LIBAPI virtual void setEnabledIfAvailableInFom() = 0;

     /**
      * Check if the HlaRadioTransmitter manager is actually enabled when connected.
      * An HlaRadioTransmitter manager may be disabled with the <code>setEnabled(false)</code> function, or it may be configured
      * as <i>if available in the FOM</i> by the <code>setEnabledIfAvailableInFom()</code> function and
      * then not available in the FOM at runtime.
      * <p>
      * This method should only be used when connected, it will always return <code>false</code>
      * when not connected.
      *
      * @return <code>true</code> if connected and the HlaRadioTransmitter manager is enabled.
      */
      LIBAPI virtual bool isEnabled() = 0;
   };
}
#endif
