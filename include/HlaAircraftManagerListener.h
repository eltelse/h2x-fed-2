/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAAIRCRAFTMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLAAIRCRAFTMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaAircraft.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaAircraft instances.
    */
    class HlaAircraftManagerListener {

    public:

        LIBAPI virtual ~HlaAircraftManagerListener() {}

        /**
        * This method is called when a new HlaAircraft instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param aircraft the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaAircraftDiscovered(HlaAircraftPtr aircraft, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaAircraft instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param aircraft the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaAircraftInitialized(HlaAircraftPtr aircraft, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaAircraftManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param aircraft the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaAircraftInInterest(HlaAircraftPtr aircraft, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaAircraftManagerListener instance goes out of interest.
        *
        * @param aircraft the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaAircraftOutOfInterest(HlaAircraftPtr aircraft, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaAircraft instance is deleted.
        *
        * @param aircraft the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaAircraftDeleted(HlaAircraftPtr aircraft, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaAircraftManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaAircraftManagerListener::Adapter : public HlaAircraftManagerListener {

    public:
        LIBAPI virtual void hlaAircraftDiscovered(HlaAircraftPtr aircraft, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaAircraftInitialized(HlaAircraftPtr aircraft, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaAircraftInInterest(HlaAircraftPtr aircraft, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaAircraftOutOfInterest(HlaAircraftPtr aircraft, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaAircraftDeleted(HlaAircraftPtr aircraft, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
