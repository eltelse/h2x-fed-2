/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLASOVIETIFFTRANSPONDERUPDATER_H
#define DEVELOPER_STUDIO_HLASOVIETIFFTRANSPONDERUPDATER_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <boost/noncopyable.hpp>

#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EventIdentifierStruct.h>
#include <DevStudio/datatypes/FundamentalParameterDataStruct.h>
#include <DevStudio/datatypes/FundamentalParameterDataStructLengthlessArray.h>
#include <DevStudio/datatypes/IffOperationalParameter1Enum.h>
#include <DevStudio/datatypes/IffOperationalParameter2Enum.h>
#include <DevStudio/datatypes/IffSystemModeEnum.h>
#include <DevStudio/datatypes/IffSystemNameEnum.h>
#include <DevStudio/datatypes/IffSystemTypeEnum.h>
#include <DevStudio/datatypes/RelativePositionStruct.h>
#include <string>
#include <vector>

#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaException.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaSovietIFFUpdater.h>

namespace DevStudio {

    /**
    * Updater used to update attribute values.
    */
    class HlaSovietIFFTransponderUpdater : public HlaSovietIFFUpdater {

    public:

    LIBAPI virtual ~HlaSovietIFFTransponderUpdater() {}

    /**
    * Set the beamAzimuthCenter for this update.
    * <br>Description from the FOM: <i>The azimuth center of the IFF beam's scan volume relative to the IFF system.</i>
    * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
    *
    * @param beamAzimuthCenter the new beamAzimuthCenter
    */
    LIBAPI virtual void setBeamAzimuthCenter(const float& beamAzimuthCenter) = 0;

    /**
    * Set the beamAzimuthSweep for this update.
    * <br>Description from the FOM: <i>The azimuth half-angle of the IFF beam's scan volume relative to the IFF system.</i>
    * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
    *
    * @param beamAzimuthSweep the new beamAzimuthSweep
    */
    LIBAPI virtual void setBeamAzimuthSweep(const float& beamAzimuthSweep) = 0;

    /**
    * Set the beamElevationCenter for this update.
    * <br>Description from the FOM: <i>The elevation center of the IFF beam's scan volume relative to the IFF system.</i>
    * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
    *
    * @param beamElevationCenter the new beamElevationCenter
    */
    LIBAPI virtual void setBeamElevationCenter(const float& beamElevationCenter) = 0;

    /**
    * Set the beamElevationSweep for this update.
    * <br>Description from the FOM: <i>The elevation half-angle of the IFF beam's scan volume relative to the IFF system.</i>
    * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
    *
    * @param beamElevationSweep the new beamElevationSweep
    */
    LIBAPI virtual void setBeamElevationSweep(const float& beamElevationSweep) = 0;

    /**
    * Set the beamSweepSync for this update.
    * <br>Description from the FOM: <i>The percentage of time a scan is through its pattern from its origin.</i>
    * <br>Description of the data type from the FOM: <i>Percentage [unit: percent (%), resolution: NA, accuracy: NA]</i>
    *
    * @param beamSweepSync the new beamSweepSync
    */
    LIBAPI virtual void setBeamSweepSync(const float& beamSweepSync) = 0;

    /**
    * Set the eventIdentifier for this update.
    * <br>Description from the FOM: <i>Used to associate related events.</i>
    * <br>Description of the data type from the FOM: <i>Identification of an event. Based on the Event Identifier record as specified in IEEE 1278.1-1995 section 5.2.18.</i>
    *
    * @param eventIdentifier the new eventIdentifier
    */
    LIBAPI virtual void setEventIdentifier(const DevStudio::EventIdentifierStruct& eventIdentifier) = 0;

    /**
    * Set the fundamentalParameterData for this update.
    * <br>Description from the FOM: <i>The fundamental energy radiation characteristics of the IFF/ATC/NAVAIDS system.</i>
    * <br>Description of the data type from the FOM: <i>Array of Fundamental Parameter Data records.</i>
    *
    * @param fundamentalParameterData the new fundamentalParameterData
    */
    LIBAPI virtual void setFundamentalParameterData(const std::vector<DevStudio::FundamentalParameterDataStruct >& fundamentalParameterData) = 0;

    /**
    * Set the layer2DataAvailable for this update.
    * <br>Description from the FOM: <i>Specifies if level 2 data is available for this IFF system. If level 2 data is available then the BeamAzimuthCenter, BeamAzimuthSweep, BeamElevationCenter, BeamElevationSweep, BeamSweepSync, FundamentalParameterData, SecondaryOperationalDataParameter1, and SecondaryOperationalDataParameter2 attributes shall be generated.</i>
    * <br>Description of the data type from the FOM: <i></i>
    *
    * @param layer2DataAvailable the new layer2DataAvailable
    */
    LIBAPI virtual void setLayer2DataAvailable(const bool& layer2DataAvailable) = 0;

    /**
    * Set the secondaryOperationalDataParameter1 for this update.
    * <br>Description from the FOM: <i>Additional characteristics of the IFF/ATC/NAVAIDS emitting system.</i>
    * <br>Description of the data type from the FOM: <i>IFF operational parameter 1</i>
    *
    * @param secondaryOperationalDataParameter1 the new secondaryOperationalDataParameter1
    */
    LIBAPI virtual void setSecondaryOperationalDataParameter1(const DevStudio::IffOperationalParameter1Enum::IffOperationalParameter1Enum& secondaryOperationalDataParameter1) = 0;

    /**
    * Set the secondaryOperationalDataParameter2 for this update.
    * <br>Description from the FOM: <i>Additional characteristics of the IFF/ATC/NAVAIDS emitting system.</i>
    * <br>Description of the data type from the FOM: <i>IFF operational parameter 2</i>
    *
    * @param secondaryOperationalDataParameter2 the new secondaryOperationalDataParameter2
    */
    LIBAPI virtual void setSecondaryOperationalDataParameter2(const DevStudio::IffOperationalParameter2Enum::IffOperationalParameter2Enum& secondaryOperationalDataParameter2) = 0;

    /**
    * Set the systemMode for this update.
    * <br>Description from the FOM: <i>Mode of operation.</i>
    * <br>Description of the data type from the FOM: <i>IFF system mode</i>
    *
    * @param systemMode the new systemMode
    */
    LIBAPI virtual void setSystemMode(const DevStudio::IffSystemModeEnum::IffSystemModeEnum& systemMode) = 0;

    /**
    * Set the systemName for this update.
    * <br>Description from the FOM: <i>Particular named type of the IFF system in use.</i>
    * <br>Description of the data type from the FOM: <i>IFF system name</i>
    *
    * @param systemName the new systemName
    */
    LIBAPI virtual void setSystemName(const DevStudio::IffSystemNameEnum::IffSystemNameEnum& systemName) = 0;

    /**
    * Set the systemType for this update.
    * <br>Description from the FOM: <i>General type of IFF system in use.</i>
    * <br>Description of the data type from the FOM: <i>IFF system type</i>
    *
    * @param systemType the new systemType
    */
    LIBAPI virtual void setSystemType(const DevStudio::IffSystemTypeEnum::IffSystemTypeEnum& systemType) = 0;

    /**
    * Set the systemIsOn for this update.
    * <br>Description from the FOM: <i>Whether or not the system is on.</i>
    * <br>Description of the data type from the FOM: <i></i>
    *
    * @param systemIsOn the new systemIsOn
    */
    LIBAPI virtual void setSystemIsOn(const bool& systemIsOn) = 0;

    /**
    * Set the systemIsOperational for this update.
    * <br>Description from the FOM: <i>Whether or not the system is operational.</i>
    * <br>Description of the data type from the FOM: <i></i>
    *
    * @param systemIsOperational the new systemIsOperational
    */
    LIBAPI virtual void setSystemIsOperational(const bool& systemIsOperational) = 0;

    /**
    * Set the entityIdentifier for this update.
    * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
    * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
    *
    * @param entityIdentifier the new entityIdentifier
    */
    LIBAPI virtual void setEntityIdentifier(const DevStudio::EntityIdentifierStruct& entityIdentifier) = 0;

    /**
    * Set the hostObjectIdentifier for this update.
    * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
    * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
    *
    * @param hostObjectIdentifier the new hostObjectIdentifier
    */
    LIBAPI virtual void setHostObjectIdentifier(const std::string& hostObjectIdentifier) = 0;

    /**
    * Set the relativePosition for this update.
    * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
    * <br>Description of the data type from the FOM: <i>Relative position in right-handed Cartesian coordinates.</i>
    *
    * @param relativePosition the new relativePosition
    */
    LIBAPI virtual void setRelativePosition(const DevStudio::RelativePositionStruct& relativePosition) = 0;

    /**
    * Send all the attributes.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate()
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

    /**
    * Send all the attributes with a specified timestamp.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @param timestamp timestamp to send for this update
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate(HlaTimeStampPtr timestamp)
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

    /**
    * Send all the attributes with a specified timestamp.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @param logicalTime logical time to send for this update
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaInvalidLogicalTimeException if the logical time is not valid.
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate(HlaLogicalTimePtr logicalTime)
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaInvalidLogicalTimeException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

    /**
    * Send all the attributes with a specified timestamp.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @param timestamp timestamp to send for this update
    * @param logicalTime logical time to send for this update
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaInvalidLogicalTimeException if the logical time is not valid.
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate(HlaTimeStampPtr timestamp, HlaLogicalTimePtr logicalTime)
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaInvalidLogicalTimeException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;
    };
}
#endif
