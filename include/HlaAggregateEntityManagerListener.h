/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAAGGREGATEENTITYMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLAAGGREGATEENTITYMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaAggregateEntity.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaAggregateEntity instances.
    */
    class HlaAggregateEntityManagerListener {

    public:

        LIBAPI virtual ~HlaAggregateEntityManagerListener() {}

        /**
        * This method is called when a new HlaAggregateEntity instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param aggregateEntity the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaAggregateEntityDiscovered(HlaAggregateEntityPtr aggregateEntity, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaAggregateEntity instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param aggregateEntity the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaAggregateEntityInitialized(HlaAggregateEntityPtr aggregateEntity, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaAggregateEntityManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param aggregateEntity the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaAggregateEntityInInterest(HlaAggregateEntityPtr aggregateEntity, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaAggregateEntityManagerListener instance goes out of interest.
        *
        * @param aggregateEntity the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaAggregateEntityOutOfInterest(HlaAggregateEntityPtr aggregateEntity, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaAggregateEntity instance is deleted.
        *
        * @param aggregateEntity the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaAggregateEntityDeleted(HlaAggregateEntityPtr aggregateEntity, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaAggregateEntityManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaAggregateEntityManagerListener::Adapter : public HlaAggregateEntityManagerListener {

    public:
        LIBAPI virtual void hlaAggregateEntityDiscovered(HlaAggregateEntityPtr aggregateEntity, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaAggregateEntityInitialized(HlaAggregateEntityPtr aggregateEntity, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaAggregateEntityInInterest(HlaAggregateEntityPtr aggregateEntity, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaAggregateEntityOutOfInterest(HlaAggregateEntityPtr aggregateEntity, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaAggregateEntityDeleted(HlaAggregateEntityPtr aggregateEntity, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
