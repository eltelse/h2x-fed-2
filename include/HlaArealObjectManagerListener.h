/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAAREALOBJECTMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLAAREALOBJECTMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaArealObject.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaArealObject instances.
    */
    class HlaArealObjectManagerListener {

    public:

        LIBAPI virtual ~HlaArealObjectManagerListener() {}

        /**
        * This method is called when a new HlaArealObject instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param arealObject the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaArealObjectDiscovered(HlaArealObjectPtr arealObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaArealObject instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param arealObject the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaArealObjectInitialized(HlaArealObjectPtr arealObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaArealObjectManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param arealObject the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaArealObjectInInterest(HlaArealObjectPtr arealObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaArealObjectManagerListener instance goes out of interest.
        *
        * @param arealObject the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaArealObjectOutOfInterest(HlaArealObjectPtr arealObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaArealObject instance is deleted.
        *
        * @param arealObject the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaArealObjectDeleted(HlaArealObjectPtr arealObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaArealObjectManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaArealObjectManagerListener::Adapter : public HlaArealObjectManagerListener {

    public:
        LIBAPI virtual void hlaArealObjectDiscovered(HlaArealObjectPtr arealObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaArealObjectInitialized(HlaArealObjectPtr arealObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaArealObjectInInterest(HlaArealObjectPtr arealObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaArealObjectOutOfInterest(HlaArealObjectPtr arealObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaArealObjectDeleted(HlaArealObjectPtr arealObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
