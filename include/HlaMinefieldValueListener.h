/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAMINEFIELDVALUELISTENER_H
#define DEVELOPER_STUDIO_HLAMINEFIELDVALUELISTENER_H

#include <memory>

#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EntityTypeStruct.h>
#include <DevStudio/datatypes/EntityTypeStructLengthlessArray.h>
#include <DevStudio/datatypes/ForceIdentifierEnum.h>
#include <DevStudio/datatypes/MinefieldLaneEnum.h>
#include <DevStudio/datatypes/MinefieldProtocolEnum.h>
#include <DevStudio/datatypes/MinefieldStatusEnum.h>
#include <DevStudio/datatypes/MinefieldTypeEnum.h>
#include <DevStudio/datatypes/OrientationStruct.h>
#include <DevStudio/datatypes/PerimeterPointStruct.h>
#include <DevStudio/datatypes/PerimeterPointStructLengthlessArray.h>
#include <DevStudio/datatypes/WorldLocationStruct.h>
#include <vector>

#include <DevStudio/HlaLogicalTime.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaMinefieldAttributes.h>    

namespace DevStudio {

   /**
   * Listener for updates of attributes, with the new updated values.  
   */
   class HlaMinefieldValueListener {

   public:

      LIBAPI virtual ~HlaMinefieldValueListener() {}
    
      /**
      * This method is called when the attribute <code>activeStatus</code> is updated.
      * <br>Description from the FOM: <i>Specifies the active status of the minefield</i>
      * <br>Description of the data type from the FOM: <i>Minefield status</i>
      *
      * @param minefield The object which is updated.
      * @param activeStatus The new value of the attribute in this update
      * @param validOldActiveStatus True if oldActiveStatus contains a valid value
      * @param oldActiveStatus The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void activeStatusUpdated(HlaMinefieldPtr minefield, MinefieldStatusEnum::MinefieldStatusEnum activeStatus, bool validOldActiveStatus, MinefieldStatusEnum::MinefieldStatusEnum oldActiveStatus, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>forceIdentifier</code> is updated.
      * <br>Description from the FOM: <i>Identifies the force to which the minefield belongs</i>
      * <br>Description of the data type from the FOM: <i>Force ID</i>
      *
      * @param minefield The object which is updated.
      * @param forceIdentifier The new value of the attribute in this update
      * @param validOldForceIdentifier True if oldForceIdentifier contains a valid value
      * @param oldForceIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void forceIdentifierUpdated(HlaMinefieldPtr minefield, ForceIdentifierEnum::ForceIdentifierEnum forceIdentifier, bool validOldForceIdentifier, ForceIdentifierEnum::ForceIdentifierEnum oldForceIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>lane</code> is updated.
      * <br>Description from the FOM: <i>Specifies whether or not the minefield has an active lane</i>
      * <br>Description of the data type from the FOM: <i>Minefield lane status</i>
      *
      * @param minefield The object which is updated.
      * @param lane The new value of the attribute in this update
      * @param validOldLane True if oldLane contains a valid value
      * @param oldLane The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void laneUpdated(HlaMinefieldPtr minefield, MinefieldLaneEnum::MinefieldLaneEnum lane, bool validOldLane, MinefieldLaneEnum::MinefieldLaneEnum oldLane, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>minefieldAppearanceType</code> is updated.
      * <br>Description from the FOM: <i>Specifies the appearance information needed for displaying the symbology of the minefield</i>
      * <br>Description of the data type from the FOM: <i>Minefield type</i>
      *
      * @param minefield The object which is updated.
      * @param minefieldAppearanceType The new value of the attribute in this update
      * @param validOldMinefieldAppearanceType True if oldMinefieldAppearanceType contains a valid value
      * @param oldMinefieldAppearanceType The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void minefieldAppearanceTypeUpdated(HlaMinefieldPtr minefield, MinefieldTypeEnum::MinefieldTypeEnum minefieldAppearanceType, bool validOldMinefieldAppearanceType, MinefieldTypeEnum::MinefieldTypeEnum oldMinefieldAppearanceType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>minefieldIdentifier</code> is updated.
      * <br>Description from the FOM: <i>Uniquely identifies this minefield instance in association with the federate's site and application</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @param minefield The object which is updated.
      * @param minefieldIdentifier The new value of the attribute in this update
      * @param validOldMinefieldIdentifier True if oldMinefieldIdentifier contains a valid value
      * @param oldMinefieldIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void minefieldIdentifierUpdated(HlaMinefieldPtr minefield, EntityIdentifierStruct minefieldIdentifier, bool validOldMinefieldIdentifier, EntityIdentifierStruct oldMinefieldIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>minefieldLocation</code> is updated.
      * <br>Description from the FOM: <i>Specifies the location of the center of the minefield</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @param minefield The object which is updated.
      * @param minefieldLocation The new value of the attribute in this update
      * @param validOldMinefieldLocation True if oldMinefieldLocation contains a valid value
      * @param oldMinefieldLocation The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void minefieldLocationUpdated(HlaMinefieldPtr minefield, WorldLocationStruct minefieldLocation, bool validOldMinefieldLocation, WorldLocationStruct oldMinefieldLocation, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>minefieldOrientation</code> is updated.
      * <br>Description from the FOM: <i>Specifies the orientation of the minefield, with Euler angles</i>
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @param minefield The object which is updated.
      * @param minefieldOrientation The new value of the attribute in this update
      * @param validOldMinefieldOrientation True if oldMinefieldOrientation contains a valid value
      * @param oldMinefieldOrientation The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void minefieldOrientationUpdated(HlaMinefieldPtr minefield, OrientationStruct minefieldOrientation, bool validOldMinefieldOrientation, OrientationStruct oldMinefieldOrientation, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>minefieldType</code> is updated.
      * <br>Description from the FOM: <i>Specifies the minefield type</i>
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      *
      * @param minefield The object which is updated.
      * @param minefieldType The new value of the attribute in this update
      * @param validOldMinefieldType True if oldMinefieldType contains a valid value
      * @param oldMinefieldType The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void minefieldTypeUpdated(HlaMinefieldPtr minefield, EntityTypeStruct minefieldType, bool validOldMinefieldType, EntityTypeStruct oldMinefieldType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>mineTypes</code> is updated.
      * <br>Description from the FOM: <i>Specifies the type of each mine contained within the minefield</i>
      * <br>Description of the data type from the FOM: <i>Dynamic array of EntityTypeStruct elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
      *
      * @param minefield The object which is updated.
      * @param mineTypes The new value of the attribute in this update
      * @param validOldMineTypes True if oldMineTypes contains a valid value
      * @param oldMineTypes The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void mineTypesUpdated(HlaMinefieldPtr minefield, std::vector<DevStudio::EntityTypeStruct > mineTypes, bool validOldMineTypes, std::vector<DevStudio::EntityTypeStruct > oldMineTypes, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>perimeterPointCoordinates</code> is updated.
      * <br>Description from the FOM: <i>Specifies the location of each perimeter point, relative to the minefield location</i>
      * <br>Description of the data type from the FOM: <i>Specifies the location of perimeter points (collection)</i>
      *
      * @param minefield The object which is updated.
      * @param perimeterPointCoordinates The new value of the attribute in this update
      * @param validOldPerimeterPointCoordinates True if oldPerimeterPointCoordinates contains a valid value
      * @param oldPerimeterPointCoordinates The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void perimeterPointCoordinatesUpdated(HlaMinefieldPtr minefield, std::vector<DevStudio::PerimeterPointStruct > perimeterPointCoordinates, bool validOldPerimeterPointCoordinates, std::vector<DevStudio::PerimeterPointStruct > oldPerimeterPointCoordinates, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>protocolMode</code> is updated.
      * <br>Description from the FOM: <i>Specifies the mode (Heartbeat or Query Response Protocol) being used to communicate data about the minefield</i>
      * <br>Description of the data type from the FOM: <i>Minefield communication protocol mode</i>
      *
      * @param minefield The object which is updated.
      * @param protocolMode The new value of the attribute in this update
      * @param validOldProtocolMode True if oldProtocolMode contains a valid value
      * @param oldProtocolMode The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void protocolModeUpdated(HlaMinefieldPtr minefield, MinefieldProtocolEnum::MinefieldProtocolEnum protocolMode, bool validOldProtocolMode, MinefieldProtocolEnum::MinefieldProtocolEnum oldProtocolMode, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>state</code> is updated.
      * <br>Description from the FOM: <i>Specifies whether or not the minefield has been deactivated</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param minefield The object which is updated.
      * @param state The new value of the attribute in this update
      * @param validOldState True if oldState contains a valid value
      * @param oldState The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void stateUpdated(HlaMinefieldPtr minefield, bool state, bool validOldState, bool oldState, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
      /**
      * This method is called when the producing federate of an attribute is changed.
      * Only available when using HLA Evolved.
      *
      * @param minefield The object which is updated.
      * @param attribute The attribute that now has a new producing federate
      * @param oldProducingFederate The federate handle of the old producing federate
      * @param newProducingFederate The federate handle of the new producing federate
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void producingFederateUpdated(HlaMinefieldPtr minefield, HlaMinefieldAttributes::Attribute attribute, HlaFederateIdPtr oldProducingFederate, HlaFederateIdPtr newProducingFederate, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;

      class Adapter;
   };

   /**
   * An adapter class that implements the HlaMinefieldValueListener interface with empty methods.
   * It might be used as a base class for a listener.
   */
   class HlaMinefieldValueListener::Adapter : public HlaMinefieldValueListener {

   public:

      LIBAPI virtual void activeStatusUpdated(HlaMinefieldPtr minefield, MinefieldStatusEnum::MinefieldStatusEnum activeStatus, bool validOldActiveStatus, MinefieldStatusEnum::MinefieldStatusEnum oldActiveStatus, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void forceIdentifierUpdated(HlaMinefieldPtr minefield, ForceIdentifierEnum::ForceIdentifierEnum forceIdentifier, bool validOldForceIdentifier, ForceIdentifierEnum::ForceIdentifierEnum oldForceIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void laneUpdated(HlaMinefieldPtr minefield, MinefieldLaneEnum::MinefieldLaneEnum lane, bool validOldLane, MinefieldLaneEnum::MinefieldLaneEnum oldLane, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void minefieldAppearanceTypeUpdated(HlaMinefieldPtr minefield, MinefieldTypeEnum::MinefieldTypeEnum minefieldAppearanceType, bool validOldMinefieldAppearanceType, MinefieldTypeEnum::MinefieldTypeEnum oldMinefieldAppearanceType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void minefieldIdentifierUpdated(HlaMinefieldPtr minefield, EntityIdentifierStruct minefieldIdentifier, bool validOldMinefieldIdentifier, EntityIdentifierStruct oldMinefieldIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void minefieldLocationUpdated(HlaMinefieldPtr minefield, WorldLocationStruct minefieldLocation, bool validOldMinefieldLocation, WorldLocationStruct oldMinefieldLocation, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void minefieldOrientationUpdated(HlaMinefieldPtr minefield, OrientationStruct minefieldOrientation, bool validOldMinefieldOrientation, OrientationStruct oldMinefieldOrientation, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void minefieldTypeUpdated(HlaMinefieldPtr minefield, EntityTypeStruct minefieldType, bool validOldMinefieldType, EntityTypeStruct oldMinefieldType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void mineTypesUpdated(HlaMinefieldPtr minefield, std::vector<DevStudio::EntityTypeStruct > mineTypes, bool validOldMineTypes, std::vector<DevStudio::EntityTypeStruct > oldMineTypes, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void perimeterPointCoordinatesUpdated(HlaMinefieldPtr minefield, std::vector<DevStudio::PerimeterPointStruct > perimeterPointCoordinates, bool validOldPerimeterPointCoordinates, std::vector<DevStudio::PerimeterPointStruct > oldPerimeterPointCoordinates, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void protocolModeUpdated(HlaMinefieldPtr minefield, MinefieldProtocolEnum::MinefieldProtocolEnum protocolMode, bool validOldProtocolMode, MinefieldProtocolEnum::MinefieldProtocolEnum oldProtocolMode, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void stateUpdated(HlaMinefieldPtr minefield, bool state, bool validOldState, bool oldState, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
      LIBAPI virtual void producingFederateUpdated(HlaMinefieldPtr minefield, HlaMinefieldAttributes::Attribute attribute, HlaFederateIdPtr oldProducingFederate, HlaFederateIdPtr newProducingFederate, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
   };

}
#endif
