/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLALIBSETTINGS_H
#define DEVELOPER_STUDIO_HLALIBSETTINGS_H

#if defined(_WIN32)
    #if defined(LIB_EXPORT)
        #define LIBAPI __declspec(dllexport)
    #else
        #if defined(LIB_IMPORT)
            #define LIBAPI __declspec(dllimport)
        #else
            #define LIBAPI
        #endif
    #endif
#else
    #define LIBAPI
#endif


/*
 * The usage of non-empty exception specifications are disable by default.
 * They can be enabled by defining DEVELOPER_STUDIO_USE_THROW_SPEC.
 *
 * Note that Microsoft Visual C++ compilers ignore non-empty exception specifications,
 * http://msdn.microsoft.com/en-us/library/skce93f2(v=vs.100).aspx
 */

// Microsoft Visual C++ 7.1 does not support variadic macros
#if defined(_WIN32) && (_MSC_VER < 1400)
    #define THROW_SPEC       throw
#elif defined(DEVELOPER_STUDIO_USE_THROW_SPEC)
    #define THROW_SPEC       throw
#else
    #define THROW_SPEC(...)
#endif

#endif
