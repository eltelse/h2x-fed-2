/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_CAMOUFLAGEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_CAMOUFLAGEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace CamouflageEnum {
        /**
        * Implementation of the <code>CamouflageEnum32</code> data type from the FOM.
        * <br>Description from the FOM: <i>Camouflage type</i>
        */
        enum CamouflageEnum {
            /** <code>UniformPaintScheme</code> (with ordinal 0) */
            UNIFORM_PAINT_SCHEME = 0,
            /** <code>DesertCamouflage</code> (with ordinal 1) */
            DESERT_CAMOUFLAGE = 1,
            /** <code>WinterCamouflage</code> (with ordinal 2) */
            WINTER_CAMOUFLAGE = 2,
            /** <code>ForestCamouflage</code> (with ordinal 3) */
            FOREST_CAMOUFLAGE = 3,
            /** <code>GenericCamouflage</code> (with ordinal 4) */
            GENERIC_CAMOUFLAGE = 4
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to CamouflageEnum: static_cast<DevStudio::CamouflageEnum::CamouflageEnum>(i)
        */
        LIBAPI bool isValid(const CamouflageEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::CamouflageEnum::CamouflageEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::CamouflageEnum::CamouflageEnum const &);
}


#endif
