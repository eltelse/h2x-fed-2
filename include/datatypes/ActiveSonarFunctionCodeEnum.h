/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_ACTIVESONARFUNCTIONCODEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_ACTIVESONARFUNCTIONCODEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace ActiveSonarFunctionCodeEnum {
        /**
        * Implementation of the <code>ActiveSonarFunctionCodeEnum8</code> data type from the FOM.
        * <br>Description from the FOM: <i>The current function being performed by the sonar</i>
        */
        enum ActiveSonarFunctionCodeEnum {
            /** <code>Other</code> (with ordinal 0) */
            OTHER = 0,
            /** <code>PlatformSearch_detect_track</code> (with ordinal 1) */
            PLATFORM_SEARCH_DETECT_TRACK = 1,
            /** <code>Navigation</code> (with ordinal 2) */
            NAVIGATION = 2,
            /** <code>MineHunting</code> (with ordinal 3) */
            MINE_HUNTING = 3,
            /** <code>WeaponSearch_detect_track_detect</code> (with ordinal 4) */
            WEAPON_SEARCH_DETECT_TRACK_DETECT = 4
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to ActiveSonarFunctionCodeEnum: static_cast<DevStudio::ActiveSonarFunctionCodeEnum::ActiveSonarFunctionCodeEnum>(i)
        */
        LIBAPI bool isValid(const ActiveSonarFunctionCodeEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::ActiveSonarFunctionCodeEnum::ActiveSonarFunctionCodeEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::ActiveSonarFunctionCodeEnum::ActiveSonarFunctionCodeEnum const &);
}


#endif
