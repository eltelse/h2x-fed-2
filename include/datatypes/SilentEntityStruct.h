/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_SILENTENTITYSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_SILENTENTITYSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/EntityTypeStruct.h>
#include <DevStudio/datatypes/UnsignedInteger32LengthlessArray.h>
#include <vector>

namespace DevStudio {
   /**
   * Implementation of the <code>SilentEntityStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>These fields contain information about entities not registered in the federation.</i>
   */
   class SilentEntityStruct {

   public:
      /**
      * Description from the FOM: <i>This field shall specify the number of entities that have the type specified in the field EntityType.</i>.
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      unsigned short numberOfEntitiesOfThisType;
      /**
      * Description from the FOM: <i>This field shall specify the number of Entity Appearance records that follow. This number shall be between zero and the number of entities of this type. Simulation applications representing the aggregate that do not model entity appearances shall set this field to zero. Simulation applications representing the aggregate that model entity appearances shall set this field to the number of entity appearances that deviate from the default appearance. Other simulations can safely assume that any entity appearances not specified are default appearances.</i>.
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      unsigned short numberOfAppearanceRecords;
      /**
      * Description from the FOM: <i>This field shall specify the entity types common to the entities in this system list.</i>.
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      */
      EntityTypeStruct entityType;
      /**
      * Description from the FOM: <i>This field shall specify the entity appearances of entities in the aggregate that deviate from the default. The length of the array is defined in the NumberOfAppearanceRecords field.</i>.
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of UnsignedInteger32 elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
      */
      std::vector<unsigned int > entityAppearance;

      LIBAPI SilentEntityStruct()
         :
         numberOfEntitiesOfThisType(0),
         numberOfAppearanceRecords(0),
         entityType(EntityTypeStruct()),
         entityAppearance(0)
      {}

      /**
      * Constructor for SilentEntityStruct
      *
      * @param numberOfEntitiesOfThisType_ value to set as numberOfEntitiesOfThisType.
      * <br>Description from the FOM: <i>This field shall specify the number of entities that have the type specified in the field EntityType.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param numberOfAppearanceRecords_ value to set as numberOfAppearanceRecords.
      * <br>Description from the FOM: <i>This field shall specify the number of Entity Appearance records that follow. This number shall be between zero and the number of entities of this type. Simulation applications representing the aggregate that do not model entity appearances shall set this field to zero. Simulation applications representing the aggregate that model entity appearances shall set this field to the number of entity appearances that deviate from the default appearance. Other simulations can safely assume that any entity appearances not specified are default appearances.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param entityType_ value to set as entityType.
      * <br>Description from the FOM: <i>This field shall specify the entity types common to the entities in this system list.</i>
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      * @param entityAppearance_ value to set as entityAppearance.
      * <br>Description from the FOM: <i>This field shall specify the entity appearances of entities in the aggregate that deviate from the default. The length of the array is defined in the NumberOfAppearanceRecords field.</i>
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of UnsignedInteger32 elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
      */
      LIBAPI SilentEntityStruct(
         unsigned short numberOfEntitiesOfThisType_,
         unsigned short numberOfAppearanceRecords_,
         EntityTypeStruct entityType_,
         std::vector<unsigned int > entityAppearance_
         )
         :
         numberOfEntitiesOfThisType(numberOfEntitiesOfThisType_),
         numberOfAppearanceRecords(numberOfAppearanceRecords_),
         entityType(entityType_),
         entityAppearance(entityAppearance_)
      {}



      /**
      * Function to get numberOfEntitiesOfThisType.
      * <br>Description from the FOM: <i>This field shall specify the number of entities that have the type specified in the field EntityType.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return numberOfEntitiesOfThisType
      */
      LIBAPI unsigned short & getNumberOfEntitiesOfThisType() {
         return numberOfEntitiesOfThisType;
      }

      /**
      * Function to get numberOfAppearanceRecords.
      * <br>Description from the FOM: <i>This field shall specify the number of Entity Appearance records that follow. This number shall be between zero and the number of entities of this type. Simulation applications representing the aggregate that do not model entity appearances shall set this field to zero. Simulation applications representing the aggregate that model entity appearances shall set this field to the number of entity appearances that deviate from the default appearance. Other simulations can safely assume that any entity appearances not specified are default appearances.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return numberOfAppearanceRecords
      */
      LIBAPI unsigned short & getNumberOfAppearanceRecords() {
         return numberOfAppearanceRecords;
      }

      /**
      * Function to get entityType.
      * <br>Description from the FOM: <i>This field shall specify the entity types common to the entities in this system list.</i>
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      *
      * @return entityType
      */
      LIBAPI DevStudio::EntityTypeStruct & getEntityType() {
         return entityType;
      }

      /**
      * Function to get entityAppearance.
      * <br>Description from the FOM: <i>This field shall specify the entity appearances of entities in the aggregate that deviate from the default. The length of the array is defined in the NumberOfAppearanceRecords field.</i>
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of UnsignedInteger32 elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
      *
      * @return entityAppearance
      */
      LIBAPI std::vector<unsigned int > & getEntityAppearance() {
         return entityAppearance;
      }

   };


   LIBAPI bool operator ==(const DevStudio::SilentEntityStruct& l, const DevStudio::SilentEntityStruct& r);
   LIBAPI bool operator !=(const DevStudio::SilentEntityStruct& l, const DevStudio::SilentEntityStruct& r);
   LIBAPI bool operator <(const DevStudio::SilentEntityStruct& l, const DevStudio::SilentEntityStruct& r);
   LIBAPI bool operator >(const DevStudio::SilentEntityStruct& l, const DevStudio::SilentEntityStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::SilentEntityStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::SilentEntityStruct const &);
}
#endif
