/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_STATIONNAMELOCATIONVARIANTSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_STATIONNAMELOCATIONVARIANTSTRUCT_H

#include <DevStudio/datatypes/ConstituentPartStationNameEnum.h>
#include <DevStudio/datatypes/RelativePositionStruct.h>
#include <DevStudio/datatypes/RelativeRangeBearingStruct.h>

#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
   /**
   * Implementation of the <code>StationNameLocationVariantStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>The station name at which the constituent part is located. In case of 'On Station', the alternative specifies its location relative to the host object.</i>
   */
   class StationNameLocationVariantStruct {

   public:
      LIBAPI StationNameLocationVariantStruct()
         : 
         _discriminant(DevStudio::ConstituentPartStationNameEnum::ConstituentPartStationNameEnum())
      {}

      /** 
      * Create a new alternative StationNameLocationVariantStruct, with <code>OnStationXYZ</code> as discriminant.
      *
      * @param relativeLocation value of the RelativeLocation field
      *
      * @return a new StationNameLocationVariantStruct
      */
      LIBAPI static StationNameLocationVariantStruct createRelativeLocation(DevStudio::RelativePositionStruct relativeLocation);

      /** 
      * Create a new alternative StationNameLocationVariantStruct, with <code>OnStationRangeBearing</code> as discriminant.
      *
      * @param relativeRangeAndBearing value of the RelativeRangeAndBearing field
      *
      * @return a new StationNameLocationVariantStruct
      */
      LIBAPI static StationNameLocationVariantStruct createRelativeRangeAndBearing(DevStudio::RelativeRangeBearingStruct relativeRangeAndBearing);

      /**
      * Create a new alternative StationNameLocationVariantStruct, with <code>Other</code> as discriminant.
      *
      * @return a new StationNameLocationVariantStruct
      */
      LIBAPI static StationNameLocationVariantStruct createOther();

      /**
      * Create a new alternative StationNameLocationVariantStruct, with <code>AircraftWingstation</code> as discriminant.
      *
      * @return a new StationNameLocationVariantStruct
      */
      LIBAPI static StationNameLocationVariantStruct createAircraftWingstation();

      /**
      * Create a new alternative StationNameLocationVariantStruct, with <code>ShipsForwardGunmountStarboard</code> as discriminant.
      *
      * @return a new StationNameLocationVariantStruct
      */
      LIBAPI static StationNameLocationVariantStruct createShipsForwardGunmountStarboard();

      /**
      * Create a new alternative StationNameLocationVariantStruct, with <code>ShipsForwardGunmountPort</code> as discriminant.
      *
      * @return a new StationNameLocationVariantStruct
      */
      LIBAPI static StationNameLocationVariantStruct createShipsForwardGunmountPort();

      /**
      * Create a new alternative StationNameLocationVariantStruct, with <code>ShipsForwardGunmountCenterline</code> as discriminant.
      *
      * @return a new StationNameLocationVariantStruct
      */
      LIBAPI static StationNameLocationVariantStruct createShipsForwardGunmountCenterline();

      /**
      * Create a new alternative StationNameLocationVariantStruct, with <code>ShipsAftGunmountStarboard</code> as discriminant.
      *
      * @return a new StationNameLocationVariantStruct
      */
      LIBAPI static StationNameLocationVariantStruct createShipsAftGunmountStarboard();

      /**
      * Create a new alternative StationNameLocationVariantStruct, with <code>ShipsAftGunmountPort</code> as discriminant.
      *
      * @return a new StationNameLocationVariantStruct
      */
      LIBAPI static StationNameLocationVariantStruct createShipsAftGunmountPort();

      /**
      * Create a new alternative StationNameLocationVariantStruct, with <code>ShipsAftGunmountCenterline</code> as discriminant.
      *
      * @return a new StationNameLocationVariantStruct
      */
      LIBAPI static StationNameLocationVariantStruct createShipsAftGunmountCenterline();

      /**
      * Create a new alternative StationNameLocationVariantStruct, with <code>ForwardTorpedoTube</code> as discriminant.
      *
      * @return a new StationNameLocationVariantStruct
      */
      LIBAPI static StationNameLocationVariantStruct createForwardTorpedoTube();

      /**
      * Create a new alternative StationNameLocationVariantStruct, with <code>AftTorpedoTube</code> as discriminant.
      *
      * @return a new StationNameLocationVariantStruct
      */
      LIBAPI static StationNameLocationVariantStruct createAftTorpedoTube();

      /**
      * Create a new alternative StationNameLocationVariantStruct, with <code>BombBay</code> as discriminant.
      *
      * @return a new StationNameLocationVariantStruct
      */
      LIBAPI static StationNameLocationVariantStruct createBombBay();

      /**
      * Create a new alternative StationNameLocationVariantStruct, with <code>CargoBay</code> as discriminant.
      *
      * @return a new StationNameLocationVariantStruct
      */
      LIBAPI static StationNameLocationVariantStruct createCargoBay();

      /**
      * Create a new alternative StationNameLocationVariantStruct, with <code>TruckBed</code> as discriminant.
      *
      * @return a new StationNameLocationVariantStruct
      */
      LIBAPI static StationNameLocationVariantStruct createTruckBed();

      /**
      * Create a new alternative StationNameLocationVariantStruct, with <code>TrailerBed</code> as discriminant.
      *
      * @return a new StationNameLocationVariantStruct
      */
      LIBAPI static StationNameLocationVariantStruct createTrailerBed();

      /**
      * Create a new alternative StationNameLocationVariantStruct, with <code>WellDeck</code> as discriminant.
      *
      * @return a new StationNameLocationVariantStruct
      */
      LIBAPI static StationNameLocationVariantStruct createWellDeck();



      /**
      * Function to get discriminant
      *
      * @return disciminant
      */
      LIBAPI DevStudio::ConstituentPartStationNameEnum::ConstituentPartStationNameEnum getDiscriminant() const {
          return _discriminant;
      }

      /**
      * Function to get RelativeLocation.
      * Note that this field is only valid of the discriminant is <code>OnStationXYZ</code>.
      * <br>Description from the FOM: <i>The location of the constituent part object relative to the host object entity coordinate system.</i>
      * <br>Description of the data type from the FOM: <i>Relative position in right-handed Cartesian coordinates.</i>
      *
      * @return RelativeLocation value
      */
      LIBAPI DevStudio::RelativePositionStructPtr getRelativeLocation() const {
         return _relativeLocation;
      }

      /**
      * Function to set RelativeLocation.
      * Note that this will set the discriminant to <code>OnStationXYZ</code>.
      * <br>Description from the FOM: <i>The location of the constituent part object relative to the host object entity coordinate system.</i>
      * <br>Description of the data type from the FOM: <i>Relative position in right-handed Cartesian coordinates.</i>
      *
      * @param relativeLocation value used to create a RelativePositionStructPtr
      */
      LIBAPI void setRelativeLocation(const DevStudio::RelativePositionStruct relativeLocation) {
         _relativeLocation = DevStudio::RelativePositionStructPtr( new DevStudio::RelativePositionStruct (relativeLocation));
         _discriminant = ConstituentPartStationNameEnum::ON_STATION_XYZ;
      }

      /**
      * Function to get RelativeRangeAndBearing.
      * Note that this field is only valid of the discriminant is <code>OnStationRangeBearing</code>.
      * <br>Description from the FOM: <i>The location of the constituent part object relative to the host object in polar coordinates.</i>
      * <br>Description of the data type from the FOM: <i>Relative position in polar coordinates.</i>
      *
      * @return RelativeRangeAndBearing value
      */
      LIBAPI DevStudio::RelativeRangeBearingStructPtr getRelativeRangeAndBearing() const {
         return _relativeRangeAndBearing;
      }

      /**
      * Function to set RelativeRangeAndBearing.
      * Note that this will set the discriminant to <code>OnStationRangeBearing</code>.
      * <br>Description from the FOM: <i>The location of the constituent part object relative to the host object in polar coordinates.</i>
      * <br>Description of the data type from the FOM: <i>Relative position in polar coordinates.</i>
      *
      * @param relativeRangeAndBearing value used to create a RelativeRangeBearingStructPtr
      */
      LIBAPI void setRelativeRangeAndBearing(const DevStudio::RelativeRangeBearingStruct relativeRangeAndBearing) {
         _relativeRangeAndBearing = DevStudio::RelativeRangeBearingStructPtr( new DevStudio::RelativeRangeBearingStruct (relativeRangeAndBearing));
         _discriminant = ConstituentPartStationNameEnum::ON_STATION_RANGE_BEARING;
      }

      /** Description from the FOM: <i>The location of the constituent part object relative to the host object entity coordinate system.</i> */
      DevStudio::RelativePositionStructPtr _relativeLocation;
      /** Description from the FOM: <i>The location of the constituent part object relative to the host object in polar coordinates.</i> */
      DevStudio::RelativeRangeBearingStructPtr _relativeRangeAndBearing;

   private:
      DevStudio::ConstituentPartStationNameEnum::ConstituentPartStationNameEnum _discriminant;

      StationNameLocationVariantStruct(
         DevStudio::RelativePositionStructPtr relativeLocation,
         DevStudio::RelativeRangeBearingStructPtr relativeRangeAndBearing,
         DevStudio::ConstituentPartStationNameEnum::ConstituentPartStationNameEnum discriminant
      );
   };

   LIBAPI bool operator ==(const DevStudio::StationNameLocationVariantStruct& l, const DevStudio::StationNameLocationVariantStruct& r);
   LIBAPI bool operator !=(const DevStudio::StationNameLocationVariantStruct& l, const DevStudio::StationNameLocationVariantStruct& r);
   LIBAPI bool operator <(const DevStudio::StationNameLocationVariantStruct& l, const DevStudio::StationNameLocationVariantStruct& r);
   LIBAPI bool operator >(const DevStudio::StationNameLocationVariantStruct& l, const DevStudio::StationNameLocationVariantStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::StationNameLocationVariantStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::StationNameLocationVariantStruct const &);
}

#endif
