/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_DATUMIDENTIFIERENUM_H
#define DEVELOPER_STUDIO_DATATYPES_DATUMIDENTIFIERENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace DatumIdentifierEnum {
        /**
        * Implementation of the <code>DatumIdentifierEnum32</code> data type from the FOM.
        * <br>Description from the FOM: <i>Datum ID</i>
        */
        enum DatumIdentifierEnum {
            /** <code>HighFidelityHAVEQUICKRadio</code> (with ordinal 3000) */
            HIGH_FIDELITY_HAVEQUICKRADIO = 3000,
            /** <code>BlankingSectorAttributeRecord</code> (with ordinal 3500) */
            BLANKING_SECTOR_ATTRIBUTE_RECORD = 3500,
            /** <code>AngleDeceptionAttributeRecord</code> (with ordinal 3501) */
            ANGLE_DECEPTION_ATTRIBUTE_RECORD = 3501,
            /** <code>FalseTargetsAttributeRecord</code> (with ordinal 3502) */
            FALSE_TARGETS_ATTRIBUTE_RECORD = 3502,
            /** <code>DEPrecisionAimpointRecord</code> (with ordinal 4000) */
            DEPRECISION_AIMPOINT_RECORD = 4000,
            /** <code>DEAreaAimpointRecord</code> (with ordinal 4001) */
            DEAREA_AIMPOINT_RECORD = 4001,
            /** <code>DirectedEnergyDamageDescriptionRecord</code> (with ordinal 4500) */
            DIRECTED_ENERGY_DAMAGE_DESCRIPTION_RECORD = 4500,
            /** <code>CryptoControl</code> (with ordinal 5000) */
            CRYPTO_CONTROL = 5000,
            /** <code>Mode5_STransponderLocation</code> (with ordinal 5001) */
            MODE5_STRANSPONDER_LOCATION = 5001,
            /** <code>Mode5_STransponderLocationError</code> (with ordinal 5002) */
            MODE5_STRANSPONDER_LOCATION_ERROR = 5002,
            /** <code>SquitterAirbornePositionReport</code> (with ordinal 5003) */
            SQUITTER_AIRBORNE_POSITION_REPORT = 5003,
            /** <code>SquitterAirborneVelocityReport</code> (with ordinal 5004) */
            SQUITTER_AIRBORNE_VELOCITY_REPORT = 5004,
            /** <code>SquitterSurfacePositionReport</code> (with ordinal 5005) */
            SQUITTER_SURFACE_POSITION_REPORT = 5005,
            /** <code>SquitterIdentificationReport</code> (with ordinal 5006) */
            SQUITTER_IDENTIFICATION_REPORT = 5006,
            /** <code>GICB</code> (with ordinal 5007) */
            GICB = 5007,
            /** <code>SquitterEvent-DrivenReport</code> (with ordinal 5008) */
            SQUITTER_EVENT_DRIVEN_REPORT = 5008,
            /** <code>AntennaLocation</code> (with ordinal 5009) */
            ANTENNA_LOCATION = 5009,
            /** <code>BasicInteractive</code> (with ordinal 5010) */
            BASIC_INTERACTIVE = 5010,
            /** <code>InteractiveMode4Reply</code> (with ordinal 5011) */
            INTERACTIVE_MODE4REPLY = 5011,
            /** <code>InteractiveMode5Reply</code> (with ordinal 5012) */
            INTERACTIVE_MODE5REPLY = 5012,
            /** <code>InteractiveBasicMode5</code> (with ordinal 5013) */
            INTERACTIVE_BASIC_MODE5 = 5013,
            /** <code>InteractiveBasicModeS</code> (with ordinal 5014) */
            INTERACTIVE_BASIC_MODE_S = 5014,
            /** <code>IOEffect</code> (with ordinal 5500) */
            IOEFFECT = 5500,
            /** <code>IOCommunicationsNode</code> (with ordinal 5501) */
            IOCOMMUNICATIONS_NODE = 5501,
            /** <code>Entity_Identification</code> (with ordinal 10000) */
            ENTITY_IDENTIFICATION = 10000,
            /** <code>Entity_Type</code> (with ordinal 11000) */
            ENTITY_TYPE = 11000,
            /** <code>Concatenated</code> (with ordinal 11100) */
            CONCATENATED = 11100,
            /** <code>Entity_Type-Kind</code> (with ordinal 11110) */
            ENTITY_TYPE_KIND = 11110,
            /** <code>Entity_Type-Domain</code> (with ordinal 11120) */
            ENTITY_TYPE_DOMAIN = 11120,
            /** <code>Entity_Type-Country</code> (with ordinal 11130) */
            ENTITY_TYPE_COUNTRY = 11130,
            /** <code>Entity_Type-Category</code> (with ordinal 11140) */
            ENTITY_TYPE_CATEGORY = 11140,
            /** <code>Entity_Type-Subcategory</code> (with ordinal 11150) */
            ENTITY_TYPE_SUBCATEGORY = 11150,
            /** <code>Entity_Type-Specific</code> (with ordinal 11160) */
            ENTITY_TYPE_SPECIFIC = 11160,
            /** <code>Entity_Type-Extra</code> (with ordinal 11170) */
            ENTITY_TYPE_EXTRA = 11170,
            /** <code>Force_ID</code> (with ordinal 11200) */
            FORCE_ID = 11200,
            /** <code>Description</code> (with ordinal 11300) */
            DESCRIPTION = 11300,
            /** <code>Alternative_Entity_Type</code> (with ordinal 12000) */
            ALTERNATIVE_ENTITY_TYPE = 12000,
            /** <code>Alternative_Entity_Type-Kind</code> (with ordinal 12110) */
            ALTERNATIVE_ENTITY_TYPE_KIND = 12110,
            /** <code>Alternative_Entity_Type-Domain</code> (with ordinal 12120) */
            ALTERNATIVE_ENTITY_TYPE_DOMAIN = 12120,
            /** <code>Alternative_Entity_Type-Country</code> (with ordinal 12130) */
            ALTERNATIVE_ENTITY_TYPE_COUNTRY = 12130,
            /** <code>Alternative_Entity_Type-Category</code> (with ordinal 12140) */
            ALTERNATIVE_ENTITY_TYPE_CATEGORY = 12140,
            /** <code>Alternative_Entity_Type-Subcategory</code> (with ordinal 12150) */
            ALTERNATIVE_ENTITY_TYPE_SUBCATEGORY = 12150,
            /** <code>Alternative_Entity_Type-Specific</code> (with ordinal 12160) */
            ALTERNATIVE_ENTITY_TYPE_SPECIFIC = 12160,
            /** <code>Alternative_Entity_Type-Extra</code> (with ordinal 12170) */
            ALTERNATIVE_ENTITY_TYPE_EXTRA = 12170,
            /** <code>Alternative_Entity_Type-Description</code> (with ordinal 12300) */
            ALTERNATIVE_ENTITY_TYPE_DESCRIPTION = 12300,
            /** <code>Entity_Marking</code> (with ordinal 13000) */
            ENTITY_MARKING = 13000,
            /** <code>Entity_Marking_Characters</code> (with ordinal 13100) */
            ENTITY_MARKING_CHARACTERS = 13100,
            /** <code>Crew_ID</code> (with ordinal 13200) */
            CREW_ID = 13200,
            /** <code>Task_Organization</code> (with ordinal 14000) */
            TASK_ORGANIZATION = 14000,
            /** <code>Regiment_Name</code> (with ordinal 14200) */
            REGIMENT_NAME = 14200,
            /** <code>Battalion_Name</code> (with ordinal 14300) */
            BATTALION_NAME = 14300,
            /** <code>Company_Name</code> (with ordinal 14400) */
            COMPANY_NAME = 14400,
            /** <code>Platoon_Name</code> (with ordinal 14500) */
            PLATOON_NAME = 14500,
            /** <code>Squad_Name</code> (with ordinal 14520) */
            SQUAD_NAME = 14520,
            /** <code>Team_Name</code> (with ordinal 14540) */
            TEAM_NAME = 14540,
            /** <code>Bumper_Number</code> (with ordinal 14600) */
            BUMPER_NUMBER = 14600,
            /** <code>Vehicle_Number</code> (with ordinal 14700) */
            VEHICLE_NUMBER = 14700,
            /** <code>Unit_Number</code> (with ordinal 14800) */
            UNIT_NUMBER = 14800,
            /** <code>DIS_Identity</code> (with ordinal 15000) */
            DIS_IDENTITY = 15000,
            /** <code>DIS_Site_ID</code> (with ordinal 15100) */
            DIS_SITE_ID = 15100,
            /** <code>DIS_Host_ID</code> (with ordinal 15200) */
            DIS_HOST_ID = 15200,
            /** <code>DIS_Entity_ID</code> (with ordinal 15300) */
            DIS_ENTITY_ID = 15300,
            /** <code>Mount_Intent</code> (with ordinal 15400) */
            MOUNT_INTENT = 15400,
            /** <code>Tether-Unthether_Command_ID</code> (with ordinal 15500) */
            TETHER_UNTHETHER_COMMAND_ID = 15500,
            /** <code>Teleport_Entity_Data_Record</code> (with ordinal 15510) */
            TELEPORT_ENTITY_DATA_RECORD = 15510,
            /** <code>DIS_Aggregate_ID</code> (with ordinal 15600) */
            DIS_AGGREGATE_ID = 15600,
            /** <code>OwnershipStatus</code> (with ordinal 15800) */
            OWNERSHIP_STATUS = 15800,
            /** <code>Loads</code> (with ordinal 20000) */
            LOADS = 20000,
            /** <code>Crew_Members</code> (with ordinal 21000) */
            CREW_MEMBERS = 21000,
            /** <code>Crew_Member_ID</code> (with ordinal 21100) */
            CREW_MEMBER_ID = 21100,
            /** <code>Health</code> (with ordinal 21200) */
            HEALTH = 21200,
            /** <code>Job_Assignment</code> (with ordinal 21300) */
            JOB_ASSIGNMENT = 21300,
            /** <code>Fuel</code> (with ordinal 23000) */
            FUEL = 23000,
            /** <code>Fuel_Quantity-Liters</code> (with ordinal 23100) */
            FUEL_QUANTITY_LITERS = 23100,
            /** <code>Fuel_Quantity-Gallons</code> (with ordinal 23105) */
            FUEL_QUANTITY_GALLONS = 23105,
            /** <code>Ammunition</code> (with ordinal 24000) */
            AMMUNITION = 24000,
            /** <code>Ammunition_quantity_120mm_HEAT</code> (with ordinal 24001) */
            AMMUNITION_QUANTITY_120MM_HEAT = 24001,
            /** <code>Ammunition_quantity_120mm_SABOT</code> (with ordinal 24002) */
            AMMUNITION_QUANTITY_120MM_SABOT = 24002,
            /** <code>Ammunition_quantity_12-7mm_M8</code> (with ordinal 24003) */
            AMMUNITION_QUANTITY_12_7MM_M8 = 24003,
            /** <code>Ammunition_quantity_12-7mm_M20</code> (with ordinal 24004) */
            AMMUNITION_QUANTITY_12_7MM_M20 = 24004,
            /** <code>Ammunition_quantity_7-62mm_M62</code> (with ordinal 24005) */
            AMMUNITION_QUANTITY_7_62MM_M62 = 24005,
            /** <code>Ammunition_quantity_M250_UKL8A1</code> (with ordinal 24006) */
            AMMUNITION_QUANTITY_M250_UKL8A1 = 24006,
            /** <code>Ammunition_quantity_M250_UKL8A3</code> (with ordinal 24007) */
            AMMUNITION_QUANTITY_M250_UKL8A3 = 24007,
            /** <code>Ammunition_quantity_7-62mm_M80</code> (with ordinal 24008) */
            AMMUNITION_QUANTITY_7_62MM_M80 = 24008,
            /** <code>Ammunition_quantity_12-7mm</code> (with ordinal 24009) */
            AMMUNITION_QUANTITY_12_7MM = 24009,
            /** <code>Ammunition_quantity_7-62mm</code> (with ordinal 24010) */
            AMMUNITION_QUANTITY_7_62MM = 24010,
            /** <code>Mines-quantity</code> (with ordinal 24060) */
            MINES_QUANTITY = 24060,
            /** <code>Mines-Type</code> (with ordinal 24100) */
            MINES_TYPE = 24100,
            /** <code>Mines-Kind</code> (with ordinal 24110) */
            MINES_KIND = 24110,
            /** <code>Mines-Domain</code> (with ordinal 24120) */
            MINES_DOMAIN = 24120,
            /** <code>Mines-Country</code> (with ordinal 24130) */
            MINES_COUNTRY = 24130,
            /** <code>Mines-Category</code> (with ordinal 24140) */
            MINES_CATEGORY = 24140,
            /** <code>Mines-Subcategory</code> (with ordinal 24150) */
            MINES_SUBCATEGORY = 24150,
            /** <code>Mines-Extra</code> (with ordinal 24160) */
            MINES_EXTRA = 24160,
            /** <code>Mines-Description</code> (with ordinal 24300) */
            MINES_DESCRIPTION = 24300,
            /** <code>Cargo</code> (with ordinal 25000) */
            CARGO = 25000,
            /** <code>Vehicle_Mass</code> (with ordinal 26000) */
            VEHICLE_MASS = 26000,
            /** <code>Supply_Quantity</code> (with ordinal 27000) */
            SUPPLY_QUANTITY = 27000,
            /** <code>Armament</code> (with ordinal 28000) */
            ARMAMENT = 28000,
            /** <code>Status</code> (with ordinal 30000) */
            STATUS = 30000,
            /** <code>ActivateEntity</code> (with ordinal 30010) */
            ACTIVATE_ENTITY = 30010,
            /** <code>Subscription_State</code> (with ordinal 30100) */
            SUBSCRIPTION_STATE = 30100,
            /** <code>Round_trip_time_delay</code> (with ordinal 30300) */
            ROUND_TRIP_TIME_DELAY = 30300,
            /** <code>TADIL-J_message_count_label0</code> (with ordinal 30400) */
            TADIL_J_MESSAGE_COUNT_LABEL0 = 30400,
            /** <code>TADIL-J_message_count_label1</code> (with ordinal 30401) */
            TADIL_J_MESSAGE_COUNT_LABEL1 = 30401,
            /** <code>TADIL-J_message_count_label2</code> (with ordinal 30402) */
            TADIL_J_MESSAGE_COUNT_LABEL2 = 30402,
            /** <code>TADIL-J_message_count_label3</code> (with ordinal 30403) */
            TADIL_J_MESSAGE_COUNT_LABEL3 = 30403,
            /** <code>TADIL-J_message_count_label4</code> (with ordinal 30404) */
            TADIL_J_MESSAGE_COUNT_LABEL4 = 30404,
            /** <code>TADIL-J_message_count_label5</code> (with ordinal 30405) */
            TADIL_J_MESSAGE_COUNT_LABEL5 = 30405,
            /** <code>TADIL-J_message_count_label6</code> (with ordinal 30406) */
            TADIL_J_MESSAGE_COUNT_LABEL6 = 30406,
            /** <code>TADIL-J_message-count_label7</code> (with ordinal 30407) */
            TADIL_J_MESSAGE_COUNT_LABEL7 = 30407,
            /** <code>TADIL-J_message-count_label8</code> (with ordinal 30408) */
            TADIL_J_MESSAGE_COUNT_LABEL8 = 30408,
            /** <code>TADIL-J_message-count_label9</code> (with ordinal 30409) */
            TADIL_J_MESSAGE_COUNT_LABEL9 = 30409,
            /** <code>TADIL-J_message-count_label10</code> (with ordinal 30410) */
            TADIL_J_MESSAGE_COUNT_LABEL10 = 30410,
            /** <code>TADIL-J_message-count_label11</code> (with ordinal 30411) */
            TADIL_J_MESSAGE_COUNT_LABEL11 = 30411,
            /** <code>TADIL-J_message-count_label12</code> (with ordinal 30412) */
            TADIL_J_MESSAGE_COUNT_LABEL12 = 30412,
            /** <code>TADIL-J_message-count_label13</code> (with ordinal 30413) */
            TADIL_J_MESSAGE_COUNT_LABEL13 = 30413,
            /** <code>TADIL-J_message-count_label14</code> (with ordinal 30414) */
            TADIL_J_MESSAGE_COUNT_LABEL14 = 30414,
            /** <code>TADIL-J_message-count_label15</code> (with ordinal 30415) */
            TADIL_J_MESSAGE_COUNT_LABEL15 = 30415,
            /** <code>TADIL-J_message-count_label16</code> (with ordinal 30416) */
            TADIL_J_MESSAGE_COUNT_LABEL16 = 30416,
            /** <code>TADIL-J_message-count_label17</code> (with ordinal 30417) */
            TADIL_J_MESSAGE_COUNT_LABEL17 = 30417,
            /** <code>TADIL-J_message-count_label18</code> (with ordinal 30418) */
            TADIL_J_MESSAGE_COUNT_LABEL18 = 30418,
            /** <code>TADIL-J_message-count_label19</code> (with ordinal 30419) */
            TADIL_J_MESSAGE_COUNT_LABEL19 = 30419,
            /** <code>TADIL-J_message-count_label20</code> (with ordinal 30420) */
            TADIL_J_MESSAGE_COUNT_LABEL20 = 30420,
            /** <code>TADIL-J_message-count_label21</code> (with ordinal 30421) */
            TADIL_J_MESSAGE_COUNT_LABEL21 = 30421,
            /** <code>TADIL-J_message-count_label22</code> (with ordinal 30422) */
            TADIL_J_MESSAGE_COUNT_LABEL22 = 30422,
            /** <code>TADIL-J_message-count_label23</code> (with ordinal 30423) */
            TADIL_J_MESSAGE_COUNT_LABEL23 = 30423,
            /** <code>TADIL-J_message-count_label24</code> (with ordinal 30424) */
            TADIL_J_MESSAGE_COUNT_LABEL24 = 30424,
            /** <code>TADIL-J_message-count_label25</code> (with ordinal 30425) */
            TADIL_J_MESSAGE_COUNT_LABEL25 = 30425,
            /** <code>TADIL-J_message-count_label26</code> (with ordinal 30426) */
            TADIL_J_MESSAGE_COUNT_LABEL26 = 30426,
            /** <code>TADIL-J_message-count_label27</code> (with ordinal 30427) */
            TADIL_J_MESSAGE_COUNT_LABEL27 = 30427,
            /** <code>TADIL-J_message-count_label28</code> (with ordinal 30428) */
            TADIL_J_MESSAGE_COUNT_LABEL28 = 30428,
            /** <code>TADIL-J_message-count_label29</code> (with ordinal 30429) */
            TADIL_J_MESSAGE_COUNT_LABEL29 = 30429,
            /** <code>TADIL-J_message-count_label30</code> (with ordinal 30430) */
            TADIL_J_MESSAGE_COUNT_LABEL30 = 30430,
            /** <code>TADIL-J_message-count_label31</code> (with ordinal 30431) */
            TADIL_J_MESSAGE_COUNT_LABEL31 = 30431,
            /** <code>Position</code> (with ordinal 31000) */
            POSITION = 31000,
            /** <code>Route_Waypoint_Type</code> (with ordinal 31010) */
            ROUTE_WAYPOINT_TYPE = 31010,
            /** <code>MilGrid10</code> (with ordinal 31100) */
            MIL_GRID10 = 31100,
            /** <code>Geocentric_Coordinates</code> (with ordinal 31200) */
            GEOCENTRIC_COORDINATES = 31200,
            /** <code>Geocentric_Coordinate_X</code> (with ordinal 31210) */
            GEOCENTRIC_COORDINATE_X = 31210,
            /** <code>Geocentric_Coordinate_Y</code> (with ordinal 31220) */
            GEOCENTRIC_COORDINATE_Y = 31220,
            /** <code>Geocentric_Coordinate_Z</code> (with ordinal 31230) */
            GEOCENTRIC_COORDINATE_Z = 31230,
            /** <code>Latitude</code> (with ordinal 31300) */
            LATITUDE = 31300,
            /** <code>Longitude</code> (with ordinal 31400) */
            LONGITUDE = 31400,
            /** <code>Line_of_Sight</code> (with ordinal 31500) */
            LINE_OF_SIGHT = 31500,
            /** <code>Line_of_Sight_X</code> (with ordinal 31510) */
            LINE_OF_SIGHT_X = 31510,
            /** <code>Line_of_Sight_Y</code> (with ordinal 31520) */
            LINE_OF_SIGHT_Y = 31520,
            /** <code>Line_of_Sight_Z</code> (with ordinal 31530) */
            LINE_OF_SIGHT_Z = 31530,
            /** <code>Altitude</code> (with ordinal 31600) */
            ALTITUDE = 31600,
            /** <code>Destination_Latitude</code> (with ordinal 31700) */
            DESTINATION_LATITUDE = 31700,
            /** <code>Destination_Longitude</code> (with ordinal 31800) */
            DESTINATION_LONGITUDE = 31800,
            /** <code>Destination_Altitude</code> (with ordinal 31900) */
            DESTINATION_ALTITUDE = 31900,
            /** <code>Orientation</code> (with ordinal 32000) */
            ORIENTATION = 32000,
            /** <code>Hull_Heading_Angle</code> (with ordinal 32100) */
            HULL_HEADING_ANGLE = 32100,
            /** <code>Hull_Pitch_Angle</code> (with ordinal 32200) */
            HULL_PITCH_ANGLE = 32200,
            /** <code>Roll_Angle</code> (with ordinal 32300) */
            ROLL_ANGLE = 32300,
            /** <code>Roll_Angle_X</code> (with ordinal 32500) */
            ROLL_ANGLE_X = 32500,
            /** <code>Roll_Angle_Y</code> (with ordinal 32600) */
            ROLL_ANGLE_Y = 32600,
            /** <code>Roll_Angle_Z</code> (with ordinal 32700) */
            ROLL_ANGLE_Z = 32700,
            /** <code>Appearance</code> (with ordinal 33000) */
            APPEARANCE = 33000,
            /** <code>Ambient_Lighting</code> (with ordinal 33100) */
            AMBIENT_LIGHTING = 33100,
            /** <code>Lights</code> (with ordinal 33101) */
            LIGHTS = 33101,
            /** <code>Paint_Scheme</code> (with ordinal 33200) */
            PAINT_SCHEME = 33200,
            /** <code>Smoke</code> (with ordinal 33300) */
            SMOKE = 33300,
            /** <code>Trailing_Effects</code> (with ordinal 33400) */
            TRAILING_EFFECTS = 33400,
            /** <code>Flaming</code> (with ordinal 33500) */
            FLAMING = 33500,
            /** <code>Marking</code> (with ordinal 33600) */
            MARKING = 33600,
            /** <code>Mine_Plows_Attached</code> (with ordinal 33710) */
            MINE_PLOWS_ATTACHED = 33710,
            /** <code>Mine_Rollers_Attached</code> (with ordinal 33720) */
            MINE_ROLLERS_ATTACHED = 33720,
            /** <code>Tank_Turret_Azimuth</code> (with ordinal 33730) */
            TANK_TURRET_AZIMUTH = 33730,
            /** <code>Failures_and_Malfunctions</code> (with ordinal 34000) */
            FAILURES_AND_MALFUNCTIONS = 34000,
            /** <code>Age</code> (with ordinal 34100) */
            AGE = 34100,
            /** <code>Kilometers</code> (with ordinal 34110) */
            KILOMETERS = 34110,
            /** <code>Damage</code> (with ordinal 35000) */
            DAMAGE = 35000,
            /** <code>Cause</code> (with ordinal 35050) */
            CAUSE = 35050,
            /** <code>Mobility_Kill</code> (with ordinal 35100) */
            MOBILITY_KILL = 35100,
            /** <code>Fire-Power_Kill</code> (with ordinal 35200) */
            FIRE_POWER_KILL = 35200,
            /** <code>Personnel_Casualties</code> (with ordinal 35300) */
            PERSONNEL_CASUALTIES = 35300,
            /** <code>Velocity</code> (with ordinal 36000) */
            VELOCITY = 36000,
            /** <code>X-velocity</code> (with ordinal 36100) */
            X_VELOCITY = 36100,
            /** <code>Y-velocity</code> (with ordinal 36200) */
            Y_VELOCITY = 36200,
            /** <code>Z-velocity</code> (with ordinal 36300) */
            Z_VELOCITY = 36300,
            /** <code>Speed</code> (with ordinal 36400) */
            SPEED = 36400,
            /** <code>Acceleration</code> (with ordinal 37000) */
            ACCELERATION = 37000,
            /** <code>X-acceleration</code> (with ordinal 37100) */
            X_ACCELERATION = 37100,
            /** <code>Y-acceleration</code> (with ordinal 37200) */
            Y_ACCELERATION = 37200,
            /** <code>Z-acceleration</code> (with ordinal 37300) */
            Z_ACCELERATION = 37300,
            /** <code>Engine_Status</code> (with ordinal 38100) */
            ENGINE_STATUS = 38100,
            /** <code>Primary_Target_Line</code> (with ordinal 39000) */
            PRIMARY_TARGET_LINE = 39000,
            /** <code>Exercise</code> (with ordinal 40000) */
            EXERCISE = 40000,
            /** <code>Exercise_State</code> (with ordinal 40010) */
            EXERCISE_STATE = 40010,
            /** <code>Restart_Refresh</code> (with ordinal 40015) */
            RESTART_REFRESH = 40015,
            /** <code>AFATDS_File_Name</code> (with ordinal 40020) */
            AFATDS_FILE_NAME = 40020,
            /** <code>Terrain_Database</code> (with ordinal 41000) */
            TERRAIN_DATABASE = 41000,
            /** <code>Missions</code> (with ordinal 42000) */
            MISSIONS = 42000,
            /** <code>Mission_ID</code> (with ordinal 42100) */
            MISSION_ID = 42100,
            /** <code>Mission_Type</code> (with ordinal 42200) */
            MISSION_TYPE = 42200,
            /** <code>Mission_Request_Time_Stamp</code> (with ordinal 42300) */
            MISSION_REQUEST_TIME_STAMP = 42300,
            /** <code>Exercise_Description</code> (with ordinal 43000) */
            EXERCISE_DESCRIPTION = 43000,
            /** <code>Name</code> (with ordinal 43100) */
            NAME = 43100,
            /** <code>Entities</code> (with ordinal 43200) */
            ENTITIES = 43200,
            /** <code>Version</code> (with ordinal 43300) */
            VERSION = 43300,
            /** <code>Guise_Mode</code> (with ordinal 43410) */
            GUISE_MODE = 43410,
            /** <code>Simulation_Application_Active_Status</code> (with ordinal 43420) */
            SIMULATION_APPLICATION_ACTIVE_STATUS = 43420,
            /** <code>Simulation_Application_Role_Record</code> (with ordinal 43430) */
            SIMULATION_APPLICATION_ROLE_RECORD = 43430,
            /** <code>Simulation_Application_State</code> (with ordinal 43440) */
            SIMULATION_APPLICATION_STATE = 43440,
            /** <code>Visual_Output_Mode</code> (with ordinal 44000) */
            VISUAL_OUTPUT_MODE = 44000,
            /** <code>Simulation_Manager_Role</code> (with ordinal 44100) */
            SIMULATION_MANAGER_ROLE = 44100,
            /** <code>Simulation_Manager_Site_ID</code> (with ordinal 44110) */
            SIMULATION_MANAGER_SITE_ID = 44110,
            /** <code>Simulation_Manager_Application_ID</code> (with ordinal 44120) */
            SIMULATION_MANAGER_APPLICATION_ID = 44120,
            /** <code>Simulation_Manager_Entity_ID</code> (with ordinal 44130) */
            SIMULATION_MANAGER_ENTITY_ID = 44130,
            /** <code>Simulation_Manager_Active_Status</code> (with ordinal 44140) */
            SIMULATION_MANAGER_ACTIVE_STATUS = 44140,
            /** <code>After_Active_Review_Role</code> (with ordinal 44200) */
            AFTER_ACTIVE_REVIEW_ROLE = 44200,
            /** <code>After_Active_Review_Site_ID</code> (with ordinal 44210) */
            AFTER_ACTIVE_REVIEW_SITE_ID = 44210,
            /** <code>After_Active_Application_ID</code> (with ordinal 44220) */
            AFTER_ACTIVE_APPLICATION_ID = 44220,
            /** <code>After_Active_Review_Entity_ID</code> (with ordinal 44230) */
            AFTER_ACTIVE_REVIEW_ENTITY_ID = 44230,
            /** <code>After_Active_Review_Active_Status</code> (with ordinal 44240) */
            AFTER_ACTIVE_REVIEW_ACTIVE_STATUS = 44240,
            /** <code>Exercise_Logger_Role</code> (with ordinal 44300) */
            EXERCISE_LOGGER_ROLE = 44300,
            /** <code>Exercise_Logger_Site_ID</code> (with ordinal 44310) */
            EXERCISE_LOGGER_SITE_ID = 44310,
            /** <code>Exercise_Logger_Application_ID</code> (with ordinal 44320) */
            EXERCISE_LOGGER_APPLICATION_ID = 44320,
            /** <code>Exercise_Entity_ID</code> (with ordinal 44330) */
            EXERCISE_ENTITY_ID = 44330,
            /** <code>Exercise_Logger_Active_Status</code> (with ordinal 44340) */
            EXERCISE_LOGGER_ACTIVE_STATUS = 44340,
            /** <code>Synthetic_Environment_Manager_Role</code> (with ordinal 44400) */
            SYNTHETIC_ENVIRONMENT_MANAGER_ROLE = 44400,
            /** <code>Synthetic_Environment_Manager_Site_ID</code> (with ordinal 44410) */
            SYNTHETIC_ENVIRONMENT_MANAGER_SITE_ID = 44410,
            /** <code>Synthetic_Environment_Manager_Application_ID</code> (with ordinal 44420) */
            SYNTHETIC_ENVIRONMENT_MANAGER_APPLICATION_ID = 44420,
            /** <code>Synthetic_Environment_Manager_Entity_ID</code> (with ordinal 44430) */
            SYNTHETIC_ENVIRONMENT_MANAGER_ENTITY_ID = 44430,
            /** <code>Synthetic_Environment_Manager_Active_Status</code> (with ordinal 44440) */
            SYNTHETIC_ENVIRONMENT_MANAGER_ACTIVE_STATUS = 44440,
            /** <code>SIMNET-DIS_Translator_Role</code> (with ordinal 44500) */
            SIMNET_DIS_TRANSLATOR_ROLE = 44500,
            /** <code>SIMNET-DIS_Translator_Site_ID</code> (with ordinal 44510) */
            SIMNET_DIS_TRANSLATOR_SITE_ID = 44510,
            /** <code>SIMNET-DIS_Translator_Application_ID</code> (with ordinal 44520) */
            SIMNET_DIS_TRANSLATOR_APPLICATION_ID = 44520,
            /** <code>SIMNET-DIS_Translator_Entity_ID</code> (with ordinal 44530) */
            SIMNET_DIS_TRANSLATOR_ENTITY_ID = 44530,
            /** <code>SIMNET-DIS_Translator_Active_Status</code> (with ordinal 44540) */
            SIMNET_DIS_TRANSLATOR_ACTIVE_STATUS = 44540,
            /** <code>Application_Rate</code> (with ordinal 45000) */
            APPLICATION_RATE = 45000,
            /** <code>Application_Time</code> (with ordinal 45005) */
            APPLICATION_TIME = 45005,
            /** <code>Application_Timestep</code> (with ordinal 45010) */
            APPLICATION_TIMESTEP = 45010,
            /** <code>Feedback_Time</code> (with ordinal 45020) */
            FEEDBACK_TIME = 45020,
            /** <code>Simulation_Rate</code> (with ordinal 45030) */
            SIMULATION_RATE = 45030,
            /** <code>Simulation_Time</code> (with ordinal 45040) */
            SIMULATION_TIME = 45040,
            /** <code>Simulation_Timestep</code> (with ordinal 45050) */
            SIMULATION_TIMESTEP = 45050,
            /** <code>Time_Interval</code> (with ordinal 45060) */
            TIME_INTERVAL = 45060,
            /** <code>Time_Latency</code> (with ordinal 45070) */
            TIME_LATENCY = 45070,
            /** <code>Time_Scheme</code> (with ordinal 45080) */
            TIME_SCHEME = 45080,
            /** <code>Exercise_Elapsed_Time</code> (with ordinal 46000) */
            EXERCISE_ELAPSED_TIME = 46000,
            /** <code>Elapsed_Time</code> (with ordinal 46010) */
            ELAPSED_TIME = 46010,
            /** <code>Environment</code> (with ordinal 50000) */
            ENVIRONMENT = 50000,
            /** <code>Weather</code> (with ordinal 51000) */
            WEATHER = 51000,
            /** <code>Weather_Condition</code> (with ordinal 51010) */
            WEATHER_CONDITION = 51010,
            /** <code>Thermal_Condition</code> (with ordinal 51100) */
            THERMAL_CONDITION = 51100,
            /** <code>Thermal_Visibility_FloatingPoint32</code> (with ordinal 51110) */
            THERMAL_VISIBILITY_FLOATING_POINT32 = 51110,
            /** <code>Thermal_Visibility_UnsignedInteger32</code> (with ordinal 51111) */
            THERMAL_VISIBILITY_UNSIGNED_INTEGER32 = 51111,
            /** <code>Time</code> (with ordinal 52000) */
            TIME = 52000,
            /** <code>Time_String</code> (with ordinal 52001) */
            TIME_STRING = 52001,
            /** <code>Time_of_Day-Discrete</code> (with ordinal 52100) */
            TIME_OF_DAY_DISCRETE = 52100,
            /** <code>Time_of_Day-Continuous</code> (with ordinal 52200) */
            TIME_OF_DAY_CONTINUOUS = 52200,
            /** <code>Time_Mode</code> (with ordinal 52300) */
            TIME_MODE = 52300,
            /** <code>Time_Scene</code> (with ordinal 52305) */
            TIME_SCENE = 52305,
            /** <code>Current_Hour</code> (with ordinal 52310) */
            CURRENT_HOUR = 52310,
            /** <code>Current_Minute</code> (with ordinal 52320) */
            CURRENT_MINUTE = 52320,
            /** <code>Current_Second</code> (with ordinal 52330) */
            CURRENT_SECOND = 52330,
            /** <code>Azimuth</code> (with ordinal 52340) */
            AZIMUTH = 52340,
            /** <code>Maximum_Elevation</code> (with ordinal 52350) */
            MAXIMUM_ELEVATION = 52350,
            /** <code>Time_Zone</code> (with ordinal 52360) */
            TIME_ZONE = 52360,
            /** <code>Time_Rate</code> (with ordinal 52370) */
            TIME_RATE = 52370,
            /** <code>Simulation_Time_2</code> (with ordinal 52380) */
            SIMULATION_TIME_2 = 52380,
            /** <code>Time_Sunrise_Enabled</code> (with ordinal 52400) */
            TIME_SUNRISE_ENABLED = 52400,
            /** <code>Sunrise_Hour</code> (with ordinal 52410) */
            SUNRISE_HOUR = 52410,
            /** <code>Sunrise_Minute</code> (with ordinal 52420) */
            SUNRISE_MINUTE = 52420,
            /** <code>Sunrise_Second</code> (with ordinal 52430) */
            SUNRISE_SECOND = 52430,
            /** <code>Sunrise_Azimuth</code> (with ordinal 52440) */
            SUNRISE_AZIMUTH = 52440,
            /** <code>Time_Sunset_Enabled</code> (with ordinal 52500) */
            TIME_SUNSET_ENABLED = 52500,
            /** <code>Sunset_Hour</code> (with ordinal 52510) */
            SUNSET_HOUR = 52510,
            /** <code>Sunset_Hour_2</code> (with ordinal 52511) */
            SUNSET_HOUR_2 = 52511,
            /** <code>Sunset_Minute</code> (with ordinal 52520) */
            SUNSET_MINUTE = 52520,
            /** <code>Sunset_Second</code> (with ordinal 52530) */
            SUNSET_SECOND = 52530,
            /** <code>Date</code> (with ordinal 52600) */
            DATE = 52600,
            /** <code>Date_European</code> (with ordinal 52601) */
            DATE_EUROPEAN = 52601,
            /** <code>Date_US</code> (with ordinal 52602) */
            DATE_US = 52602,
            /** <code>Month</code> (with ordinal 52610) */
            MONTH = 52610,
            /** <code>Day</code> (with ordinal 52620) */
            DAY = 52620,
            /** <code>Year</code> (with ordinal 52630) */
            YEAR = 52630,
            /** <code>Clouds</code> (with ordinal 53000) */
            CLOUDS = 53000,
            /** <code>Cloud_Layer_Enable</code> (with ordinal 53050) */
            CLOUD_LAYER_ENABLE = 53050,
            /** <code>Cloud_Layer_Selection</code> (with ordinal 53060) */
            CLOUD_LAYER_SELECTION = 53060,
            /** <code>Cloud_Visibility</code> (with ordinal 53100) */
            CLOUD_VISIBILITY = 53100,
            /** <code>Base_Altitude-Meters</code> (with ordinal 53200) */
            BASE_ALTITUDE_METERS = 53200,
            /** <code>Base_Altitude-Feet</code> (with ordinal 53250) */
            BASE_ALTITUDE_FEET = 53250,
            /** <code>Ceiling-Meters</code> (with ordinal 53300) */
            CEILING_METERS = 53300,
            /** <code>Ceiling-Feet</code> (with ordinal 53350) */
            CEILING_FEET = 53350,
            /** <code>Characteristics</code> (with ordinal 53400) */
            CHARACTERISTICS = 53400,
            /** <code>Concentration_Length</code> (with ordinal 53410) */
            CONCENTRATION_LENGTH = 53410,
            /** <code>Transmittance</code> (with ordinal 53420) */
            TRANSMITTANCE = 53420,
            /** <code>Radiance</code> (with ordinal 53430) */
            RADIANCE = 53430,
            /** <code>Precipitation</code> (with ordinal 54000) */
            PRECIPITATION = 54000,
            /** <code>Rain</code> (with ordinal 54100) */
            RAIN = 54100,
            /** <code>Fog</code> (with ordinal 55000) */
            FOG = 55000,
            /** <code>Visibility-Meters</code> (with ordinal 55100) */
            VISIBILITY_METERS = 55100,
            /** <code>Visibility-Meters_UnsignedInteger32</code> (with ordinal 55101) */
            VISIBILITY_METERS_UNSIGNED_INTEGER32 = 55101,
            /** <code>Visibility-Miles</code> (with ordinal 55105) */
            VISIBILITY_MILES = 55105,
            /** <code>Fog_Density</code> (with ordinal 55200) */
            FOG_DENSITY = 55200,
            /** <code>Base</code> (with ordinal 55300) */
            BASE = 55300,
            /** <code>View_Layer_from_above</code> (with ordinal 55401) */
            VIEW_LAYER_FROM_ABOVE = 55401,
            /** <code>Transition_Range</code> (with ordinal 55410) */
            TRANSITION_RANGE = 55410,
            /** <code>Bottom-Meters</code> (with ordinal 55420) */
            BOTTOM_METERS = 55420,
            /** <code>Bottom-Feet</code> (with ordinal 55425) */
            BOTTOM_FEET = 55425,
            /** <code>Fog_Ceiling-Meters</code> (with ordinal 55430) */
            FOG_CEILING_METERS = 55430,
            /** <code>Fog_Ceiling-Feet</code> (with ordinal 55435) */
            FOG_CEILING_FEET = 55435,
            /** <code>Heavenly_Bodies</code> (with ordinal 56000) */
            HEAVENLY_BODIES = 56000,
            /** <code>Sun</code> (with ordinal 56100) */
            SUN = 56100,
            /** <code>Sun-Visible</code> (with ordinal 56105) */
            SUN_VISIBLE = 56105,
            /** <code>Sun-Position</code> (with ordinal 56110) */
            SUN_POSITION = 56110,
            /** <code>Sun-Position_Elevation-Degrees</code> (with ordinal 56111) */
            SUN_POSITION_ELEVATION_DEGREES = 56111,
            /** <code>Sun-Position_Azimuth</code> (with ordinal 56120) */
            SUN_POSITION_AZIMUTH = 56120,
            /** <code>Sun-Position_Azimuth-Degrees</code> (with ordinal 56121) */
            SUN_POSITION_AZIMUTH_DEGREES = 56121,
            /** <code>Sun-Position_Elevation</code> (with ordinal 56130) */
            SUN_POSITION_ELEVATION = 56130,
            /** <code>Sun-Position_Intensity</code> (with ordinal 56140) */
            SUN_POSITION_INTENSITY = 56140,
            /** <code>Moon</code> (with ordinal 56200) */
            MOON = 56200,
            /** <code>Moon-Visible</code> (with ordinal 56205) */
            MOON_VISIBLE = 56205,
            /** <code>Moon-Position</code> (with ordinal 56210) */
            MOON_POSITION = 56210,
            /** <code>Moon-Position_Azimuth</code> (with ordinal 56220) */
            MOON_POSITION_AZIMUTH = 56220,
            /** <code>Moon-Position_Azimuth-Degrees</code> (with ordinal 56221) */
            MOON_POSITION_AZIMUTH_DEGREES = 56221,
            /** <code>Moon-Position_Elevation</code> (with ordinal 56230) */
            MOON_POSITION_ELEVATION = 56230,
            /** <code>Moon-Position_Elevation-Degrees</code> (with ordinal 56231) */
            MOON_POSITION_ELEVATION_DEGREES = 56231,
            /** <code>Moon-Position_Intensity</code> (with ordinal 56240) */
            MOON_POSITION_INTENSITY = 56240,
            /** <code>Horizon</code> (with ordinal 56310) */
            HORIZON = 56310,
            /** <code>Horizon_Azimuth</code> (with ordinal 56320) */
            HORIZON_AZIMUTH = 56320,
            /** <code>Horizon_Elevation</code> (with ordinal 56330) */
            HORIZON_ELEVATION = 56330,
            /** <code>Horizon_Heading</code> (with ordinal 56340) */
            HORIZON_HEADING = 56340,
            /** <code>Horizon_Intensity</code> (with ordinal 56350) */
            HORIZON_INTENSITY = 56350,
            /** <code>Humidity</code> (with ordinal 57200) */
            HUMIDITY = 57200,
            /** <code>Visibility</code> (with ordinal 57300) */
            VISIBILITY = 57300,
            /** <code>Winds</code> (with ordinal 57400) */
            WINDS = 57400,
            /** <code>Speed_2</code> (with ordinal 57410) */
            SPEED_2 = 57410,
            /** <code>Wind-Speed-Knots</code> (with ordinal 57411) */
            WIND_SPEED_KNOTS = 57411,
            /** <code>Wind-Direction</code> (with ordinal 57420) */
            WIND_DIRECTION = 57420,
            /** <code>Wind-Direction-Degrees</code> (with ordinal 57421) */
            WIND_DIRECTION_DEGREES = 57421,
            /** <code>Rainsoak</code> (with ordinal 57500) */
            RAINSOAK = 57500,
            /** <code>Tide-Speed</code> (with ordinal 57610) */
            TIDE_SPEED = 57610,
            /** <code>Tide-Speed-Knots</code> (with ordinal 57611) */
            TIDE_SPEED_KNOTS = 57611,
            /** <code>Tide-Direction</code> (with ordinal 57620) */
            TIDE_DIRECTION = 57620,
            /** <code>Tide-Direction-Degrees</code> (with ordinal 57621) */
            TIDE_DIRECTION_DEGREES = 57621,
            /** <code>Haze</code> (with ordinal 58000) */
            HAZE = 58000,
            /** <code>Haze_Visibility-Meters</code> (with ordinal 58100) */
            HAZE_VISIBILITY_METERS = 58100,
            /** <code>Haze_Visibility-Miles</code> (with ordinal 58105) */
            HAZE_VISIBILITY_MILES = 58105,
            /** <code>Haze_Density</code> (with ordinal 58200) */
            HAZE_DENSITY = 58200,
            /** <code>Haze_Ceiling-Meters</code> (with ordinal 58430) */
            HAZE_CEILING_METERS = 58430,
            /** <code>Haze_Ceiling-Feet</code> (with ordinal 58435) */
            HAZE_CEILING_FEET = 58435,
            /** <code>Contaminants_and_Obscurants</code> (with ordinal 59000) */
            CONTAMINANTS_AND_OBSCURANTS = 59000,
            /** <code>Contaminant_Obscurant_Type</code> (with ordinal 59100) */
            CONTAMINANT_OBSCURANT_TYPE = 59100,
            /** <code>Persistence</code> (with ordinal 59110) */
            PERSISTENCE = 59110,
            /** <code>Chemical_Dosage</code> (with ordinal 59115) */
            CHEMICAL_DOSAGE = 59115,
            /** <code>Chemical_Air_Concentration</code> (with ordinal 59120) */
            CHEMICAL_AIR_CONCENTRATION = 59120,
            /** <code>Chemical_Ground_Deposition</code> (with ordinal 59125) */
            CHEMICAL_GROUND_DEPOSITION = 59125,
            /** <code>Chemical_Maximum_Ground_Deposition</code> (with ordinal 59130) */
            CHEMICAL_MAXIMUM_GROUND_DEPOSITION = 59130,
            /** <code>Chemical_Dosage_Threshold</code> (with ordinal 59135) */
            CHEMICAL_DOSAGE_THRESHOLD = 59135,
            /** <code>Biological_Dosage</code> (with ordinal 59140) */
            BIOLOGICAL_DOSAGE = 59140,
            /** <code>Biological_Air_Concentration</code> (with ordinal 59145) */
            BIOLOGICAL_AIR_CONCENTRATION = 59145,
            /** <code>Biological_Dosage_Threshold</code> (with ordinal 59150) */
            BIOLOGICAL_DOSAGE_THRESHOLD = 59150,
            /** <code>Biological_Binned_Particle_Count</code> (with ordinal 59155) */
            BIOLOGICAL_BINNED_PARTICLE_COUNT = 59155,
            /** <code>Radiological_Dosage</code> (with ordinal 59160) */
            RADIOLOGICAL_DOSAGE = 59160,
            /** <code>Communications</code> (with ordinal 60000) */
            COMMUNICATIONS = 60000,
            /** <code>Channel_Type</code> (with ordinal 61100) */
            CHANNEL_TYPE = 61100,
            /** <code>Channel_Type_2</code> (with ordinal 61101) */
            CHANNEL_TYPE_2 = 61101,
            /** <code>Channel_Identification</code> (with ordinal 61200) */
            CHANNEL_IDENTIFICATION = 61200,
            /** <code>Alpha_Identification</code> (with ordinal 61300) */
            ALPHA_IDENTIFICATION = 61300,
            /** <code>Radio_Identification</code> (with ordinal 61400) */
            RADIO_IDENTIFICATION = 61400,
            /** <code>Land_Line_Identification</code> (with ordinal 61500) */
            LAND_LINE_IDENTIFICATION = 61500,
            /** <code>Intercom_Identification</code> (with ordinal 61600) */
            INTERCOM_IDENTIFICATION = 61600,
            /** <code>Group_Network_Channel_Number</code> (with ordinal 61700) */
            GROUP_NETWORK_CHANNEL_NUMBER = 61700,
            /** <code>Radio_Communications_Status</code> (with ordinal 62100) */
            RADIO_COMMUNICATIONS_STATUS = 62100,
            /** <code>Stationary_Radio_Transmitters_Default_Time</code> (with ordinal 62200) */
            STATIONARY_RADIO_TRANSMITTERS_DEFAULT_TIME = 62200,
            /** <code>Moving_Radio_Transmitters_Default_Time</code> (with ordinal 62300) */
            MOVING_RADIO_TRANSMITTERS_DEFAULT_TIME = 62300,
            /** <code>Stationary_Radio_Signals_Default_Time</code> (with ordinal 62400) */
            STATIONARY_RADIO_SIGNALS_DEFAULT_TIME = 62400,
            /** <code>Moving_Radio_Signal_Default_Time</code> (with ordinal 62500) */
            MOVING_RADIO_SIGNAL_DEFAULT_TIME = 62500,
            /** <code>Radio_Initialization_Transec_Security_Key</code> (with ordinal 63101) */
            RADIO_INITIALIZATION_TRANSEC_SECURITY_KEY = 63101,
            /** <code>Radio_Initialization_Internal_Noise_Level</code> (with ordinal 63102) */
            RADIO_INITIALIZATION_INTERNAL_NOISE_LEVEL = 63102,
            /** <code>Radio_Initialization_Squelch_Threshold</code> (with ordinal 63103) */
            RADIO_INITIALIZATION_SQUELCH_THRESHOLD = 63103,
            /** <code>Radio_Initialization_Antenna_Location</code> (with ordinal 63104) */
            RADIO_INITIALIZATION_ANTENNA_LOCATION = 63104,
            /** <code>Radio_Initialization_Antenna_Pattern_Type</code> (with ordinal 63105) */
            RADIO_INITIALIZATION_ANTENNA_PATTERN_TYPE = 63105,
            /** <code>Radio_Initialization_Antenna_Pattern_Length</code> (with ordinal 63106) */
            RADIO_INITIALIZATION_ANTENNA_PATTERN_LENGTH = 63106,
            /** <code>Radio_Initialization_Beam_Definition</code> (with ordinal 63107) */
            RADIO_INITIALIZATION_BEAM_DEFINITION = 63107,
            /** <code>Radio_Initialization_Transmit_Heartbeat_Time</code> (with ordinal 63108) */
            RADIO_INITIALIZATION_TRANSMIT_HEARTBEAT_TIME = 63108,
            /** <code>Radio_Initialization_Transmit_Distance_Threshold</code> (with ordinal 63109) */
            RADIO_INITIALIZATION_TRANSMIT_DISTANCE_THRESHOLD = 63109,
            /** <code>Radio_Channel_Initialization_Lockout_ID</code> (with ordinal 63110) */
            RADIO_CHANNEL_INITIALIZATION_LOCKOUT_ID = 63110,
            /** <code>Radio_Channel_Initialization_Hopset_ID</code> (with ordinal 63111) */
            RADIO_CHANNEL_INITIALIZATION_HOPSET_ID = 63111,
            /** <code>Radio_Channel_Initialization_Preset_Frequency</code> (with ordinal 63112) */
            RADIO_CHANNEL_INITIALIZATION_PRESET_FREQUENCY = 63112,
            /** <code>Radio_Channel_Initialization_Frequency_Sync_Time</code> (with ordinal 63113) */
            RADIO_CHANNEL_INITIALIZATION_FREQUENCY_SYNC_TIME = 63113,
            /** <code>Radio_Channel_Initialization_Comsec_Key</code> (with ordinal 63114) */
            RADIO_CHANNEL_INITIALIZATION_COMSEC_KEY = 63114,
            /** <code>Radio_Channel_Initialization_Alpha</code> (with ordinal 63115) */
            RADIO_CHANNEL_INITIALIZATION_ALPHA = 63115,
            /** <code>Algorithm_Parameters</code> (with ordinal 70000) */
            ALGORITHM_PARAMETERS = 70000,
            /** <code>Dead_Reckoning_Algorithm__DRA_</code> (with ordinal 71000) */
            DEAD_RECKONING_ALGORITHM_DRA = 71000,
            /** <code>DRA_Location_Threshold</code> (with ordinal 71100) */
            DRA_LOCATION_THRESHOLD = 71100,
            /** <code>DRA_Orientation_Threshold</code> (with ordinal 71200) */
            DRA_ORIENTATION_THRESHOLD = 71200,
            /** <code>DRA_Time_Threshold</code> (with ordinal 71300) */
            DRA_TIME_THRESHOLD = 71300,
            /** <code>Simulation_Management_Parameters</code> (with ordinal 72000) */
            SIMULATION_MANAGEMENT_PARAMETERS = 72000,
            /** <code>Checkpoint_Interval</code> (with ordinal 72100) */
            CHECKPOINT_INTERVAL = 72100,
            /** <code>Transmitter_Time_Threshold</code> (with ordinal 72600) */
            TRANSMITTER_TIME_THRESHOLD = 72600,
            /** <code>Receiver_Time_Threshold</code> (with ordinal 72700) */
            RECEIVER_TIME_THRESHOLD = 72700,
            /** <code>Interoperability_Mode</code> (with ordinal 73000) */
            INTEROPERABILITY_MODE = 73000,
            /** <code>SIMNET_Data_Collection</code> (with ordinal 74000) */
            SIMNET_DATA_COLLECTION = 74000,
            /** <code>Event_ID</code> (with ordinal 75000) */
            EVENT_ID = 75000,
            /** <code>Source_Site_ID</code> (with ordinal 75100) */
            SOURCE_SITE_ID = 75100,
            /** <code>Source_Host_ID</code> (with ordinal 75200) */
            SOURCE_HOST_ID = 75200,
            /** <code>Articulated_Parts</code> (with ordinal 90000) */
            ARTICULATED_PARTS = 90000,
            /** <code>Articulated_Parts-Part_ID</code> (with ordinal 90050) */
            ARTICULATED_PARTS_PART_ID = 90050,
            /** <code>Articulated_Parts-Index</code> (with ordinal 90070) */
            ARTICULATED_PARTS_INDEX = 90070,
            /** <code>Articulated_Parts-Position</code> (with ordinal 90100) */
            ARTICULATED_PARTS_POSITION = 90100,
            /** <code>Articulated_Parts-Position_Rate</code> (with ordinal 90200) */
            ARTICULATED_PARTS_POSITION_RATE = 90200,
            /** <code>Articulated_Parts-Extension</code> (with ordinal 90300) */
            ARTICULATED_PARTS_EXTENSION = 90300,
            /** <code>Articulated_Parts-Extension_Rate</code> (with ordinal 90400) */
            ARTICULATED_PARTS_EXTENSION_RATE = 90400,
            /** <code>Articulated_Parts-X</code> (with ordinal 90500) */
            ARTICULATED_PARTS_X = 90500,
            /** <code>Articulated_Parts-X-rate</code> (with ordinal 90600) */
            ARTICULATED_PARTS_X_RATE = 90600,
            /** <code>Articulated_Parts-Y</code> (with ordinal 90700) */
            ARTICULATED_PARTS_Y = 90700,
            /** <code>Articulated_Parts-Y-rate</code> (with ordinal 90800) */
            ARTICULATED_PARTS_Y_RATE = 90800,
            /** <code>Articulated_Parts-Z</code> (with ordinal 90900) */
            ARTICULATED_PARTS_Z = 90900,
            /** <code>Articulated_Parts-Z-rate</code> (with ordinal 91000) */
            ARTICULATED_PARTS_Z_RATE = 91000,
            /** <code>Articulated_Parts-Azimuth</code> (with ordinal 91100) */
            ARTICULATED_PARTS_AZIMUTH = 91100,
            /** <code>Articulated_Parts-Azimuth_Rate</code> (with ordinal 91200) */
            ARTICULATED_PARTS_AZIMUTH_RATE = 91200,
            /** <code>Articulated_Parts-Elevation</code> (with ordinal 91300) */
            ARTICULATED_PARTS_ELEVATION = 91300,
            /** <code>Articulated_Parts-Elevation_Rate</code> (with ordinal 91400) */
            ARTICULATED_PARTS_ELEVATION_RATE = 91400,
            /** <code>Articulated_Parts-Rotation</code> (with ordinal 91500) */
            ARTICULATED_PARTS_ROTATION = 91500,
            /** <code>Articulated_Parts-Rotation_Rate</code> (with ordinal 91600) */
            ARTICULATED_PARTS_ROTATION_RATE = 91600,
            /** <code>DRA_Angular_X-Velocity</code> (with ordinal 100001) */
            DRA_ANGULAR_X_VELOCITY = 100001,
            /** <code>DRA_Angular_Y-Velocity</code> (with ordinal 100002) */
            DRA_ANGULAR_Y_VELOCITY = 100002,
            /** <code>DRA_Angular_Z-Velocity</code> (with ordinal 100003) */
            DRA_ANGULAR_Z_VELOCITY = 100003,
            /** <code>Appearance-Trailing_Effects</code> (with ordinal 100004) */
            APPEARANCE_TRAILING_EFFECTS = 100004,
            /** <code>Appearance-Hatch</code> (with ordinal 100005) */
            APPEARANCE_HATCH = 100005,
            /** <code>Appearance-Character_Set</code> (with ordinal 100008) */
            APPEARANCE_CHARACTER_SET = 100008,
            /** <code>Capability-Ammunition_Supplier</code> (with ordinal 100010) */
            CAPABILITY_AMMUNITION_SUPPLIER = 100010,
            /** <code>Capability-Miscellaneous_Supplier</code> (with ordinal 100011) */
            CAPABILITY_MISCELLANEOUS_SUPPLIER = 100011,
            /** <code>Capability-Repair_Provider</code> (with ordinal 100012) */
            CAPABILITY_REPAIR_PROVIDER = 100012,
            /** <code>Articulation_Parameter</code> (with ordinal 100014) */
            ARTICULATION_PARAMETER = 100014,
            /** <code>Articulation_Parameter_Type</code> (with ordinal 100047) */
            ARTICULATION_PARAMETER_TYPE = 100047,
            /** <code>Articulation_Parameter_Value</code> (with ordinal 100048) */
            ARTICULATION_PARAMETER_VALUE = 100048,
            /** <code>Time_of_Day-Scene</code> (with ordinal 100058) */
            TIME_OF_DAY_SCENE = 100058,
            /** <code>Latitude-North__Location_of_weather_cell_</code> (with ordinal 100061) */
            LATITUDE_NORTH_LOCATION_OF_WEATHER_CELL = 100061,
            /** <code>Longitude-East__Location_of_weather_cell_</code> (with ordinal 100063) */
            LONGITUDE_EAST_LOCATION_OF_WEATHER_CELL = 100063,
            /** <code>Tactical_Driver_Status</code> (with ordinal 100068) */
            TACTICAL_DRIVER_STATUS = 100068,
            /** <code>Sonar_System_Status</code> (with ordinal 100100) */
            SONAR_SYSTEM_STATUS = 100100,
            /** <code>AccomplishedAccept</code> (with ordinal 100160) */
            ACCOMPLISHED_ACCEPT = 100160,
            /** <code>Upper_latitude</code> (with ordinal 100161) */
            UPPER_LATITUDE = 100161,
            /** <code>Latitude-South__Location_of_weather_cell_</code> (with ordinal 100162) */
            LATITUDE_SOUTH_LOCATION_OF_WEATHER_CELL = 100162,
            /** <code>WesternLongitude</code> (with ordinal 100163) */
            WESTERN_LONGITUDE = 100163,
            /** <code>Longitude-West__location_of_weather_cell_</code> (with ordinal 100164) */
            LONGITUDE_WEST_LOCATION_OF_WEATHER_CELL = 100164,
            /** <code>CDROMNumber_DiskIDForTerrain_</code> (with ordinal 100165) */
            CDROMNUMBER_DISK_IDFOR_TERRAIN = 100165,
            /** <code>DTEDDiskID</code> (with ordinal 100166) */
            DTEDDISK_ID = 100166,
            /** <code>Altitude_1</code> (with ordinal 100167) */
            ALTITUDE_1 = 100167,
            /** <code>Tactical_System_Status</code> (with ordinal 100169) */
            TACTICAL_SYSTEM_STATUS = 100169,
            /** <code>JTIDS_Status</code> (with ordinal 100170) */
            JTIDS_STATUS = 100170,
            /** <code>TADIL-J_Status</code> (with ordinal 100171) */
            TADIL_J_STATUS = 100171,
            /** <code>DSDD_Status</code> (with ordinal 100172) */
            DSDD_STATUS = 100172,
            /** <code>Weapon_System_Status</code> (with ordinal 100200) */
            WEAPON_SYSTEM_STATUS = 100200,
            /** <code>Subsystem_status</code> (with ordinal 100205) */
            SUBSYSTEM_STATUS = 100205,
            /** <code>Number_of_interceptors_fired</code> (with ordinal 100206) */
            NUMBER_OF_INTERCEPTORS_FIRED = 100206,
            /** <code>Number_of_interceptor_detonations</code> (with ordinal 100207) */
            NUMBER_OF_INTERCEPTOR_DETONATIONS = 100207,
            /** <code>Number_of_message_buffers_dropped</code> (with ordinal 100208) */
            NUMBER_OF_MESSAGE_BUFFERS_DROPPED = 100208,
            /** <code>Satellite_sensor_background__year_day_</code> (with ordinal 100213) */
            SATELLITE_SENSOR_BACKGROUND_YEAR_DAY = 100213,
            /** <code>Satellite_sensor_background__hour_minute_</code> (with ordinal 100214) */
            SATELLITE_SENSOR_BACKGROUND_HOUR_MINUTE = 100214,
            /** <code>Script_Number</code> (with ordinal 100218) */
            SCRIPT_NUMBER = 100218,
            /** <code>Entity_Track_Update_Data</code> (with ordinal 100300) */
            ENTITY_TRACK_UPDATE_DATA = 100300,
            /** <code>Local_Force_Training</code> (with ordinal 100400) */
            LOCAL_FORCE_TRAINING = 100400,
            /** <code>Entity_Track_Identity_Data</code> (with ordinal 100500) */
            ENTITY_TRACK_IDENTITY_DATA = 100500,
            /** <code>Entity_for_Track_Event</code> (with ordinal 100510) */
            ENTITY_FOR_TRACK_EVENT = 100510,
            /** <code>IFF__Friend-Foe__status</code> (with ordinal 100520) */
            IFF_FRIEND_FOE_STATUS = 100520,
            /** <code>Engagement_Data</code> (with ordinal 100600) */
            ENGAGEMENT_DATA = 100600,
            /** <code>Target_Latitude</code> (with ordinal 100610) */
            TARGET_LATITUDE = 100610,
            /** <code>Target_Longitude</code> (with ordinal 100620) */
            TARGET_LONGITUDE = 100620,
            /** <code>Area_of_Interest__Ground_Impact_Circle__Center_Latitude</code> (with ordinal 100631) */
            AREA_OF_INTEREST_GROUND_IMPACT_CIRCLE_CENTER_LATITUDE = 100631,
            /** <code>Area_of_Interest__Ground_Impact_Circle__Center_Longitude</code> (with ordinal 100632) */
            AREA_OF_INTEREST_GROUND_IMPACT_CIRCLE_CENTER_LONGITUDE = 100632,
            /** <code>Area_of_Interest__Ground_Impact_Circle__Radius</code> (with ordinal 100633) */
            AREA_OF_INTEREST_GROUND_IMPACT_CIRCLE_RADIUS = 100633,
            /** <code>Area_of_Interest_Type</code> (with ordinal 100634) */
            AREA_OF_INTEREST_TYPE = 100634,
            /** <code>Target_Aggregate_ID</code> (with ordinal 100640) */
            TARGET_AGGREGATE_ID = 100640,
            /** <code>GIC_Identification_Number</code> (with ordinal 100650) */
            GIC_IDENTIFICATION_NUMBER = 100650,
            /** <code>Estimated_Time_of_Flight_to_TBM_Impact</code> (with ordinal 100660) */
            ESTIMATED_TIME_OF_FLIGHT_TO_TBM_IMPACT = 100660,
            /** <code>Estimated_Intercept_Time</code> (with ordinal 100661) */
            ESTIMATED_INTERCEPT_TIME = 100661,
            /** <code>Estimated_Time_of_Flight_to_Next_Waypoint</code> (with ordinal 100662) */
            ESTIMATED_TIME_OF_FLIGHT_TO_NEXT_WAYPOINT = 100662,
            /** <code>Entity_Track_Equipment_Data</code> (with ordinal 100700) */
            ENTITY_TRACK_EQUIPMENT_DATA = 100700,
            /** <code>Emission_EW_Data</code> (with ordinal 100800) */
            EMISSION_EW_DATA = 100800,
            /** <code>Appearance_Data</code> (with ordinal 100900) */
            APPEARANCE_DATA = 100900,
            /** <code>Command_Order_Data</code> (with ordinal 101000) */
            COMMAND_ORDER_DATA = 101000,
            /** <code>Environmental_Data</code> (with ordinal 101100) */
            ENVIRONMENTAL_DATA = 101100,
            /** <code>Significant_Event_Data</code> (with ordinal 101200) */
            SIGNIFICANT_EVENT_DATA = 101200,
            /** <code>Operator_Action_Data</code> (with ordinal 101300) */
            OPERATOR_ACTION_DATA = 101300,
            /** <code>ADA_Engagement_Mode</code> (with ordinal 101310) */
            ADA_ENGAGEMENT_MODE = 101310,
            /** <code>ADA_Shooting_Status</code> (with ordinal 101320) */
            ADA_SHOOTING_STATUS = 101320,
            /** <code>ADA_Mode</code> (with ordinal 101321) */
            ADA_MODE = 101321,
            /** <code>ADA_Radar_Status</code> (with ordinal 101330) */
            ADA_RADAR_STATUS = 101330,
            /** <code>Shoot_Command</code> (with ordinal 101340) */
            SHOOT_COMMAND = 101340,
            /** <code>ADA_Weapon_Status</code> (with ordinal 101350) */
            ADA_WEAPON_STATUS = 101350,
            /** <code>ADA_Firing_Disciple</code> (with ordinal 101360) */
            ADA_FIRING_DISCIPLE = 101360,
            /** <code>Order_Status</code> (with ordinal 101370) */
            ORDER_STATUS = 101370,
            /** <code>Time_Synchronization</code> (with ordinal 101400) */
            TIME_SYNCHRONIZATION = 101400,
            /** <code>Tomahawk_Data</code> (with ordinal 101500) */
            TOMAHAWK_DATA = 101500,
            /** <code>Number_of_Detonations</code> (with ordinal 102100) */
            NUMBER_OF_DETONATIONS = 102100,
            /** <code>Number_of_Intercepts</code> (with ordinal 102200) */
            NUMBER_OF_INTERCEPTS = 102200,
            /** <code>OBT_Control_MT-201</code> (with ordinal 200201) */
            OBT_CONTROL_MT_201 = 200201,
            /** <code>Sensor_Data_MT-202</code> (with ordinal 200202) */
            SENSOR_DATA_MT_202 = 200202,
            /** <code>Environmental_Data_MT-203</code> (with ordinal 200203) */
            ENVIRONMENTAL_DATA_MT_203 = 200203,
            /** <code>Ownship_Data_MT-204</code> (with ordinal 200204) */
            OWNSHIP_DATA_MT_204 = 200204,
            /** <code>Acoustic_Contact_Data_MT-205</code> (with ordinal 200205) */
            ACOUSTIC_CONTACT_DATA_MT_205 = 200205,
            /** <code>Sonobuoy_Data_MT-207</code> (with ordinal 200207) */
            SONOBUOY_DATA_MT_207 = 200207,
            /** <code>Sonobuoy_Contact_Data_MT-210</code> (with ordinal 200210) */
            SONOBUOY_CONTACT_DATA_MT_210 = 200210,
            /** <code>Helo_Control_MT-211</code> (with ordinal 200211) */
            HELO_CONTROL_MT_211 = 200211,
            /** <code>ESM_Control_Data</code> (with ordinal 200213) */
            ESM_CONTROL_DATA = 200213,
            /** <code>ESM_Contact_Data_MT-214</code> (with ordinal 200214) */
            ESM_CONTACT_DATA_MT_214 = 200214,
            /** <code>ESM_Emitter_Data_MT-215</code> (with ordinal 200215) */
            ESM_EMITTER_DATA_MT_215 = 200215,
            /** <code>Weapon_Definition_Data_MT-217</code> (with ordinal 200216) */
            WEAPON_DEFINITION_DATA_MT_217 = 200216,
            /** <code>Weapon_Preset_Data_MT-217</code> (with ordinal 200217) */
            WEAPON_PRESET_DATA_MT_217 = 200217,
            /** <code>OBT_Control_MT-301</code> (with ordinal 200301) */
            OBT_CONTROL_MT_301 = 200301,
            /** <code>Sensor_Data_MT-302</code> (with ordinal 200302) */
            SENSOR_DATA_MT_302 = 200302,
            /** <code>Environmental_Data_MT-303m</code> (with ordinal 200303) */
            ENVIRONMENTAL_DATA_MT_303M = 200303,
            /** <code>Ownship_Data_MT-304</code> (with ordinal 200304) */
            OWNSHIP_DATA_MT_304 = 200304,
            /** <code>Acoustic_Contact_Data_MT-305</code> (with ordinal 200305) */
            ACOUSTIC_CONTACT_DATA_MT_305 = 200305,
            /** <code>Sonobuoy_Data_MT-307</code> (with ordinal 200307) */
            SONOBUOY_DATA_MT_307 = 200307,
            /** <code>Sonobuoy_Contact_Data_MT-310</code> (with ordinal 200310) */
            SONOBUOY_CONTACT_DATA_MT_310 = 200310,
            /** <code>Helo_Scenario___Equipment_Status</code> (with ordinal 200311) */
            HELO_SCENARIO_EQUIPMENT_STATUS = 200311,
            /** <code>ESM_Control_Data_MT-313</code> (with ordinal 200313) */
            ESM_CONTROL_DATA_MT_313 = 200313,
            /** <code>ESM_Contact_Data_MT-314</code> (with ordinal 200314) */
            ESM_CONTACT_DATA_MT_314 = 200314,
            /** <code>ESM_Emitter_Data_MT-315</code> (with ordinal 200315) */
            ESM_EMITTER_DATA_MT_315 = 200315,
            /** <code>Weapon_Definition_Data_MT-316</code> (with ordinal 200316) */
            WEAPON_DEFINITION_DATA_MT_316 = 200316,
            /** <code>Weapon_Preset_Data_MT-317</code> (with ordinal 200317) */
            WEAPON_PRESET_DATA_MT_317 = 200317,
            /** <code>Pairing_Association__eMT-56_</code> (with ordinal 200400) */
            PAIRING_ASSOCIATION_E_MT_56 = 200400,
            /** <code>Pointer__eMT-57_</code> (with ordinal 200401) */
            POINTER_E_MT_57 = 200401,
            /** <code>Reporting_Responsibility__eMT-58_</code> (with ordinal 200402) */
            REPORTING_RESPONSIBILITY_E_MT_58 = 200402,
            /** <code>Track_Number__eMT-59_</code> (with ordinal 200403) */
            TRACK_NUMBER_E_MT_59 = 200403,
            /** <code>ID_for_Link-11_Reporting__eMT-60_</code> (with ordinal 200404) */
            ID_FOR_LINK_11_REPORTING_E_MT_60 = 200404,
            /** <code>Remote_Track__eMT-62_</code> (with ordinal 200405) */
            REMOTE_TRACK_E_MT_62 = 200405,
            /** <code>Link-11_Error_Rate__eMT-63_</code> (with ordinal 200406) */
            LINK_11_ERROR_RATE_E_MT_63 = 200406,
            /** <code>Track_Quality__eMT-64_</code> (with ordinal 200407) */
            TRACK_QUALITY_E_MT_64 = 200407,
            /** <code>Gridlock__eMT-65_</code> (with ordinal 200408) */
            GRIDLOCK_E_MT_65 = 200408,
            /** <code>Kill__eMT-66_</code> (with ordinal 200409) */
            KILL_E_MT_66 = 200409,
            /** <code>Track_ID_Change___Resolution__eMT-68_</code> (with ordinal 200410) */
            TRACK_ID_CHANGE_RESOLUTION_E_MT_68 = 200410,
            /** <code>Weapons_Status__eMT-69_</code> (with ordinal 200411) */
            WEAPONS_STATUS_E_MT_69 = 200411,
            /** <code>Link-11_Operator__eMT-70_</code> (with ordinal 200412) */
            LINK_11_OPERATOR_E_MT_70 = 200412,
            /** <code>Force_Training_Transmit__eMT-71_</code> (with ordinal 200413) */
            FORCE_TRAINING_TRANSMIT_E_MT_71 = 200413,
            /** <code>Force_Training_Receive__eMT-72_</code> (with ordinal 200414) */
            FORCE_TRAINING_RECEIVE_E_MT_72 = 200414,
            /** <code>Interceptor_Amplification__eMT-75_</code> (with ordinal 200415) */
            INTERCEPTOR_AMPLIFICATION_E_MT_75 = 200415,
            /** <code>Consumables__eMT-78_</code> (with ordinal 200416) */
            CONSUMABLES_E_MT_78 = 200416,
            /** <code>Link-11_Local_Track_Quality__eMT-95_</code> (with ordinal 200417) */
            LINK_11_LOCAL_TRACK_QUALITY_E_MT_95 = 200417,
            /** <code>DLRP__eMT-19_</code> (with ordinal 200418) */
            DLRP_E_MT_19 = 200418,
            /** <code>Force_Order__eMT-52_</code> (with ordinal 200419) */
            FORCE_ORDER_E_MT_52 = 200419,
            /** <code>Wilco___Cantco__eMT-53_</code> (with ordinal 200420) */
            WILCO_CANTCO_E_MT_53 = 200420,
            /** <code>EMC_Bearing__eMT-54_</code> (with ordinal 200421) */
            EMC_BEARING_E_MT_54 = 200421,
            /** <code>Change_Track_Eligibility__eMT-55_</code> (with ordinal 200422) */
            CHANGE_TRACK_ELIGIBILITY_E_MT_55 = 200422,
            /** <code>Land_Mass_Reference_Point</code> (with ordinal 200423) */
            LAND_MASS_REFERENCE_POINT = 200423,
            /** <code>System_Reference_Point</code> (with ordinal 200424) */
            SYSTEM_REFERENCE_POINT = 200424,
            /** <code>PU_Amplification</code> (with ordinal 200425) */
            PU_AMPLIFICATION = 200425,
            /** <code>Set_Drift</code> (with ordinal 200426) */
            SET_DRIFT = 200426,
            /** <code>Begin_Initialization__MT-1_</code> (with ordinal 200427) */
            BEGIN_INITIALIZATION_MT_1 = 200427,
            /** <code>Status_and_Control__MT-3_</code> (with ordinal 200428) */
            STATUS_AND_CONTROL_MT_3 = 200428,
            /** <code>Scintillation_Change__MT-39_</code> (with ordinal 200429) */
            SCINTILLATION_CHANGE_MT_39 = 200429,
            /** <code>Link_11_ID_Control__MT-61_</code> (with ordinal 200430) */
            LINK_11_ID_CONTROL_MT_61 = 200430,
            /** <code>PU_Guard_List</code> (with ordinal 200431) */
            PU_GUARD_LIST = 200431,
            /** <code>Winds_Aloft__MT-14_</code> (with ordinal 200432) */
            WINDS_ALOFT_MT_14 = 200432,
            /** <code>Surface_Winds__MT-15_</code> (with ordinal 200433) */
            SURFACE_WINDS_MT_15 = 200433,
            /** <code>Sea_State__MT-17_</code> (with ordinal 200434) */
            SEA_STATE_MT_17 = 200434,
            /** <code>Magnetic_Variation__MT-37_</code> (with ordinal 200435) */
            MAGNETIC_VARIATION_MT_37 = 200435,
            /** <code>Track_Eligibility__MT-29_</code> (with ordinal 200436) */
            TRACK_ELIGIBILITY_MT_29 = 200436,
            /** <code>Training_Track_Notification</code> (with ordinal 200437) */
            TRAINING_TRACK_NOTIFICATION = 200437,
            /** <code>Tacan_Data__MT-32_</code> (with ordinal 200501) */
            TACAN_DATA_MT_32 = 200501,
            /** <code>Interceptor_Amplification__MT-75_</code> (with ordinal 200502) */
            INTERCEPTOR_AMPLIFICATION_MT_75 = 200502,
            /** <code>Tacan_Assignment__MT-76_</code> (with ordinal 200503) */
            TACAN_ASSIGNMENT_MT_76 = 200503,
            /** <code>Autopilot_Status__MT-77_</code> (with ordinal 200504) */
            AUTOPILOT_STATUS_MT_77 = 200504,
            /** <code>Consumables__MT-78_</code> (with ordinal 200505) */
            CONSUMABLES_MT_78 = 200505,
            /** <code>Downlink__MT-79_</code> (with ordinal 200506) */
            DOWNLINK_MT_79 = 200506,
            /** <code>TIN_Report__MT-80_</code> (with ordinal 200507) */
            TIN_REPORT_MT_80 = 200507,
            /** <code>Special_Point_Control__MT-81_</code> (with ordinal 200508) */
            SPECIAL_POINT_CONTROL_MT_81 = 200508,
            /** <code>Control_Discretes__MT-82_</code> (with ordinal 200509) */
            CONTROL_DISCRETES_MT_82 = 200509,
            /** <code>Request_Target_Discretes_MT-83_</code> (with ordinal 200510) */
            REQUEST_TARGET_DISCRETES_MT_83 = 200510,
            /** <code>Target_Discretes__MT-84_</code> (with ordinal 200511) */
            TARGET_DISCRETES_MT_84 = 200511,
            /** <code>Reply_Discretes__MT-85_</code> (with ordinal 200512) */
            REPLY_DISCRETES_MT_85 = 200512,
            /** <code>Command_Maneuvers__MT-86_</code> (with ordinal 200513) */
            COMMAND_MANEUVERS_MT_86 = 200513,
            /** <code>Target_Data__MT-87_</code> (with ordinal 200514) */
            TARGET_DATA_MT_87 = 200514,
            /** <code>Target_Pointer__MT-88_</code> (with ordinal 200515) */
            TARGET_POINTER_MT_88 = 200515,
            /** <code>Intercept_Data__MT-89_</code> (with ordinal 200516) */
            INTERCEPT_DATA_MT_89 = 200516,
            /** <code>Decrement_Missile_Inventory__MT-90_</code> (with ordinal 200517) */
            DECREMENT_MISSILE_INVENTORY_MT_90 = 200517,
            /** <code>Link-4A_Alert__MT-91_</code> (with ordinal 200518) */
            LINK_4A_ALERT_MT_91 = 200518,
            /** <code>Strike_Control__MT-92_</code> (with ordinal 200519) */
            STRIKE_CONTROL_MT_92 = 200519,
            /** <code>Speed_Change__MT-25_</code> (with ordinal 200521) */
            SPEED_CHANGE_MT_25 = 200521,
            /** <code>Course_Change__MT-26_</code> (with ordinal 200522) */
            COURSE_CHANGE_MT_26 = 200522,
            /** <code>Altitude_Change__MT-27_</code> (with ordinal 200523) */
            ALTITUDE_CHANGE_MT_27 = 200523,
            /** <code>ACLS_AN_SPN-46_Status</code> (with ordinal 200524) */
            ACLS_AN_SPN_46_STATUS = 200524,
            /** <code>ACLS_Aircraft_Report</code> (with ordinal 200525) */
            ACLS_AIRCRAFT_REPORT = 200525,
            /** <code>SPS-67_Radar_Operator_Functions</code> (with ordinal 200600) */
            SPS_67_RADAR_OPERATOR_FUNCTIONS = 200600,
            /** <code>SPS-55_Radar_Operator_Functions</code> (with ordinal 200601) */
            SPS_55_RADAR_OPERATOR_FUNCTIONS = 200601,
            /** <code>SPQ-9A_Radar_Operator_Functions</code> (with ordinal 200602) */
            SPQ_9A_RADAR_OPERATOR_FUNCTIONS = 200602,
            /** <code>SPS-49_Radar_Operator_Functions</code> (with ordinal 200603) */
            SPS_49_RADAR_OPERATOR_FUNCTIONS = 200603,
            /** <code>MK-23_Radar_Operator_Functions</code> (with ordinal 200604) */
            MK_23_RADAR_OPERATOR_FUNCTIONS = 200604,
            /** <code>SPS-48_Radar_Operator_Functions</code> (with ordinal 200605) */
            SPS_48_RADAR_OPERATOR_FUNCTIONS = 200605,
            /** <code>SPS-40_Radar_Operator_Functions</code> (with ordinal 200606) */
            SPS_40_RADAR_OPERATOR_FUNCTIONS = 200606,
            /** <code>MK-95_Radar_Operator_Functions</code> (with ordinal 200607) */
            MK_95_RADAR_OPERATOR_FUNCTIONS = 200607,
            /** <code>Kill_NoKill</code> (with ordinal 200608) */
            KILL_NO_KILL = 200608,
            /** <code>CMTPc</code> (with ordinal 200609) */
            CMTPC = 200609,
            /** <code>CMC4AirGlobalData</code> (with ordinal 200610) */
            CMC4AIR_GLOBAL_DATA = 200610,
            /** <code>CMC4GlobalData</code> (with ordinal 200611) */
            CMC4GLOBAL_DATA = 200611,
            /** <code>LINKSIM_COMMENT_PDU</code> (with ordinal 200612) */
            LINKSIM_COMMENT_PDU = 200612,
            /** <code>NSSTOwnshipControl</code> (with ordinal 200613) */
            NSSTOWNSHIP_CONTROL = 200613,
            /** <code>Other</code> (with ordinal 240000) */
            OTHER = 240000,
            /** <code>Mass_Of_The_Vehicle</code> (with ordinal 240001) */
            MASS_OF_THE_VEHICLE = 240001,
            /** <code>Force_ID_2</code> (with ordinal 240002) */
            FORCE_ID_2 = 240002,
            /** <code>Entity_Type_Kind</code> (with ordinal 240003) */
            ENTITY_TYPE_KIND_2 = 240003,
            /** <code>Entity_Type_Domain</code> (with ordinal 240004) */
            ENTITY_TYPE_DOMAIN_2 = 240004,
            /** <code>Entity_Type_Country</code> (with ordinal 240005) */
            ENTITY_TYPE_COUNTRY_2 = 240005,
            /** <code>Entity_Type_Category</code> (with ordinal 240006) */
            ENTITY_TYPE_CATEGORY_2 = 240006,
            /** <code>Entity_Type_Sub_Category</code> (with ordinal 240007) */
            ENTITY_TYPE_SUB_CATEGORY = 240007,
            /** <code>Entity_Type_Specific</code> (with ordinal 240008) */
            ENTITY_TYPE_SPECIFIC_2 = 240008,
            /** <code>Entity_Type_Extra</code> (with ordinal 240009) */
            ENTITY_TYPE_EXTRA_2 = 240009,
            /** <code>Alternative_Entity_Type_Kind</code> (with ordinal 240010) */
            ALTERNATIVE_ENTITY_TYPE_KIND_2 = 240010,
            /** <code>Alternative_Entity_Type_Domain</code> (with ordinal 240011) */
            ALTERNATIVE_ENTITY_TYPE_DOMAIN_2 = 240011,
            /** <code>Alternative_Entity_Type_Country</code> (with ordinal 240012) */
            ALTERNATIVE_ENTITY_TYPE_COUNTRY_2 = 240012,
            /** <code>Alternative_Entity_Type_Category</code> (with ordinal 240013) */
            ALTERNATIVE_ENTITY_TYPE_CATEGORY_2 = 240013,
            /** <code>Alternative_Entity_Type_Sub_Category</code> (with ordinal 240014) */
            ALTERNATIVE_ENTITY_TYPE_SUB_CATEGORY = 240014,
            /** <code>Alternative_Entity_Type_Specific</code> (with ordinal 240015) */
            ALTERNATIVE_ENTITY_TYPE_SPECIFIC_2 = 240015,
            /** <code>Alternative_Entity_Type_Extra</code> (with ordinal 240016) */
            ALTERNATIVE_ENTITY_TYPE_EXTRA_2 = 240016,
            /** <code>Entity_Location_X</code> (with ordinal 240017) */
            ENTITY_LOCATION_X = 240017,
            /** <code>Entity_Location_Y</code> (with ordinal 240018) */
            ENTITY_LOCATION_Y = 240018,
            /** <code>Entity_Location_Z</code> (with ordinal 240019) */
            ENTITY_LOCATION_Z = 240019,
            /** <code>Entity_Linear_Velocity_X</code> (with ordinal 240020) */
            ENTITY_LINEAR_VELOCITY_X = 240020,
            /** <code>Entity_Linear_Velocity_Y</code> (with ordinal 240021) */
            ENTITY_LINEAR_VELOCITY_Y = 240021,
            /** <code>Entity_Linear_Velocity_Z</code> (with ordinal 240022) */
            ENTITY_LINEAR_VELOCITY_Z = 240022,
            /** <code>Entity_Orientation_Psi</code> (with ordinal 240023) */
            ENTITY_ORIENTATION_PSI = 240023,
            /** <code>Entity_Orientation_Theta</code> (with ordinal 240024) */
            ENTITY_ORIENTATION_THETA = 240024,
            /** <code>Entity_Orientation_Phi</code> (with ordinal 240025) */
            ENTITY_ORIENTATION_PHI = 240025,
            /** <code>Dead_Reckoning_Algorithm</code> (with ordinal 240026) */
            DEAD_RECKONING_ALGORITHM = 240026,
            /** <code>Dead_Reckoning_Linear_Acceleration_X</code> (with ordinal 240027) */
            DEAD_RECKONING_LINEAR_ACCELERATION_X = 240027,
            /** <code>Dead_Reckoning_Linear_Acceleration_Y</code> (with ordinal 240028) */
            DEAD_RECKONING_LINEAR_ACCELERATION_Y = 240028,
            /** <code>Dead_Reckoning_Linear_Acceleration_Z</code> (with ordinal 240029) */
            DEAD_RECKONING_LINEAR_ACCELERATION_Z = 240029,
            /** <code>Dead_Reckoning_Angular_Velocity_X</code> (with ordinal 240030) */
            DEAD_RECKONING_ANGULAR_VELOCITY_X = 240030,
            /** <code>Dead_Reckoning_Angular_Velocity_Y</code> (with ordinal 240031) */
            DEAD_RECKONING_ANGULAR_VELOCITY_Y = 240031,
            /** <code>Dead_Reckoning_Angular_Velocity_Z</code> (with ordinal 240032) */
            DEAD_RECKONING_ANGULAR_VELOCITY_Z = 240032,
            /** <code>Entity_Appearance</code> (with ordinal 240033) */
            ENTITY_APPEARANCE = 240033,
            /** <code>Entity_Marking_Character_Set</code> (with ordinal 240034) */
            ENTITY_MARKING_CHARACTER_SET = 240034,
            /** <code>Entity_Marking_11_Bytes</code> (with ordinal 240035) */
            ENTITY_MARKING_11_BYTES = 240035,
            /** <code>Capability</code> (with ordinal 240036) */
            CAPABILITY = 240036,
            /** <code>Number_Articulation_Parameters</code> (with ordinal 240037) */
            NUMBER_ARTICULATION_PARAMETERS = 240037,
            /** <code>Articulation_Parameter_ID</code> (with ordinal 240038) */
            ARTICULATION_PARAMETER_ID = 240038,
            /** <code>Articulation_Parameter_Type_2</code> (with ordinal 240039) */
            ARTICULATION_PARAMETER_TYPE_2 = 240039,
            /** <code>Articulation_Parameter_Value_2</code> (with ordinal 240040) */
            ARTICULATION_PARAMETER_VALUE_2 = 240040,
            /** <code>Type_Of_Stores</code> (with ordinal 240041) */
            TYPE_OF_STORES = 240041,
            /** <code>Quantity_Of_Stores</code> (with ordinal 240042) */
            QUANTITY_OF_STORES = 240042,
            /** <code>Fuel_Quantity</code> (with ordinal 240043) */
            FUEL_QUANTITY = 240043,
            /** <code>Radar_System_Status</code> (with ordinal 240044) */
            RADAR_SYSTEM_STATUS = 240044,
            /** <code>Radio_Communication_System_Status</code> (with ordinal 240045) */
            RADIO_COMMUNICATION_SYSTEM_STATUS = 240045,
            /** <code>Default_Time_For_Radio_Transmission_For_Stationary_Transmitters</code> (with ordinal 240046) */
            DEFAULT_TIME_FOR_RADIO_TRANSMISSION_FOR_STATIONARY_TRANSMITTERS = 240046,
            /** <code>Default_Time_For_Radio_Transmission_For_Moving_Transmitters</code> (with ordinal 240047) */
            DEFAULT_TIME_FOR_RADIO_TRANSMISSION_FOR_MOVING_TRANSMITTERS = 240047,
            /** <code>Body_Part_Damaged_Ratio</code> (with ordinal 240048) */
            BODY_PART_DAMAGED_RATIO = 240048,
            /** <code>Name_Of_The_Terrain_Database_File</code> (with ordinal 240049) */
            NAME_OF_THE_TERRAIN_DATABASE_FILE = 240049,
            /** <code>Name_Of_Local_File</code> (with ordinal 240050) */
            NAME_OF_LOCAL_FILE = 240050,
            /** <code>Aimpoint_Bearing</code> (with ordinal 240051) */
            AIMPOINT_BEARING = 240051,
            /** <code>Aimpoint_Elevation</code> (with ordinal 240052) */
            AIMPOINT_ELEVATION = 240052,
            /** <code>Aimpoint_Range</code> (with ordinal 240053) */
            AIMPOINT_RANGE = 240053,
            /** <code>Air_Speed</code> (with ordinal 240054) */
            AIR_SPEED = 240054,
            /** <code>Altitude_2</code> (with ordinal 240055) */
            ALTITUDE_2 = 240055,
            /** <code>Application_Status</code> (with ordinal 240056) */
            APPLICATION_STATUS = 240056,
            /** <code>Auto_Iff</code> (with ordinal 240057) */
            AUTO_IFF = 240057,
            /** <code>Beacon_Delay</code> (with ordinal 240058) */
            BEACON_DELAY = 240058,
            /** <code>Bingo_Fuel_Setting</code> (with ordinal 240059) */
            BINGO_FUEL_SETTING = 240059,
            /** <code>Cloud_Bottom</code> (with ordinal 240060) */
            CLOUD_BOTTOM = 240060,
            /** <code>Cloud_Top</code> (with ordinal 240061) */
            CLOUD_TOP = 240061,
            /** <code>Direction</code> (with ordinal 240062) */
            DIRECTION = 240062,
            /** <code>End_Action</code> (with ordinal 240063) */
            END_ACTION = 240063,
            /** <code>Frequency</code> (with ordinal 240064) */
            FREQUENCY = 240064,
            /** <code>Freeze</code> (with ordinal 240065) */
            FREEZE = 240065,
            /** <code>Heading</code> (with ordinal 240066) */
            HEADING = 240066,
            /** <code>Identification</code> (with ordinal 240067) */
            IDENTIFICATION = 240067,
            /** <code>Initial_Point_Data</code> (with ordinal 240068) */
            INITIAL_POINT_DATA = 240068,
            /** <code>Latitude_2</code> (with ordinal 240069) */
            LATITUDE_2 = 240069,
            /** <code>Lights_2</code> (with ordinal 240070) */
            LIGHTS_2 = 240070,
            /** <code>Linear</code> (with ordinal 240071) */
            LINEAR = 240071,
            /** <code>Longitude_2</code> (with ordinal 240072) */
            LONGITUDE_2 = 240072,
            /** <code>Low_Altitude</code> (with ordinal 240073) */
            LOW_ALTITUDE = 240073,
            /** <code>Mfd_Formats</code> (with ordinal 240074) */
            MFD_FORMATS = 240074,
            /** <code>Nctr</code> (with ordinal 240075) */
            NCTR = 240075,
            /** <code>Number_Projectiles</code> (with ordinal 240076) */
            NUMBER_PROJECTILES = 240076,
            /** <code>Operation_Code</code> (with ordinal 240077) */
            OPERATION_CODE = 240077,
            /** <code>Pitch</code> (with ordinal 240078) */
            PITCH = 240078,
            /** <code>Profiles</code> (with ordinal 240079) */
            PROFILES = 240079,
            /** <code>Quantity</code> (with ordinal 240080) */
            QUANTITY = 240080,
            /** <code>Radar_Modes</code> (with ordinal 240081) */
            RADAR_MODES = 240081,
            /** <code>Radar_Search_Volume</code> (with ordinal 240082) */
            RADAR_SEARCH_VOLUME = 240082,
            /** <code>Roll</code> (with ordinal 240083) */
            ROLL = 240083,
            /** <code>Rotation</code> (with ordinal 240084) */
            ROTATION = 240084,
            /** <code>Scale_Factor_X</code> (with ordinal 240085) */
            SCALE_FACTOR_X = 240085,
            /** <code>Scale_Factor_Y</code> (with ordinal 240086) */
            SCALE_FACTOR_Y = 240086,
            /** <code>Shields</code> (with ordinal 240087) */
            SHIELDS = 240087,
            /** <code>Steerpoint</code> (with ordinal 240088) */
            STEERPOINT = 240088,
            /** <code>Spare1</code> (with ordinal 240089) */
            SPARE1 = 240089,
            /** <code>Spare2</code> (with ordinal 240090) */
            SPARE2 = 240090,
            /** <code>Team</code> (with ordinal 240091) */
            TEAM = 240091,
            /** <code>Text</code> (with ordinal 240092) */
            TEXT = 240092,
            /** <code>Time_Of_Day</code> (with ordinal 240093) */
            TIME_OF_DAY = 240093,
            /** <code>Trail_Flag</code> (with ordinal 240094) */
            TRAIL_FLAG = 240094,
            /** <code>Trail_Size</code> (with ordinal 240095) */
            TRAIL_SIZE = 240095,
            /** <code>Type_Of_Projectile</code> (with ordinal 240096) */
            TYPE_OF_PROJECTILE = 240096,
            /** <code>Type_Of_Target</code> (with ordinal 240097) */
            TYPE_OF_TARGET = 240097,
            /** <code>Type_Of_Threat</code> (with ordinal 240098) */
            TYPE_OF_THREAT = 240098,
            /** <code>Uhf_Frequency</code> (with ordinal 240099) */
            UHF_FREQUENCY = 240099,
            /** <code>Utm_Altitude</code> (with ordinal 240100) */
            UTM_ALTITUDE = 240100,
            /** <code>Utm_Latitude</code> (with ordinal 240101) */
            UTM_LATITUDE = 240101,
            /** <code>Utm_Longitude</code> (with ordinal 240102) */
            UTM_LONGITUDE = 240102,
            /** <code>Vhf_Frequency</code> (with ordinal 240103) */
            VHF_FREQUENCY = 240103,
            /** <code>Visibility_Range</code> (with ordinal 240104) */
            VISIBILITY_RANGE = 240104,
            /** <code>Void_Aaa_Hit</code> (with ordinal 240105) */
            VOID_AAA_HIT = 240105,
            /** <code>Void_Collision</code> (with ordinal 240106) */
            VOID_COLLISION = 240106,
            /** <code>Void_Earth_Hit</code> (with ordinal 240107) */
            VOID_EARTH_HIT = 240107,
            /** <code>Void_Friendly</code> (with ordinal 240108) */
            VOID_FRIENDLY = 240108,
            /** <code>Void_Gun_Hit</code> (with ordinal 240109) */
            VOID_GUN_HIT = 240109,
            /** <code>Void_Rocket_Hit</code> (with ordinal 240110) */
            VOID_ROCKET_HIT = 240110,
            /** <code>Void_Sam_Hit</code> (with ordinal 240111) */
            VOID_SAM_HIT = 240111,
            /** <code>Weapon_Data</code> (with ordinal 240112) */
            WEAPON_DATA = 240112,
            /** <code>Weapon_Type</code> (with ordinal 240113) */
            WEAPON_TYPE = 240113,
            /** <code>Weather_2</code> (with ordinal 240114) */
            WEATHER_2 = 240114,
            /** <code>Wind_Direction</code> (with ordinal 240115) */
            WIND_DIRECTION_2 = 240115,
            /** <code>Wind_Speed</code> (with ordinal 240116) */
            WIND_SPEED = 240116,
            /** <code>Wing_Station</code> (with ordinal 240117) */
            WING_STATION = 240117,
            /** <code>Yaw</code> (with ordinal 240118) */
            YAW = 240118,
            /** <code>Memory_Offset</code> (with ordinal 240119) */
            MEMORY_OFFSET = 240119,
            /** <code>Memory_Data</code> (with ordinal 240120) */
            MEMORY_DATA = 240120,
            /** <code>VASI</code> (with ordinal 240121) */
            VASI = 240121,
            /** <code>Beacon</code> (with ordinal 240122) */
            BEACON = 240122,
            /** <code>Strobe</code> (with ordinal 240123) */
            STROBE = 240123,
            /** <code>Culture</code> (with ordinal 240124) */
            CULTURE = 240124,
            /** <code>Approach</code> (with ordinal 240125) */
            APPROACH = 240125,
            /** <code>Runway_End</code> (with ordinal 240126) */
            RUNWAY_END = 240126,
            /** <code>Obstruction</code> (with ordinal 240127) */
            OBSTRUCTION = 240127,
            /** <code>Runway_Edge</code> (with ordinal 240128) */
            RUNWAY_EDGE = 240128,
            /** <code>Ramp_Taxiway</code> (with ordinal 240129) */
            RAMP_TAXIWAY = 240129,
            /** <code>Laser_Bomb_Code</code> (with ordinal 240130) */
            LASER_BOMB_CODE = 240130,
            /** <code>Rack_Type</code> (with ordinal 240131) */
            RACK_TYPE = 240131,
            /** <code>HUD</code> (with ordinal 240132) */
            HUD = 240132,
            /** <code>RoleFileName</code> (with ordinal 240133) */
            ROLE_FILE_NAME = 240133,
            /** <code>PilotName</code> (with ordinal 240134) */
            PILOT_NAME = 240134,
            /** <code>PilotDesignation</code> (with ordinal 240135) */
            PILOT_DESIGNATION = 240135,
            /** <code>Model_Type</code> (with ordinal 240136) */
            MODEL_TYPE = 240136,
            /** <code>DIS_Type</code> (with ordinal 240137) */
            DIS_TYPE = 240137,
            /** <code>Class</code> (with ordinal 240138) */
            CLASS_ = 240138,
            /** <code>Channel</code> (with ordinal 240139) */
            CHANNEL = 240139,
            /** <code>Entity_Type_2</code> (with ordinal 240140) */
            ENTITY_TYPE_2 = 240140,
            /** <code>Alternative_Entity_Type_2</code> (with ordinal 240141) */
            ALTERNATIVE_ENTITY_TYPE_2 = 240141,
            /** <code>Entity_Location</code> (with ordinal 240142) */
            ENTITY_LOCATION = 240142,
            /** <code>Entity_Linear_Velocity</code> (with ordinal 240143) */
            ENTITY_LINEAR_VELOCITY = 240143,
            /** <code>Entity_Orientation</code> (with ordinal 240144) */
            ENTITY_ORIENTATION = 240144,
            /** <code>Dead_Reckoning</code> (with ordinal 240145) */
            DEAD_RECKONING = 240145,
            /** <code>Failure_Symptom</code> (with ordinal 240146) */
            FAILURE_SYMPTOM = 240146,
            /** <code>Max_Fuel</code> (with ordinal 240147) */
            MAX_FUEL = 240147,
            /** <code>Refueling_Boom_Connect</code> (with ordinal 240148) */
            REFUELING_BOOM_CONNECT = 240148,
            /** <code>Altitude_AGL</code> (with ordinal 240149) */
            ALTITUDE_AGL = 240149,
            /** <code>Calibrated_Airspeed</code> (with ordinal 240150) */
            CALIBRATED_AIRSPEED = 240150,
            /** <code>TACAN_Channel</code> (with ordinal 240151) */
            TACAN_CHANNEL = 240151,
            /** <code>TACAN_Band</code> (with ordinal 240152) */
            TACAN_BAND = 240152,
            /** <code>TACAN_Mode</code> (with ordinal 240153) */
            TACAN_MODE = 240153,
            /** <code>Munition</code> (with ordinal 500001) */
            MUNITION = 500001,
            /** <code>EngineFuel</code> (with ordinal 500002) */
            ENGINE_FUEL = 500002,
            /** <code>StorageFuel</code> (with ordinal 500003) */
            STORAGE_FUEL = 500003,
            /** <code>NotUsed</code> (with ordinal 500004) */
            NOT_USED = 500004,
            /** <code>Expendable</code> (with ordinal 500005) */
            EXPENDABLE = 500005,
            /** <code>TotalRecordSets</code> (with ordinal 500006) */
            TOTAL_RECORD_SETS = 500006,
            /** <code>LaunchedMunition</code> (with ordinal 500007) */
            LAUNCHED_MUNITION = 500007,
            /** <code>Association</code> (with ordinal 500008) */
            ASSOCIATION = 500008,
            /** <code>Sensor</code> (with ordinal 500009) */
            SENSOR = 500009,
            /** <code>MunitionReload</code> (with ordinal 500010) */
            MUNITION_RELOAD = 500010,
            /** <code>EngineFuelReload</code> (with ordinal 500011) */
            ENGINE_FUEL_RELOAD = 500011,
            /** <code>StorageFuelReload</code> (with ordinal 500012) */
            STORAGE_FUEL_RELOAD = 500012,
            /** <code>ExpendableReload</code> (with ordinal 500013) */
            EXPENDABLE_RELOAD = 500013
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to DatumIdentifierEnum: static_cast<DevStudio::DatumIdentifierEnum::DatumIdentifierEnum>(i)
        */
        LIBAPI bool isValid(const DatumIdentifierEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::DatumIdentifierEnum::DatumIdentifierEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::DatumIdentifierEnum::DatumIdentifierEnum const &);
}


#endif
