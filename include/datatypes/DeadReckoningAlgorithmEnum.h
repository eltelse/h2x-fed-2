/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_DEADRECKONINGALGORITHMENUM_H
#define DEVELOPER_STUDIO_DATATYPES_DEADRECKONINGALGORITHMENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace DeadReckoningAlgorithmEnum {
        /**
        * Implementation of the <code>DeadReckoningAlgorithmEnum8</code> data type from the FOM.
        * <br>Description from the FOM: <i>Dead-reckoning algorithm</i>
        */
        enum DeadReckoningAlgorithmEnum {
            /** <code>Other</code> (with ordinal 0) */
            OTHER = 0,
            /** <code>Static</code> (with ordinal 1) */
            STATIC_ = 1,
            /** <code>DRM_FPW</code> (with ordinal 2) */
            DRM_FPW = 2,
            /** <code>DRM_RPW</code> (with ordinal 3) */
            DRM_RPW = 3,
            /** <code>DRM_RVW</code> (with ordinal 4) */
            DRM_RVW = 4,
            /** <code>DRM_FVW</code> (with ordinal 5) */
            DRM_FVW = 5,
            /** <code>DRM_FPB</code> (with ordinal 6) */
            DRM_FPB = 6,
            /** <code>DRM_RPB</code> (with ordinal 7) */
            DRM_RPB = 7,
            /** <code>DRM_RVB</code> (with ordinal 8) */
            DRM_RVB = 8,
            /** <code>DRM_FVB</code> (with ordinal 9) */
            DRM_FVB = 9
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to DeadReckoningAlgorithmEnum: static_cast<DevStudio::DeadReckoningAlgorithmEnum::DeadReckoningAlgorithmEnum>(i)
        */
        LIBAPI bool isValid(const DeadReckoningAlgorithmEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::DeadReckoningAlgorithmEnum::DeadReckoningAlgorithmEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::DeadReckoningAlgorithmEnum::DeadReckoningAlgorithmEnum const &);
}


#endif
