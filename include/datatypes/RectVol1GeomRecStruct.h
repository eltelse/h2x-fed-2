/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_RECTVOL1GEOMRECSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_RECTVOL1GEOMRECSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/DimensionStruct.h>
#include <DevStudio/datatypes/OrientationStruct.h>
#include <DevStudio/datatypes/WorldLocationStruct.h>

namespace DevStudio {
   /**
   * Implementation of the <code>RectVol1GeomRecStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Record specifying Rectangular Volume 1 geometry record</i>
   */
   class RectVol1GeomRecStruct {

   public:
      /**
      * Description from the FOM: <i>Corner location X, Y, Z</i>.
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      */
      WorldLocationStruct cornerLocation;
      /**
      * Description from the FOM: <i>Dimensions</i>.
      * <br>Description of the data type from the FOM: <i>Bounding box in X,Y,Z axis.</i>
      */
      DimensionStruct dimensions;
      /**
      * Description from the FOM: <i>Orientation, specified by Euler angles</i>.
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      */
      OrientationStruct orientation;

      LIBAPI RectVol1GeomRecStruct()
         :
         cornerLocation(WorldLocationStruct()),
         dimensions(DimensionStruct()),
         orientation(OrientationStruct())
      {}

      /**
      * Constructor for RectVol1GeomRecStruct
      *
      * @param cornerLocation_ value to set as cornerLocation.
      * <br>Description from the FOM: <i>Corner location X, Y, Z</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      * @param dimensions_ value to set as dimensions.
      * <br>Description from the FOM: <i>Dimensions</i>
      * <br>Description of the data type from the FOM: <i>Bounding box in X,Y,Z axis.</i>
      * @param orientation_ value to set as orientation.
      * <br>Description from the FOM: <i>Orientation, specified by Euler angles</i>
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      */
      LIBAPI RectVol1GeomRecStruct(
         WorldLocationStruct cornerLocation_,
         DimensionStruct dimensions_,
         OrientationStruct orientation_
         )
         :
         cornerLocation(cornerLocation_),
         dimensions(dimensions_),
         orientation(orientation_)
      {}



      /**
      * Function to get cornerLocation.
      * <br>Description from the FOM: <i>Corner location X, Y, Z</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @return cornerLocation
      */
      LIBAPI DevStudio::WorldLocationStruct & getCornerLocation() {
         return cornerLocation;
      }

      /**
      * Function to get dimensions.
      * <br>Description from the FOM: <i>Dimensions</i>
      * <br>Description of the data type from the FOM: <i>Bounding box in X,Y,Z axis.</i>
      *
      * @return dimensions
      */
      LIBAPI DevStudio::DimensionStruct & getDimensions() {
         return dimensions;
      }

      /**
      * Function to get orientation.
      * <br>Description from the FOM: <i>Orientation, specified by Euler angles</i>
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @return orientation
      */
      LIBAPI DevStudio::OrientationStruct & getOrientation() {
         return orientation;
      }

   };


   LIBAPI bool operator ==(const DevStudio::RectVol1GeomRecStruct& l, const DevStudio::RectVol1GeomRecStruct& r);
   LIBAPI bool operator !=(const DevStudio::RectVol1GeomRecStruct& l, const DevStudio::RectVol1GeomRecStruct& r);
   LIBAPI bool operator <(const DevStudio::RectVol1GeomRecStruct& l, const DevStudio::RectVol1GeomRecStruct& r);
   LIBAPI bool operator >(const DevStudio::RectVol1GeomRecStruct& l, const DevStudio::RectVol1GeomRecStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::RectVol1GeomRecStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::RectVol1GeomRecStruct const &);
}
#endif
