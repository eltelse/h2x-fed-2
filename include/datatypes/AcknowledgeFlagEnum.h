/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_ACKNOWLEDGEFLAGENUM_H
#define DEVELOPER_STUDIO_DATATYPES_ACKNOWLEDGEFLAGENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace AcknowledgeFlagEnum {
        /**
        * Implementation of the <code>AcknowledgeFlagEnum16</code> data type from the FOM.
        * <br>Description from the FOM: <i>Acknowledgment flags</i>
        */
        enum AcknowledgeFlagEnum {
            /** <code>CreateEntity</code> (with ordinal 1) */
            CREATE_ENTITY = 1,
            /** <code>RemoveEntity</code> (with ordinal 2) */
            REMOVE_ENTITY = 2,
            /** <code>StartResume</code> (with ordinal 3) */
            START_RESUME = 3,
            /** <code>StopFreeze</code> (with ordinal 4) */
            STOP_FREEZE = 4,
            /** <code>TransferOwnership</code> (with ordinal 5) */
            TRANSFER_OWNERSHIP = 5
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to AcknowledgeFlagEnum: static_cast<DevStudio::AcknowledgeFlagEnum::AcknowledgeFlagEnum>(i)
        */
        LIBAPI bool isValid(const AcknowledgeFlagEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::AcknowledgeFlagEnum::AcknowledgeFlagEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::AcknowledgeFlagEnum::AcknowledgeFlagEnum const &);
}


#endif
