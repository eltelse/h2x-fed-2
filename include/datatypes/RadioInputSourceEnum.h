/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_RADIOINPUTSOURCEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_RADIOINPUTSOURCEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace RadioInputSourceEnum {
        /**
        * Implementation of the <code>RadioInputSourceEnum8</code> data type from the FOM.
        * <br>Description from the FOM: <i>Radio input source</i>
        */
        enum RadioInputSourceEnum {
            /** <code>Other</code> (with ordinal 0) */
            OTHER = 0,
            /** <code>Pilot</code> (with ordinal 1) */
            PILOT = 1,
            /** <code>Copilot</code> (with ordinal 2) */
            COPILOT = 2,
            /** <code>FirstOfficer</code> (with ordinal 3) */
            FIRST_OFFICER = 3,
            /** <code>Driver</code> (with ordinal 4) */
            DRIVER = 4,
            /** <code>Loader</code> (with ordinal 5) */
            LOADER = 5,
            /** <code>Gunner</code> (with ordinal 6) */
            GUNNER = 6,
            /** <code>Commander</code> (with ordinal 7) */
            COMMANDER = 7,
            /** <code>DigitalDataDevice</code> (with ordinal 8) */
            DIGITAL_DATA_DEVICE = 8,
            /** <code>Intercom</code> (with ordinal 9) */
            INTERCOM = 9,
            /** <code>AudioJammer</code> (with ordinal 10) */
            AUDIO_JAMMER = 10,
            /** <code>DataJammer</code> (with ordinal 11) */
            DATA_JAMMER = 11,
            /** <code>GPSJammer</code> (with ordinal 12) */
            GPSJAMMER = 12,
            /** <code>GPSMeaconer</code> (with ordinal 13) */
            GPSMEACONER = 13
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to RadioInputSourceEnum: static_cast<DevStudio::RadioInputSourceEnum::RadioInputSourceEnum>(i)
        */
        LIBAPI bool isValid(const RadioInputSourceEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::RadioInputSourceEnum::RadioInputSourceEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::RadioInputSourceEnum::RadioInputSourceEnum const &);
}


#endif
