/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_ANTENNAPATTERNTYPEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_ANTENNAPATTERNTYPEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace AntennaPatternTypeEnum {
        /**
        * Implementation of the <code>AntennaPatternTypeEnum32</code> data type from the FOM.
        * <br>Description from the FOM: <i>The radiation pattern from an antenna.</i>
        */
        enum AntennaPatternTypeEnum {
            /** <code>OmniDirectional</code> (with ordinal 0) */
            OMNI_DIRECTIONAL = 0,
            /** <code>Beam</code> (with ordinal 1) */
            BEAM = 1,
            /** <code>SphericalHarmonic</code> (with ordinal 2) */
            SPHERICAL_HARMONIC = 2
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to AntennaPatternTypeEnum: static_cast<DevStudio::AntennaPatternTypeEnum::AntennaPatternTypeEnum>(i)
        */
        LIBAPI bool isValid(const AntennaPatternTypeEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::AntennaPatternTypeEnum::AntennaPatternTypeEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::AntennaPatternTypeEnum::AntennaPatternTypeEnum const &);
}


#endif
