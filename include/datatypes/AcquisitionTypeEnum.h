/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_ACQUISITIONTYPEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_ACQUISITIONTYPEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace AcquisitionTypeEnum {
        /**
        * Implementation of the <code>AcquisitionTypeEnum32</code> data type from the FOM.
        * <br>Description from the FOM: <i>-NULL-</i>
        */
        enum AcquisitionTypeEnum {
            /** <code>UNDEFINED</code> (with ordinal 0) */
            UNDEFINED = 0,
            /** <code>MARKING</code> (with ordinal 1) */
            MARKING = 1,
            /** <code>LASING</code> (with ordinal 2) */
            LASING = 2,
            /** <code>FIRE</code> (with ordinal 4) */
            FIRE = 4,
            /** <code>RADAR</code> (with ordinal 8) */
            RADAR = 8,
            /** <code>CONCLUSION</code> (with ordinal 16) */
            CONCLUSION = 16,
            /** <code>COORDINATE</code> (with ordinal 32) */
            COORDINATE = 32
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to AcquisitionTypeEnum: static_cast<DevStudio::AcquisitionTypeEnum::AcquisitionTypeEnum>(i)
        */
        LIBAPI bool isValid(const AcquisitionTypeEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::AcquisitionTypeEnum::AcquisitionTypeEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::AcquisitionTypeEnum::AcquisitionTypeEnum const &);
}


#endif
