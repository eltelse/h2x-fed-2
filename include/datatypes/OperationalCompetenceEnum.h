/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_OPERATIONALCOMPETENCEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_OPERATIONALCOMPETENCEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace OperationalCompetenceEnum {
        /**
        * Implementation of the <code>OperationalCompetenceEnum32</code> data type from the FOM.
        * <br>Description from the FOM: <i>-NULL-</i>
        */
        enum OperationalCompetenceEnum {
            /** <code>FULL_COMPETENCE</code> (with ordinal 1) */
            FULL_COMPETENCE = 1,
            /** <code>HIGH_COMPETENCE</code> (with ordinal 2) */
            HIGH_COMPETENCE = 2,
            /** <code>MEDIUM_COMPETENCE</code> (with ordinal 3) */
            MEDIUM_COMPETENCE = 3,
            /** <code>LOW_COMPETENCE</code> (with ordinal 4) */
            LOW_COMPETENCE = 4,
            /** <code>NONCOMPETENT</code> (with ordinal 5) */
            NONCOMPETENT = 5
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to OperationalCompetenceEnum: static_cast<DevStudio::OperationalCompetenceEnum::OperationalCompetenceEnum>(i)
        */
        LIBAPI bool isValid(const OperationalCompetenceEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::OperationalCompetenceEnum::OperationalCompetenceEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::OperationalCompetenceEnum::OperationalCompetenceEnum const &);
}


#endif
