/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_ENVIRONMENTTYPESTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_ENVIRONMENTTYPESTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>


namespace DevStudio {
   /**
   * Implementation of the <code>EnvironmentTypeStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Record specifying the kind of environment, the domain and any extra information necessary for describing the environmental entity</i>
   */
   class EnvironmentTypeStruct {

   public:
      /**
      * Description from the FOM: <i>Identifies the kind of entity</i>.
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      char entityKind;
      /**
      * Description from the FOM: <i>Specifies a single primary domain in which the environmental condition exists</i>.
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      char domain;
      /**
      * Description from the FOM: <i>Identifies the type of environmental entity</i>.
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      unsigned short class_;
      /**
      * Description from the FOM: <i>Specifies the main category that describes the environmental entity</i>.
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      char category;
      /**
      * Description from the FOM: <i>Specifies a particular subcategory to which an environmental entity belongs based on the Category field</i>.
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      char subcategory;
      /**
      * Description from the FOM: <i>Identifies specific information about an environmental entity based on the Subcategory field</i>.
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      char specific;
      /**
      * Description from the FOM: <i>Specifies extra information required to describe a particular environmental entity</i>.
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      char extra;

      LIBAPI EnvironmentTypeStruct()
         :
         entityKind(0),
         domain(0),
         class_(0),
         category(0),
         subcategory(0),
         specific(0),
         extra(0)
      {}

      /**
      * Constructor for EnvironmentTypeStruct
      *
      * @param entityKind_ value to set as entityKind.
      * <br>Description from the FOM: <i>Identifies the kind of entity</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param domain_ value to set as domain.
      * <br>Description from the FOM: <i>Specifies a single primary domain in which the environmental condition exists</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param class__ value to set as class_.
      * <br>Description from the FOM: <i>Identifies the type of environmental entity</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param category_ value to set as category.
      * <br>Description from the FOM: <i>Specifies the main category that describes the environmental entity</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param subcategory_ value to set as subcategory.
      * <br>Description from the FOM: <i>Specifies a particular subcategory to which an environmental entity belongs based on the Category field</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param specific_ value to set as specific.
      * <br>Description from the FOM: <i>Identifies specific information about an environmental entity based on the Subcategory field</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param extra_ value to set as extra.
      * <br>Description from the FOM: <i>Specifies extra information required to describe a particular environmental entity</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      LIBAPI EnvironmentTypeStruct(
         char entityKind_,
         char domain_,
         unsigned short class__,
         char category_,
         char subcategory_,
         char specific_,
         char extra_
         )
         :
         entityKind(entityKind_),
         domain(domain_),
         class_(class__),
         category(category_),
         subcategory(subcategory_),
         specific(specific_),
         extra(extra_)
      {}



      /**
      * Function to get entityKind.
      * <br>Description from the FOM: <i>Identifies the kind of entity</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return entityKind
      */
      LIBAPI char & getEntityKind() {
         return entityKind;
      }

      /**
      * Function to get domain.
      * <br>Description from the FOM: <i>Specifies a single primary domain in which the environmental condition exists</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return domain
      */
      LIBAPI char & getDomain() {
         return domain;
      }

      /**
      * Function to get class_.
      * <br>Description from the FOM: <i>Identifies the type of environmental entity</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return class_
      */
      LIBAPI unsigned short & getClass_() {
         return class_;
      }

      /**
      * Function to get category.
      * <br>Description from the FOM: <i>Specifies the main category that describes the environmental entity</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return category
      */
      LIBAPI char & getCategory() {
         return category;
      }

      /**
      * Function to get subcategory.
      * <br>Description from the FOM: <i>Specifies a particular subcategory to which an environmental entity belongs based on the Category field</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return subcategory
      */
      LIBAPI char & getSubcategory() {
         return subcategory;
      }

      /**
      * Function to get specific.
      * <br>Description from the FOM: <i>Identifies specific information about an environmental entity based on the Subcategory field</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return specific
      */
      LIBAPI char & getSpecific() {
         return specific;
      }

      /**
      * Function to get extra.
      * <br>Description from the FOM: <i>Specifies extra information required to describe a particular environmental entity</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return extra
      */
      LIBAPI char & getExtra() {
         return extra;
      }

   };


   LIBAPI bool operator ==(const DevStudio::EnvironmentTypeStruct& l, const DevStudio::EnvironmentTypeStruct& r);
   LIBAPI bool operator !=(const DevStudio::EnvironmentTypeStruct& l, const DevStudio::EnvironmentTypeStruct& r);
   LIBAPI bool operator <(const DevStudio::EnvironmentTypeStruct& l, const DevStudio::EnvironmentTypeStruct& r);
   LIBAPI bool operator >(const DevStudio::EnvironmentTypeStruct& l, const DevStudio::EnvironmentTypeStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::EnvironmentTypeStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::EnvironmentTypeStruct const &);
}
#endif
