/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_RECEIVEROPERATIONALSTATUSENUM_H
#define DEVELOPER_STUDIO_DATATYPES_RECEIVEROPERATIONALSTATUSENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace ReceiverOperationalStatusEnum {
        /**
        * Implementation of the <code>ReceiverOperationalStatusEnum16</code> data type from the FOM.
        * <br>Description from the FOM: <i>The operational state of a radio receiver.</i>
        */
        enum ReceiverOperationalStatusEnum {
            /** <code>Off</code> (with ordinal 0) */
            OFF = 0,
            /** <code>OnButNotReceiving</code> (with ordinal 1) */
            ON_BUT_NOT_RECEIVING = 1,
            /** <code>OnAndReceiving</code> (with ordinal 2) */
            ON_AND_RECEIVING = 2
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to ReceiverOperationalStatusEnum: static_cast<DevStudio::ReceiverOperationalStatusEnum::ReceiverOperationalStatusEnum>(i)
        */
        LIBAPI bool isValid(const ReceiverOperationalStatusEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::ReceiverOperationalStatusEnum::ReceiverOperationalStatusEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::ReceiverOperationalStatusEnum::ReceiverOperationalStatusEnum const &);
}


#endif
