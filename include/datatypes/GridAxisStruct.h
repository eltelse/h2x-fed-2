/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_GRIDAXISSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_GRIDAXISSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/GridAxisTypeVariantStruct.h>

namespace DevStudio {
   /**
   * Implementation of the <code>GridAxisStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Record specifying grid Xi axis data</i>
   */
   class GridAxisStruct {

   public:
      /**
      * Description from the FOM: <i>Specifies the coordinate of the origin (or initial value) for the Xi axis</i>.
      * <br>Description of the data type from the FOM: <i>Double-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      */
      double initialValue;
      /**
      * Description from the FOM: <i>Specifies the coordinate of the endpoint (or final value) for the Xi axis</i>.
      * <br>Description of the data type from the FOM: <i>Double-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      */
      double finalValue;
      /**
      * Description from the FOM: <i>Specifies the total number of grid points along the Xi domain axis</i>.
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      unsigned short totalNumberOfPoints;
      /**
      * Description from the FOM: <i>Specifies the integer valued interleaf factor along a domain (grid) Xi axis ; a value of one shall indicate no sub-sampling (interleaving), while integer values greater than one shall indicate the sampling frequency along an axis</i>.
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      char interleafFactor;
      /**
      * Description from the FOM: <i>Specifies the number of grid locations along the Xi axis</i>.
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      unsigned short numberOfPoints;
      /**
      * Description from the FOM: <i>Specifies the index of the initial grid point along the Xi domain axis</i>.
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      unsigned short initialIndex;
      /**
      * Description from the FOM: <i>Specifies axis data alternatives</i>.
      * <br>Description of the data type from the FOM: <i>Record specifying either regular (fixed spacing) or irregular (variable spacing) axis data</i>
      */
      GridAxisTypeVariantStruct axisTypeAAlternatives;

      LIBAPI GridAxisStruct()
         :
         initialValue(0),
         finalValue(0),
         totalNumberOfPoints(0),
         interleafFactor(0),
         numberOfPoints(0),
         initialIndex(0),
         axisTypeAAlternatives(GridAxisTypeVariantStruct())
      {}

      /**
      * Constructor for GridAxisStruct
      *
      * @param initialValue_ value to set as initialValue.
      * <br>Description from the FOM: <i>Specifies the coordinate of the origin (or initial value) for the Xi axis</i>
      * <br>Description of the data type from the FOM: <i>Double-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      * @param finalValue_ value to set as finalValue.
      * <br>Description from the FOM: <i>Specifies the coordinate of the endpoint (or final value) for the Xi axis</i>
      * <br>Description of the data type from the FOM: <i>Double-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      * @param totalNumberOfPoints_ value to set as totalNumberOfPoints.
      * <br>Description from the FOM: <i>Specifies the total number of grid points along the Xi domain axis</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param interleafFactor_ value to set as interleafFactor.
      * <br>Description from the FOM: <i>Specifies the integer valued interleaf factor along a domain (grid) Xi axis ; a value of one shall indicate no sub-sampling (interleaving), while integer values greater than one shall indicate the sampling frequency along an axis</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param numberOfPoints_ value to set as numberOfPoints.
      * <br>Description from the FOM: <i>Specifies the number of grid locations along the Xi axis</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param initialIndex_ value to set as initialIndex.
      * <br>Description from the FOM: <i>Specifies the index of the initial grid point along the Xi domain axis</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param axisTypeAAlternatives_ value to set as axisTypeAAlternatives.
      * <br>Description from the FOM: <i>Specifies axis data alternatives</i>
      * <br>Description of the data type from the FOM: <i>Record specifying either regular (fixed spacing) or irregular (variable spacing) axis data</i>
      */
      LIBAPI GridAxisStruct(
         double initialValue_,
         double finalValue_,
         unsigned short totalNumberOfPoints_,
         char interleafFactor_,
         unsigned short numberOfPoints_,
         unsigned short initialIndex_,
         GridAxisTypeVariantStruct axisTypeAAlternatives_
         )
         :
         initialValue(initialValue_),
         finalValue(finalValue_),
         totalNumberOfPoints(totalNumberOfPoints_),
         interleafFactor(interleafFactor_),
         numberOfPoints(numberOfPoints_),
         initialIndex(initialIndex_),
         axisTypeAAlternatives(axisTypeAAlternatives_)
      {}



      /**
      * Function to get initialValue.
      * <br>Description from the FOM: <i>Specifies the coordinate of the origin (or initial value) for the Xi axis</i>
      * <br>Description of the data type from the FOM: <i>Double-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      *
      * @return initialValue
      */
      LIBAPI double & getInitialValue() {
         return initialValue;
      }

      /**
      * Function to get finalValue.
      * <br>Description from the FOM: <i>Specifies the coordinate of the endpoint (or final value) for the Xi axis</i>
      * <br>Description of the data type from the FOM: <i>Double-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      *
      * @return finalValue
      */
      LIBAPI double & getFinalValue() {
         return finalValue;
      }

      /**
      * Function to get totalNumberOfPoints.
      * <br>Description from the FOM: <i>Specifies the total number of grid points along the Xi domain axis</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return totalNumberOfPoints
      */
      LIBAPI unsigned short & getTotalNumberOfPoints() {
         return totalNumberOfPoints;
      }

      /**
      * Function to get interleafFactor.
      * <br>Description from the FOM: <i>Specifies the integer valued interleaf factor along a domain (grid) Xi axis ; a value of one shall indicate no sub-sampling (interleaving), while integer values greater than one shall indicate the sampling frequency along an axis</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return interleafFactor
      */
      LIBAPI char & getInterleafFactor() {
         return interleafFactor;
      }

      /**
      * Function to get numberOfPoints.
      * <br>Description from the FOM: <i>Specifies the number of grid locations along the Xi axis</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return numberOfPoints
      */
      LIBAPI unsigned short & getNumberOfPoints() {
         return numberOfPoints;
      }

      /**
      * Function to get initialIndex.
      * <br>Description from the FOM: <i>Specifies the index of the initial grid point along the Xi domain axis</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return initialIndex
      */
      LIBAPI unsigned short & getInitialIndex() {
         return initialIndex;
      }

      /**
      * Function to get axisTypeAAlternatives.
      * <br>Description from the FOM: <i>Specifies axis data alternatives</i>
      * <br>Description of the data type from the FOM: <i>Record specifying either regular (fixed spacing) or irregular (variable spacing) axis data</i>
      *
      * @return axisTypeAAlternatives
      */
      LIBAPI DevStudio::GridAxisTypeVariantStruct & getAxisTypeAAlternatives() {
         return axisTypeAAlternatives;
      }

   };


   LIBAPI bool operator ==(const DevStudio::GridAxisStruct& l, const DevStudio::GridAxisStruct& r);
   LIBAPI bool operator !=(const DevStudio::GridAxisStruct& l, const DevStudio::GridAxisStruct& r);
   LIBAPI bool operator <(const DevStudio::GridAxisStruct& l, const DevStudio::GridAxisStruct& r);
   LIBAPI bool operator >(const DevStudio::GridAxisStruct& l, const DevStudio::GridAxisStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::GridAxisStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::GridAxisStruct const &);
}
#endif
