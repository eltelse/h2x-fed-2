/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_CONSTITUENTPARTRELATIONSHIPSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_CONSTITUENTPARTRELATIONSHIPSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/ConstituentPartNatureEnum.h>
#include <DevStudio/datatypes/ConstituentPartPositionEnum.h>

namespace DevStudio {
   /**
   * Implementation of the <code>ConstituentPartRelationshipStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>The relationship of the constituent part object to its host object. Based on the Relationship record as specified in IEEE 1278.1a-1998 section 5.2.56.</i>
   */
   class ConstituentPartRelationshipStruct {

   public:
      /**
      * Description from the FOM: <i>The nature or purpose for the joining of the constituent part object to the host object.</i>.
      * <br>Description of the data type from the FOM: <i>Relationship nature</i>
      */
      ConstituentPartNatureEnum::ConstituentPartNatureEnum nature;
      /**
      * Description from the FOM: <i>The position of the constituent part object with respect to the host object.</i>.
      * <br>Description of the data type from the FOM: <i>Relationship position</i>
      */
      ConstituentPartPositionEnum::ConstituentPartPositionEnum position;

      LIBAPI ConstituentPartRelationshipStruct()
         :
         nature(ConstituentPartNatureEnum::ConstituentPartNatureEnum()),
         position(ConstituentPartPositionEnum::ConstituentPartPositionEnum())
      {}

      /**
      * Constructor for ConstituentPartRelationshipStruct
      *
      * @param nature_ value to set as nature.
      * <br>Description from the FOM: <i>The nature or purpose for the joining of the constituent part object to the host object.</i>
      * <br>Description of the data type from the FOM: <i>Relationship nature</i>
      * @param position_ value to set as position.
      * <br>Description from the FOM: <i>The position of the constituent part object with respect to the host object.</i>
      * <br>Description of the data type from the FOM: <i>Relationship position</i>
      */
      LIBAPI ConstituentPartRelationshipStruct(
         ConstituentPartNatureEnum::ConstituentPartNatureEnum nature_,
         ConstituentPartPositionEnum::ConstituentPartPositionEnum position_
         )
         :
         nature(nature_),
         position(position_)
      {}



      /**
      * Function to get nature.
      * <br>Description from the FOM: <i>The nature or purpose for the joining of the constituent part object to the host object.</i>
      * <br>Description of the data type from the FOM: <i>Relationship nature</i>
      *
      * @return nature
      */
      LIBAPI DevStudio::ConstituentPartNatureEnum::ConstituentPartNatureEnum & getNature() {
         return nature;
      }

      /**
      * Function to get position.
      * <br>Description from the FOM: <i>The position of the constituent part object with respect to the host object.</i>
      * <br>Description of the data type from the FOM: <i>Relationship position</i>
      *
      * @return position
      */
      LIBAPI DevStudio::ConstituentPartPositionEnum::ConstituentPartPositionEnum & getPosition() {
         return position;
      }

   };


   LIBAPI bool operator ==(const DevStudio::ConstituentPartRelationshipStruct& l, const DevStudio::ConstituentPartRelationshipStruct& r);
   LIBAPI bool operator !=(const DevStudio::ConstituentPartRelationshipStruct& l, const DevStudio::ConstituentPartRelationshipStruct& r);
   LIBAPI bool operator <(const DevStudio::ConstituentPartRelationshipStruct& l, const DevStudio::ConstituentPartRelationshipStruct& r);
   LIBAPI bool operator >(const DevStudio::ConstituentPartRelationshipStruct& l, const DevStudio::ConstituentPartRelationshipStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::ConstituentPartRelationshipStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::ConstituentPartRelationshipStruct const &);
}
#endif
