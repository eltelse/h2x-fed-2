/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_VECTORINGNOZZLESYSTEMDATASTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_VECTORINGNOZZLESYSTEMDATASTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>


namespace DevStudio {
   /**
   * Implementation of the <code>VectoringNozzleSystemDataStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Operational data for describing the vectoring nozzle systems being simulated.</i>
   */
   class VectoringNozzleSystemDataStruct {

   public:
      /**
      * Description from the FOM: <i>The nozzle deflection angle in the horizontal body axis of the entity.</i>.
      * <br>Description of the data type from the FOM: <i>Angle, based on unit degree (of arc), unit symbol °. [unit: degree (deg), resolution: NA, accuracy: NA]</i>
      */
      float horizontalDeflectionAngle;
      /**
      * Description from the FOM: <i>The nozzle deflection angle in the vertical body axis of the entity.</i>.
      * <br>Description of the data type from the FOM: <i>Angle, based on unit degree (of arc), unit symbol °. [unit: degree (deg), resolution: NA, accuracy: NA]</i>
      */
      float verticalDeflectionAngle;

      LIBAPI VectoringNozzleSystemDataStruct()
         :
         horizontalDeflectionAngle(0),
         verticalDeflectionAngle(0)
      {}

      /**
      * Constructor for VectoringNozzleSystemDataStruct
      *
      * @param horizontalDeflectionAngle_ value to set as horizontalDeflectionAngle.
      * <br>Description from the FOM: <i>The nozzle deflection angle in the horizontal body axis of the entity.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on unit degree (of arc), unit symbol °. [unit: degree (deg), resolution: NA, accuracy: NA]</i>
      * @param verticalDeflectionAngle_ value to set as verticalDeflectionAngle.
      * <br>Description from the FOM: <i>The nozzle deflection angle in the vertical body axis of the entity.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on unit degree (of arc), unit symbol °. [unit: degree (deg), resolution: NA, accuracy: NA]</i>
      */
      LIBAPI VectoringNozzleSystemDataStruct(
         float horizontalDeflectionAngle_,
         float verticalDeflectionAngle_
         )
         :
         horizontalDeflectionAngle(horizontalDeflectionAngle_),
         verticalDeflectionAngle(verticalDeflectionAngle_)
      {}



      /**
      * Function to get horizontalDeflectionAngle.
      * <br>Description from the FOM: <i>The nozzle deflection angle in the horizontal body axis of the entity.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on unit degree (of arc), unit symbol °. [unit: degree (deg), resolution: NA, accuracy: NA]</i>
      *
      * @return horizontalDeflectionAngle
      */
      LIBAPI float & getHorizontalDeflectionAngle() {
         return horizontalDeflectionAngle;
      }

      /**
      * Function to get verticalDeflectionAngle.
      * <br>Description from the FOM: <i>The nozzle deflection angle in the vertical body axis of the entity.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on unit degree (of arc), unit symbol °. [unit: degree (deg), resolution: NA, accuracy: NA]</i>
      *
      * @return verticalDeflectionAngle
      */
      LIBAPI float & getVerticalDeflectionAngle() {
         return verticalDeflectionAngle;
      }

   };


   LIBAPI bool operator ==(const DevStudio::VectoringNozzleSystemDataStruct& l, const DevStudio::VectoringNozzleSystemDataStruct& r);
   LIBAPI bool operator !=(const DevStudio::VectoringNozzleSystemDataStruct& l, const DevStudio::VectoringNozzleSystemDataStruct& r);
   LIBAPI bool operator <(const DevStudio::VectoringNozzleSystemDataStruct& l, const DevStudio::VectoringNozzleSystemDataStruct& r);
   LIBAPI bool operator >(const DevStudio::VectoringNozzleSystemDataStruct& l, const DevStudio::VectoringNozzleSystemDataStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::VectoringNozzleSystemDataStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::VectoringNozzleSystemDataStruct const &);
}
#endif
