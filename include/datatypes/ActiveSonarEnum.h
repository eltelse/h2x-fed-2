/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_ACTIVESONARENUM_H
#define DEVELOPER_STUDIO_DATATYPES_ACTIVESONARENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace ActiveSonarEnum {
        /**
        * Implementation of the <code>ActiveSonarEnum16</code> data type from the FOM.
        * <br>Description from the FOM: <i>Acoustic system name</i>
        */
        enum ActiveSonarEnum {
            /** <code>Other</code> (with ordinal 0) */
            OTHER = 0,
            /** <code>AN_BQQ-5</code> (with ordinal 1) */
            AN_BQQ_5 = 1,
            /** <code>AN_SSQ-62</code> (with ordinal 2) */
            AN_SSQ_62 = 2,
            /** <code>AN_SQS-23</code> (with ordinal 3) */
            AN_SQS_23 = 3,
            /** <code>AN_SQS-26</code> (with ordinal 4) */
            AN_SQS_26 = 4,
            /** <code>AN_SQS-53</code> (with ordinal 5) */
            AN_SQS_53 = 5,
            /** <code>ALFS</code> (with ordinal 6) */
            ALFS = 6,
            /** <code>LFA</code> (with ordinal 7) */
            LFA = 7,
            /** <code>AN_AQS-901</code> (with ordinal 8) */
            AN_AQS_901 = 8,
            /** <code>AN_AQS-902</code> (with ordinal 9) */
            AN_AQS_902 = 9
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to ActiveSonarEnum: static_cast<DevStudio::ActiveSonarEnum::ActiveSonarEnum>(i)
        */
        LIBAPI bool isValid(const ActiveSonarEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::ActiveSonarEnum::ActiveSonarEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::ActiveSonarEnum::ActiveSonarEnum const &);
}


#endif
