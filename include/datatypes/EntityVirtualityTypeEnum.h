/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_ENTITYVIRTUALITYTYPEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_ENTITYVIRTUALITYTYPEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace EntityVirtualityTypeEnum {
        /**
        * Implementation of the <code>EntityVirtualityTypeEnum32</code> data type from the FOM.
        * <br>Description from the FOM: <i>-NULL-</i>
        */
        enum EntityVirtualityTypeEnum {
            /** <code>ENTITY_VIRTUAL</code> (with ordinal 0) */
            ENTITY_VIRTUAL = 0,
            /** <code>ENTITY_INSTRUMENTED</code> (with ordinal 1) */
            ENTITY_INSTRUMENTED = 1
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to EntityVirtualityTypeEnum: static_cast<DevStudio::EntityVirtualityTypeEnum::EntityVirtualityTypeEnum>(i)
        */
        LIBAPI bool isValid(const EntityVirtualityTypeEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::EntityVirtualityTypeEnum::EntityVirtualityTypeEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::EntityVirtualityTypeEnum::EntityVirtualityTypeEnum const &);
}


#endif
