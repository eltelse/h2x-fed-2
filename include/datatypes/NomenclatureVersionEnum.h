/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_NOMENCLATUREVERSIONENUM_H
#define DEVELOPER_STUDIO_DATATYPES_NOMENCLATUREVERSIONENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace NomenclatureVersionEnum {
        /**
        * Implementation of the <code>NomenclatureVersionEnum8</code> data type from the FOM.
        * <br>Description from the FOM: <i>The naming convention applied to the radio system by the manufacturer/controlling agency.</i>
        */
        enum NomenclatureVersionEnum {
            /** <code>Other</code> (with ordinal 0) */
            OTHER = 0,
            /** <code>JointElectronicsTypeDesignationSystem_JETDS_Non-specificSeries</code> (with ordinal 1) */
            JOINT_ELECTRONICS_TYPE_DESIGNATION_SYSTEM_JETDS_NON_SPECIFIC_SERIES = 1,
            /** <code>ManufacturerDesignation</code> (with ordinal 2) */
            MANUFACTURER_DESIGNATION = 2,
            /** <code>NationalDesignation</code> (with ordinal 3) */
            NATIONAL_DESIGNATION = 3,
            /** <code>JETDSARCSet1</code> (with ordinal 11) */
            JETDSARCSET1 = 11,
            /** <code>JETDSARCSet2</code> (with ordinal 12) */
            JETDSARCSET2 = 12,
            /** <code>JETDSARCSet3</code> (with ordinal 13) */
            JETDSARCSET3 = 13,
            /** <code>JETDSARCSet4</code> (with ordinal 14) */
            JETDSARCSET4 = 14,
            /** <code>JETDSBRCSet1</code> (with ordinal 15) */
            JETDSBRCSET1 = 15,
            /** <code>JETDSBRCSet2</code> (with ordinal 16) */
            JETDSBRCSET2 = 16,
            /** <code>JETDSBRCSet3</code> (with ordinal 17) */
            JETDSBRCSET3 = 17,
            /** <code>JETDSBRCSet4</code> (with ordinal 18) */
            JETDSBRCSET4 = 18,
            /** <code>JETDSCRCSet1</code> (with ordinal 19) */
            JETDSCRCSET1 = 19,
            /** <code>JETDSCRCSet2</code> (with ordinal 20) */
            JETDSCRCSET2 = 20,
            /** <code>JETDSCRCSet3</code> (with ordinal 21) */
            JETDSCRCSET3 = 21,
            /** <code>JETDSCRCSet4</code> (with ordinal 22) */
            JETDSCRCSET4 = 22,
            /** <code>JETDSDRCSet1</code> (with ordinal 23) */
            JETDSDRCSET1 = 23,
            /** <code>JETDSDRCSet2</code> (with ordinal 24) */
            JETDSDRCSET2 = 24,
            /** <code>JETDSDRCSet3</code> (with ordinal 25) */
            JETDSDRCSET3 = 25,
            /** <code>JETDSDRCSet4</code> (with ordinal 26) */
            JETDSDRCSET4 = 26,
            /** <code>JETDSFRCSet1</code> (with ordinal 27) */
            JETDSFRCSET1 = 27,
            /** <code>JETDSFRCSet2</code> (with ordinal 28) */
            JETDSFRCSET2 = 28,
            /** <code>JETDSFRCSet3</code> (with ordinal 29) */
            JETDSFRCSET3 = 29,
            /** <code>JETDSFRCSet4</code> (with ordinal 30) */
            JETDSFRCSET4 = 30,
            /** <code>JETDSGRCSet1</code> (with ordinal 31) */
            JETDSGRCSET1 = 31,
            /** <code>JETDSGRCSet2</code> (with ordinal 32) */
            JETDSGRCSET2 = 32,
            /** <code>JETDSGRCSet3</code> (with ordinal 33) */
            JETDSGRCSET3 = 33,
            /** <code>JETDSGRCSet4</code> (with ordinal 34) */
            JETDSGRCSET4 = 34,
            /** <code>JETDSKRCSet1</code> (with ordinal 35) */
            JETDSKRCSET1 = 35,
            /** <code>JETDSKRCSet2</code> (with ordinal 36) */
            JETDSKRCSET2 = 36,
            /** <code>JETDSKRCSet3</code> (with ordinal 37) */
            JETDSKRCSET3 = 37,
            /** <code>JETDSKRCSet4</code> (with ordinal 38) */
            JETDSKRCSET4 = 38,
            /** <code>JETDSMRCSet1</code> (with ordinal 39) */
            JETDSMRCSET1 = 39,
            /** <code>JETDSMRCSet2</code> (with ordinal 40) */
            JETDSMRCSET2 = 40,
            /** <code>JETDSMRCSet3</code> (with ordinal 41) */
            JETDSMRCSET3 = 41,
            /** <code>JETDSMRCSet4</code> (with ordinal 42) */
            JETDSMRCSET4 = 42,
            /** <code>JETDSPRCSet1</code> (with ordinal 43) */
            JETDSPRCSET1 = 43,
            /** <code>JETDSPRCSet2</code> (with ordinal 44) */
            JETDSPRCSET2 = 44,
            /** <code>JETDSPRCSet3</code> (with ordinal 45) */
            JETDSPRCSET3 = 45,
            /** <code>JETDSPRCSet4</code> (with ordinal 46) */
            JETDSPRCSET4 = 46,
            /** <code>JETDSSRCSet1</code> (with ordinal 47) */
            JETDSSRCSET1 = 47,
            /** <code>JETDSSRCSet2</code> (with ordinal 48) */
            JETDSSRCSET2 = 48,
            /** <code>JETDSSRCSet3</code> (with ordinal 49) */
            JETDSSRCSET3 = 49,
            /** <code>JETDSSRCSet4</code> (with ordinal 50) */
            JETDSSRCSET4 = 50,
            /** <code>JETDSTRCSet1</code> (with ordinal 51) */
            JETDSTRCSET1 = 51,
            /** <code>JETDSTRCSet2</code> (with ordinal 52) */
            JETDSTRCSET2 = 52,
            /** <code>JETDSTRCSet3</code> (with ordinal 53) */
            JETDSTRCSET3 = 53,
            /** <code>JETDSTRCSet4</code> (with ordinal 54) */
            JETDSTRCSET4 = 54,
            /** <code>JETDSVRCSet1</code> (with ordinal 55) */
            JETDSVRCSET1 = 55,
            /** <code>JETDSVRCSet2</code> (with ordinal 56) */
            JETDSVRCSET2 = 56,
            /** <code>JETDSVRCSet3</code> (with ordinal 57) */
            JETDSVRCSET3 = 57,
            /** <code>JETDSVRCSet4</code> (with ordinal 58) */
            JETDSVRCSET4 = 58,
            /** <code>JETDSWRCSet1</code> (with ordinal 59) */
            JETDSWRCSET1 = 59,
            /** <code>JETDSWRCSet2</code> (with ordinal 60) */
            JETDSWRCSET2 = 60,
            /** <code>JETDSWRCSet3</code> (with ordinal 61) */
            JETDSWRCSET3 = 61,
            /** <code>JETDSWRCSet4</code> (with ordinal 62) */
            JETDSWRCSET4 = 62,
            /** <code>JETDSZRCSet1</code> (with ordinal 63) */
            JETDSZRCSET1 = 63,
            /** <code>JETDSZRCSet2</code> (with ordinal 64) */
            JETDSZRCSET2 = 64,
            /** <code>JETDSZRCSet3</code> (with ordinal 65) */
            JETDSZRCSET3 = 65,
            /** <code>JETDSZRCSet4</code> (with ordinal 66) */
            JETDSZRCSET4 = 66
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to NomenclatureVersionEnum: static_cast<DevStudio::NomenclatureVersionEnum::NomenclatureVersionEnum>(i)
        */
        LIBAPI bool isValid(const NomenclatureVersionEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::NomenclatureVersionEnum::NomenclatureVersionEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::NomenclatureVersionEnum::NomenclatureVersionEnum const &);
}


#endif
