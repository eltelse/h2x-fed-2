/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_VARIABLEDATUMSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_VARIABLEDATUMSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/DatumIdentifierEnum.h>
#include <DevStudio/datatypes/UnsignedInteger64Array1Plus.h>
#include <vector>

namespace DevStudio {
   /**
   * Implementation of the <code>VariableDatumStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>These fields shall specify the types of variable datum, their length, and their value.</i>
   */
   class VariableDatumStruct {

   public:
      /**
      * Description from the FOM: <i>The fixed datum id represented by a 32-bit enumeration</i>.
      * <br>Description of the data type from the FOM: <i>Datum ID</i>
      */
      DatumIdentifierEnum::DatumIdentifierEnum datumID;
      /**
      * Description from the FOM: <i>This field shall specify the length of the variable datum in bits.</i>.
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      unsigned int datumLength;
      /**
      * Description from the FOM: <i>Value of the variable datum defined by the Variable Datum ID and Variable Datum length. This field shall be padded at the end to make the length a multiple of 64-bits.</i>.
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of UnsignedInteger64 elements, containing at least one element.</i>
      */
      std::vector</* not empty */ unsigned long long > datumValue;

      LIBAPI VariableDatumStruct()
         :
         datumID(DatumIdentifierEnum::DatumIdentifierEnum()),
         datumLength(0),
         datumValue(0)
      {}

      /**
      * Constructor for VariableDatumStruct
      *
      * @param datumID_ value to set as datumID.
      * <br>Description from the FOM: <i>The fixed datum id represented by a 32-bit enumeration</i>
      * <br>Description of the data type from the FOM: <i>Datum ID</i>
      * @param datumLength_ value to set as datumLength.
      * <br>Description from the FOM: <i>This field shall specify the length of the variable datum in bits.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param datumValue_ value to set as datumValue.
      * <br>Description from the FOM: <i>Value of the variable datum defined by the Variable Datum ID and Variable Datum length. This field shall be padded at the end to make the length a multiple of 64-bits.</i>
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of UnsignedInteger64 elements, containing at least one element.</i>
      */
      LIBAPI VariableDatumStruct(
         DatumIdentifierEnum::DatumIdentifierEnum datumID_,
         unsigned int datumLength_,
         std::vector</* not empty */ unsigned long long > datumValue_
         )
         :
         datumID(datumID_),
         datumLength(datumLength_),
         datumValue(datumValue_)
      {}



      /**
      * Function to get datumID.
      * <br>Description from the FOM: <i>The fixed datum id represented by a 32-bit enumeration</i>
      * <br>Description of the data type from the FOM: <i>Datum ID</i>
      *
      * @return datumID
      */
      LIBAPI DevStudio::DatumIdentifierEnum::DatumIdentifierEnum & getDatumID() {
         return datumID;
      }

      /**
      * Function to get datumLength.
      * <br>Description from the FOM: <i>This field shall specify the length of the variable datum in bits.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return datumLength
      */
      LIBAPI unsigned int & getDatumLength() {
         return datumLength;
      }

      /**
      * Function to get datumValue.
      * <br>Description from the FOM: <i>Value of the variable datum defined by the Variable Datum ID and Variable Datum length. This field shall be padded at the end to make the length a multiple of 64-bits.</i>
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of UnsignedInteger64 elements, containing at least one element.</i>
      *
      * @return datumValue
      */
      LIBAPI std::vector</* not empty */ unsigned long long > & getDatumValue() {
         return datumValue;
      }

   };


   LIBAPI bool operator ==(const DevStudio::VariableDatumStruct& l, const DevStudio::VariableDatumStruct& r);
   LIBAPI bool operator !=(const DevStudio::VariableDatumStruct& l, const DevStudio::VariableDatumStruct& r);
   LIBAPI bool operator <(const DevStudio::VariableDatumStruct& l, const DevStudio::VariableDatumStruct& r);
   LIBAPI bool operator >(const DevStudio::VariableDatumStruct& l, const DevStudio::VariableDatumStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::VariableDatumStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::VariableDatumStruct const &);
}
#endif
