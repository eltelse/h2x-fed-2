/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_GRIDAXISTYPEVARIANTSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_GRIDAXISTYPEVARIANTSTRUCT_H

#include <DevStudio/datatypes/EnvironmentGridAxisTypeEnum.h>
#include <DevStudio/datatypes/IrregularGridAxisStruct.h>

#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
   /**
   * Implementation of the <code>GridAxisTypeVariantStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Record specifying either regular (fixed spacing) or irregular (variable spacing) axis data</i>
   */
   class GridAxisTypeVariantStruct {

   public:
      LIBAPI GridAxisTypeVariantStruct()
         : 
         _discriminant(DevStudio::EnvironmentGridAxisTypeEnum::EnvironmentGridAxisTypeEnum())
      {}

      /** 
      * Create a new alternative GridAxisTypeVariantStruct, with <code>IrregularGridAxisType</code> as discriminant.
      *
      * @param irregularGridAxis value of the IrregularGridAxis field
      *
      * @return a new GridAxisTypeVariantStruct
      */
      LIBAPI static GridAxisTypeVariantStruct createIrregularGridAxis(DevStudio::IrregularGridAxisStruct irregularGridAxis);

      /**
      * Create a new alternative GridAxisTypeVariantStruct, with <code>RegularGridAxisType</code> as discriminant.
      *
      * @return a new GridAxisTypeVariantStruct
      */
      LIBAPI static GridAxisTypeVariantStruct createRegularGridAxisType();



      /**
      * Function to get discriminant
      *
      * @return disciminant
      */
      LIBAPI DevStudio::EnvironmentGridAxisTypeEnum::EnvironmentGridAxisTypeEnum getDiscriminant() const {
          return _discriminant;
      }

      /**
      * Function to get IrregularGridAxis.
      * Note that this field is only valid of the discriminant is <code>IrregularGridAxisType</code>.
      * <br>Description from the FOM: <i>Specifies irregular (variable spacing) axis data alternative</i>
      * <br>Description of the data type from the FOM: <i>Record specifying irregular (variable spacing) axis data</i>
      *
      * @return IrregularGridAxis value
      */
      LIBAPI DevStudio::IrregularGridAxisStructPtr getIrregularGridAxis() const {
         return _irregularGridAxis;
      }

      /**
      * Function to set IrregularGridAxis.
      * Note that this will set the discriminant to <code>IrregularGridAxisType</code>.
      * <br>Description from the FOM: <i>Specifies irregular (variable spacing) axis data alternative</i>
      * <br>Description of the data type from the FOM: <i>Record specifying irregular (variable spacing) axis data</i>
      *
      * @param irregularGridAxis value used to create a IrregularGridAxisStructPtr
      */
      LIBAPI void setIrregularGridAxis(const DevStudio::IrregularGridAxisStruct irregularGridAxis) {
         _irregularGridAxis = DevStudio::IrregularGridAxisStructPtr( new DevStudio::IrregularGridAxisStruct (irregularGridAxis));
         _discriminant = EnvironmentGridAxisTypeEnum::IRREGULAR_GRID_AXIS_TYPE;
      }

      /** Description from the FOM: <i>Specifies irregular (variable spacing) axis data alternative</i> */
      DevStudio::IrregularGridAxisStructPtr _irregularGridAxis;

   private:
      DevStudio::EnvironmentGridAxisTypeEnum::EnvironmentGridAxisTypeEnum _discriminant;

      GridAxisTypeVariantStruct(
         DevStudio::IrregularGridAxisStructPtr irregularGridAxis,
         DevStudio::EnvironmentGridAxisTypeEnum::EnvironmentGridAxisTypeEnum discriminant
      );
   };

   LIBAPI bool operator ==(const DevStudio::GridAxisTypeVariantStruct& l, const DevStudio::GridAxisTypeVariantStruct& r);
   LIBAPI bool operator !=(const DevStudio::GridAxisTypeVariantStruct& l, const DevStudio::GridAxisTypeVariantStruct& r);
   LIBAPI bool operator <(const DevStudio::GridAxisTypeVariantStruct& l, const DevStudio::GridAxisTypeVariantStruct& r);
   LIBAPI bool operator >(const DevStudio::GridAxisTypeVariantStruct& l, const DevStudio::GridAxisTypeVariantStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::GridAxisTypeVariantStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::GridAxisTypeVariantStruct const &);
}

#endif
