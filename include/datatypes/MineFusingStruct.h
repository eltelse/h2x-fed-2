/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_MINEFUSINGSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_MINEFUSINGSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/MinefieldFusingEnum.h>
#include <DevStudio/datatypes/OctetArray3.h>
#include <vector>

namespace DevStudio {
   /**
   * Implementation of the <code>MineFusingStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Record specifying the type of primary fuse, the type of the secondary fuse and the anti-handling device status of a mine</i>
   */
   class MineFusingStruct {

   public:
      /**
      * Description from the FOM: <i>Specifies the type of the primary fuse of a mine</i>.
      * <br>Description of the data type from the FOM: <i>Minefield fuse type</i>
      */
      MinefieldFusingEnum::MinefieldFusingEnum primary;
      /**
      * Description from the FOM: <i>Specifies the type of the secondary fuse of a mine</i>.
      * <br>Description of the data type from the FOM: <i>Minefield fuse type</i>
      */
      MinefieldFusingEnum::MinefieldFusingEnum secondary;
      /**
      * Description from the FOM: <i>Specifies the anti-handling device status of a mine</i>.
      * <br>Description of the data type from the FOM: <i></i>
      */
      bool antiHandlingDevice;
      /**
      * Description from the FOM: <i>Padding to 32 bits</i>.
      * <br>Description of the data type from the FOM: <i>Generic array of three Octet elements.</i>
      */
      std::vector</* 3 */ char > padding;

      LIBAPI MineFusingStruct()
         :
         primary(MinefieldFusingEnum::MinefieldFusingEnum()),
         secondary(MinefieldFusingEnum::MinefieldFusingEnum()),
         antiHandlingDevice(0),
         padding(0)
      {}

      /**
      * Constructor for MineFusingStruct
      *
      * @param primary_ value to set as primary.
      * <br>Description from the FOM: <i>Specifies the type of the primary fuse of a mine</i>
      * <br>Description of the data type from the FOM: <i>Minefield fuse type</i>
      * @param secondary_ value to set as secondary.
      * <br>Description from the FOM: <i>Specifies the type of the secondary fuse of a mine</i>
      * <br>Description of the data type from the FOM: <i>Minefield fuse type</i>
      * @param antiHandlingDevice_ value to set as antiHandlingDevice.
      * <br>Description from the FOM: <i>Specifies the anti-handling device status of a mine</i>
      * <br>Description of the data type from the FOM: <i></i>
      * @param padding_ value to set as padding.
      * <br>Description from the FOM: <i>Padding to 32 bits</i>
      * <br>Description of the data type from the FOM: <i>Generic array of three Octet elements.</i>
      */
      LIBAPI MineFusingStruct(
         MinefieldFusingEnum::MinefieldFusingEnum primary_,
         MinefieldFusingEnum::MinefieldFusingEnum secondary_,
         bool antiHandlingDevice_,
         std::vector</* 3 */ char > padding_
         )
         :
         primary(primary_),
         secondary(secondary_),
         antiHandlingDevice(antiHandlingDevice_),
         padding(padding_)
      {}



      /**
      * Function to get primary.
      * <br>Description from the FOM: <i>Specifies the type of the primary fuse of a mine</i>
      * <br>Description of the data type from the FOM: <i>Minefield fuse type</i>
      *
      * @return primary
      */
      LIBAPI DevStudio::MinefieldFusingEnum::MinefieldFusingEnum & getPrimary() {
         return primary;
      }

      /**
      * Function to get secondary.
      * <br>Description from the FOM: <i>Specifies the type of the secondary fuse of a mine</i>
      * <br>Description of the data type from the FOM: <i>Minefield fuse type</i>
      *
      * @return secondary
      */
      LIBAPI DevStudio::MinefieldFusingEnum::MinefieldFusingEnum & getSecondary() {
         return secondary;
      }

      /**
      * Function to get antiHandlingDevice.
      * <br>Description from the FOM: <i>Specifies the anti-handling device status of a mine</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return antiHandlingDevice
      */
      LIBAPI bool & getAntiHandlingDevice() {
         return antiHandlingDevice;
      }

      /**
      * Function to get padding.
      * <br>Description from the FOM: <i>Padding to 32 bits</i>
      * <br>Description of the data type from the FOM: <i>Generic array of three Octet elements.</i>
      *
      * @return padding
      */
      LIBAPI std::vector</* 3 */ char > & getPadding() {
         return padding;
      }

   };


   LIBAPI bool operator ==(const DevStudio::MineFusingStruct& l, const DevStudio::MineFusingStruct& r);
   LIBAPI bool operator !=(const DevStudio::MineFusingStruct& l, const DevStudio::MineFusingStruct& r);
   LIBAPI bool operator <(const DevStudio::MineFusingStruct& l, const DevStudio::MineFusingStruct& r);
   LIBAPI bool operator >(const DevStudio::MineFusingStruct& l, const DevStudio::MineFusingStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::MineFusingStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::MineFusingStruct const &);
}
#endif
