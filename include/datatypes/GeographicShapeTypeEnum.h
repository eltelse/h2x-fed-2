/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_GEOGRAPHICSHAPETYPEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_GEOGRAPHICSHAPETYPEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace GeographicShapeTypeEnum {
        /**
        * Implementation of the <code>GeographicShapeTypeEnum32</code> data type from the FOM.
        * <br>Description from the FOM: <i>-NULL-</i>
        */
        enum GeographicShapeTypeEnum {
            /** <code>GEOSHAPE_POINT</code> (with ordinal 0) */
            GEOSHAPE_POINT = 0,
            /** <code>GEOSHAPE_POLYLINE</code> (with ordinal 1) */
            GEOSHAPE_POLYLINE = 1,
            /** <code>GEOSHAPE_POLYGON</code> (with ordinal 2) */
            GEOSHAPE_POLYGON = 2,
            /** <code>GEOSHAPE_ELIPSE</code> (with ordinal 3) */
            GEOSHAPE_ELIPSE = 3,
            /** <code>GEOSHAPE_SECTOR</code> (with ordinal 4) */
            GEOSHAPE_SECTOR = 4,
            /** <code>GEOSHAPE_ARROW</code> (with ordinal 5) */
            GEOSHAPE_ARROW = 5
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to GeographicShapeTypeEnum: static_cast<DevStudio::GeographicShapeTypeEnum::GeographicShapeTypeEnum>(i)
        */
        LIBAPI bool isValid(const GeographicShapeTypeEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::GeographicShapeTypeEnum::GeographicShapeTypeEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::GeographicShapeTypeEnum::GeographicShapeTypeEnum const &);
}


#endif
