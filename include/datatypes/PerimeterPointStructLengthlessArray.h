/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_PERIMETERPOINTSTRUCTLENGTHLESSARRAY_H
#define DEVELOPER_STUDIO_DATATYPES_PERIMETERPOINTSTRUCTLENGTHLESSARRAY_H

#include <iostream>
#include <vector>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/PerimeterPointStruct.h>

namespace DevStudio {
   LIBAPI std::wostream & operator << (std::wostream &, std::vector< DevStudio::PerimeterPointStruct > const &);
   LIBAPI std::ostream & operator << (std::ostream &, std::vector< DevStudio::PerimeterPointStruct > const &);
}
#endif
