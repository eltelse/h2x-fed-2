/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_SILENTAGGREGATESTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_SILENTAGGREGATESTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/EntityTypeStruct.h>

namespace DevStudio {
   /**
   * Implementation of the <code>SilentAggregateStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>These fields contain information about subaggregates not registered in the federation.</i>
   */
   class SilentAggregateStruct {

   public:
      /**
      * Description from the FOM: <i>This field shall specify the aggregates common to this system list.</i>.
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      */
      EntityTypeStruct aggregateType;
      /**
      * Description from the FOM: <i>This field shall specify the number of aggregates that have the type specified in AggregateType field.</i>.
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      unsigned short numberOfAggregatesOfThisType;

      LIBAPI SilentAggregateStruct()
         :
         aggregateType(EntityTypeStruct()),
         numberOfAggregatesOfThisType(0)
      {}

      /**
      * Constructor for SilentAggregateStruct
      *
      * @param aggregateType_ value to set as aggregateType.
      * <br>Description from the FOM: <i>This field shall specify the aggregates common to this system list.</i>
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      * @param numberOfAggregatesOfThisType_ value to set as numberOfAggregatesOfThisType.
      * <br>Description from the FOM: <i>This field shall specify the number of aggregates that have the type specified in AggregateType field.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      LIBAPI SilentAggregateStruct(
         EntityTypeStruct aggregateType_,
         unsigned short numberOfAggregatesOfThisType_
         )
         :
         aggregateType(aggregateType_),
         numberOfAggregatesOfThisType(numberOfAggregatesOfThisType_)
      {}



      /**
      * Function to get aggregateType.
      * <br>Description from the FOM: <i>This field shall specify the aggregates common to this system list.</i>
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      *
      * @return aggregateType
      */
      LIBAPI DevStudio::EntityTypeStruct & getAggregateType() {
         return aggregateType;
      }

      /**
      * Function to get numberOfAggregatesOfThisType.
      * <br>Description from the FOM: <i>This field shall specify the number of aggregates that have the type specified in AggregateType field.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return numberOfAggregatesOfThisType
      */
      LIBAPI unsigned short & getNumberOfAggregatesOfThisType() {
         return numberOfAggregatesOfThisType;
      }

   };


   LIBAPI bool operator ==(const DevStudio::SilentAggregateStruct& l, const DevStudio::SilentAggregateStruct& r);
   LIBAPI bool operator !=(const DevStudio::SilentAggregateStruct& l, const DevStudio::SilentAggregateStruct& r);
   LIBAPI bool operator <(const DevStudio::SilentAggregateStruct& l, const DevStudio::SilentAggregateStruct& r);
   LIBAPI bool operator >(const DevStudio::SilentAggregateStruct& l, const DevStudio::SilentAggregateStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::SilentAggregateStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::SilentAggregateStruct const &);
}
#endif
