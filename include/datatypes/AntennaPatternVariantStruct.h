/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_ANTENNAPATTERNVARIANTSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_ANTENNAPATTERNVARIANTSTRUCT_H

#include <DevStudio/datatypes/AntennaPatternTypeEnum.h>
#include <DevStudio/datatypes/BeamAntennaStruct.h>
#include <DevStudio/datatypes/SphericalHarmonicAntennaStruct.h>

#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
   /**
   * Implementation of the <code>AntennaPatternVariantStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Specifies the radiation pattern from the antenna, its orientation in space, and the polarization of the radiation.</i>
   */
   class AntennaPatternVariantStruct {

   public:
      LIBAPI AntennaPatternVariantStruct()
         : 
         _discriminant(DevStudio::AntennaPatternTypeEnum::AntennaPatternTypeEnum())
      {}

      /** 
      * Create a new alternative AntennaPatternVariantStruct, with <code>Beam</code> as discriminant.
      *
      * @param beamAntenna value of the BeamAntenna field
      *
      * @return a new AntennaPatternVariantStruct
      */
      LIBAPI static AntennaPatternVariantStruct createBeamAntenna(DevStudio::BeamAntennaStruct beamAntenna);

      /** 
      * Create a new alternative AntennaPatternVariantStruct, with <code>SphericalHarmonic</code> as discriminant.
      *
      * @param sphericalHarmonicAntenna value of the SphericalHarmonicAntenna field
      *
      * @return a new AntennaPatternVariantStruct
      */
      LIBAPI static AntennaPatternVariantStruct createSphericalHarmonicAntenna(DevStudio::SphericalHarmonicAntennaStruct sphericalHarmonicAntenna);

      /**
      * Create a new alternative AntennaPatternVariantStruct, with <code>OmniDirectional</code> as discriminant.
      *
      * @return a new AntennaPatternVariantStruct
      */
      LIBAPI static AntennaPatternVariantStruct createOmniDirectional();



      /**
      * Function to get discriminant
      *
      * @return disciminant
      */
      LIBAPI DevStudio::AntennaPatternTypeEnum::AntennaPatternTypeEnum getDiscriminant() const {
          return _discriminant;
      }

      /**
      * Function to get BeamAntenna.
      * Note that this field is only valid of the discriminant is <code>Beam</code>.
      * <br>Description from the FOM: <i>Specifies the direction, pattern, and polarization of radiation from a radio transmitter's antenna, represented with respect to a beam coordinate system.</i>
      * <br>Description of the data type from the FOM: <i>Specifies the direction, pattern, and polarization of radiation from a radio transmitter's antenna.</i>
      *
      * @return BeamAntenna value
      */
      LIBAPI DevStudio::BeamAntennaStructPtr getBeamAntenna() const {
         return _beamAntenna;
      }

      /**
      * Function to set BeamAntenna.
      * Note that this will set the discriminant to <code>Beam</code>.
      * <br>Description from the FOM: <i>Specifies the direction, pattern, and polarization of radiation from a radio transmitter's antenna, represented with respect to a beam coordinate system.</i>
      * <br>Description of the data type from the FOM: <i>Specifies the direction, pattern, and polarization of radiation from a radio transmitter's antenna.</i>
      *
      * @param beamAntenna value used to create a BeamAntennaStructPtr
      */
      LIBAPI void setBeamAntenna(const DevStudio::BeamAntennaStruct beamAntenna) {
         _beamAntenna = DevStudio::BeamAntennaStructPtr( new DevStudio::BeamAntennaStruct (beamAntenna));
         _discriminant = AntennaPatternTypeEnum::BEAM;
      }

      /**
      * Function to get SphericalHarmonicAntenna.
      * Note that this field is only valid of the discriminant is <code>SphericalHarmonic</code>.
      * <br>Description from the FOM: <i>Specifies the direction and radiation pattern from a radio transmitter's antenna, represented with respect to the world coordinate system or entity coordinate system.</i>
      * <br>Description of the data type from the FOM: <i>Specifies the direction and radiation pattern from a radio transmitter's antenna.</i>
      *
      * @return SphericalHarmonicAntenna value
      */
      LIBAPI DevStudio::SphericalHarmonicAntennaStructPtr getSphericalHarmonicAntenna() const {
         return _sphericalHarmonicAntenna;
      }

      /**
      * Function to set SphericalHarmonicAntenna.
      * Note that this will set the discriminant to <code>SphericalHarmonic</code>.
      * <br>Description from the FOM: <i>Specifies the direction and radiation pattern from a radio transmitter's antenna, represented with respect to the world coordinate system or entity coordinate system.</i>
      * <br>Description of the data type from the FOM: <i>Specifies the direction and radiation pattern from a radio transmitter's antenna.</i>
      *
      * @param sphericalHarmonicAntenna value used to create a SphericalHarmonicAntennaStructPtr
      */
      LIBAPI void setSphericalHarmonicAntenna(const DevStudio::SphericalHarmonicAntennaStruct sphericalHarmonicAntenna) {
         _sphericalHarmonicAntenna = DevStudio::SphericalHarmonicAntennaStructPtr( new DevStudio::SphericalHarmonicAntennaStruct (sphericalHarmonicAntenna));
         _discriminant = AntennaPatternTypeEnum::SPHERICAL_HARMONIC;
      }

      /** Description from the FOM: <i>Specifies the direction, pattern, and polarization of radiation from a radio transmitter's antenna, represented with respect to a beam coordinate system.</i> */
      DevStudio::BeamAntennaStructPtr _beamAntenna;
      /** Description from the FOM: <i>Specifies the direction and radiation pattern from a radio transmitter's antenna, represented with respect to the world coordinate system or entity coordinate system.</i> */
      DevStudio::SphericalHarmonicAntennaStructPtr _sphericalHarmonicAntenna;

   private:
      DevStudio::AntennaPatternTypeEnum::AntennaPatternTypeEnum _discriminant;

      AntennaPatternVariantStruct(
         DevStudio::BeamAntennaStructPtr beamAntenna,
         DevStudio::SphericalHarmonicAntennaStructPtr sphericalHarmonicAntenna,
         DevStudio::AntennaPatternTypeEnum::AntennaPatternTypeEnum discriminant
      );
   };

   LIBAPI bool operator ==(const DevStudio::AntennaPatternVariantStruct& l, const DevStudio::AntennaPatternVariantStruct& r);
   LIBAPI bool operator !=(const DevStudio::AntennaPatternVariantStruct& l, const DevStudio::AntennaPatternVariantStruct& r);
   LIBAPI bool operator <(const DevStudio::AntennaPatternVariantStruct& l, const DevStudio::AntennaPatternVariantStruct& r);
   LIBAPI bool operator >(const DevStudio::AntennaPatternVariantStruct& l, const DevStudio::AntennaPatternVariantStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::AntennaPatternVariantStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::AntennaPatternVariantStruct const &);
}

#endif
