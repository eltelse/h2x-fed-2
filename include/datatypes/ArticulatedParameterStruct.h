/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_ARTICULATEDPARAMETERSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_ARTICULATEDPARAMETERSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/ParameterValueVariantStruct.h>

namespace DevStudio {
   /**
   * Implementation of the <code>ArticulatedParameterStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Structure to specify a movable or attached part of an entity. Based on the Articulation Parameter record as specified in IEEE 1278.1-1995 section 5.2.5. 
Note that usage of this datatype for the PhyscialEntity object class attribute ArticulatedParametersArray and MunitionDetonation interaction class parameter ArticulatedPartData shall be in accordance with IEEE 1278.1-1995 Annex A.</i>
   */
   class ArticulatedParameterStruct {

   public:
      /**
      * Description from the FOM: <i>Indicator of a change to the part. This field shall be set to zero for each exercise and sequentially incremented by one for each change in articulation parameters. In the case where all possible values are exhausted, the numbers shall be reused beginning at zero.</i>.
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      char articulatedParameterChange;
      /**
      * Description from the FOM: <i>Identification of the articulated part to which this articulation parameter is attached. This field shall contain the value zero if the articulated part is attached directly to the entity.</i>.
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      unsigned short partAttachedTo;
      /**
      * Description from the FOM: <i>Details of the parameter: whether it is an articulated or an attached part, and its type and value.</i>.
      * <br>Description of the data type from the FOM: <i>Variant record specifying the type of articulation parameter (articulated or attached part), and its type and value.</i>
      */
      ParameterValueVariantStruct parameterValue;

      LIBAPI ArticulatedParameterStruct()
         :
         articulatedParameterChange(0),
         partAttachedTo(0),
         parameterValue(ParameterValueVariantStruct())
      {}

      /**
      * Constructor for ArticulatedParameterStruct
      *
      * @param articulatedParameterChange_ value to set as articulatedParameterChange.
      * <br>Description from the FOM: <i>Indicator of a change to the part. This field shall be set to zero for each exercise and sequentially incremented by one for each change in articulation parameters. In the case where all possible values are exhausted, the numbers shall be reused beginning at zero.</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param partAttachedTo_ value to set as partAttachedTo.
      * <br>Description from the FOM: <i>Identification of the articulated part to which this articulation parameter is attached. This field shall contain the value zero if the articulated part is attached directly to the entity.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param parameterValue_ value to set as parameterValue.
      * <br>Description from the FOM: <i>Details of the parameter: whether it is an articulated or an attached part, and its type and value.</i>
      * <br>Description of the data type from the FOM: <i>Variant record specifying the type of articulation parameter (articulated or attached part), and its type and value.</i>
      */
      LIBAPI ArticulatedParameterStruct(
         char articulatedParameterChange_,
         unsigned short partAttachedTo_,
         ParameterValueVariantStruct parameterValue_
         )
         :
         articulatedParameterChange(articulatedParameterChange_),
         partAttachedTo(partAttachedTo_),
         parameterValue(parameterValue_)
      {}



      /**
      * Function to get articulatedParameterChange.
      * <br>Description from the FOM: <i>Indicator of a change to the part. This field shall be set to zero for each exercise and sequentially incremented by one for each change in articulation parameters. In the case where all possible values are exhausted, the numbers shall be reused beginning at zero.</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return articulatedParameterChange
      */
      LIBAPI char & getArticulatedParameterChange() {
         return articulatedParameterChange;
      }

      /**
      * Function to get partAttachedTo.
      * <br>Description from the FOM: <i>Identification of the articulated part to which this articulation parameter is attached. This field shall contain the value zero if the articulated part is attached directly to the entity.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return partAttachedTo
      */
      LIBAPI unsigned short & getPartAttachedTo() {
         return partAttachedTo;
      }

      /**
      * Function to get parameterValue.
      * <br>Description from the FOM: <i>Details of the parameter: whether it is an articulated or an attached part, and its type and value.</i>
      * <br>Description of the data type from the FOM: <i>Variant record specifying the type of articulation parameter (articulated or attached part), and its type and value.</i>
      *
      * @return parameterValue
      */
      LIBAPI DevStudio::ParameterValueVariantStruct & getParameterValue() {
         return parameterValue;
      }

   };


   LIBAPI bool operator ==(const DevStudio::ArticulatedParameterStruct& l, const DevStudio::ArticulatedParameterStruct& r);
   LIBAPI bool operator !=(const DevStudio::ArticulatedParameterStruct& l, const DevStudio::ArticulatedParameterStruct& r);
   LIBAPI bool operator <(const DevStudio::ArticulatedParameterStruct& l, const DevStudio::ArticulatedParameterStruct& r);
   LIBAPI bool operator >(const DevStudio::ArticulatedParameterStruct& l, const DevStudio::ArticulatedParameterStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::ArticulatedParameterStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::ArticulatedParameterStruct const &);
}
#endif
