/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_RFMODULATIONSYSTEMTYPEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_RFMODULATIONSYSTEMTYPEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace RFModulationSystemTypeEnum {
        /**
        * Implementation of the <code>RFModulationSystemTypeEnum16</code> data type from the FOM.
        * <br>Description from the FOM: <i>The radio system type associated with this transmitter.</i>
        */
        enum RFModulationSystemTypeEnum {
            /** <code>Other</code> (with ordinal 0) */
            OTHER = 0,
            /** <code>Generic</code> (with ordinal 1) */
            GENERIC = 1,
            /** <code>HQ</code> (with ordinal 2) */
            HQ = 2,
            /** <code>HQII</code> (with ordinal 3) */
            HQII = 3,
            /** <code>HQIIA</code> (with ordinal 4) */
            HQIIA = 4,
            /** <code>SINCGARS</code> (with ordinal 5) */
            SINCGARS = 5,
            /** <code>CCTT_SINCGARS</code> (with ordinal 6) */
            CCTT_SINCGARS = 6,
            /** <code>EPLRS_EnhancedPositionLocationReportingSystem_</code> (with ordinal 7) */
            EPLRS_ENHANCED_POSITION_LOCATION_REPORTING_SYSTEM = 7,
            /** <code>JTIDS_MIDS</code> (with ordinal 8) */
            JTIDS_MIDS = 8,
            /** <code>Link11</code> (with ordinal 9) */
            LINK11 = 9,
            /** <code>Link11B</code> (with ordinal 10) */
            LINK11B = 10,
            /** <code>L-BandSATCOM</code> (with ordinal 11) */
            L_BAND_SATCOM = 11,
            /** <code>EnhancedSINCGARS7_3</code> (with ordinal 12) */
            ENHANCED_SINCGARS7_3 = 12,
            /** <code>NavigationAid</code> (with ordinal 13) */
            NAVIGATION_AID = 13
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to RFModulationSystemTypeEnum: static_cast<DevStudio::RFModulationSystemTypeEnum::RFModulationSystemTypeEnum>(i)
        */
        LIBAPI bool isValid(const RFModulationSystemTypeEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::RFModulationSystemTypeEnum::RFModulationSystemTypeEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::RFModulationSystemTypeEnum::RFModulationSystemTypeEnum const &);
}


#endif
