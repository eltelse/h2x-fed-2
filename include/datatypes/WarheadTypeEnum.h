/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_WARHEADTYPEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_WARHEADTYPEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace WarheadTypeEnum {
        /**
        * Implementation of the <code>WarheadTypeEnum16</code> data type from the FOM.
        * <br>Description from the FOM: <i>Warhead</i>
        */
        enum WarheadTypeEnum {
            /** <code>Other</code> (with ordinal 0) */
            OTHER = 0,
            /** <code>CargoVariableSubmunitions</code> (with ordinal 10) */
            CARGO_VARIABLE_SUBMUNITIONS = 10,
            /** <code>FuelAirExplosive</code> (with ordinal 20) */
            FUEL_AIR_EXPLOSIVE = 20,
            /** <code>GlassBeads</code> (with ordinal 30) */
            GLASS_BEADS = 30,
            /** <code>Warhead_1um</code> (with ordinal 31) */
            WARHEAD_1UM = 31,
            /** <code>Warhead_5um</code> (with ordinal 32) */
            WARHEAD_5UM = 32,
            /** <code>Warhead_10um</code> (with ordinal 33) */
            WARHEAD_10UM = 33,
            /** <code>HighExplosive</code> (with ordinal 1000) */
            HIGH_EXPLOSIVE = 1000,
            /** <code>HE_Plastic</code> (with ordinal 1100) */
            HE_PLASTIC = 1100,
            /** <code>HE_Incendiary</code> (with ordinal 1200) */
            HE_INCENDIARY = 1200,
            /** <code>HE_Fragmentation</code> (with ordinal 1300) */
            HE_FRAGMENTATION = 1300,
            /** <code>HE_Antitank</code> (with ordinal 1400) */
            HE_ANTITANK = 1400,
            /** <code>HE_Bomblets</code> (with ordinal 1500) */
            HE_BOMBLETS = 1500,
            /** <code>HE_ShapedCharge</code> (with ordinal 1600) */
            HE_SHAPED_CHARGE = 1600,
            /** <code>HE_ContinuousRod</code> (with ordinal 1610) */
            HE_CONTINUOUS_ROD = 1610,
            /** <code>HE_TungstenBall</code> (with ordinal 1615) */
            HE_TUNGSTEN_BALL = 1615,
            /** <code>HE_BlastFragmentation</code> (with ordinal 1620) */
            HE_BLAST_FRAGMENTATION = 1620,
            /** <code>HE_SteerableDartswithHE</code> (with ordinal 1625) */
            HE_STEERABLE_DARTSWITH_HE = 1625,
            /** <code>HE_Darts</code> (with ordinal 1630) */
            HE_DARTS = 1630,
            /** <code>HE_Flechettes</code> (with ordinal 1635) */
            HE_FLECHETTES = 1635,
            /** <code>HE_DirectedFragmentation</code> (with ordinal 1640) */
            HE_DIRECTED_FRAGMENTATION = 1640,
            /** <code>HE_SemiArmorPiercing</code> (with ordinal 1645) */
            HE_SEMI_ARMOR_PIERCING = 1645,
            /** <code>HE_ShapedChargeFragmentation</code> (with ordinal 1650) */
            HE_SHAPED_CHARGE_FRAGMENTATION = 1650,
            /** <code>HE_SemiArmorPiercingFragmentation</code> (with ordinal 1655) */
            HE_SEMI_ARMOR_PIERCING_FRAGMENTATION = 1655,
            /** <code>HE_HollowCharge</code> (with ordinal 1660) */
            HE_HOLLOW_CHARGE = 1660,
            /** <code>HE_DoubleHollowCharge</code> (with ordinal 1665) */
            HE_DOUBLE_HOLLOW_CHARGE = 1665,
            /** <code>HE_GeneralPurpose</code> (with ordinal 1670) */
            HE_GENERAL_PURPOSE = 1670,
            /** <code>HE_BlastPenetrator</code> (with ordinal 1675) */
            HE_BLAST_PENETRATOR = 1675,
            /** <code>HE_RodPenetrator</code> (with ordinal 1680) */
            HE_ROD_PENETRATOR = 1680,
            /** <code>HE_Antipersonnel</code> (with ordinal 1685) */
            HE_ANTIPERSONNEL = 1685,
            /** <code>Smoke</code> (with ordinal 2000) */
            SMOKE = 2000,
            /** <code>Illumination</code> (with ordinal 3000) */
            ILLUMINATION = 3000,
            /** <code>Practice</code> (with ordinal 4000) */
            PRACTICE = 4000,
            /** <code>Kinetic</code> (with ordinal 5000) */
            KINETIC = 5000,
            /** <code>Mines</code> (with ordinal 6000) */
            MINES = 6000,
            /** <code>Nuclear</code> (with ordinal 7000) */
            NUCLEAR = 7000,
            /** <code>NuclearIMT</code> (with ordinal 7010) */
            NUCLEAR_IMT = 7010,
            /** <code>ChemicalGeneral</code> (with ordinal 8000) */
            CHEMICAL_GENERAL = 8000,
            /** <code>ChemicalBlisterAgent</code> (with ordinal 8100) */
            CHEMICAL_BLISTER_AGENT = 8100,
            /** <code>HD_Mustard</code> (with ordinal 8110) */
            HD_MUSTARD = 8110,
            /** <code>ThickenedHD_Mustard</code> (with ordinal 8115) */
            THICKENED_HD_MUSTARD = 8115,
            /** <code>DustyHD_Mustard</code> (with ordinal 8120) */
            DUSTY_HD_MUSTARD = 8120,
            /** <code>ChemicalBloodAgent</code> (with ordinal 8200) */
            CHEMICAL_BLOOD_AGENT = 8200,
            /** <code>AC_HCN</code> (with ordinal 8210) */
            AC_HCN = 8210,
            /** <code>CK_CNCI</code> (with ordinal 8215) */
            CK_CNCI = 8215,
            /** <code>CG_Phosgene</code> (with ordinal 8220) */
            CG_PHOSGENE = 8220,
            /** <code>ChemicalNerveAgent</code> (with ordinal 8300) */
            CHEMICAL_NERVE_AGENT = 8300,
            /** <code>VX</code> (with ordinal 8310) */
            VX = 8310,
            /** <code>ThickenedVX</code> (with ordinal 8315) */
            THICKENED_VX = 8315,
            /** <code>DustyVX</code> (with ordinal 8320) */
            DUSTY_VX = 8320,
            /** <code>GA_Tabun</code> (with ordinal 8325) */
            GA_TABUN = 8325,
            /** <code>ThickenedGA_Tabun</code> (with ordinal 8330) */
            THICKENED_GA_TABUN = 8330,
            /** <code>DustyGA_Tabun</code> (with ordinal 8335) */
            DUSTY_GA_TABUN = 8335,
            /** <code>GB_Sarin</code> (with ordinal 8340) */
            GB_SARIN = 8340,
            /** <code>ThickenedGB_Sarin</code> (with ordinal 8345) */
            THICKENED_GB_SARIN = 8345,
            /** <code>DustyGB_Sarin</code> (with ordinal 8350) */
            DUSTY_GB_SARIN = 8350,
            /** <code>GD_Soman</code> (with ordinal 8355) */
            GD_SOMAN = 8355,
            /** <code>ThickenedGD_Soman</code> (with ordinal 8360) */
            THICKENED_GD_SOMAN = 8360,
            /** <code>DustyGD_Soman</code> (with ordinal 8365) */
            DUSTY_GD_SOMAN = 8365,
            /** <code>GF</code> (with ordinal 8370) */
            GF = 8370,
            /** <code>ThickenedGF</code> (with ordinal 8375) */
            THICKENED_GF = 8375,
            /** <code>DustyGF</code> (with ordinal 8380) */
            DUSTY_GF = 8380,
            /** <code>Biological</code> (with ordinal 9000) */
            BIOLOGICAL = 9000,
            /** <code>BiologicalVirus</code> (with ordinal 9100) */
            BIOLOGICAL_VIRUS = 9100,
            /** <code>BiologicalBacteria</code> (with ordinal 9200) */
            BIOLOGICAL_BACTERIA = 9200,
            /** <code>BiologicalRickettsia</code> (with ordinal 9300) */
            BIOLOGICAL_RICKETTSIA = 9300,
            /** <code>BiologicalGeneticallyModifiedMicroOrganisms</code> (with ordinal 9400) */
            BIOLOGICAL_GENETICALLY_MODIFIED_MICRO_ORGANISMS = 9400,
            /** <code>BiologicalToxin</code> (with ordinal 9500) */
            BIOLOGICAL_TOXIN = 9500
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to WarheadTypeEnum: static_cast<DevStudio::WarheadTypeEnum::WarheadTypeEnum>(i)
        */
        LIBAPI bool isValid(const WarheadTypeEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::WarheadTypeEnum::WarheadTypeEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::WarheadTypeEnum::WarheadTypeEnum const &);
}


#endif
