/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_GEOGRAPHICSHAPESTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_GEOGRAPHICSHAPESTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/GeographicShapeTypeEnum.h>

namespace DevStudio {
   /**
   * Implementation of the <code>GeographicShapeStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>-NULL-</i>
   */
   class GeographicShapeStruct {

   public:
      /**
      * Description from the FOM: <i></i>.
      * <br>Description of the data type from the FOM: <i>-NULL-</i>
      */
      GeographicShapeTypeEnum::GeographicShapeTypeEnum shapeType;

      LIBAPI GeographicShapeStruct()
         :
         shapeType(GeographicShapeTypeEnum::GeographicShapeTypeEnum())
      {}

      /**
      * Constructor for GeographicShapeStruct
      *
      * @param shapeType_ value to set as shapeType.
      * <br>Description from the FOM: <i></i>
      * <br>Description of the data type from the FOM: <i>-NULL-</i>
      */
      LIBAPI GeographicShapeStruct(
         GeographicShapeTypeEnum::GeographicShapeTypeEnum shapeType_
         )
         :
         shapeType(shapeType_)
      {}



      /**
      * Function to get shapeType.
      * <br>Description from the FOM: <i></i>
      * <br>Description of the data type from the FOM: <i>-NULL-</i>
      *
      * @return shapeType
      */
      LIBAPI DevStudio::GeographicShapeTypeEnum::GeographicShapeTypeEnum & getShapeType() {
         return shapeType;
      }

   };


   LIBAPI bool operator ==(const DevStudio::GeographicShapeStruct& l, const DevStudio::GeographicShapeStruct& r);
   LIBAPI bool operator !=(const DevStudio::GeographicShapeStruct& l, const DevStudio::GeographicShapeStruct& r);
   LIBAPI bool operator <(const DevStudio::GeographicShapeStruct& l, const DevStudio::GeographicShapeStruct& r);
   LIBAPI bool operator >(const DevStudio::GeographicShapeStruct& l, const DevStudio::GeographicShapeStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::GeographicShapeStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::GeographicShapeStruct const &);
}
#endif
