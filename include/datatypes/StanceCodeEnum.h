/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_STANCECODEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_STANCECODEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace StanceCodeEnum {
        /**
        * Implementation of the <code>StanceCodeEnum32</code> data type from the FOM.
        * <br>Description from the FOM: <i>Life form state</i>
        */
        enum StanceCodeEnum {
            /** <code>NotApplicable</code> (with ordinal 0) */
            NOT_APPLICABLE = 0,
            /** <code>UprightStandingStill</code> (with ordinal 1) */
            UPRIGHT_STANDING_STILL = 1,
            /** <code>UprightWalking</code> (with ordinal 2) */
            UPRIGHT_WALKING = 2,
            /** <code>UprightRunning</code> (with ordinal 3) */
            UPRIGHT_RUNNING = 3,
            /** <code>Kneeling</code> (with ordinal 4) */
            KNEELING = 4,
            /** <code>Prone</code> (with ordinal 5) */
            PRONE = 5,
            /** <code>Crawling</code> (with ordinal 6) */
            CRAWLING = 6,
            /** <code>Swimming</code> (with ordinal 7) */
            SWIMMING = 7,
            /** <code>Parachuting</code> (with ordinal 8) */
            PARACHUTING = 8,
            /** <code>Jumping</code> (with ordinal 9) */
            JUMPING = 9,
            /** <code>Sitting</code> (with ordinal 10) */
            SITTING = 10,
            /** <code>Squatting</code> (with ordinal 11) */
            SQUATTING = 11,
            /** <code>Crouching</code> (with ordinal 12) */
            CROUCHING = 12,
            /** <code>Wading</code> (with ordinal 13) */
            WADING = 13,
            /** <code>Surrender</code> (with ordinal 14) */
            SURRENDER = 14,
            /** <code>Detained</code> (with ordinal 15) */
            DETAINED = 15
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to StanceCodeEnum: static_cast<DevStudio::StanceCodeEnum::StanceCodeEnum>(i)
        */
        LIBAPI bool isValid(const StanceCodeEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::StanceCodeEnum::StanceCodeEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::StanceCodeEnum::StanceCodeEnum const &);
}


#endif
