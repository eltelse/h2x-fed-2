/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_DETONATIONRESULTCODEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_DETONATIONRESULTCODEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace DetonationResultCodeEnum {
        /**
        * Implementation of the <code>DetonationResultCodeEnum8</code> data type from the FOM.
        * <br>Description from the FOM: <i>Detonation result</i>
        */
        enum DetonationResultCodeEnum {
            /** <code>Other</code> (with ordinal 0) */
            OTHER = 0,
            /** <code>EntityImpact</code> (with ordinal 1) */
            ENTITY_IMPACT = 1,
            /** <code>EntityProximateDetonation</code> (with ordinal 2) */
            ENTITY_PROXIMATE_DETONATION = 2,
            /** <code>GroundImpact</code> (with ordinal 3) */
            GROUND_IMPACT = 3,
            /** <code>GroundProximateDetonation</code> (with ordinal 4) */
            GROUND_PROXIMATE_DETONATION = 4,
            /** <code>Detonation</code> (with ordinal 5) */
            DETONATION = 5,
            /** <code>None</code> (with ordinal 6) */
            NONE = 6,
            /** <code>HE_hit_Small</code> (with ordinal 7) */
            HE_HIT_SMALL = 7,
            /** <code>HE_hit_Medium</code> (with ordinal 8) */
            HE_HIT_MEDIUM = 8,
            /** <code>HE_hit_Large</code> (with ordinal 9) */
            HE_HIT_LARGE = 9,
            /** <code>ArmorPiercingHit</code> (with ordinal 10) */
            ARMOR_PIERCING_HIT = 10,
            /** <code>DirtBlast_Small</code> (with ordinal 11) */
            DIRT_BLAST_SMALL = 11,
            /** <code>DirtBlast_Medium</code> (with ordinal 12) */
            DIRT_BLAST_MEDIUM = 12,
            /** <code>DirtBlast_Large</code> (with ordinal 13) */
            DIRT_BLAST_LARGE = 13,
            /** <code>WaterBlast_Small</code> (with ordinal 14) */
            WATER_BLAST_SMALL = 14,
            /** <code>WaterBlast_Medium</code> (with ordinal 15) */
            WATER_BLAST_MEDIUM = 15,
            /** <code>WaterBlast_Large</code> (with ordinal 16) */
            WATER_BLAST_LARGE = 16,
            /** <code>AirHit</code> (with ordinal 17) */
            AIR_HIT = 17,
            /** <code>BuildingHit_Small</code> (with ordinal 18) */
            BUILDING_HIT_SMALL = 18,
            /** <code>BuildingHit_Medium</code> (with ordinal 19) */
            BUILDING_HIT_MEDIUM = 19,
            /** <code>BuildingHit_Large</code> (with ordinal 20) */
            BUILDING_HIT_LARGE = 20,
            /** <code>MineClearingLineCharge</code> (with ordinal 21) */
            MINE_CLEARING_LINE_CHARGE = 21,
            /** <code>EnvironmentObjectImpact</code> (with ordinal 22) */
            ENVIRONMENT_OBJECT_IMPACT = 22,
            /** <code>EnvironmentObjectProximateDetonation</code> (with ordinal 23) */
            ENVIRONMENT_OBJECT_PROXIMATE_DETONATION = 23,
            /** <code>WaterImpact</code> (with ordinal 24) */
            WATER_IMPACT = 24,
            /** <code>AirBurst</code> (with ordinal 25) */
            AIR_BURST = 25,
            /** <code>Kill_with_fragment_type_1</code> (with ordinal 26) */
            KILL_WITH_FRAGMENT_TYPE_1 = 26,
            /** <code>Kill_with_fragment_type_2</code> (with ordinal 27) */
            KILL_WITH_FRAGMENT_TYPE_2 = 27,
            /** <code>Kill_with_fragment_type_3</code> (with ordinal 28) */
            KILL_WITH_FRAGMENT_TYPE_3 = 28,
            /** <code>Kill_with_fragment_type_1_after_fly-out_failure</code> (with ordinal 29) */
            KILL_WITH_FRAGMENT_TYPE_1_AFTER_FLY_OUT_FAILURE = 29,
            /** <code>Kill_with_fragment_type_2_after_fly-out_failure</code> (with ordinal 30) */
            KILL_WITH_FRAGMENT_TYPE_2_AFTER_FLY_OUT_FAILURE = 30,
            /** <code>Miss_due_to_fly-out_failure</code> (with ordinal 31) */
            MISS_DUE_TO_FLY_OUT_FAILURE = 31,
            /** <code>Miss_due_to_end-game_failure</code> (with ordinal 32) */
            MISS_DUE_TO_END_GAME_FAILURE = 32,
            /** <code>Miss_due_to_fly-out_and_end-game_failure</code> (with ordinal 33) */
            MISS_DUE_TO_FLY_OUT_AND_END_GAME_FAILURE = 33
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to DetonationResultCodeEnum: static_cast<DevStudio::DetonationResultCodeEnum::DetonationResultCodeEnum>(i)
        */
        LIBAPI bool isValid(const DetonationResultCodeEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::DetonationResultCodeEnum::DetonationResultCodeEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::DetonationResultCodeEnum::DetonationResultCodeEnum const &);
}


#endif
