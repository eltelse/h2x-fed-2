/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_PARAMETERTYPEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_PARAMETERTYPEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace ParameterTypeEnum {
        /**
        * Implementation of the <code>ParameterTypeEnum32</code> data type from the FOM.
        * <br>Description from the FOM: <i>Parameter type</i>
        */
        enum ParameterTypeEnum {
            /** <code>ArticulatedPart</code> (with ordinal 0) */
            ARTICULATED_PART = 0,
            /** <code>AttachedPart</code> (with ordinal 1) */
            ATTACHED_PART = 1,
            /** <code>Separation</code> (with ordinal 2) */
            SEPARATION = 2,
            /** <code>EntityType</code> (with ordinal 3) */
            ENTITY_TYPE = 3,
            /** <code>EntityAssociation</code> (with ordinal 4) */
            ENTITY_ASSOCIATION = 4
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to ParameterTypeEnum: static_cast<DevStudio::ParameterTypeEnum::ParameterTypeEnum>(i)
        */
        LIBAPI bool isValid(const ParameterTypeEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::ParameterTypeEnum::ParameterTypeEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::ParameterTypeEnum::ParameterTypeEnum const &);
}


#endif
