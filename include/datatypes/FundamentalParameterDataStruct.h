/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_FUNDAMENTALPARAMETERDATASTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_FUNDAMENTALPARAMETERDATASTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/IffApplicableModesEnum.h>
#include <DevStudio/datatypes/OctetArray3.h>
#include <vector>

namespace DevStudio {
   /**
   * Implementation of the <code>FundamentalParameterDataStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Fundamental Parameter Data record.</i>
   */
   class FundamentalParameterDataStruct {

   public:
      /**
      * Description from the FOM: <i>The average peak radiated power of the emission.</i>.
      * <br>Description of the data type from the FOM: <i>Power ratio in decibels (dB) of a measured power referenced to 1 milliwatt (mW). [unit: decibel milliwatt (dBm), resolution: NA, accuracy: perfect]</i>
      */
      float eRP;
      /**
      * Description from the FOM: <i>The center frequency of the emission.</i>.
      * <br>Description of the data type from the FOM: <i>Frequency, based on SI derived unit hertz, unit symbol Hz. [unit: hertz (Hz), resolution: NA, accuracy: NA]</i>
      */
      float frequency;
      /**
      * Description from the FOM: <i>When applied to originators, this field shall specify the number of interrogations per second emitted. This field shall be set to zero when applied to a responder (i.e. transponder) systems.</i>.
      * <br>Description of the data type from the FOM: <i>Number of interrogations per second. [unit: interrogations/second, resolution: NA, accuracy: perfect]</i>
      */
      float pgRF;
      /**
      * Description from the FOM: <i>The duration of the fundamental pulse of which the interrogation or reply is composed.</i>.
      * <br>Description of the data type from the FOM: <i>Time, based on SI base unit second, expressed in microsecond, unit symbol μs. [unit: microsecond, resolution: NA, accuracy: NA]</i>
      */
      float pulseWidth;
      /**
      * Description from the FOM: <i>The number of emissions generated in a single burst. This field shall contain zero for continuously emitting systems and shall contain the value 1 for responders.</i>.
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^31, 2^31-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      int burstLength;
      /**
      * Description from the FOM: <i>Specifies the modes to which the fundamental parameter data applies.</i>.
      * <br>Description of the data type from the FOM: <i>IFF applicable modes</i>
      */
      IffApplicableModesEnum::IffApplicableModesEnum applicableModes;
      /**
      * Description from the FOM: <i>Padding to 32 bits</i>.
      * <br>Description of the data type from the FOM: <i>Generic array of three Octet elements.</i>
      */
      std::vector</* 3 */ char > padding;

      LIBAPI FundamentalParameterDataStruct()
         :
         eRP(0),
         frequency(0),
         pgRF(0),
         pulseWidth(0),
         burstLength(0),
         applicableModes(IffApplicableModesEnum::IffApplicableModesEnum()),
         padding(0)
      {}

      /**
      * Constructor for FundamentalParameterDataStruct
      *
      * @param eRP_ value to set as eRP.
      * <br>Description from the FOM: <i>The average peak radiated power of the emission.</i>
      * <br>Description of the data type from the FOM: <i>Power ratio in decibels (dB) of a measured power referenced to 1 milliwatt (mW). [unit: decibel milliwatt (dBm), resolution: NA, accuracy: perfect]</i>
      * @param frequency_ value to set as frequency.
      * <br>Description from the FOM: <i>The center frequency of the emission.</i>
      * <br>Description of the data type from the FOM: <i>Frequency, based on SI derived unit hertz, unit symbol Hz. [unit: hertz (Hz), resolution: NA, accuracy: NA]</i>
      * @param pgRF_ value to set as pgRF.
      * <br>Description from the FOM: <i>When applied to originators, this field shall specify the number of interrogations per second emitted. This field shall be set to zero when applied to a responder (i.e. transponder) systems.</i>
      * <br>Description of the data type from the FOM: <i>Number of interrogations per second. [unit: interrogations/second, resolution: NA, accuracy: perfect]</i>
      * @param pulseWidth_ value to set as pulseWidth.
      * <br>Description from the FOM: <i>The duration of the fundamental pulse of which the interrogation or reply is composed.</i>
      * <br>Description of the data type from the FOM: <i>Time, based on SI base unit second, expressed in microsecond, unit symbol μs. [unit: microsecond, resolution: NA, accuracy: NA]</i>
      * @param burstLength_ value to set as burstLength.
      * <br>Description from the FOM: <i>The number of emissions generated in a single burst. This field shall contain zero for continuously emitting systems and shall contain the value 1 for responders.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^31, 2^31-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param applicableModes_ value to set as applicableModes.
      * <br>Description from the FOM: <i>Specifies the modes to which the fundamental parameter data applies.</i>
      * <br>Description of the data type from the FOM: <i>IFF applicable modes</i>
      * @param padding_ value to set as padding.
      * <br>Description from the FOM: <i>Padding to 32 bits</i>
      * <br>Description of the data type from the FOM: <i>Generic array of three Octet elements.</i>
      */
      LIBAPI FundamentalParameterDataStruct(
         float eRP_,
         float frequency_,
         float pgRF_,
         float pulseWidth_,
         int burstLength_,
         IffApplicableModesEnum::IffApplicableModesEnum applicableModes_,
         std::vector</* 3 */ char > padding_
         )
         :
         eRP(eRP_),
         frequency(frequency_),
         pgRF(pgRF_),
         pulseWidth(pulseWidth_),
         burstLength(burstLength_),
         applicableModes(applicableModes_),
         padding(padding_)
      {}



      /**
      * Function to get eRP.
      * <br>Description from the FOM: <i>The average peak radiated power of the emission.</i>
      * <br>Description of the data type from the FOM: <i>Power ratio in decibels (dB) of a measured power referenced to 1 milliwatt (mW). [unit: decibel milliwatt (dBm), resolution: NA, accuracy: perfect]</i>
      *
      * @return eRP
      */
      LIBAPI float & getERP() {
         return eRP;
      }

      /**
      * Function to get frequency.
      * <br>Description from the FOM: <i>The center frequency of the emission.</i>
      * <br>Description of the data type from the FOM: <i>Frequency, based on SI derived unit hertz, unit symbol Hz. [unit: hertz (Hz), resolution: NA, accuracy: NA]</i>
      *
      * @return frequency
      */
      LIBAPI float & getFrequency() {
         return frequency;
      }

      /**
      * Function to get pgRF.
      * <br>Description from the FOM: <i>When applied to originators, this field shall specify the number of interrogations per second emitted. This field shall be set to zero when applied to a responder (i.e. transponder) systems.</i>
      * <br>Description of the data type from the FOM: <i>Number of interrogations per second. [unit: interrogations/second, resolution: NA, accuracy: perfect]</i>
      *
      * @return pgRF
      */
      LIBAPI float & getPgRF() {
         return pgRF;
      }

      /**
      * Function to get pulseWidth.
      * <br>Description from the FOM: <i>The duration of the fundamental pulse of which the interrogation or reply is composed.</i>
      * <br>Description of the data type from the FOM: <i>Time, based on SI base unit second, expressed in microsecond, unit symbol μs. [unit: microsecond, resolution: NA, accuracy: NA]</i>
      *
      * @return pulseWidth
      */
      LIBAPI float & getPulseWidth() {
         return pulseWidth;
      }

      /**
      * Function to get burstLength.
      * <br>Description from the FOM: <i>The number of emissions generated in a single burst. This field shall contain zero for continuously emitting systems and shall contain the value 1 for responders.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^31, 2^31-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return burstLength
      */
      LIBAPI int & getBurstLength() {
         return burstLength;
      }

      /**
      * Function to get applicableModes.
      * <br>Description from the FOM: <i>Specifies the modes to which the fundamental parameter data applies.</i>
      * <br>Description of the data type from the FOM: <i>IFF applicable modes</i>
      *
      * @return applicableModes
      */
      LIBAPI DevStudio::IffApplicableModesEnum::IffApplicableModesEnum & getApplicableModes() {
         return applicableModes;
      }

      /**
      * Function to get padding.
      * <br>Description from the FOM: <i>Padding to 32 bits</i>
      * <br>Description of the data type from the FOM: <i>Generic array of three Octet elements.</i>
      *
      * @return padding
      */
      LIBAPI std::vector</* 3 */ char > & getPadding() {
         return padding;
      }

   };


   LIBAPI bool operator ==(const DevStudio::FundamentalParameterDataStruct& l, const DevStudio::FundamentalParameterDataStruct& r);
   LIBAPI bool operator !=(const DevStudio::FundamentalParameterDataStruct& l, const DevStudio::FundamentalParameterDataStruct& r);
   LIBAPI bool operator <(const DevStudio::FundamentalParameterDataStruct& l, const DevStudio::FundamentalParameterDataStruct& r);
   LIBAPI bool operator >(const DevStudio::FundamentalParameterDataStruct& l, const DevStudio::FundamentalParameterDataStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::FundamentalParameterDataStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::FundamentalParameterDataStruct const &);
}
#endif
