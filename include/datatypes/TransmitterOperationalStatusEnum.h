/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_TRANSMITTEROPERATIONALSTATUSENUM_H
#define DEVELOPER_STUDIO_DATATYPES_TRANSMITTEROPERATIONALSTATUSENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace TransmitterOperationalStatusEnum {
        /**
        * Implementation of the <code>TransmitterOperationalStatusEnum8</code> data type from the FOM.
        * <br>Description from the FOM: <i>The current operational state of a radio transmitter.</i>
        */
        enum TransmitterOperationalStatusEnum {
            /** <code>Off</code> (with ordinal 0) */
            OFF = 0,
            /** <code>OnButNotTransmitting</code> (with ordinal 1) */
            ON_BUT_NOT_TRANSMITTING = 1,
            /** <code>OnAndTransmitting</code> (with ordinal 2) */
            ON_AND_TRANSMITTING = 2
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to TransmitterOperationalStatusEnum: static_cast<DevStudio::TransmitterOperationalStatusEnum::TransmitterOperationalStatusEnum>(i)
        */
        LIBAPI bool isValid(const TransmitterOperationalStatusEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::TransmitterOperationalStatusEnum::TransmitterOperationalStatusEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::TransmitterOperationalStatusEnum::TransmitterOperationalStatusEnum const &);
}


#endif
