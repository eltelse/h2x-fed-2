/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_SHAFTDATASTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_SHAFTDATASTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>


namespace DevStudio {
   /**
   * Implementation of the <code>ShaftDataStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Rotation state of a ship's propulsion shaft</i>
   */
   class ShaftDataStruct {

   public:
      /**
      * Description from the FOM: <i>The current shaft speed. A positive shaft speed indicates that the shaft is rotating in a clockwise direction (when viewed from the stern to bow), and a negative shaft speed indicates an anti-clockwise direction.</i>.
      * <br>Description of the data type from the FOM: <i>Frequency of rotation, expressed in revolutions per minute. [unit: revolutions per minute (RPM), resolution: 1, accuracy: NA]</i>
      */
      short currentShaftRate;
      /**
      * Description from the FOM: <i>The ordered shaft speed. If the ordered shaft speed is different from the current shaft speed then the shaft will accelerate or decelerate at the shaft rate of change. A positive shaft speed indicates that the shaft is rotating in a clockwise direction (when viewed from the stern to bow), and a negative shaft speed indicates an anti-clockwise direction.</i>.
      * <br>Description of the data type from the FOM: <i>Frequency of rotation, expressed in revolutions per minute. [unit: revolutions per minute (RPM), resolution: 1, accuracy: NA]</i>
      */
      short orderedShaftRate;
      /**
      * Description from the FOM: <i>The absolute value of the shaft's rotational acceleration. The shaft will change its rotational speed at this rate if the current shaft speed differs from the ordered shaft rate.</i>.
      * <br>Description of the data type from the FOM: <i>Angular acceleration [unit: RPM/s, resolution: 1, accuracy: perfect]</i>
      */
      short shaftRateOfChange;

      LIBAPI ShaftDataStruct()
         :
         currentShaftRate(0),
         orderedShaftRate(0),
         shaftRateOfChange(0)
      {}

      /**
      * Constructor for ShaftDataStruct
      *
      * @param currentShaftRate_ value to set as currentShaftRate.
      * <br>Description from the FOM: <i>The current shaft speed. A positive shaft speed indicates that the shaft is rotating in a clockwise direction (when viewed from the stern to bow), and a negative shaft speed indicates an anti-clockwise direction.</i>
      * <br>Description of the data type from the FOM: <i>Frequency of rotation, expressed in revolutions per minute. [unit: revolutions per minute (RPM), resolution: 1, accuracy: NA]</i>
      * @param orderedShaftRate_ value to set as orderedShaftRate.
      * <br>Description from the FOM: <i>The ordered shaft speed. If the ordered shaft speed is different from the current shaft speed then the shaft will accelerate or decelerate at the shaft rate of change. A positive shaft speed indicates that the shaft is rotating in a clockwise direction (when viewed from the stern to bow), and a negative shaft speed indicates an anti-clockwise direction.</i>
      * <br>Description of the data type from the FOM: <i>Frequency of rotation, expressed in revolutions per minute. [unit: revolutions per minute (RPM), resolution: 1, accuracy: NA]</i>
      * @param shaftRateOfChange_ value to set as shaftRateOfChange.
      * <br>Description from the FOM: <i>The absolute value of the shaft's rotational acceleration. The shaft will change its rotational speed at this rate if the current shaft speed differs from the ordered shaft rate.</i>
      * <br>Description of the data type from the FOM: <i>Angular acceleration [unit: RPM/s, resolution: 1, accuracy: perfect]</i>
      */
      LIBAPI ShaftDataStruct(
         short currentShaftRate_,
         short orderedShaftRate_,
         short shaftRateOfChange_
         )
         :
         currentShaftRate(currentShaftRate_),
         orderedShaftRate(orderedShaftRate_),
         shaftRateOfChange(shaftRateOfChange_)
      {}



      /**
      * Function to get currentShaftRate.
      * <br>Description from the FOM: <i>The current shaft speed. A positive shaft speed indicates that the shaft is rotating in a clockwise direction (when viewed from the stern to bow), and a negative shaft speed indicates an anti-clockwise direction.</i>
      * <br>Description of the data type from the FOM: <i>Frequency of rotation, expressed in revolutions per minute. [unit: revolutions per minute (RPM), resolution: 1, accuracy: NA]</i>
      *
      * @return currentShaftRate
      */
      LIBAPI short & getCurrentShaftRate() {
         return currentShaftRate;
      }

      /**
      * Function to get orderedShaftRate.
      * <br>Description from the FOM: <i>The ordered shaft speed. If the ordered shaft speed is different from the current shaft speed then the shaft will accelerate or decelerate at the shaft rate of change. A positive shaft speed indicates that the shaft is rotating in a clockwise direction (when viewed from the stern to bow), and a negative shaft speed indicates an anti-clockwise direction.</i>
      * <br>Description of the data type from the FOM: <i>Frequency of rotation, expressed in revolutions per minute. [unit: revolutions per minute (RPM), resolution: 1, accuracy: NA]</i>
      *
      * @return orderedShaftRate
      */
      LIBAPI short & getOrderedShaftRate() {
         return orderedShaftRate;
      }

      /**
      * Function to get shaftRateOfChange.
      * <br>Description from the FOM: <i>The absolute value of the shaft's rotational acceleration. The shaft will change its rotational speed at this rate if the current shaft speed differs from the ordered shaft rate.</i>
      * <br>Description of the data type from the FOM: <i>Angular acceleration [unit: RPM/s, resolution: 1, accuracy: perfect]</i>
      *
      * @return shaftRateOfChange
      */
      LIBAPI short & getShaftRateOfChange() {
         return shaftRateOfChange;
      }

   };


   LIBAPI bool operator ==(const DevStudio::ShaftDataStruct& l, const DevStudio::ShaftDataStruct& r);
   LIBAPI bool operator !=(const DevStudio::ShaftDataStruct& l, const DevStudio::ShaftDataStruct& r);
   LIBAPI bool operator <(const DevStudio::ShaftDataStruct& l, const DevStudio::ShaftDataStruct& r);
   LIBAPI bool operator >(const DevStudio::ShaftDataStruct& l, const DevStudio::ShaftDataStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::ShaftDataStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::ShaftDataStruct const &);
}
#endif
