/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_ACKNOWLEDGEMENTPROTOCOLENUM_H
#define DEVELOPER_STUDIO_DATATYPES_ACKNOWLEDGEMENTPROTOCOLENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace AcknowledgementProtocolEnum {
        /**
        * Implementation of the <code>AcknowledgementProtocolEnum8</code> data type from the FOM.
        * <br>Description from the FOM: <i>Required reliability service</i>
        */
        enum AcknowledgementProtocolEnum {
            /** <code>Acknowledged</code> (with ordinal 0) */
            ACKNOWLEDGED = 0,
            /** <code>Unacknowledged</code> (with ordinal 1) */
            UNACKNOWLEDGED = 1
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to AcknowledgementProtocolEnum: static_cast<DevStudio::AcknowledgementProtocolEnum::AcknowledgementProtocolEnum>(i)
        */
        LIBAPI bool isValid(const AcknowledgementProtocolEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::AcknowledgementProtocolEnum::AcknowledgementProtocolEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::AcknowledgementProtocolEnum::AcknowledgementProtocolEnum const &);
}


#endif
