/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_ENTITYIDENTIFIERSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_ENTITYIDENTIFIERSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/FederateIdentifierStruct.h>

namespace DevStudio {
   /**
   * Implementation of the <code>EntityIdentifierStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
   */
   class EntityIdentifierStruct {

   public:
      /**
      * Description from the FOM: <i>Simulation application (federate) identifier.</i>.
      * <br>Description of the data type from the FOM: <i>Unique identification of the simulation application (federate) in an exercise, or a symbolic group address referencing multiple simulation applications. Based on the Simulation Address record as specified in IEEE 1278.1-1995 section 5.2.14.1.</i>
      */
      FederateIdentifierStruct federateIdentifier;
      /**
      * Description from the FOM: <i>Each entity in a given simulation application shall be given an entity identifier number unique to all other entities in that application. This identifier number is valid for the duration of the exercise; however, entity identifier numbers may be reused when all possible numbers have been exhausted. No entity shall have an entity identifier number of NO_ENTITY (0), ALL_ENTITIES (0xFFFF), or RQST_ASSIGN_ID (0xFFFE). The entity identifier number need not be registered or retained for future exercises. An entity identifier number equal to zero with valid site and application identification shall address a simulation application. An entity identifier number equal to ALL_ENTITIES shall mean all entities within the specified site and application. An entity identifier number equal to RQST_ASSIGN_ID allows the receiver of the CreateEntity interaction to define the entity identifier number of the new entity. The new entity will specify its entity identifier number in the Acknowledge interaction.</i>.
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      unsigned short entityNumber;

      LIBAPI EntityIdentifierStruct()
         :
         federateIdentifier(FederateIdentifierStruct()),
         entityNumber(0)
      {}

      /**
      * Constructor for EntityIdentifierStruct
      *
      * @param federateIdentifier_ value to set as federateIdentifier.
      * <br>Description from the FOM: <i>Simulation application (federate) identifier.</i>
      * <br>Description of the data type from the FOM: <i>Unique identification of the simulation application (federate) in an exercise, or a symbolic group address referencing multiple simulation applications. Based on the Simulation Address record as specified in IEEE 1278.1-1995 section 5.2.14.1.</i>
      * @param entityNumber_ value to set as entityNumber.
      * <br>Description from the FOM: <i>Each entity in a given simulation application shall be given an entity identifier number unique to all other entities in that application. This identifier number is valid for the duration of the exercise; however, entity identifier numbers may be reused when all possible numbers have been exhausted. No entity shall have an entity identifier number of NO_ENTITY (0), ALL_ENTITIES (0xFFFF), or RQST_ASSIGN_ID (0xFFFE). The entity identifier number need not be registered or retained for future exercises. An entity identifier number equal to zero with valid site and application identification shall address a simulation application. An entity identifier number equal to ALL_ENTITIES shall mean all entities within the specified site and application. An entity identifier number equal to RQST_ASSIGN_ID allows the receiver of the CreateEntity interaction to define the entity identifier number of the new entity. The new entity will specify its entity identifier number in the Acknowledge interaction.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      LIBAPI EntityIdentifierStruct(
         FederateIdentifierStruct federateIdentifier_,
         unsigned short entityNumber_
         )
         :
         federateIdentifier(federateIdentifier_),
         entityNumber(entityNumber_)
      {}



      /**
      * Function to get federateIdentifier.
      * <br>Description from the FOM: <i>Simulation application (federate) identifier.</i>
      * <br>Description of the data type from the FOM: <i>Unique identification of the simulation application (federate) in an exercise, or a symbolic group address referencing multiple simulation applications. Based on the Simulation Address record as specified in IEEE 1278.1-1995 section 5.2.14.1.</i>
      *
      * @return federateIdentifier
      */
      LIBAPI DevStudio::FederateIdentifierStruct & getFederateIdentifier() {
         return federateIdentifier;
      }

      /**
      * Function to get entityNumber.
      * <br>Description from the FOM: <i>Each entity in a given simulation application shall be given an entity identifier number unique to all other entities in that application. This identifier number is valid for the duration of the exercise; however, entity identifier numbers may be reused when all possible numbers have been exhausted. No entity shall have an entity identifier number of NO_ENTITY (0), ALL_ENTITIES (0xFFFF), or RQST_ASSIGN_ID (0xFFFE). The entity identifier number need not be registered or retained for future exercises. An entity identifier number equal to zero with valid site and application identification shall address a simulation application. An entity identifier number equal to ALL_ENTITIES shall mean all entities within the specified site and application. An entity identifier number equal to RQST_ASSIGN_ID allows the receiver of the CreateEntity interaction to define the entity identifier number of the new entity. The new entity will specify its entity identifier number in the Acknowledge interaction.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return entityNumber
      */
      LIBAPI unsigned short & getEntityNumber() {
         return entityNumber;
      }

   };


   LIBAPI bool operator ==(const DevStudio::EntityIdentifierStruct& l, const DevStudio::EntityIdentifierStruct& r);
   LIBAPI bool operator !=(const DevStudio::EntityIdentifierStruct& l, const DevStudio::EntityIdentifierStruct& r);
   LIBAPI bool operator <(const DevStudio::EntityIdentifierStruct& l, const DevStudio::EntityIdentifierStruct& r);
   LIBAPI bool operator >(const DevStudio::EntityIdentifierStruct& l, const DevStudio::EntityIdentifierStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::EntityIdentifierStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::EntityIdentifierStruct const &);
}
#endif
