/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_MINEFIELDTYPEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_MINEFIELDTYPEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace MinefieldTypeEnum {
        /**
        * Implementation of the <code>MinefieldTypeEnum8</code> data type from the FOM.
        * <br>Description from the FOM: <i>Minefield type</i>
        */
        enum MinefieldTypeEnum {
            /** <code>MixedAntiPersonnelAntiTank</code> (with ordinal 0) */
            MIXED_ANTI_PERSONNEL_ANTI_TANK = 0,
            /** <code>PureAntiPersonnel</code> (with ordinal 1) */
            PURE_ANTI_PERSONNEL = 1,
            /** <code>PureAntiTank</code> (with ordinal 2) */
            PURE_ANTI_TANK = 2
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to MinefieldTypeEnum: static_cast<DevStudio::MinefieldTypeEnum::MinefieldTypeEnum>(i)
        */
        LIBAPI bool isValid(const MinefieldTypeEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::MinefieldTypeEnum::MinefieldTypeEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::MinefieldTypeEnum::MinefieldTypeEnum const &);
}


#endif
