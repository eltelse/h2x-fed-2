/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_AGGREGATESTATEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_AGGREGATESTATEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace AggregateStateEnum {
        /**
        * Implementation of the <code>AggregateStateEnum8</code> data type from the FOM.
        * <br>Description from the FOM: <i>Aggregate state</i>
        */
        enum AggregateStateEnum {
            /** <code>Other</code> (with ordinal 0) */
            OTHER = 0,
            /** <code>Aggregated</code> (with ordinal 1) */
            AGGREGATED = 1,
            /** <code>Disaggregated</code> (with ordinal 2) */
            DISAGGREGATED = 2,
            /** <code>FullyDisaggregated</code> (with ordinal 3) */
            FULLY_DISAGGREGATED = 3,
            /** <code>PseudoDisaggregated</code> (with ordinal 4) */
            PSEUDO_DISAGGREGATED = 4,
            /** <code>PartiallyDisaggregated</code> (with ordinal 5) */
            PARTIALLY_DISAGGREGATED = 5
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to AggregateStateEnum: static_cast<DevStudio::AggregateStateEnum::AggregateStateEnum>(i)
        */
        LIBAPI bool isValid(const AggregateStateEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::AggregateStateEnum::AggregateStateEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::AggregateStateEnum::AggregateStateEnum const &);
}


#endif
