/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_CRYPTOGRAPHICSYSTEMTYPEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_CRYPTOGRAPHICSYSTEMTYPEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace CryptographicSystemTypeEnum {
        /**
        * Implementation of the <code>CryptographicSystemTypeEnum16</code> data type from the FOM.
        * <br>Description from the FOM: <i>Identifies the type of cryptographic equipment</i>
        */
        enum CryptographicSystemTypeEnum {
            /** <code>Other</code> (with ordinal 0) */
            OTHER = 0,
            /** <code>KY_28</code> (with ordinal 1) */
            KY_28 = 1,
            /** <code>KY_58</code> (with ordinal 2) */
            KY_58 = 2,
            /** <code>NarrowSpectrumSecureVoice_NSVE</code> (with ordinal 3) */
            NARROW_SPECTRUM_SECURE_VOICE_NSVE = 3,
            /** <code>WideSpectrumSecureVoice_WSVE</code> (with ordinal 4) */
            WIDE_SPECTRUM_SECURE_VOICE_WSVE = 4,
            /** <code>SINCGARS_ICOM</code> (with ordinal 5) */
            SINCGARS_ICOM = 5,
            /** <code>KY-75</code> (with ordinal 6) */
            KY_75 = 6,
            /** <code>KY-100</code> (with ordinal 7) */
            KY_100 = 7,
            /** <code>KY-57</code> (with ordinal 8) */
            KY_57 = 8,
            /** <code>KYV-5</code> (with ordinal 9) */
            KYV_5 = 9,
            /** <code>Link11KG-40A-P_NTDS_</code> (with ordinal 10) */
            LINK11KG_40A_P_NTDS = 10,
            /** <code>Link11BKG-40A-S</code> (with ordinal 11) */
            LINK11BKG_40A_S = 11,
            /** <code>Link11KG-40AR</code> (with ordinal 12) */
            LINK11KG_40AR = 12
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to CryptographicSystemTypeEnum: static_cast<DevStudio::CryptographicSystemTypeEnum::CryptographicSystemTypeEnum>(i)
        */
        LIBAPI bool isValid(const CryptographicSystemTypeEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::CryptographicSystemTypeEnum::CryptographicSystemTypeEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::CryptographicSystemTypeEnum::CryptographicSystemTypeEnum const &);
}


#endif
