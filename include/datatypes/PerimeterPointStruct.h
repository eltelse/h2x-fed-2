/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_PERIMETERPOINTSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_PERIMETERPOINTSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>


namespace DevStudio {
   /**
   * Implementation of the <code>PerimeterPointStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Record specifying the location of a perimeter point</i>
   */
   class PerimeterPointStruct {

   public:
      /**
      * Description from the FOM: <i>Specifies the X coordinate</i>.
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      */
      float x;
      /**
      * Description from the FOM: <i>Specifies the Y coordinate</i>.
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      */
      float y;

      LIBAPI PerimeterPointStruct()
         :
         x(0),
         y(0)
      {}

      /**
      * Constructor for PerimeterPointStruct
      *
      * @param x_ value to set as x.
      * <br>Description from the FOM: <i>Specifies the X coordinate</i>
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      * @param y_ value to set as y.
      * <br>Description from the FOM: <i>Specifies the Y coordinate</i>
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      */
      LIBAPI PerimeterPointStruct(
         float x_,
         float y_
         )
         :
         x(x_),
         y(y_)
      {}



      /**
      * Function to get x.
      * <br>Description from the FOM: <i>Specifies the X coordinate</i>
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      *
      * @return x
      */
      LIBAPI float & getX() {
         return x;
      }

      /**
      * Function to get y.
      * <br>Description from the FOM: <i>Specifies the Y coordinate</i>
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      *
      * @return y
      */
      LIBAPI float & getY() {
         return y;
      }

   };


   LIBAPI bool operator ==(const DevStudio::PerimeterPointStruct& l, const DevStudio::PerimeterPointStruct& r);
   LIBAPI bool operator !=(const DevStudio::PerimeterPointStruct& l, const DevStudio::PerimeterPointStruct& r);
   LIBAPI bool operator <(const DevStudio::PerimeterPointStruct& l, const DevStudio::PerimeterPointStruct& r);
   LIBAPI bool operator >(const DevStudio::PerimeterPointStruct& l, const DevStudio::PerimeterPointStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::PerimeterPointStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::PerimeterPointStruct const &);
}
#endif
