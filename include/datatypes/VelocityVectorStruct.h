/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_VELOCITYVECTORSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_VELOCITYVECTORSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <RtiDriver/RprUtility/RprUtility.h>


namespace DevStudio {
   /**
   * Implementation of the <code>VelocityVectorStruct</code> data type from the FOM.
   * This datatype can be used with the RprUtility package. Please see the Overview document
   * located in the project root directory for more information.
   * <br>Description from the FOM: <i>The rate at which the position is changing over time.</i>
   */
   class VelocityVectorStruct {

   public:
      /**
      * Description from the FOM: <i>Velocity component along the X axis.</i>.
      * <br>Description of the data type from the FOM: <i>Speed/Velocity in meter per second. [unit: meter per second (m/s), resolution: NA, accuracy: perfect]</i>
      */
      float xVelocity;
      /**
      * Description from the FOM: <i>Velocity component along the Y axis.</i>.
      * <br>Description of the data type from the FOM: <i>Speed/Velocity in meter per second. [unit: meter per second (m/s), resolution: NA, accuracy: perfect]</i>
      */
      float yVelocity;
      /**
      * Description from the FOM: <i>Velocity component along the Z axis.</i>.
      * <br>Description of the data type from the FOM: <i>Speed/Velocity in meter per second. [unit: meter per second (m/s), resolution: NA, accuracy: perfect]</i>
      */
      float zVelocity;

      LIBAPI VelocityVectorStruct()
         :
         xVelocity(0),
         yVelocity(0),
         zVelocity(0)
      {}

      /**
      * Constructor for VelocityVectorStruct
      *
      * @param xVelocity_ value to set as xVelocity.
      * <br>Description from the FOM: <i>Velocity component along the X axis.</i>
      * <br>Description of the data type from the FOM: <i>Speed/Velocity in meter per second. [unit: meter per second (m/s), resolution: NA, accuracy: perfect]</i>
      * @param yVelocity_ value to set as yVelocity.
      * <br>Description from the FOM: <i>Velocity component along the Y axis.</i>
      * <br>Description of the data type from the FOM: <i>Speed/Velocity in meter per second. [unit: meter per second (m/s), resolution: NA, accuracy: perfect]</i>
      * @param zVelocity_ value to set as zVelocity.
      * <br>Description from the FOM: <i>Velocity component along the Z axis.</i>
      * <br>Description of the data type from the FOM: <i>Speed/Velocity in meter per second. [unit: meter per second (m/s), resolution: NA, accuracy: perfect]</i>
      */
      LIBAPI VelocityVectorStruct(
         float xVelocity_,
         float yVelocity_,
         float zVelocity_
         )
         :
         xVelocity(xVelocity_),
         yVelocity(yVelocity_),
         zVelocity(zVelocity_)
      {}

      /**
      * Convert to RprUtility datatype
      *
      * @param velocity VelocityVectorStruct to convert
      *
      * @return velocity converted to RprUtility::VelocityVector
      */
      LIBAPI static RprUtility::VelocityVector convert(const VelocityVectorStruct velocity);

      /**
      * Convert from RprUtility datatype
      *
      * @param velocity RprUtility::VelocityVector to convert to VelocityVectorStruct
      *
      * @return velocity converted to VelocityVectorStruct
      */
      LIBAPI static VelocityVectorStruct convert(const RprUtility::VelocityVector velocity);


      /**
      * Function to get xVelocity.
      * <br>Description from the FOM: <i>Velocity component along the X axis.</i>
      * <br>Description of the data type from the FOM: <i>Speed/Velocity in meter per second. [unit: meter per second (m/s), resolution: NA, accuracy: perfect]</i>
      *
      * @return xVelocity
      */
      LIBAPI float & getXVelocity() {
         return xVelocity;
      }

      /**
      * Function to get yVelocity.
      * <br>Description from the FOM: <i>Velocity component along the Y axis.</i>
      * <br>Description of the data type from the FOM: <i>Speed/Velocity in meter per second. [unit: meter per second (m/s), resolution: NA, accuracy: perfect]</i>
      *
      * @return yVelocity
      */
      LIBAPI float & getYVelocity() {
         return yVelocity;
      }

      /**
      * Function to get zVelocity.
      * <br>Description from the FOM: <i>Velocity component along the Z axis.</i>
      * <br>Description of the data type from the FOM: <i>Speed/Velocity in meter per second. [unit: meter per second (m/s), resolution: NA, accuracy: perfect]</i>
      *
      * @return zVelocity
      */
      LIBAPI float & getZVelocity() {
         return zVelocity;
      }

   };


   LIBAPI bool operator ==(const DevStudio::VelocityVectorStruct& l, const DevStudio::VelocityVectorStruct& r);
   LIBAPI bool operator !=(const DevStudio::VelocityVectorStruct& l, const DevStudio::VelocityVectorStruct& r);
   LIBAPI bool operator <(const DevStudio::VelocityVectorStruct& l, const DevStudio::VelocityVectorStruct& r);
   LIBAPI bool operator >(const DevStudio::VelocityVectorStruct& l, const DevStudio::VelocityVectorStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::VelocityVectorStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::VelocityVectorStruct const &);
}
#endif
