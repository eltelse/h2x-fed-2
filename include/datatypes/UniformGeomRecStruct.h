/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_UNIFORMGEOMRECSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_UNIFORMGEOMRECSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/OctetArray8.h>
#include <vector>

namespace DevStudio {
   /**
   * Implementation of the <code>UniformGeomRecStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Record specifying Uniform geometry record</i>
   */
   class UniformGeomRecStruct {

   public:
      /**
      * Description from the FOM: <i>Padding field</i>.
      * <br>Description of the data type from the FOM: <i>Generic array of eight Octet elements.</i>
      */
      std::vector</* 8 */ char > padding;

      LIBAPI UniformGeomRecStruct()
         :
         padding(0)
      {}

      /**
      * Constructor for UniformGeomRecStruct
      *
      * @param padding_ value to set as padding.
      * <br>Description from the FOM: <i>Padding field</i>
      * <br>Description of the data type from the FOM: <i>Generic array of eight Octet elements.</i>
      */
      LIBAPI UniformGeomRecStruct(
         std::vector</* 8 */ char > padding_
         )
         :
         padding(padding_)
      {}



      /**
      * Function to get padding.
      * <br>Description from the FOM: <i>Padding field</i>
      * <br>Description of the data type from the FOM: <i>Generic array of eight Octet elements.</i>
      *
      * @return padding
      */
      LIBAPI std::vector</* 8 */ char > & getPadding() {
         return padding;
      }

   };


   LIBAPI bool operator ==(const DevStudio::UniformGeomRecStruct& l, const DevStudio::UniformGeomRecStruct& r);
   LIBAPI bool operator !=(const DevStudio::UniformGeomRecStruct& l, const DevStudio::UniformGeomRecStruct& r);
   LIBAPI bool operator <(const DevStudio::UniformGeomRecStruct& l, const DevStudio::UniformGeomRecStruct& r);
   LIBAPI bool operator >(const DevStudio::UniformGeomRecStruct& l, const DevStudio::UniformGeomRecStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::UniformGeomRecStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::UniformGeomRecStruct const &);
}
#endif
