/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_STOPFREEZEREASONENUM_H
#define DEVELOPER_STUDIO_DATATYPES_STOPFREEZEREASONENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace StopFreezeReasonEnum {
        /**
        * Implementation of the <code>StopFreezeReasonEnum8</code> data type from the FOM.
        * <br>Description from the FOM: <i>Reason to stop</i>
        */
        enum StopFreezeReasonEnum {
            /** <code>Other</code> (with ordinal 0) */
            OTHER = 0,
            /** <code>Recess</code> (with ordinal 1) */
            RECESS = 1,
            /** <code>Termination</code> (with ordinal 2) */
            TERMINATION = 2,
            /** <code>SystemFailure</code> (with ordinal 3) */
            SYSTEM_FAILURE = 3,
            /** <code>SecurityViolation</code> (with ordinal 4) */
            SECURITY_VIOLATION = 4,
            /** <code>EntityReconstitution</code> (with ordinal 5) */
            ENTITY_RECONSTITUTION = 5,
            /** <code>StopForReset</code> (with ordinal 6) */
            STOP_FOR_RESET = 6,
            /** <code>StopForRestart</code> (with ordinal 7) */
            STOP_FOR_RESTART = 7,
            /** <code>AbortTrainingResumeTacOps</code> (with ordinal 8) */
            ABORT_TRAINING_RESUME_TAC_OPS = 8
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to StopFreezeReasonEnum: static_cast<DevStudio::StopFreezeReasonEnum::StopFreezeReasonEnum>(i)
        */
        LIBAPI bool isValid(const StopFreezeReasonEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::StopFreezeReasonEnum::StopFreezeReasonEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::StopFreezeReasonEnum::StopFreezeReasonEnum const &);
}


#endif
