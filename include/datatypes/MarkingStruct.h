/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_MARKINGSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_MARKINGSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/MarkingArray11.h>
#include <DevStudio/datatypes/MarkingEncodingEnum.h>
#include <vector>

namespace DevStudio {
   /**
   * Implementation of the <code>MarkingStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Character set used in the marking and the string of characters to be interpreted for display.</i>
   */
   class MarkingStruct {

   public:
      /**
      * Description from the FOM: <i>Character set representation.</i>.
      * <br>Description of the data type from the FOM: <i>Marking character set</i>
      */
      MarkingEncodingEnum::MarkingEncodingEnum markingEncodingType;
      /**
      * Description from the FOM: <i>11 element character string</i>.
      * <br>Description of the data type from the FOM: <i>String of characters represented by an 11 element character string.</i>
      */
      std::vector</* 11 */ char > markingData;

      LIBAPI MarkingStruct()
         :
         markingEncodingType(MarkingEncodingEnum::MarkingEncodingEnum()),
         markingData(0)
      {}

      /**
      * Constructor for MarkingStruct
      *
      * @param markingEncodingType_ value to set as markingEncodingType.
      * <br>Description from the FOM: <i>Character set representation.</i>
      * <br>Description of the data type from the FOM: <i>Marking character set</i>
      * @param markingData_ value to set as markingData.
      * <br>Description from the FOM: <i>11 element character string</i>
      * <br>Description of the data type from the FOM: <i>String of characters represented by an 11 element character string.</i>
      */
      LIBAPI MarkingStruct(
         MarkingEncodingEnum::MarkingEncodingEnum markingEncodingType_,
         std::vector</* 11 */ char > markingData_
         )
         :
         markingEncodingType(markingEncodingType_),
         markingData(markingData_)
      {}



      /**
      * Function to get markingEncodingType.
      * <br>Description from the FOM: <i>Character set representation.</i>
      * <br>Description of the data type from the FOM: <i>Marking character set</i>
      *
      * @return markingEncodingType
      */
      LIBAPI DevStudio::MarkingEncodingEnum::MarkingEncodingEnum & getMarkingEncodingType() {
         return markingEncodingType;
      }

      /**
      * Function to get markingData.
      * <br>Description from the FOM: <i>11 element character string</i>
      * <br>Description of the data type from the FOM: <i>String of characters represented by an 11 element character string.</i>
      *
      * @return markingData
      */
      LIBAPI std::vector</* 11 */ char > & getMarkingData() {
         return markingData;
      }

   };


   LIBAPI bool operator ==(const DevStudio::MarkingStruct& l, const DevStudio::MarkingStruct& r);
   LIBAPI bool operator !=(const DevStudio::MarkingStruct& l, const DevStudio::MarkingStruct& r);
   LIBAPI bool operator <(const DevStudio::MarkingStruct& l, const DevStudio::MarkingStruct& r);
   LIBAPI bool operator >(const DevStudio::MarkingStruct& l, const DevStudio::MarkingStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::MarkingStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::MarkingStruct const &);
}
#endif
