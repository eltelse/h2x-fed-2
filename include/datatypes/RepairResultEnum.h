/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_REPAIRRESULTENUM_H
#define DEVELOPER_STUDIO_DATATYPES_REPAIRRESULTENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace RepairResultEnum {
        /**
        * Implementation of the <code>RepairResultEnum8</code> data type from the FOM.
        * <br>Description from the FOM: <i>Result of repair</i>
        */
        enum RepairResultEnum {
            /** <code>Other</code> (with ordinal 0) */
            OTHER = 0,
            /** <code>RepairEnded</code> (with ordinal 1) */
            REPAIR_ENDED = 1,
            /** <code>InvalidRepair</code> (with ordinal 2) */
            INVALID_REPAIR = 2,
            /** <code>RepairInterrupted</code> (with ordinal 3) */
            REPAIR_INTERRUPTED = 3,
            /** <code>ServiceCanceledByTheSupplier</code> (with ordinal 4) */
            SERVICE_CANCELED_BY_THE_SUPPLIER = 4
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to RepairResultEnum: static_cast<DevStudio::RepairResultEnum::RepairResultEnum>(i)
        */
        LIBAPI bool isValid(const RepairResultEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::RepairResultEnum::RepairResultEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::RepairResultEnum::RepairResultEnum const &);
}


#endif
