/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_CONE2GEOMRECSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_CONE2GEOMRECSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/AngularVelocityVectorStruct.h>
#include <DevStudio/datatypes/OctetArray4.h>
#include <DevStudio/datatypes/OrientationStruct.h>
#include <DevStudio/datatypes/VelocityVectorStruct.h>
#include <DevStudio/datatypes/WorldLocationStruct.h>
#include <vector>

namespace DevStudio {
   /**
   * Implementation of the <code>Cone2GeomRecStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Record specifying Cone 2 geometry record</i>
   */
   class Cone2GeomRecStruct {

   public:
      /**
      * Description from the FOM: <i>Vertex location X, Y, Z</i>.
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      */
      WorldLocationStruct vertexLocation;
      /**
      * Description from the FOM: <i>Orientation, specified by Euler angles</i>.
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      */
      OrientationStruct orientation;
      /**
      * Description from the FOM: <i>Velocity Vx, Vy, Vz</i>.
      * <br>Description of the data type from the FOM: <i>The rate at which the position is changing over time.</i>
      */
      VelocityVectorStruct velocity;
      /**
      * Description from the FOM: <i>Angular velocity Vx, Vy, Vz</i>.
      * <br>Description of the data type from the FOM: <i>The rate at which the orientation is changing over time, in body coordinates.</i>
      */
      AngularVelocityVectorStruct angularVelocity;
      /**
      * Description from the FOM: <i>Height</i>.
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      */
      float height;
      /**
      * Description from the FOM: <i>Variation of height</i>.
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      */
      float heightRate;
      /**
      * Description from the FOM: <i>Peak angle</i>.
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      */
      float peakAngle;
      /**
      * Description from the FOM: <i>Variation of peak angle</i>.
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      */
      float peakAngleRate;
      /**
      * Description from the FOM: <i>Padding field</i>.
      * <br>Description of the data type from the FOM: <i>Generic array of four Octet elements.</i>
      */
      std::vector</* 4 */ char > padding;

      LIBAPI Cone2GeomRecStruct()
         :
         vertexLocation(WorldLocationStruct()),
         orientation(OrientationStruct()),
         velocity(VelocityVectorStruct()),
         angularVelocity(AngularVelocityVectorStruct()),
         height(0),
         heightRate(0),
         peakAngle(0),
         peakAngleRate(0),
         padding(0)
      {}

      /**
      * Constructor for Cone2GeomRecStruct
      *
      * @param vertexLocation_ value to set as vertexLocation.
      * <br>Description from the FOM: <i>Vertex location X, Y, Z</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      * @param orientation_ value to set as orientation.
      * <br>Description from the FOM: <i>Orientation, specified by Euler angles</i>
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      * @param velocity_ value to set as velocity.
      * <br>Description from the FOM: <i>Velocity Vx, Vy, Vz</i>
      * <br>Description of the data type from the FOM: <i>The rate at which the position is changing over time.</i>
      * @param angularVelocity_ value to set as angularVelocity.
      * <br>Description from the FOM: <i>Angular velocity Vx, Vy, Vz</i>
      * <br>Description of the data type from the FOM: <i>The rate at which the orientation is changing over time, in body coordinates.</i>
      * @param height_ value to set as height.
      * <br>Description from the FOM: <i>Height</i>
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      * @param heightRate_ value to set as heightRate.
      * <br>Description from the FOM: <i>Variation of height</i>
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      * @param peakAngle_ value to set as peakAngle.
      * <br>Description from the FOM: <i>Peak angle</i>
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      * @param peakAngleRate_ value to set as peakAngleRate.
      * <br>Description from the FOM: <i>Variation of peak angle</i>
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      * @param padding_ value to set as padding.
      * <br>Description from the FOM: <i>Padding field</i>
      * <br>Description of the data type from the FOM: <i>Generic array of four Octet elements.</i>
      */
      LIBAPI Cone2GeomRecStruct(
         WorldLocationStruct vertexLocation_,
         OrientationStruct orientation_,
         VelocityVectorStruct velocity_,
         AngularVelocityVectorStruct angularVelocity_,
         float height_,
         float heightRate_,
         float peakAngle_,
         float peakAngleRate_,
         std::vector</* 4 */ char > padding_
         )
         :
         vertexLocation(vertexLocation_),
         orientation(orientation_),
         velocity(velocity_),
         angularVelocity(angularVelocity_),
         height(height_),
         heightRate(heightRate_),
         peakAngle(peakAngle_),
         peakAngleRate(peakAngleRate_),
         padding(padding_)
      {}



      /**
      * Function to get vertexLocation.
      * <br>Description from the FOM: <i>Vertex location X, Y, Z</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @return vertexLocation
      */
      LIBAPI DevStudio::WorldLocationStruct & getVertexLocation() {
         return vertexLocation;
      }

      /**
      * Function to get orientation.
      * <br>Description from the FOM: <i>Orientation, specified by Euler angles</i>
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @return orientation
      */
      LIBAPI DevStudio::OrientationStruct & getOrientation() {
         return orientation;
      }

      /**
      * Function to get velocity.
      * <br>Description from the FOM: <i>Velocity Vx, Vy, Vz</i>
      * <br>Description of the data type from the FOM: <i>The rate at which the position is changing over time.</i>
      *
      * @return velocity
      */
      LIBAPI DevStudio::VelocityVectorStruct & getVelocity() {
         return velocity;
      }

      /**
      * Function to get angularVelocity.
      * <br>Description from the FOM: <i>Angular velocity Vx, Vy, Vz</i>
      * <br>Description of the data type from the FOM: <i>The rate at which the orientation is changing over time, in body coordinates.</i>
      *
      * @return angularVelocity
      */
      LIBAPI DevStudio::AngularVelocityVectorStruct & getAngularVelocity() {
         return angularVelocity;
      }

      /**
      * Function to get height.
      * <br>Description from the FOM: <i>Height</i>
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      *
      * @return height
      */
      LIBAPI float & getHeight() {
         return height;
      }

      /**
      * Function to get heightRate.
      * <br>Description from the FOM: <i>Variation of height</i>
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      *
      * @return heightRate
      */
      LIBAPI float & getHeightRate() {
         return heightRate;
      }

      /**
      * Function to get peakAngle.
      * <br>Description from the FOM: <i>Peak angle</i>
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      *
      * @return peakAngle
      */
      LIBAPI float & getPeakAngle() {
         return peakAngle;
      }

      /**
      * Function to get peakAngleRate.
      * <br>Description from the FOM: <i>Variation of peak angle</i>
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      *
      * @return peakAngleRate
      */
      LIBAPI float & getPeakAngleRate() {
         return peakAngleRate;
      }

      /**
      * Function to get padding.
      * <br>Description from the FOM: <i>Padding field</i>
      * <br>Description of the data type from the FOM: <i>Generic array of four Octet elements.</i>
      *
      * @return padding
      */
      LIBAPI std::vector</* 4 */ char > & getPadding() {
         return padding;
      }

   };


   LIBAPI bool operator ==(const DevStudio::Cone2GeomRecStruct& l, const DevStudio::Cone2GeomRecStruct& r);
   LIBAPI bool operator !=(const DevStudio::Cone2GeomRecStruct& l, const DevStudio::Cone2GeomRecStruct& r);
   LIBAPI bool operator <(const DevStudio::Cone2GeomRecStruct& l, const DevStudio::Cone2GeomRecStruct& r);
   LIBAPI bool operator >(const DevStudio::Cone2GeomRecStruct& l, const DevStudio::Cone2GeomRecStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::Cone2GeomRecStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::Cone2GeomRecStruct const &);
}
#endif
