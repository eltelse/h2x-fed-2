/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_USERPROTOCOLENUM_H
#define DEVELOPER_STUDIO_DATATYPES_USERPROTOCOLENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace UserProtocolEnum {
        /**
        * Implementation of the <code>UserProtocolEnum32</code> data type from the FOM.
        * <br>Description from the FOM: <i>The ID of the user protocol used to transmit a radio signal.</i>
        */
        enum UserProtocolEnum {
            /** <code>CCSIL</code> (with ordinal 1) */
            CCSIL = 1,
            /** <code>A2ATD_SINCGARS_ERF</code> (with ordinal 5) */
            A2ATD_SINCGARS_ERF = 5,
            /** <code>A2ATD_CAC2</code> (with ordinal 6) */
            A2ATD_CAC2 = 6,
            /** <code>Battle_Command</code> (with ordinal 20) */
            BATTLE_COMMAND = 20,
            /** <code>AFIWCIADSTrackReport</code> (with ordinal 30) */
            AFIWCIADSTRACK_REPORT = 30,
            /** <code>AFIWCIADSCommC2Message</code> (with ordinal 31) */
            AFIWCIADSCOMM_C2MESSAGE = 31,
            /** <code>AFIWCIADSGroundControlInterceptor_GCI_Command</code> (with ordinal 32) */
            AFIWCIADSGROUND_CONTROL_INTERCEPTOR_GCI_COMMAND = 32,
            /** <code>AFIWCVoiceTextMessage</code> (with ordinal 35) */
            AFIWCVOICE_TEXT_MESSAGE = 35,
            /** <code>ModSAF_Text_Radio</code> (with ordinal 177) */
            MOD_SAF_TEXT_RADIO = 177,
            /** <code>CCTT_SINCGARS_ERF-LOCKOUT</code> (with ordinal 200) */
            CCTT_SINCGARS_ERF_LOCKOUT = 200,
            /** <code>CCTT_SINCGARS_ERF-HOPSET</code> (with ordinal 201) */
            CCTT_SINCGARS_ERF_HOPSET = 201,
            /** <code>CCTT_SINCGARS_OTAR</code> (with ordinal 202) */
            CCTT_SINCGARS_OTAR = 202,
            /** <code>CCTT_SINCGARS_DATA</code> (with ordinal 203) */
            CCTT_SINCGARS_DATA = 203,
            /** <code>ModSAF_FWA_Forward_Air_Controller</code> (with ordinal 546) */
            MOD_SAF_FWA_FORWARD_AIR_CONTROLLER = 546,
            /** <code>ModSAF_Threat_ADA_C3</code> (with ordinal 832) */
            MOD_SAF_THREAT_ADA_C3 = 832,
            /** <code>F-16_MTC_AFAPD</code> (with ordinal 1000) */
            F_16_MTC_AFAPD = 1000,
            /** <code>F-16_MTC_IDL</code> (with ordinal 1100) */
            F_16_MTC_IDL = 1100,
            /** <code>AutomaticIdentificationSystem_AIS_</code> (with ordinal 1371) */
            AUTOMATIC_IDENTIFICATION_SYSTEM_AIS = 1371,
            /** <code>ModSAF_Artillery_Fire_Control</code> (with ordinal 4570) */
            MOD_SAF_ARTILLERY_FIRE_CONTROL = 4570,
            /** <code>AGTS</code> (with ordinal 5361) */
            AGTS = 5361,
            /** <code>GC3</code> (with ordinal 6000) */
            GC3 = 6000,
            /** <code>WNCP_data</code> (with ordinal 6010) */
            WNCP_DATA = 6010,
            /** <code>Spoken_text_message</code> (with ordinal 6020) */
            SPOKEN_TEXT_MESSAGE = 6020,
            /** <code>Longbow_IDM_message</code> (with ordinal 6661) */
            LONGBOW_IDM_MESSAGE = 6661,
            /** <code>Comanche_IDM_message</code> (with ordinal 6662) */
            COMANCHE_IDM_MESSAGE = 6662,
            /** <code>Longbow_Airborne_TACFIRE_Message</code> (with ordinal 6663) */
            LONGBOW_AIRBORNE_TACFIRE_MESSAGE = 6663,
            /** <code>Longbow_Ground_TACFIRE_Message</code> (with ordinal 6664) */
            LONGBOW_GROUND_TACFIRE_MESSAGE = 6664,
            /** <code>LongbowAFAPDMessage</code> (with ordinal 6665) */
            LONGBOW_AFAPDMESSAGE = 6665,
            /** <code>Longbow_ERF_message</code> (with ordinal 6666) */
            LONGBOW_ERF_MESSAGE = 6666
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to UserProtocolEnum: static_cast<DevStudio::UserProtocolEnum::UserProtocolEnum>(i)
        */
        LIBAPI bool isValid(const UserProtocolEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::UserProtocolEnum::UserProtocolEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::UserProtocolEnum::UserProtocolEnum const &);
}


#endif
