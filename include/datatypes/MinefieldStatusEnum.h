/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_MINEFIELDSTATUSENUM_H
#define DEVELOPER_STUDIO_DATATYPES_MINEFIELDSTATUSENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace MinefieldStatusEnum {
        /**
        * Implementation of the <code>MinefieldStatusEnum8</code> data type from the FOM.
        * <br>Description from the FOM: <i>Minefield status</i>
        */
        enum MinefieldStatusEnum {
            /** <code>Active</code> (with ordinal 0) */
            ACTIVE = 0,
            /** <code>Inactive</code> (with ordinal 1) */
            INACTIVE = 1
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to MinefieldStatusEnum: static_cast<DevStudio::MinefieldStatusEnum::MinefieldStatusEnum>(i)
        */
        LIBAPI bool isValid(const MinefieldStatusEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::MinefieldStatusEnum::MinefieldStatusEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::MinefieldStatusEnum::MinefieldStatusEnum const &);
}


#endif
