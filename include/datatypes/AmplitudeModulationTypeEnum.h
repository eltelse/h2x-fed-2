/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_AMPLITUDEMODULATIONTYPEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_AMPLITUDEMODULATIONTYPEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace AmplitudeModulationTypeEnum {
        /**
        * Implementation of the <code>AmplitudeModulationTypeEnum16</code> data type from the FOM.
        * <br>Description from the FOM: <i>Detailed modulation types for Amplitude Modulation</i>
        */
        enum AmplitudeModulationTypeEnum {
            /** <code>Other</code> (with ordinal 0) */
            OTHER = 0,
            /** <code>AudioFrequencyShiftKeying</code> (with ordinal 1) */
            AUDIO_FREQUENCY_SHIFT_KEYING = 1,
            /** <code>AmplitudeModulation</code> (with ordinal 2) */
            AMPLITUDE_MODULATION = 2,
            /** <code>ContinuousWaveModulation</code> (with ordinal 3) */
            CONTINUOUS_WAVE_MODULATION = 3,
            /** <code>DoubleSideband</code> (with ordinal 4) */
            DOUBLE_SIDEBAND = 4,
            /** <code>IndependentSideband</code> (with ordinal 5) */
            INDEPENDENT_SIDEBAND = 5,
            /** <code>SSB_LowerSideband</code> (with ordinal 6) */
            SSB_LOWER_SIDEBAND = 6,
            /** <code>SSB_FullCarrier</code> (with ordinal 7) */
            SSB_FULL_CARRIER = 7,
            /** <code>SSB_ReducedCarrier</code> (with ordinal 8) */
            SSB_REDUCED_CARRIER = 8,
            /** <code>SSB_UpperSideband</code> (with ordinal 9) */
            SSB_UPPER_SIDEBAND = 9,
            /** <code>VestigialSideband</code> (with ordinal 10) */
            VESTIGIAL_SIDEBAND = 10
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to AmplitudeModulationTypeEnum: static_cast<DevStudio::AmplitudeModulationTypeEnum::AmplitudeModulationTypeEnum>(i)
        */
        LIBAPI bool isValid(const AmplitudeModulationTypeEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::AmplitudeModulationTypeEnum::AmplitudeModulationTypeEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::AmplitudeModulationTypeEnum::AmplitudeModulationTypeEnum const &);
}


#endif
