/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_BREACHABLESEGMENTSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_BREACHABLESEGMENTSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/BreachedStatusArray8.h>
#include <DevStudio/datatypes/BreachedStatusEnum.h>
#include <DevStudio/datatypes/LinearSegmentStruct.h>
#include <DevStudio/datatypes/OctetArray7.h>
#include <vector>

namespace DevStudio {
   /**
   * Implementation of the <code>BreachableSegmentStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Record specifying the characteristics of a breachable linear object segment</i>
   */
   class BreachableSegmentStruct {

   public:
      /**
      * Description from the FOM: <i>Specifies the breachable linear object segment characteristics</i>.
      * <br>Description of the data type from the FOM: <i>Specifies linear object segment characteristics.</i>
      */
      LinearSegmentStruct segmentParameters;
      /**
      * Description from the FOM: <i>Specifies the breachable linear object segment as 8 portions of length = BreachLength</i>.
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      unsigned int breachLength;
      /**
      * Description from the FOM: <i>Specifies the breached appearance of the breachable linear object segment</i>.
      * <br>Description of the data type from the FOM: <i>Breached appearance</i>
      */
      BreachedStatusEnum::BreachedStatusEnum breachedState;
      /**
      * Description from the FOM: <i>Specifies whether the segment portion beginning at the segment origin + (i*BreachLength) and extending BreachLength meters is breached or not</i>.
      * <br>Description of the data type from the FOM: <i>Specifies the breached appearance for each individual segment portion of length = BreachLength</i>
      */
      std::vector</* 8 */ DevStudio::BreachedStatusEnum::BreachedStatusEnum > segmentBreached;
      /**
      * Description from the FOM: <i>Padding to 64 bits</i>.
      * <br>Description of the data type from the FOM: <i>Generic array of seven Octet elements.</i>
      */
      std::vector</* 7 */ char > padding;

      LIBAPI BreachableSegmentStruct()
         :
         segmentParameters(LinearSegmentStruct()),
         breachLength(0),
         breachedState(BreachedStatusEnum::BreachedStatusEnum()),
         segmentBreached(std::vector</* 8 */ DevStudio::BreachedStatusEnum::BreachedStatusEnum >()),
         padding(0)
      {}

      /**
      * Constructor for BreachableSegmentStruct
      *
      * @param segmentParameters_ value to set as segmentParameters.
      * <br>Description from the FOM: <i>Specifies the breachable linear object segment characteristics</i>
      * <br>Description of the data type from the FOM: <i>Specifies linear object segment characteristics.</i>
      * @param breachLength_ value to set as breachLength.
      * <br>Description from the FOM: <i>Specifies the breachable linear object segment as 8 portions of length = BreachLength</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param breachedState_ value to set as breachedState.
      * <br>Description from the FOM: <i>Specifies the breached appearance of the breachable linear object segment</i>
      * <br>Description of the data type from the FOM: <i>Breached appearance</i>
      * @param segmentBreached_ value to set as segmentBreached.
      * <br>Description from the FOM: <i>Specifies whether the segment portion beginning at the segment origin + (i*BreachLength) and extending BreachLength meters is breached or not</i>
      * <br>Description of the data type from the FOM: <i>Specifies the breached appearance for each individual segment portion of length = BreachLength</i>
      * @param padding_ value to set as padding.
      * <br>Description from the FOM: <i>Padding to 64 bits</i>
      * <br>Description of the data type from the FOM: <i>Generic array of seven Octet elements.</i>
      */
      LIBAPI BreachableSegmentStruct(
         LinearSegmentStruct segmentParameters_,
         unsigned int breachLength_,
         BreachedStatusEnum::BreachedStatusEnum breachedState_,
         std::vector</* 8 */ DevStudio::BreachedStatusEnum::BreachedStatusEnum > segmentBreached_,
         std::vector</* 7 */ char > padding_
         )
         :
         segmentParameters(segmentParameters_),
         breachLength(breachLength_),
         breachedState(breachedState_),
         segmentBreached(segmentBreached_),
         padding(padding_)
      {}



      /**
      * Function to get segmentParameters.
      * <br>Description from the FOM: <i>Specifies the breachable linear object segment characteristics</i>
      * <br>Description of the data type from the FOM: <i>Specifies linear object segment characteristics.</i>
      *
      * @return segmentParameters
      */
      LIBAPI DevStudio::LinearSegmentStruct & getSegmentParameters() {
         return segmentParameters;
      }

      /**
      * Function to get breachLength.
      * <br>Description from the FOM: <i>Specifies the breachable linear object segment as 8 portions of length = BreachLength</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return breachLength
      */
      LIBAPI unsigned int & getBreachLength() {
         return breachLength;
      }

      /**
      * Function to get breachedState.
      * <br>Description from the FOM: <i>Specifies the breached appearance of the breachable linear object segment</i>
      * <br>Description of the data type from the FOM: <i>Breached appearance</i>
      *
      * @return breachedState
      */
      LIBAPI DevStudio::BreachedStatusEnum::BreachedStatusEnum & getBreachedState() {
         return breachedState;
      }

      /**
      * Function to get segmentBreached.
      * <br>Description from the FOM: <i>Specifies whether the segment portion beginning at the segment origin + (i*BreachLength) and extending BreachLength meters is breached or not</i>
      * <br>Description of the data type from the FOM: <i>Specifies the breached appearance for each individual segment portion of length = BreachLength</i>
      *
      * @return segmentBreached
      */
      LIBAPI std::vector</* 8 */ DevStudio::BreachedStatusEnum::BreachedStatusEnum > & getSegmentBreached() {
         return segmentBreached;
      }

      /**
      * Function to get padding.
      * <br>Description from the FOM: <i>Padding to 64 bits</i>
      * <br>Description of the data type from the FOM: <i>Generic array of seven Octet elements.</i>
      *
      * @return padding
      */
      LIBAPI std::vector</* 7 */ char > & getPadding() {
         return padding;
      }

   };


   LIBAPI bool operator ==(const DevStudio::BreachableSegmentStruct& l, const DevStudio::BreachableSegmentStruct& r);
   LIBAPI bool operator !=(const DevStudio::BreachableSegmentStruct& l, const DevStudio::BreachableSegmentStruct& r);
   LIBAPI bool operator <(const DevStudio::BreachableSegmentStruct& l, const DevStudio::BreachableSegmentStruct& r);
   LIBAPI bool operator >(const DevStudio::BreachableSegmentStruct& l, const DevStudio::BreachableSegmentStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::BreachableSegmentStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::BreachableSegmentStruct const &);
}
#endif
