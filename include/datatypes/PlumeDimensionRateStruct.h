/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_PLUMEDIMENSIONRATESTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_PLUMEDIMENSIONRATESTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>


namespace DevStudio {
   /**
   * Implementation of the <code>PlumeDimensionRateStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Record specifying plume dimension rates</i>
   */
   class PlumeDimensionRateStruct {

   public:
      /**
      * Description from the FOM: <i>Variation of plume length</i>.
      * <br>Description of the data type from the FOM: <i>Speed/Velocity in meter per second. [unit: meter per second (m/s), resolution: NA, accuracy: perfect]</i>
      */
      float lengthRate;
      /**
      * Description from the FOM: <i>Variation of plume width</i>.
      * <br>Description of the data type from the FOM: <i>Speed/Velocity in meter per second. [unit: meter per second (m/s), resolution: NA, accuracy: perfect]</i>
      */
      float widthRate;
      /**
      * Description from the FOM: <i>Variation of plume height</i>.
      * <br>Description of the data type from the FOM: <i>Speed/Velocity in meter per second. [unit: meter per second (m/s), resolution: NA, accuracy: perfect]</i>
      */
      float heightRate;

      LIBAPI PlumeDimensionRateStruct()
         :
         lengthRate(0),
         widthRate(0),
         heightRate(0)
      {}

      /**
      * Constructor for PlumeDimensionRateStruct
      *
      * @param lengthRate_ value to set as lengthRate.
      * <br>Description from the FOM: <i>Variation of plume length</i>
      * <br>Description of the data type from the FOM: <i>Speed/Velocity in meter per second. [unit: meter per second (m/s), resolution: NA, accuracy: perfect]</i>
      * @param widthRate_ value to set as widthRate.
      * <br>Description from the FOM: <i>Variation of plume width</i>
      * <br>Description of the data type from the FOM: <i>Speed/Velocity in meter per second. [unit: meter per second (m/s), resolution: NA, accuracy: perfect]</i>
      * @param heightRate_ value to set as heightRate.
      * <br>Description from the FOM: <i>Variation of plume height</i>
      * <br>Description of the data type from the FOM: <i>Speed/Velocity in meter per second. [unit: meter per second (m/s), resolution: NA, accuracy: perfect]</i>
      */
      LIBAPI PlumeDimensionRateStruct(
         float lengthRate_,
         float widthRate_,
         float heightRate_
         )
         :
         lengthRate(lengthRate_),
         widthRate(widthRate_),
         heightRate(heightRate_)
      {}



      /**
      * Function to get lengthRate.
      * <br>Description from the FOM: <i>Variation of plume length</i>
      * <br>Description of the data type from the FOM: <i>Speed/Velocity in meter per second. [unit: meter per second (m/s), resolution: NA, accuracy: perfect]</i>
      *
      * @return lengthRate
      */
      LIBAPI float & getLengthRate() {
         return lengthRate;
      }

      /**
      * Function to get widthRate.
      * <br>Description from the FOM: <i>Variation of plume width</i>
      * <br>Description of the data type from the FOM: <i>Speed/Velocity in meter per second. [unit: meter per second (m/s), resolution: NA, accuracy: perfect]</i>
      *
      * @return widthRate
      */
      LIBAPI float & getWidthRate() {
         return widthRate;
      }

      /**
      * Function to get heightRate.
      * <br>Description from the FOM: <i>Variation of plume height</i>
      * <br>Description of the data type from the FOM: <i>Speed/Velocity in meter per second. [unit: meter per second (m/s), resolution: NA, accuracy: perfect]</i>
      *
      * @return heightRate
      */
      LIBAPI float & getHeightRate() {
         return heightRate;
      }

   };


   LIBAPI bool operator ==(const DevStudio::PlumeDimensionRateStruct& l, const DevStudio::PlumeDimensionRateStruct& r);
   LIBAPI bool operator !=(const DevStudio::PlumeDimensionRateStruct& l, const DevStudio::PlumeDimensionRateStruct& r);
   LIBAPI bool operator <(const DevStudio::PlumeDimensionRateStruct& l, const DevStudio::PlumeDimensionRateStruct& r);
   LIBAPI bool operator >(const DevStudio::PlumeDimensionRateStruct& l, const DevStudio::PlumeDimensionRateStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::PlumeDimensionRateStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::PlumeDimensionRateStruct const &);
}
#endif
