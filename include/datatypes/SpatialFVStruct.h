/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_SPATIALFVSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_SPATIALFVSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <RtiDriver/RprUtility/RprUtility.h>
#include <DevStudio/HlaPointers.h>

#include <DevStudio/datatypes/AccelerationVectorStruct.h>
#include <DevStudio/datatypes/OrientationStruct.h>
#include <DevStudio/datatypes/VelocityVectorStruct.h>
#include <DevStudio/datatypes/WorldLocationStruct.h>

namespace DevStudio {
   /**
   * Implementation of the <code>SpatialFVStruct</code> data type from the FOM.
   * This datatype can be used with the RprUtility package. Please see the Overview document
   * located in the project root directory for more information.
   * <br>Description from the FOM: <i>Spatial structure for Dead Reckoning Algorithm FVW (5) and RVB (9).</i>
   */
   class SpatialFVStruct {

   public:
      /**
      * Description from the FOM: <i>Location of the object.</i>.
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      */
      WorldLocationStruct worldLocation;
      /**
      * Description from the FOM: <i>Whether the object is frozen or not.</i>.
      * <br>Description of the data type from the FOM: <i></i>
      */
      bool isFrozen;
      /**
      * Description from the FOM: <i>The angles of rotation around the coordinate axes between the object's attitude and the reference coordinate system axes (calculated as the Tait-Bryan Euler angles specifying the successive rotations needed to transform from the world coordinate system to the entity coordinate system).</i>.
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      */
      OrientationStruct orientation;
      /**
      * Description from the FOM: <i>The rate at which an object's position is changing over time.</i>.
      * <br>Description of the data type from the FOM: <i>The rate at which the position is changing over time.</i>
      */
      VelocityVectorStruct velocityVector;
      /**
      * Description from the FOM: <i>The magnitude of the change in linear velocity of an object over time.</i>.
      * <br>Description of the data type from the FOM: <i>The magnitude of the change in linear velocity over time.</i>
      */
      AccelerationVectorStruct accelerationVector;

      LIBAPI SpatialFVStruct()
         :
         worldLocation(WorldLocationStruct()),
         isFrozen(0),
         orientation(OrientationStruct()),
         velocityVector(VelocityVectorStruct()),
         accelerationVector(AccelerationVectorStruct())
      {}

      /**
      * Constructor for SpatialFVStruct
      *
      * @param worldLocation_ value to set as worldLocation.
      * <br>Description from the FOM: <i>Location of the object.</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      * @param isFrozen_ value to set as isFrozen.
      * <br>Description from the FOM: <i>Whether the object is frozen or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      * @param orientation_ value to set as orientation.
      * <br>Description from the FOM: <i>The angles of rotation around the coordinate axes between the object's attitude and the reference coordinate system axes (calculated as the Tait-Bryan Euler angles specifying the successive rotations needed to transform from the world coordinate system to the entity coordinate system).</i>
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      * @param velocityVector_ value to set as velocityVector.
      * <br>Description from the FOM: <i>The rate at which an object's position is changing over time.</i>
      * <br>Description of the data type from the FOM: <i>The rate at which the position is changing over time.</i>
      * @param accelerationVector_ value to set as accelerationVector.
      * <br>Description from the FOM: <i>The magnitude of the change in linear velocity of an object over time.</i>
      * <br>Description of the data type from the FOM: <i>The magnitude of the change in linear velocity over time.</i>
      */
      LIBAPI SpatialFVStruct(
         WorldLocationStruct worldLocation_,
         bool isFrozen_,
         OrientationStruct orientation_,
         VelocityVectorStruct velocityVector_,
         AccelerationVectorStruct accelerationVector_
         )
         :
         worldLocation(worldLocation_),
         isFrozen(isFrozen_),
         orientation(orientation_),
         velocityVector(velocityVector_),
         accelerationVector(accelerationVector_)
      {}

      /**
      * Convert to RprUtility datatype
      *
      * @param spatialFV SpatialFVStruct to convert
      *
      * @return spatialFV converted to RprUtility::SpatialStructFV
      */
      LIBAPI static RprUtility::SpatialStructFV convert(const SpatialFVStruct spatialFV);

      /**
      * Convert to RprUtility datatype
      *
      * @param spatialFV SpatialFVStructPtr to convert
      *
      * @return spatialFV converted to RprUtility::SpatialStructFV
      */
      LIBAPI static RprUtility::SpatialStructFV convert(const SpatialFVStructPtr spatialFV);

      /**
      * Convert from RprUtility datatype
      *
      * @param spatialFV RprUtility::SpatialStructFV to convert to SpatialFVStruct
      *
      * @return spatialFV converted to SpatialFVStruct
      */
      LIBAPI static SpatialFVStruct convert(const RprUtility::SpatialStructFV spatialFV);


      /**
      * Function to get worldLocation.
      * <br>Description from the FOM: <i>Location of the object.</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @return worldLocation
      */
      LIBAPI DevStudio::WorldLocationStruct & getWorldLocation() {
         return worldLocation;
      }

      /**
      * Function to get isFrozen.
      * <br>Description from the FOM: <i>Whether the object is frozen or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return isFrozen
      */
      LIBAPI bool & getIsFrozen() {
         return isFrozen;
      }

      /**
      * Function to get orientation.
      * <br>Description from the FOM: <i>The angles of rotation around the coordinate axes between the object's attitude and the reference coordinate system axes (calculated as the Tait-Bryan Euler angles specifying the successive rotations needed to transform from the world coordinate system to the entity coordinate system).</i>
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @return orientation
      */
      LIBAPI DevStudio::OrientationStruct & getOrientation() {
         return orientation;
      }

      /**
      * Function to get velocityVector.
      * <br>Description from the FOM: <i>The rate at which an object's position is changing over time.</i>
      * <br>Description of the data type from the FOM: <i>The rate at which the position is changing over time.</i>
      *
      * @return velocityVector
      */
      LIBAPI DevStudio::VelocityVectorStruct & getVelocityVector() {
         return velocityVector;
      }

      /**
      * Function to get accelerationVector.
      * <br>Description from the FOM: <i>The magnitude of the change in linear velocity of an object over time.</i>
      * <br>Description of the data type from the FOM: <i>The magnitude of the change in linear velocity over time.</i>
      *
      * @return accelerationVector
      */
      LIBAPI DevStudio::AccelerationVectorStruct & getAccelerationVector() {
         return accelerationVector;
      }

   };


   LIBAPI bool operator ==(const DevStudio::SpatialFVStruct& l, const DevStudio::SpatialFVStruct& r);
   LIBAPI bool operator !=(const DevStudio::SpatialFVStruct& l, const DevStudio::SpatialFVStruct& r);
   LIBAPI bool operator <(const DevStudio::SpatialFVStruct& l, const DevStudio::SpatialFVStruct& r);
   LIBAPI bool operator >(const DevStudio::SpatialFVStruct& l, const DevStudio::SpatialFVStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::SpatialFVStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::SpatialFVStruct const &);
}
#endif
