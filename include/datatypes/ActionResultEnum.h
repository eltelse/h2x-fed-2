/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_ACTIONRESULTENUM_H
#define DEVELOPER_STUDIO_DATATYPES_ACTIONRESULTENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace ActionResultEnum {
        /**
        * Implementation of the <code>ActionResultEnum32</code> data type from the FOM.
        * <br>Description from the FOM: <i>Request status</i>
        */
        enum ActionResultEnum {
            /** <code>Other</code> (with ordinal 0) */
            OTHER = 0,
            /** <code>Pending</code> (with ordinal 1) */
            PENDING = 1,
            /** <code>Executing</code> (with ordinal 2) */
            EXECUTING = 2,
            /** <code>PartiallyComplete</code> (with ordinal 3) */
            PARTIALLY_COMPLETE = 3,
            /** <code>Complete</code> (with ordinal 4) */
            COMPLETE = 4,
            /** <code>RequestRejected</code> (with ordinal 5) */
            REQUEST_REJECTED = 5,
            /** <code>RetransmitRequestNow</code> (with ordinal 6) */
            RETRANSMIT_REQUEST_NOW = 6,
            /** <code>RetransmitRequestLater</code> (with ordinal 7) */
            RETRANSMIT_REQUEST_LATER = 7,
            /** <code>InvalidTimeParameters</code> (with ordinal 8) */
            INVALID_TIME_PARAMETERS = 8,
            /** <code>SimulationTimeExceeded</code> (with ordinal 9) */
            SIMULATION_TIME_EXCEEDED = 9,
            /** <code>RequestDone</code> (with ordinal 10) */
            REQUEST_DONE = 10,
            /** <code>TACCSF_LOS_Reply-Type1</code> (with ordinal 100) */
            TACCSF_LOS_REPLY_TYPE1 = 100,
            /** <code>TACCSF_LOS_Reply-Type2</code> (with ordinal 101) */
            TACCSF_LOS_REPLY_TYPE2 = 101,
            /** <code>JoinExerciseRequestRejected</code> (with ordinal 201) */
            JOIN_EXERCISE_REQUEST_REJECTED = 201
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to ActionResultEnum: static_cast<DevStudio::ActionResultEnum::ActionResultEnum>(i)
        */
        LIBAPI bool isValid(const ActionResultEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::ActionResultEnum::ActionResultEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::ActionResultEnum::ActionResultEnum const &);
}


#endif
