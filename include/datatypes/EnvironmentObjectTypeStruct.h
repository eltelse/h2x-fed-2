/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_ENVIRONMENTOBJECTTYPESTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_ENVIRONMENTOBJECTTYPESTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>


namespace DevStudio {
   /**
   * Implementation of the <code>EnvironmentObjectTypeStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Record specifying the domain, the kind and the specific identification of the environment object</i>
   */
   class EnvironmentObjectTypeStruct {

   public:
      /**
      * Description from the FOM: <i>Specifies the domain in which the object exists</i>.
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      char domain;
      /**
      * Description from the FOM: <i>Identifies the kind of object</i>.
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      char kind;
      /**
      * Description from the FOM: <i>Specifies the main category that describes the object</i>.
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      char category;
      /**
      * Description from the FOM: <i>Specifies a particular subcategory to which an object belongs based on the Category field</i>.
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      char subcategory;

      LIBAPI EnvironmentObjectTypeStruct()
         :
         domain(0),
         kind(0),
         category(0),
         subcategory(0)
      {}

      /**
      * Constructor for EnvironmentObjectTypeStruct
      *
      * @param domain_ value to set as domain.
      * <br>Description from the FOM: <i>Specifies the domain in which the object exists</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param kind_ value to set as kind.
      * <br>Description from the FOM: <i>Identifies the kind of object</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param category_ value to set as category.
      * <br>Description from the FOM: <i>Specifies the main category that describes the object</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param subcategory_ value to set as subcategory.
      * <br>Description from the FOM: <i>Specifies a particular subcategory to which an object belongs based on the Category field</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      LIBAPI EnvironmentObjectTypeStruct(
         char domain_,
         char kind_,
         char category_,
         char subcategory_
         )
         :
         domain(domain_),
         kind(kind_),
         category(category_),
         subcategory(subcategory_)
      {}



      /**
      * Function to get domain.
      * <br>Description from the FOM: <i>Specifies the domain in which the object exists</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return domain
      */
      LIBAPI char & getDomain() {
         return domain;
      }

      /**
      * Function to get kind.
      * <br>Description from the FOM: <i>Identifies the kind of object</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return kind
      */
      LIBAPI char & getKind() {
         return kind;
      }

      /**
      * Function to get category.
      * <br>Description from the FOM: <i>Specifies the main category that describes the object</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return category
      */
      LIBAPI char & getCategory() {
         return category;
      }

      /**
      * Function to get subcategory.
      * <br>Description from the FOM: <i>Specifies a particular subcategory to which an object belongs based on the Category field</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return subcategory
      */
      LIBAPI char & getSubcategory() {
         return subcategory;
      }

   };


   LIBAPI bool operator ==(const DevStudio::EnvironmentObjectTypeStruct& l, const DevStudio::EnvironmentObjectTypeStruct& r);
   LIBAPI bool operator !=(const DevStudio::EnvironmentObjectTypeStruct& l, const DevStudio::EnvironmentObjectTypeStruct& r);
   LIBAPI bool operator <(const DevStudio::EnvironmentObjectTypeStruct& l, const DevStudio::EnvironmentObjectTypeStruct& r);
   LIBAPI bool operator >(const DevStudio::EnvironmentObjectTypeStruct& l, const DevStudio::EnvironmentObjectTypeStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::EnvironmentObjectTypeStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::EnvironmentObjectTypeStruct const &);
}
#endif
