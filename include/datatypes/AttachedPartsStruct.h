/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_ATTACHEDPARTSSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_ATTACHEDPARTSSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/EntityTypeStruct.h>
#include <DevStudio/datatypes/StationEnum.h>

namespace DevStudio {
   /**
   * Implementation of the <code>AttachedPartsStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Structure to represent removable parts that may be attached to an entity.</i>
   */
   class AttachedPartsStruct {

   public:
      /**
      * Description from the FOM: <i>Identification of the location (or station) to which the part is attached.</i>.
      * <br>Description of the data type from the FOM: <i>Attached part station</i>
      */
      StationEnum::StationEnum station;
      /**
      * Description from the FOM: <i>The entity type of the attached part.</i>.
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      */
      EntityTypeStruct storeType;

      LIBAPI AttachedPartsStruct()
         :
         station(StationEnum::StationEnum()),
         storeType(EntityTypeStruct())
      {}

      /**
      * Constructor for AttachedPartsStruct
      *
      * @param station_ value to set as station.
      * <br>Description from the FOM: <i>Identification of the location (or station) to which the part is attached.</i>
      * <br>Description of the data type from the FOM: <i>Attached part station</i>
      * @param storeType_ value to set as storeType.
      * <br>Description from the FOM: <i>The entity type of the attached part.</i>
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      */
      LIBAPI AttachedPartsStruct(
         StationEnum::StationEnum station_,
         EntityTypeStruct storeType_
         )
         :
         station(station_),
         storeType(storeType_)
      {}



      /**
      * Function to get station.
      * <br>Description from the FOM: <i>Identification of the location (or station) to which the part is attached.</i>
      * <br>Description of the data type from the FOM: <i>Attached part station</i>
      *
      * @return station
      */
      LIBAPI DevStudio::StationEnum::StationEnum & getStation() {
         return station;
      }

      /**
      * Function to get storeType.
      * <br>Description from the FOM: <i>The entity type of the attached part.</i>
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      *
      * @return storeType
      */
      LIBAPI DevStudio::EntityTypeStruct & getStoreType() {
         return storeType;
      }

   };


   LIBAPI bool operator ==(const DevStudio::AttachedPartsStruct& l, const DevStudio::AttachedPartsStruct& r);
   LIBAPI bool operator !=(const DevStudio::AttachedPartsStruct& l, const DevStudio::AttachedPartsStruct& r);
   LIBAPI bool operator <(const DevStudio::AttachedPartsStruct& l, const DevStudio::AttachedPartsStruct& r);
   LIBAPI bool operator >(const DevStudio::AttachedPartsStruct& l, const DevStudio::AttachedPartsStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::AttachedPartsStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::AttachedPartsStruct const &);
}
#endif
