/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_ENVIRONMENTGRIDAXISTYPEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_ENVIRONMENTGRIDAXISTYPEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace EnvironmentGridAxisTypeEnum {
        /**
        * Implementation of the <code>EnvironmentGridAxisTypeEnum8</code> data type from the FOM.
        * <br>Description from the FOM: <i>Grid axis type</i>
        */
        enum EnvironmentGridAxisTypeEnum {
            /** <code>RegularGridAxisType</code> (with ordinal 0) */
            REGULAR_GRID_AXIS_TYPE = 0,
            /** <code>IrregularGridAxisType</code> (with ordinal 1) */
            IRREGULAR_GRID_AXIS_TYPE = 1
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to EnvironmentGridAxisTypeEnum: static_cast<DevStudio::EnvironmentGridAxisTypeEnum::EnvironmentGridAxisTypeEnum>(i)
        */
        LIBAPI bool isValid(const EnvironmentGridAxisTypeEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::EnvironmentGridAxisTypeEnum::EnvironmentGridAxisTypeEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::EnvironmentGridAxisTypeEnum::EnvironmentGridAxisTypeEnum const &);
}


#endif
