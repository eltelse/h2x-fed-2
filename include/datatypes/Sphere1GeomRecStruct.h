/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_SPHERE1GEOMRECSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_SPHERE1GEOMRECSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/OctetArray4.h>
#include <DevStudio/datatypes/WorldLocationStruct.h>
#include <vector>

namespace DevStudio {
   /**
   * Implementation of the <code>Sphere1GeomRecStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Record specifying Bounding Sphere & Sphere 1 geometry record</i>
   */
   class Sphere1GeomRecStruct {

   public:
      /**
      * Description from the FOM: <i>Centroid location X, Y, Z</i>.
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      */
      WorldLocationStruct centroidLocation;
      /**
      * Description from the FOM: <i>Radius</i>.
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      */
      float radius;
      /**
      * Description from the FOM: <i>Padding field</i>.
      * <br>Description of the data type from the FOM: <i>Generic array of four Octet elements.</i>
      */
      std::vector</* 4 */ char > padding;

      LIBAPI Sphere1GeomRecStruct()
         :
         centroidLocation(WorldLocationStruct()),
         radius(0),
         padding(0)
      {}

      /**
      * Constructor for Sphere1GeomRecStruct
      *
      * @param centroidLocation_ value to set as centroidLocation.
      * <br>Description from the FOM: <i>Centroid location X, Y, Z</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      * @param radius_ value to set as radius.
      * <br>Description from the FOM: <i>Radius</i>
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      * @param padding_ value to set as padding.
      * <br>Description from the FOM: <i>Padding field</i>
      * <br>Description of the data type from the FOM: <i>Generic array of four Octet elements.</i>
      */
      LIBAPI Sphere1GeomRecStruct(
         WorldLocationStruct centroidLocation_,
         float radius_,
         std::vector</* 4 */ char > padding_
         )
         :
         centroidLocation(centroidLocation_),
         radius(radius_),
         padding(padding_)
      {}



      /**
      * Function to get centroidLocation.
      * <br>Description from the FOM: <i>Centroid location X, Y, Z</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @return centroidLocation
      */
      LIBAPI DevStudio::WorldLocationStruct & getCentroidLocation() {
         return centroidLocation;
      }

      /**
      * Function to get radius.
      * <br>Description from the FOM: <i>Radius</i>
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      *
      * @return radius
      */
      LIBAPI float & getRadius() {
         return radius;
      }

      /**
      * Function to get padding.
      * <br>Description from the FOM: <i>Padding field</i>
      * <br>Description of the data type from the FOM: <i>Generic array of four Octet elements.</i>
      *
      * @return padding
      */
      LIBAPI std::vector</* 4 */ char > & getPadding() {
         return padding;
      }

   };


   LIBAPI bool operator ==(const DevStudio::Sphere1GeomRecStruct& l, const DevStudio::Sphere1GeomRecStruct& r);
   LIBAPI bool operator !=(const DevStudio::Sphere1GeomRecStruct& l, const DevStudio::Sphere1GeomRecStruct& r);
   LIBAPI bool operator <(const DevStudio::Sphere1GeomRecStruct& l, const DevStudio::Sphere1GeomRecStruct& r);
   LIBAPI bool operator >(const DevStudio::Sphere1GeomRecStruct& l, const DevStudio::Sphere1GeomRecStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::Sphere1GeomRecStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::Sphere1GeomRecStruct const &);
}
#endif
