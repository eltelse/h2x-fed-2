/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_SPOTCATEGORYENUM_H
#define DEVELOPER_STUDIO_DATATYPES_SPOTCATEGORYENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace SpotCategoryEnum {
        /**
        * Implementation of the <code>SpotCategoryEnum32</code> data type from the FOM.
        * <br>Description from the FOM: <i>-NULL-</i>
        */
        enum SpotCategoryEnum {
            /** <code>UNDEFINED</code> (with ordinal 0) */
            UNDEFINED = 0,
            /** <code>COMBAT_SPOT</code> (with ordinal 1) */
            COMBAT_SPOT = 1,
            /** <code>VISINT_SPOT</code> (with ordinal 2) */
            VISINT_SPOT = 2,
            /** <code>COMMINT_SPOT</code> (with ordinal 4) */
            COMMINT_SPOT = 4,
            /** <code>ELINT_SPOT</code> (with ordinal 8) */
            ELINT_SPOT = 8,
            /** <code>ARTILLERY_SPOT</code> (with ordinal 16) */
            ARTILLERY_SPOT = 16,
            /** <code>THREAT_SPOT</code> (with ordinal 32) */
            THREAT_SPOT = 32,
            /** <code>MALAR_SPOT</code> (with ordinal 64) */
            MALAR_SPOT = 64,
            /** <code>RADAR_SPOT</code> (with ordinal 128) */
            RADAR_SPOT = 128,
            /** <code>AIRCRAFT_SPOT</code> (with ordinal 256) */
            AIRCRAFT_SPOT = 256,
            /** <code>WATERCRAFT_SPOT</code> (with ordinal 512) */
            WATERCRAFT_SPOT = 512,
            /** <code>OTHER_SPOT</code> (with ordinal 1024) */
            OTHER_SPOT = 1024
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to SpotCategoryEnum: static_cast<DevStudio::SpotCategoryEnum::SpotCategoryEnum>(i)
        */
        LIBAPI bool isValid(const SpotCategoryEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::SpotCategoryEnum::SpotCategoryEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::SpotCategoryEnum::SpotCategoryEnum const &);
}


#endif
