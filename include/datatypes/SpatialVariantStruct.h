/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_SPATIALVARIANTSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_SPATIALVARIANTSTRUCT_H

#include <DevStudio/datatypes/DeadReckoningAlgorithmEnum.h>
#include <DevStudio/datatypes/SpatialFPStruct.h>
#include <DevStudio/datatypes/SpatialFVStruct.h>
#include <DevStudio/datatypes/SpatialRPStruct.h>
#include <DevStudio/datatypes/SpatialRVStruct.h>
#include <DevStudio/datatypes/SpatialStaticStruct.h>

#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

#include <RtiDriver/RprUtility/RprUtility.h>

namespace DevStudio {
   /**
   * Implementation of the <code>SpatialVariantStruct</code> data type from the FOM.
   * This datatype can be used with the RprUtility package. Please see the Overview document
   * located in the project root directory for more information.
   * <br>Description from the FOM: <i>Variant Record for a single spatial attribute.</i>
   */
   class SpatialVariantStruct {

   public:
      LIBAPI SpatialVariantStruct()
         : 
         _discriminant(DevStudio::DeadReckoningAlgorithmEnum::DeadReckoningAlgorithmEnum())
      {}

      /** 
      * Create a new alternative SpatialVariantStruct, with <code>Static</code> as discriminant.
      *
      * @param spatialStatic value of the SpatialStatic field
      *
      * @return a new SpatialVariantStruct
      */
      LIBAPI static SpatialVariantStruct createSpatialStatic(DevStudio::SpatialStaticStruct spatialStatic);

      /** 
      * Create a new alternative SpatialVariantStruct, with <code>DRM_FPW</code> as discriminant.
      *
      * @param spatialFPW value of the SpatialFPW field
      *
      * @return a new SpatialVariantStruct
      */
      LIBAPI static SpatialVariantStruct createSpatialFPW(DevStudio::SpatialFPStruct spatialFPW);

      /** 
      * Create a new alternative SpatialVariantStruct, with <code>DRM_RPW</code> as discriminant.
      *
      * @param spatialRPW value of the SpatialRPW field
      *
      * @return a new SpatialVariantStruct
      */
      LIBAPI static SpatialVariantStruct createSpatialRPW(DevStudio::SpatialRPStruct spatialRPW);

      /** 
      * Create a new alternative SpatialVariantStruct, with <code>DRM_RVW</code> as discriminant.
      *
      * @param spatialRVW value of the SpatialRVW field
      *
      * @return a new SpatialVariantStruct
      */
      LIBAPI static SpatialVariantStruct createSpatialRVW(DevStudio::SpatialRVStruct spatialRVW);

      /** 
      * Create a new alternative SpatialVariantStruct, with <code>DRM_FVW</code> as discriminant.
      *
      * @param spatialFVW value of the SpatialFVW field
      *
      * @return a new SpatialVariantStruct
      */
      LIBAPI static SpatialVariantStruct createSpatialFVW(DevStudio::SpatialFVStruct spatialFVW);

      /** 
      * Create a new alternative SpatialVariantStruct, with <code>DRM_FPB</code> as discriminant.
      *
      * @param spatialFPB value of the SpatialFPB field
      *
      * @return a new SpatialVariantStruct
      */
      LIBAPI static SpatialVariantStruct createSpatialFPB(DevStudio::SpatialFPStruct spatialFPB);

      /** 
      * Create a new alternative SpatialVariantStruct, with <code>DRM_RPB</code> as discriminant.
      *
      * @param spatialRPB value of the SpatialRPB field
      *
      * @return a new SpatialVariantStruct
      */
      LIBAPI static SpatialVariantStruct createSpatialRPB(DevStudio::SpatialRPStruct spatialRPB);

      /** 
      * Create a new alternative SpatialVariantStruct, with <code>DRM_RVB</code> as discriminant.
      *
      * @param spatialRVB value of the SpatialRVB field
      *
      * @return a new SpatialVariantStruct
      */
      LIBAPI static SpatialVariantStruct createSpatialRVB(DevStudio::SpatialRVStruct spatialRVB);

      /** 
      * Create a new alternative SpatialVariantStruct, with <code>DRM_FVB</code> as discriminant.
      *
      * @param spatialFVB value of the SpatialFVB field
      *
      * @return a new SpatialVariantStruct
      */
      LIBAPI static SpatialVariantStruct createSpatialFVB(DevStudio::SpatialFVStruct spatialFVB);

      /**
      * Create a new alternative SpatialVariantStruct, with <code>Other</code> as discriminant.
      *
      * @return a new SpatialVariantStruct
      */
      LIBAPI static SpatialVariantStruct createOther();


      /**
      * Convert to RprUtility datatype
      *
      * @param spatial SpatialVariantStruct to convert
      *
      * @return spatial converted to RprUtility::SpatialStruct
      */
      LIBAPI static RprUtility::SpatialStruct convert(const SpatialVariantStruct spatial);

      /**
      * Convert from RprUtility datatype
      *
      * @param spatial RprUtility::SpatialStruct to convert to SpatialVariantStruct
      *
      * @return spatial converted to SpatialVariantStruct
      */
      LIBAPI static SpatialVariantStruct convert(const RprUtility::SpatialStruct spatial);

      /**
      * Function to get discriminant
      *
      * @return disciminant
      */
      LIBAPI DevStudio::DeadReckoningAlgorithmEnum::DeadReckoningAlgorithmEnum getDiscriminant() const {
          return _discriminant;
      }

      /**
      * Function to get SpatialStatic.
      * Note that this field is only valid of the discriminant is <code>Static</code>.
      * <br>Description from the FOM: <i>Variant for representing a static object.</i>
      * <br>Description of the data type from the FOM: <i>Spatial structure for Dead Reckoning Algorithm Static (1).</i>
      *
      * @return SpatialStatic value
      */
      LIBAPI DevStudio::SpatialStaticStructPtr getSpatialStatic() const {
         return _spatialStatic;
      }

      /**
      * Function to set SpatialStatic.
      * Note that this will set the discriminant to <code>Static</code>.
      * <br>Description from the FOM: <i>Variant for representing a static object.</i>
      * <br>Description of the data type from the FOM: <i>Spatial structure for Dead Reckoning Algorithm Static (1).</i>
      *
      * @param spatialStatic value used to create a SpatialStaticStructPtr
      */
      LIBAPI void setSpatialStatic(const DevStudio::SpatialStaticStruct spatialStatic) {
         _spatialStatic = DevStudio::SpatialStaticStructPtr( new DevStudio::SpatialStaticStruct (spatialStatic));
         _discriminant = DeadReckoningAlgorithmEnum::STATIC_;
      }

      /**
      * Function to get SpatialFPW.
      * Note that this field is only valid of the discriminant is <code>DRM_FPW</code>.
      * <br>Description from the FOM: <i>Variant for representing an object with a constant velocity (or low acceleration) linear motion in world coordinates.</i>
      * <br>Description of the data type from the FOM: <i>Spatial structure for Dead Reckoning Algorithm FPW (2) and FPB (6).</i>
      *
      * @return SpatialFPW value
      */
      LIBAPI DevStudio::SpatialFPStructPtr getSpatialFPW() const {
         return _spatialFPW;
      }

      /**
      * Function to set SpatialFPW.
      * Note that this will set the discriminant to <code>DRM_FPW</code>.
      * <br>Description from the FOM: <i>Variant for representing an object with a constant velocity (or low acceleration) linear motion in world coordinates.</i>
      * <br>Description of the data type from the FOM: <i>Spatial structure for Dead Reckoning Algorithm FPW (2) and FPB (6).</i>
      *
      * @param spatialFPW value used to create a SpatialFPStructPtr
      */
      LIBAPI void setSpatialFPW(const DevStudio::SpatialFPStruct spatialFPW) {
         _spatialFPW = DevStudio::SpatialFPStructPtr( new DevStudio::SpatialFPStruct (spatialFPW));
         _discriminant = DeadReckoningAlgorithmEnum::DRM_FPW;
      }

      /**
      * Function to get SpatialRPW.
      * Note that this field is only valid of the discriminant is <code>DRM_RPW</code>.
      * <br>Description from the FOM: <i>Variant for representing an object with a constant velocity (or low acceleration) linear motion, including rotation information, in world coordinates.</i>
      * <br>Description of the data type from the FOM: <i>Spatial structure for Dead Reckoning Algorithm RPW (3) and RPB (7).</i>
      *
      * @return SpatialRPW value
      */
      LIBAPI DevStudio::SpatialRPStructPtr getSpatialRPW() const {
         return _spatialRPW;
      }

      /**
      * Function to set SpatialRPW.
      * Note that this will set the discriminant to <code>DRM_RPW</code>.
      * <br>Description from the FOM: <i>Variant for representing an object with a constant velocity (or low acceleration) linear motion, including rotation information, in world coordinates.</i>
      * <br>Description of the data type from the FOM: <i>Spatial structure for Dead Reckoning Algorithm RPW (3) and RPB (7).</i>
      *
      * @param spatialRPW value used to create a SpatialRPStructPtr
      */
      LIBAPI void setSpatialRPW(const DevStudio::SpatialRPStruct spatialRPW) {
         _spatialRPW = DevStudio::SpatialRPStructPtr( new DevStudio::SpatialRPStruct (spatialRPW));
         _discriminant = DeadReckoningAlgorithmEnum::DRM_RPW;
      }

      /**
      * Function to get SpatialRVW.
      * Note that this field is only valid of the discriminant is <code>DRM_RVW</code>.
      * <br>Description from the FOM: <i>Variant for representing an object with high speed or maneuvering at any speed, including rotation information, in world coordinates.</i>
      * <br>Description of the data type from the FOM: <i>Spatial structure for Dead Reckoning Algorithm RVW (4) and RVB (8).</i>
      *
      * @return SpatialRVW value
      */
      LIBAPI DevStudio::SpatialRVStructPtr getSpatialRVW() const {
         return _spatialRVW;
      }

      /**
      * Function to set SpatialRVW.
      * Note that this will set the discriminant to <code>DRM_RVW</code>.
      * <br>Description from the FOM: <i>Variant for representing an object with high speed or maneuvering at any speed, including rotation information, in world coordinates.</i>
      * <br>Description of the data type from the FOM: <i>Spatial structure for Dead Reckoning Algorithm RVW (4) and RVB (8).</i>
      *
      * @param spatialRVW value used to create a SpatialRVStructPtr
      */
      LIBAPI void setSpatialRVW(const DevStudio::SpatialRVStruct spatialRVW) {
         _spatialRVW = DevStudio::SpatialRVStructPtr( new DevStudio::SpatialRVStruct (spatialRVW));
         _discriminant = DeadReckoningAlgorithmEnum::DRM_RVW;
      }

      /**
      * Function to get SpatialFVW.
      * Note that this field is only valid of the discriminant is <code>DRM_FVW</code>.
      * <br>Description from the FOM: <i>Variant for representing an object with high speed or maneuvering at any speed in world coordinates.</i>
      * <br>Description of the data type from the FOM: <i>Spatial structure for Dead Reckoning Algorithm FVW (5) and RVB (9).</i>
      *
      * @return SpatialFVW value
      */
      LIBAPI DevStudio::SpatialFVStructPtr getSpatialFVW() const {
         return _spatialFVW;
      }

      /**
      * Function to set SpatialFVW.
      * Note that this will set the discriminant to <code>DRM_FVW</code>.
      * <br>Description from the FOM: <i>Variant for representing an object with high speed or maneuvering at any speed in world coordinates.</i>
      * <br>Description of the data type from the FOM: <i>Spatial structure for Dead Reckoning Algorithm FVW (5) and RVB (9).</i>
      *
      * @param spatialFVW value used to create a SpatialFVStructPtr
      */
      LIBAPI void setSpatialFVW(const DevStudio::SpatialFVStruct spatialFVW) {
         _spatialFVW = DevStudio::SpatialFVStructPtr( new DevStudio::SpatialFVStruct (spatialFVW));
         _discriminant = DeadReckoningAlgorithmEnum::DRM_FVW;
      }

      /**
      * Function to get SpatialFPB.
      * Note that this field is only valid of the discriminant is <code>DRM_FPB</code>.
      * <br>Description from the FOM: <i>Variant for representing an object with a constant velocity (or low acceleration) linear motion in body axis coordinates.</i>
      * <br>Description of the data type from the FOM: <i>Spatial structure for Dead Reckoning Algorithm FPW (2) and FPB (6).</i>
      *
      * @return SpatialFPB value
      */
      LIBAPI DevStudio::SpatialFPStructPtr getSpatialFPB() const {
         return _spatialFPB;
      }

      /**
      * Function to set SpatialFPB.
      * Note that this will set the discriminant to <code>DRM_FPB</code>.
      * <br>Description from the FOM: <i>Variant for representing an object with a constant velocity (or low acceleration) linear motion in body axis coordinates.</i>
      * <br>Description of the data type from the FOM: <i>Spatial structure for Dead Reckoning Algorithm FPW (2) and FPB (6).</i>
      *
      * @param spatialFPB value used to create a SpatialFPStructPtr
      */
      LIBAPI void setSpatialFPB(const DevStudio::SpatialFPStruct spatialFPB) {
         _spatialFPB = DevStudio::SpatialFPStructPtr( new DevStudio::SpatialFPStruct (spatialFPB));
         _discriminant = DeadReckoningAlgorithmEnum::DRM_FPB;
      }

      /**
      * Function to get SpatialRPB.
      * Note that this field is only valid of the discriminant is <code>DRM_RPB</code>.
      * <br>Description from the FOM: <i>Variant for representing an object with a constant velocity (or low acceleration) linear motion, including rotation information, in body axis coordinates.</i>
      * <br>Description of the data type from the FOM: <i>Spatial structure for Dead Reckoning Algorithm RPW (3) and RPB (7).</i>
      *
      * @return SpatialRPB value
      */
      LIBAPI DevStudio::SpatialRPStructPtr getSpatialRPB() const {
         return _spatialRPB;
      }

      /**
      * Function to set SpatialRPB.
      * Note that this will set the discriminant to <code>DRM_RPB</code>.
      * <br>Description from the FOM: <i>Variant for representing an object with a constant velocity (or low acceleration) linear motion, including rotation information, in body axis coordinates.</i>
      * <br>Description of the data type from the FOM: <i>Spatial structure for Dead Reckoning Algorithm RPW (3) and RPB (7).</i>
      *
      * @param spatialRPB value used to create a SpatialRPStructPtr
      */
      LIBAPI void setSpatialRPB(const DevStudio::SpatialRPStruct spatialRPB) {
         _spatialRPB = DevStudio::SpatialRPStructPtr( new DevStudio::SpatialRPStruct (spatialRPB));
         _discriminant = DeadReckoningAlgorithmEnum::DRM_RPB;
      }

      /**
      * Function to get SpatialRVB.
      * Note that this field is only valid of the discriminant is <code>DRM_RVB</code>.
      * <br>Description from the FOM: <i>Variant for representing an object with high speed or maneuvering at any speed, including rotation information, in body axis coordinates.</i>
      * <br>Description of the data type from the FOM: <i>Spatial structure for Dead Reckoning Algorithm RVW (4) and RVB (8).</i>
      *
      * @return SpatialRVB value
      */
      LIBAPI DevStudio::SpatialRVStructPtr getSpatialRVB() const {
         return _spatialRVB;
      }

      /**
      * Function to set SpatialRVB.
      * Note that this will set the discriminant to <code>DRM_RVB</code>.
      * <br>Description from the FOM: <i>Variant for representing an object with high speed or maneuvering at any speed, including rotation information, in body axis coordinates.</i>
      * <br>Description of the data type from the FOM: <i>Spatial structure for Dead Reckoning Algorithm RVW (4) and RVB (8).</i>
      *
      * @param spatialRVB value used to create a SpatialRVStructPtr
      */
      LIBAPI void setSpatialRVB(const DevStudio::SpatialRVStruct spatialRVB) {
         _spatialRVB = DevStudio::SpatialRVStructPtr( new DevStudio::SpatialRVStruct (spatialRVB));
         _discriminant = DeadReckoningAlgorithmEnum::DRM_RVB;
      }

      /**
      * Function to get SpatialFVB.
      * Note that this field is only valid of the discriminant is <code>DRM_FVB</code>.
      * <br>Description from the FOM: <i>Variant for representing an object with high speed or maneuvering at any speed in body axis coordinates.</i>
      * <br>Description of the data type from the FOM: <i>Spatial structure for Dead Reckoning Algorithm FVW (5) and RVB (9).</i>
      *
      * @return SpatialFVB value
      */
      LIBAPI DevStudio::SpatialFVStructPtr getSpatialFVB() const {
         return _spatialFVB;
      }

      /**
      * Function to set SpatialFVB.
      * Note that this will set the discriminant to <code>DRM_FVB</code>.
      * <br>Description from the FOM: <i>Variant for representing an object with high speed or maneuvering at any speed in body axis coordinates.</i>
      * <br>Description of the data type from the FOM: <i>Spatial structure for Dead Reckoning Algorithm FVW (5) and RVB (9).</i>
      *
      * @param spatialFVB value used to create a SpatialFVStructPtr
      */
      LIBAPI void setSpatialFVB(const DevStudio::SpatialFVStruct spatialFVB) {
         _spatialFVB = DevStudio::SpatialFVStructPtr( new DevStudio::SpatialFVStruct (spatialFVB));
         _discriminant = DeadReckoningAlgorithmEnum::DRM_FVB;
      }

      /** Description from the FOM: <i>Variant for representing a static object.</i> */
      DevStudio::SpatialStaticStructPtr _spatialStatic;
      /** Description from the FOM: <i>Variant for representing an object with a constant velocity (or low acceleration) linear motion in world coordinates.</i> */
      DevStudio::SpatialFPStructPtr _spatialFPW;
      /** Description from the FOM: <i>Variant for representing an object with a constant velocity (or low acceleration) linear motion, including rotation information, in world coordinates.</i> */
      DevStudio::SpatialRPStructPtr _spatialRPW;
      /** Description from the FOM: <i>Variant for representing an object with high speed or maneuvering at any speed, including rotation information, in world coordinates.</i> */
      DevStudio::SpatialRVStructPtr _spatialRVW;
      /** Description from the FOM: <i>Variant for representing an object with high speed or maneuvering at any speed in world coordinates.</i> */
      DevStudio::SpatialFVStructPtr _spatialFVW;
      /** Description from the FOM: <i>Variant for representing an object with a constant velocity (or low acceleration) linear motion in body axis coordinates.</i> */
      DevStudio::SpatialFPStructPtr _spatialFPB;
      /** Description from the FOM: <i>Variant for representing an object with a constant velocity (or low acceleration) linear motion, including rotation information, in body axis coordinates.</i> */
      DevStudio::SpatialRPStructPtr _spatialRPB;
      /** Description from the FOM: <i>Variant for representing an object with high speed or maneuvering at any speed, including rotation information, in body axis coordinates.</i> */
      DevStudio::SpatialRVStructPtr _spatialRVB;
      /** Description from the FOM: <i>Variant for representing an object with high speed or maneuvering at any speed in body axis coordinates.</i> */
      DevStudio::SpatialFVStructPtr _spatialFVB;

   private:
      DevStudio::DeadReckoningAlgorithmEnum::DeadReckoningAlgorithmEnum _discriminant;

      SpatialVariantStruct(
         DevStudio::SpatialStaticStructPtr spatialStatic,
         DevStudio::SpatialFPStructPtr spatialFPW,
         DevStudio::SpatialRPStructPtr spatialRPW,
         DevStudio::SpatialRVStructPtr spatialRVW,
         DevStudio::SpatialFVStructPtr spatialFVW,
         DevStudio::SpatialFPStructPtr spatialFPB,
         DevStudio::SpatialRPStructPtr spatialRPB,
         DevStudio::SpatialRVStructPtr spatialRVB,
         DevStudio::SpatialFVStructPtr spatialFVB,
         DevStudio::DeadReckoningAlgorithmEnum::DeadReckoningAlgorithmEnum discriminant
      );
   };

   LIBAPI bool operator ==(const DevStudio::SpatialVariantStruct& l, const DevStudio::SpatialVariantStruct& r);
   LIBAPI bool operator !=(const DevStudio::SpatialVariantStruct& l, const DevStudio::SpatialVariantStruct& r);
   LIBAPI bool operator <(const DevStudio::SpatialVariantStruct& l, const DevStudio::SpatialVariantStruct& r);
   LIBAPI bool operator >(const DevStudio::SpatialVariantStruct& l, const DevStudio::SpatialVariantStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::SpatialVariantStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::SpatialVariantStruct const &);
}

#endif
