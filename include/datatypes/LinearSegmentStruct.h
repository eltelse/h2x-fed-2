/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_LINEARSEGMENTSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_LINEARSEGMENTSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/DamageStatusEnum.h>
#include <DevStudio/datatypes/OrientationStruct.h>
#include <DevStudio/datatypes/WorldLocationStruct.h>

namespace DevStudio {
   /**
   * Implementation of the <code>LinearSegmentStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Specifies linear object segment characteristics.</i>
   */
   class LinearSegmentStruct {

   public:
      /**
      * Description from the FOM: <i>Identifies the individual segment.</i>.
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      unsigned int segmentNumber;
      /**
      * Description from the FOM: <i>Specifies the percent completion of the segment.</i>.
      * <br>Description of the data type from the FOM: <i>Percentage [unit: percent (%), resolution: 1, accuracy: perfect]</i>
      */
      unsigned int percentComplete;
      /**
      * Description from the FOM: <i>Specifies the location of the segment.</i>.
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      */
      WorldLocationStruct location;
      /**
      * Description from the FOM: <i>Specifies the orientation of the segment.</i>.
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      */
      OrientationStruct orientation;
      /**
      * Description from the FOM: <i>Specifies the length of the segment, in meters, extending in the positive X direction.</i>.
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      unsigned short length;
      /**
      * Description from the FOM: <i>Specifies the total width of the segment, in meters; one-half of the width shall extend in the positive Y direction, and one-half of the width shall extend in the negative Y direction.</i>.
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      unsigned short width;
      /**
      * Description from the FOM: <i>Specifies the height of the segment, in meters, above ground.</i>.
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      unsigned short height;
      /**
      * Description from the FOM: <i>Specifies the depth of the segment, in meters, below ground level.</i>.
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      unsigned short depth;
      /**
      * Description from the FOM: <i>Specifies the damaged appearance of the segment.</i>.
      * <br>Description of the data type from the FOM: <i>Damaged appearance</i>
      */
      DamageStatusEnum::DamageStatusEnum damagedState;
      /**
      * Description from the FOM: <i>Specifies whether or not the segment has been deactivated (it has ceased to exist in the synthetic environment).</i>.
      * <br>Description of the data type from the FOM: <i></i>
      */
      bool deactivated;
      /**
      * Description from the FOM: <i>Specifies whether or not the segment is aflame.</i>.
      * <br>Description of the data type from the FOM: <i></i>
      */
      bool flaming;
      /**
      * Description from the FOM: <i>Specifies whether or not the segment was created before the start of the exercise.</i>.
      * <br>Description of the data type from the FOM: <i></i>
      */
      bool objectPreDistributed;
      /**
      * Description from the FOM: <i>Specifies whether or not the segment is smoking (creating a smoke plume).</i>.
      * <br>Description of the data type from the FOM: <i></i>
      */
      bool smoking;

      LIBAPI LinearSegmentStruct()
         :
         segmentNumber(0),
         percentComplete(0),
         location(WorldLocationStruct()),
         orientation(OrientationStruct()),
         length(0),
         width(0),
         height(0),
         depth(0),
         damagedState(DamageStatusEnum::DamageStatusEnum()),
         deactivated(0),
         flaming(0),
         objectPreDistributed(0),
         smoking(0)
      {}

      /**
      * Constructor for LinearSegmentStruct
      *
      * @param segmentNumber_ value to set as segmentNumber.
      * <br>Description from the FOM: <i>Identifies the individual segment.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param percentComplete_ value to set as percentComplete.
      * <br>Description from the FOM: <i>Specifies the percent completion of the segment.</i>
      * <br>Description of the data type from the FOM: <i>Percentage [unit: percent (%), resolution: 1, accuracy: perfect]</i>
      * @param location_ value to set as location.
      * <br>Description from the FOM: <i>Specifies the location of the segment.</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      * @param orientation_ value to set as orientation.
      * <br>Description from the FOM: <i>Specifies the orientation of the segment.</i>
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      * @param length_ value to set as length.
      * <br>Description from the FOM: <i>Specifies the length of the segment, in meters, extending in the positive X direction.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param width_ value to set as width.
      * <br>Description from the FOM: <i>Specifies the total width of the segment, in meters; one-half of the width shall extend in the positive Y direction, and one-half of the width shall extend in the negative Y direction.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param height_ value to set as height.
      * <br>Description from the FOM: <i>Specifies the height of the segment, in meters, above ground.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param depth_ value to set as depth.
      * <br>Description from the FOM: <i>Specifies the depth of the segment, in meters, below ground level.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param damagedState_ value to set as damagedState.
      * <br>Description from the FOM: <i>Specifies the damaged appearance of the segment.</i>
      * <br>Description of the data type from the FOM: <i>Damaged appearance</i>
      * @param deactivated_ value to set as deactivated.
      * <br>Description from the FOM: <i>Specifies whether or not the segment has been deactivated (it has ceased to exist in the synthetic environment).</i>
      * <br>Description of the data type from the FOM: <i></i>
      * @param flaming_ value to set as flaming.
      * <br>Description from the FOM: <i>Specifies whether or not the segment is aflame.</i>
      * <br>Description of the data type from the FOM: <i></i>
      * @param objectPreDistributed_ value to set as objectPreDistributed.
      * <br>Description from the FOM: <i>Specifies whether or not the segment was created before the start of the exercise.</i>
      * <br>Description of the data type from the FOM: <i></i>
      * @param smoking_ value to set as smoking.
      * <br>Description from the FOM: <i>Specifies whether or not the segment is smoking (creating a smoke plume).</i>
      * <br>Description of the data type from the FOM: <i></i>
      */
      LIBAPI LinearSegmentStruct(
         unsigned int segmentNumber_,
         unsigned int percentComplete_,
         WorldLocationStruct location_,
         OrientationStruct orientation_,
         unsigned short length_,
         unsigned short width_,
         unsigned short height_,
         unsigned short depth_,
         DamageStatusEnum::DamageStatusEnum damagedState_,
         bool deactivated_,
         bool flaming_,
         bool objectPreDistributed_,
         bool smoking_
         )
         :
         segmentNumber(segmentNumber_),
         percentComplete(percentComplete_),
         location(location_),
         orientation(orientation_),
         length(length_),
         width(width_),
         height(height_),
         depth(depth_),
         damagedState(damagedState_),
         deactivated(deactivated_),
         flaming(flaming_),
         objectPreDistributed(objectPreDistributed_),
         smoking(smoking_)
      {}



      /**
      * Function to get segmentNumber.
      * <br>Description from the FOM: <i>Identifies the individual segment.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return segmentNumber
      */
      LIBAPI unsigned int & getSegmentNumber() {
         return segmentNumber;
      }

      /**
      * Function to get percentComplete.
      * <br>Description from the FOM: <i>Specifies the percent completion of the segment.</i>
      * <br>Description of the data type from the FOM: <i>Percentage [unit: percent (%), resolution: 1, accuracy: perfect]</i>
      *
      * @return percentComplete
      */
      LIBAPI unsigned int & getPercentComplete() {
         return percentComplete;
      }

      /**
      * Function to get location.
      * <br>Description from the FOM: <i>Specifies the location of the segment.</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @return location
      */
      LIBAPI DevStudio::WorldLocationStruct & getLocation() {
         return location;
      }

      /**
      * Function to get orientation.
      * <br>Description from the FOM: <i>Specifies the orientation of the segment.</i>
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @return orientation
      */
      LIBAPI DevStudio::OrientationStruct & getOrientation() {
         return orientation;
      }

      /**
      * Function to get length.
      * <br>Description from the FOM: <i>Specifies the length of the segment, in meters, extending in the positive X direction.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return length
      */
      LIBAPI unsigned short & getLength() {
         return length;
      }

      /**
      * Function to get width.
      * <br>Description from the FOM: <i>Specifies the total width of the segment, in meters; one-half of the width shall extend in the positive Y direction, and one-half of the width shall extend in the negative Y direction.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return width
      */
      LIBAPI unsigned short & getWidth() {
         return width;
      }

      /**
      * Function to get height.
      * <br>Description from the FOM: <i>Specifies the height of the segment, in meters, above ground.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return height
      */
      LIBAPI unsigned short & getHeight() {
         return height;
      }

      /**
      * Function to get depth.
      * <br>Description from the FOM: <i>Specifies the depth of the segment, in meters, below ground level.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return depth
      */
      LIBAPI unsigned short & getDepth() {
         return depth;
      }

      /**
      * Function to get damagedState.
      * <br>Description from the FOM: <i>Specifies the damaged appearance of the segment.</i>
      * <br>Description of the data type from the FOM: <i>Damaged appearance</i>
      *
      * @return damagedState
      */
      LIBAPI DevStudio::DamageStatusEnum::DamageStatusEnum & getDamagedState() {
         return damagedState;
      }

      /**
      * Function to get deactivated.
      * <br>Description from the FOM: <i>Specifies whether or not the segment has been deactivated (it has ceased to exist in the synthetic environment).</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return deactivated
      */
      LIBAPI bool & getDeactivated() {
         return deactivated;
      }

      /**
      * Function to get flaming.
      * <br>Description from the FOM: <i>Specifies whether or not the segment is aflame.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return flaming
      */
      LIBAPI bool & getFlaming() {
         return flaming;
      }

      /**
      * Function to get objectPreDistributed.
      * <br>Description from the FOM: <i>Specifies whether or not the segment was created before the start of the exercise.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return objectPreDistributed
      */
      LIBAPI bool & getObjectPreDistributed() {
         return objectPreDistributed;
      }

      /**
      * Function to get smoking.
      * <br>Description from the FOM: <i>Specifies whether or not the segment is smoking (creating a smoke plume).</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return smoking
      */
      LIBAPI bool & getSmoking() {
         return smoking;
      }

   };


   LIBAPI bool operator ==(const DevStudio::LinearSegmentStruct& l, const DevStudio::LinearSegmentStruct& r);
   LIBAPI bool operator !=(const DevStudio::LinearSegmentStruct& l, const DevStudio::LinearSegmentStruct& r);
   LIBAPI bool operator <(const DevStudio::LinearSegmentStruct& l, const DevStudio::LinearSegmentStruct& r);
   LIBAPI bool operator >(const DevStudio::LinearSegmentStruct& l, const DevStudio::LinearSegmentStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::LinearSegmentStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::LinearSegmentStruct const &);
}
#endif
