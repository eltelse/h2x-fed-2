/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_ENCODINGTYPEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_ENCODINGTYPEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace EncodingTypeEnum {
        /**
        * Implementation of the <code>EncodingTypeEnum32</code> data type from the FOM.
        * <br>Description from the FOM: <i>Radio signal encoding type</i>
        */
        enum EncodingTypeEnum {
            /** <code>Encoding_8-bit_mu-law</code> (with ordinal 1) */
            ENCODING_8_BIT_MU_LAW = 1,
            /** <code>CVSD_per_MIL-STD-188-113</code> (with ordinal 2) */
            CVSD_PER_MIL_STD_188_113 = 2,
            /** <code>ADPCM_per_CCITT_G721</code> (with ordinal 3) */
            ADPCM_PER_CCITT_G721 = 3,
            /** <code>Encoding_16-bit_linear_PCM</code> (with ordinal 4) */
            ENCODING_16_BIT_LINEAR_PCM = 4,
            /** <code>Encoding_8-bit_linear_PCM</code> (with ordinal 5) */
            ENCODING_8_BIT_LINEAR_PCM = 5,
            /** <code>VQ__Vector_Quantization_</code> (with ordinal 6) */
            VQ_VECTOR_QUANTIZATION = 6,
            /** <code>_unavailableForUse_</code> (with ordinal 7) */
            UNAVAILABLE_FOR_USE = 7,
            /** <code>GSMFull-Rate_ETSI06_10_</code> (with ordinal 8) */
            GSMFULL_RATE_ETSI06_10 = 8,
            /** <code>GSMHalf-Rate_ETSI06_20_</code> (with ordinal 9) */
            GSMHALF_RATE_ETSI06_20 = 9,
            /** <code>SpeexNarrowBand</code> (with ordinal 10) */
            SPEEX_NARROW_BAND = 10,
            /** <code>Encoding_16-bitLinearPCM2_sComplement_LittleEndian</code> (with ordinal 100) */
            ENCODING_16_BIT_LINEAR_PCM2_S_COMPLEMENT_LITTLE_ENDIAN = 100,
            /** <code>_unavailableForUse2_</code> (with ordinal 255) */
            UNAVAILABLE_FOR_USE2 = 255
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to EncodingTypeEnum: static_cast<DevStudio::EncodingTypeEnum::EncodingTypeEnum>(i)
        */
        LIBAPI bool isValid(const EncodingTypeEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::EncodingTypeEnum::EncodingTypeEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::EncodingTypeEnum::EncodingTypeEnum const &);
}


#endif
