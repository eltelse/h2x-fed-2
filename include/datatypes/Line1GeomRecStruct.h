/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_LINE1GEOMRECSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_LINE1GEOMRECSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/WorldLocationStruct.h>

namespace DevStudio {
   /**
   * Implementation of the <code>Line1GeomRecStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Record specifying Line 1 geometry record</i>
   */
   class Line1GeomRecStruct {

   public:
      /**
      * Description from the FOM: <i>Start point location</i>.
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      */
      WorldLocationStruct startPointLocation;
      /**
      * Description from the FOM: <i>End point location</i>.
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      */
      WorldLocationStruct endPointLocation;

      LIBAPI Line1GeomRecStruct()
         :
         startPointLocation(WorldLocationStruct()),
         endPointLocation(WorldLocationStruct())
      {}

      /**
      * Constructor for Line1GeomRecStruct
      *
      * @param startPointLocation_ value to set as startPointLocation.
      * <br>Description from the FOM: <i>Start point location</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      * @param endPointLocation_ value to set as endPointLocation.
      * <br>Description from the FOM: <i>End point location</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      */
      LIBAPI Line1GeomRecStruct(
         WorldLocationStruct startPointLocation_,
         WorldLocationStruct endPointLocation_
         )
         :
         startPointLocation(startPointLocation_),
         endPointLocation(endPointLocation_)
      {}



      /**
      * Function to get startPointLocation.
      * <br>Description from the FOM: <i>Start point location</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @return startPointLocation
      */
      LIBAPI DevStudio::WorldLocationStruct & getStartPointLocation() {
         return startPointLocation;
      }

      /**
      * Function to get endPointLocation.
      * <br>Description from the FOM: <i>End point location</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @return endPointLocation
      */
      LIBAPI DevStudio::WorldLocationStruct & getEndPointLocation() {
         return endPointLocation;
      }

   };


   LIBAPI bool operator ==(const DevStudio::Line1GeomRecStruct& l, const DevStudio::Line1GeomRecStruct& r);
   LIBAPI bool operator !=(const DevStudio::Line1GeomRecStruct& l, const DevStudio::Line1GeomRecStruct& r);
   LIBAPI bool operator <(const DevStudio::Line1GeomRecStruct& l, const DevStudio::Line1GeomRecStruct& r);
   LIBAPI bool operator >(const DevStudio::Line1GeomRecStruct& l, const DevStudio::Line1GeomRecStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::Line1GeomRecStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::Line1GeomRecStruct const &);
}
#endif
