/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_RELATIVEPOSITIONSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_RELATIVEPOSITIONSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>


namespace DevStudio {
   /**
   * Implementation of the <code>RelativePositionStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Relative position in right-handed Cartesian coordinates.</i>
   */
   class RelativePositionStruct {

   public:
      /**
      * Description from the FOM: <i>The distance from the reference location along the X axis.</i>.
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      */
      float bodyXDistance;
      /**
      * Description from the FOM: <i>The distance from the reference location along the Y axis.</i>.
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      */
      float bodyYDistance;
      /**
      * Description from the FOM: <i>The distance from the reference location along the Z axis.</i>.
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      */
      float bodyZDistance;

      LIBAPI RelativePositionStruct()
         :
         bodyXDistance(0),
         bodyYDistance(0),
         bodyZDistance(0)
      {}

      /**
      * Constructor for RelativePositionStruct
      *
      * @param bodyXDistance_ value to set as bodyXDistance.
      * <br>Description from the FOM: <i>The distance from the reference location along the X axis.</i>
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      * @param bodyYDistance_ value to set as bodyYDistance.
      * <br>Description from the FOM: <i>The distance from the reference location along the Y axis.</i>
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      * @param bodyZDistance_ value to set as bodyZDistance.
      * <br>Description from the FOM: <i>The distance from the reference location along the Z axis.</i>
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      */
      LIBAPI RelativePositionStruct(
         float bodyXDistance_,
         float bodyYDistance_,
         float bodyZDistance_
         )
         :
         bodyXDistance(bodyXDistance_),
         bodyYDistance(bodyYDistance_),
         bodyZDistance(bodyZDistance_)
      {}



      /**
      * Function to get bodyXDistance.
      * <br>Description from the FOM: <i>The distance from the reference location along the X axis.</i>
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      *
      * @return bodyXDistance
      */
      LIBAPI float & getBodyXDistance() {
         return bodyXDistance;
      }

      /**
      * Function to get bodyYDistance.
      * <br>Description from the FOM: <i>The distance from the reference location along the Y axis.</i>
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      *
      * @return bodyYDistance
      */
      LIBAPI float & getBodyYDistance() {
         return bodyYDistance;
      }

      /**
      * Function to get bodyZDistance.
      * <br>Description from the FOM: <i>The distance from the reference location along the Z axis.</i>
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      *
      * @return bodyZDistance
      */
      LIBAPI float & getBodyZDistance() {
         return bodyZDistance;
      }

   };


   LIBAPI bool operator ==(const DevStudio::RelativePositionStruct& l, const DevStudio::RelativePositionStruct& r);
   LIBAPI bool operator !=(const DevStudio::RelativePositionStruct& l, const DevStudio::RelativePositionStruct& r);
   LIBAPI bool operator <(const DevStudio::RelativePositionStruct& l, const DevStudio::RelativePositionStruct& r);
   LIBAPI bool operator >(const DevStudio::RelativePositionStruct& l, const DevStudio::RelativePositionStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::RelativePositionStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::RelativePositionStruct const &);
}
#endif
