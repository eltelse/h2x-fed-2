/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_GRIDVALUETYPE2STRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_GRIDVALUETYPE2STRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/Float32Array1Plus.h>
#include <vector>

namespace DevStudio {
   /**
   * Implementation of the <code>GridValueType2Struct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Record specifying type 2 data representation</i>
   */
   class GridValueType2Struct {

   public:
      /**
      * Description from the FOM: <i>Specifies the number of environmental state variable data values</i>.
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of Float32 elements, containing at least one element.</i>
      */
      std::vector</* not empty */ float > numberOfValuesAValues;

      LIBAPI GridValueType2Struct()
         :
         numberOfValuesAValues(0)
      {}

      /**
      * Constructor for GridValueType2Struct
      *
      * @param numberOfValuesAValues_ value to set as numberOfValuesAValues.
      * <br>Description from the FOM: <i>Specifies the number of environmental state variable data values</i>
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of Float32 elements, containing at least one element.</i>
      */
      LIBAPI GridValueType2Struct(
         std::vector</* not empty */ float > numberOfValuesAValues_
         )
         :
         numberOfValuesAValues(numberOfValuesAValues_)
      {}



      /**
      * Function to get numberOfValuesAValues.
      * <br>Description from the FOM: <i>Specifies the number of environmental state variable data values</i>
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of Float32 elements, containing at least one element.</i>
      *
      * @return numberOfValuesAValues
      */
      LIBAPI std::vector</* not empty */ float > & getNumberOfValuesAValues() {
         return numberOfValuesAValues;
      }

   };


   LIBAPI bool operator ==(const DevStudio::GridValueType2Struct& l, const DevStudio::GridValueType2Struct& r);
   LIBAPI bool operator !=(const DevStudio::GridValueType2Struct& l, const DevStudio::GridValueType2Struct& r);
   LIBAPI bool operator <(const DevStudio::GridValueType2Struct& l, const DevStudio::GridValueType2Struct& r);
   LIBAPI bool operator >(const DevStudio::GridValueType2Struct& l, const DevStudio::GridValueType2Struct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::GridValueType2Struct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::GridValueType2Struct const &);
}
#endif
