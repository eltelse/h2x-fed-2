/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_SPREADSPECTRUMENUM_H
#define DEVELOPER_STUDIO_DATATYPES_SPREADSPECTRUMENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace SpreadSpectrumEnum {
        /**
        * Implementation of the <code>SpreadSpectrumEnum16</code> data type from the FOM.
        * <br>Description from the FOM: <i>The type of spread spectrum characteristics employed by a transmitter.</i>
        */
        enum SpreadSpectrumEnum {
            /** <code>None</code> (with ordinal 0) */
            NONE = 0,
            /** <code>SINCGARSFrequencyHop</code> (with ordinal 1) */
            SINCGARSFREQUENCY_HOP = 1
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to SpreadSpectrumEnum: static_cast<DevStudio::SpreadSpectrumEnum::SpreadSpectrumEnum>(i)
        */
        LIBAPI bool isValid(const SpreadSpectrumEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::SpreadSpectrumEnum::SpreadSpectrumEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::SpreadSpectrumEnum::SpreadSpectrumEnum const &);
}


#endif
