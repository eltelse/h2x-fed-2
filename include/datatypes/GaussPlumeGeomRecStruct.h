/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_GAUSSPLUMEGEOMRECSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_GAUSSPLUMEGEOMRECSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/OctetArray4.h>
#include <DevStudio/datatypes/OrientationStruct.h>
#include <DevStudio/datatypes/PlumeDimensionRateStruct.h>
#include <DevStudio/datatypes/PlumeDimensionStruct.h>
#include <DevStudio/datatypes/VelocityVectorStruct.h>
#include <DevStudio/datatypes/WorldLocationStruct.h>
#include <vector>

namespace DevStudio {
   /**
   * Implementation of the <code>GaussPlumeGeomRecStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Record specifying Gaussian Plume geometry record</i>
   */
   class GaussPlumeGeomRecStruct {

   public:
      /**
      * Description from the FOM: <i>Source location X, Y, Z</i>.
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      */
      WorldLocationStruct sourceLocation;
      /**
      * Description from the FOM: <i>Orientation, specified by Euler angles</i>.
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      */
      OrientationStruct orientation;
      /**
      * Description from the FOM: <i>Plume dimensions</i>.
      * <br>Description of the data type from the FOM: <i>Record specifying plume dimensions</i>
      */
      PlumeDimensionStruct plumeDimension;
      /**
      * Description from the FOM: <i>Variation of plume dimensions</i>.
      * <br>Description of the data type from the FOM: <i>Record specifying plume dimension rates</i>
      */
      PlumeDimensionRateStruct plumeDimensionRate;
      /**
      * Description from the FOM: <i>Leading edge</i>.
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      */
      float leadingEdge;
      /**
      * Description from the FOM: <i>Leading edge velocity</i>.
      * <br>Description of the data type from the FOM: <i>The rate at which the position is changing over time.</i>
      */
      VelocityVectorStruct leadingEdgeVelocity;
      /**
      * Description from the FOM: <i>Padding field</i>.
      * <br>Description of the data type from the FOM: <i>Generic array of four Octet elements.</i>
      */
      std::vector</* 4 */ char > padding;

      LIBAPI GaussPlumeGeomRecStruct()
         :
         sourceLocation(WorldLocationStruct()),
         orientation(OrientationStruct()),
         plumeDimension(PlumeDimensionStruct()),
         plumeDimensionRate(PlumeDimensionRateStruct()),
         leadingEdge(0),
         leadingEdgeVelocity(VelocityVectorStruct()),
         padding(0)
      {}

      /**
      * Constructor for GaussPlumeGeomRecStruct
      *
      * @param sourceLocation_ value to set as sourceLocation.
      * <br>Description from the FOM: <i>Source location X, Y, Z</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      * @param orientation_ value to set as orientation.
      * <br>Description from the FOM: <i>Orientation, specified by Euler angles</i>
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      * @param plumeDimension_ value to set as plumeDimension.
      * <br>Description from the FOM: <i>Plume dimensions</i>
      * <br>Description of the data type from the FOM: <i>Record specifying plume dimensions</i>
      * @param plumeDimensionRate_ value to set as plumeDimensionRate.
      * <br>Description from the FOM: <i>Variation of plume dimensions</i>
      * <br>Description of the data type from the FOM: <i>Record specifying plume dimension rates</i>
      * @param leadingEdge_ value to set as leadingEdge.
      * <br>Description from the FOM: <i>Leading edge</i>
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      * @param leadingEdgeVelocity_ value to set as leadingEdgeVelocity.
      * <br>Description from the FOM: <i>Leading edge velocity</i>
      * <br>Description of the data type from the FOM: <i>The rate at which the position is changing over time.</i>
      * @param padding_ value to set as padding.
      * <br>Description from the FOM: <i>Padding field</i>
      * <br>Description of the data type from the FOM: <i>Generic array of four Octet elements.</i>
      */
      LIBAPI GaussPlumeGeomRecStruct(
         WorldLocationStruct sourceLocation_,
         OrientationStruct orientation_,
         PlumeDimensionStruct plumeDimension_,
         PlumeDimensionRateStruct plumeDimensionRate_,
         float leadingEdge_,
         VelocityVectorStruct leadingEdgeVelocity_,
         std::vector</* 4 */ char > padding_
         )
         :
         sourceLocation(sourceLocation_),
         orientation(orientation_),
         plumeDimension(plumeDimension_),
         plumeDimensionRate(plumeDimensionRate_),
         leadingEdge(leadingEdge_),
         leadingEdgeVelocity(leadingEdgeVelocity_),
         padding(padding_)
      {}



      /**
      * Function to get sourceLocation.
      * <br>Description from the FOM: <i>Source location X, Y, Z</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @return sourceLocation
      */
      LIBAPI DevStudio::WorldLocationStruct & getSourceLocation() {
         return sourceLocation;
      }

      /**
      * Function to get orientation.
      * <br>Description from the FOM: <i>Orientation, specified by Euler angles</i>
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @return orientation
      */
      LIBAPI DevStudio::OrientationStruct & getOrientation() {
         return orientation;
      }

      /**
      * Function to get plumeDimension.
      * <br>Description from the FOM: <i>Plume dimensions</i>
      * <br>Description of the data type from the FOM: <i>Record specifying plume dimensions</i>
      *
      * @return plumeDimension
      */
      LIBAPI DevStudio::PlumeDimensionStruct & getPlumeDimension() {
         return plumeDimension;
      }

      /**
      * Function to get plumeDimensionRate.
      * <br>Description from the FOM: <i>Variation of plume dimensions</i>
      * <br>Description of the data type from the FOM: <i>Record specifying plume dimension rates</i>
      *
      * @return plumeDimensionRate
      */
      LIBAPI DevStudio::PlumeDimensionRateStruct & getPlumeDimensionRate() {
         return plumeDimensionRate;
      }

      /**
      * Function to get leadingEdge.
      * <br>Description from the FOM: <i>Leading edge</i>
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      *
      * @return leadingEdge
      */
      LIBAPI float & getLeadingEdge() {
         return leadingEdge;
      }

      /**
      * Function to get leadingEdgeVelocity.
      * <br>Description from the FOM: <i>Leading edge velocity</i>
      * <br>Description of the data type from the FOM: <i>The rate at which the position is changing over time.</i>
      *
      * @return leadingEdgeVelocity
      */
      LIBAPI DevStudio::VelocityVectorStruct & getLeadingEdgeVelocity() {
         return leadingEdgeVelocity;
      }

      /**
      * Function to get padding.
      * <br>Description from the FOM: <i>Padding field</i>
      * <br>Description of the data type from the FOM: <i>Generic array of four Octet elements.</i>
      *
      * @return padding
      */
      LIBAPI std::vector</* 4 */ char > & getPadding() {
         return padding;
      }

   };


   LIBAPI bool operator ==(const DevStudio::GaussPlumeGeomRecStruct& l, const DevStudio::GaussPlumeGeomRecStruct& r);
   LIBAPI bool operator !=(const DevStudio::GaussPlumeGeomRecStruct& l, const DevStudio::GaussPlumeGeomRecStruct& r);
   LIBAPI bool operator <(const DevStudio::GaussPlumeGeomRecStruct& l, const DevStudio::GaussPlumeGeomRecStruct& r);
   LIBAPI bool operator >(const DevStudio::GaussPlumeGeomRecStruct& l, const DevStudio::GaussPlumeGeomRecStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::GaussPlumeGeomRecStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::GaussPlumeGeomRecStruct const &);
}
#endif
