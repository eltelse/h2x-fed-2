/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_FEDERATEIDENTIFIERSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_FEDERATEIDENTIFIERSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>


namespace DevStudio {
   /**
   * Implementation of the <code>FederateIdentifierStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Unique identification of the simulation application (federate) in an exercise, or a symbolic group address referencing multiple simulation applications. Based on the Simulation Address record as specified in IEEE 1278.1-1995 section 5.2.14.1.</i>
   */
   class FederateIdentifierStruct {

   public:
      /**
      * Description from the FOM: <i>Each site shall be assigned a unique site identification. No individual site shall be assigned an identification number containing NO_SITE (0) or ALL_SITES (0xFFFF). An identification number equal to ALL_SITES (0xFFFF) shall mean all sites; this may be used to initialize or start all sites. The mechanism by which Site Identification numbers are assigned is part of federation agreements.</i>.
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      unsigned short siteID;
      /**
      * Description from the FOM: <i>Each simulation application (federate) at a site shall be assigned an identification number unique within that site. No simulation application shall be assigned an identification number containing NO_APPLIC (0) or ALL_APPLIC (0xFFFF). An application identification number equal to ALL_APPLIC (0xFFFF) shall mean all applications; this may be used to start all applications within a site. One or more simulation applications may reside in a single host computer. The mechanism by which application identification numbers are assigned is part of federation agreements.</i>.
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      unsigned short applicationID;

      LIBAPI FederateIdentifierStruct()
         :
         siteID(0),
         applicationID(0)
      {}

      /**
      * Constructor for FederateIdentifierStruct
      *
      * @param siteID_ value to set as siteID.
      * <br>Description from the FOM: <i>Each site shall be assigned a unique site identification. No individual site shall be assigned an identification number containing NO_SITE (0) or ALL_SITES (0xFFFF). An identification number equal to ALL_SITES (0xFFFF) shall mean all sites; this may be used to initialize or start all sites. The mechanism by which Site Identification numbers are assigned is part of federation agreements.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param applicationID_ value to set as applicationID.
      * <br>Description from the FOM: <i>Each simulation application (federate) at a site shall be assigned an identification number unique within that site. No simulation application shall be assigned an identification number containing NO_APPLIC (0) or ALL_APPLIC (0xFFFF). An application identification number equal to ALL_APPLIC (0xFFFF) shall mean all applications; this may be used to start all applications within a site. One or more simulation applications may reside in a single host computer. The mechanism by which application identification numbers are assigned is part of federation agreements.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      LIBAPI FederateIdentifierStruct(
         unsigned short siteID_,
         unsigned short applicationID_
         )
         :
         siteID(siteID_),
         applicationID(applicationID_)
      {}



      /**
      * Function to get siteID.
      * <br>Description from the FOM: <i>Each site shall be assigned a unique site identification. No individual site shall be assigned an identification number containing NO_SITE (0) or ALL_SITES (0xFFFF). An identification number equal to ALL_SITES (0xFFFF) shall mean all sites; this may be used to initialize or start all sites. The mechanism by which Site Identification numbers are assigned is part of federation agreements.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return siteID
      */
      LIBAPI unsigned short & getSiteID() {
         return siteID;
      }

      /**
      * Function to get applicationID.
      * <br>Description from the FOM: <i>Each simulation application (federate) at a site shall be assigned an identification number unique within that site. No simulation application shall be assigned an identification number containing NO_APPLIC (0) or ALL_APPLIC (0xFFFF). An application identification number equal to ALL_APPLIC (0xFFFF) shall mean all applications; this may be used to start all applications within a site. One or more simulation applications may reside in a single host computer. The mechanism by which application identification numbers are assigned is part of federation agreements.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return applicationID
      */
      LIBAPI unsigned short & getApplicationID() {
         return applicationID;
      }

   };


   LIBAPI bool operator ==(const DevStudio::FederateIdentifierStruct& l, const DevStudio::FederateIdentifierStruct& r);
   LIBAPI bool operator !=(const DevStudio::FederateIdentifierStruct& l, const DevStudio::FederateIdentifierStruct& r);
   LIBAPI bool operator <(const DevStudio::FederateIdentifierStruct& l, const DevStudio::FederateIdentifierStruct& r);
   LIBAPI bool operator >(const DevStudio::FederateIdentifierStruct& l, const DevStudio::FederateIdentifierStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::FederateIdentifierStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::FederateIdentifierStruct const &);
}
#endif
