/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_ARTICULATEDTYPEMETRICENUM_H
#define DEVELOPER_STUDIO_DATATYPES_ARTICULATEDTYPEMETRICENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace ArticulatedTypeMetricEnum {
        /**
        * Implementation of the <code>ArticulatedTypeMetricEnum32</code> data type from the FOM.
        * <br>Description from the FOM: <i>Articulated part type metric</i>
        */
        enum ArticulatedTypeMetricEnum {
            /** <code>Position</code> (with ordinal 1) */
            POSITION = 1,
            /** <code>PositionRate</code> (with ordinal 2) */
            POSITION_RATE = 2,
            /** <code>Extension</code> (with ordinal 3) */
            EXTENSION = 3,
            /** <code>ExtensionRate</code> (with ordinal 4) */
            EXTENSION_RATE = 4,
            /** <code>X</code> (with ordinal 5) */
            X = 5,
            /** <code>XRate</code> (with ordinal 6) */
            XRATE = 6,
            /** <code>Y</code> (with ordinal 7) */
            Y = 7,
            /** <code>YRate</code> (with ordinal 8) */
            YRATE = 8,
            /** <code>Z</code> (with ordinal 9) */
            Z = 9,
            /** <code>ZRate</code> (with ordinal 10) */
            ZRATE = 10,
            /** <code>Azimuth</code> (with ordinal 11) */
            AZIMUTH = 11,
            /** <code>AzimuthRate</code> (with ordinal 12) */
            AZIMUTH_RATE = 12,
            /** <code>Elevation</code> (with ordinal 13) */
            ELEVATION = 13,
            /** <code>ElevationRate</code> (with ordinal 14) */
            ELEVATION_RATE = 14,
            /** <code>Rotation</code> (with ordinal 15) */
            ROTATION = 15,
            /** <code>RotationRate</code> (with ordinal 16) */
            ROTATION_RATE = 16
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to ArticulatedTypeMetricEnum: static_cast<DevStudio::ArticulatedTypeMetricEnum::ArticulatedTypeMetricEnum>(i)
        */
        LIBAPI bool isValid(const ArticulatedTypeMetricEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::ArticulatedTypeMetricEnum::ArticulatedTypeMetricEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::ArticulatedTypeMetricEnum::ArticulatedTypeMetricEnum const &);
}


#endif
