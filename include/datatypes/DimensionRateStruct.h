/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_DIMENSIONRATESTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_DIMENSIONRATESTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>


namespace DevStudio {
   /**
   * Implementation of the <code>DimensionRateStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Record specifying length X, Y, Z rates</i>
   */
   class DimensionRateStruct {

   public:
      /**
      * Description from the FOM: <i>Variation of X axis length</i>.
      * <br>Description of the data type from the FOM: <i>Speed/Velocity in meter per second. [unit: meter per second (m/s), resolution: NA, accuracy: perfect]</i>
      */
      float xRate;
      /**
      * Description from the FOM: <i>Variation of Y axis length</i>.
      * <br>Description of the data type from the FOM: <i>Speed/Velocity in meter per second. [unit: meter per second (m/s), resolution: NA, accuracy: perfect]</i>
      */
      float yRate;
      /**
      * Description from the FOM: <i>Variation of Z axis length</i>.
      * <br>Description of the data type from the FOM: <i>Speed/Velocity in meter per second. [unit: meter per second (m/s), resolution: NA, accuracy: perfect]</i>
      */
      float zRate;

      LIBAPI DimensionRateStruct()
         :
         xRate(0),
         yRate(0),
         zRate(0)
      {}

      /**
      * Constructor for DimensionRateStruct
      *
      * @param xRate_ value to set as xRate.
      * <br>Description from the FOM: <i>Variation of X axis length</i>
      * <br>Description of the data type from the FOM: <i>Speed/Velocity in meter per second. [unit: meter per second (m/s), resolution: NA, accuracy: perfect]</i>
      * @param yRate_ value to set as yRate.
      * <br>Description from the FOM: <i>Variation of Y axis length</i>
      * <br>Description of the data type from the FOM: <i>Speed/Velocity in meter per second. [unit: meter per second (m/s), resolution: NA, accuracy: perfect]</i>
      * @param zRate_ value to set as zRate.
      * <br>Description from the FOM: <i>Variation of Z axis length</i>
      * <br>Description of the data type from the FOM: <i>Speed/Velocity in meter per second. [unit: meter per second (m/s), resolution: NA, accuracy: perfect]</i>
      */
      LIBAPI DimensionRateStruct(
         float xRate_,
         float yRate_,
         float zRate_
         )
         :
         xRate(xRate_),
         yRate(yRate_),
         zRate(zRate_)
      {}



      /**
      * Function to get xRate.
      * <br>Description from the FOM: <i>Variation of X axis length</i>
      * <br>Description of the data type from the FOM: <i>Speed/Velocity in meter per second. [unit: meter per second (m/s), resolution: NA, accuracy: perfect]</i>
      *
      * @return xRate
      */
      LIBAPI float & getXRate() {
         return xRate;
      }

      /**
      * Function to get yRate.
      * <br>Description from the FOM: <i>Variation of Y axis length</i>
      * <br>Description of the data type from the FOM: <i>Speed/Velocity in meter per second. [unit: meter per second (m/s), resolution: NA, accuracy: perfect]</i>
      *
      * @return yRate
      */
      LIBAPI float & getYRate() {
         return yRate;
      }

      /**
      * Function to get zRate.
      * <br>Description from the FOM: <i>Variation of Z axis length</i>
      * <br>Description of the data type from the FOM: <i>Speed/Velocity in meter per second. [unit: meter per second (m/s), resolution: NA, accuracy: perfect]</i>
      *
      * @return zRate
      */
      LIBAPI float & getZRate() {
         return zRate;
      }

   };


   LIBAPI bool operator ==(const DevStudio::DimensionRateStruct& l, const DevStudio::DimensionRateStruct& r);
   LIBAPI bool operator !=(const DevStudio::DimensionRateStruct& l, const DevStudio::DimensionRateStruct& r);
   LIBAPI bool operator <(const DevStudio::DimensionRateStruct& l, const DevStudio::DimensionRateStruct& r);
   LIBAPI bool operator >(const DevStudio::DimensionRateStruct& l, const DevStudio::DimensionRateStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::DimensionRateStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::DimensionRateStruct const &);
}
#endif
