/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_LINE2GEOMRECSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_LINE2GEOMRECSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/VelocityVectorStruct.h>
#include <DevStudio/datatypes/WorldLocationStruct.h>

namespace DevStudio {
   /**
   * Implementation of the <code>Line2GeomRecStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Record specifying Line 2 geometry record</i>
   */
   class Line2GeomRecStruct {

   public:
      /**
      * Description from the FOM: <i>Start point location</i>.
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      */
      WorldLocationStruct startPointLocation;
      /**
      * Description from the FOM: <i>End point location</i>.
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      */
      WorldLocationStruct endPointLocation;
      /**
      * Description from the FOM: <i>Start point velocity</i>.
      * <br>Description of the data type from the FOM: <i>The rate at which the position is changing over time.</i>
      */
      VelocityVectorStruct startPointVelocity;
      /**
      * Description from the FOM: <i>End point velocity</i>.
      * <br>Description of the data type from the FOM: <i>The rate at which the position is changing over time.</i>
      */
      VelocityVectorStruct endPointVelocity;

      LIBAPI Line2GeomRecStruct()
         :
         startPointLocation(WorldLocationStruct()),
         endPointLocation(WorldLocationStruct()),
         startPointVelocity(VelocityVectorStruct()),
         endPointVelocity(VelocityVectorStruct())
      {}

      /**
      * Constructor for Line2GeomRecStruct
      *
      * @param startPointLocation_ value to set as startPointLocation.
      * <br>Description from the FOM: <i>Start point location</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      * @param endPointLocation_ value to set as endPointLocation.
      * <br>Description from the FOM: <i>End point location</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      * @param startPointVelocity_ value to set as startPointVelocity.
      * <br>Description from the FOM: <i>Start point velocity</i>
      * <br>Description of the data type from the FOM: <i>The rate at which the position is changing over time.</i>
      * @param endPointVelocity_ value to set as endPointVelocity.
      * <br>Description from the FOM: <i>End point velocity</i>
      * <br>Description of the data type from the FOM: <i>The rate at which the position is changing over time.</i>
      */
      LIBAPI Line2GeomRecStruct(
         WorldLocationStruct startPointLocation_,
         WorldLocationStruct endPointLocation_,
         VelocityVectorStruct startPointVelocity_,
         VelocityVectorStruct endPointVelocity_
         )
         :
         startPointLocation(startPointLocation_),
         endPointLocation(endPointLocation_),
         startPointVelocity(startPointVelocity_),
         endPointVelocity(endPointVelocity_)
      {}



      /**
      * Function to get startPointLocation.
      * <br>Description from the FOM: <i>Start point location</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @return startPointLocation
      */
      LIBAPI DevStudio::WorldLocationStruct & getStartPointLocation() {
         return startPointLocation;
      }

      /**
      * Function to get endPointLocation.
      * <br>Description from the FOM: <i>End point location</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @return endPointLocation
      */
      LIBAPI DevStudio::WorldLocationStruct & getEndPointLocation() {
         return endPointLocation;
      }

      /**
      * Function to get startPointVelocity.
      * <br>Description from the FOM: <i>Start point velocity</i>
      * <br>Description of the data type from the FOM: <i>The rate at which the position is changing over time.</i>
      *
      * @return startPointVelocity
      */
      LIBAPI DevStudio::VelocityVectorStruct & getStartPointVelocity() {
         return startPointVelocity;
      }

      /**
      * Function to get endPointVelocity.
      * <br>Description from the FOM: <i>End point velocity</i>
      * <br>Description of the data type from the FOM: <i>The rate at which the position is changing over time.</i>
      *
      * @return endPointVelocity
      */
      LIBAPI DevStudio::VelocityVectorStruct & getEndPointVelocity() {
         return endPointVelocity;
      }

   };


   LIBAPI bool operator ==(const DevStudio::Line2GeomRecStruct& l, const DevStudio::Line2GeomRecStruct& r);
   LIBAPI bool operator !=(const DevStudio::Line2GeomRecStruct& l, const DevStudio::Line2GeomRecStruct& r);
   LIBAPI bool operator <(const DevStudio::Line2GeomRecStruct& l, const DevStudio::Line2GeomRecStruct& r);
   LIBAPI bool operator >(const DevStudio::Line2GeomRecStruct& l, const DevStudio::Line2GeomRecStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::Line2GeomRecStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::Line2GeomRecStruct const &);
}
#endif
