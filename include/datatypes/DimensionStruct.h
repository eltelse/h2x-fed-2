/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_DIMENSIONSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_DIMENSIONSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>


namespace DevStudio {
   /**
   * Implementation of the <code>DimensionStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Bounding box in X,Y,Z axis.</i>
   */
   class DimensionStruct {

   public:
      /**
      * Description from the FOM: <i>Length in meters along X axis.</i>.
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      */
      float xAxisLength;
      /**
      * Description from the FOM: <i>Length in meters along Y axis.</i>.
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      */
      float yAxisLength;
      /**
      * Description from the FOM: <i>Length in meters along Z axis.</i>.
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      */
      float zAxisLength;

      LIBAPI DimensionStruct()
         :
         xAxisLength(0),
         yAxisLength(0),
         zAxisLength(0)
      {}

      /**
      * Constructor for DimensionStruct
      *
      * @param xAxisLength_ value to set as xAxisLength.
      * <br>Description from the FOM: <i>Length in meters along X axis.</i>
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      * @param yAxisLength_ value to set as yAxisLength.
      * <br>Description from the FOM: <i>Length in meters along Y axis.</i>
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      * @param zAxisLength_ value to set as zAxisLength.
      * <br>Description from the FOM: <i>Length in meters along Z axis.</i>
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      */
      LIBAPI DimensionStruct(
         float xAxisLength_,
         float yAxisLength_,
         float zAxisLength_
         )
         :
         xAxisLength(xAxisLength_),
         yAxisLength(yAxisLength_),
         zAxisLength(zAxisLength_)
      {}



      /**
      * Function to get xAxisLength.
      * <br>Description from the FOM: <i>Length in meters along X axis.</i>
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      *
      * @return xAxisLength
      */
      LIBAPI float & getXAxisLength() {
         return xAxisLength;
      }

      /**
      * Function to get yAxisLength.
      * <br>Description from the FOM: <i>Length in meters along Y axis.</i>
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      *
      * @return yAxisLength
      */
      LIBAPI float & getYAxisLength() {
         return yAxisLength;
      }

      /**
      * Function to get zAxisLength.
      * <br>Description from the FOM: <i>Length in meters along Z axis.</i>
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      *
      * @return zAxisLength
      */
      LIBAPI float & getZAxisLength() {
         return zAxisLength;
      }

   };


   LIBAPI bool operator ==(const DevStudio::DimensionStruct& l, const DevStudio::DimensionStruct& r);
   LIBAPI bool operator !=(const DevStudio::DimensionStruct& l, const DevStudio::DimensionStruct& r);
   LIBAPI bool operator <(const DevStudio::DimensionStruct& l, const DevStudio::DimensionStruct& r);
   LIBAPI bool operator >(const DevStudio::DimensionStruct& l, const DevStudio::DimensionStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::DimensionStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::DimensionStruct const &);
}
#endif
