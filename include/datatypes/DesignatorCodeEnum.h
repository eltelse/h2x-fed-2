/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_DESIGNATORCODEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_DESIGNATORCODEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace DesignatorCodeEnum {
        /**
        * Implementation of the <code>DesignatorCodeEnum16</code> data type from the FOM.
        * <br>Description from the FOM: <i>Designator code</i>
        */
        enum DesignatorCodeEnum {
            /** <code>Other</code> (with ordinal 0) */
            OTHER = 0
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to DesignatorCodeEnum: static_cast<DevStudio::DesignatorCodeEnum::DesignatorCodeEnum>(i)
        */
        LIBAPI bool isValid(const DesignatorCodeEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::DesignatorCodeEnum::DesignatorCodeEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::DesignatorCodeEnum::DesignatorCodeEnum const &);
}


#endif
