/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_PARAMETERVALUEVARIANTSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_PARAMETERVALUEVARIANTSTRUCT_H

#include <DevStudio/datatypes/ArticulatedPartsStruct.h>
#include <DevStudio/datatypes/AttachedPartsStruct.h>
#include <DevStudio/datatypes/ParameterTypeEnum.h>

#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
   /**
   * Implementation of the <code>ParameterValueVariantStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Variant record specifying the type of articulation parameter (articulated or attached part), and its type and value.</i>
   */
   class ParameterValueVariantStruct {

   public:
      LIBAPI ParameterValueVariantStruct()
         : 
         _discriminant(DevStudio::ParameterTypeEnum::ParameterTypeEnum())
      {}

      /** 
      * Create a new alternative ParameterValueVariantStruct, with <code>ArticulatedPart</code> as discriminant.
      *
      * @param articulatedParts value of the ArticulatedParts field
      *
      * @return a new ParameterValueVariantStruct
      */
      LIBAPI static ParameterValueVariantStruct createArticulatedParts(DevStudio::ArticulatedPartsStruct articulatedParts);

      /** 
      * Create a new alternative ParameterValueVariantStruct, with <code>AttachedPart</code> as discriminant.
      *
      * @param attachedParts value of the AttachedParts field
      *
      * @return a new ParameterValueVariantStruct
      */
      LIBAPI static ParameterValueVariantStruct createAttachedParts(DevStudio::AttachedPartsStruct attachedParts);

      /**
      * Create a new alternative ParameterValueVariantStruct, with <code>Separation</code> as discriminant.
      *
      * @return a new ParameterValueVariantStruct
      */
      LIBAPI static ParameterValueVariantStruct createSeparation();

      /**
      * Create a new alternative ParameterValueVariantStruct, with <code>EntityType</code> as discriminant.
      *
      * @return a new ParameterValueVariantStruct
      */
      LIBAPI static ParameterValueVariantStruct createEntityType();

      /**
      * Create a new alternative ParameterValueVariantStruct, with <code>EntityAssociation</code> as discriminant.
      *
      * @return a new ParameterValueVariantStruct
      */
      LIBAPI static ParameterValueVariantStruct createEntityAssociation();



      /**
      * Function to get discriminant
      *
      * @return disciminant
      */
      LIBAPI DevStudio::ParameterTypeEnum::ParameterTypeEnum getDiscriminant() const {
          return _discriminant;
      }

      /**
      * Function to get ArticulatedParts.
      * Note that this field is only valid of the discriminant is <code>ArticulatedPart</code>.
      * <br>Description from the FOM: <i>Alternative for an articulated part.</i>
      * <br>Description of the data type from the FOM: <i>Structure to represent the state of a movable part of an entity.</i>
      *
      * @return ArticulatedParts value
      */
      LIBAPI DevStudio::ArticulatedPartsStructPtr getArticulatedParts() const {
         return _articulatedParts;
      }

      /**
      * Function to set ArticulatedParts.
      * Note that this will set the discriminant to <code>ArticulatedPart</code>.
      * <br>Description from the FOM: <i>Alternative for an articulated part.</i>
      * <br>Description of the data type from the FOM: <i>Structure to represent the state of a movable part of an entity.</i>
      *
      * @param articulatedParts value used to create a ArticulatedPartsStructPtr
      */
      LIBAPI void setArticulatedParts(const DevStudio::ArticulatedPartsStruct articulatedParts) {
         _articulatedParts = DevStudio::ArticulatedPartsStructPtr( new DevStudio::ArticulatedPartsStruct (articulatedParts));
         _discriminant = ParameterTypeEnum::ARTICULATED_PART;
      }

      /**
      * Function to get AttachedParts.
      * Note that this field is only valid of the discriminant is <code>AttachedPart</code>.
      * <br>Description from the FOM: <i>Alternative for an attached part.</i>
      * <br>Description of the data type from the FOM: <i>Structure to represent removable parts that may be attached to an entity.</i>
      *
      * @return AttachedParts value
      */
      LIBAPI DevStudio::AttachedPartsStructPtr getAttachedParts() const {
         return _attachedParts;
      }

      /**
      * Function to set AttachedParts.
      * Note that this will set the discriminant to <code>AttachedPart</code>.
      * <br>Description from the FOM: <i>Alternative for an attached part.</i>
      * <br>Description of the data type from the FOM: <i>Structure to represent removable parts that may be attached to an entity.</i>
      *
      * @param attachedParts value used to create a AttachedPartsStructPtr
      */
      LIBAPI void setAttachedParts(const DevStudio::AttachedPartsStruct attachedParts) {
         _attachedParts = DevStudio::AttachedPartsStructPtr( new DevStudio::AttachedPartsStruct (attachedParts));
         _discriminant = ParameterTypeEnum::ATTACHED_PART;
      }

      /** Description from the FOM: <i>Alternative for an articulated part.</i> */
      DevStudio::ArticulatedPartsStructPtr _articulatedParts;
      /** Description from the FOM: <i>Alternative for an attached part.</i> */
      DevStudio::AttachedPartsStructPtr _attachedParts;

   private:
      DevStudio::ParameterTypeEnum::ParameterTypeEnum _discriminant;

      ParameterValueVariantStruct(
         DevStudio::ArticulatedPartsStructPtr articulatedParts,
         DevStudio::AttachedPartsStructPtr attachedParts,
         DevStudio::ParameterTypeEnum::ParameterTypeEnum discriminant
      );
   };

   LIBAPI bool operator ==(const DevStudio::ParameterValueVariantStruct& l, const DevStudio::ParameterValueVariantStruct& r);
   LIBAPI bool operator !=(const DevStudio::ParameterValueVariantStruct& l, const DevStudio::ParameterValueVariantStruct& r);
   LIBAPI bool operator <(const DevStudio::ParameterValueVariantStruct& l, const DevStudio::ParameterValueVariantStruct& r);
   LIBAPI bool operator >(const DevStudio::ParameterValueVariantStruct& l, const DevStudio::ParameterValueVariantStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::ParameterValueVariantStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::ParameterValueVariantStruct const &);
}

#endif
