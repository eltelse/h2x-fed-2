/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_HATCHSTATEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_HATCHSTATEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace HatchStateEnum {
        /**
        * Implementation of the <code>HatchStateEnum32</code> data type from the FOM.
        * <br>Description from the FOM: <i>Hatch state</i>
        */
        enum HatchStateEnum {
            /** <code>NotApplicable</code> (with ordinal 0) */
            NOT_APPLICABLE = 0,
            /** <code>PrimaryHatchIsClosed</code> (with ordinal 1) */
            PRIMARY_HATCH_IS_CLOSED = 1,
            /** <code>PrimaryHatchIsPopped</code> (with ordinal 2) */
            PRIMARY_HATCH_IS_POPPED = 2,
            /** <code>PrimaryHatchIsPoppedAndPersonIsVisibleUnderHatch</code> (with ordinal 3) */
            PRIMARY_HATCH_IS_POPPED_AND_PERSON_IS_VISIBLE_UNDER_HATCH = 3,
            /** <code>PrimaryHatchIsOpen</code> (with ordinal 4) */
            PRIMARY_HATCH_IS_OPEN = 4,
            /** <code>PrimaryHatchIsOpenAndPersonIsVisible</code> (with ordinal 5) */
            PRIMARY_HATCH_IS_OPEN_AND_PERSON_IS_VISIBLE = 5
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to HatchStateEnum: static_cast<DevStudio::HatchStateEnum::HatchStateEnum>(i)
        */
        LIBAPI bool isValid(const HatchStateEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::HatchStateEnum::HatchStateEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::HatchStateEnum::HatchStateEnum const &);
}


#endif
