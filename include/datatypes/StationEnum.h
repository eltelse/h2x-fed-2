/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_STATIONENUM_H
#define DEVELOPER_STUDIO_DATATYPES_STATIONENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace StationEnum {
        /**
        * Implementation of the <code>StationEnum32</code> data type from the FOM.
        * <br>Description from the FOM: <i>Attached part station</i>
        */
        enum StationEnum {
            /** <code>Nothing_Empty</code> (with ordinal 0) */
            NOTHING_EMPTY = 0,
            /** <code>Fuselage_Station1</code> (with ordinal 512) */
            FUSELAGE_STATION1 = 512,
            /** <code>Fuselage_Station2</code> (with ordinal 513) */
            FUSELAGE_STATION2 = 513,
            /** <code>Fuselage_Station3</code> (with ordinal 514) */
            FUSELAGE_STATION3 = 514,
            /** <code>Fuselage_Station4</code> (with ordinal 515) */
            FUSELAGE_STATION4 = 515,
            /** <code>Fuselage_Station5</code> (with ordinal 516) */
            FUSELAGE_STATION5 = 516,
            /** <code>Fuselage_Station6</code> (with ordinal 517) */
            FUSELAGE_STATION6 = 517,
            /** <code>Fuselage_Station7</code> (with ordinal 518) */
            FUSELAGE_STATION7 = 518,
            /** <code>Fuselage_Station8</code> (with ordinal 519) */
            FUSELAGE_STATION8 = 519,
            /** <code>Fuselage_Station9</code> (with ordinal 520) */
            FUSELAGE_STATION9 = 520,
            /** <code>Fuselage_Station10</code> (with ordinal 521) */
            FUSELAGE_STATION10 = 521,
            /** <code>Fuselage_Station11</code> (with ordinal 522) */
            FUSELAGE_STATION11 = 522,
            /** <code>Fuselage_Station12</code> (with ordinal 523) */
            FUSELAGE_STATION12 = 523,
            /** <code>Fuselage_Station13</code> (with ordinal 524) */
            FUSELAGE_STATION13 = 524,
            /** <code>Fuselage_Station14</code> (with ordinal 525) */
            FUSELAGE_STATION14 = 525,
            /** <code>Fuselage_Station15</code> (with ordinal 526) */
            FUSELAGE_STATION15 = 526,
            /** <code>Fuselage_Station16</code> (with ordinal 527) */
            FUSELAGE_STATION16 = 527,
            /** <code>Fuselage_Station17</code> (with ordinal 528) */
            FUSELAGE_STATION17 = 528,
            /** <code>Fuselage_Station18</code> (with ordinal 529) */
            FUSELAGE_STATION18 = 529,
            /** <code>Fuselage_Station19</code> (with ordinal 530) */
            FUSELAGE_STATION19 = 530,
            /** <code>Fuselage_Station20</code> (with ordinal 531) */
            FUSELAGE_STATION20 = 531,
            /** <code>Fuselage_Station21</code> (with ordinal 532) */
            FUSELAGE_STATION21 = 532,
            /** <code>Fuselage_Station22</code> (with ordinal 533) */
            FUSELAGE_STATION22 = 533,
            /** <code>Fuselage_Station23</code> (with ordinal 534) */
            FUSELAGE_STATION23 = 534,
            /** <code>Fuselage_Station24</code> (with ordinal 535) */
            FUSELAGE_STATION24 = 535,
            /** <code>Fuselage_Station25</code> (with ordinal 536) */
            FUSELAGE_STATION25 = 536,
            /** <code>Fuselage_Station26</code> (with ordinal 537) */
            FUSELAGE_STATION26 = 537,
            /** <code>Fuselage_Station27</code> (with ordinal 538) */
            FUSELAGE_STATION27 = 538,
            /** <code>Fuselage_Station28</code> (with ordinal 539) */
            FUSELAGE_STATION28 = 539,
            /** <code>Fuselage_Station29</code> (with ordinal 540) */
            FUSELAGE_STATION29 = 540,
            /** <code>Fuselage_Station30</code> (with ordinal 541) */
            FUSELAGE_STATION30 = 541,
            /** <code>Fuselage_Station31</code> (with ordinal 542) */
            FUSELAGE_STATION31 = 542,
            /** <code>Fuselage_Station32</code> (with ordinal 543) */
            FUSELAGE_STATION32 = 543,
            /** <code>Fuselage_Station33</code> (with ordinal 544) */
            FUSELAGE_STATION33 = 544,
            /** <code>Fuselage_Station34</code> (with ordinal 545) */
            FUSELAGE_STATION34 = 545,
            /** <code>Fuselage_Station35</code> (with ordinal 546) */
            FUSELAGE_STATION35 = 546,
            /** <code>Fuselage_Station36</code> (with ordinal 547) */
            FUSELAGE_STATION36 = 547,
            /** <code>Fuselage_Station37</code> (with ordinal 548) */
            FUSELAGE_STATION37 = 548,
            /** <code>Fuselage_Station38</code> (with ordinal 549) */
            FUSELAGE_STATION38 = 549,
            /** <code>Fuselage_Station39</code> (with ordinal 550) */
            FUSELAGE_STATION39 = 550,
            /** <code>Fuselage_Station40</code> (with ordinal 551) */
            FUSELAGE_STATION40 = 551,
            /** <code>Fuselage_Station41</code> (with ordinal 552) */
            FUSELAGE_STATION41 = 552,
            /** <code>Fuselage_Station42</code> (with ordinal 553) */
            FUSELAGE_STATION42 = 553,
            /** <code>Fuselage_Station43</code> (with ordinal 554) */
            FUSELAGE_STATION43 = 554,
            /** <code>Fuselage_Station44</code> (with ordinal 555) */
            FUSELAGE_STATION44 = 555,
            /** <code>Fuselage_Station45</code> (with ordinal 556) */
            FUSELAGE_STATION45 = 556,
            /** <code>Fuselage_Station46</code> (with ordinal 557) */
            FUSELAGE_STATION46 = 557,
            /** <code>Fuselage_Station47</code> (with ordinal 558) */
            FUSELAGE_STATION47 = 558,
            /** <code>Fuselage_Station48</code> (with ordinal 559) */
            FUSELAGE_STATION48 = 559,
            /** <code>Fuselage_Station49</code> (with ordinal 560) */
            FUSELAGE_STATION49 = 560,
            /** <code>Fuselage_Station50</code> (with ordinal 561) */
            FUSELAGE_STATION50 = 561,
            /** <code>Fuselage_Station51</code> (with ordinal 562) */
            FUSELAGE_STATION51 = 562,
            /** <code>Fuselage_Station52</code> (with ordinal 563) */
            FUSELAGE_STATION52 = 563,
            /** <code>Fuselage_Station53</code> (with ordinal 564) */
            FUSELAGE_STATION53 = 564,
            /** <code>Fuselage_Station54</code> (with ordinal 565) */
            FUSELAGE_STATION54 = 565,
            /** <code>Fuselage_Station55</code> (with ordinal 566) */
            FUSELAGE_STATION55 = 566,
            /** <code>Fuselage_Station56</code> (with ordinal 567) */
            FUSELAGE_STATION56 = 567,
            /** <code>Fuselage_Station57</code> (with ordinal 568) */
            FUSELAGE_STATION57 = 568,
            /** <code>Fuselage_Station58</code> (with ordinal 569) */
            FUSELAGE_STATION58 = 569,
            /** <code>Fuselage_Station59</code> (with ordinal 570) */
            FUSELAGE_STATION59 = 570,
            /** <code>Fuselage_Station60</code> (with ordinal 571) */
            FUSELAGE_STATION60 = 571,
            /** <code>Fuselage_Station61</code> (with ordinal 572) */
            FUSELAGE_STATION61 = 572,
            /** <code>Fuselage_Station62</code> (with ordinal 573) */
            FUSELAGE_STATION62 = 573,
            /** <code>Fuselage_Station63</code> (with ordinal 574) */
            FUSELAGE_STATION63 = 574,
            /** <code>Fuselage_Station64</code> (with ordinal 575) */
            FUSELAGE_STATION64 = 575,
            /** <code>Fuselage_Station65</code> (with ordinal 576) */
            FUSELAGE_STATION65 = 576,
            /** <code>Fuselage_Station66</code> (with ordinal 577) */
            FUSELAGE_STATION66 = 577,
            /** <code>Fuselage_Station67</code> (with ordinal 578) */
            FUSELAGE_STATION67 = 578,
            /** <code>Fuselage_Station68</code> (with ordinal 579) */
            FUSELAGE_STATION68 = 579,
            /** <code>Fuselage_Station69</code> (with ordinal 580) */
            FUSELAGE_STATION69 = 580,
            /** <code>Fuselage_Station70</code> (with ordinal 581) */
            FUSELAGE_STATION70 = 581,
            /** <code>Fuselage_Station71</code> (with ordinal 582) */
            FUSELAGE_STATION71 = 582,
            /** <code>Fuselage_Station72</code> (with ordinal 583) */
            FUSELAGE_STATION72 = 583,
            /** <code>Fuselage_Station73</code> (with ordinal 584) */
            FUSELAGE_STATION73 = 584,
            /** <code>Fuselage_Station74</code> (with ordinal 585) */
            FUSELAGE_STATION74 = 585,
            /** <code>Fuselage_Station75</code> (with ordinal 586) */
            FUSELAGE_STATION75 = 586,
            /** <code>Fuselage_Station76</code> (with ordinal 587) */
            FUSELAGE_STATION76 = 587,
            /** <code>Fuselage_Station77</code> (with ordinal 588) */
            FUSELAGE_STATION77 = 588,
            /** <code>Fuselage_Station78</code> (with ordinal 589) */
            FUSELAGE_STATION78 = 589,
            /** <code>Fuselage_Station79</code> (with ordinal 590) */
            FUSELAGE_STATION79 = 590,
            /** <code>Fuselage_Station80</code> (with ordinal 591) */
            FUSELAGE_STATION80 = 591,
            /** <code>Fuselage_Station81</code> (with ordinal 592) */
            FUSELAGE_STATION81 = 592,
            /** <code>Fuselage_Station82</code> (with ordinal 593) */
            FUSELAGE_STATION82 = 593,
            /** <code>Fuselage_Station83</code> (with ordinal 594) */
            FUSELAGE_STATION83 = 594,
            /** <code>Fuselage_Station84</code> (with ordinal 595) */
            FUSELAGE_STATION84 = 595,
            /** <code>Fuselage_Station85</code> (with ordinal 596) */
            FUSELAGE_STATION85 = 596,
            /** <code>Fuselage_Station86</code> (with ordinal 597) */
            FUSELAGE_STATION86 = 597,
            /** <code>Fuselage_Station87</code> (with ordinal 598) */
            FUSELAGE_STATION87 = 598,
            /** <code>Fuselage_Station88</code> (with ordinal 599) */
            FUSELAGE_STATION88 = 599,
            /** <code>Fuselage_Station89</code> (with ordinal 600) */
            FUSELAGE_STATION89 = 600,
            /** <code>Fuselage_Station90</code> (with ordinal 601) */
            FUSELAGE_STATION90 = 601,
            /** <code>Fuselage_Station91</code> (with ordinal 602) */
            FUSELAGE_STATION91 = 602,
            /** <code>Fuselage_Station92</code> (with ordinal 603) */
            FUSELAGE_STATION92 = 603,
            /** <code>Fuselage_Station93</code> (with ordinal 604) */
            FUSELAGE_STATION93 = 604,
            /** <code>Fuselage_Station94</code> (with ordinal 605) */
            FUSELAGE_STATION94 = 605,
            /** <code>Fuselage_Station95</code> (with ordinal 606) */
            FUSELAGE_STATION95 = 606,
            /** <code>Fuselage_Station96</code> (with ordinal 607) */
            FUSELAGE_STATION96 = 607,
            /** <code>Fuselage_Station97</code> (with ordinal 608) */
            FUSELAGE_STATION97 = 608,
            /** <code>Fuselage_Station98</code> (with ordinal 609) */
            FUSELAGE_STATION98 = 609,
            /** <code>Fuselage_Station99</code> (with ordinal 610) */
            FUSELAGE_STATION99 = 610,
            /** <code>Fuselage_Station100</code> (with ordinal 611) */
            FUSELAGE_STATION100 = 611,
            /** <code>Fuselage_Station101</code> (with ordinal 612) */
            FUSELAGE_STATION101 = 612,
            /** <code>Fuselage_Station102</code> (with ordinal 613) */
            FUSELAGE_STATION102 = 613,
            /** <code>Fuselage_Station103</code> (with ordinal 614) */
            FUSELAGE_STATION103 = 614,
            /** <code>Fuselage_Station104</code> (with ordinal 615) */
            FUSELAGE_STATION104 = 615,
            /** <code>Fuselage_Station105</code> (with ordinal 616) */
            FUSELAGE_STATION105 = 616,
            /** <code>Fuselage_Station106</code> (with ordinal 617) */
            FUSELAGE_STATION106 = 617,
            /** <code>Fuselage_Station107</code> (with ordinal 618) */
            FUSELAGE_STATION107 = 618,
            /** <code>Fuselage_Station108</code> (with ordinal 619) */
            FUSELAGE_STATION108 = 619,
            /** <code>Fuselage_Station109</code> (with ordinal 620) */
            FUSELAGE_STATION109 = 620,
            /** <code>Fuselage_Station110</code> (with ordinal 621) */
            FUSELAGE_STATION110 = 621,
            /** <code>Fuselage_Station111</code> (with ordinal 622) */
            FUSELAGE_STATION111 = 622,
            /** <code>Fuselage_Station112</code> (with ordinal 623) */
            FUSELAGE_STATION112 = 623,
            /** <code>Fuselage_Station113</code> (with ordinal 624) */
            FUSELAGE_STATION113 = 624,
            /** <code>Fuselage_Station114</code> (with ordinal 625) */
            FUSELAGE_STATION114 = 625,
            /** <code>Fuselage_Station115</code> (with ordinal 626) */
            FUSELAGE_STATION115 = 626,
            /** <code>Fuselage_Station116</code> (with ordinal 627) */
            FUSELAGE_STATION116 = 627,
            /** <code>Fuselage_Station117</code> (with ordinal 628) */
            FUSELAGE_STATION117 = 628,
            /** <code>Fuselage_Station118</code> (with ordinal 629) */
            FUSELAGE_STATION118 = 629,
            /** <code>Fuselage_Station119</code> (with ordinal 630) */
            FUSELAGE_STATION119 = 630,
            /** <code>Fuselage_Station120</code> (with ordinal 631) */
            FUSELAGE_STATION120 = 631,
            /** <code>Fuselage_Station121</code> (with ordinal 632) */
            FUSELAGE_STATION121 = 632,
            /** <code>Fuselage_Station122</code> (with ordinal 633) */
            FUSELAGE_STATION122 = 633,
            /** <code>Fuselage_Station123</code> (with ordinal 634) */
            FUSELAGE_STATION123 = 634,
            /** <code>Fuselage_Station124</code> (with ordinal 635) */
            FUSELAGE_STATION124 = 635,
            /** <code>Fuselage_Station125</code> (with ordinal 636) */
            FUSELAGE_STATION125 = 636,
            /** <code>Fuselage_Station126</code> (with ordinal 637) */
            FUSELAGE_STATION126 = 637,
            /** <code>Fuselage_Station127</code> (with ordinal 638) */
            FUSELAGE_STATION127 = 638,
            /** <code>Fuselage_Station128</code> (with ordinal 639) */
            FUSELAGE_STATION128 = 639,
            /** <code>LeftWingStation1</code> (with ordinal 640) */
            LEFT_WING_STATION1 = 640,
            /** <code>LeftWingStation2</code> (with ordinal 641) */
            LEFT_WING_STATION2 = 641,
            /** <code>LeftWingStation3</code> (with ordinal 642) */
            LEFT_WING_STATION3 = 642,
            /** <code>LeftWingStation4</code> (with ordinal 643) */
            LEFT_WING_STATION4 = 643,
            /** <code>LeftWingStation5</code> (with ordinal 644) */
            LEFT_WING_STATION5 = 644,
            /** <code>LeftWingStation6</code> (with ordinal 645) */
            LEFT_WING_STATION6 = 645,
            /** <code>LeftWingStation7</code> (with ordinal 646) */
            LEFT_WING_STATION7 = 646,
            /** <code>LeftWingStation8</code> (with ordinal 647) */
            LEFT_WING_STATION8 = 647,
            /** <code>LeftWingStation9</code> (with ordinal 648) */
            LEFT_WING_STATION9 = 648,
            /** <code>LeftWingStation10</code> (with ordinal 649) */
            LEFT_WING_STATION10 = 649,
            /** <code>LeftWingStation11</code> (with ordinal 650) */
            LEFT_WING_STATION11 = 650,
            /** <code>LeftWingStation12</code> (with ordinal 651) */
            LEFT_WING_STATION12 = 651,
            /** <code>LeftWingStation13</code> (with ordinal 652) */
            LEFT_WING_STATION13 = 652,
            /** <code>LeftWingStation14</code> (with ordinal 653) */
            LEFT_WING_STATION14 = 653,
            /** <code>LeftWingStation15</code> (with ordinal 654) */
            LEFT_WING_STATION15 = 654,
            /** <code>LeftWingStation16</code> (with ordinal 655) */
            LEFT_WING_STATION16 = 655,
            /** <code>LeftWingStation17</code> (with ordinal 656) */
            LEFT_WING_STATION17 = 656,
            /** <code>LeftWingStation18</code> (with ordinal 657) */
            LEFT_WING_STATION18 = 657,
            /** <code>LeftWingStation19</code> (with ordinal 658) */
            LEFT_WING_STATION19 = 658,
            /** <code>LeftWingStation20</code> (with ordinal 659) */
            LEFT_WING_STATION20 = 659,
            /** <code>LeftWingStation21</code> (with ordinal 660) */
            LEFT_WING_STATION21 = 660,
            /** <code>LeftWingStation22</code> (with ordinal 661) */
            LEFT_WING_STATION22 = 661,
            /** <code>LeftWingStation23</code> (with ordinal 662) */
            LEFT_WING_STATION23 = 662,
            /** <code>LeftWingStation24</code> (with ordinal 663) */
            LEFT_WING_STATION24 = 663,
            /** <code>LeftWingStation25</code> (with ordinal 664) */
            LEFT_WING_STATION25 = 664,
            /** <code>LeftWingStation26</code> (with ordinal 665) */
            LEFT_WING_STATION26 = 665,
            /** <code>LeftWingStation27</code> (with ordinal 666) */
            LEFT_WING_STATION27 = 666,
            /** <code>LeftWingStation28</code> (with ordinal 667) */
            LEFT_WING_STATION28 = 667,
            /** <code>LeftWingStation29</code> (with ordinal 668) */
            LEFT_WING_STATION29 = 668,
            /** <code>LeftWingStation30</code> (with ordinal 669) */
            LEFT_WING_STATION30 = 669,
            /** <code>LeftWingStation31</code> (with ordinal 670) */
            LEFT_WING_STATION31 = 670,
            /** <code>LeftWingStation32</code> (with ordinal 671) */
            LEFT_WING_STATION32 = 671,
            /** <code>LeftWingStation33</code> (with ordinal 672) */
            LEFT_WING_STATION33 = 672,
            /** <code>LeftWingStation34</code> (with ordinal 673) */
            LEFT_WING_STATION34 = 673,
            /** <code>LeftWingStation35</code> (with ordinal 674) */
            LEFT_WING_STATION35 = 674,
            /** <code>LeftWingStation36</code> (with ordinal 675) */
            LEFT_WING_STATION36 = 675,
            /** <code>LeftWingStation37</code> (with ordinal 676) */
            LEFT_WING_STATION37 = 676,
            /** <code>LeftWingStation38</code> (with ordinal 677) */
            LEFT_WING_STATION38 = 677,
            /** <code>LeftWingStation39</code> (with ordinal 678) */
            LEFT_WING_STATION39 = 678,
            /** <code>LeftWingStation40</code> (with ordinal 679) */
            LEFT_WING_STATION40 = 679,
            /** <code>LeftWingStation41</code> (with ordinal 680) */
            LEFT_WING_STATION41 = 680,
            /** <code>LeftWingStation42</code> (with ordinal 681) */
            LEFT_WING_STATION42 = 681,
            /** <code>LeftWingStation43</code> (with ordinal 682) */
            LEFT_WING_STATION43 = 682,
            /** <code>LeftWingStation44</code> (with ordinal 683) */
            LEFT_WING_STATION44 = 683,
            /** <code>LeftWingStation45</code> (with ordinal 684) */
            LEFT_WING_STATION45 = 684,
            /** <code>LeftWingStation46</code> (with ordinal 685) */
            LEFT_WING_STATION46 = 685,
            /** <code>LeftWingStation47</code> (with ordinal 686) */
            LEFT_WING_STATION47 = 686,
            /** <code>LeftWingStation48</code> (with ordinal 687) */
            LEFT_WING_STATION48 = 687,
            /** <code>LeftWingStation49</code> (with ordinal 688) */
            LEFT_WING_STATION49 = 688,
            /** <code>LeftWingStation50</code> (with ordinal 689) */
            LEFT_WING_STATION50 = 689,
            /** <code>LeftWingStation51</code> (with ordinal 690) */
            LEFT_WING_STATION51 = 690,
            /** <code>LeftWingStation52</code> (with ordinal 691) */
            LEFT_WING_STATION52 = 691,
            /** <code>LeftWingStation53</code> (with ordinal 692) */
            LEFT_WING_STATION53 = 692,
            /** <code>LeftWingStation54</code> (with ordinal 693) */
            LEFT_WING_STATION54 = 693,
            /** <code>LeftWingStation55</code> (with ordinal 694) */
            LEFT_WING_STATION55 = 694,
            /** <code>LeftWingStation56</code> (with ordinal 695) */
            LEFT_WING_STATION56 = 695,
            /** <code>LeftWingStation57</code> (with ordinal 696) */
            LEFT_WING_STATION57 = 696,
            /** <code>LeftWingStation58</code> (with ordinal 697) */
            LEFT_WING_STATION58 = 697,
            /** <code>LeftWingStation59</code> (with ordinal 698) */
            LEFT_WING_STATION59 = 698,
            /** <code>LeftWingStation60</code> (with ordinal 699) */
            LEFT_WING_STATION60 = 699,
            /** <code>LeftWingStation61</code> (with ordinal 700) */
            LEFT_WING_STATION61 = 700,
            /** <code>LeftWingStation62</code> (with ordinal 701) */
            LEFT_WING_STATION62 = 701,
            /** <code>LeftWingStation63</code> (with ordinal 702) */
            LEFT_WING_STATION63 = 702,
            /** <code>LeftWingStation64</code> (with ordinal 703) */
            LEFT_WING_STATION64 = 703,
            /** <code>LeftWingStation65</code> (with ordinal 704) */
            LEFT_WING_STATION65 = 704,
            /** <code>LeftWingStation66</code> (with ordinal 705) */
            LEFT_WING_STATION66 = 705,
            /** <code>LeftWingStation67</code> (with ordinal 706) */
            LEFT_WING_STATION67 = 706,
            /** <code>LeftWingStation68</code> (with ordinal 707) */
            LEFT_WING_STATION68 = 707,
            /** <code>LeftWingStation69</code> (with ordinal 708) */
            LEFT_WING_STATION69 = 708,
            /** <code>LeftWingStation70</code> (with ordinal 709) */
            LEFT_WING_STATION70 = 709,
            /** <code>LeftWingStation71</code> (with ordinal 710) */
            LEFT_WING_STATION71 = 710,
            /** <code>LeftWingStation72</code> (with ordinal 711) */
            LEFT_WING_STATION72 = 711,
            /** <code>LeftWingStation73</code> (with ordinal 712) */
            LEFT_WING_STATION73 = 712,
            /** <code>LeftWingStation74</code> (with ordinal 713) */
            LEFT_WING_STATION74 = 713,
            /** <code>LeftWingStation75</code> (with ordinal 714) */
            LEFT_WING_STATION75 = 714,
            /** <code>LeftWingStation76</code> (with ordinal 715) */
            LEFT_WING_STATION76 = 715,
            /** <code>LeftWingStation77</code> (with ordinal 716) */
            LEFT_WING_STATION77 = 716,
            /** <code>LeftWingStation78</code> (with ordinal 717) */
            LEFT_WING_STATION78 = 717,
            /** <code>LeftWingStation79</code> (with ordinal 718) */
            LEFT_WING_STATION79 = 718,
            /** <code>LeftWingStation80</code> (with ordinal 719) */
            LEFT_WING_STATION80 = 719,
            /** <code>LeftWingStation81</code> (with ordinal 720) */
            LEFT_WING_STATION81 = 720,
            /** <code>LeftWingStation82</code> (with ordinal 721) */
            LEFT_WING_STATION82 = 721,
            /** <code>LeftWingStation83</code> (with ordinal 722) */
            LEFT_WING_STATION83 = 722,
            /** <code>LeftWingStation84</code> (with ordinal 723) */
            LEFT_WING_STATION84 = 723,
            /** <code>LeftWingStation85</code> (with ordinal 724) */
            LEFT_WING_STATION85 = 724,
            /** <code>LeftWingStation86</code> (with ordinal 725) */
            LEFT_WING_STATION86 = 725,
            /** <code>LeftWingStation87</code> (with ordinal 726) */
            LEFT_WING_STATION87 = 726,
            /** <code>LeftWingStation88</code> (with ordinal 727) */
            LEFT_WING_STATION88 = 727,
            /** <code>LeftWingStation89</code> (with ordinal 728) */
            LEFT_WING_STATION89 = 728,
            /** <code>LeftWingStation90</code> (with ordinal 729) */
            LEFT_WING_STATION90 = 729,
            /** <code>LeftWingStation91</code> (with ordinal 730) */
            LEFT_WING_STATION91 = 730,
            /** <code>LeftWingStation92</code> (with ordinal 731) */
            LEFT_WING_STATION92 = 731,
            /** <code>LeftWingStation93</code> (with ordinal 732) */
            LEFT_WING_STATION93 = 732,
            /** <code>LeftWingStation94</code> (with ordinal 733) */
            LEFT_WING_STATION94 = 733,
            /** <code>LeftWingStation95</code> (with ordinal 734) */
            LEFT_WING_STATION95 = 734,
            /** <code>LeftWingStation96</code> (with ordinal 735) */
            LEFT_WING_STATION96 = 735,
            /** <code>LeftWingStation97</code> (with ordinal 736) */
            LEFT_WING_STATION97 = 736,
            /** <code>LeftWingStation98</code> (with ordinal 737) */
            LEFT_WING_STATION98 = 737,
            /** <code>LeftWingStation99</code> (with ordinal 738) */
            LEFT_WING_STATION99 = 738,
            /** <code>LeftWingStation100</code> (with ordinal 739) */
            LEFT_WING_STATION100 = 739,
            /** <code>LeftWingStation101</code> (with ordinal 740) */
            LEFT_WING_STATION101 = 740,
            /** <code>LeftWingStation102</code> (with ordinal 741) */
            LEFT_WING_STATION102 = 741,
            /** <code>LeftWingStation103</code> (with ordinal 742) */
            LEFT_WING_STATION103 = 742,
            /** <code>LeftWingStation104</code> (with ordinal 743) */
            LEFT_WING_STATION104 = 743,
            /** <code>LeftWingStation105</code> (with ordinal 744) */
            LEFT_WING_STATION105 = 744,
            /** <code>LeftWingStation106</code> (with ordinal 745) */
            LEFT_WING_STATION106 = 745,
            /** <code>LeftWingStation107</code> (with ordinal 746) */
            LEFT_WING_STATION107 = 746,
            /** <code>LeftWingStation108</code> (with ordinal 747) */
            LEFT_WING_STATION108 = 747,
            /** <code>LeftWingStation109</code> (with ordinal 748) */
            LEFT_WING_STATION109 = 748,
            /** <code>LeftWingStation110</code> (with ordinal 749) */
            LEFT_WING_STATION110 = 749,
            /** <code>LeftWingStation111</code> (with ordinal 750) */
            LEFT_WING_STATION111 = 750,
            /** <code>LeftWingStation112</code> (with ordinal 751) */
            LEFT_WING_STATION112 = 751,
            /** <code>LeftWingStation113</code> (with ordinal 752) */
            LEFT_WING_STATION113 = 752,
            /** <code>LeftWingStation114</code> (with ordinal 753) */
            LEFT_WING_STATION114 = 753,
            /** <code>LeftWingStation115</code> (with ordinal 754) */
            LEFT_WING_STATION115 = 754,
            /** <code>LeftWingStation116</code> (with ordinal 755) */
            LEFT_WING_STATION116 = 755,
            /** <code>LeftWingStation117</code> (with ordinal 756) */
            LEFT_WING_STATION117 = 756,
            /** <code>LeftWingStation118</code> (with ordinal 757) */
            LEFT_WING_STATION118 = 757,
            /** <code>LeftWingStation119</code> (with ordinal 758) */
            LEFT_WING_STATION119 = 758,
            /** <code>LeftWingStation120</code> (with ordinal 759) */
            LEFT_WING_STATION120 = 759,
            /** <code>LeftWingStation121</code> (with ordinal 760) */
            LEFT_WING_STATION121 = 760,
            /** <code>LeftWingStation122</code> (with ordinal 761) */
            LEFT_WING_STATION122 = 761,
            /** <code>LeftWingStation123</code> (with ordinal 762) */
            LEFT_WING_STATION123 = 762,
            /** <code>LeftWingStation124</code> (with ordinal 763) */
            LEFT_WING_STATION124 = 763,
            /** <code>LeftWingStation125</code> (with ordinal 764) */
            LEFT_WING_STATION125 = 764,
            /** <code>LeftWingStation126</code> (with ordinal 765) */
            LEFT_WING_STATION126 = 765,
            /** <code>LeftWingStation127</code> (with ordinal 766) */
            LEFT_WING_STATION127 = 766,
            /** <code>LeftWingStation128</code> (with ordinal 767) */
            LEFT_WING_STATION128 = 767,
            /** <code>RightWingStation1</code> (with ordinal 768) */
            RIGHT_WING_STATION1 = 768,
            /** <code>RightWingStation2</code> (with ordinal 769) */
            RIGHT_WING_STATION2 = 769,
            /** <code>RightWingStation3</code> (with ordinal 770) */
            RIGHT_WING_STATION3 = 770,
            /** <code>RightWingStation4</code> (with ordinal 771) */
            RIGHT_WING_STATION4 = 771,
            /** <code>RightWingStation5</code> (with ordinal 772) */
            RIGHT_WING_STATION5 = 772,
            /** <code>RightWingStation6</code> (with ordinal 773) */
            RIGHT_WING_STATION6 = 773,
            /** <code>RightWingStation7</code> (with ordinal 774) */
            RIGHT_WING_STATION7 = 774,
            /** <code>RightWingStation8</code> (with ordinal 775) */
            RIGHT_WING_STATION8 = 775,
            /** <code>RightWingStation9</code> (with ordinal 776) */
            RIGHT_WING_STATION9 = 776,
            /** <code>RightWingStation10</code> (with ordinal 777) */
            RIGHT_WING_STATION10 = 777,
            /** <code>RightWingStation11</code> (with ordinal 778) */
            RIGHT_WING_STATION11 = 778,
            /** <code>RightWingStation12</code> (with ordinal 779) */
            RIGHT_WING_STATION12 = 779,
            /** <code>RightWingStation13</code> (with ordinal 780) */
            RIGHT_WING_STATION13 = 780,
            /** <code>RightWingStation14</code> (with ordinal 781) */
            RIGHT_WING_STATION14 = 781,
            /** <code>RightWingStation15</code> (with ordinal 782) */
            RIGHT_WING_STATION15 = 782,
            /** <code>RightWingStation16</code> (with ordinal 783) */
            RIGHT_WING_STATION16 = 783,
            /** <code>RightWingStation17</code> (with ordinal 784) */
            RIGHT_WING_STATION17 = 784,
            /** <code>RightWingStation18</code> (with ordinal 785) */
            RIGHT_WING_STATION18 = 785,
            /** <code>RightWingStation19</code> (with ordinal 786) */
            RIGHT_WING_STATION19 = 786,
            /** <code>RightWingStation20</code> (with ordinal 787) */
            RIGHT_WING_STATION20 = 787,
            /** <code>RightWingStation21</code> (with ordinal 788) */
            RIGHT_WING_STATION21 = 788,
            /** <code>RightWingStation22</code> (with ordinal 789) */
            RIGHT_WING_STATION22 = 789,
            /** <code>RightWingStation23</code> (with ordinal 790) */
            RIGHT_WING_STATION23 = 790,
            /** <code>RightWingStation24</code> (with ordinal 791) */
            RIGHT_WING_STATION24 = 791,
            /** <code>RightWingStation25</code> (with ordinal 792) */
            RIGHT_WING_STATION25 = 792,
            /** <code>RightWingStation26</code> (with ordinal 793) */
            RIGHT_WING_STATION26 = 793,
            /** <code>RightWingStation27</code> (with ordinal 794) */
            RIGHT_WING_STATION27 = 794,
            /** <code>RightWingStation28</code> (with ordinal 795) */
            RIGHT_WING_STATION28 = 795,
            /** <code>RightWingStation29</code> (with ordinal 796) */
            RIGHT_WING_STATION29 = 796,
            /** <code>RightWingStation30</code> (with ordinal 797) */
            RIGHT_WING_STATION30 = 797,
            /** <code>RightWingStation31</code> (with ordinal 798) */
            RIGHT_WING_STATION31 = 798,
            /** <code>RightWingStation32</code> (with ordinal 799) */
            RIGHT_WING_STATION32 = 799,
            /** <code>RightWingStation33</code> (with ordinal 800) */
            RIGHT_WING_STATION33 = 800,
            /** <code>RightWingStation34</code> (with ordinal 801) */
            RIGHT_WING_STATION34 = 801,
            /** <code>RightWingStation35</code> (with ordinal 802) */
            RIGHT_WING_STATION35 = 802,
            /** <code>RightWingStation36</code> (with ordinal 803) */
            RIGHT_WING_STATION36 = 803,
            /** <code>RightWingStation37</code> (with ordinal 804) */
            RIGHT_WING_STATION37 = 804,
            /** <code>RightWingStation38</code> (with ordinal 805) */
            RIGHT_WING_STATION38 = 805,
            /** <code>RightWingStation39</code> (with ordinal 806) */
            RIGHT_WING_STATION39 = 806,
            /** <code>RightWingStation40</code> (with ordinal 807) */
            RIGHT_WING_STATION40 = 807,
            /** <code>RightWingStation41</code> (with ordinal 808) */
            RIGHT_WING_STATION41 = 808,
            /** <code>RightWingStation42</code> (with ordinal 809) */
            RIGHT_WING_STATION42 = 809,
            /** <code>RightWingStation43</code> (with ordinal 810) */
            RIGHT_WING_STATION43 = 810,
            /** <code>RightWingStation44</code> (with ordinal 811) */
            RIGHT_WING_STATION44 = 811,
            /** <code>RightWingStation45</code> (with ordinal 812) */
            RIGHT_WING_STATION45 = 812,
            /** <code>RightWingStation46</code> (with ordinal 813) */
            RIGHT_WING_STATION46 = 813,
            /** <code>RightWingStation47</code> (with ordinal 814) */
            RIGHT_WING_STATION47 = 814,
            /** <code>RightWingStation48</code> (with ordinal 815) */
            RIGHT_WING_STATION48 = 815,
            /** <code>RightWingStation49</code> (with ordinal 816) */
            RIGHT_WING_STATION49 = 816,
            /** <code>RightWingStation50</code> (with ordinal 817) */
            RIGHT_WING_STATION50 = 817,
            /** <code>RightWingStation51</code> (with ordinal 818) */
            RIGHT_WING_STATION51 = 818,
            /** <code>RightWingStation52</code> (with ordinal 819) */
            RIGHT_WING_STATION52 = 819,
            /** <code>RightWingStation53</code> (with ordinal 820) */
            RIGHT_WING_STATION53 = 820,
            /** <code>RightWingStation54</code> (with ordinal 821) */
            RIGHT_WING_STATION54 = 821,
            /** <code>RightWingStation55</code> (with ordinal 822) */
            RIGHT_WING_STATION55 = 822,
            /** <code>RightWingStation56</code> (with ordinal 823) */
            RIGHT_WING_STATION56 = 823,
            /** <code>RightWingStation57</code> (with ordinal 824) */
            RIGHT_WING_STATION57 = 824,
            /** <code>RightWingStation58</code> (with ordinal 825) */
            RIGHT_WING_STATION58 = 825,
            /** <code>RightWingStation59</code> (with ordinal 826) */
            RIGHT_WING_STATION59 = 826,
            /** <code>RightWingStation60</code> (with ordinal 827) */
            RIGHT_WING_STATION60 = 827,
            /** <code>RightWingStation61</code> (with ordinal 828) */
            RIGHT_WING_STATION61 = 828,
            /** <code>RightWingStation62</code> (with ordinal 829) */
            RIGHT_WING_STATION62 = 829,
            /** <code>RightWingStation63</code> (with ordinal 830) */
            RIGHT_WING_STATION63 = 830,
            /** <code>RightWingStation64</code> (with ordinal 831) */
            RIGHT_WING_STATION64 = 831,
            /** <code>RightWingStation65</code> (with ordinal 832) */
            RIGHT_WING_STATION65 = 832,
            /** <code>RightWingStation66</code> (with ordinal 833) */
            RIGHT_WING_STATION66 = 833,
            /** <code>RightWingStation67</code> (with ordinal 834) */
            RIGHT_WING_STATION67 = 834,
            /** <code>RightWingStation68</code> (with ordinal 835) */
            RIGHT_WING_STATION68 = 835,
            /** <code>RightWingStation69</code> (with ordinal 836) */
            RIGHT_WING_STATION69 = 836,
            /** <code>RightWingStation70</code> (with ordinal 837) */
            RIGHT_WING_STATION70 = 837,
            /** <code>RightWingStation71</code> (with ordinal 838) */
            RIGHT_WING_STATION71 = 838,
            /** <code>RightWingStation72</code> (with ordinal 839) */
            RIGHT_WING_STATION72 = 839,
            /** <code>RightWingStation73</code> (with ordinal 840) */
            RIGHT_WING_STATION73 = 840,
            /** <code>RightWingStation74</code> (with ordinal 841) */
            RIGHT_WING_STATION74 = 841,
            /** <code>RightWingStation75</code> (with ordinal 842) */
            RIGHT_WING_STATION75 = 842,
            /** <code>RightWingStation76</code> (with ordinal 843) */
            RIGHT_WING_STATION76 = 843,
            /** <code>RightWingStation77</code> (with ordinal 844) */
            RIGHT_WING_STATION77 = 844,
            /** <code>RightWingStation78</code> (with ordinal 845) */
            RIGHT_WING_STATION78 = 845,
            /** <code>RightWingStation79</code> (with ordinal 846) */
            RIGHT_WING_STATION79 = 846,
            /** <code>RightWingStation80</code> (with ordinal 847) */
            RIGHT_WING_STATION80 = 847,
            /** <code>RightWingStation81</code> (with ordinal 848) */
            RIGHT_WING_STATION81 = 848,
            /** <code>RightWingStation82</code> (with ordinal 849) */
            RIGHT_WING_STATION82 = 849,
            /** <code>RightWingStation83</code> (with ordinal 850) */
            RIGHT_WING_STATION83 = 850,
            /** <code>RightWingStation84</code> (with ordinal 851) */
            RIGHT_WING_STATION84 = 851,
            /** <code>RightWingStation85</code> (with ordinal 852) */
            RIGHT_WING_STATION85 = 852,
            /** <code>RightWingStation86</code> (with ordinal 853) */
            RIGHT_WING_STATION86 = 853,
            /** <code>RightWingStation87</code> (with ordinal 854) */
            RIGHT_WING_STATION87 = 854,
            /** <code>RightWingStation88</code> (with ordinal 855) */
            RIGHT_WING_STATION88 = 855,
            /** <code>RightWingStation89</code> (with ordinal 856) */
            RIGHT_WING_STATION89 = 856,
            /** <code>RightWingStation90</code> (with ordinal 857) */
            RIGHT_WING_STATION90 = 857,
            /** <code>RightWingStation91</code> (with ordinal 858) */
            RIGHT_WING_STATION91 = 858,
            /** <code>RightWingStation92</code> (with ordinal 859) */
            RIGHT_WING_STATION92 = 859,
            /** <code>RightWingStation93</code> (with ordinal 860) */
            RIGHT_WING_STATION93 = 860,
            /** <code>RightWingStation94</code> (with ordinal 861) */
            RIGHT_WING_STATION94 = 861,
            /** <code>RightWingStation95</code> (with ordinal 862) */
            RIGHT_WING_STATION95 = 862,
            /** <code>RightWingStation96</code> (with ordinal 863) */
            RIGHT_WING_STATION96 = 863,
            /** <code>RightWingStation97</code> (with ordinal 864) */
            RIGHT_WING_STATION97 = 864,
            /** <code>RightWingStation98</code> (with ordinal 865) */
            RIGHT_WING_STATION98 = 865,
            /** <code>RightWingStation99</code> (with ordinal 866) */
            RIGHT_WING_STATION99 = 866,
            /** <code>RightWingStation100</code> (with ordinal 867) */
            RIGHT_WING_STATION100 = 867,
            /** <code>RightWingStation101</code> (with ordinal 868) */
            RIGHT_WING_STATION101 = 868,
            /** <code>RightWingStation102</code> (with ordinal 869) */
            RIGHT_WING_STATION102 = 869,
            /** <code>RightWingStation103</code> (with ordinal 870) */
            RIGHT_WING_STATION103 = 870,
            /** <code>RightWingStation104</code> (with ordinal 871) */
            RIGHT_WING_STATION104 = 871,
            /** <code>RightWingStation105</code> (with ordinal 872) */
            RIGHT_WING_STATION105 = 872,
            /** <code>RightWingStation106</code> (with ordinal 873) */
            RIGHT_WING_STATION106 = 873,
            /** <code>RightWingStation107</code> (with ordinal 874) */
            RIGHT_WING_STATION107 = 874,
            /** <code>RightWingStation108</code> (with ordinal 875) */
            RIGHT_WING_STATION108 = 875,
            /** <code>RightWingStation109</code> (with ordinal 876) */
            RIGHT_WING_STATION109 = 876,
            /** <code>RightWingStation110</code> (with ordinal 877) */
            RIGHT_WING_STATION110 = 877,
            /** <code>RightWingStation111</code> (with ordinal 878) */
            RIGHT_WING_STATION111 = 878,
            /** <code>RightWingStation112</code> (with ordinal 879) */
            RIGHT_WING_STATION112 = 879,
            /** <code>RightWingStation113</code> (with ordinal 880) */
            RIGHT_WING_STATION113 = 880,
            /** <code>RightWingStation114</code> (with ordinal 881) */
            RIGHT_WING_STATION114 = 881,
            /** <code>RightWingStation115</code> (with ordinal 882) */
            RIGHT_WING_STATION115 = 882,
            /** <code>RightWingStation116</code> (with ordinal 883) */
            RIGHT_WING_STATION116 = 883,
            /** <code>RightWingStation117</code> (with ordinal 884) */
            RIGHT_WING_STATION117 = 884,
            /** <code>RightWingStation118</code> (with ordinal 885) */
            RIGHT_WING_STATION118 = 885,
            /** <code>RightWingStation119</code> (with ordinal 886) */
            RIGHT_WING_STATION119 = 886,
            /** <code>RightWingStation120</code> (with ordinal 887) */
            RIGHT_WING_STATION120 = 887,
            /** <code>RightWingStation121</code> (with ordinal 888) */
            RIGHT_WING_STATION121 = 888,
            /** <code>RightWingStation122</code> (with ordinal 889) */
            RIGHT_WING_STATION122 = 889,
            /** <code>RightWingStation123</code> (with ordinal 890) */
            RIGHT_WING_STATION123 = 890,
            /** <code>RightWingStation124</code> (with ordinal 891) */
            RIGHT_WING_STATION124 = 891,
            /** <code>RightWingStation125</code> (with ordinal 892) */
            RIGHT_WING_STATION125 = 892,
            /** <code>RightWingStation126</code> (with ordinal 893) */
            RIGHT_WING_STATION126 = 893,
            /** <code>RightWingStation127</code> (with ordinal 894) */
            RIGHT_WING_STATION127 = 894,
            /** <code>RightWingStation128</code> (with ordinal 895) */
            RIGHT_WING_STATION128 = 895,
            /** <code>M16A42_rifle</code> (with ordinal 896) */
            M16A42_RIFLE = 896,
            /** <code>M249_SAW</code> (with ordinal 897) */
            M249_SAW = 897,
            /** <code>M60_Machine_gun</code> (with ordinal 898) */
            M60_MACHINE_GUN = 898,
            /** <code>M203_Grenade_Launcher</code> (with ordinal 899) */
            M203_GRENADE_LAUNCHER = 899,
            /** <code>M136_AT4</code> (with ordinal 900) */
            M136_AT4 = 900,
            /** <code>M47_Dragon</code> (with ordinal 901) */
            M47_DRAGON = 901,
            /** <code>AAWS_M_Javelin</code> (with ordinal 902) */
            AAWS_M_JAVELIN = 902,
            /** <code>M18A1_Claymore_Mine</code> (with ordinal 903) */
            M18A1_CLAYMORE_MINE = 903,
            /** <code>MK19_Grenade_Launcher</code> (with ordinal 904) */
            MK19_GRENADE_LAUNCHER = 904,
            /** <code>M2_Machine_Gun</code> (with ordinal 905) */
            M2_MACHINE_GUN = 905,
            /** <code>Other_attached_parts</code> (with ordinal 906) */
            OTHER_ATTACHED_PARTS = 906
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to StationEnum: static_cast<DevStudio::StationEnum::StationEnum>(i)
        */
        LIBAPI bool isValid(const StationEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::StationEnum::StationEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::StationEnum::StationEnum const &);
}


#endif
