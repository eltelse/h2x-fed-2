/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_BREACHEDSTATUSENUM_H
#define DEVELOPER_STUDIO_DATATYPES_BREACHEDSTATUSENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace BreachedStatusEnum {
        /**
        * Implementation of the <code>BreachedStatusEnum8</code> data type from the FOM.
        * <br>Description from the FOM: <i>Breached appearance</i>
        */
        enum BreachedStatusEnum {
            /** <code>NoBreaching</code> (with ordinal 0) */
            NO_BREACHING = 0,
            /** <code>SlightBreaching</code> (with ordinal 1) */
            SLIGHT_BREACHING = 1,
            /** <code>ModerateBreaching</code> (with ordinal 2) */
            MODERATE_BREACHING = 2,
            /** <code>Cleared</code> (with ordinal 3) */
            CLEARED = 3
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to BreachedStatusEnum: static_cast<DevStudio::BreachedStatusEnum::BreachedStatusEnum>(i)
        */
        LIBAPI bool isValid(const BreachedStatusEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::BreachedStatusEnum::BreachedStatusEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::BreachedStatusEnum::BreachedStatusEnum const &);
}


#endif
