/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_SPHERE2GEOMRECSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_SPHERE2GEOMRECSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/AngularVelocityVectorStruct.h>
#include <DevStudio/datatypes/VelocityVectorStruct.h>
#include <DevStudio/datatypes/WorldLocationStruct.h>

namespace DevStudio {
   /**
   * Implementation of the <code>Sphere2GeomRecStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Record specifying Sphere 2 geometry record</i>
   */
   class Sphere2GeomRecStruct {

   public:
      /**
      * Description from the FOM: <i>Centroid location X, Y, Z</i>.
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      */
      WorldLocationStruct centroidLocation;
      /**
      * Description from the FOM: <i>Radius</i>.
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      */
      float radius;
      /**
      * Description from the FOM: <i>Variation of radius</i>.
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      */
      float radiusRate;
      /**
      * Description from the FOM: <i>Velocity Vx, Vy, Vz</i>.
      * <br>Description of the data type from the FOM: <i>The rate at which the position is changing over time.</i>
      */
      VelocityVectorStruct velocity;
      /**
      * Description from the FOM: <i>Angular velocity Vx, Vy, Vz</i>.
      * <br>Description of the data type from the FOM: <i>The rate at which the orientation is changing over time, in body coordinates.</i>
      */
      AngularVelocityVectorStruct angularVelocity;

      LIBAPI Sphere2GeomRecStruct()
         :
         centroidLocation(WorldLocationStruct()),
         radius(0),
         radiusRate(0),
         velocity(VelocityVectorStruct()),
         angularVelocity(AngularVelocityVectorStruct())
      {}

      /**
      * Constructor for Sphere2GeomRecStruct
      *
      * @param centroidLocation_ value to set as centroidLocation.
      * <br>Description from the FOM: <i>Centroid location X, Y, Z</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      * @param radius_ value to set as radius.
      * <br>Description from the FOM: <i>Radius</i>
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      * @param radiusRate_ value to set as radiusRate.
      * <br>Description from the FOM: <i>Variation of radius</i>
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      * @param velocity_ value to set as velocity.
      * <br>Description from the FOM: <i>Velocity Vx, Vy, Vz</i>
      * <br>Description of the data type from the FOM: <i>The rate at which the position is changing over time.</i>
      * @param angularVelocity_ value to set as angularVelocity.
      * <br>Description from the FOM: <i>Angular velocity Vx, Vy, Vz</i>
      * <br>Description of the data type from the FOM: <i>The rate at which the orientation is changing over time, in body coordinates.</i>
      */
      LIBAPI Sphere2GeomRecStruct(
         WorldLocationStruct centroidLocation_,
         float radius_,
         float radiusRate_,
         VelocityVectorStruct velocity_,
         AngularVelocityVectorStruct angularVelocity_
         )
         :
         centroidLocation(centroidLocation_),
         radius(radius_),
         radiusRate(radiusRate_),
         velocity(velocity_),
         angularVelocity(angularVelocity_)
      {}



      /**
      * Function to get centroidLocation.
      * <br>Description from the FOM: <i>Centroid location X, Y, Z</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @return centroidLocation
      */
      LIBAPI DevStudio::WorldLocationStruct & getCentroidLocation() {
         return centroidLocation;
      }

      /**
      * Function to get radius.
      * <br>Description from the FOM: <i>Radius</i>
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      *
      * @return radius
      */
      LIBAPI float & getRadius() {
         return radius;
      }

      /**
      * Function to get radiusRate.
      * <br>Description from the FOM: <i>Variation of radius</i>
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      *
      * @return radiusRate
      */
      LIBAPI float & getRadiusRate() {
         return radiusRate;
      }

      /**
      * Function to get velocity.
      * <br>Description from the FOM: <i>Velocity Vx, Vy, Vz</i>
      * <br>Description of the data type from the FOM: <i>The rate at which the position is changing over time.</i>
      *
      * @return velocity
      */
      LIBAPI DevStudio::VelocityVectorStruct & getVelocity() {
         return velocity;
      }

      /**
      * Function to get angularVelocity.
      * <br>Description from the FOM: <i>Angular velocity Vx, Vy, Vz</i>
      * <br>Description of the data type from the FOM: <i>The rate at which the orientation is changing over time, in body coordinates.</i>
      *
      * @return angularVelocity
      */
      LIBAPI DevStudio::AngularVelocityVectorStruct & getAngularVelocity() {
         return angularVelocity;
      }

   };


   LIBAPI bool operator ==(const DevStudio::Sphere2GeomRecStruct& l, const DevStudio::Sphere2GeomRecStruct& r);
   LIBAPI bool operator !=(const DevStudio::Sphere2GeomRecStruct& l, const DevStudio::Sphere2GeomRecStruct& r);
   LIBAPI bool operator <(const DevStudio::Sphere2GeomRecStruct& l, const DevStudio::Sphere2GeomRecStruct& r);
   LIBAPI bool operator >(const DevStudio::Sphere2GeomRecStruct& l, const DevStudio::Sphere2GeomRecStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::Sphere2GeomRecStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::Sphere2GeomRecStruct const &);
}
#endif
