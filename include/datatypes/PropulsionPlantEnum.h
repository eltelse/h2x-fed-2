/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_PROPULSIONPLANTENUM_H
#define DEVELOPER_STUDIO_DATATYPES_PROPULSIONPLANTENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace PropulsionPlantEnum {
        /**
        * Implementation of the <code>PropulsionPlantEnum8</code> data type from the FOM.
        * <br>Description from the FOM: <i>Propulsion plant configuration</i>
        */
        enum PropulsionPlantEnum {
            /** <code>Other</code> (with ordinal 0) */
            OTHER = 0,
            /** <code>Diesel_Electric</code> (with ordinal 1) */
            DIESEL_ELECTRIC = 1,
            /** <code>Diesel</code> (with ordinal 2) */
            DIESEL = 2,
            /** <code>Battery</code> (with ordinal 3) */
            BATTERY = 3,
            /** <code>TurbineReduction</code> (with ordinal 4) */
            TURBINE_REDUCTION = 4,
            /** <code>Steam</code> (with ordinal 6) */
            STEAM = 6,
            /** <code>GasTurbine</code> (with ordinal 7) */
            GAS_TURBINE = 7
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to PropulsionPlantEnum: static_cast<DevStudio::PropulsionPlantEnum::PropulsionPlantEnum>(i)
        */
        LIBAPI bool isValid(const PropulsionPlantEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::PropulsionPlantEnum::PropulsionPlantEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::PropulsionPlantEnum::PropulsionPlantEnum const &);
}


#endif
