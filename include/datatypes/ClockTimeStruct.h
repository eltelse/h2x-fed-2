/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_CLOCKTIMESTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_CLOCKTIMESTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>


namespace DevStudio {
   /**
   * Implementation of the <code>ClockTimeStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Specification of the point in time of an occurrence. Based on the Clock Time record as specified in IEEE 1278.1-1995 section 5.2.8.</i>
   */
   class ClockTimeStruct {

   public:
      /**
      * Description from the FOM: <i>The number of hours since 0000 hours, January 1, 1970 UTC.</i>.
      * <br>Description of the data type from the FOM: <i>Time past on the clock in full hours since a specified point in time. [unit: hour, resolution: 1, accuracy: perfect]</i>
      */
      int hours;
      /**
      * Description from the FOM: <i>The time past the hour indicated in the Hours field.</i>.
      * <br>Description of the data type from the FOM: <i>The time past the hour, scaled so that value 0 represents the start of the hour and value 2^31 - 1 represents one time unit before the start of the next hour, thereby resulting in each time unit representing exactly 3600/(2^31) s, which is approximately 1.67638063 microsecond. [unit: 3600/(2^31) second, resolution: 1, accuracy: perfect]</i>
      */
      unsigned int timePastTheHour;

      LIBAPI ClockTimeStruct()
         :
         hours(0),
         timePastTheHour(0)
      {}

      /**
      * Constructor for ClockTimeStruct
      *
      * @param hours_ value to set as hours.
      * <br>Description from the FOM: <i>The number of hours since 0000 hours, January 1, 1970 UTC.</i>
      * <br>Description of the data type from the FOM: <i>Time past on the clock in full hours since a specified point in time. [unit: hour, resolution: 1, accuracy: perfect]</i>
      * @param timePastTheHour_ value to set as timePastTheHour.
      * <br>Description from the FOM: <i>The time past the hour indicated in the Hours field.</i>
      * <br>Description of the data type from the FOM: <i>The time past the hour, scaled so that value 0 represents the start of the hour and value 2^31 - 1 represents one time unit before the start of the next hour, thereby resulting in each time unit representing exactly 3600/(2^31) s, which is approximately 1.67638063 microsecond. [unit: 3600/(2^31) second, resolution: 1, accuracy: perfect]</i>
      */
      LIBAPI ClockTimeStruct(
         int hours_,
         unsigned int timePastTheHour_
         )
         :
         hours(hours_),
         timePastTheHour(timePastTheHour_)
      {}



      /**
      * Function to get hours.
      * <br>Description from the FOM: <i>The number of hours since 0000 hours, January 1, 1970 UTC.</i>
      * <br>Description of the data type from the FOM: <i>Time past on the clock in full hours since a specified point in time. [unit: hour, resolution: 1, accuracy: perfect]</i>
      *
      * @return hours
      */
      LIBAPI int & getHours() {
         return hours;
      }

      /**
      * Function to get timePastTheHour.
      * <br>Description from the FOM: <i>The time past the hour indicated in the Hours field.</i>
      * <br>Description of the data type from the FOM: <i>The time past the hour, scaled so that value 0 represents the start of the hour and value 2^31 - 1 represents one time unit before the start of the next hour, thereby resulting in each time unit representing exactly 3600/(2^31) s, which is approximately 1.67638063 microsecond. [unit: 3600/(2^31) second, resolution: 1, accuracy: perfect]</i>
      *
      * @return timePastTheHour
      */
      LIBAPI unsigned int & getTimePastTheHour() {
         return timePastTheHour;
      }

   };


   LIBAPI bool operator ==(const DevStudio::ClockTimeStruct& l, const DevStudio::ClockTimeStruct& r);
   LIBAPI bool operator !=(const DevStudio::ClockTimeStruct& l, const DevStudio::ClockTimeStruct& r);
   LIBAPI bool operator <(const DevStudio::ClockTimeStruct& l, const DevStudio::ClockTimeStruct& r);
   LIBAPI bool operator >(const DevStudio::ClockTimeStruct& l, const DevStudio::ClockTimeStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::ClockTimeStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::ClockTimeStruct const &);
}
#endif
