/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_IFFSYSTEMTYPEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_IFFSYSTEMTYPEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace IffSystemTypeEnum {
        /**
        * Implementation of the <code>IffSystemTypeEnum16</code> data type from the FOM.
        * <br>Description from the FOM: <i>IFF system type</i>
        */
        enum IffSystemTypeEnum {
            /** <code>Other</code> (with ordinal 0) */
            OTHER = 0,
            /** <code>MarkTransponder</code> (with ordinal 1) */
            MARK_TRANSPONDER = 1,
            /** <code>MarkInterrogator</code> (with ordinal 2) */
            MARK_INTERROGATOR = 2,
            /** <code>SovietTransponder</code> (with ordinal 3) */
            SOVIET_TRANSPONDER = 3,
            /** <code>SovietInterrogator</code> (with ordinal 4) */
            SOVIET_INTERROGATOR = 4,
            /** <code>RRBTransponder</code> (with ordinal 5) */
            RRBTRANSPONDER = 5,
            /** <code>MarkXIIAInterrogator</code> (with ordinal 6) */
            MARK_XIIAINTERROGATOR = 6,
            /** <code>Mode5Interrogator</code> (with ordinal 7) */
            MODE5INTERROGATOR = 7,
            /** <code>ModeSInterrogator</code> (with ordinal 8) */
            MODE_SINTERROGATOR = 8,
            /** <code>MarkXIIATransponder</code> (with ordinal 9) */
            MARK_XIIATRANSPONDER = 9,
            /** <code>Mode5Transponder</code> (with ordinal 10) */
            MODE5TRANSPONDER = 10,
            /** <code>ModeSTransponder</code> (with ordinal 11) */
            MODE_STRANSPONDER = 11,
            /** <code>MarkXIIACombinedInterrogator_Transponder_CIT_</code> (with ordinal 12) */
            MARK_XIIACOMBINED_INTERROGATOR_TRANSPONDER_CIT = 12,
            /** <code>MarkXIICombinedInterrogator_Transponder_CIT_</code> (with ordinal 13) */
            MARK_XIICOMBINED_INTERROGATOR_TRANSPONDER_CIT = 13,
            /** <code>TCAS_ACASTransceiver</code> (with ordinal 14) */
            TCAS_ACASTRANSCEIVER = 14
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to IffSystemTypeEnum: static_cast<DevStudio::IffSystemTypeEnum::IffSystemTypeEnum>(i)
        */
        LIBAPI bool isValid(const IffSystemTypeEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::IffSystemTypeEnum::IffSystemTypeEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::IffSystemTypeEnum::IffSystemTypeEnum const &);
}


#endif
