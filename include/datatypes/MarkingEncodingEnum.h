/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_MARKINGENCODINGENUM_H
#define DEVELOPER_STUDIO_DATATYPES_MARKINGENCODINGENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace MarkingEncodingEnum {
        /**
        * Implementation of the <code>MarkingEncodingEnum8</code> data type from the FOM.
        * <br>Description from the FOM: <i>Marking character set</i>
        */
        enum MarkingEncodingEnum {
            /** <code>Other</code> (with ordinal 0) */
            OTHER = 0,
            /** <code>ASCII</code> (with ordinal 1) */
            ASCII = 1,
            /** <code>ArmyMarkingCCTT</code> (with ordinal 2) */
            ARMY_MARKING_CCTT = 2,
            /** <code>DigitChevron</code> (with ordinal 3) */
            DIGIT_CHEVRON = 3
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to MarkingEncodingEnum: static_cast<DevStudio::MarkingEncodingEnum::MarkingEncodingEnum>(i)
        */
        LIBAPI bool isValid(const MarkingEncodingEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::MarkingEncodingEnum::MarkingEncodingEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::MarkingEncodingEnum::MarkingEncodingEnum const &);
}


#endif
