/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_COMBICSTATERECSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_COMBICSTATERECSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/EntityTypeStruct.h>
#include <DevStudio/datatypes/OctetArray4.h>
#include <DevStudio/datatypes/VelocityVectorStruct.h>
#include <vector>

namespace DevStudio {
   /**
   * Implementation of the <code>COMBICStateRecStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Record specifying COMBIC state record</i>
   */
   class COMBICStateRecStruct {

   public:
      /**
      * Description from the FOM: <i>Time since creation</i>.
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      unsigned int timeSinceCreation;
      /**
      * Description from the FOM: <i>Munition source</i>.
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      */
      EntityTypeStruct munitionSource;
      /**
      * Description from the FOM: <i>Number of sources</i>.
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^31, 2^31-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      int numberOfSources;
      /**
      * Description from the FOM: <i>Geometry index</i>.
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      unsigned short geometryIndex;
      /**
      * Description from the FOM: <i>Source type</i>.
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      unsigned int sourceType;
      /**
      * Description from the FOM: <i>Barrage rate</i>.
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      */
      float barrageRate;
      /**
      * Description from the FOM: <i>Barrage duration</i>.
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      */
      float barrageDuration;
      /**
      * Description from the FOM: <i>Barrage crosswind length</i>.
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      */
      float barrageCrosswindLength;
      /**
      * Description from the FOM: <i>Barrage downwind length</i>.
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      */
      float barrageDownwindLength;
      /**
      * Description from the FOM: <i>Detonation velocity</i>.
      * <br>Description of the data type from the FOM: <i>The rate at which the position is changing over time.</i>
      */
      VelocityVectorStruct detonationVelocity;
      /**
      * Description from the FOM: <i>Padding field</i>.
      * <br>Description of the data type from the FOM: <i>Generic array of four Octet elements.</i>
      */
      std::vector</* 4 */ char > padding;

      LIBAPI COMBICStateRecStruct()
         :
         timeSinceCreation(0),
         munitionSource(EntityTypeStruct()),
         numberOfSources(0),
         geometryIndex(0),
         sourceType(0),
         barrageRate(0),
         barrageDuration(0),
         barrageCrosswindLength(0),
         barrageDownwindLength(0),
         detonationVelocity(VelocityVectorStruct()),
         padding(0)
      {}

      /**
      * Constructor for COMBICStateRecStruct
      *
      * @param timeSinceCreation_ value to set as timeSinceCreation.
      * <br>Description from the FOM: <i>Time since creation</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param munitionSource_ value to set as munitionSource.
      * <br>Description from the FOM: <i>Munition source</i>
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      * @param numberOfSources_ value to set as numberOfSources.
      * <br>Description from the FOM: <i>Number of sources</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^31, 2^31-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param geometryIndex_ value to set as geometryIndex.
      * <br>Description from the FOM: <i>Geometry index</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param sourceType_ value to set as sourceType.
      * <br>Description from the FOM: <i>Source type</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param barrageRate_ value to set as barrageRate.
      * <br>Description from the FOM: <i>Barrage rate</i>
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      * @param barrageDuration_ value to set as barrageDuration.
      * <br>Description from the FOM: <i>Barrage duration</i>
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      * @param barrageCrosswindLength_ value to set as barrageCrosswindLength.
      * <br>Description from the FOM: <i>Barrage crosswind length</i>
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      * @param barrageDownwindLength_ value to set as barrageDownwindLength.
      * <br>Description from the FOM: <i>Barrage downwind length</i>
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      * @param detonationVelocity_ value to set as detonationVelocity.
      * <br>Description from the FOM: <i>Detonation velocity</i>
      * <br>Description of the data type from the FOM: <i>The rate at which the position is changing over time.</i>
      * @param padding_ value to set as padding.
      * <br>Description from the FOM: <i>Padding field</i>
      * <br>Description of the data type from the FOM: <i>Generic array of four Octet elements.</i>
      */
      LIBAPI COMBICStateRecStruct(
         unsigned int timeSinceCreation_,
         EntityTypeStruct munitionSource_,
         int numberOfSources_,
         unsigned short geometryIndex_,
         unsigned int sourceType_,
         float barrageRate_,
         float barrageDuration_,
         float barrageCrosswindLength_,
         float barrageDownwindLength_,
         VelocityVectorStruct detonationVelocity_,
         std::vector</* 4 */ char > padding_
         )
         :
         timeSinceCreation(timeSinceCreation_),
         munitionSource(munitionSource_),
         numberOfSources(numberOfSources_),
         geometryIndex(geometryIndex_),
         sourceType(sourceType_),
         barrageRate(barrageRate_),
         barrageDuration(barrageDuration_),
         barrageCrosswindLength(barrageCrosswindLength_),
         barrageDownwindLength(barrageDownwindLength_),
         detonationVelocity(detonationVelocity_),
         padding(padding_)
      {}



      /**
      * Function to get timeSinceCreation.
      * <br>Description from the FOM: <i>Time since creation</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return timeSinceCreation
      */
      LIBAPI unsigned int & getTimeSinceCreation() {
         return timeSinceCreation;
      }

      /**
      * Function to get munitionSource.
      * <br>Description from the FOM: <i>Munition source</i>
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      *
      * @return munitionSource
      */
      LIBAPI DevStudio::EntityTypeStruct & getMunitionSource() {
         return munitionSource;
      }

      /**
      * Function to get numberOfSources.
      * <br>Description from the FOM: <i>Number of sources</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^31, 2^31-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return numberOfSources
      */
      LIBAPI int & getNumberOfSources() {
         return numberOfSources;
      }

      /**
      * Function to get geometryIndex.
      * <br>Description from the FOM: <i>Geometry index</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return geometryIndex
      */
      LIBAPI unsigned short & getGeometryIndex() {
         return geometryIndex;
      }

      /**
      * Function to get sourceType.
      * <br>Description from the FOM: <i>Source type</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return sourceType
      */
      LIBAPI unsigned int & getSourceType() {
         return sourceType;
      }

      /**
      * Function to get barrageRate.
      * <br>Description from the FOM: <i>Barrage rate</i>
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      *
      * @return barrageRate
      */
      LIBAPI float & getBarrageRate() {
         return barrageRate;
      }

      /**
      * Function to get barrageDuration.
      * <br>Description from the FOM: <i>Barrage duration</i>
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      *
      * @return barrageDuration
      */
      LIBAPI float & getBarrageDuration() {
         return barrageDuration;
      }

      /**
      * Function to get barrageCrosswindLength.
      * <br>Description from the FOM: <i>Barrage crosswind length</i>
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      *
      * @return barrageCrosswindLength
      */
      LIBAPI float & getBarrageCrosswindLength() {
         return barrageCrosswindLength;
      }

      /**
      * Function to get barrageDownwindLength.
      * <br>Description from the FOM: <i>Barrage downwind length</i>
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      *
      * @return barrageDownwindLength
      */
      LIBAPI float & getBarrageDownwindLength() {
         return barrageDownwindLength;
      }

      /**
      * Function to get detonationVelocity.
      * <br>Description from the FOM: <i>Detonation velocity</i>
      * <br>Description of the data type from the FOM: <i>The rate at which the position is changing over time.</i>
      *
      * @return detonationVelocity
      */
      LIBAPI DevStudio::VelocityVectorStruct & getDetonationVelocity() {
         return detonationVelocity;
      }

      /**
      * Function to get padding.
      * <br>Description from the FOM: <i>Padding field</i>
      * <br>Description of the data type from the FOM: <i>Generic array of four Octet elements.</i>
      *
      * @return padding
      */
      LIBAPI std::vector</* 4 */ char > & getPadding() {
         return padding;
      }

   };


   LIBAPI bool operator ==(const DevStudio::COMBICStateRecStruct& l, const DevStudio::COMBICStateRecStruct& r);
   LIBAPI bool operator !=(const DevStudio::COMBICStateRecStruct& l, const DevStudio::COMBICStateRecStruct& r);
   LIBAPI bool operator <(const DevStudio::COMBICStateRecStruct& l, const DevStudio::COMBICStateRecStruct& r);
   LIBAPI bool operator >(const DevStudio::COMBICStateRecStruct& l, const DevStudio::COMBICStateRecStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::COMBICStateRecStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::COMBICStateRecStruct const &);
}
#endif
