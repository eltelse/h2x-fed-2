/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_ENTITYCOORDINATEVECTORSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_ENTITYCOORDINATEVECTORSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>


namespace DevStudio {
   /**
   * Implementation of the <code>EntityCoordinateVectorStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Vector in the entity coordinate system. Based on the Entity Coordinate Vector record as specified in IEEE 1278.1-1995 section 5.2.33a.</i>
   */
   class EntityCoordinateVectorStruct {

   public:
      /**
      * Description from the FOM: <i>Component along the X axis</i>.
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      */
      float xComponent;
      /**
      * Description from the FOM: <i>Component along the Y axis</i>.
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      */
      float yComponent;
      /**
      * Description from the FOM: <i>Component along the Z axis</i>.
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      */
      float zComponent;

      LIBAPI EntityCoordinateVectorStruct()
         :
         xComponent(0),
         yComponent(0),
         zComponent(0)
      {}

      /**
      * Constructor for EntityCoordinateVectorStruct
      *
      * @param xComponent_ value to set as xComponent.
      * <br>Description from the FOM: <i>Component along the X axis</i>
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      * @param yComponent_ value to set as yComponent.
      * <br>Description from the FOM: <i>Component along the Y axis</i>
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      * @param zComponent_ value to set as zComponent.
      * <br>Description from the FOM: <i>Component along the Z axis</i>
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      */
      LIBAPI EntityCoordinateVectorStruct(
         float xComponent_,
         float yComponent_,
         float zComponent_
         )
         :
         xComponent(xComponent_),
         yComponent(yComponent_),
         zComponent(zComponent_)
      {}



      /**
      * Function to get xComponent.
      * <br>Description from the FOM: <i>Component along the X axis</i>
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      *
      * @return xComponent
      */
      LIBAPI float & getXComponent() {
         return xComponent;
      }

      /**
      * Function to get yComponent.
      * <br>Description from the FOM: <i>Component along the Y axis</i>
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      *
      * @return yComponent
      */
      LIBAPI float & getYComponent() {
         return yComponent;
      }

      /**
      * Function to get zComponent.
      * <br>Description from the FOM: <i>Component along the Z axis</i>
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      *
      * @return zComponent
      */
      LIBAPI float & getZComponent() {
         return zComponent;
      }

   };


   LIBAPI bool operator ==(const DevStudio::EntityCoordinateVectorStruct& l, const DevStudio::EntityCoordinateVectorStruct& r);
   LIBAPI bool operator !=(const DevStudio::EntityCoordinateVectorStruct& l, const DevStudio::EntityCoordinateVectorStruct& r);
   LIBAPI bool operator <(const DevStudio::EntityCoordinateVectorStruct& l, const DevStudio::EntityCoordinateVectorStruct& r);
   LIBAPI bool operator >(const DevStudio::EntityCoordinateVectorStruct& l, const DevStudio::EntityCoordinateVectorStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::EntityCoordinateVectorStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::EntityCoordinateVectorStruct const &);
}
#endif
