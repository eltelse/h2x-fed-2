/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_MINEFIELDPAINTSCHEMEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_MINEFIELDPAINTSCHEMEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace MinefieldPaintSchemeEnum {
        /**
        * Implementation of the <code>MinefieldPaintSchemeEnum32</code> data type from the FOM.
        * <br>Description from the FOM: <i>Minefield paint scheme</i>
        */
        enum MinefieldPaintSchemeEnum {
            /** <code>Other</code> (with ordinal 0) */
            OTHER = 0,
            /** <code>Standard</code> (with ordinal 1) */
            STANDARD = 1,
            /** <code>CamouflageDesert</code> (with ordinal 2) */
            CAMOUFLAGE_DESERT = 2,
            /** <code>CamouflageJungle</code> (with ordinal 3) */
            CAMOUFLAGE_JUNGLE = 3,
            /** <code>CamouflageSnow</code> (with ordinal 4) */
            CAMOUFLAGE_SNOW = 4,
            /** <code>CamouflageGravel</code> (with ordinal 5) */
            CAMOUFLAGE_GRAVEL = 5,
            /** <code>CamouflagePavement</code> (with ordinal 6) */
            CAMOUFLAGE_PAVEMENT = 6,
            /** <code>CamouflageSand</code> (with ordinal 7) */
            CAMOUFLAGE_SAND = 7,
            /** <code>NaturalWood</code> (with ordinal 8) */
            NATURAL_WOOD = 8,
            /** <code>Clear</code> (with ordinal 9) */
            CLEAR = 9,
            /** <code>Red</code> (with ordinal 10) */
            RED = 10,
            /** <code>Blue</code> (with ordinal 11) */
            BLUE = 11,
            /** <code>Green</code> (with ordinal 12) */
            GREEN = 12,
            /** <code>Olive</code> (with ordinal 13) */
            OLIVE = 13,
            /** <code>White</code> (with ordinal 14) */
            WHITE = 14,
            /** <code>Tan</code> (with ordinal 15) */
            TAN = 15,
            /** <code>Black</code> (with ordinal 16) */
            BLACK = 16,
            /** <code>Yellow</code> (with ordinal 17) */
            YELLOW = 17,
            /** <code>Brown</code> (with ordinal 18) */
            BROWN = 18
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to MinefieldPaintSchemeEnum: static_cast<DevStudio::MinefieldPaintSchemeEnum::MinefieldPaintSchemeEnum>(i)
        */
        LIBAPI bool isValid(const MinefieldPaintSchemeEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::MinefieldPaintSchemeEnum::MinefieldPaintSchemeEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::MinefieldPaintSchemeEnum::MinefieldPaintSchemeEnum const &);
}


#endif
