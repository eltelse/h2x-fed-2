/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_ANGLEMODULATIONTYPEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_ANGLEMODULATIONTYPEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace AngleModulationTypeEnum {
        /**
        * Implementation of the <code>AngleModulationTypeEnum16</code> data type from the FOM.
        * <br>Description from the FOM: <i>Detailed modulation types for Angle Modulation</i>
        */
        enum AngleModulationTypeEnum {
            /** <code>Other</code> (with ordinal 0) */
            OTHER = 0,
            /** <code>FrequencyModulation</code> (with ordinal 1) */
            FREQUENCY_MODULATION = 1,
            /** <code>FrequencyShiftKeying</code> (with ordinal 2) */
            FREQUENCY_SHIFT_KEYING = 2,
            /** <code>PhaseModulation</code> (with ordinal 3) */
            PHASE_MODULATION = 3
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to AngleModulationTypeEnum: static_cast<DevStudio::AngleModulationTypeEnum::AngleModulationTypeEnum>(i)
        */
        LIBAPI bool isValid(const AngleModulationTypeEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::AngleModulationTypeEnum::AngleModulationTypeEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::AngleModulationTypeEnum::AngleModulationTypeEnum const &);
}


#endif
