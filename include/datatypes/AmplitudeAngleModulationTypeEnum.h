/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_AMPLITUDEANGLEMODULATIONTYPEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_AMPLITUDEANGLEMODULATIONTYPEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace AmplitudeAngleModulationTypeEnum {
        /**
        * Implementation of the <code>AmplitudeAngleModulationTypeEnum16</code> data type from the FOM.
        * <br>Description from the FOM: <i>Detailed modulation types for Amplitude and Angle Modulation</i>
        */
        enum AmplitudeAngleModulationTypeEnum {
            /** <code>Other</code> (with ordinal 0) */
            OTHER = 0,
            /** <code>AmplitudeAndAngle</code> (with ordinal 1) */
            AMPLITUDE_AND_ANGLE = 1
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to AmplitudeAngleModulationTypeEnum: static_cast<DevStudio::AmplitudeAngleModulationTypeEnum::AmplitudeAngleModulationTypeEnum>(i)
        */
        LIBAPI bool isValid(const AmplitudeAngleModulationTypeEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::AmplitudeAngleModulationTypeEnum::AmplitudeAngleModulationTypeEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::AmplitudeAngleModulationTypeEnum::AmplitudeAngleModulationTypeEnum const &);
}


#endif
