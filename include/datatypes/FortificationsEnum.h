/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_FORTIFICATIONSENUM_H
#define DEVELOPER_STUDIO_DATATYPES_FORTIFICATIONSENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace FortificationsEnum {
        /**
        * Implementation of the <code>FortificationsEnum32</code> data type from the FOM.
        * <br>Description from the FOM: <i>-NULL-</i>
        */
        enum FortificationsEnum {
            /** <code>UNDEFINED</code> (with ordinal 0) */
            UNDEFINED = 0,
            /** <code>UNDERFROUND</code> (with ordinal 1) */
            UNDERFROUND = 1,
            /** <code>BUILDING</code> (with ordinal 2) */
            BUILDING = 2,
            /** <code>DUG_IN</code> (with ordinal 3) */
            DUG_IN = 3,
            /** <code>FORTIFICATIONS</code> (with ordinal 4) */
            FORTIFICATIONS = 4
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to FortificationsEnum: static_cast<DevStudio::FortificationsEnum::FortificationsEnum>(i)
        */
        LIBAPI bool isValid(const FortificationsEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::FortificationsEnum::FortificationsEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::FortificationsEnum::FortificationsEnum const &);
}


#endif
