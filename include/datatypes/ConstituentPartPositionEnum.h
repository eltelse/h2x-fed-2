/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_CONSTITUENTPARTPOSITIONENUM_H
#define DEVELOPER_STUDIO_DATATYPES_CONSTITUENTPARTPOSITIONENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace ConstituentPartPositionEnum {
        /**
        * Implementation of the <code>ConstituentPartPositionEnum16</code> data type from the FOM.
        * <br>Description from the FOM: <i>Relationship position</i>
        */
        enum ConstituentPartPositionEnum {
            /** <code>Other</code> (with ordinal 0) */
            OTHER = 0,
            /** <code>OnTopOf</code> (with ordinal 1) */
            ON_TOP_OF = 1,
            /** <code>Inside</code> (with ordinal 2) */
            INSIDE = 2
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to ConstituentPartPositionEnum: static_cast<DevStudio::ConstituentPartPositionEnum::ConstituentPartPositionEnum>(i)
        */
        LIBAPI bool isValid(const ConstituentPartPositionEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::ConstituentPartPositionEnum::ConstituentPartPositionEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::ConstituentPartPositionEnum::ConstituentPartPositionEnum const &);
}


#endif
