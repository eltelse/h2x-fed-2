/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_ENVIRONMENTRECORDTYPEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_ENVIRONMENTRECORDTYPEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace EnvironmentRecordTypeEnum {
        /**
        * Implementation of the <code>EnvironmentRecordTypeEnum32</code> data type from the FOM.
        * <br>Description from the FOM: <i>Environment record type</i>
        */
        enum EnvironmentRecordTypeEnum {
            /** <code>COMBICStateRecordType</code> (with ordinal 256) */
            COMBICSTATE_RECORD_TYPE = 256,
            /** <code>FlareStateRecordType</code> (with ordinal 259) */
            FLARE_STATE_RECORD_TYPE = 259,
            /** <code>BoundingSphereRecordType</code> (with ordinal 65536) */
            BOUNDING_SPHERE_RECORD_TYPE = 65536,
            /** <code>UniformGeometryRecordType</code> (with ordinal 327680) */
            UNIFORM_GEOMETRY_RECORD_TYPE = 327680,
            /** <code>PointRecord1Type</code> (with ordinal 655360) */
            POINT_RECORD1TYPE = 655360,
            /** <code>LineRecord1Type</code> (with ordinal 786432) */
            LINE_RECORD1TYPE = 786432,
            /** <code>SphereRecord1Type</code> (with ordinal 851968) */
            SPHERE_RECORD1TYPE = 851968,
            /** <code>EllipsoidRecord1Type</code> (with ordinal 1048576) */
            ELLIPSOID_RECORD1TYPE = 1048576,
            /** <code>ConeRecord1Type</code> (with ordinal 3145728) */
            CONE_RECORD1TYPE = 3145728,
            /** <code>RectangularVolRecord1Type</code> (with ordinal 5242880) */
            RECTANGULAR_VOL_RECORD1TYPE = 5242880,
            /** <code>RectangularVolRecord3Type</code> (with ordinal 83886080) */
            RECTANGULAR_VOL_RECORD3TYPE = 83886080,
            /** <code>PointRecord2Type</code> (with ordinal 167772160) */
            POINT_RECORD2TYPE = 167772160,
            /** <code>LineRecord2Type</code> (with ordinal 201326592) */
            LINE_RECORD2TYPE = 201326592,
            /** <code>SphereRecord2Type</code> (with ordinal 218103808) */
            SPHERE_RECORD2TYPE = 218103808,
            /** <code>EllipsoidRecord2Type</code> (with ordinal 268435456) */
            ELLIPSOID_RECORD2TYPE = 268435456,
            /** <code>ConeRecord2Type</code> (with ordinal 805306368) */
            CONE_RECORD2TYPE = 805306368,
            /** <code>RectangularVolRecord2Type</code> (with ordinal 1342177280) */
            RECTANGULAR_VOL_RECORD2TYPE = 1342177280,
            /** <code>GaussianPlumeRecordType</code> (with ordinal 1610612736) */
            GAUSSIAN_PLUME_RECORD_TYPE = 1610612736,
            /** <code>GaussianPuffRecordType</code> (with ordinal 1879048192) */
            GAUSSIAN_PUFF_RECORD_TYPE = 1879048192
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to EnvironmentRecordTypeEnum: static_cast<DevStudio::EnvironmentRecordTypeEnum::EnvironmentRecordTypeEnum>(i)
        */
        LIBAPI bool isValid(const EnvironmentRecordTypeEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::EnvironmentRecordTypeEnum::EnvironmentRecordTypeEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::EnvironmentRecordTypeEnum::EnvironmentRecordTypeEnum const &);
}


#endif
