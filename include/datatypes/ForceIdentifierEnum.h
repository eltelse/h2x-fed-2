/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_FORCEIDENTIFIERENUM_H
#define DEVELOPER_STUDIO_DATATYPES_FORCEIDENTIFIERENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace ForceIdentifierEnum {
        /**
        * Implementation of the <code>ForceIdentifierEnum8</code> data type from the FOM.
        * <br>Description from the FOM: <i>Force ID</i>
        */
        enum ForceIdentifierEnum {
            /** <code>Other</code> (with ordinal 0) */
            OTHER = 0,
            /** <code>Friendly</code> (with ordinal 1) */
            FRIENDLY = 1,
            /** <code>Opposing</code> (with ordinal 2) */
            OPPOSING = 2,
            /** <code>Neutral</code> (with ordinal 3) */
            NEUTRAL = 3,
            /** <code>Friendly_2</code> (with ordinal 4) */
            FRIENDLY_2 = 4,
            /** <code>Opposing_2</code> (with ordinal 5) */
            OPPOSING_2 = 5,
            /** <code>Neutral_2</code> (with ordinal 6) */
            NEUTRAL_2 = 6,
            /** <code>Friendly_3</code> (with ordinal 7) */
            FRIENDLY_3 = 7,
            /** <code>Opposing_3</code> (with ordinal 8) */
            OPPOSING_3 = 8,
            /** <code>Neutral_3</code> (with ordinal 9) */
            NEUTRAL_3 = 9,
            /** <code>Friendly_4</code> (with ordinal 10) */
            FRIENDLY_4 = 10,
            /** <code>Opposing_4</code> (with ordinal 11) */
            OPPOSING_4 = 11,
            /** <code>Neutral_4</code> (with ordinal 12) */
            NEUTRAL_4 = 12,
            /** <code>Friendly_5</code> (with ordinal 13) */
            FRIENDLY_5 = 13,
            /** <code>Opposing_5</code> (with ordinal 14) */
            OPPOSING_5 = 14,
            /** <code>Neutral_5</code> (with ordinal 15) */
            NEUTRAL_5 = 15,
            /** <code>Friendly_6</code> (with ordinal 16) */
            FRIENDLY_6 = 16,
            /** <code>Opposing_6</code> (with ordinal 17) */
            OPPOSING_6 = 17,
            /** <code>Neutral_6</code> (with ordinal 18) */
            NEUTRAL_6 = 18,
            /** <code>Friendly_7</code> (with ordinal 19) */
            FRIENDLY_7 = 19,
            /** <code>Opposing_7</code> (with ordinal 20) */
            OPPOSING_7 = 20,
            /** <code>Neutral_7</code> (with ordinal 21) */
            NEUTRAL_7 = 21,
            /** <code>Friendly_8</code> (with ordinal 22) */
            FRIENDLY_8 = 22,
            /** <code>Opposing_8</code> (with ordinal 23) */
            OPPOSING_8 = 23,
            /** <code>Neutral_8</code> (with ordinal 24) */
            NEUTRAL_8 = 24,
            /** <code>Friendly_9</code> (with ordinal 25) */
            FRIENDLY_9 = 25,
            /** <code>Opposing_9</code> (with ordinal 26) */
            OPPOSING_9 = 26,
            /** <code>Neutral_9</code> (with ordinal 27) */
            NEUTRAL_9 = 27,
            /** <code>Friendly_10</code> (with ordinal 28) */
            FRIENDLY_10 = 28,
            /** <code>Opposing_10</code> (with ordinal 29) */
            OPPOSING_10 = 29,
            /** <code>Neutral_10</code> (with ordinal 30) */
            NEUTRAL_10 = 30
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to ForceIdentifierEnum: static_cast<DevStudio::ForceIdentifierEnum::ForceIdentifierEnum>(i)
        */
        LIBAPI bool isValid(const ForceIdentifierEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::ForceIdentifierEnum::ForceIdentifierEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::ForceIdentifierEnum::ForceIdentifierEnum const &);
}


#endif
