/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_ELLIPSOID1GEOMRECSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_ELLIPSOID1GEOMRECSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/DimensionStruct.h>
#include <DevStudio/datatypes/OrientationStruct.h>
#include <DevStudio/datatypes/WorldLocationStruct.h>

namespace DevStudio {
   /**
   * Implementation of the <code>Ellipsoid1GeomRecStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Record specifying Ellipsoid 1 geometry record</i>
   */
   class Ellipsoid1GeomRecStruct {

   public:
      /**
      * Description from the FOM: <i>Centroid location X, Y, Z</i>.
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      */
      WorldLocationStruct centroidLocation;
      /**
      * Description from the FOM: <i>Sigma dimensions</i>.
      * <br>Description of the data type from the FOM: <i>Bounding box in X,Y,Z axis.</i>
      */
      DimensionStruct sigmaValue;
      /**
      * Description from the FOM: <i>Orientation, specified by Euler angles</i>.
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      */
      OrientationStruct orientation;

      LIBAPI Ellipsoid1GeomRecStruct()
         :
         centroidLocation(WorldLocationStruct()),
         sigmaValue(DimensionStruct()),
         orientation(OrientationStruct())
      {}

      /**
      * Constructor for Ellipsoid1GeomRecStruct
      *
      * @param centroidLocation_ value to set as centroidLocation.
      * <br>Description from the FOM: <i>Centroid location X, Y, Z</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      * @param sigmaValue_ value to set as sigmaValue.
      * <br>Description from the FOM: <i>Sigma dimensions</i>
      * <br>Description of the data type from the FOM: <i>Bounding box in X,Y,Z axis.</i>
      * @param orientation_ value to set as orientation.
      * <br>Description from the FOM: <i>Orientation, specified by Euler angles</i>
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      */
      LIBAPI Ellipsoid1GeomRecStruct(
         WorldLocationStruct centroidLocation_,
         DimensionStruct sigmaValue_,
         OrientationStruct orientation_
         )
         :
         centroidLocation(centroidLocation_),
         sigmaValue(sigmaValue_),
         orientation(orientation_)
      {}



      /**
      * Function to get centroidLocation.
      * <br>Description from the FOM: <i>Centroid location X, Y, Z</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @return centroidLocation
      */
      LIBAPI DevStudio::WorldLocationStruct & getCentroidLocation() {
         return centroidLocation;
      }

      /**
      * Function to get sigmaValue.
      * <br>Description from the FOM: <i>Sigma dimensions</i>
      * <br>Description of the data type from the FOM: <i>Bounding box in X,Y,Z axis.</i>
      *
      * @return sigmaValue
      */
      LIBAPI DevStudio::DimensionStruct & getSigmaValue() {
         return sigmaValue;
      }

      /**
      * Function to get orientation.
      * <br>Description from the FOM: <i>Orientation, specified by Euler angles</i>
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @return orientation
      */
      LIBAPI DevStudio::OrientationStruct & getOrientation() {
         return orientation;
      }

   };


   LIBAPI bool operator ==(const DevStudio::Ellipsoid1GeomRecStruct& l, const DevStudio::Ellipsoid1GeomRecStruct& r);
   LIBAPI bool operator !=(const DevStudio::Ellipsoid1GeomRecStruct& l, const DevStudio::Ellipsoid1GeomRecStruct& r);
   LIBAPI bool operator <(const DevStudio::Ellipsoid1GeomRecStruct& l, const DevStudio::Ellipsoid1GeomRecStruct& r);
   LIBAPI bool operator >(const DevStudio::Ellipsoid1GeomRecStruct& l, const DevStudio::Ellipsoid1GeomRecStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::Ellipsoid1GeomRecStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::Ellipsoid1GeomRecStruct const &);
}
#endif
