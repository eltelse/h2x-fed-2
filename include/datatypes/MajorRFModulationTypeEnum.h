/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_MAJORRFMODULATIONTYPEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_MAJORRFMODULATIONTYPEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace MajorRFModulationTypeEnum {
        /**
        * Implementation of the <code>MajorRFModulationTypeEnum16</code> data type from the FOM.
        * <br>Description from the FOM: <i>Major classification of the modulation type.</i>
        */
        enum MajorRFModulationTypeEnum {
            /** <code>Other</code> (with ordinal 0) */
            OTHER = 0,
            /** <code>Amplitude</code> (with ordinal 1) */
            AMPLITUDE = 1,
            /** <code>AmplitudeAndAngle</code> (with ordinal 2) */
            AMPLITUDE_AND_ANGLE = 2,
            /** <code>Angle</code> (with ordinal 3) */
            ANGLE = 3,
            /** <code>Combination</code> (with ordinal 4) */
            COMBINATION = 4,
            /** <code>Pulse</code> (with ordinal 5) */
            PULSE = 5,
            /** <code>Unmodulated</code> (with ordinal 6) */
            UNMODULATED = 6,
            /** <code>CarrierPhaseShiftModulation_CPSM_</code> (with ordinal 7) */
            CARRIER_PHASE_SHIFT_MODULATION_CPSM = 7
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to MajorRFModulationTypeEnum: static_cast<DevStudio::MajorRFModulationTypeEnum::MajorRFModulationTypeEnum>(i)
        */
        LIBAPI bool isValid(const MajorRFModulationTypeEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::MajorRFModulationTypeEnum::MajorRFModulationTypeEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::MajorRFModulationTypeEnum::MajorRFModulationTypeEnum const &);
}


#endif
