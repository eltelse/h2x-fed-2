/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_SINCGARSMODULATIONSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_SINCGARSMODULATIONSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>


namespace DevStudio {
   /**
   * Implementation of the <code>SINCGARSModulationStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Modulation parameters for SINCGARS radio system</i>
   */
   class SINCGARSModulationStruct {

   public:
      /**
      * Description from the FOM: <i>Frequency hopping network identifier.</i>.
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^15, 2^15-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      short fHNetID;
      /**
      * Description from the FOM: <i>Identification for the set of frequencies used when creating a hopping pattern.</i>.
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^15, 2^15-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      short hopSetID;
      /**
      * Description from the FOM: <i>Identification for the set of frequencies that are excluded from the hopping pattern.</i>.
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^15, 2^15-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      short lockoutSetID;
      /**
      * Description from the FOM: <i>Identifies the transmission security key that is used when generating the hopping pattern.</i>.
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^15, 2^15-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      short transmissionSecurityKey;
      /**
      * Description from the FOM: <i>The offset to exercise time in seconds for the clock in the radio.</i>.
      * <br>Description of the data type from the FOM: <i>Time, based on SI base unit second, unit symbol s. [unit: second (s), resolution: 1, accuracy: perfect]</i>
      */
      int fHSynchronizationTimeOffset;

      LIBAPI SINCGARSModulationStruct()
         :
         fHNetID(0),
         hopSetID(0),
         lockoutSetID(0),
         transmissionSecurityKey(0),
         fHSynchronizationTimeOffset(0)
      {}

      /**
      * Constructor for SINCGARSModulationStruct
      *
      * @param fHNetID_ value to set as fHNetID.
      * <br>Description from the FOM: <i>Frequency hopping network identifier.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^15, 2^15-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param hopSetID_ value to set as hopSetID.
      * <br>Description from the FOM: <i>Identification for the set of frequencies used when creating a hopping pattern.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^15, 2^15-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param lockoutSetID_ value to set as lockoutSetID.
      * <br>Description from the FOM: <i>Identification for the set of frequencies that are excluded from the hopping pattern.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^15, 2^15-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param transmissionSecurityKey_ value to set as transmissionSecurityKey.
      * <br>Description from the FOM: <i>Identifies the transmission security key that is used when generating the hopping pattern.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^15, 2^15-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param fHSynchronizationTimeOffset_ value to set as fHSynchronizationTimeOffset.
      * <br>Description from the FOM: <i>The offset to exercise time in seconds for the clock in the radio.</i>
      * <br>Description of the data type from the FOM: <i>Time, based on SI base unit second, unit symbol s. [unit: second (s), resolution: 1, accuracy: perfect]</i>
      */
      LIBAPI SINCGARSModulationStruct(
         short fHNetID_,
         short hopSetID_,
         short lockoutSetID_,
         short transmissionSecurityKey_,
         int fHSynchronizationTimeOffset_
         )
         :
         fHNetID(fHNetID_),
         hopSetID(hopSetID_),
         lockoutSetID(lockoutSetID_),
         transmissionSecurityKey(transmissionSecurityKey_),
         fHSynchronizationTimeOffset(fHSynchronizationTimeOffset_)
      {}



      /**
      * Function to get fHNetID.
      * <br>Description from the FOM: <i>Frequency hopping network identifier.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^15, 2^15-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return fHNetID
      */
      LIBAPI short & getFHNetID() {
         return fHNetID;
      }

      /**
      * Function to get hopSetID.
      * <br>Description from the FOM: <i>Identification for the set of frequencies used when creating a hopping pattern.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^15, 2^15-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return hopSetID
      */
      LIBAPI short & getHopSetID() {
         return hopSetID;
      }

      /**
      * Function to get lockoutSetID.
      * <br>Description from the FOM: <i>Identification for the set of frequencies that are excluded from the hopping pattern.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^15, 2^15-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return lockoutSetID
      */
      LIBAPI short & getLockoutSetID() {
         return lockoutSetID;
      }

      /**
      * Function to get transmissionSecurityKey.
      * <br>Description from the FOM: <i>Identifies the transmission security key that is used when generating the hopping pattern.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^15, 2^15-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return transmissionSecurityKey
      */
      LIBAPI short & getTransmissionSecurityKey() {
         return transmissionSecurityKey;
      }

      /**
      * Function to get fHSynchronizationTimeOffset.
      * <br>Description from the FOM: <i>The offset to exercise time in seconds for the clock in the radio.</i>
      * <br>Description of the data type from the FOM: <i>Time, based on SI base unit second, unit symbol s. [unit: second (s), resolution: 1, accuracy: perfect]</i>
      *
      * @return fHSynchronizationTimeOffset
      */
      LIBAPI int & getFHSynchronizationTimeOffset() {
         return fHSynchronizationTimeOffset;
      }

   };


   LIBAPI bool operator ==(const DevStudio::SINCGARSModulationStruct& l, const DevStudio::SINCGARSModulationStruct& r);
   LIBAPI bool operator !=(const DevStudio::SINCGARSModulationStruct& l, const DevStudio::SINCGARSModulationStruct& r);
   LIBAPI bool operator <(const DevStudio::SINCGARSModulationStruct& l, const DevStudio::SINCGARSModulationStruct& r);
   LIBAPI bool operator >(const DevStudio::SINCGARSModulationStruct& l, const DevStudio::SINCGARSModulationStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::SINCGARSModulationStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::SINCGARSModulationStruct const &);
}
#endif
