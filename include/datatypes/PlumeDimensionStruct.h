/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_PLUMEDIMENSIONSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_PLUMEDIMENSIONSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>


namespace DevStudio {
   /**
   * Implementation of the <code>PlumeDimensionStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Record specifying plume dimensions</i>
   */
   class PlumeDimensionStruct {

   public:
      /**
      * Description from the FOM: <i>Plume length</i>.
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      */
      float length;
      /**
      * Description from the FOM: <i>Plume width</i>.
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      */
      float width;
      /**
      * Description from the FOM: <i>Plume height</i>.
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      */
      float height;

      LIBAPI PlumeDimensionStruct()
         :
         length(0),
         width(0),
         height(0)
      {}

      /**
      * Constructor for PlumeDimensionStruct
      *
      * @param length_ value to set as length.
      * <br>Description from the FOM: <i>Plume length</i>
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      * @param width_ value to set as width.
      * <br>Description from the FOM: <i>Plume width</i>
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      * @param height_ value to set as height.
      * <br>Description from the FOM: <i>Plume height</i>
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      */
      LIBAPI PlumeDimensionStruct(
         float length_,
         float width_,
         float height_
         )
         :
         length(length_),
         width(width_),
         height(height_)
      {}



      /**
      * Function to get length.
      * <br>Description from the FOM: <i>Plume length</i>
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      *
      * @return length
      */
      LIBAPI float & getLength() {
         return length;
      }

      /**
      * Function to get width.
      * <br>Description from the FOM: <i>Plume width</i>
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      *
      * @return width
      */
      LIBAPI float & getWidth() {
         return width;
      }

      /**
      * Function to get height.
      * <br>Description from the FOM: <i>Plume height</i>
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      *
      * @return height
      */
      LIBAPI float & getHeight() {
         return height;
      }

   };


   LIBAPI bool operator ==(const DevStudio::PlumeDimensionStruct& l, const DevStudio::PlumeDimensionStruct& r);
   LIBAPI bool operator !=(const DevStudio::PlumeDimensionStruct& l, const DevStudio::PlumeDimensionStruct& r);
   LIBAPI bool operator <(const DevStudio::PlumeDimensionStruct& l, const DevStudio::PlumeDimensionStruct& r);
   LIBAPI bool operator >(const DevStudio::PlumeDimensionStruct& l, const DevStudio::PlumeDimensionStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::PlumeDimensionStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::PlumeDimensionStruct const &);
}
#endif
