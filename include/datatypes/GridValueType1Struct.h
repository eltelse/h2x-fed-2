/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_GRIDVALUETYPE1STRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_GRIDVALUETYPE1STRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/Integer16Array1Plus.h>
#include <DevStudio/datatypes/OctetPadding32Array.h>
#include <vector>

namespace DevStudio {
   /**
   * Implementation of the <code>GridValueType1Struct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Record specifying type 1 data representation</i>
   */
   class GridValueType1Struct {

   public:
      /**
      * Description from the FOM: <i>Specifies the constant scale factor used to scale the environmental state variable data values</i>.
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      */
      float scale;
      /**
      * Description from the FOM: <i>Specifies the constant offset used to scale the environmental state variable data values</i>.
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      */
      float offset;
      /**
      * Description from the FOM: <i>Specifies the number of environmental state variable data values</i>.
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of Integer16 elements, containing at least one element.</i>
      */
      std::vector</* not empty */ short > numberOfValuesAValues;
      /**
      * Description from the FOM: <i>Brings the record length to a 32-bit boundary</i>.
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of meaningless Octet elements, to align the subsequent data structure to the next 32 bit octet boundary value (OBV). The array is encoded without array length, containing zero to three elements.</i>
      */
      std::vector<char > paddingTo32;

      LIBAPI GridValueType1Struct()
         :
         scale(0),
         offset(0),
         numberOfValuesAValues(0),
         paddingTo32(0)
      {}

      /**
      * Constructor for GridValueType1Struct
      *
      * @param scale_ value to set as scale.
      * <br>Description from the FOM: <i>Specifies the constant scale factor used to scale the environmental state variable data values</i>
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      * @param offset_ value to set as offset.
      * <br>Description from the FOM: <i>Specifies the constant offset used to scale the environmental state variable data values</i>
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      * @param numberOfValuesAValues_ value to set as numberOfValuesAValues.
      * <br>Description from the FOM: <i>Specifies the number of environmental state variable data values</i>
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of Integer16 elements, containing at least one element.</i>
      * @param paddingTo32_ value to set as paddingTo32.
      * <br>Description from the FOM: <i>Brings the record length to a 32-bit boundary</i>
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of meaningless Octet elements, to align the subsequent data structure to the next 32 bit octet boundary value (OBV). The array is encoded without array length, containing zero to three elements.</i>
      */
      LIBAPI GridValueType1Struct(
         float scale_,
         float offset_,
         std::vector</* not empty */ short > numberOfValuesAValues_,
         std::vector<char > paddingTo32_
         )
         :
         scale(scale_),
         offset(offset_),
         numberOfValuesAValues(numberOfValuesAValues_),
         paddingTo32(paddingTo32_)
      {}



      /**
      * Function to get scale.
      * <br>Description from the FOM: <i>Specifies the constant scale factor used to scale the environmental state variable data values</i>
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      *
      * @return scale
      */
      LIBAPI float & getScale() {
         return scale;
      }

      /**
      * Function to get offset.
      * <br>Description from the FOM: <i>Specifies the constant offset used to scale the environmental state variable data values</i>
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      *
      * @return offset
      */
      LIBAPI float & getOffset() {
         return offset;
      }

      /**
      * Function to get numberOfValuesAValues.
      * <br>Description from the FOM: <i>Specifies the number of environmental state variable data values</i>
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of Integer16 elements, containing at least one element.</i>
      *
      * @return numberOfValuesAValues
      */
      LIBAPI std::vector</* not empty */ short > & getNumberOfValuesAValues() {
         return numberOfValuesAValues;
      }

      /**
      * Function to get paddingTo32.
      * <br>Description from the FOM: <i>Brings the record length to a 32-bit boundary</i>
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of meaningless Octet elements, to align the subsequent data structure to the next 32 bit octet boundary value (OBV). The array is encoded without array length, containing zero to three elements.</i>
      *
      * @return paddingTo32
      */
      LIBAPI std::vector<char > & getPaddingTo32() {
         return paddingTo32;
      }

   };


   LIBAPI bool operator ==(const DevStudio::GridValueType1Struct& l, const DevStudio::GridValueType1Struct& r);
   LIBAPI bool operator !=(const DevStudio::GridValueType1Struct& l, const DevStudio::GridValueType1Struct& r);
   LIBAPI bool operator <(const DevStudio::GridValueType1Struct& l, const DevStudio::GridValueType1Struct& r);
   LIBAPI bool operator >(const DevStudio::GridValueType1Struct& l, const DevStudio::GridValueType1Struct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::GridValueType1Struct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::GridValueType1Struct const &);
}
#endif
