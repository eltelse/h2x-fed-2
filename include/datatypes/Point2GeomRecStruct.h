/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_POINT2GEOMRECSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_POINT2GEOMRECSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/OctetArray4.h>
#include <DevStudio/datatypes/VelocityVectorStruct.h>
#include <DevStudio/datatypes/WorldLocationStruct.h>
#include <vector>

namespace DevStudio {
   /**
   * Implementation of the <code>Point2GeomRecStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Record specifying Point 2 geometry record</i>
   */
   class Point2GeomRecStruct {

   public:
      /**
      * Description from the FOM: <i>Location X, Y, Z</i>.
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      */
      WorldLocationStruct location;
      /**
      * Description from the FOM: <i>Velocity Vx, Vy, Vz</i>.
      * <br>Description of the data type from the FOM: <i>The rate at which the position is changing over time.</i>
      */
      VelocityVectorStruct velocity;
      /**
      * Description from the FOM: <i>Padding field</i>.
      * <br>Description of the data type from the FOM: <i>Generic array of four Octet elements.</i>
      */
      std::vector</* 4 */ char > padding;

      LIBAPI Point2GeomRecStruct()
         :
         location(WorldLocationStruct()),
         velocity(VelocityVectorStruct()),
         padding(0)
      {}

      /**
      * Constructor for Point2GeomRecStruct
      *
      * @param location_ value to set as location.
      * <br>Description from the FOM: <i>Location X, Y, Z</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      * @param velocity_ value to set as velocity.
      * <br>Description from the FOM: <i>Velocity Vx, Vy, Vz</i>
      * <br>Description of the data type from the FOM: <i>The rate at which the position is changing over time.</i>
      * @param padding_ value to set as padding.
      * <br>Description from the FOM: <i>Padding field</i>
      * <br>Description of the data type from the FOM: <i>Generic array of four Octet elements.</i>
      */
      LIBAPI Point2GeomRecStruct(
         WorldLocationStruct location_,
         VelocityVectorStruct velocity_,
         std::vector</* 4 */ char > padding_
         )
         :
         location(location_),
         velocity(velocity_),
         padding(padding_)
      {}



      /**
      * Function to get location.
      * <br>Description from the FOM: <i>Location X, Y, Z</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @return location
      */
      LIBAPI DevStudio::WorldLocationStruct & getLocation() {
         return location;
      }

      /**
      * Function to get velocity.
      * <br>Description from the FOM: <i>Velocity Vx, Vy, Vz</i>
      * <br>Description of the data type from the FOM: <i>The rate at which the position is changing over time.</i>
      *
      * @return velocity
      */
      LIBAPI DevStudio::VelocityVectorStruct & getVelocity() {
         return velocity;
      }

      /**
      * Function to get padding.
      * <br>Description from the FOM: <i>Padding field</i>
      * <br>Description of the data type from the FOM: <i>Generic array of four Octet elements.</i>
      *
      * @return padding
      */
      LIBAPI std::vector</* 4 */ char > & getPadding() {
         return padding;
      }

   };


   LIBAPI bool operator ==(const DevStudio::Point2GeomRecStruct& l, const DevStudio::Point2GeomRecStruct& r);
   LIBAPI bool operator !=(const DevStudio::Point2GeomRecStruct& l, const DevStudio::Point2GeomRecStruct& r);
   LIBAPI bool operator <(const DevStudio::Point2GeomRecStruct& l, const DevStudio::Point2GeomRecStruct& r);
   LIBAPI bool operator >(const DevStudio::Point2GeomRecStruct& l, const DevStudio::Point2GeomRecStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::Point2GeomRecStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::Point2GeomRecStruct const &);
}
#endif
