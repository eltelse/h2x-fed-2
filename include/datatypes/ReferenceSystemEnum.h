/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_REFERENCESYSTEMENUM_H
#define DEVELOPER_STUDIO_DATATYPES_REFERENCESYSTEMENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace ReferenceSystemEnum {
        /**
        * Implementation of the <code>ReferenceSystemEnum8</code> data type from the FOM.
        * <br>Description from the FOM: <i>The reference coordinate system used</i>
        */
        enum ReferenceSystemEnum {
            /** <code>WorldCoordinates</code> (with ordinal 1) */
            WORLD_COORDINATES = 1,
            /** <code>EntityCoordinates</code> (with ordinal 2) */
            ENTITY_COORDINATES = 2
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to ReferenceSystemEnum: static_cast<DevStudio::ReferenceSystemEnum::ReferenceSystemEnum>(i)
        */
        LIBAPI bool isValid(const ReferenceSystemEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::ReferenceSystemEnum::ReferenceSystemEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::ReferenceSystemEnum::ReferenceSystemEnum const &);
}


#endif
