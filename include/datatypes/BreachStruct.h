/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_BREACHSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_BREACHSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/LinearSegmentStruct.h>
#include <DevStudio/datatypes/OctetArray4.h>
#include <vector>

namespace DevStudio {
   /**
   * Implementation of the <code>BreachStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Record specifying the characteristics of a breach linear object segment</i>
   */
   class BreachStruct {

   public:
      /**
      * Description from the FOM: <i>Specifies the breach linear object segment characteristics</i>.
      * <br>Description of the data type from the FOM: <i>Specifies linear object segment characteristics.</i>
      */
      LinearSegmentStruct segmentParameters;
      /**
      * Description from the FOM: <i>Padding to 64 bits</i>.
      * <br>Description of the data type from the FOM: <i>Generic array of four Octet elements.</i>
      */
      std::vector</* 4 */ char > padding;

      LIBAPI BreachStruct()
         :
         segmentParameters(LinearSegmentStruct()),
         padding(0)
      {}

      /**
      * Constructor for BreachStruct
      *
      * @param segmentParameters_ value to set as segmentParameters.
      * <br>Description from the FOM: <i>Specifies the breach linear object segment characteristics</i>
      * <br>Description of the data type from the FOM: <i>Specifies linear object segment characteristics.</i>
      * @param padding_ value to set as padding.
      * <br>Description from the FOM: <i>Padding to 64 bits</i>
      * <br>Description of the data type from the FOM: <i>Generic array of four Octet elements.</i>
      */
      LIBAPI BreachStruct(
         LinearSegmentStruct segmentParameters_,
         std::vector</* 4 */ char > padding_
         )
         :
         segmentParameters(segmentParameters_),
         padding(padding_)
      {}



      /**
      * Function to get segmentParameters.
      * <br>Description from the FOM: <i>Specifies the breach linear object segment characteristics</i>
      * <br>Description of the data type from the FOM: <i>Specifies linear object segment characteristics.</i>
      *
      * @return segmentParameters
      */
      LIBAPI DevStudio::LinearSegmentStruct & getSegmentParameters() {
         return segmentParameters;
      }

      /**
      * Function to get padding.
      * <br>Description from the FOM: <i>Padding to 64 bits</i>
      * <br>Description of the data type from the FOM: <i>Generic array of four Octet elements.</i>
      *
      * @return padding
      */
      LIBAPI std::vector</* 4 */ char > & getPadding() {
         return padding;
      }

   };


   LIBAPI bool operator ==(const DevStudio::BreachStruct& l, const DevStudio::BreachStruct& r);
   LIBAPI bool operator !=(const DevStudio::BreachStruct& l, const DevStudio::BreachStruct& r);
   LIBAPI bool operator <(const DevStudio::BreachStruct& l, const DevStudio::BreachStruct& r);
   LIBAPI bool operator >(const DevStudio::BreachStruct& l, const DevStudio::BreachStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::BreachStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::BreachStruct const &);
}
#endif
