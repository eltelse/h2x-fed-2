/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_RESPONSEFLAGENUM_H
#define DEVELOPER_STUDIO_DATATYPES_RESPONSEFLAGENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace ResponseFlagEnum {
        /**
        * Implementation of the <code>ResponseFlagEnum16</code> data type from the FOM.
        * <br>Description from the FOM: <i>Response flag</i>
        */
        enum ResponseFlagEnum {
            /** <code>Other</code> (with ordinal 0) */
            OTHER = 0,
            /** <code>AbleToComply</code> (with ordinal 1) */
            ABLE_TO_COMPLY = 1,
            /** <code>UnableToComply</code> (with ordinal 2) */
            UNABLE_TO_COMPLY = 2,
            /** <code>PendingOperatorAction</code> (with ordinal 3) */
            PENDING_OPERATOR_ACTION = 3
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to ResponseFlagEnum: static_cast<DevStudio::ResponseFlagEnum::ResponseFlagEnum>(i)
        */
        LIBAPI bool isValid(const ResponseFlagEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::ResponseFlagEnum::ResponseFlagEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::ResponseFlagEnum::ResponseFlagEnum const &);
}


#endif
