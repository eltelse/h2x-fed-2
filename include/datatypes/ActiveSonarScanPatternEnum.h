/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_ACTIVESONARSCANPATTERNENUM_H
#define DEVELOPER_STUDIO_DATATYPES_ACTIVESONARSCANPATTERNENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace ActiveSonarScanPatternEnum {
        /**
        * Implementation of the <code>ActiveSonarScanPatternEnum16</code> data type from the FOM.
        * <br>Description from the FOM: <i>Acoustic scan pattern</i>
        */
        enum ActiveSonarScanPatternEnum {
            /** <code>ScanPatternNotUsed</code> (with ordinal 0) */
            SCAN_PATTERN_NOT_USED = 0,
            /** <code>Conical</code> (with ordinal 1) */
            CONICAL = 1,
            /** <code>Helical</code> (with ordinal 2) */
            HELICAL = 2,
            /** <code>Raster</code> (with ordinal 3) */
            RASTER = 3,
            /** <code>SectorSearch</code> (with ordinal 4) */
            SECTOR_SEARCH = 4,
            /** <code>ContinuousSearch</code> (with ordinal 5) */
            CONTINUOUS_SEARCH = 5
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to ActiveSonarScanPatternEnum: static_cast<DevStudio::ActiveSonarScanPatternEnum::ActiveSonarScanPatternEnum>(i)
        */
        LIBAPI bool isValid(const ActiveSonarScanPatternEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::ActiveSonarScanPatternEnum::ActiveSonarScanPatternEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::ActiveSonarScanPatternEnum::ActiveSonarScanPatternEnum const &);
}


#endif
