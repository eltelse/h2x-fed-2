/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_HOSTILITYTYPEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_HOSTILITYTYPEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace HostilityTypeEnum {
        /**
        * Implementation of the <code>HostilityTypeEnum32</code> data type from the FOM.
        * <br>Description from the FOM: <i>-NULL-</i>
        */
        enum HostilityTypeEnum {
            /** <code>HOSTILITY_UNKNOWN</code> (with ordinal 0) */
            HOSTILITY_UNKNOWN = 0,
            /** <code>HOSTILITY_NONE</code> (with ordinal 1) */
            HOSTILITY_NONE = 1,
            /** <code>HOSTILITY_THREAT</code> (with ordinal 2) */
            HOSTILITY_THREAT = 2,
            /** <code>HOSTILITY_ENEMY</code> (with ordinal 3) */
            HOSTILITY_ENEMY = 3,
            /** <code>HOSTILITY_NEUTRAL</code> (with ordinal 4) */
            HOSTILITY_NEUTRAL = 4,
            /** <code>HOSTILITY_ALLY</code> (with ordinal 5) */
            HOSTILITY_ALLY = 5,
            /** <code>HOSTILITY_FRIENDLY</code> (with ordinal 6) */
            HOSTILITY_FRIENDLY = 6
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to HostilityTypeEnum: static_cast<DevStudio::HostilityTypeEnum::HostilityTypeEnum>(i)
        */
        LIBAPI bool isValid(const HostilityTypeEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::HostilityTypeEnum::HostilityTypeEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::HostilityTypeEnum::HostilityTypeEnum const &);
}


#endif
