/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_ENTITYTYPESTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_ENTITYTYPESTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>


namespace DevStudio {
   /**
   * Implementation of the <code>EntityTypeStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
   */
   class EntityTypeStruct {

   public:
      /**
      * Description from the FOM: <i>Kind of entity.</i>.
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      char entityKind;
      /**
      * Description from the FOM: <i>Domain in which the entity operates.</i>.
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      char domain;
      /**
      * Description from the FOM: <i>Country to which the design of the entity is attributed.</i>.
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      unsigned short countryCode;
      /**
      * Description from the FOM: <i>Main category that describes the entity.</i>.
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      char category;
      /**
      * Description from the FOM: <i>Subcategory to which an entity belongs based on the Category field.</i>.
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      char subcategory;
      /**
      * Description from the FOM: <i>Specific information about an entity based on the Subcategory field.</i>.
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      char specific;
      /**
      * Description from the FOM: <i>Extra information required to describe a particular entity.</i>.
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      char extra;

      LIBAPI EntityTypeStruct()
         :
         entityKind(0),
         domain(0),
         countryCode(0),
         category(0),
         subcategory(0),
         specific(0),
         extra(0)
      {}

      /**
      * Constructor for EntityTypeStruct
      *
      * @param entityKind_ value to set as entityKind.
      * <br>Description from the FOM: <i>Kind of entity.</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param domain_ value to set as domain.
      * <br>Description from the FOM: <i>Domain in which the entity operates.</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param countryCode_ value to set as countryCode.
      * <br>Description from the FOM: <i>Country to which the design of the entity is attributed.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param category_ value to set as category.
      * <br>Description from the FOM: <i>Main category that describes the entity.</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param subcategory_ value to set as subcategory.
      * <br>Description from the FOM: <i>Subcategory to which an entity belongs based on the Category field.</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param specific_ value to set as specific.
      * <br>Description from the FOM: <i>Specific information about an entity based on the Subcategory field.</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param extra_ value to set as extra.
      * <br>Description from the FOM: <i>Extra information required to describe a particular entity.</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      LIBAPI EntityTypeStruct(
         char entityKind_,
         char domain_,
         unsigned short countryCode_,
         char category_,
         char subcategory_,
         char specific_,
         char extra_
         )
         :
         entityKind(entityKind_),
         domain(domain_),
         countryCode(countryCode_),
         category(category_),
         subcategory(subcategory_),
         specific(specific_),
         extra(extra_)
      {}



      /**
      * Function to get entityKind.
      * <br>Description from the FOM: <i>Kind of entity.</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return entityKind
      */
      LIBAPI char & getEntityKind() {
         return entityKind;
      }

      /**
      * Function to get domain.
      * <br>Description from the FOM: <i>Domain in which the entity operates.</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return domain
      */
      LIBAPI char & getDomain() {
         return domain;
      }

      /**
      * Function to get countryCode.
      * <br>Description from the FOM: <i>Country to which the design of the entity is attributed.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return countryCode
      */
      LIBAPI unsigned short & getCountryCode() {
         return countryCode;
      }

      /**
      * Function to get category.
      * <br>Description from the FOM: <i>Main category that describes the entity.</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return category
      */
      LIBAPI char & getCategory() {
         return category;
      }

      /**
      * Function to get subcategory.
      * <br>Description from the FOM: <i>Subcategory to which an entity belongs based on the Category field.</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return subcategory
      */
      LIBAPI char & getSubcategory() {
         return subcategory;
      }

      /**
      * Function to get specific.
      * <br>Description from the FOM: <i>Specific information about an entity based on the Subcategory field.</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return specific
      */
      LIBAPI char & getSpecific() {
         return specific;
      }

      /**
      * Function to get extra.
      * <br>Description from the FOM: <i>Extra information required to describe a particular entity.</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return extra
      */
      LIBAPI char & getExtra() {
         return extra;
      }

   };


   LIBAPI bool operator ==(const DevStudio::EntityTypeStruct& l, const DevStudio::EntityTypeStruct& r);
   LIBAPI bool operator !=(const DevStudio::EntityTypeStruct& l, const DevStudio::EntityTypeStruct& r);
   LIBAPI bool operator <(const DevStudio::EntityTypeStruct& l, const DevStudio::EntityTypeStruct& r);
   LIBAPI bool operator >(const DevStudio::EntityTypeStruct& l, const DevStudio::EntityTypeStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::EntityTypeStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::EntityTypeStruct const &);
}
#endif
