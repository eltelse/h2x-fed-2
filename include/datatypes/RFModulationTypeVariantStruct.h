/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_RFMODULATIONTYPEVARIANTSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_RFMODULATIONTYPEVARIANTSTRUCT_H

#include <DevStudio/datatypes/AmplitudeAngleModulationTypeEnum.h>
#include <DevStudio/datatypes/AmplitudeModulationTypeEnum.h>
#include <DevStudio/datatypes/AngleModulationTypeEnum.h>
#include <DevStudio/datatypes/CombinationModulationTypeEnum.h>
#include <DevStudio/datatypes/MajorRFModulationTypeEnum.h>
#include <DevStudio/datatypes/PulseModulationTypeEnum.h>
#include <DevStudio/datatypes/UnmodulatedTypeEnum.h>

#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
   /**
   * Implementation of the <code>RFModulationTypeVariantStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Specifies the major modulation type as well as certain detailed information specific to the type.</i>
   */
   class RFModulationTypeVariantStruct {

   public:
      LIBAPI RFModulationTypeVariantStruct()
         : 
         _discriminant(DevStudio::MajorRFModulationTypeEnum::MajorRFModulationTypeEnum())
      {}

      /** 
      * Create a new alternative RFModulationTypeVariantStruct, with <code>Amplitude</code> as discriminant.
      *
      * @param amplitudeModulationType value of the AmplitudeModulationType field
      *
      * @return a new RFModulationTypeVariantStruct
      */
      LIBAPI static RFModulationTypeVariantStruct createAmplitudeModulationType(DevStudio::AmplitudeModulationTypeEnum::AmplitudeModulationTypeEnum amplitudeModulationType);

      /** 
      * Create a new alternative RFModulationTypeVariantStruct, with <code>AmplitudeAndAngle</code> as discriminant.
      *
      * @param amplitudeAngleModulationType value of the AmplitudeAngleModulationType field
      *
      * @return a new RFModulationTypeVariantStruct
      */
      LIBAPI static RFModulationTypeVariantStruct createAmplitudeAngleModulationType(DevStudio::AmplitudeAngleModulationTypeEnum::AmplitudeAngleModulationTypeEnum amplitudeAngleModulationType);

      /** 
      * Create a new alternative RFModulationTypeVariantStruct, with <code>Angle</code> as discriminant.
      *
      * @param angleModulationType value of the AngleModulationType field
      *
      * @return a new RFModulationTypeVariantStruct
      */
      LIBAPI static RFModulationTypeVariantStruct createAngleModulationType(DevStudio::AngleModulationTypeEnum::AngleModulationTypeEnum angleModulationType);

      /** 
      * Create a new alternative RFModulationTypeVariantStruct, with <code>Combination</code> as discriminant.
      *
      * @param combinationModulationType value of the CombinationModulationType field
      *
      * @return a new RFModulationTypeVariantStruct
      */
      LIBAPI static RFModulationTypeVariantStruct createCombinationModulationType(DevStudio::CombinationModulationTypeEnum::CombinationModulationTypeEnum combinationModulationType);

      /** 
      * Create a new alternative RFModulationTypeVariantStruct, with <code>Pulse</code> as discriminant.
      *
      * @param pulseModulationType value of the PulseModulationType field
      *
      * @return a new RFModulationTypeVariantStruct
      */
      LIBAPI static RFModulationTypeVariantStruct createPulseModulationType(DevStudio::PulseModulationTypeEnum::PulseModulationTypeEnum pulseModulationType);

      /** 
      * Create a new alternative RFModulationTypeVariantStruct, with <code>Unmodulated</code> as discriminant.
      *
      * @param unmodulatedType value of the UnmodulatedType field
      *
      * @return a new RFModulationTypeVariantStruct
      */
      LIBAPI static RFModulationTypeVariantStruct createUnmodulatedType(DevStudio::UnmodulatedTypeEnum::UnmodulatedTypeEnum unmodulatedType);

      /**
      * Create a new alternative RFModulationTypeVariantStruct, with <code>Other</code> as discriminant.
      *
      * @return a new RFModulationTypeVariantStruct
      */
      LIBAPI static RFModulationTypeVariantStruct createOther();

      /**
      * Create a new alternative RFModulationTypeVariantStruct, with <code>CarrierPhaseShiftModulation_CPSM_</code> as discriminant.
      *
      * @return a new RFModulationTypeVariantStruct
      */
      LIBAPI static RFModulationTypeVariantStruct createCarrierPhaseShiftModulation_CPSM_();



      /**
      * Function to get discriminant
      *
      * @return disciminant
      */
      LIBAPI DevStudio::MajorRFModulationTypeEnum::MajorRFModulationTypeEnum getDiscriminant() const {
          return _discriminant;
      }

      /**
      * Function to get AmplitudeModulationType.
      * Note that this field is only valid of the discriminant is <code>Amplitude</code>.
      * <br>Description from the FOM: <i>Detailed modulation type for the amplitude major modulation type.</i>
      * <br>Description of the data type from the FOM: <i>Detailed modulation types for Amplitude Modulation</i>
      *
      * @return AmplitudeModulationType value
      */
      LIBAPI DevStudio::AmplitudeModulationTypeEnumPtr getAmplitudeModulationType() const {
         return _amplitudeModulationType;
      }

      /**
      * Function to set AmplitudeModulationType.
      * Note that this will set the discriminant to <code>Amplitude</code>.
      * <br>Description from the FOM: <i>Detailed modulation type for the amplitude major modulation type.</i>
      * <br>Description of the data type from the FOM: <i>Detailed modulation types for Amplitude Modulation</i>
      *
      * @param amplitudeModulationType value used to create a AmplitudeModulationTypeEnumPtr
      */
      LIBAPI void setAmplitudeModulationType(const DevStudio::AmplitudeModulationTypeEnum::AmplitudeModulationTypeEnum amplitudeModulationType) {
         _amplitudeModulationType = DevStudio::AmplitudeModulationTypeEnumPtr( new DevStudio::AmplitudeModulationTypeEnum::AmplitudeModulationTypeEnum (amplitudeModulationType));
         _discriminant = MajorRFModulationTypeEnum::AMPLITUDE;
      }

      /**
      * Function to get AmplitudeAngleModulationType.
      * Note that this field is only valid of the discriminant is <code>AmplitudeAndAngle</code>.
      * <br>Description from the FOM: <i>Detailed modulation type for the amplitude and angle major modulation type.</i>
      * <br>Description of the data type from the FOM: <i>Detailed modulation types for Amplitude and Angle Modulation</i>
      *
      * @return AmplitudeAngleModulationType value
      */
      LIBAPI DevStudio::AmplitudeAngleModulationTypeEnumPtr getAmplitudeAngleModulationType() const {
         return _amplitudeAngleModulationType;
      }

      /**
      * Function to set AmplitudeAngleModulationType.
      * Note that this will set the discriminant to <code>AmplitudeAndAngle</code>.
      * <br>Description from the FOM: <i>Detailed modulation type for the amplitude and angle major modulation type.</i>
      * <br>Description of the data type from the FOM: <i>Detailed modulation types for Amplitude and Angle Modulation</i>
      *
      * @param amplitudeAngleModulationType value used to create a AmplitudeAngleModulationTypeEnumPtr
      */
      LIBAPI void setAmplitudeAngleModulationType(const DevStudio::AmplitudeAngleModulationTypeEnum::AmplitudeAngleModulationTypeEnum amplitudeAngleModulationType) {
         _amplitudeAngleModulationType = DevStudio::AmplitudeAngleModulationTypeEnumPtr( new DevStudio::AmplitudeAngleModulationTypeEnum::AmplitudeAngleModulationTypeEnum (amplitudeAngleModulationType));
         _discriminant = MajorRFModulationTypeEnum::AMPLITUDE_AND_ANGLE;
      }

      /**
      * Function to get AngleModulationType.
      * Note that this field is only valid of the discriminant is <code>Angle</code>.
      * <br>Description from the FOM: <i>Detailed modulation type for the angle major modulation type.</i>
      * <br>Description of the data type from the FOM: <i>Detailed modulation types for Angle Modulation</i>
      *
      * @return AngleModulationType value
      */
      LIBAPI DevStudio::AngleModulationTypeEnumPtr getAngleModulationType() const {
         return _angleModulationType;
      }

      /**
      * Function to set AngleModulationType.
      * Note that this will set the discriminant to <code>Angle</code>.
      * <br>Description from the FOM: <i>Detailed modulation type for the angle major modulation type.</i>
      * <br>Description of the data type from the FOM: <i>Detailed modulation types for Angle Modulation</i>
      *
      * @param angleModulationType value used to create a AngleModulationTypeEnumPtr
      */
      LIBAPI void setAngleModulationType(const DevStudio::AngleModulationTypeEnum::AngleModulationTypeEnum angleModulationType) {
         _angleModulationType = DevStudio::AngleModulationTypeEnumPtr( new DevStudio::AngleModulationTypeEnum::AngleModulationTypeEnum (angleModulationType));
         _discriminant = MajorRFModulationTypeEnum::ANGLE;
      }

      /**
      * Function to get CombinationModulationType.
      * Note that this field is only valid of the discriminant is <code>Combination</code>.
      * <br>Description from the FOM: <i>Detailed modulation type for the combination major modulation type.</i>
      * <br>Description of the data type from the FOM: <i>Detailed modulation types for Combination Modulation</i>
      *
      * @return CombinationModulationType value
      */
      LIBAPI DevStudio::CombinationModulationTypeEnumPtr getCombinationModulationType() const {
         return _combinationModulationType;
      }

      /**
      * Function to set CombinationModulationType.
      * Note that this will set the discriminant to <code>Combination</code>.
      * <br>Description from the FOM: <i>Detailed modulation type for the combination major modulation type.</i>
      * <br>Description of the data type from the FOM: <i>Detailed modulation types for Combination Modulation</i>
      *
      * @param combinationModulationType value used to create a CombinationModulationTypeEnumPtr
      */
      LIBAPI void setCombinationModulationType(const DevStudio::CombinationModulationTypeEnum::CombinationModulationTypeEnum combinationModulationType) {
         _combinationModulationType = DevStudio::CombinationModulationTypeEnumPtr( new DevStudio::CombinationModulationTypeEnum::CombinationModulationTypeEnum (combinationModulationType));
         _discriminant = MajorRFModulationTypeEnum::COMBINATION;
      }

      /**
      * Function to get PulseModulationType.
      * Note that this field is only valid of the discriminant is <code>Pulse</code>.
      * <br>Description from the FOM: <i>Detailed modulation type for the pulse major modulation type.</i>
      * <br>Description of the data type from the FOM: <i>Detailed modulation types for Pulse Modulation</i>
      *
      * @return PulseModulationType value
      */
      LIBAPI DevStudio::PulseModulationTypeEnumPtr getPulseModulationType() const {
         return _pulseModulationType;
      }

      /**
      * Function to set PulseModulationType.
      * Note that this will set the discriminant to <code>Pulse</code>.
      * <br>Description from the FOM: <i>Detailed modulation type for the pulse major modulation type.</i>
      * <br>Description of the data type from the FOM: <i>Detailed modulation types for Pulse Modulation</i>
      *
      * @param pulseModulationType value used to create a PulseModulationTypeEnumPtr
      */
      LIBAPI void setPulseModulationType(const DevStudio::PulseModulationTypeEnum::PulseModulationTypeEnum pulseModulationType) {
         _pulseModulationType = DevStudio::PulseModulationTypeEnumPtr( new DevStudio::PulseModulationTypeEnum::PulseModulationTypeEnum (pulseModulationType));
         _discriminant = MajorRFModulationTypeEnum::PULSE;
      }

      /**
      * Function to get UnmodulatedType.
      * Note that this field is only valid of the discriminant is <code>Unmodulated</code>.
      * <br>Description from the FOM: <i>Detailed modulation type for the unmodulated major modulation type.</i>
      * <br>Description of the data type from the FOM: <i>Detailed modulation types for an unmodulated carrier</i>
      *
      * @return UnmodulatedType value
      */
      LIBAPI DevStudio::UnmodulatedTypeEnumPtr getUnmodulatedType() const {
         return _unmodulatedType;
      }

      /**
      * Function to set UnmodulatedType.
      * Note that this will set the discriminant to <code>Unmodulated</code>.
      * <br>Description from the FOM: <i>Detailed modulation type for the unmodulated major modulation type.</i>
      * <br>Description of the data type from the FOM: <i>Detailed modulation types for an unmodulated carrier</i>
      *
      * @param unmodulatedType value used to create a UnmodulatedTypeEnumPtr
      */
      LIBAPI void setUnmodulatedType(const DevStudio::UnmodulatedTypeEnum::UnmodulatedTypeEnum unmodulatedType) {
         _unmodulatedType = DevStudio::UnmodulatedTypeEnumPtr( new DevStudio::UnmodulatedTypeEnum::UnmodulatedTypeEnum (unmodulatedType));
         _discriminant = MajorRFModulationTypeEnum::UNMODULATED;
      }

      /** Description from the FOM: <i>Detailed modulation type for the amplitude major modulation type.</i> */
      DevStudio::AmplitudeModulationTypeEnumPtr _amplitudeModulationType;
      /** Description from the FOM: <i>Detailed modulation type for the amplitude and angle major modulation type.</i> */
      DevStudio::AmplitudeAngleModulationTypeEnumPtr _amplitudeAngleModulationType;
      /** Description from the FOM: <i>Detailed modulation type for the angle major modulation type.</i> */
      DevStudio::AngleModulationTypeEnumPtr _angleModulationType;
      /** Description from the FOM: <i>Detailed modulation type for the combination major modulation type.</i> */
      DevStudio::CombinationModulationTypeEnumPtr _combinationModulationType;
      /** Description from the FOM: <i>Detailed modulation type for the pulse major modulation type.</i> */
      DevStudio::PulseModulationTypeEnumPtr _pulseModulationType;
      /** Description from the FOM: <i>Detailed modulation type for the unmodulated major modulation type.</i> */
      DevStudio::UnmodulatedTypeEnumPtr _unmodulatedType;

   private:
      DevStudio::MajorRFModulationTypeEnum::MajorRFModulationTypeEnum _discriminant;

      RFModulationTypeVariantStruct(
         DevStudio::AmplitudeModulationTypeEnumPtr amplitudeModulationType,
         DevStudio::AmplitudeAngleModulationTypeEnumPtr amplitudeAngleModulationType,
         DevStudio::AngleModulationTypeEnumPtr angleModulationType,
         DevStudio::CombinationModulationTypeEnumPtr combinationModulationType,
         DevStudio::PulseModulationTypeEnumPtr pulseModulationType,
         DevStudio::UnmodulatedTypeEnumPtr unmodulatedType,
         DevStudio::MajorRFModulationTypeEnum::MajorRFModulationTypeEnum discriminant
      );
   };

   LIBAPI bool operator ==(const DevStudio::RFModulationTypeVariantStruct& l, const DevStudio::RFModulationTypeVariantStruct& r);
   LIBAPI bool operator !=(const DevStudio::RFModulationTypeVariantStruct& l, const DevStudio::RFModulationTypeVariantStruct& r);
   LIBAPI bool operator <(const DevStudio::RFModulationTypeVariantStruct& l, const DevStudio::RFModulationTypeVariantStruct& r);
   LIBAPI bool operator >(const DevStudio::RFModulationTypeVariantStruct& l, const DevStudio::RFModulationTypeVariantStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::RFModulationTypeVariantStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::RFModulationTypeVariantStruct const &);
}

#endif
