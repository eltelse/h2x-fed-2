/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_WORLDLOCATIONSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_WORLDLOCATIONSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <RtiDriver/RprUtility/RprUtility.h>


namespace DevStudio {
   /**
   * Implementation of the <code>WorldLocationStruct</code> data type from the FOM.
   * This datatype can be used with the RprUtility package. Please see the Overview document
   * located in the project root directory for more information.
   * <br>Description from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
   */
   class WorldLocationStruct {

   public:
      /**
      * Description from the FOM: <i>Distance from the origin along the X axis.</i>.
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      */
      double x;
      /**
      * Description from the FOM: <i>Distance from the origin along the Y axis.</i>.
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      */
      double y;
      /**
      * Description from the FOM: <i>Distance from the origin along the Z axis.</i>.
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      */
      double z;

      LIBAPI WorldLocationStruct()
         :
         x(0),
         y(0),
         z(0)
      {}

      /**
      * Constructor for WorldLocationStruct
      *
      * @param x_ value to set as x.
      * <br>Description from the FOM: <i>Distance from the origin along the X axis.</i>
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      * @param y_ value to set as y.
      * <br>Description from the FOM: <i>Distance from the origin along the Y axis.</i>
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      * @param z_ value to set as z.
      * <br>Description from the FOM: <i>Distance from the origin along the Z axis.</i>
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      */
      LIBAPI WorldLocationStruct(
         double x_,
         double y_,
         double z_
         )
         :
         x(x_),
         y(y_),
         z(z_)
      {}

      /**
      * Convert to RprUtility datatype
      *
      * @param location WorldLocationStruct to convert
      *
      * @return location converted to RprUtility::WorldLocation
      */
      LIBAPI static RprUtility::WorldLocation convert(const WorldLocationStruct location);

      /**
      * Convert from RprUtility datatype
      *
      * @param location RprUtility::WorldLocation to convert to WorldLocationStruct
      *
      * @return location converted to WorldLocationStruct
      */
      LIBAPI static WorldLocationStruct convert(const RprUtility::WorldLocation location);


      /**
      * Function to get x.
      * <br>Description from the FOM: <i>Distance from the origin along the X axis.</i>
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      *
      * @return x
      */
      LIBAPI double & getX() {
         return x;
      }

      /**
      * Function to get y.
      * <br>Description from the FOM: <i>Distance from the origin along the Y axis.</i>
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      *
      * @return y
      */
      LIBAPI double & getY() {
         return y;
      }

      /**
      * Function to get z.
      * <br>Description from the FOM: <i>Distance from the origin along the Z axis.</i>
      * <br>Description of the data type from the FOM: <i>Datatype based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: perfect]</i>
      *
      * @return z
      */
      LIBAPI double & getZ() {
         return z;
      }

   };


   LIBAPI bool operator ==(const DevStudio::WorldLocationStruct& l, const DevStudio::WorldLocationStruct& r);
   LIBAPI bool operator !=(const DevStudio::WorldLocationStruct& l, const DevStudio::WorldLocationStruct& r);
   LIBAPI bool operator <(const DevStudio::WorldLocationStruct& l, const DevStudio::WorldLocationStruct& r);
   LIBAPI bool operator >(const DevStudio::WorldLocationStruct& l, const DevStudio::WorldLocationStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::WorldLocationStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::WorldLocationStruct const &);
}
#endif
