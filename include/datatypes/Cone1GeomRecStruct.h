/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_CONE1GEOMRECSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_CONE1GEOMRECSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/OctetArray4.h>
#include <DevStudio/datatypes/OrientationStruct.h>
#include <DevStudio/datatypes/WorldLocationStruct.h>
#include <vector>

namespace DevStudio {
   /**
   * Implementation of the <code>Cone1GeomRecStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Record specifying Cone 1 geometry record</i>
   */
   class Cone1GeomRecStruct {

   public:
      /**
      * Description from the FOM: <i>Vertex location X, Y, Z</i>.
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      */
      WorldLocationStruct vertexLocation;
      /**
      * Description from the FOM: <i>Orientation, specified by Euler angles</i>.
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      */
      OrientationStruct orientation;
      /**
      * Description from the FOM: <i>Height</i>.
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      */
      float height;
      /**
      * Description from the FOM: <i>Peak angle</i>.
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      */
      float peakAngle;
      /**
      * Description from the FOM: <i>Padding field</i>.
      * <br>Description of the data type from the FOM: <i>Generic array of four Octet elements.</i>
      */
      std::vector</* 4 */ char > padding;

      LIBAPI Cone1GeomRecStruct()
         :
         vertexLocation(WorldLocationStruct()),
         orientation(OrientationStruct()),
         height(0),
         peakAngle(0),
         padding(0)
      {}

      /**
      * Constructor for Cone1GeomRecStruct
      *
      * @param vertexLocation_ value to set as vertexLocation.
      * <br>Description from the FOM: <i>Vertex location X, Y, Z</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      * @param orientation_ value to set as orientation.
      * <br>Description from the FOM: <i>Orientation, specified by Euler angles</i>
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      * @param height_ value to set as height.
      * <br>Description from the FOM: <i>Height</i>
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      * @param peakAngle_ value to set as peakAngle.
      * <br>Description from the FOM: <i>Peak angle</i>
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      * @param padding_ value to set as padding.
      * <br>Description from the FOM: <i>Padding field</i>
      * <br>Description of the data type from the FOM: <i>Generic array of four Octet elements.</i>
      */
      LIBAPI Cone1GeomRecStruct(
         WorldLocationStruct vertexLocation_,
         OrientationStruct orientation_,
         float height_,
         float peakAngle_,
         std::vector</* 4 */ char > padding_
         )
         :
         vertexLocation(vertexLocation_),
         orientation(orientation_),
         height(height_),
         peakAngle(peakAngle_),
         padding(padding_)
      {}



      /**
      * Function to get vertexLocation.
      * <br>Description from the FOM: <i>Vertex location X, Y, Z</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @return vertexLocation
      */
      LIBAPI DevStudio::WorldLocationStruct & getVertexLocation() {
         return vertexLocation;
      }

      /**
      * Function to get orientation.
      * <br>Description from the FOM: <i>Orientation, specified by Euler angles</i>
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @return orientation
      */
      LIBAPI DevStudio::OrientationStruct & getOrientation() {
         return orientation;
      }

      /**
      * Function to get height.
      * <br>Description from the FOM: <i>Height</i>
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      *
      * @return height
      */
      LIBAPI float & getHeight() {
         return height;
      }

      /**
      * Function to get peakAngle.
      * <br>Description from the FOM: <i>Peak angle</i>
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      *
      * @return peakAngle
      */
      LIBAPI float & getPeakAngle() {
         return peakAngle;
      }

      /**
      * Function to get padding.
      * <br>Description from the FOM: <i>Padding field</i>
      * <br>Description of the data type from the FOM: <i>Generic array of four Octet elements.</i>
      *
      * @return padding
      */
      LIBAPI std::vector</* 4 */ char > & getPadding() {
         return padding;
      }

   };


   LIBAPI bool operator ==(const DevStudio::Cone1GeomRecStruct& l, const DevStudio::Cone1GeomRecStruct& r);
   LIBAPI bool operator !=(const DevStudio::Cone1GeomRecStruct& l, const DevStudio::Cone1GeomRecStruct& r);
   LIBAPI bool operator <(const DevStudio::Cone1GeomRecStruct& l, const DevStudio::Cone1GeomRecStruct& r);
   LIBAPI bool operator >(const DevStudio::Cone1GeomRecStruct& l, const DevStudio::Cone1GeomRecStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::Cone1GeomRecStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::Cone1GeomRecStruct const &);
}
#endif
