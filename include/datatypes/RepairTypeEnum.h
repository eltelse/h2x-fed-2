/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_REPAIRTYPEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_REPAIRTYPEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace RepairTypeEnum {
        /**
        * Implementation of the <code>RepairTypeEnum16</code> data type from the FOM.
        * <br>Description from the FOM: <i>System repaired</i>
        */
        enum RepairTypeEnum {
            /** <code>NoRepairsPerformed</code> (with ordinal 0) */
            NO_REPAIRS_PERFORMED = 0,
            /** <code>AllRequestedRepairsPerformed</code> (with ordinal 1) */
            ALL_REQUESTED_REPAIRS_PERFORMED = 1,
            /** <code>MotorOrEngine</code> (with ordinal 10) */
            MOTOR_OR_ENGINE = 10,
            /** <code>Starter</code> (with ordinal 20) */
            STARTER = 20,
            /** <code>Alternator</code> (with ordinal 30) */
            ALTERNATOR = 30,
            /** <code>Generator</code> (with ordinal 40) */
            GENERATOR = 40,
            /** <code>Battery</code> (with ordinal 50) */
            BATTERY = 50,
            /** <code>EngineCoolantLeak</code> (with ordinal 60) */
            ENGINE_COOLANT_LEAK = 60,
            /** <code>FuelFilter</code> (with ordinal 70) */
            FUEL_FILTER = 70,
            /** <code>TransmissionOilLeak</code> (with ordinal 80) */
            TRANSMISSION_OIL_LEAK = 80,
            /** <code>EngineOilLeak</code> (with ordinal 90) */
            ENGINE_OIL_LEAK = 90,
            /** <code>Pumps</code> (with ordinal 100) */
            PUMPS = 100,
            /** <code>Filters</code> (with ordinal 110) */
            FILTERS = 110,
            /** <code>Transmission</code> (with ordinal 120) */
            TRANSMISSION = 120,
            /** <code>Brakes</code> (with ordinal 130) */
            BRAKES = 130,
            /** <code>SuspensionSystem</code> (with ordinal 140) */
            SUSPENSION_SYSTEM = 140,
            /** <code>OilFilter</code> (with ordinal 150) */
            OIL_FILTER = 150,
            /** <code>Hull</code> (with ordinal 1000) */
            HULL = 1000,
            /** <code>Airframe</code> (with ordinal 1010) */
            AIRFRAME = 1010,
            /** <code>TruckBody</code> (with ordinal 1020) */
            TRUCK_BODY = 1020,
            /** <code>TankBody</code> (with ordinal 1030) */
            TANK_BODY = 1030,
            /** <code>TrailerBody</code> (with ordinal 1040) */
            TRAILER_BODY = 1040,
            /** <code>Turret</code> (with ordinal 1050) */
            TURRET = 1050,
            /** <code>Propeller</code> (with ordinal 1500) */
            PROPELLER = 1500,
            /** <code>EnvironmentalFilters</code> (with ordinal 1520) */
            ENVIRONMENTAL_FILTERS = 1520,
            /** <code>Wheels</code> (with ordinal 1540) */
            WHEELS = 1540,
            /** <code>Tire</code> (with ordinal 1550) */
            TIRE = 1550,
            /** <code>Track</code> (with ordinal 1560) */
            TRACK = 1560,
            /** <code>GunElevationDrive</code> (with ordinal 2000) */
            GUN_ELEVATION_DRIVE = 2000,
            /** <code>GunStabilizationSystem</code> (with ordinal 2010) */
            GUN_STABILIZATION_SYSTEM = 2010,
            /** <code>GunnersPrimarySight_GPS_</code> (with ordinal 2020) */
            GUNNERS_PRIMARY_SIGHT_GPS = 2020,
            /** <code>CommandersExtensionToTheGPS</code> (with ordinal 2030) */
            COMMANDERS_EXTENSION_TO_THE_GPS = 2030,
            /** <code>LoadingMechanism</code> (with ordinal 2040) */
            LOADING_MECHANISM = 2040,
            /** <code>GunnersAuxiliarySight</code> (with ordinal 2050) */
            GUNNERS_AUXILIARY_SIGHT = 2050,
            /** <code>GunnersControlPanel</code> (with ordinal 2060) */
            GUNNERS_CONTROL_PANEL = 2060,
            /** <code>GunnersControlAssemblyHandle_Handles</code> (with ordinal 2070) */
            GUNNERS_CONTROL_ASSEMBLY_HANDLE_HANDLES = 2070,
            /** <code>CommandersControlHandles_Assembly</code> (with ordinal 2090) */
            COMMANDERS_CONTROL_HANDLES_ASSEMBLY = 2090,
            /** <code>CommandersWeaponStation</code> (with ordinal 2100) */
            COMMANDERS_WEAPON_STATION = 2100,
            /** <code>CommandersIndependentThermalViewer_CITV_</code> (with ordinal 2110) */
            COMMANDERS_INDEPENDENT_THERMAL_VIEWER_CITV = 2110,
            /** <code>GeneralWeapons</code> (with ordinal 2120) */
            GENERAL_WEAPONS = 2120,
            /** <code>FuelTransferPump</code> (with ordinal 4000) */
            FUEL_TRANSFER_PUMP = 4000,
            /** <code>FuelLines</code> (with ordinal 4010) */
            FUEL_LINES = 4010,
            /** <code>Gauges</code> (with ordinal 4020) */
            GAUGES = 4020,
            /** <code>GeneralFuelSystem</code> (with ordinal 4030) */
            GENERAL_FUEL_SYSTEM = 4030,
            /** <code>ElectronicWarfareSystems</code> (with ordinal 4500) */
            ELECTRONIC_WARFARE_SYSTEMS = 4500,
            /** <code>DetectionSystems</code> (with ordinal 4600) */
            DETECTION_SYSTEMS = 4600,
            /** <code>ElectronicWarfareRadioFrequency</code> (with ordinal 4610) */
            ELECTRONIC_WARFARE_RADIO_FREQUENCY = 4610,
            /** <code>ElectronicWarfareMicrowave</code> (with ordinal 4620) */
            ELECTRONIC_WARFARE_MICROWAVE = 4620,
            /** <code>ElectronicWarfareInfrared</code> (with ordinal 4630) */
            ELECTRONIC_WARFARE_INFRARED = 4630,
            /** <code>ElectronicWarfareLaser</code> (with ordinal 4640) */
            ELECTRONIC_WARFARE_LASER = 4640,
            /** <code>RangeFinders</code> (with ordinal 4700) */
            RANGE_FINDERS = 4700,
            /** <code>Range-OnlyRadar</code> (with ordinal 4710) */
            RANGE_ONLY_RADAR = 4710,
            /** <code>LaserRangeFinder</code> (with ordinal 4720) */
            LASER_RANGE_FINDER = 4720,
            /** <code>ElectronicSystems</code> (with ordinal 4800) */
            ELECTRONIC_SYSTEMS = 4800,
            /** <code>ElectronicSystemsRadioFrequency</code> (with ordinal 4810) */
            ELECTRONIC_SYSTEMS_RADIO_FREQUENCY = 4810,
            /** <code>ElectronicSystemsMicrowave</code> (with ordinal 4820) */
            ELECTRONIC_SYSTEMS_MICROWAVE = 4820,
            /** <code>ElectronicSystemsInfrared</code> (with ordinal 4830) */
            ELECTRONIC_SYSTEMS_INFRARED = 4830,
            /** <code>ElectronicSystemsLaser</code> (with ordinal 4840) */
            ELECTRONIC_SYSTEMS_LASER = 4840,
            /** <code>Radios</code> (with ordinal 5000) */
            RADIOS = 5000,
            /** <code>CommunicationSystems</code> (with ordinal 5010) */
            COMMUNICATION_SYSTEMS = 5010,
            /** <code>Intercoms</code> (with ordinal 5100) */
            INTERCOMS = 5100,
            /** <code>Encoders</code> (with ordinal 5200) */
            ENCODERS = 5200,
            /** <code>EncryptionDevices</code> (with ordinal 5250) */
            ENCRYPTION_DEVICES = 5250,
            /** <code>Decoders</code> (with ordinal 5300) */
            DECODERS = 5300,
            /** <code>DecryptionDevices</code> (with ordinal 5350) */
            DECRYPTION_DEVICES = 5350,
            /** <code>Computers</code> (with ordinal 5500) */
            COMPUTERS = 5500,
            /** <code>NavigationAndControlSystems</code> (with ordinal 6000) */
            NAVIGATION_AND_CONTROL_SYSTEMS = 6000,
            /** <code>FireControlSystems</code> (with ordinal 6500) */
            FIRE_CONTROL_SYSTEMS = 6500,
            /** <code>AirSupply</code> (with ordinal 8000) */
            AIR_SUPPLY = 8000,
            /** <code>LifeSupportFilters</code> (with ordinal 8010) */
            LIFE_SUPPORT_FILTERS = 8010,
            /** <code>LifeSupportWaterSupply</code> (with ordinal 8020) */
            LIFE_SUPPORT_WATER_SUPPLY = 8020,
            /** <code>RefrigerationSystem</code> (with ordinal 8030) */
            REFRIGERATION_SYSTEM = 8030,
            /** <code>ChemicalBiologicalAndRadiologicalProtection</code> (with ordinal 8040) */
            CHEMICAL_BIOLOGICAL_AND_RADIOLOGICAL_PROTECTION = 8040,
            /** <code>WaterWashDownSystems</code> (with ordinal 8050) */
            WATER_WASH_DOWN_SYSTEMS = 8050,
            /** <code>DecontaminationSystems</code> (with ordinal 8060) */
            DECONTAMINATION_SYSTEMS = 8060,
            /** <code>HydraulicSystemWaterSupply</code> (with ordinal 9000) */
            HYDRAULIC_SYSTEM_WATER_SUPPLY = 9000,
            /** <code>CoolingSystem</code> (with ordinal 9010) */
            COOLING_SYSTEM = 9010,
            /** <code>Winches</code> (with ordinal 9020) */
            WINCHES = 9020,
            /** <code>Catapults</code> (with ordinal 9030) */
            CATAPULTS = 9030,
            /** <code>Cranes</code> (with ordinal 9040) */
            CRANES = 9040,
            /** <code>Launchers</code> (with ordinal 9050) */
            LAUNCHERS = 9050,
            /** <code>LifeBoats</code> (with ordinal 10000) */
            LIFE_BOATS = 10000,
            /** <code>LandingCraft</code> (with ordinal 10010) */
            LANDING_CRAFT = 10010,
            /** <code>EjectionSeats</code> (with ordinal 10020) */
            EJECTION_SEATS = 10020
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to RepairTypeEnum: static_cast<DevStudio::RepairTypeEnum::RepairTypeEnum>(i)
        */
        LIBAPI bool isValid(const RepairTypeEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::RepairTypeEnum::RepairTypeEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::RepairTypeEnum::RepairTypeEnum const &);
}


#endif
