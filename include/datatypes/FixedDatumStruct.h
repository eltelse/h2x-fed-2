/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_FIXEDDATUMSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_FIXEDDATUMSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/DatumIdentifierEnum.h>

namespace DevStudio {
   /**
   * Implementation of the <code>FixedDatumStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Identifier and value for a fixed datum.</i>
   */
   class FixedDatumStruct {

   public:
      /**
      * Description from the FOM: <i>The identifier for this fixed datum.</i>.
      * <br>Description of the data type from the FOM: <i>Datum ID</i>
      */
      DatumIdentifierEnum::DatumIdentifierEnum fixedDatumIdentifier;
      /**
      * Description from the FOM: <i>The value for this fixed datum.</i>.
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      unsigned int fixedDatumValue;

      LIBAPI FixedDatumStruct()
         :
         fixedDatumIdentifier(DatumIdentifierEnum::DatumIdentifierEnum()),
         fixedDatumValue(0)
      {}

      /**
      * Constructor for FixedDatumStruct
      *
      * @param fixedDatumIdentifier_ value to set as fixedDatumIdentifier.
      * <br>Description from the FOM: <i>The identifier for this fixed datum.</i>
      * <br>Description of the data type from the FOM: <i>Datum ID</i>
      * @param fixedDatumValue_ value to set as fixedDatumValue.
      * <br>Description from the FOM: <i>The value for this fixed datum.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      LIBAPI FixedDatumStruct(
         DatumIdentifierEnum::DatumIdentifierEnum fixedDatumIdentifier_,
         unsigned int fixedDatumValue_
         )
         :
         fixedDatumIdentifier(fixedDatumIdentifier_),
         fixedDatumValue(fixedDatumValue_)
      {}



      /**
      * Function to get fixedDatumIdentifier.
      * <br>Description from the FOM: <i>The identifier for this fixed datum.</i>
      * <br>Description of the data type from the FOM: <i>Datum ID</i>
      *
      * @return fixedDatumIdentifier
      */
      LIBAPI DevStudio::DatumIdentifierEnum::DatumIdentifierEnum & getFixedDatumIdentifier() {
         return fixedDatumIdentifier;
      }

      /**
      * Function to get fixedDatumValue.
      * <br>Description from the FOM: <i>The value for this fixed datum.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return fixedDatumValue
      */
      LIBAPI unsigned int & getFixedDatumValue() {
         return fixedDatumValue;
      }

   };


   LIBAPI bool operator ==(const DevStudio::FixedDatumStruct& l, const DevStudio::FixedDatumStruct& r);
   LIBAPI bool operator !=(const DevStudio::FixedDatumStruct& l, const DevStudio::FixedDatumStruct& r);
   LIBAPI bool operator <(const DevStudio::FixedDatumStruct& l, const DevStudio::FixedDatumStruct& r);
   LIBAPI bool operator >(const DevStudio::FixedDatumStruct& l, const DevStudio::FixedDatumStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::FixedDatumStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::FixedDatumStruct const &);
}
#endif
