/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_GRIDVALUETYPE0STRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_GRIDVALUETYPE0STRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/OctetArray1Plus.h>
#include <DevStudio/datatypes/OctetPadding32Array.h>
#include <vector>

namespace DevStudio {
   /**
   * Implementation of the <code>GridValueType0Struct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Record specifying type 0 data representation</i>
   */
   class GridValueType0Struct {

   public:
      /**
      * Description from the FOM: <i>Specifies the number of bytes</i>.
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of Octet elements, containing at least one element.</i>
      */
      std::vector</* not empty */ char > numberOfBytesAValues;
      /**
      * Description from the FOM: <i>Brings the record length to a 32-bit boundary</i>.
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of meaningless Octet elements, to align the subsequent data structure to the next 32 bit octet boundary value (OBV). The array is encoded without array length, containing zero to three elements.</i>
      */
      std::vector<char > paddingTo32;

      LIBAPI GridValueType0Struct()
         :
         numberOfBytesAValues(0),
         paddingTo32(0)
      {}

      /**
      * Constructor for GridValueType0Struct
      *
      * @param numberOfBytesAValues_ value to set as numberOfBytesAValues.
      * <br>Description from the FOM: <i>Specifies the number of bytes</i>
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of Octet elements, containing at least one element.</i>
      * @param paddingTo32_ value to set as paddingTo32.
      * <br>Description from the FOM: <i>Brings the record length to a 32-bit boundary</i>
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of meaningless Octet elements, to align the subsequent data structure to the next 32 bit octet boundary value (OBV). The array is encoded without array length, containing zero to three elements.</i>
      */
      LIBAPI GridValueType0Struct(
         std::vector</* not empty */ char > numberOfBytesAValues_,
         std::vector<char > paddingTo32_
         )
         :
         numberOfBytesAValues(numberOfBytesAValues_),
         paddingTo32(paddingTo32_)
      {}



      /**
      * Function to get numberOfBytesAValues.
      * <br>Description from the FOM: <i>Specifies the number of bytes</i>
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of Octet elements, containing at least one element.</i>
      *
      * @return numberOfBytesAValues
      */
      LIBAPI std::vector</* not empty */ char > & getNumberOfBytesAValues() {
         return numberOfBytesAValues;
      }

      /**
      * Function to get paddingTo32.
      * <br>Description from the FOM: <i>Brings the record length to a 32-bit boundary</i>
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of meaningless Octet elements, to align the subsequent data structure to the next 32 bit octet boundary value (OBV). The array is encoded without array length, containing zero to three elements.</i>
      *
      * @return paddingTo32
      */
      LIBAPI std::vector<char > & getPaddingTo32() {
         return paddingTo32;
      }

   };


   LIBAPI bool operator ==(const DevStudio::GridValueType0Struct& l, const DevStudio::GridValueType0Struct& r);
   LIBAPI bool operator !=(const DevStudio::GridValueType0Struct& l, const DevStudio::GridValueType0Struct& r);
   LIBAPI bool operator <(const DevStudio::GridValueType0Struct& l, const DevStudio::GridValueType0Struct& r);
   LIBAPI bool operator >(const DevStudio::GridValueType0Struct& l, const DevStudio::GridValueType0Struct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::GridValueType0Struct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::GridValueType0Struct const &);
}
#endif
