/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_PROPULSIONSYSTEMDATASTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_PROPULSIONSYSTEMDATASTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>


namespace DevStudio {
   /**
   * Implementation of the <code>PropulsionSystemDataStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Information describing a propulsion system in terms of power settings and current RPM.</i>
   */
   class PropulsionSystemDataStruct {

   public:
      /**
      * Description from the FOM: <i>The power setting of the propulsion system, after any response lags have been incorporated.</i>.
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      */
      float powerSetting;
      /**
      * Description from the FOM: <i>The current engine speed.</i>.
      * <br>Description of the data type from the FOM: <i>Rotation speed expressed in revolutions per minute. [unit: RPM, resolution: NA, accuracy: perfect]</i>
      */
      float engineRPM;

      LIBAPI PropulsionSystemDataStruct()
         :
         powerSetting(0),
         engineRPM(0)
      {}

      /**
      * Constructor for PropulsionSystemDataStruct
      *
      * @param powerSetting_ value to set as powerSetting.
      * <br>Description from the FOM: <i>The power setting of the propulsion system, after any response lags have been incorporated.</i>
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      * @param engineRPM_ value to set as engineRPM.
      * <br>Description from the FOM: <i>The current engine speed.</i>
      * <br>Description of the data type from the FOM: <i>Rotation speed expressed in revolutions per minute. [unit: RPM, resolution: NA, accuracy: perfect]</i>
      */
      LIBAPI PropulsionSystemDataStruct(
         float powerSetting_,
         float engineRPM_
         )
         :
         powerSetting(powerSetting_),
         engineRPM(engineRPM_)
      {}



      /**
      * Function to get powerSetting.
      * <br>Description from the FOM: <i>The power setting of the propulsion system, after any response lags have been incorporated.</i>
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      *
      * @return powerSetting
      */
      LIBAPI float & getPowerSetting() {
         return powerSetting;
      }

      /**
      * Function to get engineRPM.
      * <br>Description from the FOM: <i>The current engine speed.</i>
      * <br>Description of the data type from the FOM: <i>Rotation speed expressed in revolutions per minute. [unit: RPM, resolution: NA, accuracy: perfect]</i>
      *
      * @return engineRPM
      */
      LIBAPI float & getEngineRPM() {
         return engineRPM;
      }

   };


   LIBAPI bool operator ==(const DevStudio::PropulsionSystemDataStruct& l, const DevStudio::PropulsionSystemDataStruct& r);
   LIBAPI bool operator !=(const DevStudio::PropulsionSystemDataStruct& l, const DevStudio::PropulsionSystemDataStruct& r);
   LIBAPI bool operator <(const DevStudio::PropulsionSystemDataStruct& l, const DevStudio::PropulsionSystemDataStruct& r);
   LIBAPI bool operator >(const DevStudio::PropulsionSystemDataStruct& l, const DevStudio::PropulsionSystemDataStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::PropulsionSystemDataStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::PropulsionSystemDataStruct const &);
}
#endif
