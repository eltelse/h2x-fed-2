/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_MINEFIELDPROTOCOLENUM_H
#define DEVELOPER_STUDIO_DATATYPES_MINEFIELDPROTOCOLENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace MinefieldProtocolEnum {
        /**
        * Implementation of the <code>MinefieldProtocolEnum8</code> data type from the FOM.
        * <br>Description from the FOM: <i>Minefield communication protocol mode</i>
        */
        enum MinefieldProtocolEnum {
            /** <code>HeartbeatMode</code> (with ordinal 0) */
            HEARTBEAT_MODE = 0,
            /** <code>QRPMode</code> (with ordinal 1) */
            QRPMODE = 1
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to MinefieldProtocolEnum: static_cast<DevStudio::MinefieldProtocolEnum::MinefieldProtocolEnum>(i)
        */
        LIBAPI bool isValid(const MinefieldProtocolEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::MinefieldProtocolEnum::MinefieldProtocolEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::MinefieldProtocolEnum::MinefieldProtocolEnum const &);
}


#endif
