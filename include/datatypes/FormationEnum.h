/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_FORMATIONENUM_H
#define DEVELOPER_STUDIO_DATATYPES_FORMATIONENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace FormationEnum {
        /**
        * Implementation of the <code>FormationEnum32</code> data type from the FOM.
        * <br>Description from the FOM: <i>Formation</i>
        */
        enum FormationEnum {
            /** <code>Other</code> (with ordinal 0) */
            OTHER = 0,
            /** <code>Assembly</code> (with ordinal 1) */
            ASSEMBLY = 1,
            /** <code>Vee</code> (with ordinal 2) */
            VEE = 2,
            /** <code>Wedge</code> (with ordinal 3) */
            WEDGE = 3,
            /** <code>Line</code> (with ordinal 4) */
            LINE = 4,
            /** <code>Column</code> (with ordinal 5) */
            COLUMN = 5
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to FormationEnum: static_cast<DevStudio::FormationEnum::FormationEnum>(i)
        */
        LIBAPI bool isValid(const FormationEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::FormationEnum::FormationEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::FormationEnum::FormationEnum const &);
}


#endif
