/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_VISIBLESIDELOCATIONENUM_H
#define DEVELOPER_STUDIO_DATATYPES_VISIBLESIDELOCATIONENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace VisibleSideLocationEnum {
        /**
        * Implementation of the <code>VisibleSideLocationEnum32</code> data type from the FOM.
        * <br>Description from the FOM: <i>Minefield lane marker visible side</i>
        */
        enum VisibleSideLocationEnum {
            /** <code>LeftSideVisible</code> (with ordinal 0) */
            LEFT_SIDE_VISIBLE = 0,
            /** <code>RightSideVisible</code> (with ordinal 1) */
            RIGHT_SIDE_VISIBLE = 1,
            /** <code>BothSideVisible</code> (with ordinal 2) */
            BOTH_SIDE_VISIBLE = 2
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to VisibleSideLocationEnum: static_cast<DevStudio::VisibleSideLocationEnum::VisibleSideLocationEnum>(i)
        */
        LIBAPI bool isValid(const VisibleSideLocationEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::VisibleSideLocationEnum::VisibleSideLocationEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::VisibleSideLocationEnum::VisibleSideLocationEnum const &);
}


#endif
