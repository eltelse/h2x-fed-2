/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_FUSETYPEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_FUSETYPEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace FuseTypeEnum {
        /**
        * Implementation of the <code>FuseTypeEnum16</code> data type from the FOM.
        * <br>Description from the FOM: <i>Fuse (detonator)</i>
        */
        enum FuseTypeEnum {
            /** <code>Other</code> (with ordinal 0) */
            OTHER = 0,
            /** <code>IntelligentInfluence</code> (with ordinal 10) */
            INTELLIGENT_INFLUENCE = 10,
            /** <code>Sensor</code> (with ordinal 20) */
            SENSOR = 20,
            /** <code>SelfDestruct</code> (with ordinal 30) */
            SELF_DESTRUCT = 30,
            /** <code>UltraQuick</code> (with ordinal 40) */
            ULTRA_QUICK = 40,
            /** <code>Body</code> (with ordinal 50) */
            BODY = 50,
            /** <code>DeepIntrusion</code> (with ordinal 60) */
            DEEP_INTRUSION = 60,
            /** <code>Multifunction</code> (with ordinal 100) */
            MULTIFUNCTION = 100,
            /** <code>PointDetonation_PD</code> (with ordinal 200) */
            POINT_DETONATION_PD = 200,
            /** <code>BaseDetonation_BD</code> (with ordinal 300) */
            BASE_DETONATION_BD = 300,
            /** <code>Contact</code> (with ordinal 1000) */
            CONTACT = 1000,
            /** <code>ContactInstantImpact</code> (with ordinal 1100) */
            CONTACT_INSTANT_IMPACT = 1100,
            /** <code>ContactDelayed</code> (with ordinal 1200) */
            CONTACT_DELAYED = 1200,
            /** <code>Contact10msDelay</code> (with ordinal 1201) */
            CONTACT10MS_DELAY = 1201,
            /** <code>Contact20msDelay</code> (with ordinal 1202) */
            CONTACT20MS_DELAY = 1202,
            /** <code>Contact50msDelay</code> (with ordinal 1205) */
            CONTACT50MS_DELAY = 1205,
            /** <code>Contact60msDelay</code> (with ordinal 1206) */
            CONTACT60MS_DELAY = 1206,
            /** <code>Contact100msDelay</code> (with ordinal 1210) */
            CONTACT100MS_DELAY = 1210,
            /** <code>Contact125msDelay</code> (with ordinal 1212) */
            CONTACT125MS_DELAY = 1212,
            /** <code>Contact250msDelay</code> (with ordinal 1225) */
            CONTACT250MS_DELAY = 1225,
            /** <code>ContactElectronicObliqueContact</code> (with ordinal 1300) */
            CONTACT_ELECTRONIC_OBLIQUE_CONTACT = 1300,
            /** <code>ContactGraze</code> (with ordinal 1400) */
            CONTACT_GRAZE = 1400,
            /** <code>ContactCrush</code> (with ordinal 1500) */
            CONTACT_CRUSH = 1500,
            /** <code>ContactHydrostatic</code> (with ordinal 1600) */
            CONTACT_HYDROSTATIC = 1600,
            /** <code>ContactMechanical</code> (with ordinal 1700) */
            CONTACT_MECHANICAL = 1700,
            /** <code>ContactChemical</code> (with ordinal 1800) */
            CONTACT_CHEMICAL = 1800,
            /** <code>ContactPiezoelectric</code> (with ordinal 1900) */
            CONTACT_PIEZOELECTRIC = 1900,
            /** <code>ContactPointInitiating</code> (with ordinal 1910) */
            CONTACT_POINT_INITIATING = 1910,
            /** <code>ContactPointInitiatingBaseDetonating</code> (with ordinal 1920) */
            CONTACT_POINT_INITIATING_BASE_DETONATING = 1920,
            /** <code>ContactBaseDetonating</code> (with ordinal 1930) */
            CONTACT_BASE_DETONATING = 1930,
            /** <code>ContactBallisticCapAndBase</code> (with ordinal 1940) */
            CONTACT_BALLISTIC_CAP_AND_BASE = 1940,
            /** <code>ContactBase</code> (with ordinal 1950) */
            CONTACT_BASE = 1950,
            /** <code>ContactNose</code> (with ordinal 1960) */
            CONTACT_NOSE = 1960,
            /** <code>ContactFittedInStandoffProbe</code> (with ordinal 1970) */
            CONTACT_FITTED_IN_STANDOFF_PROBE = 1970,
            /** <code>ContactNonAligned</code> (with ordinal 1980) */
            CONTACT_NON_ALIGNED = 1980,
            /** <code>Timed</code> (with ordinal 2000) */
            TIMED = 2000,
            /** <code>TimedProgrammable</code> (with ordinal 2100) */
            TIMED_PROGRAMMABLE = 2100,
            /** <code>TimedBurnout</code> (with ordinal 2200) */
            TIMED_BURNOUT = 2200,
            /** <code>TimedPyrotechnic</code> (with ordinal 2300) */
            TIMED_PYROTECHNIC = 2300,
            /** <code>TimedElectronic</code> (with ordinal 2400) */
            TIMED_ELECTRONIC = 2400,
            /** <code>TimedBaseDelay</code> (with ordinal 2500) */
            TIMED_BASE_DELAY = 2500,
            /** <code>TimedReinforcedNoseImpactDelay</code> (with ordinal 2600) */
            TIMED_REINFORCED_NOSE_IMPACT_DELAY = 2600,
            /** <code>TimedShortDelayImpact</code> (with ordinal 2700) */
            TIMED_SHORT_DELAY_IMPACT = 2700,
            /** <code>Timed10msDelay</code> (with ordinal 2701) */
            TIMED10MS_DELAY = 2701,
            /** <code>Timed20msDelay</code> (with ordinal 2702) */
            TIMED20MS_DELAY = 2702,
            /** <code>Timed50msDelay</code> (with ordinal 2705) */
            TIMED50MS_DELAY = 2705,
            /** <code>Timed60msDelay</code> (with ordinal 2706) */
            TIMED60MS_DELAY = 2706,
            /** <code>Timed100msDelay</code> (with ordinal 2710) */
            TIMED100MS_DELAY = 2710,
            /** <code>Timed125msDelay</code> (with ordinal 2712) */
            TIMED125MS_DELAY = 2712,
            /** <code>Timed250msDelay</code> (with ordinal 2725) */
            TIMED250MS_DELAY = 2725,
            /** <code>TimedNoseMountedVariableDelay</code> (with ordinal 2800) */
            TIMED_NOSE_MOUNTED_VARIABLE_DELAY = 2800,
            /** <code>TimedLongDelaySide</code> (with ordinal 2900) */
            TIMED_LONG_DELAY_SIDE = 2900,
            /** <code>TimedSelectableDelay</code> (with ordinal 2910) */
            TIMED_SELECTABLE_DELAY = 2910,
            /** <code>TimedImpact</code> (with ordinal 2920) */
            TIMED_IMPACT = 2920,
            /** <code>TimedSequence</code> (with ordinal 2930) */
            TIMED_SEQUENCE = 2930,
            /** <code>Proximity</code> (with ordinal 3000) */
            PROXIMITY = 3000,
            /** <code>ProximityActiveLaser</code> (with ordinal 3100) */
            PROXIMITY_ACTIVE_LASER = 3100,
            /** <code>ProximityMagneticMagpolarity</code> (with ordinal 3200) */
            PROXIMITY_MAGNETIC_MAGPOLARITY = 3200,
            /** <code>ProximityActiveDopplerRadar</code> (with ordinal 3300) */
            PROXIMITY_ACTIVE_DOPPLER_RADAR = 3300,
            /** <code>ProximityRadioFrequencyRF</code> (with ordinal 3400) */
            PROXIMITY_RADIO_FREQUENCY_RF = 3400,
            /** <code>ProximityProgrammable</code> (with ordinal 3500) */
            PROXIMITY_PROGRAMMABLE = 3500,
            /** <code>ProximityProgrammablePrefragmented</code> (with ordinal 3600) */
            PROXIMITY_PROGRAMMABLE_PREFRAGMENTED = 3600,
            /** <code>ProximityInfrared</code> (with ordinal 3700) */
            PROXIMITY_INFRARED = 3700,
            /** <code>Command</code> (with ordinal 4000) */
            COMMAND = 4000,
            /** <code>CommandElectronicRemotelySet</code> (with ordinal 4100) */
            COMMAND_ELECTRONIC_REMOTELY_SET = 4100,
            /** <code>Altitude</code> (with ordinal 5000) */
            ALTITUDE = 5000,
            /** <code>AltitudeRadioAltimeter</code> (with ordinal 5100) */
            ALTITUDE_RADIO_ALTIMETER = 5100,
            /** <code>AltitudeAirBurst</code> (with ordinal 5200) */
            ALTITUDE_AIR_BURST = 5200,
            /** <code>Depth</code> (with ordinal 6000) */
            DEPTH = 6000,
            /** <code>Acoustic</code> (with ordinal 7000) */
            ACOUSTIC = 7000,
            /** <code>Pressure</code> (with ordinal 8000) */
            PRESSURE = 8000,
            /** <code>PressureDelay</code> (with ordinal 8010) */
            PRESSURE_DELAY = 8010,
            /** <code>Inert</code> (with ordinal 8100) */
            INERT = 8100,
            /** <code>Dummy</code> (with ordinal 8110) */
            DUMMY = 8110,
            /** <code>Practice</code> (with ordinal 8120) */
            PRACTICE = 8120,
            /** <code>PlugRepresenting</code> (with ordinal 8130) */
            PLUG_REPRESENTING = 8130,
            /** <code>Training</code> (with ordinal 8150) */
            TRAINING = 8150,
            /** <code>Pyrotechnic</code> (with ordinal 9000) */
            PYROTECHNIC = 9000,
            /** <code>PyrotechnicDelay</code> (with ordinal 9010) */
            PYROTECHNIC_DELAY = 9010,
            /** <code>ElectroOptical</code> (with ordinal 9100) */
            ELECTRO_OPTICAL = 9100,
            /** <code>ElectroMechanical</code> (with ordinal 9110) */
            ELECTRO_MECHANICAL = 9110,
            /** <code>ElectroMechanicalNose</code> (with ordinal 9120) */
            ELECTRO_MECHANICAL_NOSE = 9120,
            /** <code>Strikerless</code> (with ordinal 9200) */
            STRIKERLESS = 9200,
            /** <code>StrikerlessNoseImpact</code> (with ordinal 9210) */
            STRIKERLESS_NOSE_IMPACT = 9210,
            /** <code>StrikerlessCompressionIgnition</code> (with ordinal 9220) */
            STRIKERLESS_COMPRESSION_IGNITION = 9220,
            /** <code>CompressionIgnition</code> (with ordinal 9300) */
            COMPRESSION_IGNITION = 9300,
            /** <code>CompressionIgnitionStrikerlessNoseImpact</code> (with ordinal 9310) */
            COMPRESSION_IGNITION_STRIKERLESS_NOSE_IMPACT = 9310,
            /** <code>Percussion</code> (with ordinal 9400) */
            PERCUSSION = 9400,
            /** <code>PercussionInstantaneous</code> (with ordinal 9410) */
            PERCUSSION_INSTANTANEOUS = 9410,
            /** <code>Electronic</code> (with ordinal 9500) */
            ELECTRONIC = 9500,
            /** <code>ElectronicInternallyMounted</code> (with ordinal 9510) */
            ELECTRONIC_INTERNALLY_MOUNTED = 9510,
            /** <code>ElectronicRangeSetting</code> (with ordinal 9520) */
            ELECTRONIC_RANGE_SETTING = 9520,
            /** <code>ElectronicProgrammed</code> (with ordinal 9530) */
            ELECTRONIC_PROGRAMMED = 9530,
            /** <code>Mechanical</code> (with ordinal 9600) */
            MECHANICAL = 9600,
            /** <code>MechanicalNose</code> (with ordinal 9610) */
            MECHANICAL_NOSE = 9610,
            /** <code>MechanicalTail</code> (with ordinal 9620) */
            MECHANICAL_TAIL = 9620
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to FuseTypeEnum: static_cast<DevStudio::FuseTypeEnum::FuseTypeEnum>(i)
        */
        LIBAPI bool isValid(const FuseTypeEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::FuseTypeEnum::FuseTypeEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::FuseTypeEnum::FuseTypeEnum const &);
}


#endif
