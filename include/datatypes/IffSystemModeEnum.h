/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_IFFSYSTEMMODEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_IFFSYSTEMMODEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace IffSystemModeEnum {
        /**
        * Implementation of the <code>IffSystemModeEnum8</code> data type from the FOM.
        * <br>Description from the FOM: <i>IFF system mode</i>
        */
        enum IffSystemModeEnum {
            /** <code>NoStatement</code> (with ordinal 0) */
            NO_STATEMENT = 0,
            /** <code>Off</code> (with ordinal 1) */
            OFF = 1,
            /** <code>Standby</code> (with ordinal 2) */
            STANDBY = 2,
            /** <code>Normal</code> (with ordinal 3) */
            NORMAL = 3,
            /** <code>Emergency</code> (with ordinal 4) */
            EMERGENCY = 4,
            /** <code>LowOrLowSensitivity</code> (with ordinal 5) */
            LOW_OR_LOW_SENSITIVITY = 5
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to IffSystemModeEnum: static_cast<DevStudio::IffSystemModeEnum::IffSystemModeEnum>(i)
        */
        LIBAPI bool isValid(const IffSystemModeEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::IffSystemModeEnum::IffSystemModeEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::IffSystemModeEnum::IffSystemModeEnum const &);
}


#endif
