/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_SHELLTYPEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_SHELLTYPEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace ShellTypeEnum {
        /**
        * Implementation of the <code>ShellTypeEnum32</code> data type from the FOM.
        * <br>Description from the FOM: <i>-NULL-</i>
        */
        enum ShellTypeEnum {
            /** <code>UNDEFINED</code> (with ordinal 0) */
            UNDEFINED = 0,
            /** <code>MORTAR</code> (with ordinal 1) */
            MORTAR = 1,
            /** <code>MEDIUM_RANGE_ARTILLERY</code> (with ordinal 2) */
            MEDIUM_RANGE_ARTILLERY = 2,
            /** <code>LONG_RANGE_ARTILLERY</code> (with ordinal 4) */
            LONG_RANGE_ARTILLERY = 4,
            /** <code>HEAVY_ROCKET_LAUNCHER</code> (with ordinal 8) */
            HEAVY_ROCKET_LAUNCHER = 8,
            /** <code>ROCKET_ARTILLERY</code> (with ordinal 16) */
            ROCKET_ARTILLERY = 16,
            /** <code>GROUND_MISSILE</code> (with ordinal 32) */
            GROUND_MISSILE = 32
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to ShellTypeEnum: static_cast<DevStudio::ShellTypeEnum::ShellTypeEnum>(i)
        */
        LIBAPI bool isValid(const ShellTypeEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::ShellTypeEnum::ShellTypeEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::ShellTypeEnum::ShellTypeEnum const &);
}


#endif
