/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_PULSEMODULATIONTYPEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_PULSEMODULATIONTYPEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace PulseModulationTypeEnum {
        /**
        * Implementation of the <code>PulseModulationTypeEnum16</code> data type from the FOM.
        * <br>Description from the FOM: <i>Detailed modulation types for Pulse Modulation</i>
        */
        enum PulseModulationTypeEnum {
            /** <code>Other</code> (with ordinal 0) */
            OTHER = 0,
            /** <code>Pulse</code> (with ordinal 1) */
            PULSE = 1,
            /** <code>XBandTACANPulse</code> (with ordinal 2) */
            XBAND_TACANPULSE = 2,
            /** <code>YBandTACANPulse</code> (with ordinal 3) */
            YBAND_TACANPULSE = 3
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to PulseModulationTypeEnum: static_cast<DevStudio::PulseModulationTypeEnum::PulseModulationTypeEnum>(i)
        */
        LIBAPI bool isValid(const PulseModulationTypeEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::PulseModulationTypeEnum::PulseModulationTypeEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::PulseModulationTypeEnum::PulseModulationTypeEnum const &);
}


#endif
