/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_TRAILINGEFFECTSCODEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_TRAILINGEFFECTSCODEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace TrailingEffectsCodeEnum {
        /**
        * Implementation of the <code>TrailingEffectsCodeEnum32</code> data type from the FOM.
        * <br>Description from the FOM: <i>Size of trailing effect</i>
        */
        enum TrailingEffectsCodeEnum {
            /** <code>NoTrail</code> (with ordinal 0) */
            NO_TRAIL = 0,
            /** <code>SmallTrail</code> (with ordinal 1) */
            SMALL_TRAIL = 1,
            /** <code>MediumTrail</code> (with ordinal 2) */
            MEDIUM_TRAIL = 2,
            /** <code>LargeTrail</code> (with ordinal 3) */
            LARGE_TRAIL = 3
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to TrailingEffectsCodeEnum: static_cast<DevStudio::TrailingEffectsCodeEnum::TrailingEffectsCodeEnum>(i)
        */
        LIBAPI bool isValid(const TrailingEffectsCodeEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::TrailingEffectsCodeEnum::TrailingEffectsCodeEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::TrailingEffectsCodeEnum::TrailingEffectsCodeEnum const &);
}


#endif
