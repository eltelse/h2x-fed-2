/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_SERVICETYPEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_SERVICETYPEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace ServiceTypeEnum {
        /**
        * Implementation of the <code>ServiceTypeEnum8</code> data type from the FOM.
        * <br>Description from the FOM: <i>Service type</i>
        */
        enum ServiceTypeEnum {
            /** <code>Other</code> (with ordinal 0) */
            OTHER = 0,
            /** <code>Resupply</code> (with ordinal 1) */
            RESUPPLY = 1,
            /** <code>Repair</code> (with ordinal 2) */
            REPAIR = 2
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to ServiceTypeEnum: static_cast<DevStudio::ServiceTypeEnum::ServiceTypeEnum>(i)
        */
        LIBAPI bool isValid(const ServiceTypeEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::ServiceTypeEnum::ServiceTypeEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::ServiceTypeEnum::ServiceTypeEnum const &);
}


#endif
