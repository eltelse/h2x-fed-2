/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_EVENTTYPEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_EVENTTYPEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace EventTypeEnum {
        /**
        * Implementation of the <code>EventTypeEnum32</code> data type from the FOM.
        * <br>Description from the FOM: <i>Event type</i>
        */
        enum EventTypeEnum {
            /** <code>Other</code> (with ordinal 0) */
            OTHER = 0,
            /** <code>RanOutOfAmmunition</code> (with ordinal 2) */
            RAN_OUT_OF_AMMUNITION = 2,
            /** <code>KilledInAction</code> (with ordinal 3) */
            KILLED_IN_ACTION = 3,
            /** <code>Damage</code> (with ordinal 4) */
            DAMAGE = 4,
            /** <code>MobilityDisabled</code> (with ordinal 5) */
            MOBILITY_DISABLED = 5,
            /** <code>FireDisabled</code> (with ordinal 6) */
            FIRE_DISABLED = 6,
            /** <code>RanOutOfFuel</code> (with ordinal 7) */
            RAN_OUT_OF_FUEL = 7,
            /** <code>EntityInitialization</code> (with ordinal 8) */
            ENTITY_INITIALIZATION = 8,
            /** <code>RequestForIndirectFireOrCASMission</code> (with ordinal 9) */
            REQUEST_FOR_INDIRECT_FIRE_OR_CASMISSION = 9,
            /** <code>IndirectFireOrCASMission</code> (with ordinal 10) */
            INDIRECT_FIRE_OR_CASMISSION = 10,
            /** <code>MinefieldEntry</code> (with ordinal 11) */
            MINEFIELD_ENTRY = 11,
            /** <code>MinefieldDetonation</code> (with ordinal 12) */
            MINEFIELD_DETONATION = 12,
            /** <code>VehicleMasterPowerOn</code> (with ordinal 13) */
            VEHICLE_MASTER_POWER_ON = 13,
            /** <code>VehicleMasterPowerOff</code> (with ordinal 14) */
            VEHICLE_MASTER_POWER_OFF = 14,
            /** <code>AggregateStateChangeRequested</code> (with ordinal 15) */
            AGGREGATE_STATE_CHANGE_REQUESTED = 15,
            /** <code>PreventCollision_Detonation</code> (with ordinal 16) */
            PREVENT_COLLISION_DETONATION = 16,
            /** <code>OwnershipReport</code> (with ordinal 17) */
            OWNERSHIP_REPORT = 17
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to EventTypeEnum: static_cast<DevStudio::EventTypeEnum::EventTypeEnum>(i)
        */
        LIBAPI bool isValid(const EventTypeEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::EventTypeEnum::EventTypeEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::EventTypeEnum::EventTypeEnum const &);
}


#endif
