/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_AGGREGATEREINFORCMENTLEVELENUM_H
#define DEVELOPER_STUDIO_DATATYPES_AGGREGATEREINFORCMENTLEVELENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace AggregateReinforcmentLevelEnum {
        /**
        * Implementation of the <code>AggregateReinforcmentLevelEnum32</code> data type from the FOM.
        * <br>Description from the FOM: <i>-NULL-</i>
        */
        enum AggregateReinforcmentLevelEnum {
            /** <code>REINFORCEMENT_M</code> (with ordinal 0) */
            REINFORCEMENT_M = 0,
            /** <code>REINFORCEMENT_PP</code> (with ordinal 1) */
            REINFORCEMENT_PP = 1,
            /** <code>REINFORCEMENT_MM</code> (with ordinal 2) */
            REINFORCEMENT_MM = 2,
            /** <code>REINFORCEMENT_PMM</code> (with ordinal 3) */
            REINFORCEMENT_PMM = 3,
            /** <code>REINFORCEMENT_MPP</code> (with ordinal 4) */
            REINFORCEMENT_MPP = 4,
            /** <code>REINFORCEMENT_P</code> (with ordinal 5) */
            REINFORCEMENT_P = 5,
            /** <code>REINFORCEMENT_PM</code> (with ordinal 6) */
            REINFORCEMENT_PM = 6
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to AggregateReinforcmentLevelEnum: static_cast<DevStudio::AggregateReinforcmentLevelEnum::AggregateReinforcmentLevelEnum>(i)
        */
        LIBAPI bool isValid(const AggregateReinforcmentLevelEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::AggregateReinforcmentLevelEnum::AggregateReinforcmentLevelEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::AggregateReinforcmentLevelEnum::AggregateReinforcmentLevelEnum const &);
}


#endif
