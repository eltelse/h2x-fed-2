/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_MINEFIELDLANEMARKERSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_MINEFIELDLANEMARKERSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/LinearSegmentStruct.h>
#include <DevStudio/datatypes/VisibleSideLocationEnum.h>

namespace DevStudio {
   /**
   * Implementation of the <code>MinefieldLaneMarkerStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Record specifying a minefield lane marker segment</i>
   */
   class MinefieldLaneMarkerStruct {

   public:
      /**
      * Description from the FOM: <i>Specifies the minefield lane marker segment characteristics</i>.
      * <br>Description of the data type from the FOM: <i>Specifies linear object segment characteristics.</i>
      */
      LinearSegmentStruct segmentParameters;
      /**
      * Description from the FOM: <i>Specifies the visible side(s) of the minefield lane marker segment</i>.
      * <br>Description of the data type from the FOM: <i>Minefield lane marker visible side</i>
      */
      VisibleSideLocationEnum::VisibleSideLocationEnum visibleSideLocation;

      LIBAPI MinefieldLaneMarkerStruct()
         :
         segmentParameters(LinearSegmentStruct()),
         visibleSideLocation(VisibleSideLocationEnum::VisibleSideLocationEnum())
      {}

      /**
      * Constructor for MinefieldLaneMarkerStruct
      *
      * @param segmentParameters_ value to set as segmentParameters.
      * <br>Description from the FOM: <i>Specifies the minefield lane marker segment characteristics</i>
      * <br>Description of the data type from the FOM: <i>Specifies linear object segment characteristics.</i>
      * @param visibleSideLocation_ value to set as visibleSideLocation.
      * <br>Description from the FOM: <i>Specifies the visible side(s) of the minefield lane marker segment</i>
      * <br>Description of the data type from the FOM: <i>Minefield lane marker visible side</i>
      */
      LIBAPI MinefieldLaneMarkerStruct(
         LinearSegmentStruct segmentParameters_,
         VisibleSideLocationEnum::VisibleSideLocationEnum visibleSideLocation_
         )
         :
         segmentParameters(segmentParameters_),
         visibleSideLocation(visibleSideLocation_)
      {}



      /**
      * Function to get segmentParameters.
      * <br>Description from the FOM: <i>Specifies the minefield lane marker segment characteristics</i>
      * <br>Description of the data type from the FOM: <i>Specifies linear object segment characteristics.</i>
      *
      * @return segmentParameters
      */
      LIBAPI DevStudio::LinearSegmentStruct & getSegmentParameters() {
         return segmentParameters;
      }

      /**
      * Function to get visibleSideLocation.
      * <br>Description from the FOM: <i>Specifies the visible side(s) of the minefield lane marker segment</i>
      * <br>Description of the data type from the FOM: <i>Minefield lane marker visible side</i>
      *
      * @return visibleSideLocation
      */
      LIBAPI DevStudio::VisibleSideLocationEnum::VisibleSideLocationEnum & getVisibleSideLocation() {
         return visibleSideLocation;
      }

   };


   LIBAPI bool operator ==(const DevStudio::MinefieldLaneMarkerStruct& l, const DevStudio::MinefieldLaneMarkerStruct& r);
   LIBAPI bool operator !=(const DevStudio::MinefieldLaneMarkerStruct& l, const DevStudio::MinefieldLaneMarkerStruct& r);
   LIBAPI bool operator <(const DevStudio::MinefieldLaneMarkerStruct& l, const DevStudio::MinefieldLaneMarkerStruct& r);
   LIBAPI bool operator >(const DevStudio::MinefieldLaneMarkerStruct& l, const DevStudio::MinefieldLaneMarkerStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::MinefieldLaneMarkerStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::MinefieldLaneMarkerStruct const &);
}
#endif
