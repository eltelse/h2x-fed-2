/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_IFFSYSTEMNAMEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_IFFSYSTEMNAMEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace IffSystemNameEnum {
        /**
        * Implementation of the <code>IffSystemNameEnum16</code> data type from the FOM.
        * <br>Description from the FOM: <i>IFF system name</i>
        */
        enum IffSystemNameEnum {
            /** <code>Other</code> (with ordinal 0) */
            OTHER = 0,
            /** <code>MarkX</code> (with ordinal 1) */
            MARK_X = 1,
            /** <code>MarkXII</code> (with ordinal 2) */
            MARK_XII = 2,
            /** <code>ATCRBS</code> (with ordinal 3) */
            ATCRBS = 3,
            /** <code>Soviet</code> (with ordinal 4) */
            SOVIET = 4,
            /** <code>ModeS</code> (with ordinal 5) */
            MODE_S = 5,
            /** <code>MarkX-XII-ATCRBS</code> (with ordinal 6) */
            MARK_X_XII_ATCRBS = 6,
            /** <code>Mark-X-XII-ATCRBS-ModeS</code> (with ordinal 7) */
            MARK_X_XII_ATCRBS_MODE_S = 7,
            /** <code>ARI5954</code> (with ordinal 8) */
            ARI5954 = 8,
            /** <code>ARI5983</code> (with ordinal 9) */
            ARI5983 = 9,
            /** <code>GenericRRB</code> (with ordinal 10) */
            GENERIC_RRB = 10,
            /** <code>GenericMarkXIIA</code> (with ordinal 11) */
            GENERIC_MARK_XIIA = 11,
            /** <code>GenericMode5</code> (with ordinal 12) */
            GENERIC_MODE5 = 12,
            /** <code>GenericMarkXIIACombinedInterrogator_Transponder_CIT_</code> (with ordinal 13) */
            GENERIC_MARK_XIIACOMBINED_INTERROGATOR_TRANSPONDER_CIT = 13,
            /** <code>GenericMarkXIICombinedInterrogator_Transponder_CIT_</code> (with ordinal 14) */
            GENERIC_MARK_XIICOMBINED_INTERROGATOR_TRANSPONDER_CIT = 14,
            /** <code>GenericTCASI_ACASITransceiver</code> (with ordinal 15) */
            GENERIC_TCASI_ACASITRANSCEIVER = 15,
            /** <code>GenericTCASII_ACASIITransceiver</code> (with ordinal 16) */
            GENERIC_TCASII_ACASIITRANSCEIVER = 16,
            /** <code>GenericMarkX_A_</code> (with ordinal 17) */
            GENERIC_MARK_X_A = 17,
            /** <code>GenericMarkX_SIF_</code> (with ordinal 18) */
            GENERIC_MARK_X_SIF = 18
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to IffSystemNameEnum: static_cast<DevStudio::IffSystemNameEnum::IffSystemNameEnum>(i)
        */
        LIBAPI bool isValid(const IffSystemNameEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::IffSystemNameEnum::IffSystemNameEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::IffSystemNameEnum::IffSystemNameEnum const &);
}


#endif
