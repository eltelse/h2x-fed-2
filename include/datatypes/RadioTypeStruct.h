/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_RADIOTYPESTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_RADIOTYPESTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/NomenclatureEnum.h>
#include <DevStudio/datatypes/NomenclatureVersionEnum.h>

namespace DevStudio {
   /**
   * Implementation of the <code>RadioTypeStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Specifies the type of a radio.</i>
   */
   class RadioTypeStruct {

   public:
      /**
      * Description from the FOM: <i>The kind of the entity described in this struct.</i>.
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      char entityKind;
      /**
      * Description from the FOM: <i>The domain in which the radio entity operates.</i>.
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      char domain;
      /**
      * Description from the FOM: <i>The country to which the design of the radio entity is attributed.</i>.
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      unsigned short countryCode;
      /**
      * Description from the FOM: <i>The main category that describes the radio entity.</i>.
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      char category;
      /**
      * Description from the FOM: <i>The specific modification or individual unit type for a series and/or family of equipment.</i>.
      * <br>Description of the data type from the FOM: <i>The naming convention applied to the radio system by the manufacturer/controlling agency.</i>
      */
      NomenclatureVersionEnum::NomenclatureVersionEnum nomenclatureVersion;
      /**
      * Description from the FOM: <i>The nomenclature for a particular communications device.</i>.
      * <br>Description of the data type from the FOM: <i>The nomenclature for a specific radio system.</i>
      */
      NomenclatureEnum::NomenclatureEnum nomenclature;

      LIBAPI RadioTypeStruct()
         :
         entityKind(0),
         domain(0),
         countryCode(0),
         category(0),
         nomenclatureVersion(NomenclatureVersionEnum::NomenclatureVersionEnum()),
         nomenclature(NomenclatureEnum::NomenclatureEnum())
      {}

      /**
      * Constructor for RadioTypeStruct
      *
      * @param entityKind_ value to set as entityKind.
      * <br>Description from the FOM: <i>The kind of the entity described in this struct.</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param domain_ value to set as domain.
      * <br>Description from the FOM: <i>The domain in which the radio entity operates.</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param countryCode_ value to set as countryCode.
      * <br>Description from the FOM: <i>The country to which the design of the radio entity is attributed.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param category_ value to set as category.
      * <br>Description from the FOM: <i>The main category that describes the radio entity.</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param nomenclatureVersion_ value to set as nomenclatureVersion.
      * <br>Description from the FOM: <i>The specific modification or individual unit type for a series and/or family of equipment.</i>
      * <br>Description of the data type from the FOM: <i>The naming convention applied to the radio system by the manufacturer/controlling agency.</i>
      * @param nomenclature_ value to set as nomenclature.
      * <br>Description from the FOM: <i>The nomenclature for a particular communications device.</i>
      * <br>Description of the data type from the FOM: <i>The nomenclature for a specific radio system.</i>
      */
      LIBAPI RadioTypeStruct(
         char entityKind_,
         char domain_,
         unsigned short countryCode_,
         char category_,
         NomenclatureVersionEnum::NomenclatureVersionEnum nomenclatureVersion_,
         NomenclatureEnum::NomenclatureEnum nomenclature_
         )
         :
         entityKind(entityKind_),
         domain(domain_),
         countryCode(countryCode_),
         category(category_),
         nomenclatureVersion(nomenclatureVersion_),
         nomenclature(nomenclature_)
      {}



      /**
      * Function to get entityKind.
      * <br>Description from the FOM: <i>The kind of the entity described in this struct.</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return entityKind
      */
      LIBAPI char & getEntityKind() {
         return entityKind;
      }

      /**
      * Function to get domain.
      * <br>Description from the FOM: <i>The domain in which the radio entity operates.</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return domain
      */
      LIBAPI char & getDomain() {
         return domain;
      }

      /**
      * Function to get countryCode.
      * <br>Description from the FOM: <i>The country to which the design of the radio entity is attributed.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return countryCode
      */
      LIBAPI unsigned short & getCountryCode() {
         return countryCode;
      }

      /**
      * Function to get category.
      * <br>Description from the FOM: <i>The main category that describes the radio entity.</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return category
      */
      LIBAPI char & getCategory() {
         return category;
      }

      /**
      * Function to get nomenclatureVersion.
      * <br>Description from the FOM: <i>The specific modification or individual unit type for a series and/or family of equipment.</i>
      * <br>Description of the data type from the FOM: <i>The naming convention applied to the radio system by the manufacturer/controlling agency.</i>
      *
      * @return nomenclatureVersion
      */
      LIBAPI DevStudio::NomenclatureVersionEnum::NomenclatureVersionEnum & getNomenclatureVersion() {
         return nomenclatureVersion;
      }

      /**
      * Function to get nomenclature.
      * <br>Description from the FOM: <i>The nomenclature for a particular communications device.</i>
      * <br>Description of the data type from the FOM: <i>The nomenclature for a specific radio system.</i>
      *
      * @return nomenclature
      */
      LIBAPI DevStudio::NomenclatureEnum::NomenclatureEnum & getNomenclature() {
         return nomenclature;
      }

   };


   LIBAPI bool operator ==(const DevStudio::RadioTypeStruct& l, const DevStudio::RadioTypeStruct& r);
   LIBAPI bool operator !=(const DevStudio::RadioTypeStruct& l, const DevStudio::RadioTypeStruct& r);
   LIBAPI bool operator <(const DevStudio::RadioTypeStruct& l, const DevStudio::RadioTypeStruct& r);
   LIBAPI bool operator >(const DevStudio::RadioTypeStruct& l, const DevStudio::RadioTypeStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::RadioTypeStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::RadioTypeStruct const &);
}
#endif
