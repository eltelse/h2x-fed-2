/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_FIXEDDATUMSTRUCTLENGTHLESSARRAY_H
#define DEVELOPER_STUDIO_DATATYPES_FIXEDDATUMSTRUCTLENGTHLESSARRAY_H

#include <iostream>
#include <vector>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/FixedDatumStruct.h>

namespace DevStudio {
   LIBAPI std::wostream & operator << (std::wostream &, std::vector< DevStudio::FixedDatumStruct > const &);
   LIBAPI std::ostream & operator << (std::ostream &, std::vector< DevStudio::FixedDatumStruct > const &);
}
#endif
