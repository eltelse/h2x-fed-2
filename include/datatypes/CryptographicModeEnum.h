/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_CRYPTOGRAPHICMODEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_CRYPTOGRAPHICMODEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace CryptographicModeEnum {
        /**
        * Implementation of the <code>CryptographicModeEnum32</code> data type from the FOM.
        * <br>Description from the FOM: <i>Represents the encryption mode of a cryptographic system.</i>
        */
        enum CryptographicModeEnum {
            /** <code>BasebandEncryption</code> (with ordinal 0) */
            BASEBAND_ENCRYPTION = 0,
            /** <code>DiphaseEncryption</code> (with ordinal 1) */
            DIPHASE_ENCRYPTION = 1
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to CryptographicModeEnum: static_cast<DevStudio::CryptographicModeEnum::CryptographicModeEnum>(i)
        */
        LIBAPI bool isValid(const CryptographicModeEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::CryptographicModeEnum::CryptographicModeEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::CryptographicModeEnum::CryptographicModeEnum const &);
}


#endif
