/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_DAMAGESTATUSENUM_H
#define DEVELOPER_STUDIO_DATATYPES_DAMAGESTATUSENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace DamageStatusEnum {
        /**
        * Implementation of the <code>DamageStatusEnum32</code> data type from the FOM.
        * <br>Description from the FOM: <i>Damaged appearance</i>
        */
        enum DamageStatusEnum {
            /** <code>NoDamage</code> (with ordinal 0) */
            NO_DAMAGE = 0,
            /** <code>SlightDamage</code> (with ordinal 1) */
            SLIGHT_DAMAGE = 1,
            /** <code>ModerateDamage</code> (with ordinal 2) */
            MODERATE_DAMAGE = 2,
            /** <code>Destroyed</code> (with ordinal 3) */
            DESTROYED = 3
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to DamageStatusEnum: static_cast<DevStudio::DamageStatusEnum::DamageStatusEnum>(i)
        */
        LIBAPI bool isValid(const DamageStatusEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::DamageStatusEnum::DamageStatusEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::DamageStatusEnum::DamageStatusEnum const &);
}


#endif
