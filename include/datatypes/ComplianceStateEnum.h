/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_COMPLIANCESTATEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_COMPLIANCESTATEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace ComplianceStateEnum {
        /**
        * Implementation of the <code>ComplianceStateEnum32</code> data type from the FOM.
        * <br>Description from the FOM: <i>Life form compliance</i>
        */
        enum ComplianceStateEnum {
            /** <code>Other</code> (with ordinal 0) */
            OTHER = 0,
            /** <code>Detained</code> (with ordinal 1) */
            DETAINED = 1,
            /** <code>Surrender</code> (with ordinal 2) */
            SURRENDER = 2,
            /** <code>UsingFists</code> (with ordinal 3) */
            USING_FISTS = 3,
            /** <code>VerbalAbuse1</code> (with ordinal 4) */
            VERBAL_ABUSE1 = 4,
            /** <code>VerbalAbuse2</code> (with ordinal 5) */
            VERBAL_ABUSE2 = 5,
            /** <code>VerbalAbuse3</code> (with ordinal 6) */
            VERBAL_ABUSE3 = 6,
            /** <code>PassiveResistance1</code> (with ordinal 7) */
            PASSIVE_RESISTANCE1 = 7,
            /** <code>PassiveResistance2</code> (with ordinal 8) */
            PASSIVE_RESISTANCE2 = 8,
            /** <code>PassiveResistance3</code> (with ordinal 9) */
            PASSIVE_RESISTANCE3 = 9,
            /** <code>NonLethalWeapon1</code> (with ordinal 10) */
            NON_LETHAL_WEAPON1 = 10,
            /** <code>NonLethalWeapon2</code> (with ordinal 11) */
            NON_LETHAL_WEAPON2 = 11,
            /** <code>NonLethalWeapon3</code> (with ordinal 12) */
            NON_LETHAL_WEAPON3 = 12,
            /** <code>NonLethalWeapon4</code> (with ordinal 13) */
            NON_LETHAL_WEAPON4 = 13,
            /** <code>NonLethalWeapon5</code> (with ordinal 14) */
            NON_LETHAL_WEAPON5 = 14,
            /** <code>NonLethalWeapon6</code> (with ordinal 15) */
            NON_LETHAL_WEAPON6 = 15
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to ComplianceStateEnum: static_cast<DevStudio::ComplianceStateEnum::ComplianceStateEnum>(i)
        */
        LIBAPI bool isValid(const ComplianceStateEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::ComplianceStateEnum::ComplianceStateEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::ComplianceStateEnum::ComplianceStateEnum const &);
}


#endif
