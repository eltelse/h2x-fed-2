/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_SPATIALSTATICSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_SPATIALSTATICSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <RtiDriver/RprUtility/RprUtility.h>
#include <DevStudio/HlaPointers.h>

#include <DevStudio/datatypes/OrientationStruct.h>
#include <DevStudio/datatypes/WorldLocationStruct.h>

namespace DevStudio {
   /**
   * Implementation of the <code>SpatialStaticStruct</code> data type from the FOM.
   * This datatype can be used with the RprUtility package. Please see the Overview document
   * located in the project root directory for more information.
   * <br>Description from the FOM: <i>Spatial structure for Dead Reckoning Algorithm Static (1).</i>
   */
   class SpatialStaticStruct {

   public:
      /**
      * Description from the FOM: <i>Location of the object.</i>.
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      */
      WorldLocationStruct worldLocation;
      /**
      * Description from the FOM: <i>Whether the object is frozen or not.</i>.
      * <br>Description of the data type from the FOM: <i></i>
      */
      bool isFrozen;
      /**
      * Description from the FOM: <i>The angles of rotation around the coordinate axes between the object's attitude and the reference coordinate system axes (calculated as the Tait-Bryan Euler angles specifying the successive rotations needed to transform from the world coordinate system to the entity coordinate system).</i>.
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      */
      OrientationStruct orientation;

      LIBAPI SpatialStaticStruct()
         :
         worldLocation(WorldLocationStruct()),
         isFrozen(0),
         orientation(OrientationStruct())
      {}

      /**
      * Constructor for SpatialStaticStruct
      *
      * @param worldLocation_ value to set as worldLocation.
      * <br>Description from the FOM: <i>Location of the object.</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      * @param isFrozen_ value to set as isFrozen.
      * <br>Description from the FOM: <i>Whether the object is frozen or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      * @param orientation_ value to set as orientation.
      * <br>Description from the FOM: <i>The angles of rotation around the coordinate axes between the object's attitude and the reference coordinate system axes (calculated as the Tait-Bryan Euler angles specifying the successive rotations needed to transform from the world coordinate system to the entity coordinate system).</i>
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      */
      LIBAPI SpatialStaticStruct(
         WorldLocationStruct worldLocation_,
         bool isFrozen_,
         OrientationStruct orientation_
         )
         :
         worldLocation(worldLocation_),
         isFrozen(isFrozen_),
         orientation(orientation_)
      {}

      /**
      * Convert to RprUtility datatype
      *
      * @param spatialStatic SpatialStaticStruct to convert
      *
      * @return spatialStatic converted to RprUtility::SpatialStructStatic
      */
      LIBAPI static RprUtility::SpatialStructStatic convert(const SpatialStaticStruct spatialStatic);

      /**
      * Convert to RprUtility datatype
      *
      * @param spatialStatic SpatialStaticStructPtr to convert
      *
      * @return spatialStatic converted to RprUtility::SpatialStructStatic
      */
      LIBAPI static RprUtility::SpatialStructStatic convert(const SpatialStaticStructPtr spatialStatic);

      /**
      * Convert from RprUtility datatype
      *
      * @param spatialStatic RprUtility::SpatialStructStatic to convert to SpatialStaticStruct
      *
      * @return spatialStatic converted to SpatialStaticStruct
      */
      LIBAPI static SpatialStaticStruct convert(const RprUtility::SpatialStructStatic spatialStatic);


      /**
      * Function to get worldLocation.
      * <br>Description from the FOM: <i>Location of the object.</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @return worldLocation
      */
      LIBAPI DevStudio::WorldLocationStruct & getWorldLocation() {
         return worldLocation;
      }

      /**
      * Function to get isFrozen.
      * <br>Description from the FOM: <i>Whether the object is frozen or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return isFrozen
      */
      LIBAPI bool & getIsFrozen() {
         return isFrozen;
      }

      /**
      * Function to get orientation.
      * <br>Description from the FOM: <i>The angles of rotation around the coordinate axes between the object's attitude and the reference coordinate system axes (calculated as the Tait-Bryan Euler angles specifying the successive rotations needed to transform from the world coordinate system to the entity coordinate system).</i>
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @return orientation
      */
      LIBAPI DevStudio::OrientationStruct & getOrientation() {
         return orientation;
      }

   };


   LIBAPI bool operator ==(const DevStudio::SpatialStaticStruct& l, const DevStudio::SpatialStaticStruct& r);
   LIBAPI bool operator !=(const DevStudio::SpatialStaticStruct& l, const DevStudio::SpatialStaticStruct& r);
   LIBAPI bool operator <(const DevStudio::SpatialStaticStruct& l, const DevStudio::SpatialStaticStruct& r);
   LIBAPI bool operator >(const DevStudio::SpatialStaticStruct& l, const DevStudio::SpatialStaticStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::SpatialStaticStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::SpatialStaticStruct const &);
}
#endif
