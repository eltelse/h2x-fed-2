/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_ACTIONENUM_H
#define DEVELOPER_STUDIO_DATATYPES_ACTIONENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace ActionEnum {
        /**
        * Implementation of the <code>ActionEnum32</code> data type from the FOM.
        * <br>Description from the FOM: <i>Action ID</i>
        */
        enum ActionEnum {
            /** <code>Other</code> (with ordinal 0) */
            OTHER = 0,
            /** <code>LocalStorageOfTheRequestedInformation</code> (with ordinal 1) */
            LOCAL_STORAGE_OF_THE_REQUESTED_INFORMATION = 1,
            /** <code>InformSimulationManagerOfRanOutOfAmmunitionEvent</code> (with ordinal 2) */
            INFORM_SIMULATION_MANAGER_OF_RAN_OUT_OF_AMMUNITION_EVENT = 2,
            /** <code>InformSimulationManagerOfKilledInActionEvent</code> (with ordinal 3) */
            INFORM_SIMULATION_MANAGER_OF_KILLED_IN_ACTION_EVENT = 3,
            /** <code>InformSimulationManagerOfDamageEvent</code> (with ordinal 4) */
            INFORM_SIMULATION_MANAGER_OF_DAMAGE_EVENT = 4,
            /** <code>InformSimulationManagerOfMobilityDisabledEvent</code> (with ordinal 5) */
            INFORM_SIMULATION_MANAGER_OF_MOBILITY_DISABLED_EVENT = 5,
            /** <code>InformSimulationManagerOfFireDisabledEvent</code> (with ordinal 6) */
            INFORM_SIMULATION_MANAGER_OF_FIRE_DISABLED_EVENT = 6,
            /** <code>InformSimulationManagerOfRanOutOfFuelEvent</code> (with ordinal 7) */
            INFORM_SIMULATION_MANAGER_OF_RAN_OUT_OF_FUEL_EVENT = 7,
            /** <code>RecallCheckpointData</code> (with ordinal 8) */
            RECALL_CHECKPOINT_DATA = 8,
            /** <code>RecallInitialParameters</code> (with ordinal 9) */
            RECALL_INITIAL_PARAMETERS = 9,
            /** <code>InitiateTetherLead</code> (with ordinal 10) */
            INITIATE_TETHER_LEAD = 10,
            /** <code>InitiateTetherFollow</code> (with ordinal 11) */
            INITIATE_TETHER_FOLLOW = 11,
            /** <code>Untether</code> (with ordinal 12) */
            UNTETHER = 12,
            /** <code>InitiateServiceStationResupply</code> (with ordinal 13) */
            INITIATE_SERVICE_STATION_RESUPPLY = 13,
            /** <code>InitiateTailgateResupply</code> (with ordinal 14) */
            INITIATE_TAILGATE_RESUPPLY = 14,
            /** <code>InitiateHitchLead</code> (with ordinal 15) */
            INITIATE_HITCH_LEAD = 15,
            /** <code>InitiateHitchFollow</code> (with ordinal 16) */
            INITIATE_HITCH_FOLLOW = 16,
            /** <code>Unhitch</code> (with ordinal 17) */
            UNHITCH = 17,
            /** <code>Mount</code> (with ordinal 18) */
            MOUNT = 18,
            /** <code>Dismount</code> (with ordinal 19) */
            DISMOUNT = 19,
            /** <code>StartDailyReadinessCheck</code> (with ordinal 20) */
            START_DAILY_READINESS_CHECK = 20,
            /** <code>StopDailyReadinessCheck</code> (with ordinal 21) */
            STOP_DAILY_READINESS_CHECK = 21,
            /** <code>DataQuery</code> (with ordinal 22) */
            DATA_QUERY = 22,
            /** <code>StatusRequest</code> (with ordinal 23) */
            STATUS_REQUEST = 23,
            /** <code>SendObjectStateData</code> (with ordinal 24) */
            SEND_OBJECT_STATE_DATA = 24,
            /** <code>Reconstitute</code> (with ordinal 25) */
            RECONSTITUTE = 25,
            /** <code>LockSiteConfiguration</code> (with ordinal 26) */
            LOCK_SITE_CONFIGURATION = 26,
            /** <code>UnlockSiteConfiguration</code> (with ordinal 27) */
            UNLOCK_SITE_CONFIGURATION = 27,
            /** <code>UpdateSiteConfiguration</code> (with ordinal 28) */
            UPDATE_SITE_CONFIGURATION = 28,
            /** <code>QuerySiteConfiguration</code> (with ordinal 29) */
            QUERY_SITE_CONFIGURATION = 29,
            /** <code>TetheringInformation</code> (with ordinal 30) */
            TETHERING_INFORMATION = 30,
            /** <code>MountIntent</code> (with ordinal 31) */
            MOUNT_INTENT = 31,
            /** <code>AcceptSubscription</code> (with ordinal 33) */
            ACCEPT_SUBSCRIPTION = 33,
            /** <code>Unsubscribe</code> (with ordinal 34) */
            UNSUBSCRIBE = 34,
            /** <code>TeleportEntity</code> (with ordinal 35) */
            TELEPORT_ENTITY = 35,
            /** <code>ChangeAggregateState</code> (with ordinal 36) */
            CHANGE_AGGREGATE_STATE = 36,
            /** <code>RequestStartPDU</code> (with ordinal 37) */
            REQUEST_START_PDU = 37,
            /** <code>WakeupGetReadyForInitialization</code> (with ordinal 38) */
            WAKEUP_GET_READY_FOR_INITIALIZATION = 38,
            /** <code>InitializeInternalParameters</code> (with ordinal 39) */
            INITIALIZE_INTERNAL_PARAMETERS = 39,
            /** <code>SendPlanData</code> (with ordinal 40) */
            SEND_PLAN_DATA = 40,
            /** <code>SynchronizeInternalClocks</code> (with ordinal 41) */
            SYNCHRONIZE_INTERNAL_CLOCKS = 41,
            /** <code>Run</code> (with ordinal 42) */
            RUN = 42,
            /** <code>SaveInternalParameters</code> (with ordinal 43) */
            SAVE_INTERNAL_PARAMETERS = 43,
            /** <code>SimulateMalfunction</code> (with ordinal 44) */
            SIMULATE_MALFUNCTION = 44,
            /** <code>JoinExercise</code> (with ordinal 45) */
            JOIN_EXERCISE = 45,
            /** <code>ResignExercise</code> (with ordinal 46) */
            RESIGN_EXERCISE = 46,
            /** <code>TimeAdvance</code> (with ordinal 47) */
            TIME_ADVANCE = 47,
            /** <code>TACCSF_LOS_Request-Type1</code> (with ordinal 100) */
            TACCSF_LOS_REQUEST_TYPE1 = 100,
            /** <code>TACCSF_LOS_Request-Type2</code> (with ordinal 101) */
            TACCSF_LOS_REQUEST_TYPE2 = 101
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to ActionEnum: static_cast<DevStudio::ActionEnum::ActionEnum>(i)
        */
        LIBAPI bool isValid(const ActionEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::ActionEnum::ActionEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::ActionEnum::ActionEnum const &);
}


#endif
