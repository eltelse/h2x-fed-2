/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_TACTICALDATALINKTYPEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_TACTICALDATALINKTYPEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace TacticalDataLinkTypeEnum {
        /**
        * Implementation of the <code>TacticalDataLinkTypeEnum16</code> data type from the FOM.
        * <br>Description from the FOM: <i>The type of tactical data link used to transmit a signal</i>
        */
        enum TacticalDataLinkTypeEnum {
            /** <code>Other</code> (with ordinal 0) */
            OTHER = 0,
            /** <code>PADIL</code> (with ordinal 1) */
            PADIL = 1,
            /** <code>NATOLink-1</code> (with ordinal 2) */
            NATOLINK_1 = 2,
            /** <code>ATDL-1</code> (with ordinal 3) */
            ATDL_1 = 3,
            /** <code>Link11B_TADILB_</code> (with ordinal 4) */
            LINK11B_TADILB = 4,
            /** <code>SituationalAwarenessDataLink_SADL_</code> (with ordinal 5) */
            SITUATIONAL_AWARENESS_DATA_LINK_SADL = 5,
            /** <code>Link16LegacyFormat_JTIDS_TADIL-J_</code> (with ordinal 6) */
            LINK16LEGACY_FORMAT_JTIDS_TADIL_J = 6,
            /** <code>Link16LegacyFormat_JTIDS_FDL_TADIL-J_</code> (with ordinal 7) */
            LINK16LEGACY_FORMAT_JTIDS_FDL_TADIL_J = 7,
            /** <code>Link11A_TADILA_</code> (with ordinal 8) */
            LINK11A_TADILA = 8,
            /** <code>IJMS</code> (with ordinal 9) */
            IJMS = 9,
            /** <code>Link4A_TADILC_</code> (with ordinal 10) */
            LINK4A_TADILC = 10,
            /** <code>Link4C</code> (with ordinal 11) */
            LINK4C = 11,
            /** <code>TIBS</code> (with ordinal 12) */
            TIBS = 12,
            /** <code>ATL</code> (with ordinal 13) */
            ATL = 13,
            /** <code>ConstantSource</code> (with ordinal 14) */
            CONSTANT_SOURCE = 14,
            /** <code>Abbreviated_Command_and_Control</code> (with ordinal 15) */
            ABBREVIATED_COMMAND_AND_CONTROL = 15,
            /** <code>MILSTAR</code> (with ordinal 16) */
            MILSTAR = 16,
            /** <code>ATHS</code> (with ordinal 17) */
            ATHS = 17,
            /** <code>OTHGOLD</code> (with ordinal 18) */
            OTHGOLD = 18,
            /** <code>TACELINT</code> (with ordinal 19) */
            TACELINT = 19,
            /** <code>WeaponsDataLink_AWW-13_</code> (with ordinal 20) */
            WEAPONS_DATA_LINK_AWW_13 = 20,
            /** <code>AbbreviatedCommandAndControl</code> (with ordinal 21) */
            ABBREVIATED_COMMAND_AND_CONTROL_2 = 21,
            /** <code>EnhancedPositionLocationReportingSystem_EPLRS_</code> (with ordinal 22) */
            ENHANCED_POSITION_LOCATION_REPORTING_SYSTEM_EPLRS = 22,
            /** <code>PositionLocationReportingSystem_PLRS_</code> (with ordinal 23) */
            POSITION_LOCATION_REPORTING_SYSTEM_PLRS = 23,
            /** <code>SINCGARS</code> (with ordinal 24) */
            SINCGARS = 24,
            /** <code>HaveQuickI</code> (with ordinal 25) */
            HAVE_QUICK_I = 25,
            /** <code>HaveQuickII</code> (with ordinal 26) */
            HAVE_QUICK_II = 26,
            /** <code>HaveQuickIIA_Saturn_</code> (with ordinal 27) */
            HAVE_QUICK_IIA_SATURN = 27,
            /** <code>Intra-FlightDataLink1</code> (with ordinal 28) */
            INTRA_FLIGHT_DATA_LINK1 = 28,
            /** <code>Intra-FlightDataLink2</code> (with ordinal 29) */
            INTRA_FLIGHT_DATA_LINK2 = 29,
            /** <code>ImprovedDataModem_IDM_</code> (with ordinal 30) */
            IMPROVED_DATA_MODEM_IDM = 30,
            /** <code>AirForceApplicationProgramDevelopment_AFAPD_</code> (with ordinal 31) */
            AIR_FORCE_APPLICATION_PROGRAM_DEVELOPMENT_AFAPD = 31,
            /** <code>CooperativeEngagementCapability_CEC_</code> (with ordinal 32) */
            COOPERATIVE_ENGAGEMENT_CAPABILITY_CEC = 32,
            /** <code>ForwardAreaAirDefense_FAAD_DataLink_FDL_</code> (with ordinal 33) */
            FORWARD_AREA_AIR_DEFENSE_FAAD_DATA_LINK_FDL = 33,
            /** <code>GroundBasedDataLink_GBDL_</code> (with ordinal 34) */
            GROUND_BASED_DATA_LINK_GBDL = 34,
            /** <code>IntraVehicularInfoSystem_IVIS_</code> (with ordinal 35) */
            INTRA_VEHICULAR_INFO_SYSTEM_IVIS = 35,
            /** <code>MarineTacticalSystem_MTS_</code> (with ordinal 36) */
            MARINE_TACTICAL_SYSTEM_MTS = 36,
            /** <code>TacticalFireDirectionSystem_TACFIRE_</code> (with ordinal 37) */
            TACTICAL_FIRE_DIRECTION_SYSTEM_TACFIRE = 37,
            /** <code>IntegratedBroadcastService_IBS_</code> (with ordinal 38) */
            INTEGRATED_BROADCAST_SERVICE_IBS = 38,
            /** <code>AirborneInformationTransfer_ABIT_</code> (with ordinal 39) */
            AIRBORNE_INFORMATION_TRANSFER_ABIT = 39,
            /** <code>AdvancedTacticalAirborneReconnaissanceSystem_ATARS_DataLink</code> (with ordinal 40) */
            ADVANCED_TACTICAL_AIRBORNE_RECONNAISSANCE_SYSTEM_ATARS_DATA_LINK = 40,
            /** <code>BattleGroupPassiveHorizonExtensionSystem_BGPHES_DataLink</code> (with ordinal 41) */
            BATTLE_GROUP_PASSIVE_HORIZON_EXTENSION_SYSTEM_BGPHES_DATA_LINK = 41,
            /** <code>CommonHighBandwidthDataLink_CHBDL_</code> (with ordinal 42) */
            COMMON_HIGH_BANDWIDTH_DATA_LINK_CHBDL = 42,
            /** <code>GuardrailInteroperableDataLink_IDL_</code> (with ordinal 43) */
            GUARDRAIL_INTEROPERABLE_DATA_LINK_IDL = 43,
            /** <code>GuardrailCommonSensorSystemOne_CSS1_DataLink</code> (with ordinal 44) */
            GUARDRAIL_COMMON_SENSOR_SYSTEM_ONE_CSS1_DATA_LINK = 44,
            /** <code>GuardrailCommonSensorSystemTwo_CSS2_DataLink</code> (with ordinal 45) */
            GUARDRAIL_COMMON_SENSOR_SYSTEM_TWO_CSS2_DATA_LINK = 45,
            /** <code>GuardrailCSS2Multi-RoleDataLink_MRDL_</code> (with ordinal 46) */
            GUARDRAIL_CSS2MULTI_ROLE_DATA_LINK_MRDL = 46,
            /** <code>GuardrailCSS2DirectAirToSatelliteRelay_DASR_DataLink</code> (with ordinal 47) */
            GUARDRAIL_CSS2DIRECT_AIR_TO_SATELLITE_RELAY_DASR_DATA_LINK = 47,
            /** <code>LineOfSight_LOS_DataLinkImplementation_LOSTether_</code> (with ordinal 48) */
            LINE_OF_SIGHT_LOS_DATA_LINK_IMPLEMENTATION_LOSTETHER = 48,
            /** <code>LightweightCDL_LWCDL_</code> (with ordinal 49) */
            LIGHTWEIGHT_CDL_LWCDL = 49,
            /** <code>L-52M_SR-71_</code> (with ordinal 50) */
            L_52M_SR_71 = 50,
            /** <code>RivetReach_RivetOwlDataLink</code> (with ordinal 51) */
            RIVET_REACH_RIVET_OWL_DATA_LINK = 51,
            /** <code>SeniorSpan</code> (with ordinal 52) */
            SENIOR_SPAN = 52,
            /** <code>SeniorSpur</code> (with ordinal 53) */
            SENIOR_SPUR = 53,
            /** <code>SeniorStretch_</code> (with ordinal 54) */
            SENIOR_STRETCH = 54,
            /** <code>SeniorYearInteroperableDataLink_IDL_</code> (with ordinal 55) */
            SENIOR_YEAR_INTEROPERABLE_DATA_LINK_IDL = 55,
            /** <code>SpaceCDL</code> (with ordinal 56) */
            SPACE_CDL = 56,
            /** <code>TR-1ModeMISTAirborneDataLink</code> (with ordinal 57) */
            TR_1MODE_MISTAIRBORNE_DATA_LINK = 57,
            /** <code>Ku-bandSATCOMDataLinkImplementation_UAV_</code> (with ordinal 58) */
            KU_BAND_SATCOMDATA_LINK_IMPLEMENTATION_UAV = 58,
            /** <code>MissionEquipmentControlDataLink_MECDL_</code> (with ordinal 59) */
            MISSION_EQUIPMENT_CONTROL_DATA_LINK_MECDL = 59,
            /** <code>RadarDataTransmittingSetDataLink</code> (with ordinal 60) */
            RADAR_DATA_TRANSMITTING_SET_DATA_LINK = 60,
            /** <code>SurveillanceAndControlDataLink_SCDL_</code> (with ordinal 61) */
            SURVEILLANCE_AND_CONTROL_DATA_LINK_SCDL = 61,
            /** <code>TacticalUAVVideo</code> (with ordinal 62) */
            TACTICAL_UAVVIDEO = 62,
            /** <code>UHFSATCOMDataLinkImplementation_UAV_</code> (with ordinal 63) */
            UHFSATCOMDATA_LINK_IMPLEMENTATION_UAV = 63,
            /** <code>TacticalCommonDataLink_TCDL_</code> (with ordinal 64) */
            TACTICAL_COMMON_DATA_LINK_TCDL = 64,
            /** <code>LowLevelAirPictureInterface_LLAPI_</code> (with ordinal 65) */
            LOW_LEVEL_AIR_PICTURE_INTERFACE_LLAPI = 65,
            /** <code>WeaponsDataLink_AGM-130_</code> (with ordinal 66) */
            WEAPONS_DATA_LINK_AGM_130 = 66,
            /** <code>AutomaticIdentificationSystem_AIS_</code> (with ordinal 67) */
            AUTOMATIC_IDENTIFICATION_SYSTEM_AIS = 67,
            /** <code>WeaponsDataLink_AIM-120_</code> (with ordinal 68) */
            WEAPONS_DATA_LINK_AIM_120 = 68,
            /** <code>GC3</code> (with ordinal 99) */
            GC3 = 99,
            /** <code>Link16StandardizedFormat_JTIDS_MIDS_TADILJ_</code> (with ordinal 100) */
            LINK16STANDARDIZED_FORMAT_JTIDS_MIDS_TADILJ = 100,
            /** <code>Link16EnhancedDataRate_EDRJTIDS_MIDS_TADIL-J_</code> (with ordinal 101) */
            LINK16ENHANCED_DATA_RATE_EDRJTIDS_MIDS_TADIL_J = 101,
            /** <code>JTIDS_MIDSNetDataLoad_TIMS_TOMS_</code> (with ordinal 102) */
            JTIDS_MIDSNET_DATA_LOAD_TIMS_TOMS = 102,
            /** <code>Link22</code> (with ordinal 103) */
            LINK22 = 103,
            /** <code>AFIWCIADSCommunicationsLinks</code> (with ordinal 104) */
            AFIWCIADSCOMMUNICATIONS_LINKS = 104,
            /** <code>F-22Intra-FlightDataLink_IFDL_</code> (with ordinal 105) */
            F_22INTRA_FLIGHT_DATA_LINK_IFDL = 105,
            /** <code>L-BandSATCOM</code> (with ordinal 106) */
            L_BAND_SATCOM = 106,
            /** <code>TSAFCommunicationsLink</code> (with ordinal 107) */
            TSAFCOMMUNICATIONS_LINK = 107,
            /** <code>EnhancedSINCGARS7_3</code> (with ordinal 108) */
            ENHANCED_SINCGARS7_3 = 108,
            /** <code>F-35MultifunctionAdvancedDataLink_MADL_</code> (with ordinal 109) */
            F_35MULTIFUNCTION_ADVANCED_DATA_LINK_MADL = 109,
            /** <code>CursorOnTarget</code> (with ordinal 110) */
            CURSOR_ON_TARGET = 110
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to TacticalDataLinkTypeEnum: static_cast<DevStudio::TacticalDataLinkTypeEnum::TacticalDataLinkTypeEnum>(i)
        */
        LIBAPI bool isValid(const TacticalDataLinkTypeEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::TacticalDataLinkTypeEnum::TacticalDataLinkTypeEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::TacticalDataLinkTypeEnum::TacticalDataLinkTypeEnum const &);
}


#endif
