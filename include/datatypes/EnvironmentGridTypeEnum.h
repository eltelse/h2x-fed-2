/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_ENVIRONMENTGRIDTYPEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_ENVIRONMENTGRIDTYPEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace EnvironmentGridTypeEnum {
        /**
        * Implementation of the <code>EnvironmentGridTypeEnum8</code> data type from the FOM.
        * <br>Description from the FOM: <i>Environment data grid type</i>
        */
        enum EnvironmentGridTypeEnum {
            /** <code>ConstantGrid</code> (with ordinal 0) */
            CONSTANT_GRID = 0,
            /** <code>UpdatedGrid</code> (with ordinal 1) */
            UPDATED_GRID = 1
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to EnvironmentGridTypeEnum: static_cast<DevStudio::EnvironmentGridTypeEnum::EnvironmentGridTypeEnum>(i)
        */
        LIBAPI bool isValid(const EnvironmentGridTypeEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::EnvironmentGridTypeEnum::EnvironmentGridTypeEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::EnvironmentGridTypeEnum::EnvironmentGridTypeEnum const &);
}


#endif
