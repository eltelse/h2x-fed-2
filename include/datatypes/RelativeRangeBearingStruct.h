/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_RELATIVERANGEBEARINGSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_RELATIVERANGEBEARINGSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>


namespace DevStudio {
   /**
   * Implementation of the <code>RelativeRangeBearingStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Relative position in polar coordinates.</i>
   */
   class RelativeRangeBearingStruct {

   public:
      /**
      * Description from the FOM: <i>The range from the reference location.</i>.
      * <br>Description of the data type from the FOM: <i>Length, based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: NA]</i>
      */
      float range;
      /**
      * Description from the FOM: <i>The bearing from the reference location.</i>.
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      */
      float bearing;

      LIBAPI RelativeRangeBearingStruct()
         :
         range(0),
         bearing(0)
      {}

      /**
      * Constructor for RelativeRangeBearingStruct
      *
      * @param range_ value to set as range.
      * <br>Description from the FOM: <i>The range from the reference location.</i>
      * <br>Description of the data type from the FOM: <i>Length, based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: NA]</i>
      * @param bearing_ value to set as bearing.
      * <br>Description from the FOM: <i>The bearing from the reference location.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      */
      LIBAPI RelativeRangeBearingStruct(
         float range_,
         float bearing_
         )
         :
         range(range_),
         bearing(bearing_)
      {}



      /**
      * Function to get range.
      * <br>Description from the FOM: <i>The range from the reference location.</i>
      * <br>Description of the data type from the FOM: <i>Length, based on SI base unit meter, unit symbol m. [unit: meter (m), resolution: NA, accuracy: NA]</i>
      *
      * @return range
      */
      LIBAPI float & getRange() {
         return range;
      }

      /**
      * Function to get bearing.
      * <br>Description from the FOM: <i>The bearing from the reference location.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @return bearing
      */
      LIBAPI float & getBearing() {
         return bearing;
      }

   };


   LIBAPI bool operator ==(const DevStudio::RelativeRangeBearingStruct& l, const DevStudio::RelativeRangeBearingStruct& r);
   LIBAPI bool operator !=(const DevStudio::RelativeRangeBearingStruct& l, const DevStudio::RelativeRangeBearingStruct& r);
   LIBAPI bool operator <(const DevStudio::RelativeRangeBearingStruct& l, const DevStudio::RelativeRangeBearingStruct& r);
   LIBAPI bool operator >(const DevStudio::RelativeRangeBearingStruct& l, const DevStudio::RelativeRangeBearingStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::RelativeRangeBearingStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::RelativeRangeBearingStruct const &);
}
#endif
