/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_WEAPONSTATEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_WEAPONSTATEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace WeaponStateEnum {
        /**
        * Implementation of the <code>WeaponStateEnum32</code> data type from the FOM.
        * <br>Description from the FOM: <i>Weapon position</i>
        */
        enum WeaponStateEnum {
            /** <code>NoWeapon</code> (with ordinal 0) */
            NO_WEAPON = 0,
            /** <code>Stowed</code> (with ordinal 1) */
            STOWED = 1,
            /** <code>Deployed</code> (with ordinal 2) */
            DEPLOYED = 2,
            /** <code>FiringPosition</code> (with ordinal 3) */
            FIRING_POSITION = 3
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to WeaponStateEnum: static_cast<DevStudio::WeaponStateEnum::WeaponStateEnum>(i)
        */
        LIBAPI bool isValid(const WeaponStateEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::WeaponStateEnum::WeaponStateEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::WeaponStateEnum::WeaponStateEnum const &);
}


#endif
