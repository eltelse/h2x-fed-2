/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_RECORDSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_RECORDSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/OctetArray.h>
#include <DevStudio/datatypes/OctetPadding32Array.h>
#include <vector>

namespace DevStudio {
   /**
   * Implementation of the <code>RecordStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Record data.</i>
   */
   class RecordStruct {

   public:
      /**
      * Description from the FOM: <i>Number of bits of data.</i>.
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      unsigned int numberOfBits;
      /**
      * Description from the FOM: <i>Data.</i>.
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of Octet elements, may also contain no elements.</i>
      */
      std::vector<char > numberOfBytesARecordData;
      /**
      * Description from the FOM: <i>Brings the record length to a 32-bit boundary</i>.
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of meaningless Octet elements, to align the subsequent data structure to the next 32 bit octet boundary value (OBV). The array is encoded without array length, containing zero to three elements.</i>
      */
      std::vector<char > paddingTo32;

      LIBAPI RecordStruct()
         :
         numberOfBits(0),
         numberOfBytesARecordData(0),
         paddingTo32(0)
      {}

      /**
      * Constructor for RecordStruct
      *
      * @param numberOfBits_ value to set as numberOfBits.
      * <br>Description from the FOM: <i>Number of bits of data.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param numberOfBytesARecordData_ value to set as numberOfBytesARecordData.
      * <br>Description from the FOM: <i>Data.</i>
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of Octet elements, may also contain no elements.</i>
      * @param paddingTo32_ value to set as paddingTo32.
      * <br>Description from the FOM: <i>Brings the record length to a 32-bit boundary</i>
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of meaningless Octet elements, to align the subsequent data structure to the next 32 bit octet boundary value (OBV). The array is encoded without array length, containing zero to three elements.</i>
      */
      LIBAPI RecordStruct(
         unsigned int numberOfBits_,
         std::vector<char > numberOfBytesARecordData_,
         std::vector<char > paddingTo32_
         )
         :
         numberOfBits(numberOfBits_),
         numberOfBytesARecordData(numberOfBytesARecordData_),
         paddingTo32(paddingTo32_)
      {}



      /**
      * Function to get numberOfBits.
      * <br>Description from the FOM: <i>Number of bits of data.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return numberOfBits
      */
      LIBAPI unsigned int & getNumberOfBits() {
         return numberOfBits;
      }

      /**
      * Function to get numberOfBytesARecordData.
      * <br>Description from the FOM: <i>Data.</i>
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of Octet elements, may also contain no elements.</i>
      *
      * @return numberOfBytesARecordData
      */
      LIBAPI std::vector<char > & getNumberOfBytesARecordData() {
         return numberOfBytesARecordData;
      }

      /**
      * Function to get paddingTo32.
      * <br>Description from the FOM: <i>Brings the record length to a 32-bit boundary</i>
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of meaningless Octet elements, to align the subsequent data structure to the next 32 bit octet boundary value (OBV). The array is encoded without array length, containing zero to three elements.</i>
      *
      * @return paddingTo32
      */
      LIBAPI std::vector<char > & getPaddingTo32() {
         return paddingTo32;
      }

   };


   LIBAPI bool operator ==(const DevStudio::RecordStruct& l, const DevStudio::RecordStruct& r);
   LIBAPI bool operator !=(const DevStudio::RecordStruct& l, const DevStudio::RecordStruct& r);
   LIBAPI bool operator <(const DevStudio::RecordStruct& l, const DevStudio::RecordStruct& r);
   LIBAPI bool operator >(const DevStudio::RecordStruct& l, const DevStudio::RecordStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::RecordStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::RecordStruct const &);
}
#endif
