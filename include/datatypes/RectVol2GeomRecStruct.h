/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_RECTVOL2GEOMRECSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_RECTVOL2GEOMRECSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/AngularVelocityVectorStruct.h>
#include <DevStudio/datatypes/DimensionRateStruct.h>
#include <DevStudio/datatypes/DimensionStruct.h>
#include <DevStudio/datatypes/OctetArray4.h>
#include <DevStudio/datatypes/OrientationStruct.h>
#include <DevStudio/datatypes/VelocityVectorStruct.h>
#include <DevStudio/datatypes/WorldLocationStruct.h>
#include <vector>

namespace DevStudio {
   /**
   * Implementation of the <code>RectVol2GeomRecStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Record specifying Rectangular Volume 2 geometry record</i>
   */
   class RectVol2GeomRecStruct {

   public:
      /**
      * Description from the FOM: <i>Corner location X, Y, Z</i>.
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      */
      WorldLocationStruct cornerLocation;
      /**
      * Description from the FOM: <i>Dimensions</i>.
      * <br>Description of the data type from the FOM: <i>Bounding box in X,Y,Z axis.</i>
      */
      DimensionStruct dimensions;
      /**
      * Description from the FOM: <i>Variation of dimensions</i>.
      * <br>Description of the data type from the FOM: <i>Record specifying length X, Y, Z rates</i>
      */
      DimensionRateStruct dimensionsRate;
      /**
      * Description from the FOM: <i>Orientation, specified by Euler angles</i>.
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      */
      OrientationStruct orientation;
      /**
      * Description from the FOM: <i>Velocity Vx, Vy, Vz</i>.
      * <br>Description of the data type from the FOM: <i>The rate at which the position is changing over time.</i>
      */
      VelocityVectorStruct velocity;
      /**
      * Description from the FOM: <i>Angular velocity Vx, Vy, Vz</i>.
      * <br>Description of the data type from the FOM: <i>The rate at which the orientation is changing over time, in body coordinates.</i>
      */
      AngularVelocityVectorStruct angularVelocity;
      /**
      * Description from the FOM: <i>Padding field</i>.
      * <br>Description of the data type from the FOM: <i>Generic array of four Octet elements.</i>
      */
      std::vector</* 4 */ char > padding;

      LIBAPI RectVol2GeomRecStruct()
         :
         cornerLocation(WorldLocationStruct()),
         dimensions(DimensionStruct()),
         dimensionsRate(DimensionRateStruct()),
         orientation(OrientationStruct()),
         velocity(VelocityVectorStruct()),
         angularVelocity(AngularVelocityVectorStruct()),
         padding(0)
      {}

      /**
      * Constructor for RectVol2GeomRecStruct
      *
      * @param cornerLocation_ value to set as cornerLocation.
      * <br>Description from the FOM: <i>Corner location X, Y, Z</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      * @param dimensions_ value to set as dimensions.
      * <br>Description from the FOM: <i>Dimensions</i>
      * <br>Description of the data type from the FOM: <i>Bounding box in X,Y,Z axis.</i>
      * @param dimensionsRate_ value to set as dimensionsRate.
      * <br>Description from the FOM: <i>Variation of dimensions</i>
      * <br>Description of the data type from the FOM: <i>Record specifying length X, Y, Z rates</i>
      * @param orientation_ value to set as orientation.
      * <br>Description from the FOM: <i>Orientation, specified by Euler angles</i>
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      * @param velocity_ value to set as velocity.
      * <br>Description from the FOM: <i>Velocity Vx, Vy, Vz</i>
      * <br>Description of the data type from the FOM: <i>The rate at which the position is changing over time.</i>
      * @param angularVelocity_ value to set as angularVelocity.
      * <br>Description from the FOM: <i>Angular velocity Vx, Vy, Vz</i>
      * <br>Description of the data type from the FOM: <i>The rate at which the orientation is changing over time, in body coordinates.</i>
      * @param padding_ value to set as padding.
      * <br>Description from the FOM: <i>Padding field</i>
      * <br>Description of the data type from the FOM: <i>Generic array of four Octet elements.</i>
      */
      LIBAPI RectVol2GeomRecStruct(
         WorldLocationStruct cornerLocation_,
         DimensionStruct dimensions_,
         DimensionRateStruct dimensionsRate_,
         OrientationStruct orientation_,
         VelocityVectorStruct velocity_,
         AngularVelocityVectorStruct angularVelocity_,
         std::vector</* 4 */ char > padding_
         )
         :
         cornerLocation(cornerLocation_),
         dimensions(dimensions_),
         dimensionsRate(dimensionsRate_),
         orientation(orientation_),
         velocity(velocity_),
         angularVelocity(angularVelocity_),
         padding(padding_)
      {}



      /**
      * Function to get cornerLocation.
      * <br>Description from the FOM: <i>Corner location X, Y, Z</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @return cornerLocation
      */
      LIBAPI DevStudio::WorldLocationStruct & getCornerLocation() {
         return cornerLocation;
      }

      /**
      * Function to get dimensions.
      * <br>Description from the FOM: <i>Dimensions</i>
      * <br>Description of the data type from the FOM: <i>Bounding box in X,Y,Z axis.</i>
      *
      * @return dimensions
      */
      LIBAPI DevStudio::DimensionStruct & getDimensions() {
         return dimensions;
      }

      /**
      * Function to get dimensionsRate.
      * <br>Description from the FOM: <i>Variation of dimensions</i>
      * <br>Description of the data type from the FOM: <i>Record specifying length X, Y, Z rates</i>
      *
      * @return dimensionsRate
      */
      LIBAPI DevStudio::DimensionRateStruct & getDimensionsRate() {
         return dimensionsRate;
      }

      /**
      * Function to get orientation.
      * <br>Description from the FOM: <i>Orientation, specified by Euler angles</i>
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @return orientation
      */
      LIBAPI DevStudio::OrientationStruct & getOrientation() {
         return orientation;
      }

      /**
      * Function to get velocity.
      * <br>Description from the FOM: <i>Velocity Vx, Vy, Vz</i>
      * <br>Description of the data type from the FOM: <i>The rate at which the position is changing over time.</i>
      *
      * @return velocity
      */
      LIBAPI DevStudio::VelocityVectorStruct & getVelocity() {
         return velocity;
      }

      /**
      * Function to get angularVelocity.
      * <br>Description from the FOM: <i>Angular velocity Vx, Vy, Vz</i>
      * <br>Description of the data type from the FOM: <i>The rate at which the orientation is changing over time, in body coordinates.</i>
      *
      * @return angularVelocity
      */
      LIBAPI DevStudio::AngularVelocityVectorStruct & getAngularVelocity() {
         return angularVelocity;
      }

      /**
      * Function to get padding.
      * <br>Description from the FOM: <i>Padding field</i>
      * <br>Description of the data type from the FOM: <i>Generic array of four Octet elements.</i>
      *
      * @return padding
      */
      LIBAPI std::vector</* 4 */ char > & getPadding() {
         return padding;
      }

   };


   LIBAPI bool operator ==(const DevStudio::RectVol2GeomRecStruct& l, const DevStudio::RectVol2GeomRecStruct& r);
   LIBAPI bool operator !=(const DevStudio::RectVol2GeomRecStruct& l, const DevStudio::RectVol2GeomRecStruct& r);
   LIBAPI bool operator <(const DevStudio::RectVol2GeomRecStruct& l, const DevStudio::RectVol2GeomRecStruct& r);
   LIBAPI bool operator >(const DevStudio::RectVol2GeomRecStruct& l, const DevStudio::RectVol2GeomRecStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::RectVol2GeomRecStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::RectVol2GeomRecStruct const &);
}
#endif
