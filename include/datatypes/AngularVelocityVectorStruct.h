/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_ANGULARVELOCITYVECTORSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_ANGULARVELOCITYVECTORSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <RtiDriver/RprUtility/RprUtility.h>


namespace DevStudio {
   /**
   * Implementation of the <code>AngularVelocityVectorStruct</code> data type from the FOM.
   * This datatype can be used with the RprUtility package. Please see the Overview document
   * located in the project root directory for more information.
   * <br>Description from the FOM: <i>The rate at which the orientation is changing over time, in body coordinates.</i>
   */
   class AngularVelocityVectorStruct {

   public:
      /**
      * Description from the FOM: <i>Acceleration component about the X axis.</i>.
      * <br>Description of the data type from the FOM: <i>Angular velocity vector composed of SI base units. Based on the Angular Velocity Vector record as specified in IEEE 1278.1-1995 section 5.2.2. [unit: radian per second (rad/s), resolution: NA, accuracy: perfect]</i>
      */
      float xAngularVelocity;
      /**
      * Description from the FOM: <i>Acceleration component about the Y axis.</i>.
      * <br>Description of the data type from the FOM: <i>Angular velocity vector composed of SI base units. Based on the Angular Velocity Vector record as specified in IEEE 1278.1-1995 section 5.2.2. [unit: radian per second (rad/s), resolution: NA, accuracy: perfect]</i>
      */
      float yAngularVelocity;
      /**
      * Description from the FOM: <i>Acceleration component about the Z axis.</i>.
      * <br>Description of the data type from the FOM: <i>Angular velocity vector composed of SI base units. Based on the Angular Velocity Vector record as specified in IEEE 1278.1-1995 section 5.2.2. [unit: radian per second (rad/s), resolution: NA, accuracy: perfect]</i>
      */
      float zAngularVelocity;

      LIBAPI AngularVelocityVectorStruct()
         :
         xAngularVelocity(0),
         yAngularVelocity(0),
         zAngularVelocity(0)
      {}

      /**
      * Constructor for AngularVelocityVectorStruct
      *
      * @param xAngularVelocity_ value to set as xAngularVelocity.
      * <br>Description from the FOM: <i>Acceleration component about the X axis.</i>
      * <br>Description of the data type from the FOM: <i>Angular velocity vector composed of SI base units. Based on the Angular Velocity Vector record as specified in IEEE 1278.1-1995 section 5.2.2. [unit: radian per second (rad/s), resolution: NA, accuracy: perfect]</i>
      * @param yAngularVelocity_ value to set as yAngularVelocity.
      * <br>Description from the FOM: <i>Acceleration component about the Y axis.</i>
      * <br>Description of the data type from the FOM: <i>Angular velocity vector composed of SI base units. Based on the Angular Velocity Vector record as specified in IEEE 1278.1-1995 section 5.2.2. [unit: radian per second (rad/s), resolution: NA, accuracy: perfect]</i>
      * @param zAngularVelocity_ value to set as zAngularVelocity.
      * <br>Description from the FOM: <i>Acceleration component about the Z axis.</i>
      * <br>Description of the data type from the FOM: <i>Angular velocity vector composed of SI base units. Based on the Angular Velocity Vector record as specified in IEEE 1278.1-1995 section 5.2.2. [unit: radian per second (rad/s), resolution: NA, accuracy: perfect]</i>
      */
      LIBAPI AngularVelocityVectorStruct(
         float xAngularVelocity_,
         float yAngularVelocity_,
         float zAngularVelocity_
         )
         :
         xAngularVelocity(xAngularVelocity_),
         yAngularVelocity(yAngularVelocity_),
         zAngularVelocity(zAngularVelocity_)
      {}

      /**
      * Convert to RprUtility datatype
      *
      * @param angularVelocity AngularVelocityVectorStruct to convert
      *
      * @return angularVelocity converted to RprUtility::AngularVelocityVector
      */
      LIBAPI static RprUtility::AngularVelocityVector convert(const AngularVelocityVectorStruct angularVelocity);
      
      /**
      * Convert from RprUtility datatype
      *
      * @param angularVelocity RprUtility::AngularVelocityVector to convert to AngularVelocityVectorStruct
      *
      * @return angularVelocity converted to AngularVelocityVectorStruct
      */
      LIBAPI static AngularVelocityVectorStruct convert(const RprUtility::AngularVelocityVector angularVelocity);


      /**
      * Function to get xAngularVelocity.
      * <br>Description from the FOM: <i>Acceleration component about the X axis.</i>
      * <br>Description of the data type from the FOM: <i>Angular velocity vector composed of SI base units. Based on the Angular Velocity Vector record as specified in IEEE 1278.1-1995 section 5.2.2. [unit: radian per second (rad/s), resolution: NA, accuracy: perfect]</i>
      *
      * @return xAngularVelocity
      */
      LIBAPI float & getXAngularVelocity() {
         return xAngularVelocity;
      }

      /**
      * Function to get yAngularVelocity.
      * <br>Description from the FOM: <i>Acceleration component about the Y axis.</i>
      * <br>Description of the data type from the FOM: <i>Angular velocity vector composed of SI base units. Based on the Angular Velocity Vector record as specified in IEEE 1278.1-1995 section 5.2.2. [unit: radian per second (rad/s), resolution: NA, accuracy: perfect]</i>
      *
      * @return yAngularVelocity
      */
      LIBAPI float & getYAngularVelocity() {
         return yAngularVelocity;
      }

      /**
      * Function to get zAngularVelocity.
      * <br>Description from the FOM: <i>Acceleration component about the Z axis.</i>
      * <br>Description of the data type from the FOM: <i>Angular velocity vector composed of SI base units. Based on the Angular Velocity Vector record as specified in IEEE 1278.1-1995 section 5.2.2. [unit: radian per second (rad/s), resolution: NA, accuracy: perfect]</i>
      *
      * @return zAngularVelocity
      */
      LIBAPI float & getZAngularVelocity() {
         return zAngularVelocity;
      }

   };


   LIBAPI bool operator ==(const DevStudio::AngularVelocityVectorStruct& l, const DevStudio::AngularVelocityVectorStruct& r);
   LIBAPI bool operator !=(const DevStudio::AngularVelocityVectorStruct& l, const DevStudio::AngularVelocityVectorStruct& r);
   LIBAPI bool operator <(const DevStudio::AngularVelocityVectorStruct& l, const DevStudio::AngularVelocityVectorStruct& r);
   LIBAPI bool operator >(const DevStudio::AngularVelocityVectorStruct& l, const DevStudio::AngularVelocityVectorStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::AngularVelocityVectorStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::AngularVelocityVectorStruct const &);
}
#endif
