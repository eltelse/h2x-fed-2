/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_EXHAUSTSMOKESTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_EXHAUSTSMOKESTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/ChemicalContentEnum.h>
#include <DevStudio/datatypes/LinearSegmentStruct.h>

namespace DevStudio {
   /**
   * Implementation of the <code>ExhaustSmokeStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Record specifying the characteristics of a linear object smoke segment</i>
   */
   class ExhaustSmokeStruct {

   public:
      /**
      * Description from the FOM: <i>Specifies the linear object smoke segment characteristics</i>.
      * <br>Description of the data type from the FOM: <i>Specifies linear object segment characteristics.</i>
      */
      LinearSegmentStruct segmentParameters;
      /**
      * Description from the FOM: <i>Specifies the opacity of the linear object smoke segment</i>.
      * <br>Description of the data type from the FOM: <i>Percentage [unit: percent (%), resolution: 1, accuracy: perfect]</i>
      */
      unsigned int percentOpacity;
      /**
      * Description from the FOM: <i>Specifies whether the linear object smoke segment is attached to the vehicle</i>.
      * <br>Description of the data type from the FOM: <i></i>
      */
      bool attached;
      /**
      * Description from the FOM: <i>Specifies the chemical content of the linear object smoke segment</i>.
      * <br>Description of the data type from the FOM: <i>Smoke chemical content</i>
      */
      ChemicalContentEnum::ChemicalContentEnum chemicalContent;

      LIBAPI ExhaustSmokeStruct()
         :
         segmentParameters(LinearSegmentStruct()),
         percentOpacity(0),
         attached(0),
         chemicalContent(ChemicalContentEnum::ChemicalContentEnum())
      {}

      /**
      * Constructor for ExhaustSmokeStruct
      *
      * @param segmentParameters_ value to set as segmentParameters.
      * <br>Description from the FOM: <i>Specifies the linear object smoke segment characteristics</i>
      * <br>Description of the data type from the FOM: <i>Specifies linear object segment characteristics.</i>
      * @param percentOpacity_ value to set as percentOpacity.
      * <br>Description from the FOM: <i>Specifies the opacity of the linear object smoke segment</i>
      * <br>Description of the data type from the FOM: <i>Percentage [unit: percent (%), resolution: 1, accuracy: perfect]</i>
      * @param attached_ value to set as attached.
      * <br>Description from the FOM: <i>Specifies whether the linear object smoke segment is attached to the vehicle</i>
      * <br>Description of the data type from the FOM: <i></i>
      * @param chemicalContent_ value to set as chemicalContent.
      * <br>Description from the FOM: <i>Specifies the chemical content of the linear object smoke segment</i>
      * <br>Description of the data type from the FOM: <i>Smoke chemical content</i>
      */
      LIBAPI ExhaustSmokeStruct(
         LinearSegmentStruct segmentParameters_,
         unsigned int percentOpacity_,
         bool attached_,
         ChemicalContentEnum::ChemicalContentEnum chemicalContent_
         )
         :
         segmentParameters(segmentParameters_),
         percentOpacity(percentOpacity_),
         attached(attached_),
         chemicalContent(chemicalContent_)
      {}



      /**
      * Function to get segmentParameters.
      * <br>Description from the FOM: <i>Specifies the linear object smoke segment characteristics</i>
      * <br>Description of the data type from the FOM: <i>Specifies linear object segment characteristics.</i>
      *
      * @return segmentParameters
      */
      LIBAPI DevStudio::LinearSegmentStruct & getSegmentParameters() {
         return segmentParameters;
      }

      /**
      * Function to get percentOpacity.
      * <br>Description from the FOM: <i>Specifies the opacity of the linear object smoke segment</i>
      * <br>Description of the data type from the FOM: <i>Percentage [unit: percent (%), resolution: 1, accuracy: perfect]</i>
      *
      * @return percentOpacity
      */
      LIBAPI unsigned int & getPercentOpacity() {
         return percentOpacity;
      }

      /**
      * Function to get attached.
      * <br>Description from the FOM: <i>Specifies whether the linear object smoke segment is attached to the vehicle</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return attached
      */
      LIBAPI bool & getAttached() {
         return attached;
      }

      /**
      * Function to get chemicalContent.
      * <br>Description from the FOM: <i>Specifies the chemical content of the linear object smoke segment</i>
      * <br>Description of the data type from the FOM: <i>Smoke chemical content</i>
      *
      * @return chemicalContent
      */
      LIBAPI DevStudio::ChemicalContentEnum::ChemicalContentEnum & getChemicalContent() {
         return chemicalContent;
      }

   };


   LIBAPI bool operator ==(const DevStudio::ExhaustSmokeStruct& l, const DevStudio::ExhaustSmokeStruct& r);
   LIBAPI bool operator !=(const DevStudio::ExhaustSmokeStruct& l, const DevStudio::ExhaustSmokeStruct& r);
   LIBAPI bool operator <(const DevStudio::ExhaustSmokeStruct& l, const DevStudio::ExhaustSmokeStruct& r);
   LIBAPI bool operator >(const DevStudio::ExhaustSmokeStruct& l, const DevStudio::ExhaustSmokeStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::ExhaustSmokeStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::ExhaustSmokeStruct const &);
}
#endif
