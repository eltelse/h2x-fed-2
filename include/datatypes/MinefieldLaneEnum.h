/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_MINEFIELDLANEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_MINEFIELDLANEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace MinefieldLaneEnum {
        /**
        * Implementation of the <code>MinefieldLaneEnum8</code> data type from the FOM.
        * <br>Description from the FOM: <i>Minefield lane status</i>
        */
        enum MinefieldLaneEnum {
            /** <code>MinefieldHasActiveLane</code> (with ordinal 0) */
            MINEFIELD_HAS_ACTIVE_LANE = 0,
            /** <code>MinefieldHasAnInactiveLane</code> (with ordinal 1) */
            MINEFIELD_HAS_AN_INACTIVE_LANE = 1
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to MinefieldLaneEnum: static_cast<DevStudio::MinefieldLaneEnum::MinefieldLaneEnum>(i)
        */
        LIBAPI bool isValid(const MinefieldLaneEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::MinefieldLaneEnum::MinefieldLaneEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::MinefieldLaneEnum::MinefieldLaneEnum const &);
}


#endif
