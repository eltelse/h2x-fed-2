/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_GRIDDATASTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_GRIDDATASTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/EnvironmentDataSampleTypeEnum.h>
#include <DevStudio/datatypes/GridDataRepresentationVariantStruct.h>

namespace DevStudio {
   /**
   * Implementation of the <code>GridDataStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Record specifying grid data representation</i>
   */
   class GridDataStruct {

   public:
      /**
      * Description from the FOM: <i>Specifies the environmental data sample</i>.
      * <br>Description of the data type from the FOM: <i>Environment data sample type</i>
      */
      EnvironmentDataSampleTypeEnum::EnvironmentDataSampleTypeEnum sampleType;
      /**
      * Description from the FOM: <i>Specifies grid data representation alternatives</i>.
      * <br>Description of the data type from the FOM: <i>Record specifying either type 0 or type 1 or type 2 data representation</i>
      */
      GridDataRepresentationVariantStruct dataRepresentationAAlternatives;

      LIBAPI GridDataStruct()
         :
         sampleType(EnvironmentDataSampleTypeEnum::EnvironmentDataSampleTypeEnum()),
         dataRepresentationAAlternatives(GridDataRepresentationVariantStruct())
      {}

      /**
      * Constructor for GridDataStruct
      *
      * @param sampleType_ value to set as sampleType.
      * <br>Description from the FOM: <i>Specifies the environmental data sample</i>
      * <br>Description of the data type from the FOM: <i>Environment data sample type</i>
      * @param dataRepresentationAAlternatives_ value to set as dataRepresentationAAlternatives.
      * <br>Description from the FOM: <i>Specifies grid data representation alternatives</i>
      * <br>Description of the data type from the FOM: <i>Record specifying either type 0 or type 1 or type 2 data representation</i>
      */
      LIBAPI GridDataStruct(
         EnvironmentDataSampleTypeEnum::EnvironmentDataSampleTypeEnum sampleType_,
         GridDataRepresentationVariantStruct dataRepresentationAAlternatives_
         )
         :
         sampleType(sampleType_),
         dataRepresentationAAlternatives(dataRepresentationAAlternatives_)
      {}



      /**
      * Function to get sampleType.
      * <br>Description from the FOM: <i>Specifies the environmental data sample</i>
      * <br>Description of the data type from the FOM: <i>Environment data sample type</i>
      *
      * @return sampleType
      */
      LIBAPI DevStudio::EnvironmentDataSampleTypeEnum::EnvironmentDataSampleTypeEnum & getSampleType() {
         return sampleType;
      }

      /**
      * Function to get dataRepresentationAAlternatives.
      * <br>Description from the FOM: <i>Specifies grid data representation alternatives</i>
      * <br>Description of the data type from the FOM: <i>Record specifying either type 0 or type 1 or type 2 data representation</i>
      *
      * @return dataRepresentationAAlternatives
      */
      LIBAPI DevStudio::GridDataRepresentationVariantStruct & getDataRepresentationAAlternatives() {
         return dataRepresentationAAlternatives;
      }

   };


   LIBAPI bool operator ==(const DevStudio::GridDataStruct& l, const DevStudio::GridDataStruct& r);
   LIBAPI bool operator !=(const DevStudio::GridDataStruct& l, const DevStudio::GridDataStruct& r);
   LIBAPI bool operator <(const DevStudio::GridDataStruct& l, const DevStudio::GridDataStruct& r);
   LIBAPI bool operator >(const DevStudio::GridDataStruct& l, const DevStudio::GridDataStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::GridDataStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::GridDataStruct const &);
}
#endif
