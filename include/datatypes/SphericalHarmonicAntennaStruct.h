/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_SPHERICALHARMONICANTENNASTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_SPHERICALHARMONICANTENNASTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/CoefficientsLengthlessArray1Plus.h>
#include <DevStudio/datatypes/OctetArray3.h>
#include <DevStudio/datatypes/ReferenceSystemEnum.h>
#include <vector>

namespace DevStudio {
   /**
   * Implementation of the <code>SphericalHarmonicAntennaStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Specifies the direction and radiation pattern from a radio transmitter's antenna.</i>
   */
   class SphericalHarmonicAntennaStruct {

   public:
      /**
      * Description from the FOM: <i>The highest order of the expansion in spherical harmonics, counting from zero.</i>.
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      unsigned int order;
      /**
      * Description from the FOM: <i>Represents the power distribution from the antenna as the coefficients of a spherical harmonic expansion to the order given in the Order field. The length of the array is N^2+2N+1 (N being the Order).</i>.
      * <br>Description of the data type from the FOM: <i>Represents the power distribution from the antenna as the coefficients of a spherical harmonic expansion. The highest order of the expansion can be determined by the number of coefficients in the array.</i>
      */
      std::vector</* not empty */ float > coefficients;
      /**
      * Description from the FOM: <i>This field shall specify the reference coordinate system with respect to which beam direction is specified.</i>.
      * <br>Description of the data type from the FOM: <i>The reference coordinate system used</i>
      */
      ReferenceSystemEnum::ReferenceSystemEnum referenceSystem;
      /**
      * Description from the FOM: <i>Padding to 32 bits</i>.
      * <br>Description of the data type from the FOM: <i>Generic array of three Octet elements.</i>
      */
      std::vector</* 3 */ char > padding;

      LIBAPI SphericalHarmonicAntennaStruct()
         :
         order(0),
         coefficients(0),
         referenceSystem(ReferenceSystemEnum::ReferenceSystemEnum()),
         padding(0)
      {}

      /**
      * Constructor for SphericalHarmonicAntennaStruct
      *
      * @param order_ value to set as order.
      * <br>Description from the FOM: <i>The highest order of the expansion in spherical harmonics, counting from zero.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param coefficients_ value to set as coefficients.
      * <br>Description from the FOM: <i>Represents the power distribution from the antenna as the coefficients of a spherical harmonic expansion to the order given in the Order field. The length of the array is N^2+2N+1 (N being the Order).</i>
      * <br>Description of the data type from the FOM: <i>Represents the power distribution from the antenna as the coefficients of a spherical harmonic expansion. The highest order of the expansion can be determined by the number of coefficients in the array.</i>
      * @param referenceSystem_ value to set as referenceSystem.
      * <br>Description from the FOM: <i>This field shall specify the reference coordinate system with respect to which beam direction is specified.</i>
      * <br>Description of the data type from the FOM: <i>The reference coordinate system used</i>
      * @param padding_ value to set as padding.
      * <br>Description from the FOM: <i>Padding to 32 bits</i>
      * <br>Description of the data type from the FOM: <i>Generic array of three Octet elements.</i>
      */
      LIBAPI SphericalHarmonicAntennaStruct(
         unsigned int order_,
         std::vector</* not empty */ float > coefficients_,
         ReferenceSystemEnum::ReferenceSystemEnum referenceSystem_,
         std::vector</* 3 */ char > padding_
         )
         :
         order(order_),
         coefficients(coefficients_),
         referenceSystem(referenceSystem_),
         padding(padding_)
      {}



      /**
      * Function to get order.
      * <br>Description from the FOM: <i>The highest order of the expansion in spherical harmonics, counting from zero.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return order
      */
      LIBAPI unsigned int & getOrder() {
         return order;
      }

      /**
      * Function to get coefficients.
      * <br>Description from the FOM: <i>Represents the power distribution from the antenna as the coefficients of a spherical harmonic expansion to the order given in the Order field. The length of the array is N^2+2N+1 (N being the Order).</i>
      * <br>Description of the data type from the FOM: <i>Represents the power distribution from the antenna as the coefficients of a spherical harmonic expansion. The highest order of the expansion can be determined by the number of coefficients in the array.</i>
      *
      * @return coefficients
      */
      LIBAPI std::vector</* not empty */ float > & getCoefficients() {
         return coefficients;
      }

      /**
      * Function to get referenceSystem.
      * <br>Description from the FOM: <i>This field shall specify the reference coordinate system with respect to which beam direction is specified.</i>
      * <br>Description of the data type from the FOM: <i>The reference coordinate system used</i>
      *
      * @return referenceSystem
      */
      LIBAPI DevStudio::ReferenceSystemEnum::ReferenceSystemEnum & getReferenceSystem() {
         return referenceSystem;
      }

      /**
      * Function to get padding.
      * <br>Description from the FOM: <i>Padding to 32 bits</i>
      * <br>Description of the data type from the FOM: <i>Generic array of three Octet elements.</i>
      *
      * @return padding
      */
      LIBAPI std::vector</* 3 */ char > & getPadding() {
         return padding;
      }

   };


   LIBAPI bool operator ==(const DevStudio::SphericalHarmonicAntennaStruct& l, const DevStudio::SphericalHarmonicAntennaStruct& r);
   LIBAPI bool operator !=(const DevStudio::SphericalHarmonicAntennaStruct& l, const DevStudio::SphericalHarmonicAntennaStruct& r);
   LIBAPI bool operator <(const DevStudio::SphericalHarmonicAntennaStruct& l, const DevStudio::SphericalHarmonicAntennaStruct& r);
   LIBAPI bool operator >(const DevStudio::SphericalHarmonicAntennaStruct& l, const DevStudio::SphericalHarmonicAntennaStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::SphericalHarmonicAntennaStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::SphericalHarmonicAntennaStruct const &);
}
#endif
