/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_ARTICULATEDPARTSTYPEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_ARTICULATEDPARTSTYPEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace ArticulatedPartsTypeEnum {
        /**
        * Implementation of the <code>ArticulatedPartsTypeEnum32</code> data type from the FOM.
        * <br>Description from the FOM: <i>Articulated part type class</i>
        */
        enum ArticulatedPartsTypeEnum {
            /** <code>Other</code> (with ordinal 0) */
            OTHER = 0,
            /** <code>Rudder</code> (with ordinal 1024) */
            RUDDER = 1024,
            /** <code>LeftFlap</code> (with ordinal 1056) */
            LEFT_FLAP = 1056,
            /** <code>RightFlap</code> (with ordinal 1088) */
            RIGHT_FLAP = 1088,
            /** <code>LeftAileron</code> (with ordinal 1120) */
            LEFT_AILERON = 1120,
            /** <code>RightAileron</code> (with ordinal 1152) */
            RIGHT_AILERON = 1152,
            /** <code>HelicopterMainRotor</code> (with ordinal 1184) */
            HELICOPTER_MAIN_ROTOR = 1184,
            /** <code>HelicopterTailRotor</code> (with ordinal 1216) */
            HELICOPTER_TAIL_ROTOR = 1216,
            /** <code>OtherAircraftControlSurfaces</code> (with ordinal 1248) */
            OTHER_AIRCRAFT_CONTROL_SURFACES = 1248,
            /** <code>PropellerNumber1</code> (with ordinal 1280) */
            PROPELLER_NUMBER1 = 1280,
            /** <code>PropellerNumber2</code> (with ordinal 1312) */
            PROPELLER_NUMBER2 = 1312,
            /** <code>PropellerNumber3</code> (with ordinal 1344) */
            PROPELLER_NUMBER3 = 1344,
            /** <code>PropellerNumber4</code> (with ordinal 1376) */
            PROPELLER_NUMBER4 = 1376,
            /** <code>LeftStabilator_StabilatorNumber1_</code> (with ordinal 1408) */
            LEFT_STABILATOR_STABILATOR_NUMBER1 = 1408,
            /** <code>RightStabilator_StabilatorNumber2_</code> (with ordinal 1440) */
            RIGHT_STABILATOR_STABILATOR_NUMBER2 = 1440,
            /** <code>LeftRuddervator_RuddervatorNumber1_</code> (with ordinal 1472) */
            LEFT_RUDDERVATOR_RUDDERVATOR_NUMBER1 = 1472,
            /** <code>RightRuddervator_RuddervatorNumber2_</code> (with ordinal 1504) */
            RIGHT_RUDDERVATOR_RUDDERVATOR_NUMBER2 = 1504,
            /** <code>LeftLeadingEdgeFlap_Slat</code> (with ordinal 1536) */
            LEFT_LEADING_EDGE_FLAP_SLAT = 1536,
            /** <code>RightLeadingEdgeFlap_Slat</code> (with ordinal 1568) */
            RIGHT_LEADING_EDGE_FLAP_SLAT = 1568,
            /** <code>Periscope</code> (with ordinal 2048) */
            PERISCOPE = 2048,
            /** <code>GenericAntenna</code> (with ordinal 2080) */
            GENERIC_ANTENNA = 2080,
            /** <code>Snorkel</code> (with ordinal 2112) */
            SNORKEL = 2112,
            /** <code>OtherExtendableParts</code> (with ordinal 2144) */
            OTHER_EXTENDABLE_PARTS = 2144,
            /** <code>LandingGear</code> (with ordinal 3072) */
            LANDING_GEAR = 3072,
            /** <code>TailHook</code> (with ordinal 3104) */
            TAIL_HOOK = 3104,
            /** <code>SpeedBrake</code> (with ordinal 3136) */
            SPEED_BRAKE = 3136,
            /** <code>LeftWeaponBayDoors</code> (with ordinal 3168) */
            LEFT_WEAPON_BAY_DOORS = 3168,
            /** <code>RightWeaponBayDoors</code> (with ordinal 3200) */
            RIGHT_WEAPON_BAY_DOORS = 3200,
            /** <code>TankOrAPChatch</code> (with ordinal 3232) */
            TANK_OR_APCHATCH = 3232,
            /** <code>Wingsweep</code> (with ordinal 3264) */
            WINGSWEEP = 3264,
            /** <code>BridgeLauncher</code> (with ordinal 3296) */
            BRIDGE_LAUNCHER = 3296,
            /** <code>BridgeSection1</code> (with ordinal 3328) */
            BRIDGE_SECTION1 = 3328,
            /** <code>BridgeSection2</code> (with ordinal 3360) */
            BRIDGE_SECTION2 = 3360,
            /** <code>BridgeSection3</code> (with ordinal 3392) */
            BRIDGE_SECTION3 = 3392,
            /** <code>PrimaryBlade1</code> (with ordinal 3424) */
            PRIMARY_BLADE1 = 3424,
            /** <code>PrimaryBlade2</code> (with ordinal 3456) */
            PRIMARY_BLADE2 = 3456,
            /** <code>PrimaryBoom</code> (with ordinal 3488) */
            PRIMARY_BOOM = 3488,
            /** <code>PrimaryLauncherArm</code> (with ordinal 3520) */
            PRIMARY_LAUNCHER_ARM = 3520,
            /** <code>OtherFixedPositionParts</code> (with ordinal 3552) */
            OTHER_FIXED_POSITION_PARTS = 3552,
            /** <code>LandingGear-Nose</code> (with ordinal 3584) */
            LANDING_GEAR_NOSE = 3584,
            /** <code>LandingGear-LeftMain</code> (with ordinal 3616) */
            LANDING_GEAR_LEFT_MAIN = 3616,
            /** <code>LandingGear-RightMain</code> (with ordinal 3648) */
            LANDING_GEAR_RIGHT_MAIN = 3648,
            /** <code>LeftSide_SecondaryWeaponBayDoors</code> (with ordinal 3680) */
            LEFT_SIDE_SECONDARY_WEAPON_BAY_DOORS = 3680,
            /** <code>RightSide_SecondaryWeaponBayDoors</code> (with ordinal 3712) */
            RIGHT_SIDE_SECONDARY_WEAPON_BAY_DOORS = 3712,
            /** <code>PrimaryTurretNumber1</code> (with ordinal 4096) */
            PRIMARY_TURRET_NUMBER1 = 4096,
            /** <code>PrimaryTurretNumber2</code> (with ordinal 4128) */
            PRIMARY_TURRET_NUMBER2 = 4128,
            /** <code>PrimaryTurretNumber3</code> (with ordinal 4160) */
            PRIMARY_TURRET_NUMBER3 = 4160,
            /** <code>PrimaryTurretNumber4</code> (with ordinal 4192) */
            PRIMARY_TURRET_NUMBER4 = 4192,
            /** <code>PrimaryTurretNumber5</code> (with ordinal 4224) */
            PRIMARY_TURRET_NUMBER5 = 4224,
            /** <code>PrimaryTurretNumber6</code> (with ordinal 4256) */
            PRIMARY_TURRET_NUMBER6 = 4256,
            /** <code>PrimaryTurretNumber7</code> (with ordinal 4288) */
            PRIMARY_TURRET_NUMBER7 = 4288,
            /** <code>PrimaryTurretNumber8</code> (with ordinal 4320) */
            PRIMARY_TURRET_NUMBER8 = 4320,
            /** <code>PrimaryTurretNumber9</code> (with ordinal 4352) */
            PRIMARY_TURRET_NUMBER9 = 4352,
            /** <code>PrimaryTurretNumber10</code> (with ordinal 4384) */
            PRIMARY_TURRET_NUMBER10 = 4384,
            /** <code>PrimaryGunNumber1</code> (with ordinal 4416) */
            PRIMARY_GUN_NUMBER1 = 4416,
            /** <code>PrimaryGunNumber2</code> (with ordinal 4448) */
            PRIMARY_GUN_NUMBER2 = 4448,
            /** <code>PrimaryGunNumber3</code> (with ordinal 4480) */
            PRIMARY_GUN_NUMBER3 = 4480,
            /** <code>PrimaryGunNumber4</code> (with ordinal 4512) */
            PRIMARY_GUN_NUMBER4 = 4512,
            /** <code>PrimaryGunNumber5</code> (with ordinal 4544) */
            PRIMARY_GUN_NUMBER5 = 4544,
            /** <code>PrimaryGunNumber6</code> (with ordinal 4576) */
            PRIMARY_GUN_NUMBER6 = 4576,
            /** <code>PrimaryGunNumber7</code> (with ordinal 4608) */
            PRIMARY_GUN_NUMBER7 = 4608,
            /** <code>PrimaryGunNumber8</code> (with ordinal 4640) */
            PRIMARY_GUN_NUMBER8 = 4640,
            /** <code>PrimaryGunNumber9</code> (with ordinal 4672) */
            PRIMARY_GUN_NUMBER9 = 4672,
            /** <code>PrimaryGunNumber10</code> (with ordinal 4704) */
            PRIMARY_GUN_NUMBER10 = 4704,
            /** <code>PrimaryLauncher1</code> (with ordinal 4736) */
            PRIMARY_LAUNCHER1 = 4736,
            /** <code>PrimaryLauncher2</code> (with ordinal 4768) */
            PRIMARY_LAUNCHER2 = 4768,
            /** <code>PrimaryLauncher3</code> (with ordinal 4800) */
            PRIMARY_LAUNCHER3 = 4800,
            /** <code>PrimaryLauncher4</code> (with ordinal 4832) */
            PRIMARY_LAUNCHER4 = 4832,
            /** <code>PrimaryLauncher5</code> (with ordinal 4864) */
            PRIMARY_LAUNCHER5 = 4864,
            /** <code>PrimaryLauncher6</code> (with ordinal 4896) */
            PRIMARY_LAUNCHER6 = 4896,
            /** <code>PrimaryLauncher7</code> (with ordinal 4928) */
            PRIMARY_LAUNCHER7 = 4928,
            /** <code>PrimaryLauncher8</code> (with ordinal 4960) */
            PRIMARY_LAUNCHER8 = 4960,
            /** <code>PrimaryLauncher9</code> (with ordinal 4992) */
            PRIMARY_LAUNCHER9 = 4992,
            /** <code>PrimaryLauncher10</code> (with ordinal 5024) */
            PRIMARY_LAUNCHER10 = 5024,
            /** <code>PrimaryDefenseSystems1</code> (with ordinal 5056) */
            PRIMARY_DEFENSE_SYSTEMS1 = 5056,
            /** <code>PrimaryDefenseSystems2</code> (with ordinal 5088) */
            PRIMARY_DEFENSE_SYSTEMS2 = 5088,
            /** <code>PrimaryDefenseSystems3</code> (with ordinal 5120) */
            PRIMARY_DEFENSE_SYSTEMS3 = 5120,
            /** <code>PrimaryDefenseSystems4</code> (with ordinal 5152) */
            PRIMARY_DEFENSE_SYSTEMS4 = 5152,
            /** <code>PrimaryDefenseSystems5</code> (with ordinal 5184) */
            PRIMARY_DEFENSE_SYSTEMS5 = 5184,
            /** <code>PrimaryDefenseSystems6</code> (with ordinal 5216) */
            PRIMARY_DEFENSE_SYSTEMS6 = 5216,
            /** <code>PrimaryDefenseSystems7</code> (with ordinal 5248) */
            PRIMARY_DEFENSE_SYSTEMS7 = 5248,
            /** <code>PrimaryDefenseSystems8</code> (with ordinal 5280) */
            PRIMARY_DEFENSE_SYSTEMS8 = 5280,
            /** <code>PrimaryDefenseSystems9</code> (with ordinal 5312) */
            PRIMARY_DEFENSE_SYSTEMS9 = 5312,
            /** <code>PrimaryDefenseSystems10</code> (with ordinal 5344) */
            PRIMARY_DEFENSE_SYSTEMS10 = 5344,
            /** <code>PrimaryRadar1</code> (with ordinal 5376) */
            PRIMARY_RADAR1 = 5376,
            /** <code>PrimaryRadar2</code> (with ordinal 5408) */
            PRIMARY_RADAR2 = 5408,
            /** <code>PrimaryRadar3</code> (with ordinal 5440) */
            PRIMARY_RADAR3 = 5440,
            /** <code>PrimaryRadar4</code> (with ordinal 5472) */
            PRIMARY_RADAR4 = 5472,
            /** <code>PrimaryRadar5</code> (with ordinal 5504) */
            PRIMARY_RADAR5 = 5504,
            /** <code>PrimaryRadar6</code> (with ordinal 5536) */
            PRIMARY_RADAR6 = 5536,
            /** <code>PrimaryRadar7</code> (with ordinal 5568) */
            PRIMARY_RADAR7 = 5568,
            /** <code>PrimaryRadar8</code> (with ordinal 5600) */
            PRIMARY_RADAR8 = 5600,
            /** <code>PrimaryRadar9</code> (with ordinal 5632) */
            PRIMARY_RADAR9 = 5632,
            /** <code>PrimaryRadar10</code> (with ordinal 5664) */
            PRIMARY_RADAR10 = 5664,
            /** <code>SecondaryTurretNumber1</code> (with ordinal 5696) */
            SECONDARY_TURRET_NUMBER1 = 5696,
            /** <code>SecondaryTurretNumber2</code> (with ordinal 5728) */
            SECONDARY_TURRET_NUMBER2 = 5728,
            /** <code>SecondaryTurretNumber3</code> (with ordinal 5760) */
            SECONDARY_TURRET_NUMBER3 = 5760,
            /** <code>SecondaryTurretNumber4</code> (with ordinal 5792) */
            SECONDARY_TURRET_NUMBER4 = 5792,
            /** <code>SecondaryTurretNumber5</code> (with ordinal 5824) */
            SECONDARY_TURRET_NUMBER5 = 5824,
            /** <code>SecondaryTurretNumber6</code> (with ordinal 5856) */
            SECONDARY_TURRET_NUMBER6 = 5856,
            /** <code>SecondaryTurretNumber7</code> (with ordinal 5888) */
            SECONDARY_TURRET_NUMBER7 = 5888,
            /** <code>SecondaryTurretNumber8</code> (with ordinal 5920) */
            SECONDARY_TURRET_NUMBER8 = 5920,
            /** <code>SecondaryTurretNumber9</code> (with ordinal 5952) */
            SECONDARY_TURRET_NUMBER9 = 5952,
            /** <code>SecondaryTurretNumber10</code> (with ordinal 5984) */
            SECONDARY_TURRET_NUMBER10 = 5984,
            /** <code>SecondaryGunNumber1</code> (with ordinal 6016) */
            SECONDARY_GUN_NUMBER1 = 6016,
            /** <code>SecondaryGunNumber2</code> (with ordinal 6048) */
            SECONDARY_GUN_NUMBER2 = 6048,
            /** <code>SecondaryGunNumber3</code> (with ordinal 6080) */
            SECONDARY_GUN_NUMBER3 = 6080,
            /** <code>SecondaryGunNumber4</code> (with ordinal 6112) */
            SECONDARY_GUN_NUMBER4 = 6112,
            /** <code>SecondaryGunNumber5</code> (with ordinal 6144) */
            SECONDARY_GUN_NUMBER5 = 6144,
            /** <code>SecondaryGunNumber6</code> (with ordinal 6176) */
            SECONDARY_GUN_NUMBER6 = 6176,
            /** <code>SecondaryGunNumber7</code> (with ordinal 6208) */
            SECONDARY_GUN_NUMBER7 = 6208,
            /** <code>SecondaryGunNumber8</code> (with ordinal 6240) */
            SECONDARY_GUN_NUMBER8 = 6240,
            /** <code>SecondaryGunNumber9</code> (with ordinal 6272) */
            SECONDARY_GUN_NUMBER9 = 6272,
            /** <code>SecondaryGunNumber10</code> (with ordinal 6304) */
            SECONDARY_GUN_NUMBER10 = 6304,
            /** <code>SecondaryLauncher1</code> (with ordinal 6336) */
            SECONDARY_LAUNCHER1 = 6336,
            /** <code>SecondaryLauncher2</code> (with ordinal 6368) */
            SECONDARY_LAUNCHER2 = 6368,
            /** <code>SecondaryLauncher3</code> (with ordinal 6400) */
            SECONDARY_LAUNCHER3 = 6400,
            /** <code>SecondaryLauncher4</code> (with ordinal 6432) */
            SECONDARY_LAUNCHER4 = 6432,
            /** <code>SecondaryLauncher5</code> (with ordinal 6464) */
            SECONDARY_LAUNCHER5 = 6464,
            /** <code>SecondaryLauncher6</code> (with ordinal 6496) */
            SECONDARY_LAUNCHER6 = 6496,
            /** <code>SecondaryLauncher7</code> (with ordinal 6528) */
            SECONDARY_LAUNCHER7 = 6528,
            /** <code>SecondaryLauncher8</code> (with ordinal 6560) */
            SECONDARY_LAUNCHER8 = 6560,
            /** <code>SecondaryLauncher9</code> (with ordinal 6592) */
            SECONDARY_LAUNCHER9 = 6592,
            /** <code>SecondaryLauncher10</code> (with ordinal 6624) */
            SECONDARY_LAUNCHER10 = 6624,
            /** <code>SecondaryDefenseSystems1</code> (with ordinal 6656) */
            SECONDARY_DEFENSE_SYSTEMS1 = 6656,
            /** <code>SecondaryDefenseSystems2</code> (with ordinal 6688) */
            SECONDARY_DEFENSE_SYSTEMS2 = 6688,
            /** <code>SecondaryDefenseSystems3</code> (with ordinal 6720) */
            SECONDARY_DEFENSE_SYSTEMS3 = 6720,
            /** <code>SecondaryDefenseSystems4</code> (with ordinal 6752) */
            SECONDARY_DEFENSE_SYSTEMS4 = 6752,
            /** <code>SecondaryDefenseSystems5</code> (with ordinal 6784) */
            SECONDARY_DEFENSE_SYSTEMS5 = 6784,
            /** <code>SecondaryDefenseSystems6</code> (with ordinal 6816) */
            SECONDARY_DEFENSE_SYSTEMS6 = 6816,
            /** <code>SecondaryDefenseSystems7</code> (with ordinal 6848) */
            SECONDARY_DEFENSE_SYSTEMS7 = 6848,
            /** <code>SecondaryDefenseSystems8</code> (with ordinal 6880) */
            SECONDARY_DEFENSE_SYSTEMS8 = 6880,
            /** <code>SecondaryDefenseSystems9</code> (with ordinal 6912) */
            SECONDARY_DEFENSE_SYSTEMS9 = 6912,
            /** <code>SecondaryDefenseSystems10</code> (with ordinal 6944) */
            SECONDARY_DEFENSE_SYSTEMS10 = 6944,
            /** <code>SecondaryRadar1</code> (with ordinal 6976) */
            SECONDARY_RADAR1 = 6976,
            /** <code>SecondaryRadar2</code> (with ordinal 7008) */
            SECONDARY_RADAR2 = 7008,
            /** <code>SecondaryRadar3</code> (with ordinal 7040) */
            SECONDARY_RADAR3 = 7040,
            /** <code>SecondaryRadar4</code> (with ordinal 7072) */
            SECONDARY_RADAR4 = 7072,
            /** <code>SecondaryRadar5</code> (with ordinal 7104) */
            SECONDARY_RADAR5 = 7104,
            /** <code>SecondaryRadar6</code> (with ordinal 7136) */
            SECONDARY_RADAR6 = 7136,
            /** <code>SecondaryRadar7</code> (with ordinal 7168) */
            SECONDARY_RADAR7 = 7168,
            /** <code>SecondaryRadar8</code> (with ordinal 7200) */
            SECONDARY_RADAR8 = 7200,
            /** <code>SecondaryRadar9</code> (with ordinal 7232) */
            SECONDARY_RADAR9 = 7232,
            /** <code>SecondaryRadar10</code> (with ordinal 7264) */
            SECONDARY_RADAR10 = 7264,
            /** <code>DeckElevator1</code> (with ordinal 7296) */
            DECK_ELEVATOR1 = 7296,
            /** <code>DeckElevator2</code> (with ordinal 7328) */
            DECK_ELEVATOR2 = 7328,
            /** <code>Catapult1</code> (with ordinal 7360) */
            CATAPULT1 = 7360,
            /** <code>Catapult2</code> (with ordinal 7392) */
            CATAPULT2 = 7392,
            /** <code>JetBlastDeflector1</code> (with ordinal 7424) */
            JET_BLAST_DEFLECTOR1 = 7424,
            /** <code>JetBlastDeflector2</code> (with ordinal 7456) */
            JET_BLAST_DEFLECTOR2 = 7456,
            /** <code>ArrestorWires1</code> (with ordinal 7488) */
            ARRESTOR_WIRES1 = 7488,
            /** <code>ArrestorWires2</code> (with ordinal 7520) */
            ARRESTOR_WIRES2 = 7520,
            /** <code>ArrestorWires3</code> (with ordinal 7552) */
            ARRESTOR_WIRES3 = 7552,
            /** <code>WingOrRotorFold</code> (with ordinal 7584) */
            WING_OR_ROTOR_FOLD = 7584,
            /** <code>FuselageFold</code> (with ordinal 7616) */
            FUSELAGE_FOLD = 7616,
            /** <code>CargoDoor</code> (with ordinal 7648) */
            CARGO_DOOR = 7648,
            /** <code>CargoRamp</code> (with ordinal 7680) */
            CARGO_RAMP = 7680,
            /** <code>Air-to-AirRefuelingBoom</code> (with ordinal 7712) */
            AIR_TO_AIR_REFUELING_BOOM = 7712
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to ArticulatedPartsTypeEnum: static_cast<DevStudio::ArticulatedPartsTypeEnum::ArticulatedPartsTypeEnum>(i)
        */
        LIBAPI bool isValid(const ArticulatedPartsTypeEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::ArticulatedPartsTypeEnum::ArticulatedPartsTypeEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::ArticulatedPartsTypeEnum::ArticulatedPartsTypeEnum const &);
}


#endif
