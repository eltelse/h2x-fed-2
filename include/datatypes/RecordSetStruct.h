/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_RECORDSETSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_RECORDSETSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/DatumIdentifierEnum.h>
#include <DevStudio/datatypes/RecordStruct.h>
#include <DevStudio/datatypes/RecordStructArray.h>
#include <vector>

namespace DevStudio {
   /**
   * Implementation of the <code>RecordSetStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>A set of records and record details.</i>
   */
   class RecordSetStruct {

   public:
      /**
      * Description from the FOM: <i>This field shall specify the data structure used to convey the parameter values of the record for each record.</i>.
      * <br>Description of the data type from the FOM: <i>Datum ID</i>
      */
      DatumIdentifierEnum::DatumIdentifierEnum recordSetIdentifier;
      /**
      * Description from the FOM: <i>This field shall specify the serial number of the first record in the array.</i>.
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      unsigned int recordSetSerialNumber;
      /**
      * Description from the FOM: <i>This field contains the records of the format specified by the Record ID field.</i>.
      * <br>Description of the data type from the FOM: <i>Array of RecordStruct</i>
      */
      std::vector<DevStudio::RecordStruct > numberOfRecordsARecordValues;

      LIBAPI RecordSetStruct()
         :
         recordSetIdentifier(DatumIdentifierEnum::DatumIdentifierEnum()),
         recordSetSerialNumber(0),
         numberOfRecordsARecordValues(std::vector<DevStudio::RecordStruct >())
      {}

      /**
      * Constructor for RecordSetStruct
      *
      * @param recordSetIdentifier_ value to set as recordSetIdentifier.
      * <br>Description from the FOM: <i>This field shall specify the data structure used to convey the parameter values of the record for each record.</i>
      * <br>Description of the data type from the FOM: <i>Datum ID</i>
      * @param recordSetSerialNumber_ value to set as recordSetSerialNumber.
      * <br>Description from the FOM: <i>This field shall specify the serial number of the first record in the array.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param numberOfRecordsARecordValues_ value to set as numberOfRecordsARecordValues.
      * <br>Description from the FOM: <i>This field contains the records of the format specified by the Record ID field.</i>
      * <br>Description of the data type from the FOM: <i>Array of RecordStruct</i>
      */
      LIBAPI RecordSetStruct(
         DatumIdentifierEnum::DatumIdentifierEnum recordSetIdentifier_,
         unsigned int recordSetSerialNumber_,
         std::vector<DevStudio::RecordStruct > numberOfRecordsARecordValues_
         )
         :
         recordSetIdentifier(recordSetIdentifier_),
         recordSetSerialNumber(recordSetSerialNumber_),
         numberOfRecordsARecordValues(numberOfRecordsARecordValues_)
      {}



      /**
      * Function to get recordSetIdentifier.
      * <br>Description from the FOM: <i>This field shall specify the data structure used to convey the parameter values of the record for each record.</i>
      * <br>Description of the data type from the FOM: <i>Datum ID</i>
      *
      * @return recordSetIdentifier
      */
      LIBAPI DevStudio::DatumIdentifierEnum::DatumIdentifierEnum & getRecordSetIdentifier() {
         return recordSetIdentifier;
      }

      /**
      * Function to get recordSetSerialNumber.
      * <br>Description from the FOM: <i>This field shall specify the serial number of the first record in the array.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return recordSetSerialNumber
      */
      LIBAPI unsigned int & getRecordSetSerialNumber() {
         return recordSetSerialNumber;
      }

      /**
      * Function to get numberOfRecordsARecordValues.
      * <br>Description from the FOM: <i>This field contains the records of the format specified by the Record ID field.</i>
      * <br>Description of the data type from the FOM: <i>Array of RecordStruct</i>
      *
      * @return numberOfRecordsARecordValues
      */
      LIBAPI std::vector<DevStudio::RecordStruct > & getNumberOfRecordsARecordValues() {
         return numberOfRecordsARecordValues;
      }

   };


   LIBAPI bool operator ==(const DevStudio::RecordSetStruct& l, const DevStudio::RecordSetStruct& r);
   LIBAPI bool operator !=(const DevStudio::RecordSetStruct& l, const DevStudio::RecordSetStruct& r);
   LIBAPI bool operator <(const DevStudio::RecordSetStruct& l, const DevStudio::RecordSetStruct& r);
   LIBAPI bool operator >(const DevStudio::RecordSetStruct& l, const DevStudio::RecordSetStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::RecordSetStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::RecordSetStruct const &);
}
#endif
