/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_BEAMANTENNASTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_BEAMANTENNASTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/OrientationStruct.h>
#include <DevStudio/datatypes/ReferenceSystemEnum.h>

namespace DevStudio {
   /**
   * Implementation of the <code>BeamAntennaStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Specifies the direction, pattern, and polarization of radiation from a radio transmitter's antenna.</i>
   */
   class BeamAntennaStruct {

   public:
      /**
      * Description from the FOM: <i>The rotation that transforms the reference coordinate system into the beam coordinate system.</i>.
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      */
      OrientationStruct beamOrientation;
      /**
      * Description from the FOM: <i>The full width of the beam to the -3 dB power density points in the x-y plane of the beam coordinate system.</i>.
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      */
      float beamAzimuthBeamwidth;
      /**
      * Description from the FOM: <i>The full width of the beam to the -3 dB power density points in the x-z plane of the beam coordinate system.</i>.
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      */
      float beamElevationBeamwidth;
      /**
      * Description from the FOM: <i>The reference coordinate system with respect to which beam direction is specified</i>.
      * <br>Description of the data type from the FOM: <i>The reference coordinate system used</i>
      */
      ReferenceSystemEnum::ReferenceSystemEnum referenceSystem;
      /**
      * Description from the FOM: <i>The magnitude of the Z-component (in beam coordinates) of the Electrical field at some arbitrary single point in the mainbeam and in the far field of the antenna.</i>.
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      */
      float ez;
      /**
      * Description from the FOM: <i>The magnitude of the X-component (in beam coordinates) of the Electrical field at some arbitrary single point in the mainbeam and in the far field of the antenna</i>.
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      */
      float ex;
      /**
      * Description from the FOM: <i>The phase angle between Ez and Ex in radians.</i>.
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      */
      float beamPhaseAngle;

      LIBAPI BeamAntennaStruct()
         :
         beamOrientation(OrientationStruct()),
         beamAzimuthBeamwidth(0),
         beamElevationBeamwidth(0),
         referenceSystem(ReferenceSystemEnum::ReferenceSystemEnum()),
         ez(0),
         ex(0),
         beamPhaseAngle(0)
      {}

      /**
      * Constructor for BeamAntennaStruct
      *
      * @param beamOrientation_ value to set as beamOrientation.
      * <br>Description from the FOM: <i>The rotation that transforms the reference coordinate system into the beam coordinate system.</i>
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      * @param beamAzimuthBeamwidth_ value to set as beamAzimuthBeamwidth.
      * <br>Description from the FOM: <i>The full width of the beam to the -3 dB power density points in the x-y plane of the beam coordinate system.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      * @param beamElevationBeamwidth_ value to set as beamElevationBeamwidth.
      * <br>Description from the FOM: <i>The full width of the beam to the -3 dB power density points in the x-z plane of the beam coordinate system.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      * @param referenceSystem_ value to set as referenceSystem.
      * <br>Description from the FOM: <i>The reference coordinate system with respect to which beam direction is specified</i>
      * <br>Description of the data type from the FOM: <i>The reference coordinate system used</i>
      * @param ez_ value to set as ez.
      * <br>Description from the FOM: <i>The magnitude of the Z-component (in beam coordinates) of the Electrical field at some arbitrary single point in the mainbeam and in the far field of the antenna.</i>
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      * @param ex_ value to set as ex.
      * <br>Description from the FOM: <i>The magnitude of the X-component (in beam coordinates) of the Electrical field at some arbitrary single point in the mainbeam and in the far field of the antenna</i>
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      * @param beamPhaseAngle_ value to set as beamPhaseAngle.
      * <br>Description from the FOM: <i>The phase angle between Ez and Ex in radians.</i>
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      */
      LIBAPI BeamAntennaStruct(
         OrientationStruct beamOrientation_,
         float beamAzimuthBeamwidth_,
         float beamElevationBeamwidth_,
         ReferenceSystemEnum::ReferenceSystemEnum referenceSystem_,
         float ez_,
         float ex_,
         float beamPhaseAngle_
         )
         :
         beamOrientation(beamOrientation_),
         beamAzimuthBeamwidth(beamAzimuthBeamwidth_),
         beamElevationBeamwidth(beamElevationBeamwidth_),
         referenceSystem(referenceSystem_),
         ez(ez_),
         ex(ex_),
         beamPhaseAngle(beamPhaseAngle_)
      {}



      /**
      * Function to get beamOrientation.
      * <br>Description from the FOM: <i>The rotation that transforms the reference coordinate system into the beam coordinate system.</i>
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @return beamOrientation
      */
      LIBAPI DevStudio::OrientationStruct & getBeamOrientation() {
         return beamOrientation;
      }

      /**
      * Function to get beamAzimuthBeamwidth.
      * <br>Description from the FOM: <i>The full width of the beam to the -3 dB power density points in the x-y plane of the beam coordinate system.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @return beamAzimuthBeamwidth
      */
      LIBAPI float & getBeamAzimuthBeamwidth() {
         return beamAzimuthBeamwidth;
      }

      /**
      * Function to get beamElevationBeamwidth.
      * <br>Description from the FOM: <i>The full width of the beam to the -3 dB power density points in the x-z plane of the beam coordinate system.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @return beamElevationBeamwidth
      */
      LIBAPI float & getBeamElevationBeamwidth() {
         return beamElevationBeamwidth;
      }

      /**
      * Function to get referenceSystem.
      * <br>Description from the FOM: <i>The reference coordinate system with respect to which beam direction is specified</i>
      * <br>Description of the data type from the FOM: <i>The reference coordinate system used</i>
      *
      * @return referenceSystem
      */
      LIBAPI DevStudio::ReferenceSystemEnum::ReferenceSystemEnum & getReferenceSystem() {
         return referenceSystem;
      }

      /**
      * Function to get ez.
      * <br>Description from the FOM: <i>The magnitude of the Z-component (in beam coordinates) of the Electrical field at some arbitrary single point in the mainbeam and in the far field of the antenna.</i>
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      *
      * @return ez
      */
      LIBAPI float & getEz() {
         return ez;
      }

      /**
      * Function to get ex.
      * <br>Description from the FOM: <i>The magnitude of the X-component (in beam coordinates) of the Electrical field at some arbitrary single point in the mainbeam and in the far field of the antenna</i>
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      *
      * @return ex
      */
      LIBAPI float & getEx() {
         return ex;
      }

      /**
      * Function to get beamPhaseAngle.
      * <br>Description from the FOM: <i>The phase angle between Ez and Ex in radians.</i>
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      *
      * @return beamPhaseAngle
      */
      LIBAPI float & getBeamPhaseAngle() {
         return beamPhaseAngle;
      }

   };


   LIBAPI bool operator ==(const DevStudio::BeamAntennaStruct& l, const DevStudio::BeamAntennaStruct& r);
   LIBAPI bool operator !=(const DevStudio::BeamAntennaStruct& l, const DevStudio::BeamAntennaStruct& r);
   LIBAPI bool operator <(const DevStudio::BeamAntennaStruct& l, const DevStudio::BeamAntennaStruct& r);
   LIBAPI bool operator >(const DevStudio::BeamAntennaStruct& l, const DevStudio::BeamAntennaStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::BeamAntennaStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::BeamAntennaStruct const &);
}
#endif
