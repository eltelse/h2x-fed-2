/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_BEAMFUNCTIONCODEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_BEAMFUNCTIONCODEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace BeamFunctionCodeEnum {
        /**
        * Implementation of the <code>BeamFunctionCodeEnum8</code> data type from the FOM.
        * <br>Description from the FOM: <i>Beam function</i>
        */
        enum BeamFunctionCodeEnum {
            /** <code>Other</code> (with ordinal 0) */
            OTHER = 0,
            /** <code>Search</code> (with ordinal 1) */
            SEARCH = 1,
            /** <code>HeightFinder</code> (with ordinal 2) */
            HEIGHT_FINDER = 2,
            /** <code>Acquisition</code> (with ordinal 3) */
            ACQUISITION = 3,
            /** <code>Tracking</code> (with ordinal 4) */
            TRACKING = 4,
            /** <code>AcquisitionAndTracking</code> (with ordinal 5) */
            ACQUISITION_AND_TRACKING = 5,
            /** <code>CommandGuidance</code> (with ordinal 6) */
            COMMAND_GUIDANCE = 6,
            /** <code>Illumination</code> (with ordinal 7) */
            ILLUMINATION = 7,
            /** <code>RangeOnlyRadar</code> (with ordinal 8) */
            RANGE_ONLY_RADAR = 8,
            /** <code>MissileBeacon</code> (with ordinal 9) */
            MISSILE_BEACON = 9,
            /** <code>MissileFuze</code> (with ordinal 10) */
            MISSILE_FUZE = 10,
            /** <code>ActiveRadarMissileSeeker</code> (with ordinal 11) */
            ACTIVE_RADAR_MISSILE_SEEKER = 11,
            /** <code>Jammer</code> (with ordinal 12) */
            JAMMER = 12,
            /** <code>IFF</code> (with ordinal 13) */
            IFF = 13,
            /** <code>NavigationalOrWeather</code> (with ordinal 14) */
            NAVIGATIONAL_OR_WEATHER = 14,
            /** <code>Meteorological</code> (with ordinal 15) */
            METEOROLOGICAL = 15,
            /** <code>DataTransmission</code> (with ordinal 16) */
            DATA_TRANSMISSION = 16,
            /** <code>NavigationalDirectionalBeacon</code> (with ordinal 17) */
            NAVIGATIONAL_DIRECTIONAL_BEACON = 17,
            /** <code>Time-SharedSearch</code> (with ordinal 20) */
            TIME_SHARED_SEARCH = 20,
            /** <code>Time-SharedAcquisition</code> (with ordinal 21) */
            TIME_SHARED_ACQUISITION = 21,
            /** <code>Time-SharedTrack</code> (with ordinal 22) */
            TIME_SHARED_TRACK = 22,
            /** <code>Time-SharedCommandGuidance</code> (with ordinal 23) */
            TIME_SHARED_COMMAND_GUIDANCE = 23,
            /** <code>Time-SharedIllumination</code> (with ordinal 24) */
            TIME_SHARED_ILLUMINATION = 24,
            /** <code>Time-SharedJamming</code> (with ordinal 25) */
            TIME_SHARED_JAMMING = 25
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to BeamFunctionCodeEnum: static_cast<DevStudio::BeamFunctionCodeEnum::BeamFunctionCodeEnum>(i)
        */
        LIBAPI bool isValid(const BeamFunctionCodeEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::BeamFunctionCodeEnum::BeamFunctionCodeEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::BeamFunctionCodeEnum::BeamFunctionCodeEnum const &);
}


#endif
