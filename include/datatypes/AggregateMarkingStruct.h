/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_AGGREGATEMARKINGSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_AGGREGATEMARKINGSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/MarkingArray31.h>
#include <DevStudio/datatypes/MarkingEncodingEnum.h>
#include <vector>

namespace DevStudio {
   /**
   * Implementation of the <code>AggregateMarkingStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Unique marking associated with an aggregate.</i>
   */
   class AggregateMarkingStruct {

   public:
      /**
      * Description from the FOM: <i>The type of marking.</i>.
      * <br>Description of the data type from the FOM: <i>Marking character set</i>
      */
      MarkingEncodingEnum::MarkingEncodingEnum markingEncodingType;
      /**
      * Description from the FOM: <i>The marking itself.</i>.
      * <br>Description of the data type from the FOM: <i>String of characters represented by a 31 element character string.</i>
      */
      std::vector</* 31 */ char > markingData;

      LIBAPI AggregateMarkingStruct()
         :
         markingEncodingType(MarkingEncodingEnum::MarkingEncodingEnum()),
         markingData(0)
      {}

      /**
      * Constructor for AggregateMarkingStruct
      *
      * @param markingEncodingType_ value to set as markingEncodingType.
      * <br>Description from the FOM: <i>The type of marking.</i>
      * <br>Description of the data type from the FOM: <i>Marking character set</i>
      * @param markingData_ value to set as markingData.
      * <br>Description from the FOM: <i>The marking itself.</i>
      * <br>Description of the data type from the FOM: <i>String of characters represented by a 31 element character string.</i>
      */
      LIBAPI AggregateMarkingStruct(
         MarkingEncodingEnum::MarkingEncodingEnum markingEncodingType_,
         std::vector</* 31 */ char > markingData_
         )
         :
         markingEncodingType(markingEncodingType_),
         markingData(markingData_)
      {}



      /**
      * Function to get markingEncodingType.
      * <br>Description from the FOM: <i>The type of marking.</i>
      * <br>Description of the data type from the FOM: <i>Marking character set</i>
      *
      * @return markingEncodingType
      */
      LIBAPI DevStudio::MarkingEncodingEnum::MarkingEncodingEnum & getMarkingEncodingType() {
         return markingEncodingType;
      }

      /**
      * Function to get markingData.
      * <br>Description from the FOM: <i>The marking itself.</i>
      * <br>Description of the data type from the FOM: <i>String of characters represented by a 31 element character string.</i>
      *
      * @return markingData
      */
      LIBAPI std::vector</* 31 */ char > & getMarkingData() {
         return markingData;
      }

   };


   LIBAPI bool operator ==(const DevStudio::AggregateMarkingStruct& l, const DevStudio::AggregateMarkingStruct& r);
   LIBAPI bool operator !=(const DevStudio::AggregateMarkingStruct& l, const DevStudio::AggregateMarkingStruct& r);
   LIBAPI bool operator <(const DevStudio::AggregateMarkingStruct& l, const DevStudio::AggregateMarkingStruct& r);
   LIBAPI bool operator >(const DevStudio::AggregateMarkingStruct& l, const DevStudio::AggregateMarkingStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::AggregateMarkingStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::AggregateMarkingStruct const &);
}
#endif
