/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_GAUSSPUFFGEOMRECSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_GAUSSPUFFGEOMRECSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/AngularVelocityVectorStruct.h>
#include <DevStudio/datatypes/DimensionRateStruct.h>
#include <DevStudio/datatypes/DimensionStruct.h>
#include <DevStudio/datatypes/OrientationStruct.h>
#include <DevStudio/datatypes/VelocityVectorStruct.h>
#include <DevStudio/datatypes/WorldLocationStruct.h>

namespace DevStudio {
   /**
   * Implementation of the <code>GaussPuffGeomRecStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Record specifying Gaussian Puff geometry record</i>
   */
   class GaussPuffGeomRecStruct {

   public:
      /**
      * Description from the FOM: <i>Puff location X, Y, Z</i>.
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      */
      WorldLocationStruct puffLocation;
      /**
      * Description from the FOM: <i>Origination location X, Y, Z</i>.
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      */
      WorldLocationStruct originationLocation;
      /**
      * Description from the FOM: <i>Sigma dimensions</i>.
      * <br>Description of the data type from the FOM: <i>Bounding box in X,Y,Z axis.</i>
      */
      DimensionStruct sigmaValue;
      /**
      * Description from the FOM: <i>Variation of sigma dimensions</i>.
      * <br>Description of the data type from the FOM: <i>Record specifying length X, Y, Z rates</i>
      */
      DimensionRateStruct sigmaRate;
      /**
      * Description from the FOM: <i>Orientation, specified by Euler angles</i>.
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      */
      OrientationStruct orientation;
      /**
      * Description from the FOM: <i>Velocity Vx, Vy, Vz</i>.
      * <br>Description of the data type from the FOM: <i>The rate at which the position is changing over time.</i>
      */
      VelocityVectorStruct velocity;
      /**
      * Description from the FOM: <i>Angular velocity Vx, Vy, Vz</i>.
      * <br>Description of the data type from the FOM: <i>The rate at which the orientation is changing over time, in body coordinates.</i>
      */
      AngularVelocityVectorStruct angularVelocity;
      /**
      * Description from the FOM: <i>Centroid height</i>.
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      */
      float centroidHeight;

      LIBAPI GaussPuffGeomRecStruct()
         :
         puffLocation(WorldLocationStruct()),
         originationLocation(WorldLocationStruct()),
         sigmaValue(DimensionStruct()),
         sigmaRate(DimensionRateStruct()),
         orientation(OrientationStruct()),
         velocity(VelocityVectorStruct()),
         angularVelocity(AngularVelocityVectorStruct()),
         centroidHeight(0)
      {}

      /**
      * Constructor for GaussPuffGeomRecStruct
      *
      * @param puffLocation_ value to set as puffLocation.
      * <br>Description from the FOM: <i>Puff location X, Y, Z</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      * @param originationLocation_ value to set as originationLocation.
      * <br>Description from the FOM: <i>Origination location X, Y, Z</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      * @param sigmaValue_ value to set as sigmaValue.
      * <br>Description from the FOM: <i>Sigma dimensions</i>
      * <br>Description of the data type from the FOM: <i>Bounding box in X,Y,Z axis.</i>
      * @param sigmaRate_ value to set as sigmaRate.
      * <br>Description from the FOM: <i>Variation of sigma dimensions</i>
      * <br>Description of the data type from the FOM: <i>Record specifying length X, Y, Z rates</i>
      * @param orientation_ value to set as orientation.
      * <br>Description from the FOM: <i>Orientation, specified by Euler angles</i>
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      * @param velocity_ value to set as velocity.
      * <br>Description from the FOM: <i>Velocity Vx, Vy, Vz</i>
      * <br>Description of the data type from the FOM: <i>The rate at which the position is changing over time.</i>
      * @param angularVelocity_ value to set as angularVelocity.
      * <br>Description from the FOM: <i>Angular velocity Vx, Vy, Vz</i>
      * <br>Description of the data type from the FOM: <i>The rate at which the orientation is changing over time, in body coordinates.</i>
      * @param centroidHeight_ value to set as centroidHeight.
      * <br>Description from the FOM: <i>Centroid height</i>
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      */
      LIBAPI GaussPuffGeomRecStruct(
         WorldLocationStruct puffLocation_,
         WorldLocationStruct originationLocation_,
         DimensionStruct sigmaValue_,
         DimensionRateStruct sigmaRate_,
         OrientationStruct orientation_,
         VelocityVectorStruct velocity_,
         AngularVelocityVectorStruct angularVelocity_,
         float centroidHeight_
         )
         :
         puffLocation(puffLocation_),
         originationLocation(originationLocation_),
         sigmaValue(sigmaValue_),
         sigmaRate(sigmaRate_),
         orientation(orientation_),
         velocity(velocity_),
         angularVelocity(angularVelocity_),
         centroidHeight(centroidHeight_)
      {}



      /**
      * Function to get puffLocation.
      * <br>Description from the FOM: <i>Puff location X, Y, Z</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @return puffLocation
      */
      LIBAPI DevStudio::WorldLocationStruct & getPuffLocation() {
         return puffLocation;
      }

      /**
      * Function to get originationLocation.
      * <br>Description from the FOM: <i>Origination location X, Y, Z</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @return originationLocation
      */
      LIBAPI DevStudio::WorldLocationStruct & getOriginationLocation() {
         return originationLocation;
      }

      /**
      * Function to get sigmaValue.
      * <br>Description from the FOM: <i>Sigma dimensions</i>
      * <br>Description of the data type from the FOM: <i>Bounding box in X,Y,Z axis.</i>
      *
      * @return sigmaValue
      */
      LIBAPI DevStudio::DimensionStruct & getSigmaValue() {
         return sigmaValue;
      }

      /**
      * Function to get sigmaRate.
      * <br>Description from the FOM: <i>Variation of sigma dimensions</i>
      * <br>Description of the data type from the FOM: <i>Record specifying length X, Y, Z rates</i>
      *
      * @return sigmaRate
      */
      LIBAPI DevStudio::DimensionRateStruct & getSigmaRate() {
         return sigmaRate;
      }

      /**
      * Function to get orientation.
      * <br>Description from the FOM: <i>Orientation, specified by Euler angles</i>
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @return orientation
      */
      LIBAPI DevStudio::OrientationStruct & getOrientation() {
         return orientation;
      }

      /**
      * Function to get velocity.
      * <br>Description from the FOM: <i>Velocity Vx, Vy, Vz</i>
      * <br>Description of the data type from the FOM: <i>The rate at which the position is changing over time.</i>
      *
      * @return velocity
      */
      LIBAPI DevStudio::VelocityVectorStruct & getVelocity() {
         return velocity;
      }

      /**
      * Function to get angularVelocity.
      * <br>Description from the FOM: <i>Angular velocity Vx, Vy, Vz</i>
      * <br>Description of the data type from the FOM: <i>The rate at which the orientation is changing over time, in body coordinates.</i>
      *
      * @return angularVelocity
      */
      LIBAPI DevStudio::AngularVelocityVectorStruct & getAngularVelocity() {
         return angularVelocity;
      }

      /**
      * Function to get centroidHeight.
      * <br>Description from the FOM: <i>Centroid height</i>
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      *
      * @return centroidHeight
      */
      LIBAPI float & getCentroidHeight() {
         return centroidHeight;
      }

   };


   LIBAPI bool operator ==(const DevStudio::GaussPuffGeomRecStruct& l, const DevStudio::GaussPuffGeomRecStruct& r);
   LIBAPI bool operator !=(const DevStudio::GaussPuffGeomRecStruct& l, const DevStudio::GaussPuffGeomRecStruct& r);
   LIBAPI bool operator <(const DevStudio::GaussPuffGeomRecStruct& l, const DevStudio::GaussPuffGeomRecStruct& r);
   LIBAPI bool operator >(const DevStudio::GaussPuffGeomRecStruct& l, const DevStudio::GaussPuffGeomRecStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::GaussPuffGeomRecStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::GaussPuffGeomRecStruct const &);
}
#endif
