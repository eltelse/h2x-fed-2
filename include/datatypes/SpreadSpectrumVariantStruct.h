/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_SPREADSPECTRUMVARIANTSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_SPREADSPECTRUMVARIANTSTRUCT_H

#include <DevStudio/datatypes/SINCGARSModulationStruct.h>
#include <DevStudio/datatypes/SpreadSpectrumEnum.h>

#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
   /**
   * Implementation of the <code>SpreadSpectrumVariantStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Identifies the actual spread spectrum technique in use.</i>
   */
   class SpreadSpectrumVariantStruct {

   public:
      LIBAPI SpreadSpectrumVariantStruct()
         : 
         _discriminant(DevStudio::SpreadSpectrumEnum::SpreadSpectrumEnum())
      {}

      /** 
      * Create a new alternative SpreadSpectrumVariantStruct, with <code>SINCGARSFrequencyHop</code> as discriminant.
      *
      * @param sINCGARSModulation value of the SINCGARSModulation field
      *
      * @return a new SpreadSpectrumVariantStruct
      */
      LIBAPI static SpreadSpectrumVariantStruct createSINCGARSModulation(DevStudio::SINCGARSModulationStruct sINCGARSModulation);

      /**
      * Create a new alternative SpreadSpectrumVariantStruct, with <code>None</code> as discriminant.
      *
      * @return a new SpreadSpectrumVariantStruct
      */
      LIBAPI static SpreadSpectrumVariantStruct createNone();



      /**
      * Function to get discriminant
      *
      * @return disciminant
      */
      LIBAPI DevStudio::SpreadSpectrumEnum::SpreadSpectrumEnum getDiscriminant() const {
          return _discriminant;
      }

      /**
      * Function to get SINCGARSModulation.
      * Note that this field is only valid of the discriminant is <code>SINCGARSFrequencyHop</code>.
      * <br>Description from the FOM: <i>Modulation parameters for SINCGARS radio system.</i>
      * <br>Description of the data type from the FOM: <i>Modulation parameters for SINCGARS radio system</i>
      *
      * @return SINCGARSModulation value
      */
      LIBAPI DevStudio::SINCGARSModulationStructPtr getSINCGARSModulation() const {
         return _sINCGARSModulation;
      }

      /**
      * Function to set SINCGARSModulation.
      * Note that this will set the discriminant to <code>SINCGARSFrequencyHop</code>.
      * <br>Description from the FOM: <i>Modulation parameters for SINCGARS radio system.</i>
      * <br>Description of the data type from the FOM: <i>Modulation parameters for SINCGARS radio system</i>
      *
      * @param sINCGARSModulation value used to create a SINCGARSModulationStructPtr
      */
      LIBAPI void setSINCGARSModulation(const DevStudio::SINCGARSModulationStruct sINCGARSModulation) {
         _sINCGARSModulation = DevStudio::SINCGARSModulationStructPtr( new DevStudio::SINCGARSModulationStruct (sINCGARSModulation));
         _discriminant = SpreadSpectrumEnum::SINCGARSFREQUENCY_HOP;
      }

      /** Description from the FOM: <i>Modulation parameters for SINCGARS radio system.</i> */
      DevStudio::SINCGARSModulationStructPtr _sINCGARSModulation;

   private:
      DevStudio::SpreadSpectrumEnum::SpreadSpectrumEnum _discriminant;

      SpreadSpectrumVariantStruct(
         DevStudio::SINCGARSModulationStructPtr sINCGARSModulation,
         DevStudio::SpreadSpectrumEnum::SpreadSpectrumEnum discriminant
      );
   };

   LIBAPI bool operator ==(const DevStudio::SpreadSpectrumVariantStruct& l, const DevStudio::SpreadSpectrumVariantStruct& r);
   LIBAPI bool operator !=(const DevStudio::SpreadSpectrumVariantStruct& l, const DevStudio::SpreadSpectrumVariantStruct& r);
   LIBAPI bool operator <(const DevStudio::SpreadSpectrumVariantStruct& l, const DevStudio::SpreadSpectrumVariantStruct& r);
   LIBAPI bool operator >(const DevStudio::SpreadSpectrumVariantStruct& l, const DevStudio::SpreadSpectrumVariantStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::SpreadSpectrumVariantStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::SpreadSpectrumVariantStruct const &);
}

#endif
