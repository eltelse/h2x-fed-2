/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_CONSTITUENTPARTSTATIONNAMEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_CONSTITUENTPARTSTATIONNAMEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace ConstituentPartStationNameEnum {
        /**
        * Implementation of the <code>ConstituentPartStationNameEnum16</code> data type from the FOM.
        * <br>Description from the FOM: <i>Station name</i>
        */
        enum ConstituentPartStationNameEnum {
            /** <code>Other</code> (with ordinal 0) */
            OTHER = 0,
            /** <code>AircraftWingstation</code> (with ordinal 1) */
            AIRCRAFT_WINGSTATION = 1,
            /** <code>ShipsForwardGunmountStarboard</code> (with ordinal 2) */
            SHIPS_FORWARD_GUNMOUNT_STARBOARD = 2,
            /** <code>ShipsForwardGunmountPort</code> (with ordinal 3) */
            SHIPS_FORWARD_GUNMOUNT_PORT = 3,
            /** <code>ShipsForwardGunmountCenterline</code> (with ordinal 4) */
            SHIPS_FORWARD_GUNMOUNT_CENTERLINE = 4,
            /** <code>ShipsAftGunmountStarboard</code> (with ordinal 5) */
            SHIPS_AFT_GUNMOUNT_STARBOARD = 5,
            /** <code>ShipsAftGunmountPort</code> (with ordinal 6) */
            SHIPS_AFT_GUNMOUNT_PORT = 6,
            /** <code>ShipsAftGunmountCenterline</code> (with ordinal 7) */
            SHIPS_AFT_GUNMOUNT_CENTERLINE = 7,
            /** <code>ForwardTorpedoTube</code> (with ordinal 8) */
            FORWARD_TORPEDO_TUBE = 8,
            /** <code>AftTorpedoTube</code> (with ordinal 9) */
            AFT_TORPEDO_TUBE = 9,
            /** <code>BombBay</code> (with ordinal 10) */
            BOMB_BAY = 10,
            /** <code>CargoBay</code> (with ordinal 11) */
            CARGO_BAY = 11,
            /** <code>TruckBed</code> (with ordinal 12) */
            TRUCK_BED = 12,
            /** <code>TrailerBed</code> (with ordinal 13) */
            TRAILER_BED = 13,
            /** <code>WellDeck</code> (with ordinal 14) */
            WELL_DECK = 14,
            /** <code>OnStationRangeBearing</code> (with ordinal 15) */
            ON_STATION_RANGE_BEARING = 15,
            /** <code>OnStationXYZ</code> (with ordinal 16) */
            ON_STATION_XYZ = 16
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to ConstituentPartStationNameEnum: static_cast<DevStudio::ConstituentPartStationNameEnum::ConstituentPartStationNameEnum>(i)
        */
        LIBAPI bool isValid(const ConstituentPartStationNameEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::ConstituentPartStationNameEnum::ConstituentPartStationNameEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::ConstituentPartStationNameEnum::ConstituentPartStationNameEnum const &);
}


#endif
