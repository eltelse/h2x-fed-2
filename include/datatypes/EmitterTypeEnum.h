/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_EMITTERTYPEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_EMITTERTYPEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace EmitterTypeEnum {
        /**
        * Implementation of the <code>EmitterTypeEnum16</code> data type from the FOM.
        * <br>Description from the FOM: <i>Emitter name</i>
        */
        enum EmitterTypeEnum {
            /** <code>Emitter_1L250</code> (with ordinal 5) */
            EMITTER_1L250 = 5,
            /** <code>Emitter_1RL138</code> (with ordinal 10) */
            EMITTER_1RL138 = 10,
            /** <code>Emitter_1226_DECCA_MIL</code> (with ordinal 45) */
            EMITTER_1226_DECCA_MIL = 45,
            /** <code>Emitter_9B-1348</code> (with ordinal 46) */
            EMITTER_9B_1348 = 46,
            /** <code>Emitter_3KM6</code> (with ordinal 47) */
            EMITTER_3KM6 = 47,
            /** <code>Emitter_9KR400</code> (with ordinal 48) */
            EMITTER_9KR400 = 48,
            /** <code>Emitter_9GR400</code> (with ordinal 80) */
            EMITTER_9GR400 = 80,
            /** <code>Emitter_9GR600</code> (with ordinal 90) */
            EMITTER_9GR600 = 90,
            /** <code>Emitter_9LV_200_TA</code> (with ordinal 135) */
            EMITTER_9LV_200_TA = 135,
            /** <code>Emitter_9LV_200_TV</code> (with ordinal 180) */
            EMITTER_9LV_200_TV = 180,
            /** <code>Emitter_9LV200TT</code> (with ordinal 181) */
            EMITTER_9LV200TT = 181,
            /** <code>A310Z</code> (with ordinal 225) */
            A310Z = 225,
            /** <code>A325A</code> (with ordinal 270) */
            A325A = 270,
            /** <code>A346Z</code> (with ordinal 315) */
            A346Z = 315,
            /** <code>A353B</code> (with ordinal 360) */
            A353B = 360,
            /** <code>A372A</code> (with ordinal 405) */
            A372A = 405,
            /** <code>A372B</code> (with ordinal 450) */
            A372B = 450,
            /** <code>A372C</code> (with ordinal 495) */
            A372C = 495,
            /** <code>A377A</code> (with ordinal 540) */
            A377A = 540,
            /** <code>A377B</code> (with ordinal 585) */
            A377B = 585,
            /** <code>A380Z</code> (with ordinal 630) */
            A380Z = 630,
            /** <code>A381Z</code> (with ordinal 675) */
            A381Z = 675,
            /** <code>A398Z</code> (with ordinal 720) */
            A398Z = 720,
            /** <code>A403Z</code> (with ordinal 765) */
            A403Z = 765,
            /** <code>A409A</code> (with ordinal 810) */
            A409A = 810,
            /** <code>A418A</code> (with ordinal 855) */
            A418A = 855,
            /** <code>A419Z</code> (with ordinal 900) */
            A419Z = 900,
            /** <code>A429Z</code> (with ordinal 945) */
            A429Z = 945,
            /** <code>A432Z</code> (with ordinal 990) */
            A432Z = 990,
            /** <code>A434Z</code> (with ordinal 1035) */
            A434Z = 1035,
            /** <code>A401A</code> (with ordinal 1080) */
            A401A = 1080,
            /** <code>AA-12_Seeker</code> (with ordinal 1095) */
            AA_12_SEEKER = 1095,
            /** <code>AD4A</code> (with ordinal 1096) */
            AD4A = 1096,
            /** <code>ADES</code> (with ordinal 1097) */
            ADES = 1097,
            /** <code>Agave</code> (with ordinal 1100) */
            AGAVE = 1100,
            /** <code>AGRION_15</code> (with ordinal 1125) */
            AGRION_15 = 1125,
            /** <code>AI_MK_23</code> (with ordinal 1170) */
            AI_MK_23 = 1170,
            /** <code>AIDA_II</code> (with ordinal 1215) */
            AIDA_II = 1215,
            /** <code>AIM-120A</code> (with ordinal 1216) */
            AIM_120A = 1216,
            /** <code>Albatros_MK2</code> (with ordinal 1260) */
            ALBATROS_MK2 = 1260,
            /** <code>WGU-16_B</code> (with ordinal 1270) */
            WGU_16_B = 1270,
            /** <code>BoxSpring</code> (with ordinal 1280) */
            BOX_SPRING = 1280,
            /** <code>BoxSpringB</code> (with ordinal 1282) */
            BOX_SPRING_B = 1282,
            /** <code>ANA_SPS_502</code> (with ordinal 1305) */
            ANA_SPS_502 = 1305,
            /** <code>ANRITSU_Electric_AR-30A</code> (with ordinal 1350) */
            ANRITSU_ELECTRIC_AR_30A = 1350,
            /** <code>Antilope_V</code> (with ordinal 1395) */
            ANTILOPE_V = 1395,
            /** <code>AN_ADM-160</code> (with ordinal 1398) */
            AN_ADM_160 = 1398,
            /** <code>AN_ALE-50</code> (with ordinal 1400) */
            AN_ALE_50 = 1400,
            /** <code>AN_ALQ-76</code> (with ordinal 1410) */
            AN_ALQ_76 = 1410,
            /** <code>AN_ALQ_99</code> (with ordinal 1440) */
            AN_ALQ_99 = 1440,
            /** <code>AN_ALQ-100</code> (with ordinal 1485) */
            AN_ALQ_100 = 1485,
            /** <code>AN_ALQ-101</code> (with ordinal 1530) */
            AN_ALQ_101 = 1530,
            /** <code>AN_ALQ-119</code> (with ordinal 1575) */
            AN_ALQ_119 = 1575,
            /** <code>AN_ALQ-122</code> (with ordinal 1585) */
            AN_ALQ_122 = 1585,
            /** <code>AN_ALQ-126A</code> (with ordinal 1620) */
            AN_ALQ_126A = 1620,
            /** <code>AN_ALQ-128</code> (with ordinal 1621) */
            AN_ALQ_128 = 1621,
            /** <code>AN_ALQ-126B</code> (with ordinal 1622) */
            AN_ALQ_126B = 1622,
            /** <code>AN_ALQ-131</code> (with ordinal 1626) */
            AN_ALQ_131 = 1626,
            /** <code>AN_ALQ-135C_D</code> (with ordinal 1628) */
            AN_ALQ_135C_D = 1628,
            /** <code>AN_ALQ-144A_V_3</code> (with ordinal 1630) */
            AN_ALQ_144A_V_3 = 1630,
            /** <code>AN_ALQ-153</code> (with ordinal 1632) */
            AN_ALQ_153 = 1632,
            /** <code>AN_ALQ-155</code> (with ordinal 1634) */
            AN_ALQ_155 = 1634,
            /** <code>AN_ALQ-161_A</code> (with ordinal 1636) */
            AN_ALQ_161_A = 1636,
            /** <code>AN_ALQ-162</code> (with ordinal 1638) */
            AN_ALQ_162 = 1638,
            /** <code>AN_ALQ-164</code> (with ordinal 1639) */
            AN_ALQ_164 = 1639,
            /** <code>AN_ALQ-165</code> (with ordinal 1640) */
            AN_ALQ_165 = 1640,
            /** <code>AN_ALQ-167</code> (with ordinal 1642) */
            AN_ALQ_167 = 1642,
            /** <code>AN_ALQ-172_V_1</code> (with ordinal 1643) */
            AN_ALQ_172_V_1 = 1643,
            /** <code>AN_ALQ-172_V_2</code> (with ordinal 1644) */
            AN_ALQ_172_V_2 = 1644,
            /** <code>AN_ALQ-172_V_3</code> (with ordinal 1645) */
            AN_ALQ_172_V_3 = 1645,
            /** <code>AN_ALQ-176</code> (with ordinal 1646) */
            AN_ALQ_176 = 1646,
            /** <code>AN_ALQ-178</code> (with ordinal 1647) */
            AN_ALQ_178 = 1647,
            /** <code>AN_ALQ-184</code> (with ordinal 1648) */
            AN_ALQ_184 = 1648,
            /** <code>AN_ALQ-184_V_9</code> (with ordinal 1649) */
            AN_ALQ_184_V_9 = 1649,
            /** <code>AN_ALQ-188</code> (with ordinal 1650) */
            AN_ALQ_188 = 1650,
            /** <code>AN_ALQ-214</code> (with ordinal 1651) */
            AN_ALQ_214 = 1651,
            /** <code>AN_ALR-56</code> (with ordinal 1652) */
            AN_ALR_56 = 1652,
            /** <code>AN_ALQ-221</code> (with ordinal 1653) */
            AN_ALQ_221 = 1653,
            /** <code>AN_ALR-69</code> (with ordinal 1654) */
            AN_ALR_69 = 1654,
            /** <code>AN_ALQ-211_V_</code> (with ordinal 1655) */
            AN_ALQ_211_V = 1655,
            /** <code>AN_ALT-16A</code> (with ordinal 1656) */
            AN_ALT_16A = 1656,
            /** <code>AN_ALT-28</code> (with ordinal 1658) */
            AN_ALT_28 = 1658,
            /** <code>AN_ALT-32A</code> (with ordinal 1660) */
            AN_ALT_32A = 1660,
            /** <code>AN_APD_10</code> (with ordinal 1665) */
            AN_APD_10 = 1665,
            /** <code>AN_APG-50</code> (with ordinal 1700) */
            AN_APG_50 = 1700,
            /** <code>AN_APG_53</code> (with ordinal 1710) */
            AN_APG_53 = 1710,
            /** <code>AN_APG_59</code> (with ordinal 1755) */
            AN_APG_59 = 1755,
            /** <code>AN_APG-63AB</code> (with ordinal 1800) */
            AN_APG_63AB = 1800,
            /** <code>AN_APG-63C</code> (with ordinal 1805) */
            AN_APG_63C = 1805,
            /** <code>AN_APG-63_V_2</code> (with ordinal 1807) */
            AN_APG_63_V_2 = 1807,
            /** <code>AN_APG-63_V_3</code> (with ordinal 1809) */
            AN_APG_63_V_3 = 1809,
            /** <code>AN_APG_65</code> (with ordinal 1845) */
            AN_APG_65 = 1845,
            /** <code>AN_APG-66</code> (with ordinal 1870) */
            AN_APG_66 = 1870,
            /** <code>AN_APG-67</code> (with ordinal 1880) */
            AN_APG_67 = 1880,
            /** <code>AN_APG_68</code> (with ordinal 1890) */
            AN_APG_68 = 1890,
            /** <code>AN_APG_70</code> (with ordinal 1935) */
            AN_APG_70 = 1935,
            /** <code>AN_APG-73</code> (with ordinal 1945) */
            AN_APG_73 = 1945,
            /** <code>AN_APG-77</code> (with ordinal 1960) */
            AN_APG_77 = 1960,
            /** <code>AN_APG-78</code> (with ordinal 1970) */
            AN_APG_78 = 1970,
            /** <code>AN_APG-79</code> (with ordinal 1971) */
            AN_APG_79 = 1971,
            /** <code>AN_APG-80</code> (with ordinal 1972) */
            AN_APG_80 = 1972,
            /** <code>AN_APG-81</code> (with ordinal 1974) */
            AN_APG_81 = 1974,
            /** <code>AN_APG-502</code> (with ordinal 1980) */
            AN_APG_502 = 1980,
            /** <code>AN_APN-1</code> (with ordinal 2025) */
            AN_APN_1 = 2025,
            /** <code>AN_APN-22</code> (with ordinal 2070) */
            AN_APN_22 = 2070,
            /** <code>AN_APN_59</code> (with ordinal 2115) */
            AN_APN_59 = 2115,
            /** <code>AN_APN-69</code> (with ordinal 2160) */
            AN_APN_69 = 2160,
            /** <code>AN_APN-81</code> (with ordinal 2205) */
            AN_APN_81 = 2205,
            /** <code>AN_APN-117</code> (with ordinal 2250) */
            AN_APN_117 = 2250,
            /** <code>AN_APN-118</code> (with ordinal 2295) */
            AN_APN_118 = 2295,
            /** <code>AN_APN-130</code> (with ordinal 2340) */
            AN_APN_130 = 2340,
            /** <code>AN_APN-131</code> (with ordinal 2385) */
            AN_APN_131 = 2385,
            /** <code>AN_APN-133</code> (with ordinal 2430) */
            AN_APN_133 = 2430,
            /** <code>AN_APN-134</code> (with ordinal 2475) */
            AN_APN_134 = 2475,
            /** <code>AN_APN-141_V_</code> (with ordinal 2476) */
            AN_APN_141_V = 2476,
            /** <code>AN_APN-147</code> (with ordinal 2520) */
            AN_APN_147 = 2520,
            /** <code>AN_APN-150</code> (with ordinal 2565) */
            AN_APN_150 = 2565,
            /** <code>AN_APN-153</code> (with ordinal 2610) */
            AN_APN_153 = 2610,
            /** <code>AN_APN_154</code> (with ordinal 2655) */
            AN_APN_154 = 2655,
            /** <code>AN_APN-155</code> (with ordinal 2700) */
            AN_APN_155 = 2700,
            /** <code>AN_APN-159</code> (with ordinal 2745) */
            AN_APN_159 = 2745,
            /** <code>AN_APN-177</code> (with ordinal 2746) */
            AN_APN_177 = 2746,
            /** <code>AN_APN-182</code> (with ordinal 2790) */
            AN_APN_182 = 2790,
            /** <code>AN_APN-187</code> (with ordinal 2835) */
            AN_APN_187 = 2835,
            /** <code>AN_APN-190</code> (with ordinal 2880) */
            AN_APN_190 = 2880,
            /** <code>AN_APN_194</code> (with ordinal 2925) */
            AN_APN_194 = 2925,
            /** <code>AN_APN-195</code> (with ordinal 2970) */
            AN_APN_195 = 2970,
            /** <code>AN_APN-198</code> (with ordinal 3015) */
            AN_APN_198 = 3015,
            /** <code>AN_APN-200</code> (with ordinal 3060) */
            AN_APN_200 = 3060,
            /** <code>AN_APN_202</code> (with ordinal 3105) */
            AN_APN_202 = 3105,
            /** <code>AN_APN-215</code> (with ordinal 3106) */
            AN_APN_215 = 3106,
            /** <code>AN_APN-209</code> (with ordinal 3120) */
            AN_APN_209 = 3120,
            /** <code>AN_APN-217</code> (with ordinal 3150) */
            AN_APN_217 = 3150,
            /** <code>AN_APN-218</code> (with ordinal 3152) */
            AN_APN_218 = 3152,
            /** <code>AN_APN-224</code> (with ordinal 3153) */
            AN_APN_224 = 3153,
            /** <code>AN_APN-227</code> (with ordinal 3154) */
            AN_APN_227 = 3154,
            /** <code>AN_APN-230</code> (with ordinal 3155) */
            AN_APN_230 = 3155,
            /** <code>AN_APN-232</code> (with ordinal 3156) */
            AN_APN_232 = 3156,
            /** <code>AN_APN-237A</code> (with ordinal 3157) */
            AN_APN_237A = 3157,
            /** <code>AN_APN-234</code> (with ordinal 3158) */
            AN_APN_234 = 3158,
            /** <code>AN_APN-235</code> (with ordinal 3159) */
            AN_APN_235 = 3159,
            /** <code>AN_APN-238</code> (with ordinal 3160) */
            AN_APN_238 = 3160,
            /** <code>AN_APN-222</code> (with ordinal 3161) */
            AN_APN_222 = 3161,
            /** <code>AN_APN-239</code> (with ordinal 3162) */
            AN_APN_239 = 3162,
            /** <code>AN_APN-241</code> (with ordinal 3164) */
            AN_APN_241 = 3164,
            /** <code>AN_APN-242</code> (with ordinal 3166) */
            AN_APN_242 = 3166,
            /** <code>AN_APN-243</code> (with ordinal 3170) */
            AN_APN_243 = 3170,
            /** <code>AN_APN-506</code> (with ordinal 3195) */
            AN_APN_506 = 3195,
            /** <code>AN_APQ-72</code> (with ordinal 3240) */
            AN_APQ_72 = 3240,
            /** <code>AN_APQ-99</code> (with ordinal 3285) */
            AN_APQ_99 = 3285,
            /** <code>AN_APQ_100</code> (with ordinal 3330) */
            AN_APQ_100 = 3330,
            /** <code>AN_APQ-102</code> (with ordinal 3375) */
            AN_APQ_102 = 3375,
            /** <code>AN_APQ-107</code> (with ordinal 3376) */
            AN_APQ_107 = 3376,
            /** <code>AN_APQ-109</code> (with ordinal 3420) */
            AN_APQ_109 = 3420,
            /** <code>AN_APQ_113</code> (with ordinal 3465) */
            AN_APQ_113 = 3465,
            /** <code>AN_APQ_120</code> (with ordinal 3510) */
            AN_APQ_120 = 3510,
            /** <code>AN_APQ_126</code> (with ordinal 3555) */
            AN_APQ_126 = 3555,
            /** <code>AN_APQ-128</code> (with ordinal 3600) */
            AN_APQ_128 = 3600,
            /** <code>AN_APQ-129</code> (with ordinal 3645) */
            AN_APQ_129 = 3645,
            /** <code>AN_APQ_148</code> (with ordinal 3690) */
            AN_APQ_148 = 3690,
            /** <code>AN_APQ-153</code> (with ordinal 3735) */
            AN_APQ_153 = 3735,
            /** <code>AN_APQ-155</code> (with ordinal 3770) */
            AN_APQ_155 = 3770,
            /** <code>AN_APQ_159</code> (with ordinal 3780) */
            AN_APQ_159 = 3780,
            /** <code>AN_APQ-164</code> (with ordinal 3785) */
            AN_APQ_164 = 3785,
            /** <code>AN_APQ-166</code> (with ordinal 3788) */
            AN_APQ_166 = 3788,
            /** <code>AN_APQ-180</code> (with ordinal 3794) */
            AN_APQ_180 = 3794,
            /** <code>AN_APQ-181</code> (with ordinal 3795) */
            AN_APQ_181 = 3795,
            /** <code>AN_APS-31</code> (with ordinal 3820) */
            AN_APS_31 = 3820,
            /** <code>AN_APS-42</code> (with ordinal 3825) */
            AN_APS_42 = 3825,
            /** <code>AN_APS_80</code> (with ordinal 3870) */
            AN_APS_80 = 3870,
            /** <code>AN_APS-88</code> (with ordinal 3915) */
            AN_APS_88 = 3915,
            /** <code>AN_APS-88A</code> (with ordinal 3916) */
            AN_APS_88A = 3916,
            /** <code>AN_APS_115</code> (with ordinal 3960) */
            AN_APS_115 = 3960,
            /** <code>AN_APS_116</code> (with ordinal 4005) */
            AN_APS_116 = 4005,
            /** <code>AN_APS-120</code> (with ordinal 4050) */
            AN_APS_120 = 4050,
            /** <code>AN_APS_121</code> (with ordinal 4095) */
            AN_APS_121 = 4095,
            /** <code>AN_APS_124</code> (with ordinal 4140) */
            AN_APS_124 = 4140,
            /** <code>AN_APS_125</code> (with ordinal 4185) */
            AN_APS_125 = 4185,
            /** <code>AN_APS-128</code> (with ordinal 4230) */
            AN_APS_128 = 4230,
            /** <code>AN_APS_130</code> (with ordinal 4275) */
            AN_APS_130 = 4275,
            /** <code>AN_APS_133</code> (with ordinal 4320) */
            AN_APS_133 = 4320,
            /** <code>AN_APS-134</code> (with ordinal 4365) */
            AN_APS_134 = 4365,
            /** <code>AN_APS_137</code> (with ordinal 4410) */
            AN_APS_137 = 4410,
            /** <code>AN_APS-138</code> (with ordinal 4455) */
            AN_APS_138 = 4455,
            /** <code>AN_APS-143__V__1</code> (with ordinal 4465) */
            AN_APS_143_V_1 = 4465,
            /** <code>AN_APS-143_V_3</code> (with ordinal 4467) */
            AN_APS_143_V_3 = 4467,
            /** <code>AN_APS-143B_V_3</code> (with ordinal 4468) */
            AN_APS_143B_V_3 = 4468,
            /** <code>AN_APS-153</code> (with ordinal 4475) */
            AN_APS_153 = 4475,
            /** <code>AN_APS-150</code> (with ordinal 4480) */
            AN_APS_150 = 4480,
            /** <code>AN_APS-145</code> (with ordinal 4482) */
            AN_APS_145 = 4482,
            /** <code>AN_APS-504</code> (with ordinal 4490) */
            AN_APS_504 = 4490,
            /** <code>AN_APW_22</code> (with ordinal 4500) */
            AN_APW_22 = 4500,
            /** <code>AN_APW_23</code> (with ordinal 4545) */
            AN_APW_23 = 4545,
            /** <code>AN_APX-6</code> (with ordinal 4590) */
            AN_APX_6 = 4590,
            /** <code>AN_APX_7</code> (with ordinal 4635) */
            AN_APX_7 = 4635,
            /** <code>AN_APX_39</code> (with ordinal 4680) */
            AN_APX_39 = 4680,
            /** <code>AN_APX-64_V_</code> (with ordinal 4681) */
            AN_APX_64_V = 4681,
            /** <code>AN_APX-72</code> (with ordinal 4725) */
            AN_APX_72 = 4725,
            /** <code>AN_APX_76</code> (with ordinal 4770) */
            AN_APX_76 = 4770,
            /** <code>AN_APX_78</code> (with ordinal 4815) */
            AN_APX_78 = 4815,
            /** <code>AN_APX-100</code> (with ordinal 4816) */
            AN_APX_100 = 4816,
            /** <code>AN_APX_101</code> (with ordinal 4860) */
            AN_APX_101 = 4860,
            /** <code>AN_APX-113_AIFF</code> (with ordinal 4870) */
            AN_APX_113_AIFF = 4870,
            /** <code>AN_APY-1</code> (with ordinal 4900) */
            AN_APY_1 = 4900,
            /** <code>AN_APY_2</code> (with ordinal 4905) */
            AN_APY_2 = 4905,
            /** <code>AN_APY_3</code> (with ordinal 4950) */
            AN_APY_3 = 4950,
            /** <code>AN_APY-7</code> (with ordinal 4952) */
            AN_APY_7 = 4952,
            /** <code>AN_APY-8</code> (with ordinal 4953) */
            AN_APY_8 = 4953,
            /** <code>AN_APY-9</code> (with ordinal 4954) */
            AN_APY_9 = 4954,
            /** <code>AN_APY-10</code> (with ordinal 4955) */
            AN_APY_10 = 4955,
            /** <code>AN_ARN_21</code> (with ordinal 4995) */
            AN_ARN_21 = 4995,
            /** <code>AN_ARN_52</code> (with ordinal 5040) */
            AN_ARN_52 = 5040,
            /** <code>AN_ARN_84</code> (with ordinal 5085) */
            AN_ARN_84 = 5085,
            /** <code>AN_ARN_118</code> (with ordinal 5130) */
            AN_ARN_118 = 5130,
            /** <code>AN_ARN-153_V_</code> (with ordinal 5131) */
            AN_ARN_153_V = 5131,
            /** <code>AN_ARN-153</code> (with ordinal 5165) */
            AN_ARN_153 = 5165,
            /** <code>AN_ARW_73</code> (with ordinal 5175) */
            AN_ARW_73 = 5175,
            /** <code>AN_ASB_1</code> (with ordinal 5220) */
            AN_ASB_1 = 5220,
            /** <code>AN_ASG_21</code> (with ordinal 5265) */
            AN_ASG_21 = 5265,
            /** <code>AN_ASN-137</code> (with ordinal 5266) */
            AN_ASN_137 = 5266,
            /** <code>AN_ASQ-108</code> (with ordinal 5280) */
            AN_ASQ_108 = 5280,
            /** <code>AN_AWG_9</code> (with ordinal 5310) */
            AN_AWG_9 = 5310,
            /** <code>AN_BPS-9</code> (with ordinal 5355) */
            AN_BPS_9 = 5355,
            /** <code>AN_BPS_15</code> (with ordinal 5400) */
            AN_BPS_15 = 5400,
            /** <code>AN_BPS-15H</code> (with ordinal 5401) */
            AN_BPS_15H = 5401,
            /** <code>AN_BPS-16</code> (with ordinal 5405) */
            AN_BPS_16 = 5405,
            /** <code>AN_CRM-30</code> (with ordinal 5420) */
            AN_CRM_30 = 5420,
            /** <code>AN_DPW-23</code> (with ordinal 5430) */
            AN_DPW_23 = 5430,
            /** <code>AN_DSQ_26_Phoenix_MH</code> (with ordinal 5445) */
            AN_DSQ_26_PHOENIX_MH = 5445,
            /** <code>AN_DSQ_28_Harpoon_MH</code> (with ordinal 5490) */
            AN_DSQ_28_HARPOON_MH = 5490,
            /** <code>AN_FPN-40</code> (with ordinal 5495) */
            AN_FPN_40 = 5495,
            /** <code>AN_FPN-62</code> (with ordinal 5500) */
            AN_FPN_62 = 5500,
            /** <code>AN_FPS-16</code> (with ordinal 5505) */
            AN_FPS_16 = 5505,
            /** <code>AN_FPS-18</code> (with ordinal 5507) */
            AN_FPS_18 = 5507,
            /** <code>AN_FPS-89</code> (with ordinal 5508) */
            AN_FPS_89 = 5508,
            /** <code>AN_FPS-117</code> (with ordinal 5510) */
            AN_FPS_117 = 5510,
            /** <code>AN_FPS-20R</code> (with ordinal 5515) */
            AN_FPS_20R = 5515,
            /** <code>AN_FPS-77</code> (with ordinal 5520) */
            AN_FPS_77 = 5520,
            /** <code>AN_FPS-103</code> (with ordinal 5525) */
            AN_FPS_103 = 5525,
            /** <code>AN_GPN-12</code> (with ordinal 5527) */
            AN_GPN_12 = 5527,
            /** <code>AN_GPX-6</code> (with ordinal 5530) */
            AN_GPX_6 = 5530,
            /** <code>AN_GPX_8</code> (with ordinal 5535) */
            AN_GPX_8 = 5535,
            /** <code>AN_GRN-12</code> (with ordinal 5537) */
            AN_GRN_12 = 5537,
            /** <code>AN_MPN-14</code> (with ordinal 5539) */
            AN_MPN_14 = 5539,
            /** <code>AN_MPQ-10</code> (with ordinal 5540) */
            AN_MPQ_10 = 5540,
            /** <code>AN_MPQ-46__HPI__ILL</code> (with ordinal 5545) */
            AN_MPQ_46_HPI_ILL = 5545,
            /** <code>AN_MPQ-48_55_CWAR</code> (with ordinal 5550) */
            AN_MPQ_48_55_CWAR = 5550,
            /** <code>AN_MPQ-49</code> (with ordinal 5551) */
            AN_MPQ_49 = 5551,
            /** <code>AN_MPQ-50__PAR__TA</code> (with ordinal 5555) */
            AN_MPQ_50_PAR_TA = 5555,
            /** <code>AN_MPQ-51__ROR__TT</code> (with ordinal 5560) */
            AN_MPQ_51_ROR_TT = 5560,
            /** <code>AN_MPQ-53</code> (with ordinal 5570) */
            AN_MPQ_53 = 5570,
            /** <code>AN_MPQ-63</code> (with ordinal 5571) */
            AN_MPQ_63 = 5571,
            /** <code>AN_MPQ-64</code> (with ordinal 5575) */
            AN_MPQ_64 = 5575,
            /** <code>AN_SLQ-32</code> (with ordinal 5576) */
            AN_SLQ_32 = 5576,
            /** <code>AN_SLQ-32_V_4</code> (with ordinal 5578) */
            AN_SLQ_32_V_4 = 5578,
            /** <code>AN_SPG-34</code> (with ordinal 5580) */
            AN_SPG_34 = 5580,
            /** <code>AN_SPG_50</code> (with ordinal 5625) */
            AN_SPG_50 = 5625,
            /** <code>AN_SPG_51</code> (with ordinal 5670) */
            AN_SPG_51 = 5670,
            /** <code>AN_SPG-51_CWI_TI</code> (with ordinal 5715) */
            AN_SPG_51_CWI_TI = 5715,
            /** <code>AN_SPG-51_FC</code> (with ordinal 5760) */
            AN_SPG_51_FC = 5760,
            /** <code>AN_SPG-51C_D</code> (with ordinal 5761) */
            AN_SPG_51C_D = 5761,
            /** <code>AN_SPG_52</code> (with ordinal 5805) */
            AN_SPG_52 = 5805,
            /** <code>AN_SPG-53</code> (with ordinal 5850) */
            AN_SPG_53 = 5850,
            /** <code>AN_SPG_55B</code> (with ordinal 5895) */
            AN_SPG_55B = 5895,
            /** <code>AN_SPG_60</code> (with ordinal 5940) */
            AN_SPG_60 = 5940,
            /** <code>AN_SPG_62</code> (with ordinal 5985) */
            AN_SPG_62 = 5985,
            /** <code>AN_SPN-11</code> (with ordinal 6025) */
            AN_SPN_11 = 6025,
            /** <code>AN_SPN_35</code> (with ordinal 6030) */
            AN_SPN_35 = 6030,
            /** <code>AN_SPN-41</code> (with ordinal 6050) */
            AN_SPN_41 = 6050,
            /** <code>AN_SPN_43</code> (with ordinal 6075) */
            AN_SPN_43 = 6075,
            /** <code>AN_SPN-43A</code> (with ordinal 6076) */
            AN_SPN_43A = 6076,
            /** <code>AN_SPN-46</code> (with ordinal 6085) */
            AN_SPN_46 = 6085,
            /** <code>AN_SPQ-2</code> (with ordinal 6120) */
            AN_SPQ_2 = 6120,
            /** <code>AN_SPQ_9</code> (with ordinal 6165) */
            AN_SPQ_9 = 6165,
            /** <code>AN_SPQ-9B</code> (with ordinal 6166) */
            AN_SPQ_9B = 6166,
            /** <code>AN_SPQ-34</code> (with ordinal 6190) */
            AN_SPQ_34 = 6190,
            /** <code>AN_SPS-4</code> (with ordinal 6210) */
            AN_SPS_4 = 6210,
            /** <code>AN_SPS-5</code> (with ordinal 6255) */
            AN_SPS_5 = 6255,
            /** <code>AN_SPS-5C</code> (with ordinal 6300) */
            AN_SPS_5C = 6300,
            /** <code>AN_SPS-6</code> (with ordinal 6345) */
            AN_SPS_6 = 6345,
            /** <code>AN_SPS_10</code> (with ordinal 6390) */
            AN_SPS_10 = 6390,
            /** <code>AN_SPS_21</code> (with ordinal 6435) */
            AN_SPS_21 = 6435,
            /** <code>AN_SPS-28</code> (with ordinal 6480) */
            AN_SPS_28 = 6480,
            /** <code>AN_SPS-37</code> (with ordinal 6525) */
            AN_SPS_37 = 6525,
            /** <code>AN_SPS-39A</code> (with ordinal 6570) */
            AN_SPS_39A = 6570,
            /** <code>AN_SPS_40</code> (with ordinal 6615) */
            AN_SPS_40 = 6615,
            /** <code>AN_SPS-41</code> (with ordinal 6660) */
            AN_SPS_41 = 6660,
            /** <code>AN_SPS_48</code> (with ordinal 6705) */
            AN_SPS_48 = 6705,
            /** <code>AN_SPS-48C</code> (with ordinal 6750) */
            AN_SPS_48C = 6750,
            /** <code>AN_SPS-48E</code> (with ordinal 6752) */
            AN_SPS_48E = 6752,
            /** <code>AN_SPS_49</code> (with ordinal 6795) */
            AN_SPS_49 = 6795,
            /** <code>AN_SPS-49_V_1</code> (with ordinal 6796) */
            AN_SPS_49_V_1 = 6796,
            /** <code>AN_SPS-49_V_2</code> (with ordinal 6797) */
            AN_SPS_49_V_2 = 6797,
            /** <code>AN_SPS-49_V_3</code> (with ordinal 6798) */
            AN_SPS_49_V_3 = 6798,
            /** <code>AN_SPS-49_V_4</code> (with ordinal 6799) */
            AN_SPS_49_V_4 = 6799,
            /** <code>AN_SPS-49_V_5</code> (with ordinal 6800) */
            AN_SPS_49_V_5 = 6800,
            /** <code>AN_SPS-49_V_6</code> (with ordinal 6801) */
            AN_SPS_49_V_6 = 6801,
            /** <code>AN_SPS-49_V_7</code> (with ordinal 6802) */
            AN_SPS_49_V_7 = 6802,
            /** <code>AN_SPS-49_V_8</code> (with ordinal 6803) */
            AN_SPS_49_V_8 = 6803,
            /** <code>AN_SPS-49A_V_1</code> (with ordinal 6804) */
            AN_SPS_49A_V_1 = 6804,
            /** <code>AN_SPS_52</code> (with ordinal 6840) */
            AN_SPS_52 = 6840,
            /** <code>AN_SPS_53</code> (with ordinal 6885) */
            AN_SPS_53 = 6885,
            /** <code>AN_SPS_55</code> (with ordinal 6930) */
            AN_SPS_55 = 6930,
            /** <code>AN_SPS-52C</code> (with ordinal 6945) */
            AN_SPS_52C = 6945,
            /** <code>AN_SPS-55CS</code> (with ordinal 6970) */
            AN_SPS_55CS = 6970,
            /** <code>AN_SPS-55_SS</code> (with ordinal 6975) */
            AN_SPS_55_SS = 6975,
            /** <code>AN_SPS-58</code> (with ordinal 7020) */
            AN_SPS_58 = 7020,
            /** <code>AN_SPS-58C</code> (with ordinal 7025) */
            AN_SPS_58C = 7025,
            /** <code>AN_SPS_59</code> (with ordinal 7065) */
            AN_SPS_59 = 7065,
            /** <code>AN_SPS_64</code> (with ordinal 7110) */
            AN_SPS_64 = 7110,
            /** <code>AN_SPS_65</code> (with ordinal 7155) */
            AN_SPS_65 = 7155,
            /** <code>AN_SPS-66</code> (with ordinal 7175) */
            AN_SPS_66 = 7175,
            /** <code>AN_SPS_67</code> (with ordinal 7200) */
            AN_SPS_67 = 7200,
            /** <code>AN_SPS-73_I_</code> (with ordinal 7201) */
            AN_SPS_73_I = 7201,
            /** <code>AN_SPS-69</code> (with ordinal 7210) */
            AN_SPS_69 = 7210,
            /** <code>AN_SPS-73</code> (with ordinal 7215) */
            AN_SPS_73 = 7215,
            /** <code>AN_SPS-88</code> (with ordinal 7225) */
            AN_SPS_88 = 7225,
            /** <code>AN_SPY_1</code> (with ordinal 7245) */
            AN_SPY_1 = 7245,
            /** <code>AN_SPY-1A</code> (with ordinal 7250) */
            AN_SPY_1A = 7250,
            /** <code>AN_SPY-1B</code> (with ordinal 7252) */
            AN_SPY_1B = 7252,
            /** <code>AN_SPY-1B_V_</code> (with ordinal 7253) */
            AN_SPY_1B_V = 7253,
            /** <code>AN_SPY-1D</code> (with ordinal 7260) */
            AN_SPY_1D = 7260,
            /** <code>AN_SPY-1D_V_</code> (with ordinal 7261) */
            AN_SPY_1D_V = 7261,
            /** <code>AN_SPY-1F</code> (with ordinal 7265) */
            AN_SPY_1F = 7265,
            /** <code>AN_TLQ-32ARMDecoy</code> (with ordinal 7269) */
            AN_TLQ_32ARMDECOY = 7269,
            /** <code>AN_TPN-17</code> (with ordinal 7270) */
            AN_TPN_17 = 7270,
            /** <code>AN_TPN-24</code> (with ordinal 7275) */
            AN_TPN_24 = 7275,
            /** <code>AN_TPQ-18</code> (with ordinal 7280) */
            AN_TPQ_18 = 7280,
            /** <code>AN_TPQ-36</code> (with ordinal 7295) */
            AN_TPQ_36 = 7295,
            /** <code>AN_TPQ-37</code> (with ordinal 7300) */
            AN_TPQ_37 = 7300,
            /** <code>AN_TPQ-38_V8_</code> (with ordinal 7301) */
            AN_TPQ_38_V8 = 7301,
            /** <code>AN_TPQ-47</code> (with ordinal 7303) */
            AN_TPQ_47 = 7303,
            /** <code>AN_TPS-43</code> (with ordinal 7305) */
            AN_TPS_43 = 7305,
            /** <code>AN_TPS-43E</code> (with ordinal 7310) */
            AN_TPS_43E = 7310,
            /** <code>AN_TPS-59</code> (with ordinal 7315) */
            AN_TPS_59 = 7315,
            /** <code>AN_TPS-63</code> (with ordinal 7320) */
            AN_TPS_63 = 7320,
            /** <code>AN_TPS-70__V__1</code> (with ordinal 7322) */
            AN_TPS_70_V_1 = 7322,
            /** <code>AN_TPS-75</code> (with ordinal 7325) */
            AN_TPS_75 = 7325,
            /** <code>AN_TPX-46_V_7</code> (with ordinal 7330) */
            AN_TPX_46_V_7 = 7330,
            /** <code>AN_TPY-2</code> (with ordinal 7333) */
            AN_TPY_2 = 7333,
            /** <code>AN_ULQ-6A</code> (with ordinal 7335) */
            AN_ULQ_6A = 7335,
            /** <code>AN_UPN_25</code> (with ordinal 7380) */
            AN_UPN_25 = 7380,
            /** <code>AN_UPS_1</code> (with ordinal 7425) */
            AN_UPS_1 = 7425,
            /** <code>AN_UPS-2</code> (with ordinal 7426) */
            AN_UPS_2 = 7426,
            /** <code>AN_UPX_1</code> (with ordinal 7470) */
            AN_UPX_1 = 7470,
            /** <code>AN_UPX_5</code> (with ordinal 7515) */
            AN_UPX_5 = 7515,
            /** <code>AN_UPX_11</code> (with ordinal 7560) */
            AN_UPX_11 = 7560,
            /** <code>AN_UPX_12</code> (with ordinal 7605) */
            AN_UPX_12 = 7605,
            /** <code>AN_UPX_17</code> (with ordinal 7650) */
            AN_UPX_17 = 7650,
            /** <code>AN_UPX_23</code> (with ordinal 7695) */
            AN_UPX_23 = 7695,
            /** <code>AN_VPS_2</code> (with ordinal 7740) */
            AN_VPS_2 = 7740,
            /** <code>APAR</code> (with ordinal 7765) */
            APAR = 7765,
            /** <code>Aparna</code> (with ordinal 7770) */
            APARNA = 7770,
            /** <code>Apelco_AD_7_7</code> (with ordinal 7785) */
            APELCO_AD_7_7 = 7785,
            /** <code>APG_71</code> (with ordinal 7830) */
            APG_71 = 7830,
            /** <code>APN_148</code> (with ordinal 7875) */
            APN_148 = 7875,
            /** <code>APN_227</code> (with ordinal 7920) */
            APN_227 = 7920,
            /** <code>APQ113</code> (with ordinal 7965) */
            APQ113 = 7965,
            /** <code>APQ120</code> (with ordinal 8010) */
            APQ120 = 8010,
            /** <code>APQ148</code> (with ordinal 8055) */
            APQ148 = 8055,
            /** <code>APS_504_V3</code> (with ordinal 8100) */
            APS_504_V3 = 8100,
            /** <code>AR3D</code> (with ordinal 8105) */
            AR3D = 8105,
            /** <code>PlesseyAR-5</code> (with ordinal 8112) */
            PLESSEY_AR_5 = 8112,
            /** <code>AR_320</code> (with ordinal 8115) */
            AR_320 = 8115,
            /** <code>AR327</code> (with ordinal 8120) */
            AR327 = 8120,
            /** <code>AR_M31</code> (with ordinal 8145) */
            AR_M31 = 8145,
            /** <code>ARI_5954</code> (with ordinal 8190) */
            ARI_5954 = 8190,
            /** <code>ARI_5955</code> (with ordinal 8235) */
            ARI_5955 = 8235,
            /** <code>ARI_5979</code> (with ordinal 8280) */
            ARI_5979 = 8280,
            /** <code>ARGSN-31</code> (with ordinal 8281) */
            ARGSN_31 = 8281,
            /** <code>ARINC_564_BNDX_KING_RDR_1E</code> (with ordinal 8325) */
            ARINC_564_BNDX_KING_RDR_1E = 8325,
            /** <code>ARINC_700_BNDX_KING_RDR_1E</code> (with ordinal 8370) */
            ARINC_700_BNDX_KING_RDR_1E = 8370,
            /** <code>ARK-1</code> (with ordinal 8375) */
            ARK_1 = 8375,
            /** <code>ARSR-3</code> (with ordinal 8380) */
            ARSR_3 = 8380,
            /** <code>ARSR-18</code> (with ordinal 8390) */
            ARSR_18 = 8390,
            /** <code>AS_2_Kipper</code> (with ordinal 8415) */
            AS_2_KIPPER = 8415,
            /** <code>AS_2_Kipper_MH</code> (with ordinal 8460) */
            AS_2_KIPPER_MH = 8460,
            /** <code>AS_4_Kitchen</code> (with ordinal 8505) */
            AS_4_KITCHEN = 8505,
            /** <code>AS_4_Kitchen_MH</code> (with ordinal 8550) */
            AS_4_KITCHEN_MH = 8550,
            /** <code>AS_5_Kelt_MH</code> (with ordinal 8595) */
            AS_5_KELT_MH = 8595,
            /** <code>AS_6_Kingfish_MH</code> (with ordinal 8640) */
            AS_6_KINGFISH_MH = 8640,
            /** <code>AS_7_Kerry</code> (with ordinal 8685) */
            AS_7_KERRY = 8685,
            /** <code>AS_7_Kerry_MG</code> (with ordinal 8730) */
            AS_7_KERRY_MG = 8730,
            /** <code>AS_15_KENT_altimeter</code> (with ordinal 8735) */
            AS_15_KENT_ALTIMETER = 8735,
            /** <code>Aspide_AAM_SAM_ILL</code> (with ordinal 8760) */
            ASPIDE_AAM_SAM_ILL = 8760,
            /** <code>ASR-4</code> (with ordinal 8772) */
            ASR_4 = 8772,
            /** <code>ASR_O</code> (with ordinal 8775) */
            ASR_O = 8775,
            /** <code>ASR-5</code> (with ordinal 8780) */
            ASR_5 = 8780,
            /** <code>ASR-7</code> (with ordinal 8782) */
            ASR_7 = 8782,
            /** <code>ASR-8</code> (with ordinal 8785) */
            ASR_8 = 8785,
            /** <code>ASR-9</code> (with ordinal 8790) */
            ASR_9 = 8790,
            /** <code>RaytheonASR-10SS</code> (with ordinal 8812) */
            RAYTHEON_ASR_10SS = 8812,
            /** <code>AT_2_Swatter_MG</code> (with ordinal 8820) */
            AT_2_SWATTER_MG = 8820,
            /** <code>ATCR-33</code> (with ordinal 8840) */
            ATCR_33 = 8840,
            /** <code>ATCR_33_K_M</code> (with ordinal 8845) */
            ATCR_33_K_M = 8845,
            /** <code>Atlas_Elektronk_TRS_N</code> (with ordinal 8865) */
            ATLAS_ELEKTRONK_TRS_N = 8865,
            /** <code>Atlas-9600M</code> (with ordinal 8867) */
            ATLAS_9600M = 8867,
            /** <code>ATLAS-9740VTS</code> (with ordinal 8870) */
            ATLAS_9740VTS = 8870,
            /** <code>AVG_65</code> (with ordinal 8910) */
            AVG_65 = 8910,
            /** <code>AVH_7</code> (with ordinal 8955) */
            AVH_7 = 8955,
            /** <code>Aviaconversia</code> (with ordinal 8990) */
            AVIACONVERSIA = 8990,
            /** <code>AviaconversiaIII</code> (with ordinal 8995) */
            AVIACONVERSIA_III = 8995,
            /** <code>AVQ_20</code> (with ordinal 9000) */
            AVQ_20 = 9000,
            /** <code>AVQ-21</code> (with ordinal 9005) */
            AVQ_21 = 9005,
            /** <code>AVQ30X</code> (with ordinal 9045) */
            AVQ30X = 9045,
            /** <code>AVQ-50__RCA_</code> (with ordinal 9075) */
            AVQ_50_RCA = 9075,
            /** <code>AVQ_70</code> (with ordinal 9090) */
            AVQ_70 = 9090,
            /** <code>AWS_5</code> (with ordinal 9135) */
            AWS_5 = 9135,
            /** <code>AWS_6</code> (with ordinal 9180) */
            AWS_6 = 9180,
            /** <code>AWS-6B_300</code> (with ordinal 9185) */
            AWS_6B_300 = 9185,
            /** <code>B597Z</code> (with ordinal 9200) */
            B597Z = 9200,
            /** <code>B636Z</code> (with ordinal 9205) */
            B636Z = 9205,
            /** <code>BackBoard</code> (with ordinal 9215) */
            BACK_BOARD = 9215,
            /** <code>Back_Net_A_B</code> (with ordinal 9225) */
            BACK_NET_A_B = 9225,
            /** <code>Back_Trap</code> (with ordinal 9270) */
            BACK_TRAP = 9270,
            /** <code>BAESystemsRT-1805_APN</code> (with ordinal 9280) */
            BAESYSTEMS_RT_1805_APN = 9280,
            /** <code>BALTYK</code> (with ordinal 9310) */
            BALTYK = 9310,
            /** <code>Ball_End</code> (with ordinal 9315) */
            BALL_END = 9315,
            /** <code>Ball_Gun</code> (with ordinal 9360) */
            BALL_GUN = 9360,
            /** <code>Band_Stand</code> (with ordinal 9405) */
            BAND_STAND = 9405,
            /** <code>Bar_Lock</code> (with ordinal 9450) */
            BAR_LOCK = 9450,
            /** <code>Bass_Tilt</code> (with ordinal 9495) */
            BASS_TILT = 9495,
            /** <code>Badger</code> (with ordinal 9505) */
            BADGER = 9505,
            /** <code>Beacon</code> (with ordinal 9540) */
            BEACON = 9540,
            /** <code>Bean_Sticks</code> (with ordinal 9585) */
            BEAN_STICKS = 9585,
            /** <code>Bee_Hind</code> (with ordinal 9630) */
            BEE_HIND = 9630,
            /** <code>Bell_Crown_A</code> (with ordinal 9640) */
            BELL_CROWN_A = 9640,
            /** <code>Bell_Crown_B</code> (with ordinal 9642) */
            BELL_CROWN_B = 9642,
            /** <code>BellSquat</code> (with ordinal 9643) */
            BELL_SQUAT = 9643,
            /** <code>BIG_BACK</code> (with ordinal 9645) */
            BIG_BACK = 9645,
            /** <code>BigBirdA_B_C</code> (with ordinal 9659) */
            BIG_BIRD_A_B_C = 9659,
            /** <code>Big_Bird</code> (with ordinal 9660) */
            BIG_BIRD = 9660,
            /** <code>BigBirdDMod</code> (with ordinal 9661) */
            BIG_BIRD_DMOD = 9661,
            /** <code>Big_Bulge</code> (with ordinal 9675) */
            BIG_BULGE = 9675,
            /** <code>Big_Bulge_A</code> (with ordinal 9720) */
            BIG_BULGE_A = 9720,
            /** <code>Big_Bulge_B</code> (with ordinal 9765) */
            BIG_BULGE_B = 9765,
            /** <code>SNAR-10</code> (with ordinal 9780) */
            SNAR_10 = 9780,
            /** <code>Big_Mesh</code> (with ordinal 9810) */
            BIG_MESH = 9810,
            /** <code>Big_Net</code> (with ordinal 9855) */
            BIG_NET = 9855,
            /** <code>Bill_Board</code> (with ordinal 9885) */
            BILL_BOARD = 9885,
            /** <code>Bill_Fold</code> (with ordinal 9900) */
            BILL_FOLD = 9900,
            /** <code>Blowpipe_MG</code> (with ordinal 9905) */
            BLOWPIPE_MG = 9905,
            /** <code>Blue_Fox</code> (with ordinal 9930) */
            BLUE_FOX = 9930,
            /** <code>Blue_Vixen</code> (with ordinal 9935) */
            BLUE_VIXEN = 9935,
            /** <code>Blue_Silk</code> (with ordinal 9945) */
            BLUE_SILK = 9945,
            /** <code>Blue_Parrot</code> (with ordinal 9990) */
            BLUE_PARROT = 9990,
            /** <code>Blue_Orchid</code> (with ordinal 10035) */
            BLUE_ORCHID = 10035,
            /** <code>BM_DJG-8715</code> (with ordinal 10057) */
            BM_DJG_8715 = 10057,
            /** <code>Boat_Sail</code> (with ordinal 10080) */
            BOAT_SAIL = 10080,
            /** <code>Bofors_Electronic_9LV_331</code> (with ordinal 10125) */
            BOFORS_ELECTRONIC_9LV_331 = 10125,
            /** <code>Bofors_Ericsson_Sea_Giraffe_50_HC</code> (with ordinal 10170) */
            BOFORS_ERICSSON_SEA_GIRAFFE_50_HC = 10170,
            /** <code>Bowl_Mesh</code> (with ordinal 10215) */
            BOWL_MESH = 10215,
            /** <code>Box_Brick</code> (with ordinal 10260) */
            BOX_BRICK = 10260,
            /** <code>Box_Tail</code> (with ordinal 10305) */
            BOX_TAIL = 10305,
            /** <code>BM_KG8601_8605_8606</code> (with ordinal 10315) */
            BM_KG8601_8605_8606 = 10315,
            /** <code>BPS_11A</code> (with ordinal 10350) */
            BPS_11A = 10350,
            /** <code>BPS_14</code> (with ordinal 10395) */
            BPS_14 = 10395,
            /** <code>BPS_15A</code> (with ordinal 10440) */
            BPS_15A = 10440,
            /** <code>BR-15_Tokyo_KEIKI</code> (with ordinal 10485) */
            BR_15_TOKYO_KEIKI = 10485,
            /** <code>BRIDGEMASTE</code> (with ordinal 10510) */
            BRIDGEMASTE = 10510,
            /** <code>Bread_Bin</code> (with ordinal 10530) */
            BREAD_BIN = 10530,
            /** <code>BT_271</code> (with ordinal 10575) */
            BT_271 = 10575,
            /** <code>BX_732</code> (with ordinal 10620) */
            BX_732 = 10620,
            /** <code>Buran-D</code> (with ordinal 10642) */
            BURAN_D = 10642,
            /** <code>Buzz_Stand</code> (with ordinal 10665) */
            BUZZ_STAND = 10665,
            /** <code>C_5A_Multi_Mode_Radar</code> (with ordinal 10710) */
            C_5A_MULTI_MODE_RADAR = 10710,
            /** <code>Caiman</code> (with ordinal 10755) */
            CAIMAN = 10755,
            /** <code>Cake_Stand</code> (with ordinal 10800) */
            CAKE_STAND = 10800,
            /** <code>Calypso_C61</code> (with ordinal 10845) */
            CALYPSO_C61 = 10845,
            /** <code>Calypso_Ii</code> (with ordinal 10890) */
            CALYPSO_II = 10890,
            /** <code>Cardion_Coastal</code> (with ordinal 10895) */
            CARDION_COASTAL = 10895,
            /** <code>Castor_Ii</code> (with ordinal 10935) */
            CASTOR_II = 10935,
            /** <code>Castor_2J_TT__Crotale_NG_</code> (with ordinal 10940) */
            CASTOR_2J_TT_CROTALE_NG = 10940,
            /** <code>Cat_House</code> (with ordinal 10980) */
            CAT_HOUSE = 10980,
            /** <code>CDR-431</code> (with ordinal 10985) */
            CDR_431 = 10985,
            /** <code>CH_SS-N-6</code> (with ordinal 10995) */
            CH_SS_N_6 = 10995,
            /** <code>Chair_Back_TT</code> (with ordinal 11000) */
            CHAIR_BACK_TT = 11000,
            /** <code>Chair_Back_ILL</code> (with ordinal 11010) */
            CHAIR_BACK_ILL = 11010,
            /** <code>LEMZ96L6</code> (with ordinal 11020) */
            LEMZ96L6 = 11020,
            /** <code>Cheese_Brick</code> (with ordinal 11025) */
            CHEESE_BRICK = 11025,
            /** <code>Clam_Pipe</code> (with ordinal 11070) */
            CLAM_PIPE = 11070,
            /** <code>Clamshell</code> (with ordinal 11115) */
            CLAMSHELL = 11115,
            /** <code>CoastalGiraffe</code> (with ordinal 11125) */
            COASTAL_GIRAFFE = 11125,
            /** <code>Colibri</code> (with ordinal 11137) */
            COLIBRI = 11137,
            /** <code>Collins_WXR-700X</code> (with ordinal 11160) */
            COLLINS_WXR_700X = 11160,
            /** <code>Collins_DN_101</code> (with ordinal 11205) */
            COLLINS_DN_101 = 11205,
            /** <code>Contraves_Sea_Hunter_MK_4</code> (with ordinal 11250) */
            CONTRAVES_SEA_HUNTER_MK_4 = 11250,
            /** <code>Corn_Can</code> (with ordinal 11260) */
            CORN_CAN = 11260,
            /** <code>CR-105_RMCA</code> (with ordinal 11270) */
            CR_105_RMCA = 11270,
            /** <code>Cross_Bird</code> (with ordinal 11295) */
            CROSS_BIRD = 11295,
            /** <code>Cross_Dome</code> (with ordinal 11340) */
            CROSS_DOME = 11340,
            /** <code>Cross_Legs</code> (with ordinal 11385) */
            CROSS_LEGS = 11385,
            /** <code>Cross_Out</code> (with ordinal 11430) */
            CROSS_OUT = 11430,
            /** <code>Cross_Slot</code> (with ordinal 11475) */
            CROSS_SLOT = 11475,
            /** <code>Cross_Sword</code> (with ordinal 11520) */
            CROSS_SWORD = 11520,
            /** <code>Cross_Up</code> (with ordinal 11565) */
            CROSS_UP = 11565,
            /** <code>Cross_Sword_FC</code> (with ordinal 11610) */
            CROSS_SWORD_FC = 11610,
            /** <code>Crotale_Acquisition_TA</code> (with ordinal 11655) */
            CROTALE_ACQUISITION_TA = 11655,
            /** <code>Crotale_NG_TA</code> (with ordinal 11660) */
            CROTALE_NG_TA = 11660,
            /** <code>Crotale_TT</code> (with ordinal 11665) */
            CROTALE_TT = 11665,
            /** <code>Crotale_MGMissile_System</code> (with ordinal 11700) */
            CROTALE_MGMISSILE_SYSTEM = 11700,
            /** <code>CS-10-TA</code> (with ordinal 11715) */
            CS_10_TA = 11715,
            /** <code>CSF-Varan</code> (with ordinal 11725) */
            CSF_VARAN = 11725,
            /** <code>CSS-N-4MH</code> (with ordinal 11735) */
            CSS_N_4MH = 11735,
            /** <code>CSS_C_3C_CAS_1M1_M2_MH</code> (with ordinal 11745) */
            CSS_C_3C_CAS_1M1_M2_MH = 11745,
            /** <code>CSS_C_2B_HY_1A_MH</code> (with ordinal 11790) */
            CSS_C_2B_HY_1A_MH = 11790,
            /** <code>CWS_2</code> (with ordinal 11835) */
            CWS_2 = 11835,
            /** <code>Cylinder_Head</code> (with ordinal 11880) */
            CYLINDER_HEAD = 11880,
            /** <code>Cymbeline</code> (with ordinal 11902) */
            CYMBELINE = 11902,
            /** <code>Cyrano_II</code> (with ordinal 11925) */
            CYRANO_II = 11925,
            /** <code>Cyrano_IV</code> (with ordinal 11970) */
            CYRANO_IV = 11970,
            /** <code>Cyrano_IV-M</code> (with ordinal 11975) */
            CYRANO_IV_M = 11975,
            /** <code>DA-01_00</code> (with ordinal 12010) */
            DA_01_00 = 12010,
            /** <code>DA_05_00</code> (with ordinal 12015) */
            DA_05_00 = 12015,
            /** <code>Dawn</code> (with ordinal 12060) */
            DAWN = 12060,
            /** <code>Dead_Duck</code> (with ordinal 12105) */
            DEAD_DUCK = 12105,
            /** <code>DECCA-20V90_9</code> (with ordinal 12110) */
            DECCA_20V90_9 = 12110,
            /** <code>DECCA-20V90S</code> (with ordinal 12111) */
            DECCA_20V90S = 12111,
            /** <code>DECCA_45</code> (with ordinal 12150) */
            DECCA_45 = 12150,
            /** <code>DECCA_50</code> (with ordinal 12195) */
            DECCA_50 = 12195,
            /** <code>DECCA71</code> (with ordinal 12196) */
            DECCA71 = 12196,
            /** <code>DECCA_110</code> (with ordinal 12240) */
            DECCA_110 = 12240,
            /** <code>DECCA_170</code> (with ordinal 12285) */
            DECCA_170 = 12285,
            /** <code>DECCAHF2</code> (with ordinal 12292) */
            DECCAHF2 = 12292,
            /** <code>DECCA_202</code> (with ordinal 12330) */
            DECCA_202 = 12330,
            /** <code>DECCA_D202</code> (with ordinal 12375) */
            DECCA_D202 = 12375,
            /** <code>DECCA_303</code> (with ordinal 12420) */
            DECCA_303 = 12420,
            /** <code>DECCA_535</code> (with ordinal 12430) */
            DECCA_535 = 12430,
            /** <code>DECCA_626</code> (with ordinal 12465) */
            DECCA_626 = 12465,
            /** <code>DECCA_629</code> (with ordinal 12510) */
            DECCA_629 = 12510,
            /** <code>DECCA_914</code> (with ordinal 12555) */
            DECCA_914 = 12555,
            /** <code>DECCA_916</code> (with ordinal 12600) */
            DECCA_916 = 12600,
            /** <code>DECCA_926</code> (with ordinal 12610) */
            DECCA_926 = 12610,
            /** <code>DECCA1070A</code> (with ordinal 12615) */
            DECCA1070A = 12615,
            /** <code>DECCA_1226_Commercial</code> (with ordinal 12645) */
            DECCA_1226_COMMERCIAL = 12645,
            /** <code>DECCA1290</code> (with ordinal 12655) */
            DECCA1290 = 12655,
            /** <code>DECCA_1626</code> (with ordinal 12690) */
            DECCA_1626 = 12690,
            /** <code>DECCA2070</code> (with ordinal 12691) */
            DECCA2070 = 12691,
            /** <code>DECCA_2459</code> (with ordinal 12735) */
            DECCA_2459 = 12735,
            /** <code>DECCA_AWS_1</code> (with ordinal 12780) */
            DECCA_AWS_1 = 12780,
            /** <code>DECCA_AWS_2</code> (with ordinal 12782) */
            DECCA_AWS_2 = 12782,
            /** <code>DECCA_AWS_4</code> (with ordinal 12785) */
            DECCA_AWS_4 = 12785,
            /** <code>DECCA_AWS-4__2_</code> (with ordinal 12787) */
            DECCA_AWS_4_2 = 12787,
            /** <code>DECCAMAR</code> (with ordinal 12800) */
            DECCAMAR = 12800,
            /** <code>DECCA_RM_326</code> (with ordinal 12805) */
            DECCA_RM_326 = 12805,
            /** <code>DECCA_RM_416</code> (with ordinal 12825) */
            DECCA_RM_416 = 12825,
            /** <code>DECCA_RM_914</code> (with ordinal 12870) */
            DECCA_RM_914 = 12870,
            /** <code>DECCA_RM_1690</code> (with ordinal 12915) */
            DECCA_RM_1690 = 12915,
            /** <code>DECCA_Super_101_MK_3</code> (with ordinal 12960) */
            DECCA_SUPER_101_MK_3 = 12960,
            /** <code>DISS_1</code> (with ordinal 13005) */
            DISS_1 = 13005,
            /** <code>DISS-7</code> (with ordinal 13006) */
            DISS_7 = 13006,
            /** <code>DISS-013</code> (with ordinal 13007) */
            DISS_013 = 13007,
            /** <code>Rapier_TTDN_181</code> (with ordinal 13050) */
            RAPIER_TTDN_181 = 13050,
            /** <code>Rapier_2000_TT</code> (with ordinal 13055) */
            RAPIER_2000_TT = 13055,
            /** <code>Dog_Ear</code> (with ordinal 13095) */
            DOG_EAR = 13095,
            /** <code>Dog_House</code> (with ordinal 13140) */
            DOG_HOUSE = 13140,
            /** <code>DM3</code> (with ordinal 13141) */
            DM3 = 13141,
            /** <code>DM-3B</code> (with ordinal 13142) */
            DM_3B = 13142,
            /** <code>DM-5</code> (with ordinal 13143) */
            DM_5 = 13143,
            /** <code>Don_2</code> (with ordinal 13185) */
            DON_2 = 13185,
            /** <code>Don_A_B_2_Kay</code> (with ordinal 13230) */
            DON_A_B_2_KAY = 13230,
            /** <code>Donets</code> (with ordinal 13275) */
            DONETS = 13275,
            /** <code>Down_Beat</code> (with ordinal 13320) */
            DOWN_BEAT = 13320,
            /** <code>DRAA_2A</code> (with ordinal 13365) */
            DRAA_2A = 13365,
            /** <code>DRAA_2B</code> (with ordinal 13410) */
            DRAA_2B = 13410,
            /** <code>DRAC_39</code> (with ordinal 13455) */
            DRAC_39 = 13455,
            /** <code>DragonEye</code> (with ordinal 13477) */
            DRAGON_EYE = 13477,
            /** <code>DRBC_30B</code> (with ordinal 13500) */
            DRBC_30B = 13500,
            /** <code>DRBC_31A</code> (with ordinal 13545) */
            DRBC_31A = 13545,
            /** <code>DRBC-32</code> (with ordinal 13585) */
            DRBC_32 = 13585,
            /** <code>DRBC_32A</code> (with ordinal 13590) */
            DRBC_32A = 13590,
            /** <code>DRBC_32D</code> (with ordinal 13635) */
            DRBC_32D = 13635,
            /** <code>DRBC_33A</code> (with ordinal 13680) */
            DRBC_33A = 13680,
            /** <code>DRBI_10</code> (with ordinal 13725) */
            DRBI_10 = 13725,
            /** <code>DRBI_23</code> (with ordinal 13770) */
            DRBI_23 = 13770,
            /** <code>DRBJ_11B</code> (with ordinal 13815) */
            DRBJ_11B = 13815,
            /** <code>DRBN_30</code> (with ordinal 13860) */
            DRBN_30 = 13860,
            /** <code>DRBN_32</code> (with ordinal 13905) */
            DRBN_32 = 13905,
            /** <code>DRBR_51</code> (with ordinal 13950) */
            DRBR_51 = 13950,
            /** <code>DRBV_20B</code> (with ordinal 13995) */
            DRBV_20B = 13995,
            /** <code>DRBV_22</code> (with ordinal 14040) */
            DRBV_22 = 14040,
            /** <code>DRBV_26C</code> (with ordinal 14085) */
            DRBV_26C = 14085,
            /** <code>DRBV_30</code> (with ordinal 14130) */
            DRBV_30 = 14130,
            /** <code>DRBV_50</code> (with ordinal 14175) */
            DRBV_50 = 14175,
            /** <code>DRBV_51</code> (with ordinal 14220) */
            DRBV_51 = 14220,
            /** <code>DRBV_51A</code> (with ordinal 14265) */
            DRBV_51A = 14265,
            /** <code>DRBV_51B</code> (with ordinal 14310) */
            DRBV_51B = 14310,
            /** <code>DRBV_51C</code> (with ordinal 14355) */
            DRBV_51C = 14355,
            /** <code>Drop_Kick</code> (with ordinal 14400) */
            DROP_KICK = 14400,
            /** <code>DRUA_31</code> (with ordinal 14445) */
            DRUA_31 = 14445,
            /** <code>Drum_Tilt</code> (with ordinal 14490) */
            DRUM_TILT = 14490,
            /** <code>Drum_Tilt_A</code> (with ordinal 14535) */
            DRUM_TILT_A = 14535,
            /** <code>Drum_Tilt_B</code> (with ordinal 14545) */
            DRUM_TILT_B = 14545,
            /** <code>Dumbo</code> (with ordinal 14580) */
            DUMBO = 14580,
            /** <code>EKCOE390</code> (with ordinal 14590) */
            EKCOE390 = 14590,
            /** <code>ECR-90</code> (with ordinal 14600) */
            ECR_90 = 14600,
            /** <code>Egg_Cup_A_B</code> (with ordinal 14625) */
            EGG_CUP_A_B = 14625,
            /** <code>EKCOE120</code> (with ordinal 14660) */
            EKCOE120 = 14660,
            /** <code>EKCO_190</code> (with ordinal 14670) */
            EKCO_190 = 14670,
            /** <code>EL_L-8222</code> (with ordinal 14710) */
            EL_L_8222 = 14710,
            /** <code>EL_M_2001B</code> (with ordinal 14715) */
            EL_M_2001B = 14715,
            /** <code>EL_M-2022</code> (with ordinal 14725) */
            EL_M_2022 = 14725,
            /** <code>EL_M-2200</code> (with ordinal 14750) */
            EL_M_2200 = 14750,
            /** <code>EL_M_2207</code> (with ordinal 14760) */
            EL_M_2207 = 14760,
            /** <code>EL_M2216_V_</code> (with ordinal 14770) */
            EL_M2216_V = 14770,
            /** <code>ELT-361</code> (with ordinal 14776) */
            ELT_361 = 14776,
            /** <code>ELT-572</code> (with ordinal 14785) */
            ELT_572 = 14785,
            /** <code>ELTA_EL_M_2221_GM_STGR</code> (with ordinal 14805) */
            ELTA_EL_M_2221_GM_STGR = 14805,
            /** <code>EL_M-2228S_3D</code> (with ordinal 14806) */
            EL_M_2228S_3D = 14806,
            /** <code>EL_M-2705</code> (with ordinal 14807) */
            EL_M_2705 = 14807,
            /** <code>ELTA_SIS</code> (with ordinal 14810) */
            ELTA_SIS = 14810,
            /** <code>EL_M-2238</code> (with ordinal 14811) */
            EL_M_2238 = 14811,
            /** <code>EMD_2900</code> (with ordinal 14850) */
            EMD_2900 = 14850,
            /** <code>End_Tray</code> (with ordinal 14895) */
            END_TRAY = 14895,
            /** <code>ESR1</code> (with ordinal 14900) */
            ESR1 = 14900,
            /** <code>ET-316</code> (with ordinal 14905) */
            ET_316 = 14905,
            /** <code>ExocetType</code> (with ordinal 14935) */
            EXOCET_TYPE = 14935,
            /** <code>Exocet_1</code> (with ordinal 14940) */
            EXOCET_1 = 14940,
            /** <code>Exocet_1_MH</code> (with ordinal 14985) */
            EXOCET_1_MH = 14985,
            /** <code>Exocet_2</code> (with ordinal 15030) */
            EXOCET_2 = 15030,
            /** <code>Eye_Bowl</code> (with ordinal 15075) */
            EYE_BOWL = 15075,
            /** <code>Eye_Shield</code> (with ordinal 15120) */
            EYE_SHIELD = 15120,
            /** <code>F332Z</code> (with ordinal 15140) */
            F332Z = 15140,
            /** <code>FalconClawTI</code> (with ordinal 15155) */
            FALCON_CLAW_TI = 15155,
            /** <code>FalconClawTT</code> (with ordinal 15156) */
            FALCON_CLAW_TT = 15156,
            /** <code>FALCON</code> (with ordinal 15160) */
            FALCON = 15160,
            /** <code>FALCON-G</code> (with ordinal 15161) */
            FALCON_G = 15161,
            /** <code>Fan_Song_A</code> (with ordinal 15165) */
            FAN_SONG_A = 15165,
            /** <code>Fan_Song_B_F_TA</code> (with ordinal 15200) */
            FAN_SONG_B_F_TA = 15200,
            /** <code>Fan_Song_B_F_TT</code> (with ordinal 15210) */
            FAN_SONG_B_F_TT = 15210,
            /** <code>Fan_Song_C_E_TA</code> (with ordinal 15220) */
            FAN_SONG_C_E_TA = 15220,
            /** <code>Fan_Song_C_E_TT</code> (with ordinal 15230) */
            FAN_SONG_C_E_TT = 15230,
            /** <code>Fan_Song_C_E_MG</code> (with ordinal 15240) */
            FAN_SONG_C_E_MG = 15240,
            /** <code>Fan_Song_B_FF_MG</code> (with ordinal 15255) */
            FAN_SONG_B_FF_MG = 15255,
            /** <code>Fan_Tail</code> (with ordinal 15300) */
            FAN_TAIL = 15300,
            /** <code>FB-7Radar</code> (with ordinal 15305) */
            FB_7RADAR = 15305,
            /** <code>FCR-1401</code> (with ordinal 15310) */
            FCR_1401 = 15310,
            /** <code>Fin_Curve</code> (with ordinal 15345) */
            FIN_CURVE = 15345,
            /** <code>Fire_Can</code> (with ordinal 15390) */
            FIRE_CAN = 15390,
            /** <code>Fire_Dish</code> (with ordinal 15435) */
            FIRE_DISH = 15435,
            /** <code>Fire_Dome_TA</code> (with ordinal 15470) */
            FIRE_DOME_TA = 15470,
            /** <code>Fire_Dome_TT</code> (with ordinal 15475) */
            FIRE_DOME_TT = 15475,
            /** <code>Fire_Dome_TI</code> (with ordinal 15480) */
            FIRE_DOME_TI = 15480,
            /** <code>Fire_Iron</code> (with ordinal 15525) */
            FIRE_IRON = 15525,
            /** <code>Fire_Wheel</code> (with ordinal 15570) */
            FIRE_WHEEL = 15570,
            /** <code>Fish_Bowl</code> (with ordinal 15615) */
            FISH_BOWL = 15615,
            /** <code>Flap_Lid</code> (with ordinal 15660) */
            FLAP_LID = 15660,
            /** <code>Flap_Truck</code> (with ordinal 15705) */
            FLAP_TRUCK = 15705,
            /** <code>Flap_Wheel</code> (with ordinal 15750) */
            FLAP_WHEEL = 15750,
            /** <code>Flash_Dance</code> (with ordinal 15795) */
            FLASH_DANCE = 15795,
            /** <code>Flat_Face_A_B_C_D</code> (with ordinal 15840) */
            FLAT_FACE_A_B_C_D = 15840,
            /** <code>FlatFaceE</code> (with ordinal 15842) */
            FLAT_FACE_E = 15842,
            /** <code>Flat_Screen</code> (with ordinal 15885) */
            FLAT_SCREEN = 15885,
            /** <code>Flat_Spin</code> (with ordinal 15930) */
            FLAT_SPIN = 15930,
            /** <code>Flat_Twin</code> (with ordinal 15975) */
            FLAT_TWIN = 15975,
            /** <code>FL-400</code> (with ordinal 15980) */
            FL_400 = 15980,
            /** <code>Fledermaus</code> (with ordinal 16020) */
            FLEDERMAUS = 16020,
            /** <code>FLYCATCHER</code> (with ordinal 16030) */
            FLYCATCHER = 16030,
            /** <code>Fly_Screen</code> (with ordinal 16065) */
            FLY_SCREEN = 16065,
            /** <code>Fly_Screen_A_B</code> (with ordinal 16110) */
            FLY_SCREEN_A_B = 16110,
            /** <code>Fly_Trap_B</code> (with ordinal 16155) */
            FLY_TRAP_B = 16155,
            /** <code>Fog_Lamp_MG</code> (with ordinal 16200) */
            FOG_LAMP_MG = 16200,
            /** <code>Fog_Lamp_TT</code> (with ordinal 16245) */
            FOG_LAMP_TT = 16245,
            /** <code>Foil_Two</code> (with ordinal 16290) */
            FOIL_TWO = 16290,
            /** <code>FootBall</code> (with ordinal 16300) */
            FOOT_BALL = 16300,
            /** <code>Fox_Hunter</code> (with ordinal 16335) */
            FOX_HUNTER = 16335,
            /** <code>FOX_FIREFox_Fire_AL</code> (with ordinal 16380) */
            FOX_FIREFOX_FIRE_AL = 16380,
            /** <code>FOX_FIRE_ILL</code> (with ordinal 16390) */
            FOX_FIRE_ILL = 16390,
            /** <code>FR-151A</code> (with ordinal 16400) */
            FR_151A = 16400,
            /** <code>FR-1505_DA</code> (with ordinal 16410) */
            FR_1505_DA = 16410,
            /** <code>FR-2000</code> (with ordinal 16420) */
            FR_2000 = 16420,
            /** <code>Furuno-2855W</code> (with ordinal 16421) */
            FURUNO_2855W = 16421,
            /** <code>Front_Dome</code> (with ordinal 16425) */
            FRONT_DOME = 16425,
            /** <code>Front_Door</code> (with ordinal 16470) */
            FRONT_DOOR = 16470,
            /** <code>Front_Piece</code> (with ordinal 16515) */
            FRONT_PIECE = 16515,
            /** <code>Furuno</code> (with ordinal 16560) */
            FURUNO = 16560,
            /** <code>Furuno1721</code> (with ordinal 16561) */
            FURUNO1721 = 16561,
            /** <code>Furuno1730</code> (with ordinal 16580) */
            FURUNO1730 = 16580,
            /** <code>Furuno1932</code> (with ordinal 16590) */
            FURUNO1932 = 16590,
            /** <code>Furuno_701</code> (with ordinal 16605) */
            FURUNO_701 = 16605,
            /** <code>Furuno1940</code> (with ordinal 16606) */
            FURUNO1940 = 16606,
            /** <code>Furuno_711_2</code> (with ordinal 16650) */
            FURUNO_711_2 = 16650,
            /** <code>Furuno240</code> (with ordinal 16690) */
            FURUNO240 = 16690,
            /** <code>Furuno_2400</code> (with ordinal 16695) */
            FURUNO_2400 = 16695,
            /** <code>Furuno8051</code> (with ordinal 16730) */
            FURUNO8051 = 16730,
            /** <code>G030A_APD-31_</code> (with ordinal 16735) */
            G030A_APD_31 = 16735,
            /** <code>GA_01_00</code> (with ordinal 16740) */
            GA_01_00 = 16740,
            /** <code>Gage</code> (with ordinal 16785) */
            GAGE = 16785,
            /** <code>Gardenia</code> (with ordinal 16800) */
            GARDENIA = 16800,
            /** <code>Garpin</code> (with ordinal 16830) */
            GARPIN = 16830,
            /** <code>GateGuard</code> (with ordinal 16833) */
            GATE_GUARD = 16833,
            /** <code>Garpun-Bal-E</code> (with ordinal 16835) */
            GARPUN_BAL_E = 16835,
            /** <code>GEM_BX_132</code> (with ordinal 16875) */
            GEM_BX_132 = 16875,
            /** <code>MPDR-12</code> (with ordinal 16880) */
            MPDR_12 = 16880,
            /** <code>Gepard_TT</code> (with ordinal 16884) */
            GEPARD_TT = 16884,
            /** <code>GERAN-F</code> (with ordinal 16888) */
            GERAN_F = 16888,
            /** <code>GIRAFFE</code> (with ordinal 16900) */
            GIRAFFE = 16900,
            /** <code>Gin_Sling_TA</code> (with ordinal 16915) */
            GIN_SLING_TA = 16915,
            /** <code>Gin_Sling</code> (with ordinal 16920) */
            GIN_SLING = 16920,
            /** <code>Gin_Sling_MG</code> (with ordinal 16925) */
            GIN_SLING_MG = 16925,
            /** <code>GoldenBar</code> (with ordinal 16931) */
            GOLDEN_BAR = 16931,
            /** <code>GoldenBat</code> (with ordinal 16932) */
            GOLDEN_BAT = 16932,
            /** <code>GoldenDome</code> (with ordinal 16935) */
            GOLDEN_DOME = 16935,
            /** <code>GoldenHeart</code> (with ordinal 16940) */
            GOLDEN_HEART = 16940,
            /** <code>GPN-22</code> (with ordinal 16945) */
            GPN_22 = 16945,
            /** <code>GRN-9</code> (with ordinal 16950) */
            GRN_9 = 16950,
            /** <code>GraveStone</code> (with ordinal 16960) */
            GRAVE_STONE = 16960,
            /** <code>Green_Stain</code> (with ordinal 16965) */
            GREEN_STAIN = 16965,
            /** <code>Grid_Bow</code> (with ordinal 17010) */
            GRID_BOW = 17010,
            /** <code>GRILL_PAN_TT</code> (with ordinal 17025) */
            GRILL_PAN_TT = 17025,
            /** <code>GT-4</code> (with ordinal 17031) */
            GT_4 = 17031,
            /** <code>Guardsman</code> (with ordinal 17055) */
            GUARDSMAN = 17055,
            /** <code>GUN_DISH__ZSU-23_4_</code> (with ordinal 17070) */
            GUN_DISH_ZSU_23_4 = 17070,
            /** <code>Hair_Net</code> (with ordinal 17100) */
            HAIR_NET = 17100,
            /** <code>Half_Plate_A</code> (with ordinal 17145) */
            HALF_PLATE_A = 17145,
            /** <code>Half_Plate_B</code> (with ordinal 17190) */
            HALF_PLATE_B = 17190,
            /** <code>HARD</code> (with ordinal 17220) */
            HARD = 17220,
            /** <code>Harpoon</code> (with ordinal 17225) */
            HARPOON = 17225,
            /** <code>Hawk_Screech</code> (with ordinal 17235) */
            HAWK_SCREECH = 17235,
            /** <code>Head_Light_A</code> (with ordinal 17280) */
            HEAD_LIGHT_A = 17280,
            /** <code>Head_Lights</code> (with ordinal 17325) */
            HEAD_LIGHTS = 17325,
            /** <code>Head_Lights_C</code> (with ordinal 17370) */
            HEAD_LIGHTS_C = 17370,
            /** <code>Head_Lights_MG_A</code> (with ordinal 17415) */
            HEAD_LIGHTS_MG_A = 17415,
            /** <code>Head_Lights_MG_B</code> (with ordinal 17460) */
            HEAD_LIGHTS_MG_B = 17460,
            /** <code>Head_Lights_TT</code> (with ordinal 17505) */
            HEAD_LIGHTS_TT = 17505,
            /** <code>Head_Net</code> (with ordinal 17550) */
            HEAD_NET = 17550,
            /** <code>HeartAcheB</code> (with ordinal 17572) */
            HEART_ACHE_B = 17572,
            /** <code>Hen_Egg</code> (with ordinal 17595) */
            HEN_EGG = 17595,
            /** <code>Hen_House</code> (with ordinal 17640) */
            HEN_HOUSE = 17640,
            /** <code>Hen_Nest</code> (with ordinal 17685) */
            HEN_NEST = 17685,
            /** <code>Hen_Roost</code> (with ordinal 17730) */
            HEN_ROOST = 17730,
            /** <code>High_Brick</code> (with ordinal 17775) */
            HIGH_BRICK = 17775,
            /** <code>High_Fix</code> (with ordinal 17820) */
            HIGH_FIX = 17820,
            /** <code>HighGuard</code> (with ordinal 17842) */
            HIGH_GUARD = 17842,
            /** <code>High_Lark_TI</code> (with ordinal 17865) */
            HIGH_LARK_TI = 17865,
            /** <code>High_Lark_1</code> (with ordinal 17910) */
            HIGH_LARK_1 = 17910,
            /** <code>High_Lark_2</code> (with ordinal 17955) */
            HIGH_LARK_2 = 17955,
            /** <code>High_Lark_4</code> (with ordinal 18000) */
            HIGH_LARK_4 = 18000,
            /** <code>High_Lune</code> (with ordinal 18045) */
            HIGH_LUNE = 18045,
            /** <code>High_Pole_A_B</code> (with ordinal 18090) */
            HIGH_POLE_A_B = 18090,
            /** <code>High_Scoop</code> (with ordinal 18135) */
            HIGH_SCOOP = 18135,
            /** <code>HIGH_SCREEN</code> (with ordinal 18150) */
            HIGH_SCREEN = 18150,
            /** <code>High_Sieve</code> (with ordinal 18180) */
            HIGH_SIEVE = 18180,
            /** <code>HG-9550</code> (with ordinal 18190) */
            HG_9550 = 18190,
            /** <code>HN-503</code> (with ordinal 18200) */
            HN_503 = 18200,
            /** <code>Home_Talk</code> (with ordinal 18225) */
            HOME_TALK = 18225,
            /** <code>Horn_Spoon</code> (with ordinal 18270) */
            HORN_SPOON = 18270,
            /** <code>HOT_BRICK</code> (with ordinal 18280) */
            HOT_BRICK = 18280,
            /** <code>Hot_Flash</code> (with ordinal 18315) */
            HOT_FLASH = 18315,
            /** <code>IHS-6</code> (with ordinal 18318) */
            IHS_6 = 18318,
            /** <code>Hot_Shot_TA</code> (with ordinal 18320) */
            HOT_SHOT_TA = 18320,
            /** <code>Hot_Shot_TT</code> (with ordinal 18325) */
            HOT_SHOT_TT = 18325,
            /** <code>Hot_Shot_MG</code> (with ordinal 18330) */
            HOT_SHOT_MG = 18330,
            /** <code>IFF_MK_XII_AIMS_UPX_29</code> (with ordinal 18360) */
            IFF_MK_XII_AIMS_UPX_29 = 18360,
            /** <code>IFF_MK_XV</code> (with ordinal 18405) */
            IFF_MK_XV = 18405,
            /** <code>IFFINT</code> (with ordinal 18406) */
            IFFINT = 18406,
            /** <code>JackKnife</code> (with ordinal 18407) */
            JACK_KNIFE = 18407,
            /** <code>IFFTRSP</code> (with ordinal 18408) */
            IFFTRSP = 18408,
            /** <code>Javelin_MG</code> (with ordinal 18410) */
            JAVELIN_MG = 18410,
            /** <code>Jay_Bird</code> (with ordinal 18450) */
            JAY_BIRD = 18450,
            /** <code>JL-7</code> (with ordinal 18454) */
            JL_7 = 18454,
            /** <code>JL-10B</code> (with ordinal 18455) */
            JL_10B = 18455,
            /** <code>JLP-40</code> (with ordinal 18458) */
            JLP_40 = 18458,
            /** <code>JRC-NMD-401</code> (with ordinal 18460) */
            JRC_NMD_401 = 18460,
            /** <code>Jupiter</code> (with ordinal 18495) */
            JUPITER = 18495,
            /** <code>Jupiter_II</code> (with ordinal 18540) */
            JUPITER_II = 18540,
            /** <code>JY-8</code> (with ordinal 18550) */
            JY_8 = 18550,
            /** <code>JY-9</code> (with ordinal 18555) */
            JY_9 = 18555,
            /** <code>JY-9Modified</code> (with ordinal 18556) */
            JY_9MODIFIED = 18556,
            /** <code>JY-11EW</code> (with ordinal 18557) */
            JY_11EW = 18557,
            /** <code>JY-14</code> (with ordinal 18560) */
            JY_14 = 18560,
            /** <code>K376Z</code> (with ordinal 18585) */
            K376Z = 18585,
            /** <code>Kelvin_Hughes_2A</code> (with ordinal 18630) */
            KELVIN_HUGHES_2A = 18630,
            /** <code>Kelvin_Hughes_14_9</code> (with ordinal 18675) */
            KELVIN_HUGHES_14_9 = 18675,
            /** <code>Kelvin_Hughes_type_1006</code> (with ordinal 18720) */
            KELVIN_HUGHES_TYPE_1006 = 18720,
            /** <code>Kelvin_Hughes_type_1007</code> (with ordinal 18765) */
            KELVIN_HUGHES_TYPE_1007 = 18765,
            /** <code>KHFamily</code> (with ordinal 18780) */
            KHFAMILY = 18780,
            /** <code>KH-902M</code> (with ordinal 18785) */
            KH_902M = 18785,
            /** <code>KHOROM-K</code> (with ordinal 18786) */
            KHOROM_K = 18786,
            /** <code>KH1700</code> (with ordinal 18795) */
            KH1700 = 18795,
            /** <code>KingPin</code> (with ordinal 18797) */
            KING_PIN = 18797,
            /** <code>KG-300</code> (with ordinal 18805) */
            KG_300 = 18805,
            /** <code>Kite_Screech</code> (with ordinal 18810) */
            KITE_SCREECH = 18810,
            /** <code>Kite_Screech_A</code> (with ordinal 18855) */
            KITE_SCREECH_A = 18855,
            /** <code>Kite_Screech_B</code> (with ordinal 18900) */
            KITE_SCREECH_B = 18900,
            /** <code>Kivach</code> (with ordinal 18945) */
            KIVACH = 18945,
            /** <code>KLJ-1</code> (with ordinal 18948) */
            KLJ_1 = 18948,
            /** <code>KLJ-3_Type1473_</code> (with ordinal 18950) */
            KLJ_3_TYPE1473 = 18950,
            /** <code>Knife_Rest</code> (with ordinal 18990) */
            KNIFE_REST = 18990,
            /** <code>Knife_Rest_B</code> (with ordinal 19035) */
            KNIFE_REST_B = 19035,
            /** <code>KNIFE_REST_C</code> (with ordinal 19037) */
            KNIFE_REST_C = 19037,
            /** <code>KJ-2000</code> (with ordinal 19040) */
            KJ_2000 = 19040,
            /** <code>KR-75</code> (with ordinal 19050) */
            KR_75 = 19050,
            /** <code>KSA_SRN</code> (with ordinal 19080) */
            KSA_SRN = 19080,
            /** <code>KSA_TSR</code> (with ordinal 19125) */
            KSA_TSR = 19125,
            /** <code>Land_Fall</code> (with ordinal 19170) */
            LAND_FALL = 19170,
            /** <code>Land_Roll_MG</code> (with ordinal 19215) */
            LAND_ROLL_MG = 19215,
            /** <code>Land_Roll_TA</code> (with ordinal 19260) */
            LAND_ROLL_TA = 19260,
            /** <code>Land_Roll_TT</code> (with ordinal 19305) */
            LAND_ROLL_TT = 19305,
            /** <code>LAZUR</code> (with ordinal 19306) */
            LAZUR = 19306,
            /** <code>LC-150</code> (with ordinal 19310) */
            LC_150 = 19310,
            /** <code>Leningraf</code> (with ordinal 19350) */
            LENINGRAF = 19350,
            /** <code>Light_Bulb</code> (with ordinal 19395) */
            LIGHT_BULB = 19395,
            /** <code>LMT_NRAI-6A</code> (with ordinal 19400) */
            LMT_NRAI_6A = 19400,
            /** <code>LN_55</code> (with ordinal 19440) */
            LN_55 = 19440,
            /** <code>Ln_66</code> (with ordinal 19485) */
            LN_66 = 19485,
            /** <code>Long_Bow</code> (with ordinal 19530) */
            LONG_BOW = 19530,
            /** <code>Long_Brick</code> (with ordinal 19575) */
            LONG_BRICK = 19575,
            /** <code>Long_Bull</code> (with ordinal 19620) */
            LONG_BULL = 19620,
            /** <code>Long_Eye</code> (with ordinal 19665) */
            LONG_EYE = 19665,
            /** <code>Long_Head</code> (with ordinal 19710) */
            LONG_HEAD = 19710,
            /** <code>Long_Talk</code> (with ordinal 19755) */
            LONG_TALK = 19755,
            /** <code>Long_Track</code> (with ordinal 19800) */
            LONG_TRACK = 19800,
            /** <code>Long_Trough</code> (with ordinal 19845) */
            LONG_TROUGH = 19845,
            /** <code>Look_Two</code> (with ordinal 19890) */
            LOOK_TWO = 19890,
            /** <code>LORAN</code> (with ordinal 19935) */
            LORAN = 19935,
            /** <code>Low_Blow_TA</code> (with ordinal 19950) */
            LOW_BLOW_TA = 19950,
            /** <code>Low_Blow_TT</code> (with ordinal 19955) */
            LOW_BLOW_TT = 19955,
            /** <code>Low_Blow_MG</code> (with ordinal 19960) */
            LOW_BLOW_MG = 19960,
            /** <code>Low_Sieve</code> (with ordinal 19980) */
            LOW_SIEVE = 19980,
            /** <code>Low_Trough</code> (with ordinal 20025) */
            LOW_TROUGH = 20025,
            /** <code>TRS-2050</code> (with ordinal 20040) */
            TRS_2050 = 20040,
            /** <code>LW_08</code> (with ordinal 20070) */
            LW_08 = 20070,
            /** <code>M-1983_FCR</code> (with ordinal 20090) */
            M_1983_FCR = 20090,
            /** <code>M22-40</code> (with ordinal 20115) */
            M22_40 = 20115,
            /** <code>M44</code> (with ordinal 20160) */
            M44 = 20160,
            /** <code>M401Z</code> (with ordinal 20205) */
            M401Z = 20205,
            /** <code>M585Z</code> (with ordinal 20250) */
            M585Z = 20250,
            /** <code>M588Z</code> (with ordinal 20295) */
            M588Z = 20295,
            /** <code>MA_1_IFF_Portion</code> (with ordinal 20340) */
            MA_1_IFF_PORTION = 20340,
            /** <code>MARELD</code> (with ordinal 20360) */
            MARELD = 20360,
            /** <code>MA_Type_909_</code> (with ordinal 20385) */
            MA_TYPE_909 = 20385,
            /** <code>MARCS-152</code> (with ordinal 20420) */
            MARCS_152 = 20420,
            /** <code>Marconi_1810</code> (with ordinal 20430) */
            MARCONI_1810 = 20430,
            /** <code>Marconi_Canada_HC_75</code> (with ordinal 20475) */
            MARCONI_CANADA_HC_75 = 20475,
            /** <code>Marconi_S_713</code> (with ordinal 20495) */
            MARCONI_S_713 = 20495,
            /** <code>Marconi_S_1802</code> (with ordinal 20520) */
            MARCONI_S_1802 = 20520,
            /** <code>Marconi_S_247</code> (with ordinal 20530) */
            MARCONI_S_247 = 20530,
            /** <code>Marconi_S_810</code> (with ordinal 20565) */
            MARCONI_S_810 = 20565,
            /** <code>Marconi_SA_10</code> (with ordinal 20585) */
            MARCONI_SA_10 = 20585,
            /** <code>Marconi_type_967</code> (with ordinal 20610) */
            MARCONI_TYPE_967 = 20610,
            /** <code>Marconi_type_968</code> (with ordinal 20655) */
            MARCONI_TYPE_968 = 20655,
            /** <code>Marconi_type_992</code> (with ordinal 20700) */
            MARCONI_TYPE_992 = 20700,
            /** <code>Marconi_signaal_type_1022</code> (with ordinal 20745) */
            MARCONI_SIGNAAL_TYPE_1022 = 20745,
            /** <code>Marconi_signaal_type_910</code> (with ordinal 20790) */
            MARCONI_SIGNAAL_TYPE_910 = 20790,
            /** <code>Marconi_signaal_type_911</code> (with ordinal 20835) */
            MARCONI_SIGNAAL_TYPE_911 = 20835,
            /** <code>Marconi_signaal_type_992R</code> (with ordinal 20880) */
            MARCONI_SIGNAAL_TYPE_992R = 20880,
            /** <code>MELCO-3</code> (with ordinal 20915) */
            MELCO_3 = 20915,
            /** <code>NorthropGrummanMESA</code> (with ordinal 20920) */
            NORTHROP_GRUMMAN_MESA = 20920,
            /** <code>Mesh_Brick</code> (with ordinal 20925) */
            MESH_BRICK = 20925,
            /** <code>Mirage_ILL</code> (with ordinal 20950) */
            MIRAGE_ILL = 20950,
            /** <code>MK_15_CIWS</code> (with ordinal 20970) */
            MK_15_CIWS = 20970,
            /** <code>MK-23</code> (with ordinal 21015) */
            MK_23 = 21015,
            /** <code>MK_23_TAS</code> (with ordinal 21060) */
            MK_23_TAS = 21060,
            /** <code>MK_25</code> (with ordinal 21105) */
            MK_25 = 21105,
            /** <code>Mk-25Mod-3</code> (with ordinal 21110) */
            MK_25MOD_3 = 21110,
            /** <code>MK-35_M2</code> (with ordinal 21150) */
            MK_35_M2 = 21150,
            /** <code>MK_92</code> (with ordinal 21195) */
            MK_92 = 21195,
            /** <code>MK-92_CAS</code> (with ordinal 21240) */
            MK_92_CAS = 21240,
            /** <code>MK-92_STIR</code> (with ordinal 21285) */
            MK_92_STIR = 21285,
            /** <code>MK_95</code> (with ordinal 21330) */
            MK_95 = 21330,
            /** <code>MKS-818</code> (with ordinal 21332) */
            MKS_818 = 21332,
            /** <code>MLA-1</code> (with ordinal 21340) */
            MLA_1 = 21340,
            /** <code>MM_APS_705</code> (with ordinal 21375) */
            MM_APS_705 = 21375,
            /** <code>MM_SPG_74</code> (with ordinal 21420) */
            MM_SPG_74 = 21420,
            /** <code>MM_SPG_75</code> (with ordinal 21465) */
            MM_SPG_75 = 21465,
            /** <code>MM_SPN_703</code> (with ordinal 21490) */
            MM_SPN_703 = 21490,
            /** <code>MM_SPS_702</code> (with ordinal 21510) */
            MM_SPS_702 = 21510,
            /** <code>MM_SPS_768</code> (with ordinal 21555) */
            MM_SPS_768 = 21555,
            /** <code>MM_SPS_774</code> (with ordinal 21600) */
            MM_SPS_774 = 21600,
            /** <code>Model-17C</code> (with ordinal 21625) */
            MODEL_17C = 21625,
            /** <code>Moon_4</code> (with ordinal 21645) */
            MOON_4 = 21645,
            /** <code>MoonPie</code> (with ordinal 21646) */
            MOON_PIE = 21646,
            /** <code>MMRS</code> (with ordinal 21650) */
            MMRS = 21650,
            /** <code>Model360</code> (with ordinal 21655) */
            MODEL360 = 21655,
            /** <code>Model378</code> (with ordinal 21660) */
            MODEL378 = 21660,
            /** <code>Model-970</code> (with ordinal 21661) */
            MODEL_970 = 21661,
            /** <code>Model974</code> (with ordinal 21665) */
            MODEL974 = 21665,
            /** <code>MPDR_18_X</code> (with ordinal 21690) */
            MPDR_18_X = 21690,
            /** <code>MR-1600</code> (with ordinal 21700) */
            MR_1600 = 21700,
            /** <code>MT-305X</code> (with ordinal 21710) */
            MT_305X = 21710,
            /** <code>Muff_Cob</code> (with ordinal 21735) */
            MUFF_COB = 21735,
            /** <code>Mushroom</code> (with ordinal 21780) */
            MUSHROOM = 21780,
            /** <code>Mushroom_1</code> (with ordinal 21825) */
            MUSHROOM_1 = 21825,
            /** <code>Mushroom_2</code> (with ordinal 21870) */
            MUSHROOM_2 = 21870,
            /** <code>N920Z</code> (with ordinal 21880) */
            N920Z = 21880,
            /** <code>Nanjing_B</code> (with ordinal 21890) */
            NANJING_B = 21890,
            /** <code>Nanjing_C</code> (with ordinal 21895) */
            NANJING_C = 21895,
            /** <code>Nayada</code> (with ordinal 21915) */
            NAYADA = 21915,
            /** <code>Neptun</code> (with ordinal 21960) */
            NEPTUN = 21960,
            /** <code>NIKE_TT</code> (with ordinal 21980) */
            NIKE_TT = 21980,
            /** <code>NJ-81E</code> (with ordinal 21983) */
            NJ_81E = 21983,
            /** <code>NRJ-6A</code> (with ordinal 21985) */
            NRJ_6A = 21985,
            /** <code>NutCan</code> (with ordinal 21992) */
            NUT_CAN = 21992,
            /** <code>NRBA_50</code> (with ordinal 22005) */
            NRBA_50 = 22005,
            /** <code>NRBA_51</code> (with ordinal 22050) */
            NRBA_51 = 22050,
            /** <code>NRBF_20A</code> (with ordinal 22095) */
            NRBF_20A = 22095,
            /** <code>NRJ-5</code> (with ordinal 22110) */
            NRJ_5 = 22110,
            /** <code>Nysa_B</code> (with ordinal 22140) */
            NYSA_B = 22140,
            /** <code>O524A</code> (with ordinal 22185) */
            O524A = 22185,
            /** <code>O580B</code> (with ordinal 22230) */
            O580B = 22230,
            /** <code>O625Z</code> (with ordinal 22275) */
            O625Z = 22275,
            /** <code>O626Z</code> (with ordinal 22320) */
            O626Z = 22320,
            /** <code>OceanMaster</code> (with ordinal 22335) */
            OCEAN_MASTER = 22335,
            /** <code>Odd_Group</code> (with ordinal 22345) */
            ODD_GROUP = 22345,
            /** <code>Odd_Lot</code> (with ordinal 22365) */
            ODD_LOT = 22365,
            /** <code>Odd_Pair</code> (with ordinal 22410) */
            ODD_PAIR = 22410,
            /** <code>OddRods</code> (with ordinal 22411) */
            ODD_RODS = 22411,
            /** <code>Oka</code> (with ordinal 22455) */
            OKA = 22455,
            /** <code>OKEAN</code> (with ordinal 22500) */
            OKEAN = 22500,
            /** <code>OKEANA</code> (with ordinal 22505) */
            OKEANA = 22505,
            /** <code>OKINXE_12C</code> (with ordinal 22545) */
            OKINXE_12C = 22545,
            /** <code>OMEGA</code> (with ordinal 22590) */
            OMEGA = 22590,
            /** <code>Omera_ORB32</code> (with ordinal 22635) */
            OMERA_ORB32 = 22635,
            /** <code>OMUL</code> (with ordinal 22640) */
            OMUL = 22640,
            /** <code>One_Eye</code> (with ordinal 22680) */
            ONE_EYE = 22680,
            /** <code>OP-28</code> (with ordinal 22690) */
            OP_28 = 22690,
            /** <code>OPS-16B</code> (with ordinal 22725) */
            OPS_16B = 22725,
            /** <code>OPS-18</code> (with ordinal 22730) */
            OPS_18 = 22730,
            /** <code>OPS-28</code> (with ordinal 22740) */
            OPS_28 = 22740,
            /** <code>OR-2</code> (with ordinal 22770) */
            OR_2 = 22770,
            /** <code>ORB-31S</code> (with ordinal 22810) */
            ORB_31S = 22810,
            /** <code>ORB_32</code> (with ordinal 22815) */
            ORB_32 = 22815,
            /** <code>Orion_Rtn_10X</code> (with ordinal 22860) */
            ORION_RTN_10X = 22860,
            /** <code>OtomatMK1</code> (with ordinal 22900) */
            OTOMAT_MK1 = 22900,
            /** <code>Otomat_MK_II_Teseo</code> (with ordinal 22905) */
            OTOMAT_MK_II_TESEO = 22905,
            /** <code>Owl_Screech</code> (with ordinal 22950) */
            OWL_SCREECH = 22950,
            /** <code>P360Z</code> (with ordinal 22955) */
            P360Z = 22955,
            /** <code>PA-1660</code> (with ordinal 22960) */
            PA_1660 = 22960,
            /** <code>PaintBox</code> (with ordinal 22977) */
            PAINT_BOX = 22977,
            /** <code>Palm_Frond</code> (with ordinal 22995) */
            PALM_FROND = 22995,
            /** <code>ModifiedPaintBox</code> (with ordinal 22998) */
            MODIFIED_PAINT_BOX = 22998,
            /** <code>Palm_Frond_AB</code> (with ordinal 23040) */
            PALM_FROND_AB = 23040,
            /** <code>Pat_Hand_TT</code> (with ordinal 23085) */
            PAT_HAND_TT = 23085,
            /** <code>Pat_Hand_MG</code> (with ordinal 23095) */
            PAT_HAND_MG = 23095,
            /** <code>Patty_Cake</code> (with ordinal 23130) */
            PATTY_CAKE = 23130,
            /** <code>Pawn_Cake</code> (with ordinal 23175) */
            PAWN_CAKE = 23175,
            /** <code>PBR_4_Rubin</code> (with ordinal 23220) */
            PBR_4_RUBIN = 23220,
            /** <code>Pea_Sticks</code> (with ordinal 23265) */
            PEA_STICKS = 23265,
            /** <code>Peel_Cone</code> (with ordinal 23310) */
            PEEL_CONE = 23310,
            /** <code>Peel_Group</code> (with ordinal 23355) */
            PEEL_GROUP = 23355,
            /** <code>Peel_Group_A</code> (with ordinal 23400) */
            PEEL_GROUP_A = 23400,
            /** <code>Peel_Group_B</code> (with ordinal 23445) */
            PEEL_GROUP_B = 23445,
            /** <code>PeelGroupMG</code> (with ordinal 23450) */
            PEEL_GROUP_MG = 23450,
            /** <code>Peel_Pair</code> (with ordinal 23490) */
            PEEL_PAIR = 23490,
            /** <code>Phalanx</code> (with ordinal 23525) */
            PHALANX = 23525,
            /** <code>Philips_9LV_200</code> (with ordinal 23535) */
            PHILIPS_9LV_200 = 23535,
            /** <code>Philips_9LV_331</code> (with ordinal 23580) */
            PHILIPS_9LV_331 = 23580,
            /** <code>Philips_LV_223</code> (with ordinal 23625) */
            PHILIPS_LV_223 = 23625,
            /** <code>Philips_Sea_Giraffe_50_HC</code> (with ordinal 23670) */
            PHILIPS_SEA_GIRAFFE_50_HC = 23670,
            /** <code>Pin_Jib</code> (with ordinal 23690) */
            PIN_JIB = 23690,
            /** <code>PlankShad</code> (with ordinal 23710) */
            PLANK_SHAD = 23710,
            /** <code>Plank_Shave</code> (with ordinal 23715) */
            PLANK_SHAVE = 23715,
            /** <code>Plank_Shave_A</code> (with ordinal 23760) */
            PLANK_SHAVE_A = 23760,
            /** <code>Plank_Shave_B</code> (with ordinal 23805) */
            PLANK_SHAVE_B = 23805,
            /** <code>Plate_Steer</code> (with ordinal 23850) */
            PLATE_STEER = 23850,
            /** <code>Plessey_AWS_1</code> (with ordinal 23895) */
            PLESSEY_AWS_1 = 23895,
            /** <code>PlesseyAWS-2</code> (with ordinal 23925) */
            PLESSEY_AWS_2 = 23925,
            /** <code>Plessey_AWS_4</code> (with ordinal 23940) */
            PLESSEY_AWS_4 = 23940,
            /** <code>Plessey_AWS_6</code> (with ordinal 23985) */
            PLESSEY_AWS_6 = 23985,
            /** <code>Plessey_RJ</code> (with ordinal 23990) */
            PLESSEY_RJ = 23990,
            /** <code>Plessey_type_996</code> (with ordinal 24030) */
            PLESSEY_TYPE_996 = 24030,
            /** <code>Plinth_Net</code> (with ordinal 24075) */
            PLINTH_NET = 24075,
            /** <code>Pluto</code> (with ordinal 24095) */
            PLUTO = 24095,
            /** <code>POHJANPALO</code> (with ordinal 24100) */
            POHJANPALO = 24100,
            /** <code>POLLUX</code> (with ordinal 24120) */
            POLLUX = 24120,
            /** <code>Pop_Group</code> (with ordinal 24165) */
            POP_GROUP = 24165,
            /** <code>Pop_Group_MG</code> (with ordinal 24210) */
            POP_GROUP_MG = 24210,
            /** <code>Pop_Group_TA</code> (with ordinal 24255) */
            POP_GROUP_TA = 24255,
            /** <code>Pop_Group_TT</code> (with ordinal 24300) */
            POP_GROUP_TT = 24300,
            /** <code>Pork_Trough</code> (with ordinal 24345) */
            PORK_TROUGH = 24345,
            /** <code>PositiveME</code> (with ordinal 24385) */
            POSITIVE_ME = 24385,
            /** <code>Post_Bow</code> (with ordinal 24390) */
            POST_BOW = 24390,
            /** <code>Post_Lamp</code> (with ordinal 24435) */
            POST_LAMP = 24435,
            /** <code>Pot_Drum</code> (with ordinal 24480) */
            POT_DRUM = 24480,
            /** <code>Pot_Head</code> (with ordinal 24525) */
            POT_HEAD = 24525,
            /** <code>PotShot</code> (with ordinal 24535) */
            POT_SHOT = 24535,
            /** <code>PraetorianCountermeasuresSuite</code> (with ordinal 24540) */
            PRAETORIAN_COUNTERMEASURES_SUITE = 24540,
            /** <code>PRIMUS_40_WXD</code> (with ordinal 24570) */
            PRIMUS_40_WXD = 24570,
            /** <code>PRIMUS_300SL</code> (with ordinal 24615) */
            PRIMUS_300SL = 24615,
            /** <code>Primus700</code> (with ordinal 24618) */
            PRIMUS700 = 24618,
            /** <code>Primus_3000</code> (with ordinal 24620) */
            PRIMUS_3000 = 24620,
            /** <code>PRORA</code> (with ordinal 24630) */
            PRORA = 24630,
            /** <code>PRORAPA-1660</code> (with ordinal 24635) */
            PRORAPA_1660 = 24635,
            /** <code>PS-05A</code> (with ordinal 24650) */
            PS_05A = 24650,
            /** <code>PS_46_A</code> (with ordinal 24660) */
            PS_46_A = 24660,
            /** <code>PS_70_R</code> (with ordinal 24705) */
            PS_70_R = 24705,
            /** <code>PS-860</code> (with ordinal 24707) */
            PS_860 = 24707,
            /** <code>PS-870</code> (with ordinal 24709) */
            PS_870 = 24709,
            /** <code>PS-890</code> (with ordinal 24710) */
            PS_890 = 24710,
            /** <code>Puff_Ball</code> (with ordinal 24750) */
            PUFF_BALL = 24750,
            /** <code>PVS-200</code> (with ordinal 24760) */
            PVS_200 = 24760,
            /** <code>R-76</code> (with ordinal 24770) */
            R_76 = 24770,
            /** <code>R41XXX</code> (with ordinal 24775) */
            R41XXX = 24775,
            /** <code>RAC-30</code> (with ordinal 24780) */
            RAC_30 = 24780,
            /** <code>Racal_1229</code> (with ordinal 24795) */
            RACAL_1229 = 24795,
            /** <code>Racal_AC_2690_BT</code> (with ordinal 24840) */
            RACAL_AC_2690_BT = 24840,
            /** <code>Racal_Decca_1216</code> (with ordinal 24885) */
            RACAL_DECCA_1216 = 24885,
            /** <code>Racal-DECCA20V90_9</code> (with ordinal 24890) */
            RACAL_DECCA20V90_9 = 24890,
            /** <code>Racal_Decca_360</code> (with ordinal 24930) */
            RACAL_DECCA_360 = 24930,
            /** <code>Racal_Decca_AC_1290</code> (with ordinal 24975) */
            RACAL_DECCA_AC_1290 = 24975,
            /** <code>Racal_Decca_TM_1229</code> (with ordinal 25020) */
            RACAL_DECCA_TM_1229 = 25020,
            /** <code>Racal_Decca_TM_1626</code> (with ordinal 25065) */
            RACAL_DECCA_TM_1626 = 25065,
            /** <code>Racal_DRBN_34A</code> (with ordinal 25110) */
            RACAL_DRBN_34A = 25110,
            /** <code>Radar_24</code> (with ordinal 25155) */
            RADAR_24 = 25155,
            /** <code>RAN_7S</code> (with ordinal 25200) */
            RAN_7S = 25200,
            /** <code>RAN_10S</code> (with ordinal 25205) */
            RAN_10S = 25205,
            /** <code>RAN_11_LX</code> (with ordinal 25245) */
            RAN_11_LX = 25245,
            /** <code>Rapier_TA</code> (with ordinal 25260) */
            RAPIER_TA = 25260,
            /** <code>Rapier_2000_TA</code> (with ordinal 25265) */
            RAPIER_2000_TA = 25265,
            /** <code>Rapier_MG</code> (with ordinal 25270) */
            RAPIER_MG = 25270,
            /** <code>Rashmi</code> (with ordinal 25275) */
            RASHMI = 25275,
            /** <code>Rasit</code> (with ordinal 25276) */
            RASIT = 25276,
            /** <code>RAT-31S</code> (with ordinal 25280) */
            RAT_31S = 25280,
            /** <code>RATAC__LCT_</code> (with ordinal 25285) */
            RATAC_LCT = 25285,
            /** <code>Rattler</code> (with ordinal 25287) */
            RATTLER = 25287,
            /** <code>RAWS</code> (with ordinal 25288) */
            RAWS = 25288,
            /** <code>RAWL-02</code> (with ordinal 25289) */
            RAWL_02 = 25289,
            /** <code>Raytheon_1220</code> (with ordinal 25290) */
            RAYTHEON_1220 = 25290,
            /** <code>Raytheon_1302</code> (with ordinal 25300) */
            RAYTHEON_1302 = 25300,
            /** <code>Raytheon_1500</code> (with ordinal 25335) */
            RAYTHEON_1500 = 25335,
            /** <code>Raytheon_1645</code> (with ordinal 25380) */
            RAYTHEON_1645 = 25380,
            /** <code>Raytheon_1650</code> (with ordinal 25425) */
            RAYTHEON_1650 = 25425,
            /** <code>Raytheon_1900</code> (with ordinal 25470) */
            RAYTHEON_1900 = 25470,
            /** <code>Raytheon_2502</code> (with ordinal 25515) */
            RAYTHEON_2502 = 25515,
            /** <code>Raytheon_TM_1650_6X</code> (with ordinal 25560) */
            RAYTHEON_TM_1650_6X = 25560,
            /** <code>Raytheon_TM_1660_12S</code> (with ordinal 25605) */
            RAYTHEON_TM_1660_12S = 25605,
            /** <code>RAY-1220XR</code> (with ordinal 25630) */
            RAY_1220XR = 25630,
            /** <code>RAY-1401</code> (with ordinal 25635) */
            RAY_1401 = 25635,
            /** <code>Ray_2900</code> (with ordinal 25650) */
            RAY_2900 = 25650,
            /** <code>Raypath</code> (with ordinal 25695) */
            RAYPATH = 25695,
            /** <code>RBE2</code> (with ordinal 25735) */
            RBE2 = 25735,
            /** <code>RCT-180</code> (with ordinal 25739) */
            RCT_180 = 25739,
            /** <code>RDM</code> (with ordinal 25740) */
            RDM = 25740,
            /** <code>RDY</code> (with ordinal 25760) */
            RDY = 25760,
            /** <code>RDN_72</code> (with ordinal 25785) */
            RDN_72 = 25785,
            /** <code>RDR_1A</code> (with ordinal 25830) */
            RDR_1A = 25830,
            /** <code>RDR_1E</code> (with ordinal 25835) */
            RDR_1E = 25835,
            /** <code>RDR_4A</code> (with ordinal 25840) */
            RDR_4A = 25840,
            /** <code>RDR-160XD</code> (with ordinal 25850) */
            RDR_160XD = 25850,
            /** <code>RDR_1200</code> (with ordinal 25875) */
            RDR_1200 = 25875,
            /** <code>RDR_1400</code> (with ordinal 25885) */
            RDR_1400 = 25885,
            /** <code>RDR_1400_C</code> (with ordinal 25890) */
            RDR_1400_C = 25890,
            /** <code>RDR_1500</code> (with ordinal 25895) */
            RDR_1500 = 25895,
            /** <code>RiceCake</code> (with ordinal 25896) */
            RICE_CAKE = 25896,
            /** <code>Remora</code> (with ordinal 25900) */
            REMORA = 25900,
            /** <code>RiceBowl</code> (with ordinal 25910) */
            RICE_BOWL = 25910,
            /** <code>Rice_Lamp</code> (with ordinal 25920) */
            RICE_LAMP = 25920,
            /** <code>Rice_Pad</code> (with ordinal 25965) */
            RICE_PAD = 25965,
            /** <code>Rice_Screen</code> (with ordinal 26010) */
            RICE_SCREEN = 26010,
            /** <code>DECCARM1070A</code> (with ordinal 26011) */
            DECCARM1070A = 26011,
            /** <code>RM370BT</code> (with ordinal 26015) */
            RM370BT = 26015,
            /** <code>RockwellCollinsFMR-200X</code> (with ordinal 26020) */
            ROCKWELL_COLLINS_FMR_200X = 26020,
            /** <code>ROLAND_BN</code> (with ordinal 26055) */
            ROLAND_BN = 26055,
            /** <code>ROLAND_MG</code> (with ordinal 26100) */
            ROLAND_MG = 26100,
            /** <code>ROLAND_TA</code> (with ordinal 26145) */
            ROLAND_TA = 26145,
            /** <code>ROLAND_TT</code> (with ordinal 26190) */
            ROLAND_TT = 26190,
            /** <code>Round_Ball</code> (with ordinal 26235) */
            ROUND_BALL = 26235,
            /** <code>Round_House</code> (with ordinal 26280) */
            ROUND_HOUSE = 26280,
            /** <code>Round_House_B</code> (with ordinal 26325) */
            ROUND_HOUSE_B = 26325,
            /** <code>RS-02_50</code> (with ordinal 26327) */
            RS_02_50 = 26327,
            /** <code>RT-02_50</code> (with ordinal 26330) */
            RT_02_50 = 26330,
            /** <code>RTN-1A</code> (with ordinal 26350) */
            RTN_1A = 26350,
            /** <code>RumSling</code> (with ordinal 26360) */
            RUM_SLING = 26360,
            /** <code>RV2</code> (with ordinal 26370) */
            RV2 = 26370,
            /** <code>RV3</code> (with ordinal 26415) */
            RV3 = 26415,
            /** <code>RV5</code> (with ordinal 26460) */
            RV5 = 26460,
            /** <code>RV10</code> (with ordinal 26505) */
            RV10 = 26505,
            /** <code>RV-15M</code> (with ordinal 26506) */
            RV_15M = 26506,
            /** <code>RV17</code> (with ordinal 26550) */
            RV17 = 26550,
            /** <code>RV18</code> (with ordinal 26595) */
            RV18 = 26595,
            /** <code>RV-21</code> (with ordinal 26596) */
            RV_21 = 26596,
            /** <code>RV-377</code> (with ordinal 26610) */
            RV_377 = 26610,
            /** <code>RV_UM</code> (with ordinal 26640) */
            RV_UM = 26640,
            /** <code>RXN_2-60</code> (with ordinal 26660) */
            RXN_2_60 = 26660,
            /** <code>S-1810CD</code> (with ordinal 26670) */
            S_1810CD = 26670,
            /** <code>Salamandre</code> (with ordinal 26673) */
            SALAMANDRE = 26673,
            /** <code>S1850M</code> (with ordinal 26675) */
            S1850M = 26675,
            /** <code>SA_2_Guideline</code> (with ordinal 26685) */
            SA_2_GUIDELINE = 26685,
            /** <code>SA_3_Goa</code> (with ordinal 26730) */
            SA_3_GOA = 26730,
            /** <code>SA_8_Gecko_DT</code> (with ordinal 26775) */
            SA_8_GECKO_DT = 26775,
            /** <code>SA-12_TELAR_ILL</code> (with ordinal 26795) */
            SA_12_TELAR_ILL = 26795,
            /** <code>SA_N_7_Gadfly_TI</code> (with ordinal 26820) */
            SA_N_7_GADFLY_TI = 26820,
            /** <code>SA_N_11_Cads_1_UN</code> (with ordinal 26865) */
            SA_N_11_CADS_1_UN = 26865,
            /** <code>SaccadeMH</code> (with ordinal 26900) */
            SACCADE_MH = 26900,
            /** <code>Salt_Pot_A_B</code> (with ordinal 26910) */
            SALT_POT_A_B = 26910,
            /** <code>SAP-14</code> (with ordinal 26920) */
            SAP_14 = 26920,
            /** <code>SAP-518</code> (with ordinal 26925) */
            SAP_518 = 26925,
            /** <code>SAP-518M</code> (with ordinal 26926) */
            SAP_518M = 26926,
            /** <code>SATURNE_II</code> (with ordinal 26955) */
            SATURNE_II = 26955,
            /** <code>Scan_Can</code> (with ordinal 27000) */
            SCAN_CAN = 27000,
            /** <code>Scan_Fix</code> (with ordinal 27045) */
            SCAN_FIX = 27045,
            /** <code>Scan_Odd</code> (with ordinal 27090) */
            SCAN_ODD = 27090,
            /** <code>Scan_Three</code> (with ordinal 27135) */
            SCAN_THREE = 27135,
            /** <code>SCANTER_CSR_</code> (with ordinal 27140) */
            SCANTER_CSR = 27140,
            /** <code>SCORADS</code> (with ordinal 27141) */
            SCORADS = 27141,
            /** <code>SCOREBOARD</code> (with ordinal 27150) */
            SCOREBOARD = 27150,
            /** <code>Scoup_Plate</code> (with ordinal 27180) */
            SCOUP_PLATE = 27180,
            /** <code>SCR-584</code> (with ordinal 27190) */
            SCR_584 = 27190,
            /** <code>Sea_Archer_2</code> (with ordinal 27225) */
            SEA_ARCHER_2 = 27225,
            /** <code>Sea_Hunter_4_MG</code> (with ordinal 27270) */
            SEA_HUNTER_4_MG = 27270,
            /** <code>Sea_Hunter_4_TA</code> (with ordinal 27315) */
            SEA_HUNTER_4_TA = 27315,
            /** <code>Sea_Hunter_4_TT</code> (with ordinal 27360) */
            SEA_HUNTER_4_TT = 27360,
            /** <code>Sea_Gull</code> (with ordinal 27405) */
            SEA_GULL = 27405,
            /** <code>Sea_Net</code> (with ordinal 27450) */
            SEA_NET = 27450,
            /** <code>SeaSparrow</code> (with ordinal 27451) */
            SEA_SPARROW = 27451,
            /** <code>Sea_Spray</code> (with ordinal 27495) */
            SEA_SPRAY = 27495,
            /** <code>Sea_Tiger</code> (with ordinal 27540) */
            SEA_TIGER = 27540,
            /** <code>SeaTigerM</code> (with ordinal 27550) */
            SEA_TIGER_M = 27550,
            /** <code>Searchwater</code> (with ordinal 27570) */
            SEARCHWATER = 27570,
            /** <code>Searchwater2000</code> (with ordinal 27575) */
            SEARCHWATER2000 = 27575,
            /** <code>Selenia_Orion_7</code> (with ordinal 27585) */
            SELENIA_ORION_7 = 27585,
            /** <code>Selenia_type_912</code> (with ordinal 27630) */
            SELENIA_TYPE_912 = 27630,
            /** <code>Selennia_RAN_12_L_X</code> (with ordinal 27675) */
            SELENNIA_RAN_12_L_X = 27675,
            /** <code>Selennia_RTN_10X</code> (with ordinal 27720) */
            SELENNIA_RTN_10X = 27720,
            /** <code>Selinia_ARP_1645</code> (with ordinal 27765) */
            SELINIA_ARP_1645 = 27765,
            /** <code>SG</code> (with ordinal 27800) */
            SG = 27800,
            /** <code>SGR_102_00</code> (with ordinal 27810) */
            SGR_102_00 = 27810,
            /** <code>SGR_103_02</code> (with ordinal 27855) */
            SGR_103_02 = 27855,
            /** <code>SGR-104</code> (with ordinal 27870) */
            SGR_104 = 27870,
            /** <code>Sheet_Bend</code> (with ordinal 27900) */
            SHEET_BEND = 27900,
            /** <code>Sheet_Curve</code> (with ordinal 27945) */
            SHEET_CURVE = 27945,
            /** <code>Ship_Globe</code> (with ordinal 27990) */
            SHIP_GLOBE = 27990,
            /** <code>Ship_Wheel</code> (with ordinal 28035) */
            SHIP_WHEEL = 28035,
            /** <code>SGR_114</code> (with ordinal 28080) */
            SGR_114 = 28080,
            /** <code>Shore_Walk_A</code> (with ordinal 28125) */
            SHORE_WALK_A = 28125,
            /** <code>Short_Horn</code> (with ordinal 28170) */
            SHORT_HORN = 28170,
            /** <code>Shot_Dome</code> (with ordinal 28215) */
            SHOT_DOME = 28215,
            /** <code>Side_Globe_JN</code> (with ordinal 28260) */
            SIDE_GLOBE_JN = 28260,
            /** <code>Side_Net</code> (with ordinal 28280) */
            SIDE_NET = 28280,
            /** <code>Side_Walk_A</code> (with ordinal 28305) */
            SIDE_WALK_A = 28305,
            /** <code>Signaal_DA_02</code> (with ordinal 28350) */
            SIGNAAL_DA_02 = 28350,
            /** <code>Signaal_DA_05</code> (with ordinal 28395) */
            SIGNAAL_DA_05 = 28395,
            /** <code>Signaal_DA_08</code> (with ordinal 28440) */
            SIGNAAL_DA_08 = 28440,
            /** <code>Signaal_LW_08</code> (with ordinal 28485) */
            SIGNAAL_LW_08 = 28485,
            /** <code>Signaal_LWOR</code> (with ordinal 28530) */
            SIGNAAL_LWOR = 28530,
            /** <code>Signaal_M45</code> (with ordinal 28575) */
            SIGNAAL_M45 = 28575,
            /** <code>Signaal_MW_08</code> (with ordinal 28620) */
            SIGNAAL_MW_08 = 28620,
            /** <code>Signaal_SMART</code> (with ordinal 28665) */
            SIGNAAL_SMART = 28665,
            /** <code>Signaal_STING</code> (with ordinal 28710) */
            SIGNAAL_STING = 28710,
            /** <code>Signaal_STIR</code> (with ordinal 28755) */
            SIGNAAL_STIR = 28755,
            /** <code>Signaal_WM_20_2</code> (with ordinal 28800) */
            SIGNAAL_WM_20_2 = 28800,
            /** <code>Signaal_WM_25</code> (with ordinal 28845) */
            SIGNAAL_WM_25 = 28845,
            /** <code>Signaal_WM_27</code> (with ordinal 28890) */
            SIGNAAL_WM_27 = 28890,
            /** <code>Signaal_WM_28</code> (with ordinal 28935) */
            SIGNAAL_WM_28 = 28935,
            /** <code>Signaal_ZW_01</code> (with ordinal 28980) */
            SIGNAAL_ZW_01 = 28980,
            /** <code>Signaal_ZW_06</code> (with ordinal 29025) */
            SIGNAAL_ZW_06 = 29025,
            /** <code>Ski_Pole</code> (with ordinal 29070) */
            SKI_POLE = 29070,
            /** <code>Skin_Head</code> (with ordinal 29115) */
            SKIN_HEAD = 29115,
            /** <code>Skip_Spin</code> (with ordinal 29160) */
            SKIP_SPIN = 29160,
            /** <code>SkyguardB</code> (with ordinal 29180) */
            SKYGUARD_B = 29180,
            /** <code>SKYGUARD_TA</code> (with ordinal 29185) */
            SKYGUARD_TA = 29185,
            /** <code>SKYGUARD_TT</code> (with ordinal 29190) */
            SKYGUARD_TT = 29190,
            /** <code>Skymaster</code> (with ordinal 29200) */
            SKYMASTER = 29200,
            /** <code>Sky_Watch</code> (with ordinal 29205) */
            SKY_WATCH = 29205,
            /** <code>SkyRanger</code> (with ordinal 29210) */
            SKY_RANGER = 29210,
            /** <code>SKYSHADOW</code> (with ordinal 29215) */
            SKYSHADOW = 29215,
            /** <code>SKYSHIELD_TA</code> (with ordinal 29220) */
            SKYSHIELD_TA = 29220,
            /** <code>SL</code> (with ordinal 29250) */
            SL = 29250,
            /** <code>SL_ALQ-234</code> (with ordinal 29270) */
            SL_ALQ_234 = 29270,
            /** <code>Slap_Shot_E</code> (with ordinal 29295) */
            SLAP_SHOT_E = 29295,
            /** <code>Slim_Net</code> (with ordinal 29340) */
            SLIM_NET = 29340,
            /** <code>Slot_Back_A</code> (with ordinal 29385) */
            SLOT_BACK_A = 29385,
            /** <code>Slot_Back_ILL</code> (with ordinal 29400) */
            SLOT_BACK_ILL = 29400,
            /** <code>Slot_Back_B</code> (with ordinal 29430) */
            SLOT_BACK_B = 29430,
            /** <code>SlotBackIV</code> (with ordinal 29431) */
            SLOT_BACK_IV = 29431,
            /** <code>SlotBackBTopaz</code> (with ordinal 29432) */
            SLOT_BACK_BTOPAZ = 29432,
            /** <code>SlotBackVI</code> (with ordinal 29435) */
            SLOT_BACK_VI = 29435,
            /** <code>Slot_Rest</code> (with ordinal 29440) */
            SLOT_REST = 29440,
            /** <code>SMA_3_RM</code> (with ordinal 29475) */
            SMA_3_RM = 29475,
            /** <code>SMA_3_RM_20</code> (with ordinal 29520) */
            SMA_3_RM_20 = 29520,
            /** <code>SMA_3RM_20A_SMG</code> (with ordinal 29565) */
            SMA_3RM_20A_SMG = 29565,
            /** <code>SMA_BPS_704</code> (with ordinal 29610) */
            SMA_BPS_704 = 29610,
            /** <code>SMA_SPIN_749__V__2</code> (with ordinal 29655) */
            SMA_SPIN_749_V_2 = 29655,
            /** <code>SMA_SPN_703</code> (with ordinal 29700) */
            SMA_SPN_703 = 29700,
            /** <code>SMA_SPN_751</code> (with ordinal 29745) */
            SMA_SPN_751 = 29745,
            /** <code>SMA_SPOS_748</code> (with ordinal 29790) */
            SMA_SPOS_748 = 29790,
            /** <code>SMA_SPQ_2</code> (with ordinal 29835) */
            SMA_SPQ_2 = 29835,
            /** <code>SMA_SPQ_2D</code> (with ordinal 29880) */
            SMA_SPQ_2D = 29880,
            /** <code>SMA_SPQ_701</code> (with ordinal 29925) */
            SMA_SPQ_701 = 29925,
            /** <code>SMA_SPS_702_UPX</code> (with ordinal 29970) */
            SMA_SPS_702_UPX = 29970,
            /** <code>SMA_ST_2_OTOMAT_II_MH</code> (with ordinal 30015) */
            SMA_ST_2_OTOMAT_II_MH = 30015,
            /** <code>SR-47A</code> (with ordinal 30016) */
            SR_47A = 30016,
            /** <code>SMA_718_Beacon</code> (with ordinal 30060) */
            SMA_718_BEACON = 30060,
            /** <code>SMART-L</code> (with ordinal 30070) */
            SMART_L = 30070,
            /** <code>SmogLamp</code> (with ordinal 30075) */
            SMOG_LAMP = 30075,
            /** <code>SNAP_SHOT</code> (with ordinal 30080) */
            SNAP_SHOT = 30080,
            /** <code>Snoop_Drift</code> (with ordinal 30105) */
            SNOOP_DRIFT = 30105,
            /** <code>SnoopHalf</code> (with ordinal 30140) */
            SNOOP_HALF = 30140,
            /** <code>Snoop_Head</code> (with ordinal 30150) */
            SNOOP_HEAD = 30150,
            /** <code>Snoop_Pair</code> (with ordinal 30195) */
            SNOOP_PAIR = 30195,
            /** <code>Snoop_Plate</code> (with ordinal 30240) */
            SNOOP_PLATE = 30240,
            /** <code>SnoopPing</code> (with ordinal 30255) */
            SNOOP_PING = 30255,
            /** <code>Snoop_Slab</code> (with ordinal 30285) */
            SNOOP_SLAB = 30285,
            /** <code>Snoop_Tray</code> (with ordinal 30330) */
            SNOOP_TRAY = 30330,
            /** <code>Snoop_Tray_1</code> (with ordinal 30375) */
            SNOOP_TRAY_1 = 30375,
            /** <code>Snoop_Tray_2</code> (with ordinal 30420) */
            SNOOP_TRAY_2 = 30420,
            /** <code>Snoop_Watch</code> (with ordinal 30465) */
            SNOOP_WATCH = 30465,
            /** <code>Snow_Drift</code> (with ordinal 30470) */
            SNOW_DRIFT = 30470,
            /** <code>SPB-7</code> (with ordinal 30475) */
            SPB_7 = 30475,
            /** <code>SO-1</code> (with ordinal 30510) */
            SO_1 = 30510,
            /** <code>SO-12</code> (with ordinal 30520) */
            SO_12 = 30520,
            /** <code>SO_A_Communist</code> (with ordinal 30555) */
            SO_A_COMMUNIST = 30555,
            /** <code>SO-69</code> (with ordinal 30580) */
            SO_69 = 30580,
            /** <code>Sock_Eye</code> (with ordinal 30600) */
            SOCK_EYE = 30600,
            /** <code>SOM_64</code> (with ordinal 30645) */
            SOM_64 = 30645,
            /** <code>Sorbsiya</code> (with ordinal 30660) */
            SORBSIYA = 30660,
            /** <code>SPADA_TT</code> (with ordinal 30670) */
            SPADA_TT = 30670,
            /** <code>Sparrow__AIM_RIM-7__ILL</code> (with ordinal 30690) */
            SPARROW_AIM_RIM_7_ILL = 30690,
            /** <code>SPERRYRASCAR</code> (with ordinal 30691) */
            SPERRYRASCAR = 30691,
            /** <code>Sperry_M-3</code> (with ordinal 30700) */
            SPERRY_M_3 = 30700,
            /** <code>SPG_53F</code> (with ordinal 30735) */
            SPG_53F = 30735,
            /** <code>SPG_70__RTN_10X_</code> (with ordinal 30780) */
            SPG_70_RTN_10X = 30780,
            /** <code>SPG_74__RTN_20X_</code> (with ordinal 30825) */
            SPG_74_RTN_20X = 30825,
            /** <code>SPG_75__RTN_30X_</code> (with ordinal 30870) */
            SPG_75_RTN_30X = 30870,
            /** <code>SPG_76__RTN_30X_</code> (with ordinal 30915) */
            SPG_76_RTN_30X = 30915,
            /** <code>Spin_Scan_A</code> (with ordinal 30960) */
            SPIN_SCAN_A = 30960,
            /** <code>Spin_Scan_B</code> (with ordinal 31005) */
            SPIN_SCAN_B = 31005,
            /** <code>Spin_Trough</code> (with ordinal 31050) */
            SPIN_TROUGH = 31050,
            /** <code>Splash_Drop</code> (with ordinal 31095) */
            SPLASH_DROP = 31095,
            /** <code>SPN-2</code> (with ordinal 31096) */
            SPN_2 = 31096,
            /** <code>SPN-4</code> (with ordinal 31097) */
            SPN_4 = 31097,
            /** <code>SPN-30</code> (with ordinal 31100) */
            SPN_30 = 31100,
            /** <code>SPN_35A</code> (with ordinal 31140) */
            SPN_35A = 31140,
            /** <code>SPN_41</code> (with ordinal 31185) */
            SPN_41 = 31185,
            /** <code>SPN_42</code> (with ordinal 31230) */
            SPN_42 = 31230,
            /** <code>SPN_43A</code> (with ordinal 31275) */
            SPN_43A = 31275,
            /** <code>SPN_43B</code> (with ordinal 31320) */
            SPN_43B = 31320,
            /** <code>SPN_44</code> (with ordinal 31365) */
            SPN_44 = 31365,
            /** <code>SPN_46</code> (with ordinal 31410) */
            SPN_46 = 31410,
            /** <code>SPN_703</code> (with ordinal 31455) */
            SPN_703 = 31455,
            /** <code>SPN_728__V__1</code> (with ordinal 31500) */
            SPN_728_V_1 = 31500,
            /** <code>SPN_748</code> (with ordinal 31545) */
            SPN_748 = 31545,
            /** <code>SPN_750</code> (with ordinal 31590) */
            SPN_750 = 31590,
            /** <code>SPO-8</code> (with ordinal 31592) */
            SPO_8 = 31592,
            /** <code>Sponge_Cake</code> (with ordinal 31635) */
            SPONGE_CAKE = 31635,
            /** <code>Spoon_Rest</code> (with ordinal 31680) */
            SPOON_REST = 31680,
            /** <code>SpoonRestA</code> (with ordinal 31681) */
            SPOON_REST_A = 31681,
            /** <code>SpoonRestB</code> (with ordinal 31682) */
            SPOON_REST_B = 31682,
            /** <code>SpoonRestD</code> (with ordinal 31684) */
            SPOON_REST_D = 31684,
            /** <code>SPQ_712__RAN_12_L_X_</code> (with ordinal 31725) */
            SPQ_712_RAN_12_L_X = 31725,
            /** <code>SPS_6C</code> (with ordinal 31770) */
            SPS_6C = 31770,
            /** <code>SPS_10F</code> (with ordinal 31815) */
            SPS_10F = 31815,
            /** <code>SPS_12</code> (with ordinal 31860) */
            SPS_12 = 31860,
            /** <code>SPS_58</code> (with ordinal 31905) */
            SPS_58 = 31905,
            /** <code>SPS_64</code> (with ordinal 31950) */
            SPS_64 = 31950,
            /** <code>SPS-161</code> (with ordinal 31960) */
            SPS_161 = 31960,
            /** <code>SPS_768__RAN_EL_</code> (with ordinal 31995) */
            SPS_768_RAN_EL = 31995,
            /** <code>SPS_774__RAN_10S_</code> (with ordinal 32040) */
            SPS_774_RAN_10S = 32040,
            /** <code>SPY_790</code> (with ordinal 32085) */
            SPY_790 = 32085,
            /** <code>Square_Head</code> (with ordinal 32130) */
            SQUARE_HEAD = 32130,
            /** <code>Square_Pair</code> (with ordinal 32175) */
            SQUARE_PAIR = 32175,
            /** <code>Square_Slot</code> (with ordinal 32220) */
            SQUARE_SLOT = 32220,
            /** <code>Square_Tie</code> (with ordinal 32265) */
            SQUARE_TIE = 32265,
            /** <code>Squash_Dome</code> (with ordinal 32310) */
            SQUASH_DOME = 32310,
            /** <code>Squat_Eye</code> (with ordinal 32330) */
            SQUAT_EYE = 32330,
            /** <code>Squint_Eye</code> (with ordinal 32355) */
            SQUINT_EYE = 32355,
            /** <code>SR47B-G</code> (with ordinal 32375) */
            SR47B_G = 32375,
            /** <code>SRN_6</code> (with ordinal 32400) */
            SRN_6 = 32400,
            /** <code>SRN_15</code> (with ordinal 32445) */
            SRN_15 = 32445,
            /** <code>SRN_745</code> (with ordinal 32490) */
            SRN_745 = 32490,
            /** <code>SRO_1</code> (with ordinal 32535) */
            SRO_1 = 32535,
            /** <code>SRO_2</code> (with ordinal 32580) */
            SRO_2 = 32580,
            /** <code>SS_C_2B_Samlet_MG</code> (with ordinal 32625) */
            SS_C_2B_SAMLET_MG = 32625,
            /** <code>SS_N_2A_B_CSSC</code> (with ordinal 32670) */
            SS_N_2A_B_CSSC = 32670,
            /** <code>SS_N_2A_B_CSSC_2A_3A2_MH</code> (with ordinal 32715) */
            SS_N_2A_B_CSSC_2A_3A2_MH = 32715,
            /** <code>SS_N_2C_Seeker</code> (with ordinal 32760) */
            SS_N_2C_SEEKER = 32760,
            /** <code>SS_N_2C_D_Styx</code> (with ordinal 32805) */
            SS_N_2C_D_STYX = 32805,
            /** <code>SS_N_2C_D_Styx_C_D_MH</code> (with ordinal 32850) */
            SS_N_2C_D_STYX_C_D_MH = 32850,
            /** <code>SS_N_3_SSC_SS_C_18_BN</code> (with ordinal 32895) */
            SS_N_3_SSC_SS_C_18_BN = 32895,
            /** <code>SS_N_3B_Sepal_AL</code> (with ordinal 32940) */
            SS_N_3B_SEPAL_AL = 32940,
            /** <code>SS_N_3B_Sepal_MH</code> (with ordinal 32985) */
            SS_N_3B_SEPAL_MH = 32985,
            /** <code>SS_N_9_Siren</code> (with ordinal 33030) */
            SS_N_9_SIREN = 33030,
            /** <code>SS_N_9_Siren_AL</code> (with ordinal 33075) */
            SS_N_9_SIREN_AL = 33075,
            /** <code>SS_N_9_Siren_MH</code> (with ordinal 33120) */
            SS_N_9_SIREN_MH = 33120,
            /** <code>SS_N_12_Sandbox_AL</code> (with ordinal 33165) */
            SS_N_12_SANDBOX_AL = 33165,
            /** <code>SS_N_12_Sandbox_MH</code> (with ordinal 33210) */
            SS_N_12_SANDBOX_MH = 33210,
            /** <code>SS_N_19_Shipwreck</code> (with ordinal 33255) */
            SS_N_19_SHIPWRECK = 33255,
            /** <code>SS_N_19_Shipwreck_AL</code> (with ordinal 33300) */
            SS_N_19_SHIPWRECK_AL = 33300,
            /** <code>SS_N_19_Shipwreck_MH</code> (with ordinal 33345) */
            SS_N_19_SHIPWRECK_MH = 33345,
            /** <code>SS_N_21_AL</code> (with ordinal 33390) */
            SS_N_21_AL = 33390,
            /** <code>SS_N_22_Sunburn</code> (with ordinal 33435) */
            SS_N_22_SUNBURN = 33435,
            /** <code>SS_N_22_Sunburn_MH</code> (with ordinal 33480) */
            SS_N_22_SUNBURN_MH = 33480,
            /** <code>SS-N-27SizzlerMH</code> (with ordinal 33485) */
            SS_N_27SIZZLER_MH = 33485,
            /** <code>Stone_Cake</code> (with ordinal 33525) */
            STONE_CAKE = 33525,
            /** <code>STR_41</code> (with ordinal 33570) */
            STR_41 = 33570,
            /** <code>Straight_Flush_TA</code> (with ordinal 33590) */
            STRAIGHT_FLUSH_TA = 33590,
            /** <code>Straight_Flush_TT</code> (with ordinal 33595) */
            STRAIGHT_FLUSH_TT = 33595,
            /** <code>Straight_Flush_ILL</code> (with ordinal 33600) */
            STRAIGHT_FLUSH_ILL = 33600,
            /** <code>Strike_Out</code> (with ordinal 33615) */
            STRIKE_OUT = 33615,
            /** <code>Strut_Curve</code> (with ordinal 33660) */
            STRUT_CURVE = 33660,
            /** <code>Strut_Pair</code> (with ordinal 33705) */
            STRUT_PAIR = 33705,
            /** <code>Strut_Pair_1</code> (with ordinal 33750) */
            STRUT_PAIR_1 = 33750,
            /** <code>Strut_Pair_2</code> (with ordinal 33795) */
            STRUT_PAIR_2 = 33795,
            /** <code>Sun_Visor</code> (with ordinal 33840) */
            SUN_VISOR = 33840,
            /** <code>Superfledermaus</code> (with ordinal 33860) */
            SUPERFLEDERMAUS = 33860,
            /** <code>Supersearcher</code> (with ordinal 33870) */
            SUPERSEARCHER = 33870,
            /** <code>Swift_Rod_1</code> (with ordinal 33885) */
            SWIFT_ROD_1 = 33885,
            /** <code>Swift_Rod_2</code> (with ordinal 33930) */
            SWIFT_ROD_2 = 33930,
            /** <code>T1166</code> (with ordinal 33975) */
            T1166 = 33975,
            /** <code>T1171</code> (with ordinal 34020) */
            T1171 = 34020,
            /** <code>T1202</code> (with ordinal 34040) */
            T1202 = 34040,
            /** <code>T6004</code> (with ordinal 34065) */
            T6004 = 34065,
            /** <code>T6031</code> (with ordinal 34110) */
            T6031 = 34110,
            /** <code>T8067</code> (with ordinal 34155) */
            T8067 = 34155,
            /** <code>T8068</code> (with ordinal 34200) */
            T8068 = 34200,
            /** <code>T8124</code> (with ordinal 34245) */
            T8124 = 34245,
            /** <code>T8408</code> (with ordinal 34290) */
            T8408 = 34290,
            /** <code>T8911</code> (with ordinal 34335) */
            T8911 = 34335,
            /** <code>T8937</code> (with ordinal 34380) */
            T8937 = 34380,
            /** <code>T8944</code> (with ordinal 34425) */
            T8944 = 34425,
            /** <code>T8987</code> (with ordinal 34470) */
            T8987 = 34470,
            /** <code>TACAN_SURF</code> (with ordinal 34505) */
            TACAN_SURF = 34505,
            /** <code>Tall_King</code> (with ordinal 34515) */
            TALL_KING = 34515,
            /** <code>Tall_Mike</code> (with ordinal 34560) */
            TALL_MIKE = 34560,
            /** <code>Tall_Path</code> (with ordinal 34605) */
            TALL_PATH = 34605,
            /** <code>Team_Work</code> (with ordinal 34625) */
            TEAM_WORK = 34625,
            /** <code>T1135</code> (with ordinal 34626) */
            T1135 = 34626,
            /** <code>TANCAN_SURF</code> (with ordinal 34627) */
            TANCAN_SURF = 34627,
            /** <code>THAAD_GBR</code> (with ordinal 34640) */
            THAAD_GBR = 34640,
            /** <code>THD_225</code> (with ordinal 34650) */
            THD_225 = 34650,
            /** <code>THD_1940</code> (with ordinal 34670) */
            THD_1940 = 34670,
            /** <code>THD-1955Palmier</code> (with ordinal 34680) */
            THD_1955PALMIER = 34680,
            /** <code>THD_5500</code> (with ordinal 34695) */
            THD_5500 = 34695,
            /** <code>Thin_Path</code> (with ordinal 34740) */
            THIN_PATH = 34740,
            /** <code>Thin_Skin</code> (with ordinal 34785) */
            THIN_SKIN = 34785,
            /** <code>Thompson_CSF_TA-10</code> (with ordinal 34795) */
            THOMPSON_CSF_TA_10 = 34795,
            /** <code>Thompson_CSF_TH_D_1040_Neptune</code> (with ordinal 34830) */
            THOMPSON_CSF_TH_D_1040_NEPTUNE = 34830,
            /** <code>Thompson_CSF_Calypso</code> (with ordinal 34875) */
            THOMPSON_CSF_CALYPSO = 34875,
            /** <code>Thompson_CSF_CASTOR</code> (with ordinal 34920) */
            THOMPSON_CSF_CASTOR = 34920,
            /** <code>Thompson_CSF_Castor_II</code> (with ordinal 34965) */
            THOMPSON_CSF_CASTOR_II = 34965,
            /** <code>Thompson_CSF_DRBC_32A</code> (with ordinal 35010) */
            THOMPSON_CSF_DRBC_32A = 35010,
            /** <code>Thompson_CSF_DRBJ_11_D_E</code> (with ordinal 35055) */
            THOMPSON_CSF_DRBJ_11_D_E = 35055,
            /** <code>Thompson_CSF_DRBV_15A</code> (with ordinal 35100) */
            THOMPSON_CSF_DRBV_15A = 35100,
            /** <code>Thompson_CSF_DRBV_15C</code> (with ordinal 35145) */
            THOMPSON_CSF_DRBV_15C = 35145,
            /** <code>Thompson_CSF_DRBV_22D</code> (with ordinal 35190) */
            THOMPSON_CSF_DRBV_22D = 35190,
            /** <code>Thompson_CSF_DRBV_23B</code> (with ordinal 35235) */
            THOMPSON_CSF_DRBV_23B = 35235,
            /** <code>Thompson_CSF_DRUA_33</code> (with ordinal 35280) */
            THOMPSON_CSF_DRUA_33 = 35280,
            /** <code>Thompson_CSF_Mars_DRBV_21A</code> (with ordinal 35325) */
            THOMPSON_CSF_MARS_DRBV_21A = 35325,
            /** <code>Thompson_CSF_Sea_Tiger</code> (with ordinal 35370) */
            THOMPSON_CSF_SEA_TIGER = 35370,
            /** <code>Thompson_CSF_Triton</code> (with ordinal 35415) */
            THOMPSON_CSF_TRITON = 35415,
            /** <code>Thompson_CSF_Vega_with_DRBC_32E</code> (with ordinal 35460) */
            THOMPSON_CSF_VEGA_WITH_DRBC_32E = 35460,
            /** <code>TRS-2105</code> (with ordinal 35480) */
            TRS_2105 = 35480,
            /** <code>HT-223</code> (with ordinal 35485) */
            HT_223 = 35485,
            /** <code>TRS-2100</code> (with ordinal 35490) */
            TRS_2100 = 35490,
            /** <code>Tie_Rods</code> (with ordinal 35505) */
            TIE_RODS = 35505,
            /** <code>Tin_Shield</code> (with ordinal 35550) */
            TIN_SHIELD = 35550,
            /** <code>Tin_Trap</code> (with ordinal 35570) */
            TIN_TRAP = 35570,
            /** <code>TIRSPONDER</code> (with ordinal 35580) */
            TIRSPONDER = 35580,
            /** <code>Toad_Stool_1</code> (with ordinal 35595) */
            TOAD_STOOL_1 = 35595,
            /** <code>Toad_Stool_2</code> (with ordinal 35640) */
            TOAD_STOOL_2 = 35640,
            /** <code>Toad_Stool_3</code> (with ordinal 35685) */
            TOAD_STOOL_3 = 35685,
            /** <code>Toad_Stool_4</code> (with ordinal 35730) */
            TOAD_STOOL_4 = 35730,
            /** <code>Toad_Stool_5</code> (with ordinal 35775) */
            TOAD_STOOL_5 = 35775,
            /** <code>TokenB</code> (with ordinal 35785) */
            TOKEN_B = 35785,
            /** <code>Tomb_Stone</code> (with ordinal 35800) */
            TOMB_STONE = 35800,
            /** <code>Tonson</code> (with ordinal 35810) */
            TONSON = 35810,
            /** <code>Top_Bow</code> (with ordinal 35820) */
            TOP_BOW = 35820,
            /** <code>Top_Dome</code> (with ordinal 35865) */
            TOP_DOME = 35865,
            /** <code>Top_Knot</code> (with ordinal 35910) */
            TOP_KNOT = 35910,
            /** <code>Top_Mesh</code> (with ordinal 35955) */
            TOP_MESH = 35955,
            /** <code>Top_Pair</code> (with ordinal 36000) */
            TOP_PAIR = 36000,
            /** <code>Top_Plate</code> (with ordinal 36045) */
            TOP_PLATE = 36045,
            /** <code>TopPlateB</code> (with ordinal 36046) */
            TOP_PLATE_B = 36046,
            /** <code>Top_Sail</code> (with ordinal 36090) */
            TOP_SAIL = 36090,
            /** <code>TYPE-208</code> (with ordinal 36120) */
            TYPE_208 = 36120,
            /** <code>Top_Steer</code> (with ordinal 36135) */
            TOP_STEER = 36135,
            /** <code>Top_Trough</code> (with ordinal 36180) */
            TOP_TROUGH = 36180,
            /** <code>Scrum_Half_TA</code> (with ordinal 36220) */
            SCRUM_HALF_TA = 36220,
            /** <code>TorScrum_Half_TT</code> (with ordinal 36225) */
            TOR_SCRUM_HALF_TT = 36225,
            /** <code>Scrum_Half_MG</code> (with ordinal 36230) */
            SCRUM_HALF_MG = 36230,
            /** <code>Track_Dish</code> (with ordinal 36270) */
            TRACK_DISH = 36270,
            /** <code>TORSO_M</code> (with ordinal 36315) */
            TORSO_M = 36315,
            /** <code>TQN-2</code> (with ordinal 36320) */
            TQN_2 = 36320,
            /** <code>Trap_Door</code> (with ordinal 36360) */
            TRAP_DOOR = 36360,
            /** <code>TRISPONDE</code> (with ordinal 36380) */
            TRISPONDE = 36380,
            /** <code>TritonG</code> (with ordinal 36390) */
            TRITON_G = 36390,
            /** <code>TRS_3033</code> (with ordinal 36405) */
            TRS_3033 = 36405,
            /** <code>TRS3405</code> (with ordinal 36420) */
            TRS3405 = 36420,
            /** <code>TRS3410</code> (with ordinal 36425) */
            TRS3410 = 36425,
            /** <code>TRS3415</code> (with ordinal 36430) */
            TRS3415 = 36430,
            /** <code>TRS-N</code> (with ordinal 36450) */
            TRS_N = 36450,
            /** <code>TSE_5000</code> (with ordinal 36495) */
            TSE_5000 = 36495,
            /** <code>TSR_333</code> (with ordinal 36540) */
            TSR_333 = 36540,
            /** <code>TubBrick</code> (with ordinal 36563) */
            TUB_BRICK = 36563,
            /** <code>Tube_Arm</code> (with ordinal 36585) */
            TUBE_ARM = 36585,
            /** <code>Twin_Eyes</code> (with ordinal 36630) */
            TWIN_EYES = 36630,
            /** <code>Twin_Pill</code> (with ordinal 36675) */
            TWIN_PILL = 36675,
            /** <code>Twin_Scan</code> (with ordinal 36720) */
            TWIN_SCAN = 36720,
            /** <code>Twin_Scan_Ro</code> (with ordinal 36765) */
            TWIN_SCAN_RO = 36765,
            /** <code>Two_Spot</code> (with ordinal 36810) */
            TWO_SPOT = 36810,
            /** <code>Type222</code> (with ordinal 36843) */
            TYPE222 = 36843,
            /** <code>Type226</code> (with ordinal 36846) */
            TYPE226 = 36846,
            /** <code>TYPE_262</code> (with ordinal 36855) */
            TYPE_262 = 36855,
            /** <code>TYPE_275</code> (with ordinal 36900) */
            TYPE_275 = 36900,
            /** <code>TYPE_293</code> (with ordinal 36945) */
            TYPE_293 = 36945,
            /** <code>Type341</code> (with ordinal 36946) */
            TYPE341 = 36946,
            /** <code>TYPE_343_SUN_VISOR_B</code> (with ordinal 36990) */
            TYPE_343_SUN_VISOR_B = 36990,
            /** <code>TYPE_347B</code> (with ordinal 37035) */
            TYPE_347B = 37035,
            /** <code>Type347G</code> (with ordinal 37038) */
            TYPE347G = 37038,
            /** <code>Type352</code> (with ordinal 37040) */
            TYPE352 = 37040,
            /** <code>Type354</code> (with ordinal 37045) */
            TYPE354 = 37045,
            /** <code>Type363</code> (with ordinal 37048) */
            TYPE363 = 37048,
            /** <code>Type-404A_CH_</code> (with ordinal 37050) */
            TYPE_404A_CH = 37050,
            /** <code>Type405</code> (with ordinal 37052) */
            TYPE405 = 37052,
            /** <code>Type753</code> (with ordinal 37075) */
            TYPE753 = 37075,
            /** <code>Type_756</code> (with ordinal 37080) */
            TYPE_756 = 37080,
            /** <code>TYPE_903</code> (with ordinal 37125) */
            TYPE_903 = 37125,
            /** <code>TYPE_909_TI</code> (with ordinal 37170) */
            TYPE_909_TI = 37170,
            /** <code>TYPE_909_TT</code> (with ordinal 37215) */
            TYPE_909_TT = 37215,
            /** <code>TYPE_910</code> (with ordinal 37260) */
            TYPE_910 = 37260,
            /** <code>TYPE-931_CH_</code> (with ordinal 37265) */
            TYPE_931_CH = 37265,
            /** <code>TYPE_965</code> (with ordinal 37305) */
            TYPE_965 = 37305,
            /** <code>TYPE_967</code> (with ordinal 37350) */
            TYPE_967 = 37350,
            /** <code>TYPE_968</code> (with ordinal 37395) */
            TYPE_968 = 37395,
            /** <code>TYPE_974</code> (with ordinal 37440) */
            TYPE_974 = 37440,
            /** <code>TYPE_975</code> (with ordinal 37485) */
            TYPE_975 = 37485,
            /** <code>TYPE_978</code> (with ordinal 37530) */
            TYPE_978 = 37530,
            /** <code>Type981</code> (with ordinal 37534) */
            TYPE981 = 37534,
            /** <code>TYPE_992</code> (with ordinal 37575) */
            TYPE_992 = 37575,
            /** <code>TYPE_993</code> (with ordinal 37620) */
            TYPE_993 = 37620,
            /** <code>TYPE_994</code> (with ordinal 37665) */
            TYPE_994 = 37665,
            /** <code>TYPE_1006_1_</code> (with ordinal 37710) */
            TYPE_1006_1 = 37710,
            /** <code>TYPE_1006_2_</code> (with ordinal 37755) */
            TYPE_1006_2 = 37755,
            /** <code>TYPE_1022</code> (with ordinal 37800) */
            TYPE_1022 = 37800,
            /** <code>UK_MK_10</code> (with ordinal 37845) */
            UK_MK_10 = 37845,
            /** <code>UPS-220C</code> (with ordinal 37850) */
            UPS_220C = 37850,
            /** <code>UPX_1_10</code> (with ordinal 37890) */
            UPX_1_10 = 37890,
            /** <code>UPX_27</code> (with ordinal 37935) */
            UPX_27 = 37935,
            /** <code>URN_20</code> (with ordinal 37980) */
            URN_20 = 37980,
            /** <code>URN_25</code> (with ordinal 38025) */
            URN_25 = 38025,
            /** <code>VOLEX_III_IV</code> (with ordinal 38045) */
            VOLEX_III_IV = 38045,
            /** <code>W1028</code> (with ordinal 38060) */
            W1028 = 38060,
            /** <code>W8818</code> (with ordinal 38070) */
            W8818 = 38070,
            /** <code>W8838</code> (with ordinal 38115) */
            W8838 = 38115,
            /** <code>W8852</code> (with ordinal 38120) */
            W8852 = 38120,
            /** <code>WallRust</code> (with ordinal 38150) */
            WALL_RUST = 38150,
            /** <code>WAS-74S</code> (with ordinal 38160) */
            WAS_74S = 38160,
            /** <code>Wasp_Head</code> (with ordinal 38205) */
            WASP_HEAD = 38205,
            /** <code>WATCHDOG</code> (with ordinal 38210) */
            WATCHDOG = 38210,
            /** <code>Watch_Guard</code> (with ordinal 38250) */
            WATCH_GUARD = 38250,
            /** <code>Watchman</code> (with ordinal 38260) */
            WATCHMAN = 38260,
            /** <code>Western_Electric_MK_10</code> (with ordinal 38295) */
            WESTERN_ELECTRIC_MK_10 = 38295,
            /** <code>WestinghouseADR-4LRSR</code> (with ordinal 38320) */
            WESTINGHOUSE_ADR_4LRSR = 38320,
            /** <code>Westinghouse_Electric_SPG_50</code> (with ordinal 38340) */
            WESTINGHOUSE_ELECTRIC_SPG_50 = 38340,
            /** <code>Westinghouse_Electric_W_120</code> (with ordinal 38385) */
            WESTINGHOUSE_ELECTRIC_W_120 = 38385,
            /** <code>Westinghouse_SPS_29C</code> (with ordinal 38430) */
            WESTINGHOUSE_SPS_29C = 38430,
            /** <code>Westinghouse_SPS_37</code> (with ordinal 38475) */
            WESTINGHOUSE_SPS_37 = 38475,
            /** <code>Wet_Eye</code> (with ordinal 38520) */
            WET_EYE = 38520,
            /** <code>WetEye2</code> (with ordinal 38525) */
            WET_EYE2 = 38525,
            /** <code>Wet_Eye_Mod</code> (with ordinal 38565) */
            WET_EYE_MOD = 38565,
            /** <code>WGU-41_B</code> (with ordinal 38570) */
            WGU_41_B = 38570,
            /** <code>WGU-44_B</code> (with ordinal 38572) */
            WGU_44_B = 38572,
            /** <code>Whiff</code> (with ordinal 38610) */
            WHIFF = 38610,
            /** <code>Whiff_Brick</code> (with ordinal 38655) */
            WHIFF_BRICK = 38655,
            /** <code>Whiff_Fire</code> (with ordinal 38700) */
            WHIFF_FIRE = 38700,
            /** <code>WHITE_HOUSE</code> (with ordinal 38715) */
            WHITE_HOUSE = 38715,
            /** <code>Wild_Card</code> (with ordinal 38745) */
            WILD_CARD = 38745,
            /** <code>Witch_Eight</code> (with ordinal 38790) */
            WITCH_EIGHT = 38790,
            /** <code>Witch_Five</code> (with ordinal 38835) */
            WITCH_FIVE = 38835,
            /** <code>WM2X_Series</code> (with ordinal 38880) */
            WM2X_SERIES = 38880,
            /** <code>WM2X_Series_CAS</code> (with ordinal 38925) */
            WM2X_SERIES_CAS = 38925,
            /** <code>WSR-74C</code> (with ordinal 38950) */
            WSR_74C = 38950,
            /** <code>WSR-74S</code> (with ordinal 38955) */
            WSR_74S = 38955,
            /** <code>WXR-700C</code> (with ordinal 38960) */
            WXR_700C = 38960,
            /** <code>Wood_Gage</code> (with ordinal 38970) */
            WOOD_GAGE = 38970,
            /** <code>Yard_Rake</code> (with ordinal 39015) */
            YARD_RAKE = 39015,
            /** <code>Yew_Loop</code> (with ordinal 39060) */
            YEW_LOOP = 39060,
            /** <code>YLC-4</code> (with ordinal 39073) */
            YLC_4 = 39073,
            /** <code>Yo-Yo</code> (with ordinal 39105) */
            YO_YO = 39105,
            /** <code>ZooPark1</code> (with ordinal 39125) */
            ZOO_PARK1 = 39125,
            /** <code>ZD-12</code> (with ordinal 39131) */
            ZD_12 = 39131,
            /** <code>ZW-06</code> (with ordinal 39150) */
            ZW_06 = 39150,
            /** <code>AN_ALQ-136_V_1</code> (with ordinal 39200) */
            AN_ALQ_136_V_1 = 39200,
            /** <code>AN_ALQ-136_V_2</code> (with ordinal 39201) */
            AN_ALQ_136_V_2 = 39201,
            /** <code>AN_ALQ-136_V_3</code> (with ordinal 39202) */
            AN_ALQ_136_V_3 = 39202,
            /** <code>AN_ALQ-136_V_4</code> (with ordinal 39203) */
            AN_ALQ_136_V_4 = 39203,
            /** <code>AN_ALQ-136_V_5</code> (with ordinal 39204) */
            AN_ALQ_136_V_5 = 39204,
            /** <code>AN_ALQ-162_V_2</code> (with ordinal 39210) */
            AN_ALQ_162_V_2 = 39210,
            /** <code>AN_ALQ-162_V_3</code> (with ordinal 39211) */
            AN_ALQ_162_V_3 = 39211,
            /** <code>AN_ALQ-162_V_4</code> (with ordinal 39212) */
            AN_ALQ_162_V_4 = 39212,
            /** <code>Zhuk-M</code> (with ordinal 45300) */
            ZHUK_M = 45300
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to EmitterTypeEnum: static_cast<DevStudio::EmitterTypeEnum::EmitterTypeEnum>(i)
        */
        LIBAPI bool isValid(const EmitterTypeEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::EmitterTypeEnum::EmitterTypeEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::EmitterTypeEnum::EmitterTypeEnum const &);
}


#endif
