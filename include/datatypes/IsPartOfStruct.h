/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_ISPARTOFSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_ISPARTOFSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/ConstituentPartRelationshipStruct.h>
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/NamedLocationStruct.h>
#include <string>

namespace DevStudio {
   /**
   * Implementation of the <code>IsPartOfStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Defines the spatial relationship between two objects.</i>
   */
   class IsPartOfStruct {

   public:
      /**
      * Description from the FOM: <i>The identifier of the entity of which the object is a constituent part.</i>.
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      */
      EntityIdentifierStruct hostEntityIdentifier;
      /**
      * Description from the FOM: <i>The RTI instance identifier of the object of which this object is a constituent part.</i>.
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      */
      std::string hostRTIObjectIdentifier;
      /**
      * Description from the FOM: <i>The relationship of the constituent part object to its host object.</i>.
      * <br>Description of the data type from the FOM: <i>The relationship of the constituent part object to its host object. Based on the Relationship record as specified in IEEE 1278.1a-1998 section 5.2.56.</i>
      */
      ConstituentPartRelationshipStruct relationship;
      /**
      * Description from the FOM: <i>The discrete positional relationship of the constituent part object with respect to its host object.</i>.
      * <br>Description of the data type from the FOM: <i>The discrete positional relationship of the constituent part object with respect to its host object. Based on the specifications in IEEE 1278.1a-1998 of the IsPartOf PDU 'Location of Part' (paragraph 5.3.9.4e) and 'Named Location' (paragraph 5.3.9.4f) fields.</i>
      */
      NamedLocationStruct namedLocation;

      LIBAPI IsPartOfStruct()
         :
         hostEntityIdentifier(EntityIdentifierStruct()),
         hostRTIObjectIdentifier(""),
         relationship(ConstituentPartRelationshipStruct()),
         namedLocation(NamedLocationStruct())
      {}

      /**
      * Constructor for IsPartOfStruct
      *
      * @param hostEntityIdentifier_ value to set as hostEntityIdentifier.
      * <br>Description from the FOM: <i>The identifier of the entity of which the object is a constituent part.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      * @param hostRTIObjectIdentifier_ value to set as hostRTIObjectIdentifier.
      * <br>Description from the FOM: <i>The RTI instance identifier of the object of which this object is a constituent part.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      * @param relationship_ value to set as relationship.
      * <br>Description from the FOM: <i>The relationship of the constituent part object to its host object.</i>
      * <br>Description of the data type from the FOM: <i>The relationship of the constituent part object to its host object. Based on the Relationship record as specified in IEEE 1278.1a-1998 section 5.2.56.</i>
      * @param namedLocation_ value to set as namedLocation.
      * <br>Description from the FOM: <i>The discrete positional relationship of the constituent part object with respect to its host object.</i>
      * <br>Description of the data type from the FOM: <i>The discrete positional relationship of the constituent part object with respect to its host object. Based on the specifications in IEEE 1278.1a-1998 of the IsPartOf PDU 'Location of Part' (paragraph 5.3.9.4e) and 'Named Location' (paragraph 5.3.9.4f) fields.</i>
      */
      LIBAPI IsPartOfStruct(
         EntityIdentifierStruct hostEntityIdentifier_,
         std::string hostRTIObjectIdentifier_,
         ConstituentPartRelationshipStruct relationship_,
         NamedLocationStruct namedLocation_
         )
         :
         hostEntityIdentifier(hostEntityIdentifier_),
         hostRTIObjectIdentifier(hostRTIObjectIdentifier_),
         relationship(relationship_),
         namedLocation(namedLocation_)
      {}



      /**
      * Function to get hostEntityIdentifier.
      * <br>Description from the FOM: <i>The identifier of the entity of which the object is a constituent part.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @return hostEntityIdentifier
      */
      LIBAPI DevStudio::EntityIdentifierStruct & getHostEntityIdentifier() {
         return hostEntityIdentifier;
      }

      /**
      * Function to get hostRTIObjectIdentifier.
      * <br>Description from the FOM: <i>The RTI instance identifier of the object of which this object is a constituent part.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @return hostRTIObjectIdentifier
      */
      LIBAPI std::string & getHostRTIObjectIdentifier() {
         return hostRTIObjectIdentifier;
      }

      /**
      * Function to get relationship.
      * <br>Description from the FOM: <i>The relationship of the constituent part object to its host object.</i>
      * <br>Description of the data type from the FOM: <i>The relationship of the constituent part object to its host object. Based on the Relationship record as specified in IEEE 1278.1a-1998 section 5.2.56.</i>
      *
      * @return relationship
      */
      LIBAPI DevStudio::ConstituentPartRelationshipStruct & getRelationship() {
         return relationship;
      }

      /**
      * Function to get namedLocation.
      * <br>Description from the FOM: <i>The discrete positional relationship of the constituent part object with respect to its host object.</i>
      * <br>Description of the data type from the FOM: <i>The discrete positional relationship of the constituent part object with respect to its host object. Based on the specifications in IEEE 1278.1a-1998 of the IsPartOf PDU 'Location of Part' (paragraph 5.3.9.4e) and 'Named Location' (paragraph 5.3.9.4f) fields.</i>
      *
      * @return namedLocation
      */
      LIBAPI DevStudio::NamedLocationStruct & getNamedLocation() {
         return namedLocation;
      }

   };


   LIBAPI bool operator ==(const DevStudio::IsPartOfStruct& l, const DevStudio::IsPartOfStruct& r);
   LIBAPI bool operator !=(const DevStudio::IsPartOfStruct& l, const DevStudio::IsPartOfStruct& r);
   LIBAPI bool operator <(const DevStudio::IsPartOfStruct& l, const DevStudio::IsPartOfStruct& r);
   LIBAPI bool operator >(const DevStudio::IsPartOfStruct& l, const DevStudio::IsPartOfStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::IsPartOfStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::IsPartOfStruct const &);
}
#endif
