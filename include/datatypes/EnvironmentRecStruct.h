/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_ENVIRONMENTRECSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_ENVIRONMENTRECSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/EnvironmentRecVariantStruct.h>

namespace DevStudio {
   /**
   * Implementation of the <code>EnvironmentRecStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Record specifying an environment (geometry or state) record</i>
   */
   class EnvironmentRecStruct {

   public:
      /**
      * Description from the FOM: <i>Identifies the sequentially numbered record index</i>.
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      unsigned int index;
      /**
      * Description from the FOM: <i>Specifies geometry and state record alternatives</i>.
      * <br>Description of the data type from the FOM: <i>Record specifying either a geometry or a state record</i>
      */
      EnvironmentRecVariantStruct dataVariant;

      LIBAPI EnvironmentRecStruct()
         :
         index(0),
         dataVariant(EnvironmentRecVariantStruct())
      {}

      /**
      * Constructor for EnvironmentRecStruct
      *
      * @param index_ value to set as index.
      * <br>Description from the FOM: <i>Identifies the sequentially numbered record index</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param dataVariant_ value to set as dataVariant.
      * <br>Description from the FOM: <i>Specifies geometry and state record alternatives</i>
      * <br>Description of the data type from the FOM: <i>Record specifying either a geometry or a state record</i>
      */
      LIBAPI EnvironmentRecStruct(
         unsigned int index_,
         EnvironmentRecVariantStruct dataVariant_
         )
         :
         index(index_),
         dataVariant(dataVariant_)
      {}



      /**
      * Function to get index.
      * <br>Description from the FOM: <i>Identifies the sequentially numbered record index</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return index
      */
      LIBAPI unsigned int & getIndex() {
         return index;
      }

      /**
      * Function to get dataVariant.
      * <br>Description from the FOM: <i>Specifies geometry and state record alternatives</i>
      * <br>Description of the data type from the FOM: <i>Record specifying either a geometry or a state record</i>
      *
      * @return dataVariant
      */
      LIBAPI DevStudio::EnvironmentRecVariantStruct & getDataVariant() {
         return dataVariant;
      }

   };


   LIBAPI bool operator ==(const DevStudio::EnvironmentRecStruct& l, const DevStudio::EnvironmentRecStruct& r);
   LIBAPI bool operator !=(const DevStudio::EnvironmentRecStruct& l, const DevStudio::EnvironmentRecStruct& r);
   LIBAPI bool operator <(const DevStudio::EnvironmentRecStruct& l, const DevStudio::EnvironmentRecStruct& r);
   LIBAPI bool operator >(const DevStudio::EnvironmentRecStruct& l, const DevStudio::EnvironmentRecStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::EnvironmentRecStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::EnvironmentRecStruct const &);
}
#endif
