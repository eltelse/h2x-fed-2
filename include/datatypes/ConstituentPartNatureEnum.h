/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_CONSTITUENTPARTNATUREENUM_H
#define DEVELOPER_STUDIO_DATATYPES_CONSTITUENTPARTNATUREENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace ConstituentPartNatureEnum {
        /**
        * Implementation of the <code>ConstituentPartNatureEnum16</code> data type from the FOM.
        * <br>Description from the FOM: <i>Relationship nature</i>
        */
        enum ConstituentPartNatureEnum {
            /** <code>Other</code> (with ordinal 0) */
            OTHER = 0,
            /** <code>HostFireableMunition</code> (with ordinal 1) */
            HOST_FIREABLE_MUNITION = 1,
            /** <code>MunitionCarriedAsCargo</code> (with ordinal 2) */
            MUNITION_CARRIED_AS_CARGO = 2,
            /** <code>FuelCarriedAsCargo</code> (with ordinal 3) */
            FUEL_CARRIED_AS_CARGO = 3,
            /** <code>GunmountAttachedToHost</code> (with ordinal 4) */
            GUNMOUNT_ATTACHED_TO_HOST = 4,
            /** <code>ComputerGeneratedForcesCarriedAsCargo</code> (with ordinal 5) */
            COMPUTER_GENERATED_FORCES_CARRIED_AS_CARGO = 5,
            /** <code>VehicleCarriedAsCargo</code> (with ordinal 6) */
            VEHICLE_CARRIED_AS_CARGO = 6,
            /** <code>EmitterMountedOnHost</code> (with ordinal 7) */
            EMITTER_MOUNTED_ON_HOST = 7,
            /** <code>MobileCommandAndControlEntityCarriedAboardHost</code> (with ordinal 8) */
            MOBILE_COMMAND_AND_CONTROL_ENTITY_CARRIED_ABOARD_HOST = 8,
            /** <code>EntityStationedWithRespectToHost</code> (with ordinal 9) */
            ENTITY_STATIONED_WITH_RESPECT_TO_HOST = 9,
            /** <code>TeamMemberInFormationWith</code> (with ordinal 10) */
            TEAM_MEMBER_IN_FORMATION_WITH = 10
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to ConstituentPartNatureEnum: static_cast<DevStudio::ConstituentPartNatureEnum::ConstituentPartNatureEnum>(i)
        */
        LIBAPI bool isValid(const ConstituentPartNatureEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::ConstituentPartNatureEnum::ConstituentPartNatureEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::ConstituentPartNatureEnum::ConstituentPartNatureEnum const &);
}


#endif
