/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_AGGREGATESIZESTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_AGGREGATESIZESTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/AggregateReinforcmentLevelEnum.h>
#include <DevStudio/datatypes/AggregateSizeEnum.h>

namespace DevStudio {
   /**
   * Implementation of the <code>AggregateSizeStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>-NULL-</i>
   */
   class AggregateSizeStruct {

   public:
      /**
      * Description from the FOM: <i></i>.
      * <br>Description of the data type from the FOM: <i>-NULL-</i>
      */
      AggregateSizeEnum::AggregateSizeEnum size;
      /**
      * Description from the FOM: <i></i>.
      * <br>Description of the data type from the FOM: <i>-NULL-</i>
      */
      AggregateReinforcmentLevelEnum::AggregateReinforcmentLevelEnum reinforcmentLevel;

      LIBAPI AggregateSizeStruct()
         :
         size(AggregateSizeEnum::AggregateSizeEnum()),
         reinforcmentLevel(AggregateReinforcmentLevelEnum::AggregateReinforcmentLevelEnum())
      {}

      /**
      * Constructor for AggregateSizeStruct
      *
      * @param size_ value to set as size.
      * <br>Description from the FOM: <i></i>
      * <br>Description of the data type from the FOM: <i>-NULL-</i>
      * @param reinforcmentLevel_ value to set as reinforcmentLevel.
      * <br>Description from the FOM: <i></i>
      * <br>Description of the data type from the FOM: <i>-NULL-</i>
      */
      LIBAPI AggregateSizeStruct(
         AggregateSizeEnum::AggregateSizeEnum size_,
         AggregateReinforcmentLevelEnum::AggregateReinforcmentLevelEnum reinforcmentLevel_
         )
         :
         size(size_),
         reinforcmentLevel(reinforcmentLevel_)
      {}



      /**
      * Function to get size.
      * <br>Description from the FOM: <i></i>
      * <br>Description of the data type from the FOM: <i>-NULL-</i>
      *
      * @return size
      */
      LIBAPI DevStudio::AggregateSizeEnum::AggregateSizeEnum & getSize() {
         return size;
      }

      /**
      * Function to get reinforcmentLevel.
      * <br>Description from the FOM: <i></i>
      * <br>Description of the data type from the FOM: <i>-NULL-</i>
      *
      * @return reinforcmentLevel
      */
      LIBAPI DevStudio::AggregateReinforcmentLevelEnum::AggregateReinforcmentLevelEnum & getReinforcmentLevel() {
         return reinforcmentLevel;
      }

   };


   LIBAPI bool operator ==(const DevStudio::AggregateSizeStruct& l, const DevStudio::AggregateSizeStruct& r);
   LIBAPI bool operator !=(const DevStudio::AggregateSizeStruct& l, const DevStudio::AggregateSizeStruct& r);
   LIBAPI bool operator <(const DevStudio::AggregateSizeStruct& l, const DevStudio::AggregateSizeStruct& r);
   LIBAPI bool operator >(const DevStudio::AggregateSizeStruct& l, const DevStudio::AggregateSizeStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::AggregateSizeStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::AggregateSizeStruct const &);
}
#endif
