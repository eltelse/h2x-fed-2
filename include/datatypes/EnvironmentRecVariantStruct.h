/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_ENVIRONMENTRECVARIANTSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_ENVIRONMENTRECVARIANTSTRUCT_H

#include <DevStudio/datatypes/COMBICStateRecStruct.h>
#include <DevStudio/datatypes/Cone1GeomRecStruct.h>
#include <DevStudio/datatypes/Cone2GeomRecStruct.h>
#include <DevStudio/datatypes/Ellipsoid1GeomRecStruct.h>
#include <DevStudio/datatypes/Ellipsoid2GeomRecStruct.h>
#include <DevStudio/datatypes/EnvironmentRecordTypeEnum.h>
#include <DevStudio/datatypes/FlareStateRecStruct.h>
#include <DevStudio/datatypes/GaussPlumeGeomRecStruct.h>
#include <DevStudio/datatypes/GaussPuffGeomRecStruct.h>
#include <DevStudio/datatypes/Line1GeomRecStruct.h>
#include <DevStudio/datatypes/Line2GeomRecStruct.h>
#include <DevStudio/datatypes/Point2GeomRecStruct.h>
#include <DevStudio/datatypes/RectVol1GeomRecStruct.h>
#include <DevStudio/datatypes/RectVol2GeomRecStruct.h>
#include <DevStudio/datatypes/RectVol3GeomRecStruct.h>
#include <DevStudio/datatypes/Sphere1GeomRecStruct.h>
#include <DevStudio/datatypes/Sphere2GeomRecStruct.h>
#include <DevStudio/datatypes/UniformGeomRecStruct.h>
#include <DevStudio/datatypes/WorldLocationStruct.h>

#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
   /**
   * Implementation of the <code>EnvironmentRecVariantStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Record specifying either a geometry or a state record</i>
   */
   class EnvironmentRecVariantStruct {

   public:
      LIBAPI EnvironmentRecVariantStruct()
         : 
         _discriminant(DevStudio::EnvironmentRecordTypeEnum::EnvironmentRecordTypeEnum())
      {}

      /** 
      * Create a new alternative EnvironmentRecVariantStruct, with <code>PointRecord1Type</code> as discriminant.
      *
      * @param point1GeometryData value of the Point1GeometryData field
      *
      * @return a new EnvironmentRecVariantStruct
      */
      LIBAPI static EnvironmentRecVariantStruct createPoint1GeometryData(DevStudio::WorldLocationStruct point1GeometryData);

      /** 
      * Create a new alternative EnvironmentRecVariantStruct, with <code>PointRecord2Type</code> as discriminant.
      *
      * @param point2GeometryData value of the Point2GeometryData field
      *
      * @return a new EnvironmentRecVariantStruct
      */
      LIBAPI static EnvironmentRecVariantStruct createPoint2GeometryData(DevStudio::Point2GeomRecStruct point2GeometryData);

      /** 
      * Create a new alternative EnvironmentRecVariantStruct, with <code>LineRecord1Type</code> as discriminant.
      *
      * @param line1GeometryData value of the Line1GeometryData field
      *
      * @return a new EnvironmentRecVariantStruct
      */
      LIBAPI static EnvironmentRecVariantStruct createLine1GeometryData(DevStudio::Line1GeomRecStruct line1GeometryData);

      /** 
      * Create a new alternative EnvironmentRecVariantStruct, with <code>LineRecord2Type</code> as discriminant.
      *
      * @param line2GeometryData value of the Line2GeometryData field
      *
      * @return a new EnvironmentRecVariantStruct
      */
      LIBAPI static EnvironmentRecVariantStruct createLine2GeometryData(DevStudio::Line2GeomRecStruct line2GeometryData);

      /** 
      * Create a new alternative EnvironmentRecVariantStruct, with <code>BoundingSphereRecordType</code> as discriminant.
      *
      * @param boundingSphereGeometryData value of the BoundingSphereGeometryData field
      *
      * @return a new EnvironmentRecVariantStruct
      */
      LIBAPI static EnvironmentRecVariantStruct createBoundingSphereGeometryData(DevStudio::Sphere1GeomRecStruct boundingSphereGeometryData);

      /** 
      * Create a new alternative EnvironmentRecVariantStruct, with <code>SphereRecord1Type</code> as discriminant.
      *
      * @param sphere1GeometryData value of the Sphere1GeometryData field
      *
      * @return a new EnvironmentRecVariantStruct
      */
      LIBAPI static EnvironmentRecVariantStruct createSphere1GeometryData(DevStudio::Sphere1GeomRecStruct sphere1GeometryData);

      /** 
      * Create a new alternative EnvironmentRecVariantStruct, with <code>SphereRecord2Type</code> as discriminant.
      *
      * @param sphere2GeometryData value of the Sphere2GeometryData field
      *
      * @return a new EnvironmentRecVariantStruct
      */
      LIBAPI static EnvironmentRecVariantStruct createSphere2GeometryData(DevStudio::Sphere2GeomRecStruct sphere2GeometryData);

      /** 
      * Create a new alternative EnvironmentRecVariantStruct, with <code>EllipsoidRecord1Type</code> as discriminant.
      *
      * @param ellipsoid1GeometryData value of the Ellipsoid1GeometryData field
      *
      * @return a new EnvironmentRecVariantStruct
      */
      LIBAPI static EnvironmentRecVariantStruct createEllipsoid1GeometryData(DevStudio::Ellipsoid1GeomRecStruct ellipsoid1GeometryData);

      /** 
      * Create a new alternative EnvironmentRecVariantStruct, with <code>EllipsoidRecord2Type</code> as discriminant.
      *
      * @param ellipsoid2GeometryData value of the Ellipsoid2GeometryData field
      *
      * @return a new EnvironmentRecVariantStruct
      */
      LIBAPI static EnvironmentRecVariantStruct createEllipsoid2GeometryData(DevStudio::Ellipsoid2GeomRecStruct ellipsoid2GeometryData);

      /** 
      * Create a new alternative EnvironmentRecVariantStruct, with <code>ConeRecord1Type</code> as discriminant.
      *
      * @param cone1GeometryData value of the Cone1GeometryData field
      *
      * @return a new EnvironmentRecVariantStruct
      */
      LIBAPI static EnvironmentRecVariantStruct createCone1GeometryData(DevStudio::Cone1GeomRecStruct cone1GeometryData);

      /** 
      * Create a new alternative EnvironmentRecVariantStruct, with <code>ConeRecord2Type</code> as discriminant.
      *
      * @param cone2GeometryData value of the Cone2GeometryData field
      *
      * @return a new EnvironmentRecVariantStruct
      */
      LIBAPI static EnvironmentRecVariantStruct createCone2GeometryData(DevStudio::Cone2GeomRecStruct cone2GeometryData);

      /** 
      * Create a new alternative EnvironmentRecVariantStruct, with <code>RectangularVolRecord1Type</code> as discriminant.
      *
      * @param rectVol1GeometryData value of the RectVol1GeometryData field
      *
      * @return a new EnvironmentRecVariantStruct
      */
      LIBAPI static EnvironmentRecVariantStruct createRectVol1GeometryData(DevStudio::RectVol1GeomRecStruct rectVol1GeometryData);

      /** 
      * Create a new alternative EnvironmentRecVariantStruct, with <code>RectangularVolRecord2Type</code> as discriminant.
      *
      * @param rectVol2GeometryData value of the RectVol2GeometryData field
      *
      * @return a new EnvironmentRecVariantStruct
      */
      LIBAPI static EnvironmentRecVariantStruct createRectVol2GeometryData(DevStudio::RectVol2GeomRecStruct rectVol2GeometryData);

      /** 
      * Create a new alternative EnvironmentRecVariantStruct, with <code>GaussianPlumeRecordType</code> as discriminant.
      *
      * @param gaussPlumeGeometryData value of the GaussPlumeGeometryData field
      *
      * @return a new EnvironmentRecVariantStruct
      */
      LIBAPI static EnvironmentRecVariantStruct createGaussPlumeGeometryData(DevStudio::GaussPlumeGeomRecStruct gaussPlumeGeometryData);

      /** 
      * Create a new alternative EnvironmentRecVariantStruct, with <code>GaussianPuffRecordType</code> as discriminant.
      *
      * @param gaussPuffGeometryData value of the GaussPuffGeometryData field
      *
      * @return a new EnvironmentRecVariantStruct
      */
      LIBAPI static EnvironmentRecVariantStruct createGaussPuffGeometryData(DevStudio::GaussPuffGeomRecStruct gaussPuffGeometryData);

      /** 
      * Create a new alternative EnvironmentRecVariantStruct, with <code>UniformGeometryRecordType</code> as discriminant.
      *
      * @param uniformGeometryData value of the UniformGeometryData field
      *
      * @return a new EnvironmentRecVariantStruct
      */
      LIBAPI static EnvironmentRecVariantStruct createUniformGeometryData(DevStudio::UniformGeomRecStruct uniformGeometryData);

      /** 
      * Create a new alternative EnvironmentRecVariantStruct, with <code>COMBICStateRecordType</code> as discriminant.
      *
      * @param cOMBICStateData value of the COMBICStateData field
      *
      * @return a new EnvironmentRecVariantStruct
      */
      LIBAPI static EnvironmentRecVariantStruct createCOMBICStateData(DevStudio::COMBICStateRecStruct cOMBICStateData);

      /** 
      * Create a new alternative EnvironmentRecVariantStruct, with <code>FlareStateRecordType</code> as discriminant.
      *
      * @param flareStateData value of the FlareStateData field
      *
      * @return a new EnvironmentRecVariantStruct
      */
      LIBAPI static EnvironmentRecVariantStruct createFlareStateData(DevStudio::FlareStateRecStruct flareStateData);

      /** 
      * Create a new alternative EnvironmentRecVariantStruct, with <code>RectangularVolRecord3Type</code> as discriminant.
      *
      * @param rectVol3GeometryData value of the RectVol3GeometryData field
      *
      * @return a new EnvironmentRecVariantStruct
      */
      LIBAPI static EnvironmentRecVariantStruct createRectVol3GeometryData(DevStudio::RectVol3GeomRecStruct rectVol3GeometryData);



      /**
      * Function to get discriminant
      *
      * @return disciminant
      */
      LIBAPI DevStudio::EnvironmentRecordTypeEnum::EnvironmentRecordTypeEnum getDiscriminant() const {
          return _discriminant;
      }

      /**
      * Function to get Point1GeometryData.
      * Note that this field is only valid of the discriminant is <code>PointRecord1Type</code>.
      * <br>Description from the FOM: <i>Specifies Point 1 geometry record alternative</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @return Point1GeometryData value
      */
      LIBAPI DevStudio::WorldLocationStructPtr getPoint1GeometryData() const {
         return _point1GeometryData;
      }

      /**
      * Function to set Point1GeometryData.
      * Note that this will set the discriminant to <code>PointRecord1Type</code>.
      * <br>Description from the FOM: <i>Specifies Point 1 geometry record alternative</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @param point1GeometryData value used to create a WorldLocationStructPtr
      */
      LIBAPI void setPoint1GeometryData(const DevStudio::WorldLocationStruct point1GeometryData) {
         _point1GeometryData = DevStudio::WorldLocationStructPtr( new DevStudio::WorldLocationStruct (point1GeometryData));
         _discriminant = EnvironmentRecordTypeEnum::POINT_RECORD1TYPE;
      }

      /**
      * Function to get Point2GeometryData.
      * Note that this field is only valid of the discriminant is <code>PointRecord2Type</code>.
      * <br>Description from the FOM: <i>Specifies Point 2 geometry record alternative</i>
      * <br>Description of the data type from the FOM: <i>Record specifying Point 2 geometry record</i>
      *
      * @return Point2GeometryData value
      */
      LIBAPI DevStudio::Point2GeomRecStructPtr getPoint2GeometryData() const {
         return _point2GeometryData;
      }

      /**
      * Function to set Point2GeometryData.
      * Note that this will set the discriminant to <code>PointRecord2Type</code>.
      * <br>Description from the FOM: <i>Specifies Point 2 geometry record alternative</i>
      * <br>Description of the data type from the FOM: <i>Record specifying Point 2 geometry record</i>
      *
      * @param point2GeometryData value used to create a Point2GeomRecStructPtr
      */
      LIBAPI void setPoint2GeometryData(const DevStudio::Point2GeomRecStruct point2GeometryData) {
         _point2GeometryData = DevStudio::Point2GeomRecStructPtr( new DevStudio::Point2GeomRecStruct (point2GeometryData));
         _discriminant = EnvironmentRecordTypeEnum::POINT_RECORD2TYPE;
      }

      /**
      * Function to get Line1GeometryData.
      * Note that this field is only valid of the discriminant is <code>LineRecord1Type</code>.
      * <br>Description from the FOM: <i>Specifies Line 1 geometry record alternative</i>
      * <br>Description of the data type from the FOM: <i>Record specifying Line 1 geometry record</i>
      *
      * @return Line1GeometryData value
      */
      LIBAPI DevStudio::Line1GeomRecStructPtr getLine1GeometryData() const {
         return _line1GeometryData;
      }

      /**
      * Function to set Line1GeometryData.
      * Note that this will set the discriminant to <code>LineRecord1Type</code>.
      * <br>Description from the FOM: <i>Specifies Line 1 geometry record alternative</i>
      * <br>Description of the data type from the FOM: <i>Record specifying Line 1 geometry record</i>
      *
      * @param line1GeometryData value used to create a Line1GeomRecStructPtr
      */
      LIBAPI void setLine1GeometryData(const DevStudio::Line1GeomRecStruct line1GeometryData) {
         _line1GeometryData = DevStudio::Line1GeomRecStructPtr( new DevStudio::Line1GeomRecStruct (line1GeometryData));
         _discriminant = EnvironmentRecordTypeEnum::LINE_RECORD1TYPE;
      }

      /**
      * Function to get Line2GeometryData.
      * Note that this field is only valid of the discriminant is <code>LineRecord2Type</code>.
      * <br>Description from the FOM: <i>Specifies Line 2 geometry record alternative</i>
      * <br>Description of the data type from the FOM: <i>Record specifying Line 2 geometry record</i>
      *
      * @return Line2GeometryData value
      */
      LIBAPI DevStudio::Line2GeomRecStructPtr getLine2GeometryData() const {
         return _line2GeometryData;
      }

      /**
      * Function to set Line2GeometryData.
      * Note that this will set the discriminant to <code>LineRecord2Type</code>.
      * <br>Description from the FOM: <i>Specifies Line 2 geometry record alternative</i>
      * <br>Description of the data type from the FOM: <i>Record specifying Line 2 geometry record</i>
      *
      * @param line2GeometryData value used to create a Line2GeomRecStructPtr
      */
      LIBAPI void setLine2GeometryData(const DevStudio::Line2GeomRecStruct line2GeometryData) {
         _line2GeometryData = DevStudio::Line2GeomRecStructPtr( new DevStudio::Line2GeomRecStruct (line2GeometryData));
         _discriminant = EnvironmentRecordTypeEnum::LINE_RECORD2TYPE;
      }

      /**
      * Function to get BoundingSphereGeometryData.
      * Note that this field is only valid of the discriminant is <code>BoundingSphereRecordType</code>.
      * <br>Description from the FOM: <i>Specifies Bounding Sphere geometry record alternative</i>
      * <br>Description of the data type from the FOM: <i>Record specifying Bounding Sphere & Sphere 1 geometry record</i>
      *
      * @return BoundingSphereGeometryData value
      */
      LIBAPI DevStudio::Sphere1GeomRecStructPtr getBoundingSphereGeometryData() const {
         return _boundingSphereGeometryData;
      }

      /**
      * Function to set BoundingSphereGeometryData.
      * Note that this will set the discriminant to <code>BoundingSphereRecordType</code>.
      * <br>Description from the FOM: <i>Specifies Bounding Sphere geometry record alternative</i>
      * <br>Description of the data type from the FOM: <i>Record specifying Bounding Sphere & Sphere 1 geometry record</i>
      *
      * @param boundingSphereGeometryData value used to create a Sphere1GeomRecStructPtr
      */
      LIBAPI void setBoundingSphereGeometryData(const DevStudio::Sphere1GeomRecStruct boundingSphereGeometryData) {
         _boundingSphereGeometryData = DevStudio::Sphere1GeomRecStructPtr( new DevStudio::Sphere1GeomRecStruct (boundingSphereGeometryData));
         _discriminant = EnvironmentRecordTypeEnum::BOUNDING_SPHERE_RECORD_TYPE;
      }

      /**
      * Function to get Sphere1GeometryData.
      * Note that this field is only valid of the discriminant is <code>SphereRecord1Type</code>.
      * <br>Description from the FOM: <i>Specifies Sphere 1 geometry record alternative</i>
      * <br>Description of the data type from the FOM: <i>Record specifying Bounding Sphere & Sphere 1 geometry record</i>
      *
      * @return Sphere1GeometryData value
      */
      LIBAPI DevStudio::Sphere1GeomRecStructPtr getSphere1GeometryData() const {
         return _sphere1GeometryData;
      }

      /**
      * Function to set Sphere1GeometryData.
      * Note that this will set the discriminant to <code>SphereRecord1Type</code>.
      * <br>Description from the FOM: <i>Specifies Sphere 1 geometry record alternative</i>
      * <br>Description of the data type from the FOM: <i>Record specifying Bounding Sphere & Sphere 1 geometry record</i>
      *
      * @param sphere1GeometryData value used to create a Sphere1GeomRecStructPtr
      */
      LIBAPI void setSphere1GeometryData(const DevStudio::Sphere1GeomRecStruct sphere1GeometryData) {
         _sphere1GeometryData = DevStudio::Sphere1GeomRecStructPtr( new DevStudio::Sphere1GeomRecStruct (sphere1GeometryData));
         _discriminant = EnvironmentRecordTypeEnum::SPHERE_RECORD1TYPE;
      }

      /**
      * Function to get Sphere2GeometryData.
      * Note that this field is only valid of the discriminant is <code>SphereRecord2Type</code>.
      * <br>Description from the FOM: <i>Specifies Sphere 2 geometry record alternative</i>
      * <br>Description of the data type from the FOM: <i>Record specifying Sphere 2 geometry record</i>
      *
      * @return Sphere2GeometryData value
      */
      LIBAPI DevStudio::Sphere2GeomRecStructPtr getSphere2GeometryData() const {
         return _sphere2GeometryData;
      }

      /**
      * Function to set Sphere2GeometryData.
      * Note that this will set the discriminant to <code>SphereRecord2Type</code>.
      * <br>Description from the FOM: <i>Specifies Sphere 2 geometry record alternative</i>
      * <br>Description of the data type from the FOM: <i>Record specifying Sphere 2 geometry record</i>
      *
      * @param sphere2GeometryData value used to create a Sphere2GeomRecStructPtr
      */
      LIBAPI void setSphere2GeometryData(const DevStudio::Sphere2GeomRecStruct sphere2GeometryData) {
         _sphere2GeometryData = DevStudio::Sphere2GeomRecStructPtr( new DevStudio::Sphere2GeomRecStruct (sphere2GeometryData));
         _discriminant = EnvironmentRecordTypeEnum::SPHERE_RECORD2TYPE;
      }

      /**
      * Function to get Ellipsoid1GeometryData.
      * Note that this field is only valid of the discriminant is <code>EllipsoidRecord1Type</code>.
      * <br>Description from the FOM: <i>Specifies Ellipsoid 1 geometry record alternative</i>
      * <br>Description of the data type from the FOM: <i>Record specifying Ellipsoid 1 geometry record</i>
      *
      * @return Ellipsoid1GeometryData value
      */
      LIBAPI DevStudio::Ellipsoid1GeomRecStructPtr getEllipsoid1GeometryData() const {
         return _ellipsoid1GeometryData;
      }

      /**
      * Function to set Ellipsoid1GeometryData.
      * Note that this will set the discriminant to <code>EllipsoidRecord1Type</code>.
      * <br>Description from the FOM: <i>Specifies Ellipsoid 1 geometry record alternative</i>
      * <br>Description of the data type from the FOM: <i>Record specifying Ellipsoid 1 geometry record</i>
      *
      * @param ellipsoid1GeometryData value used to create a Ellipsoid1GeomRecStructPtr
      */
      LIBAPI void setEllipsoid1GeometryData(const DevStudio::Ellipsoid1GeomRecStruct ellipsoid1GeometryData) {
         _ellipsoid1GeometryData = DevStudio::Ellipsoid1GeomRecStructPtr( new DevStudio::Ellipsoid1GeomRecStruct (ellipsoid1GeometryData));
         _discriminant = EnvironmentRecordTypeEnum::ELLIPSOID_RECORD1TYPE;
      }

      /**
      * Function to get Ellipsoid2GeometryData.
      * Note that this field is only valid of the discriminant is <code>EllipsoidRecord2Type</code>.
      * <br>Description from the FOM: <i>Specifies Ellipsoid 2 geometry record alternative</i>
      * <br>Description of the data type from the FOM: <i>Record specifying Ellipsoid 2 geometry record</i>
      *
      * @return Ellipsoid2GeometryData value
      */
      LIBAPI DevStudio::Ellipsoid2GeomRecStructPtr getEllipsoid2GeometryData() const {
         return _ellipsoid2GeometryData;
      }

      /**
      * Function to set Ellipsoid2GeometryData.
      * Note that this will set the discriminant to <code>EllipsoidRecord2Type</code>.
      * <br>Description from the FOM: <i>Specifies Ellipsoid 2 geometry record alternative</i>
      * <br>Description of the data type from the FOM: <i>Record specifying Ellipsoid 2 geometry record</i>
      *
      * @param ellipsoid2GeometryData value used to create a Ellipsoid2GeomRecStructPtr
      */
      LIBAPI void setEllipsoid2GeometryData(const DevStudio::Ellipsoid2GeomRecStruct ellipsoid2GeometryData) {
         _ellipsoid2GeometryData = DevStudio::Ellipsoid2GeomRecStructPtr( new DevStudio::Ellipsoid2GeomRecStruct (ellipsoid2GeometryData));
         _discriminant = EnvironmentRecordTypeEnum::ELLIPSOID_RECORD2TYPE;
      }

      /**
      * Function to get Cone1GeometryData.
      * Note that this field is only valid of the discriminant is <code>ConeRecord1Type</code>.
      * <br>Description from the FOM: <i>Specifies Cone 1 geometry record alternative</i>
      * <br>Description of the data type from the FOM: <i>Record specifying Cone 1 geometry record</i>
      *
      * @return Cone1GeometryData value
      */
      LIBAPI DevStudio::Cone1GeomRecStructPtr getCone1GeometryData() const {
         return _cone1GeometryData;
      }

      /**
      * Function to set Cone1GeometryData.
      * Note that this will set the discriminant to <code>ConeRecord1Type</code>.
      * <br>Description from the FOM: <i>Specifies Cone 1 geometry record alternative</i>
      * <br>Description of the data type from the FOM: <i>Record specifying Cone 1 geometry record</i>
      *
      * @param cone1GeometryData value used to create a Cone1GeomRecStructPtr
      */
      LIBAPI void setCone1GeometryData(const DevStudio::Cone1GeomRecStruct cone1GeometryData) {
         _cone1GeometryData = DevStudio::Cone1GeomRecStructPtr( new DevStudio::Cone1GeomRecStruct (cone1GeometryData));
         _discriminant = EnvironmentRecordTypeEnum::CONE_RECORD1TYPE;
      }

      /**
      * Function to get Cone2GeometryData.
      * Note that this field is only valid of the discriminant is <code>ConeRecord2Type</code>.
      * <br>Description from the FOM: <i>Specifies Cone 2 geometry record alternative</i>
      * <br>Description of the data type from the FOM: <i>Record specifying Cone 2 geometry record</i>
      *
      * @return Cone2GeometryData value
      */
      LIBAPI DevStudio::Cone2GeomRecStructPtr getCone2GeometryData() const {
         return _cone2GeometryData;
      }

      /**
      * Function to set Cone2GeometryData.
      * Note that this will set the discriminant to <code>ConeRecord2Type</code>.
      * <br>Description from the FOM: <i>Specifies Cone 2 geometry record alternative</i>
      * <br>Description of the data type from the FOM: <i>Record specifying Cone 2 geometry record</i>
      *
      * @param cone2GeometryData value used to create a Cone2GeomRecStructPtr
      */
      LIBAPI void setCone2GeometryData(const DevStudio::Cone2GeomRecStruct cone2GeometryData) {
         _cone2GeometryData = DevStudio::Cone2GeomRecStructPtr( new DevStudio::Cone2GeomRecStruct (cone2GeometryData));
         _discriminant = EnvironmentRecordTypeEnum::CONE_RECORD2TYPE;
      }

      /**
      * Function to get RectVol1GeometryData.
      * Note that this field is only valid of the discriminant is <code>RectangularVolRecord1Type</code>.
      * <br>Description from the FOM: <i>Specifies Rectangular Volume 1 geometry record alternative</i>
      * <br>Description of the data type from the FOM: <i>Record specifying Rectangular Volume 1 geometry record</i>
      *
      * @return RectVol1GeometryData value
      */
      LIBAPI DevStudio::RectVol1GeomRecStructPtr getRectVol1GeometryData() const {
         return _rectVol1GeometryData;
      }

      /**
      * Function to set RectVol1GeometryData.
      * Note that this will set the discriminant to <code>RectangularVolRecord1Type</code>.
      * <br>Description from the FOM: <i>Specifies Rectangular Volume 1 geometry record alternative</i>
      * <br>Description of the data type from the FOM: <i>Record specifying Rectangular Volume 1 geometry record</i>
      *
      * @param rectVol1GeometryData value used to create a RectVol1GeomRecStructPtr
      */
      LIBAPI void setRectVol1GeometryData(const DevStudio::RectVol1GeomRecStruct rectVol1GeometryData) {
         _rectVol1GeometryData = DevStudio::RectVol1GeomRecStructPtr( new DevStudio::RectVol1GeomRecStruct (rectVol1GeometryData));
         _discriminant = EnvironmentRecordTypeEnum::RECTANGULAR_VOL_RECORD1TYPE;
      }

      /**
      * Function to get RectVol2GeometryData.
      * Note that this field is only valid of the discriminant is <code>RectangularVolRecord2Type</code>.
      * <br>Description from the FOM: <i>Specifies Rectangular Volume 2 geometry record alternative</i>
      * <br>Description of the data type from the FOM: <i>Record specifying Rectangular Volume 2 geometry record</i>
      *
      * @return RectVol2GeometryData value
      */
      LIBAPI DevStudio::RectVol2GeomRecStructPtr getRectVol2GeometryData() const {
         return _rectVol2GeometryData;
      }

      /**
      * Function to set RectVol2GeometryData.
      * Note that this will set the discriminant to <code>RectangularVolRecord2Type</code>.
      * <br>Description from the FOM: <i>Specifies Rectangular Volume 2 geometry record alternative</i>
      * <br>Description of the data type from the FOM: <i>Record specifying Rectangular Volume 2 geometry record</i>
      *
      * @param rectVol2GeometryData value used to create a RectVol2GeomRecStructPtr
      */
      LIBAPI void setRectVol2GeometryData(const DevStudio::RectVol2GeomRecStruct rectVol2GeometryData) {
         _rectVol2GeometryData = DevStudio::RectVol2GeomRecStructPtr( new DevStudio::RectVol2GeomRecStruct (rectVol2GeometryData));
         _discriminant = EnvironmentRecordTypeEnum::RECTANGULAR_VOL_RECORD2TYPE;
      }

      /**
      * Function to get GaussPlumeGeometryData.
      * Note that this field is only valid of the discriminant is <code>GaussianPlumeRecordType</code>.
      * <br>Description from the FOM: <i>Specifies Gaussian Plume geometry record alternative</i>
      * <br>Description of the data type from the FOM: <i>Record specifying Gaussian Plume geometry record</i>
      *
      * @return GaussPlumeGeometryData value
      */
      LIBAPI DevStudio::GaussPlumeGeomRecStructPtr getGaussPlumeGeometryData() const {
         return _gaussPlumeGeometryData;
      }

      /**
      * Function to set GaussPlumeGeometryData.
      * Note that this will set the discriminant to <code>GaussianPlumeRecordType</code>.
      * <br>Description from the FOM: <i>Specifies Gaussian Plume geometry record alternative</i>
      * <br>Description of the data type from the FOM: <i>Record specifying Gaussian Plume geometry record</i>
      *
      * @param gaussPlumeGeometryData value used to create a GaussPlumeGeomRecStructPtr
      */
      LIBAPI void setGaussPlumeGeometryData(const DevStudio::GaussPlumeGeomRecStruct gaussPlumeGeometryData) {
         _gaussPlumeGeometryData = DevStudio::GaussPlumeGeomRecStructPtr( new DevStudio::GaussPlumeGeomRecStruct (gaussPlumeGeometryData));
         _discriminant = EnvironmentRecordTypeEnum::GAUSSIAN_PLUME_RECORD_TYPE;
      }

      /**
      * Function to get GaussPuffGeometryData.
      * Note that this field is only valid of the discriminant is <code>GaussianPuffRecordType</code>.
      * <br>Description from the FOM: <i>Specifies Gaussian Puff geometry record alternative</i>
      * <br>Description of the data type from the FOM: <i>Record specifying Gaussian Puff geometry record</i>
      *
      * @return GaussPuffGeometryData value
      */
      LIBAPI DevStudio::GaussPuffGeomRecStructPtr getGaussPuffGeometryData() const {
         return _gaussPuffGeometryData;
      }

      /**
      * Function to set GaussPuffGeometryData.
      * Note that this will set the discriminant to <code>GaussianPuffRecordType</code>.
      * <br>Description from the FOM: <i>Specifies Gaussian Puff geometry record alternative</i>
      * <br>Description of the data type from the FOM: <i>Record specifying Gaussian Puff geometry record</i>
      *
      * @param gaussPuffGeometryData value used to create a GaussPuffGeomRecStructPtr
      */
      LIBAPI void setGaussPuffGeometryData(const DevStudio::GaussPuffGeomRecStruct gaussPuffGeometryData) {
         _gaussPuffGeometryData = DevStudio::GaussPuffGeomRecStructPtr( new DevStudio::GaussPuffGeomRecStruct (gaussPuffGeometryData));
         _discriminant = EnvironmentRecordTypeEnum::GAUSSIAN_PUFF_RECORD_TYPE;
      }

      /**
      * Function to get UniformGeometryData.
      * Note that this field is only valid of the discriminant is <code>UniformGeometryRecordType</code>.
      * <br>Description from the FOM: <i>Specifies Uniform geometry record alternative</i>
      * <br>Description of the data type from the FOM: <i>Record specifying Uniform geometry record</i>
      *
      * @return UniformGeometryData value
      */
      LIBAPI DevStudio::UniformGeomRecStructPtr getUniformGeometryData() const {
         return _uniformGeometryData;
      }

      /**
      * Function to set UniformGeometryData.
      * Note that this will set the discriminant to <code>UniformGeometryRecordType</code>.
      * <br>Description from the FOM: <i>Specifies Uniform geometry record alternative</i>
      * <br>Description of the data type from the FOM: <i>Record specifying Uniform geometry record</i>
      *
      * @param uniformGeometryData value used to create a UniformGeomRecStructPtr
      */
      LIBAPI void setUniformGeometryData(const DevStudio::UniformGeomRecStruct uniformGeometryData) {
         _uniformGeometryData = DevStudio::UniformGeomRecStructPtr( new DevStudio::UniformGeomRecStruct (uniformGeometryData));
         _discriminant = EnvironmentRecordTypeEnum::UNIFORM_GEOMETRY_RECORD_TYPE;
      }

      /**
      * Function to get COMBICStateData.
      * Note that this field is only valid of the discriminant is <code>COMBICStateRecordType</code>.
      * <br>Description from the FOM: <i>Specifies COMBIC state record alternative</i>
      * <br>Description of the data type from the FOM: <i>Record specifying COMBIC state record</i>
      *
      * @return COMBICStateData value
      */
      LIBAPI DevStudio::COMBICStateRecStructPtr getCOMBICStateData() const {
         return _cOMBICStateData;
      }

      /**
      * Function to set COMBICStateData.
      * Note that this will set the discriminant to <code>COMBICStateRecordType</code>.
      * <br>Description from the FOM: <i>Specifies COMBIC state record alternative</i>
      * <br>Description of the data type from the FOM: <i>Record specifying COMBIC state record</i>
      *
      * @param cOMBICStateData value used to create a COMBICStateRecStructPtr
      */
      LIBAPI void setCOMBICStateData(const DevStudio::COMBICStateRecStruct cOMBICStateData) {
         _cOMBICStateData = DevStudio::COMBICStateRecStructPtr( new DevStudio::COMBICStateRecStruct (cOMBICStateData));
         _discriminant = EnvironmentRecordTypeEnum::COMBICSTATE_RECORD_TYPE;
      }

      /**
      * Function to get FlareStateData.
      * Note that this field is only valid of the discriminant is <code>FlareStateRecordType</code>.
      * <br>Description from the FOM: <i>Specifies Flare state record alternative</i>
      * <br>Description of the data type from the FOM: <i>Record specifying Flare state record</i>
      *
      * @return FlareStateData value
      */
      LIBAPI DevStudio::FlareStateRecStructPtr getFlareStateData() const {
         return _flareStateData;
      }

      /**
      * Function to set FlareStateData.
      * Note that this will set the discriminant to <code>FlareStateRecordType</code>.
      * <br>Description from the FOM: <i>Specifies Flare state record alternative</i>
      * <br>Description of the data type from the FOM: <i>Record specifying Flare state record</i>
      *
      * @param flareStateData value used to create a FlareStateRecStructPtr
      */
      LIBAPI void setFlareStateData(const DevStudio::FlareStateRecStruct flareStateData) {
         _flareStateData = DevStudio::FlareStateRecStructPtr( new DevStudio::FlareStateRecStruct (flareStateData));
         _discriminant = EnvironmentRecordTypeEnum::FLARE_STATE_RECORD_TYPE;
      }

      /**
      * Function to get RectVol3GeometryData.
      * Note that this field is only valid of the discriminant is <code>RectangularVolRecord3Type</code>.
      * <br>Description from the FOM: <i>Specifies Rectangular Volume 3 geometry record alternative</i>
      * <br>Description of the data type from the FOM: <i>Record specifying Rectangular Volume 3 geometry record</i>
      *
      * @return RectVol3GeometryData value
      */
      LIBAPI DevStudio::RectVol3GeomRecStructPtr getRectVol3GeometryData() const {
         return _rectVol3GeometryData;
      }

      /**
      * Function to set RectVol3GeometryData.
      * Note that this will set the discriminant to <code>RectangularVolRecord3Type</code>.
      * <br>Description from the FOM: <i>Specifies Rectangular Volume 3 geometry record alternative</i>
      * <br>Description of the data type from the FOM: <i>Record specifying Rectangular Volume 3 geometry record</i>
      *
      * @param rectVol3GeometryData value used to create a RectVol3GeomRecStructPtr
      */
      LIBAPI void setRectVol3GeometryData(const DevStudio::RectVol3GeomRecStruct rectVol3GeometryData) {
         _rectVol3GeometryData = DevStudio::RectVol3GeomRecStructPtr( new DevStudio::RectVol3GeomRecStruct (rectVol3GeometryData));
         _discriminant = EnvironmentRecordTypeEnum::RECTANGULAR_VOL_RECORD3TYPE;
      }

      /** Description from the FOM: <i>Specifies Point 1 geometry record alternative</i> */
      DevStudio::WorldLocationStructPtr _point1GeometryData;
      /** Description from the FOM: <i>Specifies Point 2 geometry record alternative</i> */
      DevStudio::Point2GeomRecStructPtr _point2GeometryData;
      /** Description from the FOM: <i>Specifies Line 1 geometry record alternative</i> */
      DevStudio::Line1GeomRecStructPtr _line1GeometryData;
      /** Description from the FOM: <i>Specifies Line 2 geometry record alternative</i> */
      DevStudio::Line2GeomRecStructPtr _line2GeometryData;
      /** Description from the FOM: <i>Specifies Bounding Sphere geometry record alternative</i> */
      DevStudio::Sphere1GeomRecStructPtr _boundingSphereGeometryData;
      /** Description from the FOM: <i>Specifies Sphere 1 geometry record alternative</i> */
      DevStudio::Sphere1GeomRecStructPtr _sphere1GeometryData;
      /** Description from the FOM: <i>Specifies Sphere 2 geometry record alternative</i> */
      DevStudio::Sphere2GeomRecStructPtr _sphere2GeometryData;
      /** Description from the FOM: <i>Specifies Ellipsoid 1 geometry record alternative</i> */
      DevStudio::Ellipsoid1GeomRecStructPtr _ellipsoid1GeometryData;
      /** Description from the FOM: <i>Specifies Ellipsoid 2 geometry record alternative</i> */
      DevStudio::Ellipsoid2GeomRecStructPtr _ellipsoid2GeometryData;
      /** Description from the FOM: <i>Specifies Cone 1 geometry record alternative</i> */
      DevStudio::Cone1GeomRecStructPtr _cone1GeometryData;
      /** Description from the FOM: <i>Specifies Cone 2 geometry record alternative</i> */
      DevStudio::Cone2GeomRecStructPtr _cone2GeometryData;
      /** Description from the FOM: <i>Specifies Rectangular Volume 1 geometry record alternative</i> */
      DevStudio::RectVol1GeomRecStructPtr _rectVol1GeometryData;
      /** Description from the FOM: <i>Specifies Rectangular Volume 2 geometry record alternative</i> */
      DevStudio::RectVol2GeomRecStructPtr _rectVol2GeometryData;
      /** Description from the FOM: <i>Specifies Gaussian Plume geometry record alternative</i> */
      DevStudio::GaussPlumeGeomRecStructPtr _gaussPlumeGeometryData;
      /** Description from the FOM: <i>Specifies Gaussian Puff geometry record alternative</i> */
      DevStudio::GaussPuffGeomRecStructPtr _gaussPuffGeometryData;
      /** Description from the FOM: <i>Specifies Uniform geometry record alternative</i> */
      DevStudio::UniformGeomRecStructPtr _uniformGeometryData;
      /** Description from the FOM: <i>Specifies COMBIC state record alternative</i> */
      DevStudio::COMBICStateRecStructPtr _cOMBICStateData;
      /** Description from the FOM: <i>Specifies Flare state record alternative</i> */
      DevStudio::FlareStateRecStructPtr _flareStateData;
      /** Description from the FOM: <i>Specifies Rectangular Volume 3 geometry record alternative</i> */
      DevStudio::RectVol3GeomRecStructPtr _rectVol3GeometryData;

   private:
      DevStudio::EnvironmentRecordTypeEnum::EnvironmentRecordTypeEnum _discriminant;

      EnvironmentRecVariantStruct(
         DevStudio::WorldLocationStructPtr point1GeometryData,
         DevStudio::Point2GeomRecStructPtr point2GeometryData,
         DevStudio::Line1GeomRecStructPtr line1GeometryData,
         DevStudio::Line2GeomRecStructPtr line2GeometryData,
         DevStudio::Sphere1GeomRecStructPtr boundingSphereGeometryData,
         DevStudio::Sphere1GeomRecStructPtr sphere1GeometryData,
         DevStudio::Sphere2GeomRecStructPtr sphere2GeometryData,
         DevStudio::Ellipsoid1GeomRecStructPtr ellipsoid1GeometryData,
         DevStudio::Ellipsoid2GeomRecStructPtr ellipsoid2GeometryData,
         DevStudio::Cone1GeomRecStructPtr cone1GeometryData,
         DevStudio::Cone2GeomRecStructPtr cone2GeometryData,
         DevStudio::RectVol1GeomRecStructPtr rectVol1GeometryData,
         DevStudio::RectVol2GeomRecStructPtr rectVol2GeometryData,
         DevStudio::GaussPlumeGeomRecStructPtr gaussPlumeGeometryData,
         DevStudio::GaussPuffGeomRecStructPtr gaussPuffGeometryData,
         DevStudio::UniformGeomRecStructPtr uniformGeometryData,
         DevStudio::COMBICStateRecStructPtr cOMBICStateData,
         DevStudio::FlareStateRecStructPtr flareStateData,
         DevStudio::RectVol3GeomRecStructPtr rectVol3GeometryData,
         DevStudio::EnvironmentRecordTypeEnum::EnvironmentRecordTypeEnum discriminant
      );
   };

   LIBAPI bool operator ==(const DevStudio::EnvironmentRecVariantStruct& l, const DevStudio::EnvironmentRecVariantStruct& r);
   LIBAPI bool operator !=(const DevStudio::EnvironmentRecVariantStruct& l, const DevStudio::EnvironmentRecVariantStruct& r);
   LIBAPI bool operator <(const DevStudio::EnvironmentRecVariantStruct& l, const DevStudio::EnvironmentRecVariantStruct& r);
   LIBAPI bool operator >(const DevStudio::EnvironmentRecVariantStruct& l, const DevStudio::EnvironmentRecVariantStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::EnvironmentRecVariantStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::EnvironmentRecVariantStruct const &);
}

#endif
