/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_ENVIRONMENTDATAREPRESENTATIONENUM_H
#define DEVELOPER_STUDIO_DATATYPES_ENVIRONMENTDATAREPRESENTATIONENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace EnvironmentDataRepresentationEnum {
        /**
        * Implementation of the <code>EnvironmentDataRepresentationEnum16</code> data type from the FOM.
        * <br>Description from the FOM: <i>Environment data representation type</i>
        */
        enum EnvironmentDataRepresentationEnum {
            /** <code>EnvironmentDataType0</code> (with ordinal 0) */
            ENVIRONMENT_DATA_TYPE0 = 0,
            /** <code>EnvironmentDataType1</code> (with ordinal 1) */
            ENVIRONMENT_DATA_TYPE1 = 1,
            /** <code>EnvironmentDataType2</code> (with ordinal 2) */
            ENVIRONMENT_DATA_TYPE2 = 2
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to EnvironmentDataRepresentationEnum: static_cast<DevStudio::EnvironmentDataRepresentationEnum::EnvironmentDataRepresentationEnum>(i)
        */
        LIBAPI bool isValid(const EnvironmentDataRepresentationEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::EnvironmentDataRepresentationEnum::EnvironmentDataRepresentationEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::EnvironmentDataRepresentationEnum::EnvironmentDataRepresentationEnum const &);
}


#endif
