/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_NAMEDLOCATIONSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_NAMEDLOCATIONSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/StationNameLocationVariantStruct.h>

namespace DevStudio {
   /**
   * Implementation of the <code>NamedLocationStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>The discrete positional relationship of the constituent part object with respect to its host object. Based on the specifications in IEEE 1278.1a-1998 of the IsPartOf PDU 'Location of Part' (paragraph 5.3.9.4e) and 'Named Location' (paragraph 5.3.9.4f) fields.</i>
   */
   class NamedLocationStruct {

   public:
      /**
      * Description from the FOM: <i>The number of the particular station at which the constituent part is attached.</i>.
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^15, 2^15-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      short stationNumber;
      /**
      * Description from the FOM: <i>The name of the station where the constituent part is located.</i>.
      * <br>Description of the data type from the FOM: <i>The station name at which the constituent part is located. In case of 'On Station', the alternative specifies its location relative to the host object.</i>
      */
      StationNameLocationVariantStruct stationName;

      LIBAPI NamedLocationStruct()
         :
         stationNumber(0),
         stationName(StationNameLocationVariantStruct())
      {}

      /**
      * Constructor for NamedLocationStruct
      *
      * @param stationNumber_ value to set as stationNumber.
      * <br>Description from the FOM: <i>The number of the particular station at which the constituent part is attached.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^15, 2^15-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param stationName_ value to set as stationName.
      * <br>Description from the FOM: <i>The name of the station where the constituent part is located.</i>
      * <br>Description of the data type from the FOM: <i>The station name at which the constituent part is located. In case of 'On Station', the alternative specifies its location relative to the host object.</i>
      */
      LIBAPI NamedLocationStruct(
         short stationNumber_,
         StationNameLocationVariantStruct stationName_
         )
         :
         stationNumber(stationNumber_),
         stationName(stationName_)
      {}



      /**
      * Function to get stationNumber.
      * <br>Description from the FOM: <i>The number of the particular station at which the constituent part is attached.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^15, 2^15-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return stationNumber
      */
      LIBAPI short & getStationNumber() {
         return stationNumber;
      }

      /**
      * Function to get stationName.
      * <br>Description from the FOM: <i>The name of the station where the constituent part is located.</i>
      * <br>Description of the data type from the FOM: <i>The station name at which the constituent part is located. In case of 'On Station', the alternative specifies its location relative to the host object.</i>
      *
      * @return stationName
      */
      LIBAPI DevStudio::StationNameLocationVariantStruct & getStationName() {
         return stationName;
      }

   };


   LIBAPI bool operator ==(const DevStudio::NamedLocationStruct& l, const DevStudio::NamedLocationStruct& r);
   LIBAPI bool operator !=(const DevStudio::NamedLocationStruct& l, const DevStudio::NamedLocationStruct& r);
   LIBAPI bool operator <(const DevStudio::NamedLocationStruct& l, const DevStudio::NamedLocationStruct& r);
   LIBAPI bool operator >(const DevStudio::NamedLocationStruct& l, const DevStudio::NamedLocationStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::NamedLocationStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::NamedLocationStruct const &);
}
#endif
