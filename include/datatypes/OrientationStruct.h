/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_ORIENTATIONSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_ORIENTATIONSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <RtiDriver/RprUtility/RprUtility.h>


namespace DevStudio {
   /**
   * Implementation of the <code>OrientationStruct</code> data type from the FOM.
   * This datatype can be used with the RprUtility package. Please see the Overview document
   * located in the project root directory for more information.
   * <br>Description from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
   */
   class OrientationStruct {

   public:
      /**
      * Description from the FOM: <i>Rotation about the Z axis.</i>.
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      */
      float psi;
      /**
      * Description from the FOM: <i>Rotation about the Y axis.</i>.
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      */
      float theta;
      /**
      * Description from the FOM: <i>Rotation about the X axis.</i>.
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      */
      float phi;

      LIBAPI OrientationStruct()
         :
         psi(0),
         theta(0),
         phi(0)
      {}

      /**
      * Constructor for OrientationStruct
      *
      * @param psi_ value to set as psi.
      * <br>Description from the FOM: <i>Rotation about the Z axis.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      * @param theta_ value to set as theta.
      * <br>Description from the FOM: <i>Rotation about the Y axis.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      * @param phi_ value to set as phi.
      * <br>Description from the FOM: <i>Rotation about the X axis.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      */
      LIBAPI OrientationStruct(
         float psi_,
         float theta_,
         float phi_
         )
         :
         psi(psi_),
         theta(theta_),
         phi(phi_)
      {}

      /**
      * Convert to RprUtility datatype
      *
      * @param orientation OrientationStruct to convert
      *
      * @return orientation converted to RprUtility::Orientation
      */
      LIBAPI static RprUtility::Orientation convert(const OrientationStruct orientation);

      /**
      * Convert from RprUtility datatype
      *
      * @param orientation RprUtility::Orientation to convert to OrientationStruct
      *
      * @return orientation converted to OrientationStruct
      */
      LIBAPI static OrientationStruct convert(const RprUtility::Orientation orientation);


      /**
      * Function to get psi.
      * <br>Description from the FOM: <i>Rotation about the Z axis.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @return psi
      */
      LIBAPI float & getPsi() {
         return psi;
      }

      /**
      * Function to get theta.
      * <br>Description from the FOM: <i>Rotation about the Y axis.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @return theta
      */
      LIBAPI float & getTheta() {
         return theta;
      }

      /**
      * Function to get phi.
      * <br>Description from the FOM: <i>Rotation about the X axis.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @return phi
      */
      LIBAPI float & getPhi() {
         return phi;
      }

   };


   LIBAPI bool operator ==(const DevStudio::OrientationStruct& l, const DevStudio::OrientationStruct& r);
   LIBAPI bool operator !=(const DevStudio::OrientationStruct& l, const DevStudio::OrientationStruct& r);
   LIBAPI bool operator <(const DevStudio::OrientationStruct& l, const DevStudio::OrientationStruct& r);
   LIBAPI bool operator >(const DevStudio::OrientationStruct& l, const DevStudio::OrientationStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::OrientationStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::OrientationStruct const &);
}
#endif
