/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_MINEFIELDFUSINGENUM_H
#define DEVELOPER_STUDIO_DATATYPES_MINEFIELDFUSINGENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace MinefieldFusingEnum {
        /**
        * Implementation of the <code>MinefieldFusingEnum32</code> data type from the FOM.
        * <br>Description from the FOM: <i>Minefield fuse type</i>
        */
        enum MinefieldFusingEnum {
            /** <code>NoFuse</code> (with ordinal 0) */
            NO_FUSE = 0,
            /** <code>Other</code> (with ordinal 1) */
            OTHER = 1,
            /** <code>Pressure</code> (with ordinal 2) */
            PRESSURE = 2,
            /** <code>Magnetic</code> (with ordinal 3) */
            MAGNETIC = 3,
            /** <code>TiltRod</code> (with ordinal 4) */
            TILT_ROD = 4,
            /** <code>Command</code> (with ordinal 5) */
            COMMAND = 5,
            /** <code>TripWire</code> (with ordinal 6) */
            TRIP_WIRE = 6
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to MinefieldFusingEnum: static_cast<DevStudio::MinefieldFusingEnum::MinefieldFusingEnum>(i)
        */
        LIBAPI bool isValid(const MinefieldFusingEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::MinefieldFusingEnum::MinefieldFusingEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::MinefieldFusingEnum::MinefieldFusingEnum const &);
}


#endif
