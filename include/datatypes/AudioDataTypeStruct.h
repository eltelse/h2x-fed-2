/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_AUDIODATATYPESTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_AUDIODATATYPESTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/EncodingTypeEnum.h>
#include <DevStudio/datatypes/SignalDataLengthlessArray1Plus.h>
#include <vector>

namespace DevStudio {
   /**
   * Implementation of the <code>AudioDataTypeStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Specifies an encoded audio radio signal.</i>
   */
   class AudioDataTypeStruct {

   public:
      /**
      * Description from the FOM: <i>Identifier for the audio stream. This must be globally unique.</i>.
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^64-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      unsigned long long streamTag;
      /**
      * Description from the FOM: <i>The type of encoding used for the audio data</i>.
      * <br>Description of the data type from the FOM: <i>Radio signal encoding type</i>
      */
      EncodingTypeEnum::EncodingTypeEnum encodingType;
      /**
      * Description from the FOM: <i>The number of samples per second in the audio data.</i>.
      * <br>Description of the data type from the FOM: <i>Rate of transmission, in bits per second. [unit: bit/second, resolution: 1, accuracy: perfect]</i>
      */
      unsigned int sampleRate;
      /**
      * Description from the FOM: <i>The length of the signal data.</i>.
      * <br>Description of the data type from the FOM: <i>Transmission size, in number of bits. [unit: bit, resolution: 1, accuracy: perfect]</i>
      */
      unsigned short dataLength;
      /**
      * Description from the FOM: <i>The number of samples in this transmission.</i>.
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      unsigned int sampleCount;
      /**
      * Description from the FOM: <i>The signal data.</i>.
      * <br>Description of the data type from the FOM: <i>The audio or digital data conveyed in a radio transmission.</i>
      */
      std::vector</* not empty */ char > data;

      LIBAPI AudioDataTypeStruct()
         :
         streamTag(0),
         encodingType(EncodingTypeEnum::EncodingTypeEnum()),
         sampleRate(0),
         dataLength(0),
         sampleCount(0),
         data(0)
      {}

      /**
      * Constructor for AudioDataTypeStruct
      *
      * @param streamTag_ value to set as streamTag.
      * <br>Description from the FOM: <i>Identifier for the audio stream. This must be globally unique.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^64-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param encodingType_ value to set as encodingType.
      * <br>Description from the FOM: <i>The type of encoding used for the audio data</i>
      * <br>Description of the data type from the FOM: <i>Radio signal encoding type</i>
      * @param sampleRate_ value to set as sampleRate.
      * <br>Description from the FOM: <i>The number of samples per second in the audio data.</i>
      * <br>Description of the data type from the FOM: <i>Rate of transmission, in bits per second. [unit: bit/second, resolution: 1, accuracy: perfect]</i>
      * @param dataLength_ value to set as dataLength.
      * <br>Description from the FOM: <i>The length of the signal data.</i>
      * <br>Description of the data type from the FOM: <i>Transmission size, in number of bits. [unit: bit, resolution: 1, accuracy: perfect]</i>
      * @param sampleCount_ value to set as sampleCount.
      * <br>Description from the FOM: <i>The number of samples in this transmission.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param data_ value to set as data.
      * <br>Description from the FOM: <i>The signal data.</i>
      * <br>Description of the data type from the FOM: <i>The audio or digital data conveyed in a radio transmission.</i>
      */
      LIBAPI AudioDataTypeStruct(
         unsigned long long streamTag_,
         EncodingTypeEnum::EncodingTypeEnum encodingType_,
         unsigned int sampleRate_,
         unsigned short dataLength_,
         unsigned int sampleCount_,
         std::vector</* not empty */ char > data_
         )
         :
         streamTag(streamTag_),
         encodingType(encodingType_),
         sampleRate(sampleRate_),
         dataLength(dataLength_),
         sampleCount(sampleCount_),
         data(data_)
      {}



      /**
      * Function to get streamTag.
      * <br>Description from the FOM: <i>Identifier for the audio stream. This must be globally unique.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^64-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return streamTag
      */
      LIBAPI unsigned long long & getStreamTag() {
         return streamTag;
      }

      /**
      * Function to get encodingType.
      * <br>Description from the FOM: <i>The type of encoding used for the audio data</i>
      * <br>Description of the data type from the FOM: <i>Radio signal encoding type</i>
      *
      * @return encodingType
      */
      LIBAPI DevStudio::EncodingTypeEnum::EncodingTypeEnum & getEncodingType() {
         return encodingType;
      }

      /**
      * Function to get sampleRate.
      * <br>Description from the FOM: <i>The number of samples per second in the audio data.</i>
      * <br>Description of the data type from the FOM: <i>Rate of transmission, in bits per second. [unit: bit/second, resolution: 1, accuracy: perfect]</i>
      *
      * @return sampleRate
      */
      LIBAPI unsigned int & getSampleRate() {
         return sampleRate;
      }

      /**
      * Function to get dataLength.
      * <br>Description from the FOM: <i>The length of the signal data.</i>
      * <br>Description of the data type from the FOM: <i>Transmission size, in number of bits. [unit: bit, resolution: 1, accuracy: perfect]</i>
      *
      * @return dataLength
      */
      LIBAPI unsigned short & getDataLength() {
         return dataLength;
      }

      /**
      * Function to get sampleCount.
      * <br>Description from the FOM: <i>The number of samples in this transmission.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return sampleCount
      */
      LIBAPI unsigned int & getSampleCount() {
         return sampleCount;
      }

      /**
      * Function to get data.
      * <br>Description from the FOM: <i>The signal data.</i>
      * <br>Description of the data type from the FOM: <i>The audio or digital data conveyed in a radio transmission.</i>
      *
      * @return data
      */
      LIBAPI std::vector</* not empty */ char > & getData() {
         return data;
      }

   };


   LIBAPI bool operator ==(const DevStudio::AudioDataTypeStruct& l, const DevStudio::AudioDataTypeStruct& r);
   LIBAPI bool operator !=(const DevStudio::AudioDataTypeStruct& l, const DevStudio::AudioDataTypeStruct& r);
   LIBAPI bool operator <(const DevStudio::AudioDataTypeStruct& l, const DevStudio::AudioDataTypeStruct& r);
   LIBAPI bool operator >(const DevStudio::AudioDataTypeStruct& l, const DevStudio::AudioDataTypeStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::AudioDataTypeStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::AudioDataTypeStruct const &);
}
#endif
