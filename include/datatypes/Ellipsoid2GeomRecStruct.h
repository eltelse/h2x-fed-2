/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_ELLIPSOID2GEOMRECSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_ELLIPSOID2GEOMRECSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/AngularVelocityVectorStruct.h>
#include <DevStudio/datatypes/DimensionStruct.h>
#include <DevStudio/datatypes/OctetArray4.h>
#include <DevStudio/datatypes/OrientationStruct.h>
#include <DevStudio/datatypes/VelocityVectorStruct.h>
#include <DevStudio/datatypes/WorldLocationStruct.h>
#include <vector>

namespace DevStudio {
   /**
   * Implementation of the <code>Ellipsoid2GeomRecStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Record specifying Ellipsoid 2 geometry record</i>
   */
   class Ellipsoid2GeomRecStruct {

   public:
      /**
      * Description from the FOM: <i>Centroid location X, Y, Z</i>.
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      */
      WorldLocationStruct centroidLocation;
      /**
      * Description from the FOM: <i>Sigma dimensions</i>.
      * <br>Description of the data type from the FOM: <i>Bounding box in X,Y,Z axis.</i>
      */
      DimensionStruct sigmaValue;
      /**
      * Description from the FOM: <i>Variation of sigma dimensions</i>.
      * <br>Description of the data type from the FOM: <i>The rate at which the position is changing over time.</i>
      */
      VelocityVectorStruct sigmaRate;
      /**
      * Description from the FOM: <i>Orientation, specified by Euler angles</i>.
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      */
      OrientationStruct orientation;
      /**
      * Description from the FOM: <i>Velocity Vx, Vy, Vz</i>.
      * <br>Description of the data type from the FOM: <i>The rate at which the position is changing over time.</i>
      */
      VelocityVectorStruct velocity;
      /**
      * Description from the FOM: <i>Angular velocity Vx, Vy, Vz</i>.
      * <br>Description of the data type from the FOM: <i>The rate at which the orientation is changing over time, in body coordinates.</i>
      */
      AngularVelocityVectorStruct angularVelocity;
      /**
      * Description from the FOM: <i>Padding field</i>.
      * <br>Description of the data type from the FOM: <i>Generic array of four Octet elements.</i>
      */
      std::vector</* 4 */ char > padding;

      LIBAPI Ellipsoid2GeomRecStruct()
         :
         centroidLocation(WorldLocationStruct()),
         sigmaValue(DimensionStruct()),
         sigmaRate(VelocityVectorStruct()),
         orientation(OrientationStruct()),
         velocity(VelocityVectorStruct()),
         angularVelocity(AngularVelocityVectorStruct()),
         padding(0)
      {}

      /**
      * Constructor for Ellipsoid2GeomRecStruct
      *
      * @param centroidLocation_ value to set as centroidLocation.
      * <br>Description from the FOM: <i>Centroid location X, Y, Z</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      * @param sigmaValue_ value to set as sigmaValue.
      * <br>Description from the FOM: <i>Sigma dimensions</i>
      * <br>Description of the data type from the FOM: <i>Bounding box in X,Y,Z axis.</i>
      * @param sigmaRate_ value to set as sigmaRate.
      * <br>Description from the FOM: <i>Variation of sigma dimensions</i>
      * <br>Description of the data type from the FOM: <i>The rate at which the position is changing over time.</i>
      * @param orientation_ value to set as orientation.
      * <br>Description from the FOM: <i>Orientation, specified by Euler angles</i>
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      * @param velocity_ value to set as velocity.
      * <br>Description from the FOM: <i>Velocity Vx, Vy, Vz</i>
      * <br>Description of the data type from the FOM: <i>The rate at which the position is changing over time.</i>
      * @param angularVelocity_ value to set as angularVelocity.
      * <br>Description from the FOM: <i>Angular velocity Vx, Vy, Vz</i>
      * <br>Description of the data type from the FOM: <i>The rate at which the orientation is changing over time, in body coordinates.</i>
      * @param padding_ value to set as padding.
      * <br>Description from the FOM: <i>Padding field</i>
      * <br>Description of the data type from the FOM: <i>Generic array of four Octet elements.</i>
      */
      LIBAPI Ellipsoid2GeomRecStruct(
         WorldLocationStruct centroidLocation_,
         DimensionStruct sigmaValue_,
         VelocityVectorStruct sigmaRate_,
         OrientationStruct orientation_,
         VelocityVectorStruct velocity_,
         AngularVelocityVectorStruct angularVelocity_,
         std::vector</* 4 */ char > padding_
         )
         :
         centroidLocation(centroidLocation_),
         sigmaValue(sigmaValue_),
         sigmaRate(sigmaRate_),
         orientation(orientation_),
         velocity(velocity_),
         angularVelocity(angularVelocity_),
         padding(padding_)
      {}



      /**
      * Function to get centroidLocation.
      * <br>Description from the FOM: <i>Centroid location X, Y, Z</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @return centroidLocation
      */
      LIBAPI DevStudio::WorldLocationStruct & getCentroidLocation() {
         return centroidLocation;
      }

      /**
      * Function to get sigmaValue.
      * <br>Description from the FOM: <i>Sigma dimensions</i>
      * <br>Description of the data type from the FOM: <i>Bounding box in X,Y,Z axis.</i>
      *
      * @return sigmaValue
      */
      LIBAPI DevStudio::DimensionStruct & getSigmaValue() {
         return sigmaValue;
      }

      /**
      * Function to get sigmaRate.
      * <br>Description from the FOM: <i>Variation of sigma dimensions</i>
      * <br>Description of the data type from the FOM: <i>The rate at which the position is changing over time.</i>
      *
      * @return sigmaRate
      */
      LIBAPI DevStudio::VelocityVectorStruct & getSigmaRate() {
         return sigmaRate;
      }

      /**
      * Function to get orientation.
      * <br>Description from the FOM: <i>Orientation, specified by Euler angles</i>
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @return orientation
      */
      LIBAPI DevStudio::OrientationStruct & getOrientation() {
         return orientation;
      }

      /**
      * Function to get velocity.
      * <br>Description from the FOM: <i>Velocity Vx, Vy, Vz</i>
      * <br>Description of the data type from the FOM: <i>The rate at which the position is changing over time.</i>
      *
      * @return velocity
      */
      LIBAPI DevStudio::VelocityVectorStruct & getVelocity() {
         return velocity;
      }

      /**
      * Function to get angularVelocity.
      * <br>Description from the FOM: <i>Angular velocity Vx, Vy, Vz</i>
      * <br>Description of the data type from the FOM: <i>The rate at which the orientation is changing over time, in body coordinates.</i>
      *
      * @return angularVelocity
      */
      LIBAPI DevStudio::AngularVelocityVectorStruct & getAngularVelocity() {
         return angularVelocity;
      }

      /**
      * Function to get padding.
      * <br>Description from the FOM: <i>Padding field</i>
      * <br>Description of the data type from the FOM: <i>Generic array of four Octet elements.</i>
      *
      * @return padding
      */
      LIBAPI std::vector</* 4 */ char > & getPadding() {
         return padding;
      }

   };


   LIBAPI bool operator ==(const DevStudio::Ellipsoid2GeomRecStruct& l, const DevStudio::Ellipsoid2GeomRecStruct& r);
   LIBAPI bool operator !=(const DevStudio::Ellipsoid2GeomRecStruct& l, const DevStudio::Ellipsoid2GeomRecStruct& r);
   LIBAPI bool operator <(const DevStudio::Ellipsoid2GeomRecStruct& l, const DevStudio::Ellipsoid2GeomRecStruct& r);
   LIBAPI bool operator >(const DevStudio::Ellipsoid2GeomRecStruct& l, const DevStudio::Ellipsoid2GeomRecStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::Ellipsoid2GeomRecStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::Ellipsoid2GeomRecStruct const &);
}
#endif
