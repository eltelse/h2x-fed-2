/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_MINEFIELDSENSORTYPEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_MINEFIELDSENSORTYPEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace MinefieldSensorTypeEnum {
        /**
        * Implementation of the <code>MinefieldSensorTypeEnum32</code> data type from the FOM.
        * <br>Description from the FOM: <i>Minefield sensor type</i>
        */
        enum MinefieldSensorTypeEnum {
            /** <code>Other</code> (with ordinal 0) */
            OTHER = 0,
            /** <code>UnaidedEyeActivelySearching</code> (with ordinal 4096) */
            UNAIDED_EYE_ACTIVELY_SEARCHING = 4096,
            /** <code>UnaidedEyeNotActivelySearching</code> (with ordinal 4097) */
            UNAIDED_EYE_NOT_ACTIVELY_SEARCHING = 4097,
            /** <code>Binoculars</code> (with ordinal 4098) */
            BINOCULARS = 4098,
            /** <code>ImageIntensifier</code> (with ordinal 4099) */
            IMAGE_INTENSIFIER = 4099,
            /** <code>HMMWVOccupantActivelySearching</code> (with ordinal 4100) */
            HMMWVOCCUPANT_ACTIVELY_SEARCHING = 4100,
            /** <code>HMMWVOccupantNotActivelySearching</code> (with ordinal 4101) */
            HMMWVOCCUPANT_NOT_ACTIVELY_SEARCHING = 4101,
            /** <code>TruckOccupantActivelySearching</code> (with ordinal 4102) */
            TRUCK_OCCUPANT_ACTIVELY_SEARCHING = 4102,
            /** <code>TruckOccupantNotActivelySearching</code> (with ordinal 4103) */
            TRUCK_OCCUPANT_NOT_ACTIVELY_SEARCHING = 4103,
            /** <code>TrackedVehicleOccupantClosedHatchActivelySearching</code> (with ordinal 4104) */
            TRACKED_VEHICLE_OCCUPANT_CLOSED_HATCH_ACTIVELY_SEARCHING = 4104,
            /** <code>TrackedVehicleOccupantClosedHatchNotActivelySearching</code> (with ordinal 4105) */
            TRACKED_VEHICLE_OCCUPANT_CLOSED_HATCH_NOT_ACTIVELY_SEARCHING = 4105,
            /** <code>TrackedVehicleOccupantOpenHatchActivelySearching</code> (with ordinal 4106) */
            TRACKED_VEHICLE_OCCUPANT_OPEN_HATCH_ACTIVELY_SEARCHING = 4106,
            /** <code>TrackedVehicleOccupantOpenHatchNotActivelySearching</code> (with ordinal 4107) */
            TRACKED_VEHICLE_OCCUPANT_OPEN_HATCH_NOT_ACTIVELY_SEARCHING = 4107,
            /** <code>FLIR_Generic3_5</code> (with ordinal 8192) */
            FLIR_GENERIC3_5 = 8192,
            /** <code>FLIR_Generic8_12</code> (with ordinal 8193) */
            FLIR_GENERIC8_12 = 8193,
            /** <code>FLIR_ASTAMIDS_I</code> (with ordinal 8194) */
            FLIR_ASTAMIDS_I = 8194,
            /** <code>FLIR_ASTAMIDS_II</code> (with ordinal 8195) */
            FLIR_ASTAMIDS_II = 8195,
            /** <code>FLIR_GSTAMIDS3_5</code> (with ordinal 8196) */
            FLIR_GSTAMIDS3_5 = 8196,
            /** <code>FLIR_GSTAMIDS8_12</code> (with ordinal 8197) */
            FLIR_GSTAMIDS8_12 = 8197,
            /** <code>FLIR_HSTAMIDS3_5</code> (with ordinal 8198) */
            FLIR_HSTAMIDS3_5 = 8198,
            /** <code>FLIR_HSTAMIDS8_12</code> (with ordinal 8199) */
            FLIR_HSTAMIDS8_12 = 8199,
            /** <code>FLIR_COBRA3_5</code> (with ordinal 8200) */
            FLIR_COBRA3_5 = 8200,
            /** <code>FLIR_COBRA8_12</code> (with ordinal 8201) */
            FLIR_COBRA8_12 = 8201,
            /** <code>RADAR_Generic</code> (with ordinal 12288) */
            RADAR_GENERIC = 12288,
            /** <code>RADAR_Generic_GPR</code> (with ordinal 12289) */
            RADAR_GENERIC_GPR = 12289,
            /** <code>RADAR_GSTAMIDS_I</code> (with ordinal 12290) */
            RADAR_GSTAMIDS_I = 12290,
            /** <code>RADAR_GSTAMIDS_II</code> (with ordinal 12291) */
            RADAR_GSTAMIDS_II = 12291,
            /** <code>RADAR_HSTAMIDS_I</code> (with ordinal 12292) */
            RADAR_HSTAMIDS_I = 12292,
            /** <code>RADAR_HSTAMIDS_II</code> (with ordinal 12293) */
            RADAR_HSTAMIDS_II = 12293,
            /** <code>Magnetic_Generic</code> (with ordinal 16384) */
            MAGNETIC_GENERIC = 16384,
            /** <code>Magnetic_ANPSS_11</code> (with ordinal 16385) */
            MAGNETIC_ANPSS_11 = 16385,
            /** <code>Magnetic_ANPSS_12</code> (with ordinal 16386) */
            MAGNETIC_ANPSS_12 = 16386,
            /** <code>Magnetic_GSTAMIDS</code> (with ordinal 16389) */
            MAGNETIC_GSTAMIDS = 16389,
            /** <code>Laser_Generic</code> (with ordinal 20480) */
            LASER_GENERIC = 20480,
            /** <code>Laser_ASTAMIDS</code> (with ordinal 20481) */
            LASER_ASTAMIDS = 20481,
            /** <code>SONAR_Generic</code> (with ordinal 24576) */
            SONAR_GENERIC = 24576,
            /** <code>Physical_GenericProbe</code> (with ordinal 28672) */
            PHYSICAL_GENERIC_PROBE = 28672,
            /** <code>Physical_ProbeMetalContent</code> (with ordinal 28673) */
            PHYSICAL_PROBE_METAL_CONTENT = 28673,
            /** <code>Physical_ProbeNoMetalContent</code> (with ordinal 28674) */
            PHYSICAL_PROBE_NO_METAL_CONTENT = 28674,
            /** <code>Multispectral_Generic</code> (with ordinal 32768) */
            MULTISPECTRAL_GENERIC = 32768
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to MinefieldSensorTypeEnum: static_cast<DevStudio::MinefieldSensorTypeEnum::MinefieldSensorTypeEnum>(i)
        */
        LIBAPI bool isValid(const MinefieldSensorTypeEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::MinefieldSensorTypeEnum::MinefieldSensorTypeEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::MinefieldSensorTypeEnum::MinefieldSensorTypeEnum const &);
}


#endif
