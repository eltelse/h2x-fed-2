/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_POSITIONSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_POSITIONSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/GeographicShapeTypeEnum.h>
#include <DevStudio/datatypes/WorldLocationStruct.h>

namespace DevStudio {
   /**
   * Implementation of the <code>PositionStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>-NULL-</i>
   */
   class PositionStruct {

   public:
      /**
      * Description from the FOM: <i></i>.
      * <br>Description of the data type from the FOM: <i>-NULL-</i>
      */
      GeographicShapeTypeEnum::GeographicShapeTypeEnum shapeType;
      /**
      * Description from the FOM: <i></i>.
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      */
      float azimuthStart;
      /**
      * Description from the FOM: <i></i>.
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      */
      float azimuthEnd;
      /**
      * Description from the FOM: <i></i>.
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      */
      WorldLocationStruct center;
      /**
      * Description from the FOM: <i></i>.
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      */
      float range;
      /**
      * Description from the FOM: <i></i>.
      * <br>Description of the data type from the FOM: <i>32 bit unsigned Integer</i>
      */
      unsigned int numberOfPoints;
      /**
      * Description from the FOM: <i></i>.
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      */
      float headWidth;
      /**
      * Description from the FOM: <i></i>.
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      */
      float headLength;
      /**
      * Description from the FOM: <i></i>.
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      */
      float tailWidth;
      /**
      * Description from the FOM: <i></i>.
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      */
      float radiusLength;
      /**
      * Description from the FOM: <i></i>.
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      */
      float radiusWidth;
      /**
      * Description from the FOM: <i></i>.
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      */
      float offsetAngle;
      /**
      * Description from the FOM: <i></i>.
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      */
      float angleFrom;
      /**
      * Description from the FOM: <i></i>.
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      */
      float angleTo;

      LIBAPI PositionStruct()
         :
         shapeType(GeographicShapeTypeEnum::GeographicShapeTypeEnum()),
         azimuthStart(0),
         azimuthEnd(0),
         center(WorldLocationStruct()),
         range(0),
         numberOfPoints(0),
         headWidth(0),
         headLength(0),
         tailWidth(0),
         radiusLength(0),
         radiusWidth(0),
         offsetAngle(0),
         angleFrom(0),
         angleTo(0)
      {}

      /**
      * Constructor for PositionStruct
      *
      * @param shapeType_ value to set as shapeType.
      * <br>Description from the FOM: <i></i>
      * <br>Description of the data type from the FOM: <i>-NULL-</i>
      * @param azimuthStart_ value to set as azimuthStart.
      * <br>Description from the FOM: <i></i>
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      * @param azimuthEnd_ value to set as azimuthEnd.
      * <br>Description from the FOM: <i></i>
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      * @param center_ value to set as center.
      * <br>Description from the FOM: <i></i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      * @param range_ value to set as range.
      * <br>Description from the FOM: <i></i>
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      * @param numberOfPoints_ value to set as numberOfPoints.
      * <br>Description from the FOM: <i></i>
      * <br>Description of the data type from the FOM: <i>32 bit unsigned Integer</i>
      * @param headWidth_ value to set as headWidth.
      * <br>Description from the FOM: <i></i>
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      * @param headLength_ value to set as headLength.
      * <br>Description from the FOM: <i></i>
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      * @param tailWidth_ value to set as tailWidth.
      * <br>Description from the FOM: <i></i>
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      * @param radiusLength_ value to set as radiusLength.
      * <br>Description from the FOM: <i></i>
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      * @param radiusWidth_ value to set as radiusWidth.
      * <br>Description from the FOM: <i></i>
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      * @param offsetAngle_ value to set as offsetAngle.
      * <br>Description from the FOM: <i></i>
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      * @param angleFrom_ value to set as angleFrom.
      * <br>Description from the FOM: <i></i>
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      * @param angleTo_ value to set as angleTo.
      * <br>Description from the FOM: <i></i>
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      */
      LIBAPI PositionStruct(
         GeographicShapeTypeEnum::GeographicShapeTypeEnum shapeType_,
         float azimuthStart_,
         float azimuthEnd_,
         WorldLocationStruct center_,
         float range_,
         unsigned int numberOfPoints_,
         float headWidth_,
         float headLength_,
         float tailWidth_,
         float radiusLength_,
         float radiusWidth_,
         float offsetAngle_,
         float angleFrom_,
         float angleTo_
         )
         :
         shapeType(shapeType_),
         azimuthStart(azimuthStart_),
         azimuthEnd(azimuthEnd_),
         center(center_),
         range(range_),
         numberOfPoints(numberOfPoints_),
         headWidth(headWidth_),
         headLength(headLength_),
         tailWidth(tailWidth_),
         radiusLength(radiusLength_),
         radiusWidth(radiusWidth_),
         offsetAngle(offsetAngle_),
         angleFrom(angleFrom_),
         angleTo(angleTo_)
      {}



      /**
      * Function to get shapeType.
      * <br>Description from the FOM: <i></i>
      * <br>Description of the data type from the FOM: <i>-NULL-</i>
      *
      * @return shapeType
      */
      LIBAPI DevStudio::GeographicShapeTypeEnum::GeographicShapeTypeEnum & getShapeType() {
         return shapeType;
      }

      /**
      * Function to get azimuthStart.
      * <br>Description from the FOM: <i></i>
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      *
      * @return azimuthStart
      */
      LIBAPI float & getAzimuthStart() {
         return azimuthStart;
      }

      /**
      * Function to get azimuthEnd.
      * <br>Description from the FOM: <i></i>
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      *
      * @return azimuthEnd
      */
      LIBAPI float & getAzimuthEnd() {
         return azimuthEnd;
      }

      /**
      * Function to get center.
      * <br>Description from the FOM: <i></i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @return center
      */
      LIBAPI DevStudio::WorldLocationStruct & getCenter() {
         return center;
      }

      /**
      * Function to get range.
      * <br>Description from the FOM: <i></i>
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      *
      * @return range
      */
      LIBAPI float & getRange() {
         return range;
      }

      /**
      * Function to get numberOfPoints.
      * <br>Description from the FOM: <i></i>
      * <br>Description of the data type from the FOM: <i>32 bit unsigned Integer</i>
      *
      * @return numberOfPoints
      */
      LIBAPI unsigned int & getNumberOfPoints() {
         return numberOfPoints;
      }

      /**
      * Function to get headWidth.
      * <br>Description from the FOM: <i></i>
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      *
      * @return headWidth
      */
      LIBAPI float & getHeadWidth() {
         return headWidth;
      }

      /**
      * Function to get headLength.
      * <br>Description from the FOM: <i></i>
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      *
      * @return headLength
      */
      LIBAPI float & getHeadLength() {
         return headLength;
      }

      /**
      * Function to get tailWidth.
      * <br>Description from the FOM: <i></i>
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      *
      * @return tailWidth
      */
      LIBAPI float & getTailWidth() {
         return tailWidth;
      }

      /**
      * Function to get radiusLength.
      * <br>Description from the FOM: <i></i>
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      *
      * @return radiusLength
      */
      LIBAPI float & getRadiusLength() {
         return radiusLength;
      }

      /**
      * Function to get radiusWidth.
      * <br>Description from the FOM: <i></i>
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      *
      * @return radiusWidth
      */
      LIBAPI float & getRadiusWidth() {
         return radiusWidth;
      }

      /**
      * Function to get offsetAngle.
      * <br>Description from the FOM: <i></i>
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      *
      * @return offsetAngle
      */
      LIBAPI float & getOffsetAngle() {
         return offsetAngle;
      }

      /**
      * Function to get angleFrom.
      * <br>Description from the FOM: <i></i>
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      *
      * @return angleFrom
      */
      LIBAPI float & getAngleFrom() {
         return angleFrom;
      }

      /**
      * Function to get angleTo.
      * <br>Description from the FOM: <i></i>
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      *
      * @return angleTo
      */
      LIBAPI float & getAngleTo() {
         return angleTo;
      }

   };


   LIBAPI bool operator ==(const DevStudio::PositionStruct& l, const DevStudio::PositionStruct& r);
   LIBAPI bool operator !=(const DevStudio::PositionStruct& l, const DevStudio::PositionStruct& r);
   LIBAPI bool operator <(const DevStudio::PositionStruct& l, const DevStudio::PositionStruct& r);
   LIBAPI bool operator >(const DevStudio::PositionStruct& l, const DevStudio::PositionStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::PositionStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::PositionStruct const &);
}
#endif
