/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_ACCELERATIONVECTORSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_ACCELERATIONVECTORSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <RtiDriver/RprUtility/RprUtility.h>


namespace DevStudio {
   /**
   * Implementation of the <code>AccelerationVectorStruct</code> data type from the FOM.
   * This datatype can be used with the RprUtility package. Please see the Overview document
   * located in the project root directory for more information.
   * <br>Description from the FOM: <i>The magnitude of the change in linear velocity over time.</i>
   */
   class AccelerationVectorStruct {

   public:
      /**
      * Description from the FOM: <i>Acceleration component along the X axis.</i>.
      * <br>Description of the data type from the FOM: <i>Linear acceleration vector composed of SI base units. Based on the Linear Acceleration Vector record as specified in IEEE 1278.1-1995 section 5.2.33b. [unit: meter per second squared (m/(s^2)), resolution: NA, accuracy: NA]</i>
      */
      float xAcceleration;
      /**
      * Description from the FOM: <i>Acceleration component along the Y axis.</i>.
      * <br>Description of the data type from the FOM: <i>Linear acceleration vector composed of SI base units. Based on the Linear Acceleration Vector record as specified in IEEE 1278.1-1995 section 5.2.33b. [unit: meter per second squared (m/(s^2)), resolution: NA, accuracy: NA]</i>
      */
      float yAcceleration;
      /**
      * Description from the FOM: <i>Acceleration component along the Z axis.</i>.
      * <br>Description of the data type from the FOM: <i>Linear acceleration vector composed of SI base units. Based on the Linear Acceleration Vector record as specified in IEEE 1278.1-1995 section 5.2.33b. [unit: meter per second squared (m/(s^2)), resolution: NA, accuracy: NA]</i>
      */
      float zAcceleration;

      LIBAPI AccelerationVectorStruct()
         :
         xAcceleration(0),
         yAcceleration(0),
         zAcceleration(0)
      {}

      /**
      * Constructor for AccelerationVectorStruct
      *
      * @param xAcceleration_ value to set as xAcceleration.
      * <br>Description from the FOM: <i>Acceleration component along the X axis.</i>
      * <br>Description of the data type from the FOM: <i>Linear acceleration vector composed of SI base units. Based on the Linear Acceleration Vector record as specified in IEEE 1278.1-1995 section 5.2.33b. [unit: meter per second squared (m/(s^2)), resolution: NA, accuracy: NA]</i>
      * @param yAcceleration_ value to set as yAcceleration.
      * <br>Description from the FOM: <i>Acceleration component along the Y axis.</i>
      * <br>Description of the data type from the FOM: <i>Linear acceleration vector composed of SI base units. Based on the Linear Acceleration Vector record as specified in IEEE 1278.1-1995 section 5.2.33b. [unit: meter per second squared (m/(s^2)), resolution: NA, accuracy: NA]</i>
      * @param zAcceleration_ value to set as zAcceleration.
      * <br>Description from the FOM: <i>Acceleration component along the Z axis.</i>
      * <br>Description of the data type from the FOM: <i>Linear acceleration vector composed of SI base units. Based on the Linear Acceleration Vector record as specified in IEEE 1278.1-1995 section 5.2.33b. [unit: meter per second squared (m/(s^2)), resolution: NA, accuracy: NA]</i>
      */
      LIBAPI AccelerationVectorStruct(
         float xAcceleration_,
         float yAcceleration_,
         float zAcceleration_
         )
         :
         xAcceleration(xAcceleration_),
         yAcceleration(yAcceleration_),
         zAcceleration(zAcceleration_)
      {}

      /**
      * Convert to RprUtility datatype
      *
      * @param acceleration AccelerationVectorStruct to convert
      *
      * @return acceleration converted to RprUtility::AccelerationVector
      */
      LIBAPI static RprUtility::AccelerationVector convert(const AccelerationVectorStruct acceleration);

      /**
      * Convert from RprUtility datatype
      *
      * @param acceleration RprUtility::AccelerationVector to convert to AccelerationVectorStruct
      *
      * @return acceleration converted to AccelerationVectorStruct
      */
      LIBAPI static AccelerationVectorStruct convert(const RprUtility::AccelerationVector acceleration);


      /**
      * Function to get xAcceleration.
      * <br>Description from the FOM: <i>Acceleration component along the X axis.</i>
      * <br>Description of the data type from the FOM: <i>Linear acceleration vector composed of SI base units. Based on the Linear Acceleration Vector record as specified in IEEE 1278.1-1995 section 5.2.33b. [unit: meter per second squared (m/(s^2)), resolution: NA, accuracy: NA]</i>
      *
      * @return xAcceleration
      */
      LIBAPI float & getXAcceleration() {
         return xAcceleration;
      }

      /**
      * Function to get yAcceleration.
      * <br>Description from the FOM: <i>Acceleration component along the Y axis.</i>
      * <br>Description of the data type from the FOM: <i>Linear acceleration vector composed of SI base units. Based on the Linear Acceleration Vector record as specified in IEEE 1278.1-1995 section 5.2.33b. [unit: meter per second squared (m/(s^2)), resolution: NA, accuracy: NA]</i>
      *
      * @return yAcceleration
      */
      LIBAPI float & getYAcceleration() {
         return yAcceleration;
      }

      /**
      * Function to get zAcceleration.
      * <br>Description from the FOM: <i>Acceleration component along the Z axis.</i>
      * <br>Description of the data type from the FOM: <i>Linear acceleration vector composed of SI base units. Based on the Linear Acceleration Vector record as specified in IEEE 1278.1-1995 section 5.2.33b. [unit: meter per second squared (m/(s^2)), resolution: NA, accuracy: NA]</i>
      *
      * @return zAcceleration
      */
      LIBAPI float & getZAcceleration() {
         return zAcceleration;
      }

   };


   LIBAPI bool operator ==(const DevStudio::AccelerationVectorStruct& l, const DevStudio::AccelerationVectorStruct& r);
   LIBAPI bool operator !=(const DevStudio::AccelerationVectorStruct& l, const DevStudio::AccelerationVectorStruct& r);
   LIBAPI bool operator <(const DevStudio::AccelerationVectorStruct& l, const DevStudio::AccelerationVectorStruct& r);
   LIBAPI bool operator >(const DevStudio::AccelerationVectorStruct& l, const DevStudio::AccelerationVectorStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::AccelerationVectorStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::AccelerationVectorStruct const &);
}
#endif
