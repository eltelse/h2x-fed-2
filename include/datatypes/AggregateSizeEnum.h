/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_AGGREGATESIZEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_AGGREGATESIZEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace AggregateSizeEnum {
        /**
        * Implementation of the <code>AggregateSizeEnum32</code> data type from the FOM.
        * <br>Description from the FOM: <i>-NULL-</i>
        */
        enum AggregateSizeEnum {
            /** <code>AGGSIZE_ARMY</code> (with ordinal 0) */
            AGGSIZE_ARMY = 0,
            /** <code>AGGSIZE_COMMAND</code> (with ordinal 1) */
            AGGSIZE_COMMAND = 1,
            /** <code>AGGSIZE_CORPS</code> (with ordinal 2) */
            AGGSIZE_CORPS = 2,
            /** <code>AGGSIZE_DEVISION</code> (with ordinal 3) */
            AGGSIZE_DEVISION = 3,
            /** <code>AGGSIZE_BRIGADE</code> (with ordinal 4) */
            AGGSIZE_BRIGADE = 4,
            /** <code>AGGSIZE_SUPPORT_UNIT</code> (with ordinal 5) */
            AGGSIZE_SUPPORT_UNIT = 5,
            /** <code>AGGSIZE_BATTALION</code> (with ordinal 6) */
            AGGSIZE_BATTALION = 6,
            /** <code>AGGSIZE_COMPANY</code> (with ordinal 7) */
            AGGSIZE_COMPANY = 7,
            /** <code>AGGSIZE_PLATOON</code> (with ordinal 8) */
            AGGSIZE_PLATOON = 8,
            /** <code>AGGSIZE_SQUAD</code> (with ordinal 9) */
            AGGSIZE_SQUAD = 9,
            /** <code>AGGSIZE_TEAM</code> (with ordinal 10) */
            AGGSIZE_TEAM = 10
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to AggregateSizeEnum: static_cast<DevStudio::AggregateSizeEnum::AggregateSizeEnum>(i)
        */
        LIBAPI bool isValid(const AggregateSizeEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::AggregateSizeEnum::AggregateSizeEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::AggregateSizeEnum::AggregateSizeEnum const &);
}


#endif
