/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_EMITTERFUNCTIONENUM_H
#define DEVELOPER_STUDIO_DATATYPES_EMITTERFUNCTIONENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace EmitterFunctionEnum {
        /**
        * Implementation of the <code>EmitterFunctionEnum8</code> data type from the FOM.
        * <br>Description from the FOM: <i>Emitter system function</i>
        */
        enum EmitterFunctionEnum {
            /** <code>Other</code> (with ordinal 0) */
            OTHER = 0,
            /** <code>MultiFunction</code> (with ordinal 1) */
            MULTI_FUNCTION = 1,
            /** <code>EarlyWarningSurveillance</code> (with ordinal 2) */
            EARLY_WARNING_SURVEILLANCE = 2,
            /** <code>HeightFinding</code> (with ordinal 3) */
            HEIGHT_FINDING = 3,
            /** <code>FireControl</code> (with ordinal 4) */
            FIRE_CONTROL = 4,
            /** <code>AcquisitionDetection</code> (with ordinal 5) */
            ACQUISITION_DETECTION = 5,
            /** <code>Tracking</code> (with ordinal 6) */
            TRACKING = 6,
            /** <code>GuidanceIllumination</code> (with ordinal 7) */
            GUIDANCE_ILLUMINATION = 7,
            /** <code>FiringPointLaunchPointLocation</code> (with ordinal 8) */
            FIRING_POINT_LAUNCH_POINT_LOCATION = 8,
            /** <code>Ranging</code> (with ordinal 9) */
            RANGING = 9,
            /** <code>RadarAltimeter</code> (with ordinal 10) */
            RADAR_ALTIMETER = 10,
            /** <code>Imaging</code> (with ordinal 11) */
            IMAGING = 11,
            /** <code>MotionDetection</code> (with ordinal 12) */
            MOTION_DETECTION = 12,
            /** <code>Navigation</code> (with ordinal 13) */
            NAVIGATION = 13,
            /** <code>Weather_Meterological</code> (with ordinal 14) */
            WEATHER_METEROLOGICAL = 14,
            /** <code>Instrumentation</code> (with ordinal 15) */
            INSTRUMENTATION = 15,
            /** <code>Identification_Classification__including_IFF_</code> (with ordinal 16) */
            IDENTIFICATION_CLASSIFICATION_INCLUDING_IFF = 16,
            /** <code>AAA__Anti-Aircraft_Artillery__Fire_Control</code> (with ordinal 17) */
            AAA_ANTI_AIRCRAFT_ARTILLERY_FIRE_CONTROL = 17,
            /** <code>Air_Search_Bomb</code> (with ordinal 18) */
            AIR_SEARCH_BOMB = 18,
            /** <code>Air_Intercept</code> (with ordinal 19) */
            AIR_INTERCEPT = 19,
            /** <code>Altimeter</code> (with ordinal 20) */
            ALTIMETER = 20,
            /** <code>Air_Mapping</code> (with ordinal 21) */
            AIR_MAPPING = 21,
            /** <code>Air_Traffic_Control</code> (with ordinal 22) */
            AIR_TRAFFIC_CONTROL = 22,
            /** <code>Beacon</code> (with ordinal 23) */
            BEACON = 23,
            /** <code>Battlefield_Surveillance</code> (with ordinal 24) */
            BATTLEFIELD_SURVEILLANCE = 24,
            /** <code>Ground_Control_Approach</code> (with ordinal 25) */
            GROUND_CONTROL_APPROACH = 25,
            /** <code>Ground_Control_Intercept</code> (with ordinal 26) */
            GROUND_CONTROL_INTERCEPT = 26,
            /** <code>Coastal_Surveillance</code> (with ordinal 27) */
            COASTAL_SURVEILLANCE = 27,
            /** <code>Decoy_Mimic</code> (with ordinal 28) */
            DECOY_MIMIC = 28,
            /** <code>Data_Transmission</code> (with ordinal 29) */
            DATA_TRANSMISSION = 29,
            /** <code>Earth_Surveillance</code> (with ordinal 30) */
            EARTH_SURVEILLANCE = 30,
            /** <code>Gun_Lay_Beacon</code> (with ordinal 31) */
            GUN_LAY_BEACON = 31,
            /** <code>Ground_Mapping</code> (with ordinal 32) */
            GROUND_MAPPING = 32,
            /** <code>Harbor_Surveillance</code> (with ordinal 33) */
            HARBOR_SURVEILLANCE = 33,
            /** <code>IFF_IdentifyFriendOrFoe_</code> (with ordinal 34) */
            IFF_IDENTIFY_FRIEND_OR_FOE = 34,
            /** <code>ILS__Instrument_Landing_System_</code> (with ordinal 35) */
            ILS_INSTRUMENT_LANDING_SYSTEM = 35,
            /** <code>Ionospheric_Sound</code> (with ordinal 36) */
            IONOSPHERIC_SOUND = 36,
            /** <code>Interrogator</code> (with ordinal 37) */
            INTERROGATOR = 37,
            /** <code>Barrage_Jamming</code> (with ordinal 38) */
            BARRAGE_JAMMING = 38,
            /** <code>Click_Jamming</code> (with ordinal 39) */
            CLICK_JAMMING = 39,
            /** <code>DeceptiveJamming</code> (with ordinal 40) */
            DECEPTIVE_JAMMING = 40,
            /** <code>Frequency_Swept_Jamming</code> (with ordinal 41) */
            FREQUENCY_SWEPT_JAMMING = 41,
            /** <code>Jamming</code> (with ordinal 42) */
            JAMMING = 42,
            /** <code>NoiseJamming</code> (with ordinal 43) */
            NOISE_JAMMING = 43,
            /** <code>Pulsed_Jamming</code> (with ordinal 44) */
            PULSED_JAMMING = 44,
            /** <code>Repeater_Jamming</code> (with ordinal 45) */
            REPEATER_JAMMING = 45,
            /** <code>Spot_Noise_Jamming</code> (with ordinal 46) */
            SPOT_NOISE_JAMMING = 46,
            /** <code>Missile_Acquisition</code> (with ordinal 47) */
            MISSILE_ACQUISITION = 47,
            /** <code>Missile_Downlink</code> (with ordinal 48) */
            MISSILE_DOWNLINK = 48,
            /** <code>Meteorological</code> (with ordinal 49) */
            METEOROLOGICAL = 49,
            /** <code>Space</code> (with ordinal 50) */
            SPACE = 50,
            /** <code>Surface_Search</code> (with ordinal 51) */
            SURFACE_SEARCH = 51,
            /** <code>Shell_Tracking</code> (with ordinal 52) */
            SHELL_TRACKING = 52,
            /** <code>Television</code> (with ordinal 56) */
            TELEVISION = 56,
            /** <code>Unknown</code> (with ordinal 57) */
            UNKNOWN = 57,
            /** <code>Video_Remoting</code> (with ordinal 58) */
            VIDEO_REMOTING = 58,
            /** <code>Experimental_or_Training</code> (with ordinal 59) */
            EXPERIMENTAL_OR_TRAINING = 59,
            /** <code>Missile_Guidance</code> (with ordinal 60) */
            MISSILE_GUIDANCE = 60,
            /** <code>Missile_Homing</code> (with ordinal 61) */
            MISSILE_HOMING = 61,
            /** <code>Missile_Tracking</code> (with ordinal 62) */
            MISSILE_TRACKING = 62,
            /** <code>JammingNoise</code> (with ordinal 64) */
            JAMMING_NOISE = 64,
            /** <code>JammingDeception</code> (with ordinal 65) */
            JAMMING_DECEPTION = 65,
            /** <code>Decoy</code> (with ordinal 66) */
            DECOY = 66,
            /** <code>Navigation_Distance_Measuring_Equipment</code> (with ordinal 71) */
            NAVIGATION_DISTANCE_MEASURING_EQUIPMENT = 71,
            /** <code>Terrain_Following</code> (with ordinal 72) */
            TERRAIN_FOLLOWING = 72,
            /** <code>Weather_Avoidance</code> (with ordinal 73) */
            WEATHER_AVOIDANCE = 73,
            /** <code>Proximity_Fuse</code> (with ordinal 74) */
            PROXIMITY_FUSE = 74,
            /** <code>Instrumentation_deprecated_</code> (with ordinal 75) */
            INSTRUMENTATION_DEPRECATED = 75,
            /** <code>Radiosonde</code> (with ordinal 76) */
            RADIOSONDE = 76,
            /** <code>Sonobuoy</code> (with ordinal 77) */
            SONOBUOY = 77,
            /** <code>BathythermalSensor</code> (with ordinal 78) */
            BATHYTHERMAL_SENSOR = 78,
            /** <code>TowedCounterMeasure</code> (with ordinal 79) */
            TOWED_COUNTER_MEASURE = 79,
            /** <code>WeaponNonLethal</code> (with ordinal 96) */
            WEAPON_NON_LETHAL = 96,
            /** <code>WeaponLethal</code> (with ordinal 97) */
            WEAPON_LETHAL = 97
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to EmitterFunctionEnum: static_cast<DevStudio::EmitterFunctionEnum::EmitterFunctionEnum>(i)
        */
        LIBAPI bool isValid(const EmitterFunctionEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::EmitterFunctionEnum::EmitterFunctionEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::EmitterFunctionEnum::EmitterFunctionEnum const &);
}


#endif
