/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_FLARESTATERECSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_FLARESTATERECSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/EntityTypeStruct.h>
#include <DevStudio/datatypes/OctetArray2.h>
#include <vector>

namespace DevStudio {
   /**
   * Implementation of the <code>FlareStateRecStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Record specifying Flare state record</i>
   */
   class FlareStateRecStruct {

   public:
      /**
      * Description from the FOM: <i>Time since creation</i>.
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      unsigned int timeSinceCreation;
      /**
      * Description from the FOM: <i>Source</i>.
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      */
      EntityTypeStruct source;
      /**
      * Description from the FOM: <i>Number intensity</i>.
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^31, 2^31-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      int numberIntensity;
      /**
      * Description from the FOM: <i>Number of sources</i>.
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^31, 2^31-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      int numberOfSources;
      /**
      * Description from the FOM: <i>Geometry index</i>.
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      unsigned short geometryIndex;
      /**
      * Description from the FOM: <i>Padding field</i>.
      * <br>Description of the data type from the FOM: <i>Generic array of two Octet elements.</i>
      */
      std::vector</* 2 */ char > padding;

      LIBAPI FlareStateRecStruct()
         :
         timeSinceCreation(0),
         source(EntityTypeStruct()),
         numberIntensity(0),
         numberOfSources(0),
         geometryIndex(0),
         padding(0)
      {}

      /**
      * Constructor for FlareStateRecStruct
      *
      * @param timeSinceCreation_ value to set as timeSinceCreation.
      * <br>Description from the FOM: <i>Time since creation</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param source_ value to set as source.
      * <br>Description from the FOM: <i>Source</i>
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      * @param numberIntensity_ value to set as numberIntensity.
      * <br>Description from the FOM: <i>Number intensity</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^31, 2^31-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param numberOfSources_ value to set as numberOfSources.
      * <br>Description from the FOM: <i>Number of sources</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^31, 2^31-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param geometryIndex_ value to set as geometryIndex.
      * <br>Description from the FOM: <i>Geometry index</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param padding_ value to set as padding.
      * <br>Description from the FOM: <i>Padding field</i>
      * <br>Description of the data type from the FOM: <i>Generic array of two Octet elements.</i>
      */
      LIBAPI FlareStateRecStruct(
         unsigned int timeSinceCreation_,
         EntityTypeStruct source_,
         int numberIntensity_,
         int numberOfSources_,
         unsigned short geometryIndex_,
         std::vector</* 2 */ char > padding_
         )
         :
         timeSinceCreation(timeSinceCreation_),
         source(source_),
         numberIntensity(numberIntensity_),
         numberOfSources(numberOfSources_),
         geometryIndex(geometryIndex_),
         padding(padding_)
      {}



      /**
      * Function to get timeSinceCreation.
      * <br>Description from the FOM: <i>Time since creation</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return timeSinceCreation
      */
      LIBAPI unsigned int & getTimeSinceCreation() {
         return timeSinceCreation;
      }

      /**
      * Function to get source.
      * <br>Description from the FOM: <i>Source</i>
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      *
      * @return source
      */
      LIBAPI DevStudio::EntityTypeStruct & getSource() {
         return source;
      }

      /**
      * Function to get numberIntensity.
      * <br>Description from the FOM: <i>Number intensity</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^31, 2^31-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return numberIntensity
      */
      LIBAPI int & getNumberIntensity() {
         return numberIntensity;
      }

      /**
      * Function to get numberOfSources.
      * <br>Description from the FOM: <i>Number of sources</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^31, 2^31-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return numberOfSources
      */
      LIBAPI int & getNumberOfSources() {
         return numberOfSources;
      }

      /**
      * Function to get geometryIndex.
      * <br>Description from the FOM: <i>Geometry index</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return geometryIndex
      */
      LIBAPI unsigned short & getGeometryIndex() {
         return geometryIndex;
      }

      /**
      * Function to get padding.
      * <br>Description from the FOM: <i>Padding field</i>
      * <br>Description of the data type from the FOM: <i>Generic array of two Octet elements.</i>
      *
      * @return padding
      */
      LIBAPI std::vector</* 2 */ char > & getPadding() {
         return padding;
      }

   };


   LIBAPI bool operator ==(const DevStudio::FlareStateRecStruct& l, const DevStudio::FlareStateRecStruct& r);
   LIBAPI bool operator !=(const DevStudio::FlareStateRecStruct& l, const DevStudio::FlareStateRecStruct& r);
   LIBAPI bool operator <(const DevStudio::FlareStateRecStruct& l, const DevStudio::FlareStateRecStruct& r);
   LIBAPI bool operator >(const DevStudio::FlareStateRecStruct& l, const DevStudio::FlareStateRecStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::FlareStateRecStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::FlareStateRecStruct const &);
}
#endif
