/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_ENVIRONMENTMODELTYPEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_ENVIRONMENTMODELTYPEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace EnvironmentModelTypeEnum {
        /**
        * Implementation of the <code>EnvironmentModelTypeEnum8</code> data type from the FOM.
        * <br>Description from the FOM: <i>Environment process model type</i>
        */
        enum EnvironmentModelTypeEnum {
            /** <code>EnvironmentModelUnknown</code> (with ordinal 0) */
            ENVIRONMENT_MODEL_UNKNOWN = 0
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to EnvironmentModelTypeEnum: static_cast<DevStudio::EnvironmentModelTypeEnum::EnvironmentModelTypeEnum>(i)
        */
        LIBAPI bool isValid(const EnvironmentModelTypeEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::EnvironmentModelTypeEnum::EnvironmentModelTypeEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::EnvironmentModelTypeEnum::EnvironmentModelTypeEnum const &);
}


#endif
