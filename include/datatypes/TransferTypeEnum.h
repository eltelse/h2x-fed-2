/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_TRANSFERTYPEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_TRANSFERTYPEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace TransferTypeEnum {
        /**
        * Implementation of the <code>TransferTypeEnum8</code> data type from the FOM.
        * <br>Description from the FOM: <i>Transfer type</i>
        */
        enum TransferTypeEnum {
            /** <code>Other</code> (with ordinal 0) */
            OTHER = 0,
            /** <code>EntityPush</code> (with ordinal 1) */
            ENTITY_PUSH = 1,
            /** <code>EntityPull</code> (with ordinal 2) */
            ENTITY_PULL = 2,
            /** <code>EntitySwap</code> (with ordinal 3) */
            ENTITY_SWAP = 3,
            /** <code>EnvironmentalProcessPush</code> (with ordinal 4) */
            ENVIRONMENTAL_PROCESS_PUSH = 4,
            /** <code>EnvironmentalProcessPull</code> (with ordinal 5) */
            ENVIRONMENTAL_PROCESS_PULL = 5,
            /** <code>NotUsed</code> (with ordinal 6) */
            NOT_USED = 6,
            /** <code>CancelTransfer</code> (with ordinal 7) */
            CANCEL_TRANSFER = 7,
            /** <code>ManualPullTransfer-Entity</code> (with ordinal 8) */
            MANUAL_PULL_TRANSFER_ENTITY = 8,
            /** <code>ManualPullTransfer-EnvironmentalProcess</code> (with ordinal 9) */
            MANUAL_PULL_TRANSFER_ENVIRONMENTAL_PROCESS = 9,
            /** <code>RemoveEntity</code> (with ordinal 10) */
            REMOVE_ENTITY = 10
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to TransferTypeEnum: static_cast<DevStudio::TransferTypeEnum::TransferTypeEnum>(i)
        */
        LIBAPI bool isValid(const TransferTypeEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::TransferTypeEnum::TransferTypeEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::TransferTypeEnum::TransferTypeEnum const &);
}


#endif
