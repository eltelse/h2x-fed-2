/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_COLLISIONTYPEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_COLLISIONTYPEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace CollisionTypeEnum {
        /**
        * Implementation of the <code>CollisionTypeEnum8</code> data type from the FOM.
        * <br>Description from the FOM: <i>Collision type</i>
        */
        enum CollisionTypeEnum {
            /** <code>Inelastic</code> (with ordinal 0) */
            INELASTIC = 0,
            /** <code>Elastic</code> (with ordinal 1) */
            ELASTIC = 1
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to CollisionTypeEnum: static_cast<DevStudio::CollisionTypeEnum::CollisionTypeEnum>(i)
        */
        LIBAPI bool isValid(const CollisionTypeEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::CollisionTypeEnum::CollisionTypeEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::CollisionTypeEnum::CollisionTypeEnum const &);
}


#endif
