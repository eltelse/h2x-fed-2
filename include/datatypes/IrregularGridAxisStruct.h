/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_IRREGULARGRIDAXISSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_IRREGULARGRIDAXISSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/OctetPadding64Array.h>
#include <DevStudio/datatypes/UnsignedInteger16Array1Plus.h>
#include <vector>

namespace DevStudio {
   /**
   * Implementation of the <code>IrregularGridAxisStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Record specifying irregular (variable spacing) axis data</i>
   */
   class IrregularGridAxisStruct {

   public:
      /**
      * Description from the FOM: <i>Specifies the value that linearly scales the coordinates of the grid locations for the Xi axis</i>.
      * <br>Description of the data type from the FOM: <i>Double-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      */
      double coordinateScale;
      /**
      * Description from the FOM: <i>Specifies the constant offset value that shall be applied to the grid locations for the Xi axis</i>.
      * <br>Description of the data type from the FOM: <i>Double-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      */
      double coordinateOffset;
      /**
      * Description from the FOM: <i>Specifies the coordinate values for the Ni grid locations along the irregular (variable spacing) Xi axis</i>.
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of UnsignedInteger16 elements, containing at least one element.</i>
      */
      std::vector</* not empty */ unsigned short > numberOfGridLocationsAGridLocations;
      /**
      * Description from the FOM: <i>Brings the record length to a 64-bit boundary</i>.
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of meaningless Octet elements, to align the subsequent data structure to the next 64 bit octet boundary value (OBV). The array is encoded without array length, containing zero to seven elements.</i>
      */
      std::vector<char > paddingTo64;

      LIBAPI IrregularGridAxisStruct()
         :
         coordinateScale(0),
         coordinateOffset(0),
         numberOfGridLocationsAGridLocations(0),
         paddingTo64(0)
      {}

      /**
      * Constructor for IrregularGridAxisStruct
      *
      * @param coordinateScale_ value to set as coordinateScale.
      * <br>Description from the FOM: <i>Specifies the value that linearly scales the coordinates of the grid locations for the Xi axis</i>
      * <br>Description of the data type from the FOM: <i>Double-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      * @param coordinateOffset_ value to set as coordinateOffset.
      * <br>Description from the FOM: <i>Specifies the constant offset value that shall be applied to the grid locations for the Xi axis</i>
      * <br>Description of the data type from the FOM: <i>Double-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      * @param numberOfGridLocationsAGridLocations_ value to set as numberOfGridLocationsAGridLocations.
      * <br>Description from the FOM: <i>Specifies the coordinate values for the Ni grid locations along the irregular (variable spacing) Xi axis</i>
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of UnsignedInteger16 elements, containing at least one element.</i>
      * @param paddingTo64_ value to set as paddingTo64.
      * <br>Description from the FOM: <i>Brings the record length to a 64-bit boundary</i>
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of meaningless Octet elements, to align the subsequent data structure to the next 64 bit octet boundary value (OBV). The array is encoded without array length, containing zero to seven elements.</i>
      */
      LIBAPI IrregularGridAxisStruct(
         double coordinateScale_,
         double coordinateOffset_,
         std::vector</* not empty */ unsigned short > numberOfGridLocationsAGridLocations_,
         std::vector<char > paddingTo64_
         )
         :
         coordinateScale(coordinateScale_),
         coordinateOffset(coordinateOffset_),
         numberOfGridLocationsAGridLocations(numberOfGridLocationsAGridLocations_),
         paddingTo64(paddingTo64_)
      {}



      /**
      * Function to get coordinateScale.
      * <br>Description from the FOM: <i>Specifies the value that linearly scales the coordinates of the grid locations for the Xi axis</i>
      * <br>Description of the data type from the FOM: <i>Double-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      *
      * @return coordinateScale
      */
      LIBAPI double & getCoordinateScale() {
         return coordinateScale;
      }

      /**
      * Function to get coordinateOffset.
      * <br>Description from the FOM: <i>Specifies the constant offset value that shall be applied to the grid locations for the Xi axis</i>
      * <br>Description of the data type from the FOM: <i>Double-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      *
      * @return coordinateOffset
      */
      LIBAPI double & getCoordinateOffset() {
         return coordinateOffset;
      }

      /**
      * Function to get numberOfGridLocationsAGridLocations.
      * <br>Description from the FOM: <i>Specifies the coordinate values for the Ni grid locations along the irregular (variable spacing) Xi axis</i>
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of UnsignedInteger16 elements, containing at least one element.</i>
      *
      * @return numberOfGridLocationsAGridLocations
      */
      LIBAPI std::vector</* not empty */ unsigned short > & getNumberOfGridLocationsAGridLocations() {
         return numberOfGridLocationsAGridLocations;
      }

      /**
      * Function to get paddingTo64.
      * <br>Description from the FOM: <i>Brings the record length to a 64-bit boundary</i>
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of meaningless Octet elements, to align the subsequent data structure to the next 64 bit octet boundary value (OBV). The array is encoded without array length, containing zero to seven elements.</i>
      *
      * @return paddingTo64
      */
      LIBAPI std::vector<char > & getPaddingTo64() {
         return paddingTo64;
      }

   };


   LIBAPI bool operator ==(const DevStudio::IrregularGridAxisStruct& l, const DevStudio::IrregularGridAxisStruct& r);
   LIBAPI bool operator !=(const DevStudio::IrregularGridAxisStruct& l, const DevStudio::IrregularGridAxisStruct& r);
   LIBAPI bool operator <(const DevStudio::IrregularGridAxisStruct& l, const DevStudio::IrregularGridAxisStruct& r);
   LIBAPI bool operator >(const DevStudio::IrregularGridAxisStruct& l, const DevStudio::IrregularGridAxisStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::IrregularGridAxisStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::IrregularGridAxisStruct const &);
}
#endif
