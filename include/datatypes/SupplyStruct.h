/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_SUPPLYSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_SUPPLYSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/EntityTypeStruct.h>

namespace DevStudio {
   /**
   * Implementation of the <code>SupplyStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Represents a single supply type and the quantity being offered or requested.</i>
   */
   class SupplyStruct {

   public:
      /**
      * Description from the FOM: <i>The type of supply being offered or requested.</i>.
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      */
      EntityTypeStruct supplyType;
      /**
      * Description from the FOM: <i>The number of units of the supply type. The unit measure depends on the supply type and shall use the SI units of measurement used for such supplies.</i>.
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      */
      float quantity;

      LIBAPI SupplyStruct()
         :
         supplyType(EntityTypeStruct()),
         quantity(0)
      {}

      /**
      * Constructor for SupplyStruct
      *
      * @param supplyType_ value to set as supplyType.
      * <br>Description from the FOM: <i>The type of supply being offered or requested.</i>
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      * @param quantity_ value to set as quantity.
      * <br>Description from the FOM: <i>The number of units of the supply type. The unit measure depends on the supply type and shall use the SI units of measurement used for such supplies.</i>
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      */
      LIBAPI SupplyStruct(
         EntityTypeStruct supplyType_,
         float quantity_
         )
         :
         supplyType(supplyType_),
         quantity(quantity_)
      {}



      /**
      * Function to get supplyType.
      * <br>Description from the FOM: <i>The type of supply being offered or requested.</i>
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      *
      * @return supplyType
      */
      LIBAPI DevStudio::EntityTypeStruct & getSupplyType() {
         return supplyType;
      }

      /**
      * Function to get quantity.
      * <br>Description from the FOM: <i>The number of units of the supply type. The unit measure depends on the supply type and shall use the SI units of measurement used for such supplies.</i>
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      *
      * @return quantity
      */
      LIBAPI float & getQuantity() {
         return quantity;
      }

   };


   LIBAPI bool operator ==(const DevStudio::SupplyStruct& l, const DevStudio::SupplyStruct& r);
   LIBAPI bool operator !=(const DevStudio::SupplyStruct& l, const DevStudio::SupplyStruct& r);
   LIBAPI bool operator <(const DevStudio::SupplyStruct& l, const DevStudio::SupplyStruct& r);
   LIBAPI bool operator >(const DevStudio::SupplyStruct& l, const DevStudio::SupplyStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::SupplyStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::SupplyStruct const &);
}
#endif
