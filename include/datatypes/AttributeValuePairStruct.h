/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_ATTRIBUTEVALUEPAIRSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_ATTRIBUTEVALUEPAIRSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/OctetArray.h>
#include <DevStudio/datatypes/OctetPadding32Array.h>
#include <vector>

namespace DevStudio {
   /**
   * Implementation of the <code>AttributeValuePairStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Pair of an AttributeHandle identifying an attribute and data for that attribute.</i>
   */
   class AttributeValuePairStruct {

   public:
      /**
      * Description from the FOM: <i>AttributeHandle identifying attribute.</i>.
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      unsigned int attributeHandle;
      /**
      * Description from the FOM: <i>Value for the specified attribute.</i>.
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of Octet elements, may also contain no elements.</i>
      */
      std::vector<char > numberOfBytesAValue;
      /**
      * Description from the FOM: <i>Brings the record length to a 32-bit boundary</i>.
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of meaningless Octet elements, to align the subsequent data structure to the next 32 bit octet boundary value (OBV). The array is encoded without array length, containing zero to three elements.</i>
      */
      std::vector<char > paddingTo32;

      LIBAPI AttributeValuePairStruct()
         :
         attributeHandle(0),
         numberOfBytesAValue(0),
         paddingTo32(0)
      {}

      /**
      * Constructor for AttributeValuePairStruct
      *
      * @param attributeHandle_ value to set as attributeHandle.
      * <br>Description from the FOM: <i>AttributeHandle identifying attribute.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param numberOfBytesAValue_ value to set as numberOfBytesAValue.
      * <br>Description from the FOM: <i>Value for the specified attribute.</i>
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of Octet elements, may also contain no elements.</i>
      * @param paddingTo32_ value to set as paddingTo32.
      * <br>Description from the FOM: <i>Brings the record length to a 32-bit boundary</i>
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of meaningless Octet elements, to align the subsequent data structure to the next 32 bit octet boundary value (OBV). The array is encoded without array length, containing zero to three elements.</i>
      */
      LIBAPI AttributeValuePairStruct(
         unsigned int attributeHandle_,
         std::vector<char > numberOfBytesAValue_,
         std::vector<char > paddingTo32_
         )
         :
         attributeHandle(attributeHandle_),
         numberOfBytesAValue(numberOfBytesAValue_),
         paddingTo32(paddingTo32_)
      {}



      /**
      * Function to get attributeHandle.
      * <br>Description from the FOM: <i>AttributeHandle identifying attribute.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return attributeHandle
      */
      LIBAPI unsigned int & getAttributeHandle() {
         return attributeHandle;
      }

      /**
      * Function to get numberOfBytesAValue.
      * <br>Description from the FOM: <i>Value for the specified attribute.</i>
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of Octet elements, may also contain no elements.</i>
      *
      * @return numberOfBytesAValue
      */
      LIBAPI std::vector<char > & getNumberOfBytesAValue() {
         return numberOfBytesAValue;
      }

      /**
      * Function to get paddingTo32.
      * <br>Description from the FOM: <i>Brings the record length to a 32-bit boundary</i>
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of meaningless Octet elements, to align the subsequent data structure to the next 32 bit octet boundary value (OBV). The array is encoded without array length, containing zero to three elements.</i>
      *
      * @return paddingTo32
      */
      LIBAPI std::vector<char > & getPaddingTo32() {
         return paddingTo32;
      }

   };


   LIBAPI bool operator ==(const DevStudio::AttributeValuePairStruct& l, const DevStudio::AttributeValuePairStruct& r);
   LIBAPI bool operator !=(const DevStudio::AttributeValuePairStruct& l, const DevStudio::AttributeValuePairStruct& r);
   LIBAPI bool operator <(const DevStudio::AttributeValuePairStruct& l, const DevStudio::AttributeValuePairStruct& r);
   LIBAPI bool operator >(const DevStudio::AttributeValuePairStruct& l, const DevStudio::AttributeValuePairStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::AttributeValuePairStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::AttributeValuePairStruct const &);
}
#endif
