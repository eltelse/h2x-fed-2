/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_OPACITYCODEENUM_H
#define DEVELOPER_STUDIO_DATATYPES_OPACITYCODEENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace OpacityCodeEnum {
        /**
        * Implementation of the <code>OpacityCodeEnum32</code> data type from the FOM.
        * <br>Description from the FOM: <i>Density of environmentals</i>
        */
        enum OpacityCodeEnum {
            /** <code>Clear</code> (with ordinal 0) */
            CLEAR = 0,
            /** <code>Hazy</code> (with ordinal 1) */
            HAZY = 1,
            /** <code>Dense</code> (with ordinal 2) */
            DENSE = 2,
            /** <code>VeryDense</code> (with ordinal 3) */
            VERY_DENSE = 3,
            /** <code>Opaque</code> (with ordinal 4) */
            OPAQUE = 4
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to OpacityCodeEnum: static_cast<DevStudio::OpacityCodeEnum::OpacityCodeEnum>(i)
        */
        LIBAPI bool isValid(const OpacityCodeEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::OpacityCodeEnum::OpacityCodeEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::OpacityCodeEnum::OpacityCodeEnum const &);
}


#endif
