/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_ACTIVITYSTATUSENUM_H
#define DEVELOPER_STUDIO_DATATYPES_ACTIVITYSTATUSENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace ActivityStatusEnum {
        /**
        * Implementation of the <code>ActivityStatusEnum32</code> data type from the FOM.
        * <br>Description from the FOM: <i>-NULL-</i>
        */
        enum ActivityStatusEnum {
            /** <code>UNDEFINED</code> (with ordinal 0) */
            UNDEFINED = 0,
            /** <code>SHORT_RANGE_FIRE</code> (with ordinal 1024) */
            SHORT_RANGE_FIRE = 1024,
            /** <code>MEDIUM_RANGE_FIRE</code> (with ordinal 2048) */
            MEDIUM_RANGE_FIRE = 2048,
            /** <code>LONG_RANGE_FIRE</code> (with ordinal 4096) */
            LONG_RANGE_FIRE = 4096
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to ActivityStatusEnum: static_cast<DevStudio::ActivityStatusEnum::ActivityStatusEnum>(i)
        */
        LIBAPI bool isValid(const ActivityStatusEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::ActivityStatusEnum::ActivityStatusEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::ActivityStatusEnum::ActivityStatusEnum const &);
}


#endif
