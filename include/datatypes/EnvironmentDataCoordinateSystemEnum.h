/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_ENVIRONMENTDATACOORDINATESYSTEMENUM_H
#define DEVELOPER_STUDIO_DATATYPES_ENVIRONMENTDATACOORDINATESYSTEMENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace EnvironmentDataCoordinateSystemEnum {
        /**
        * Implementation of the <code>EnvironmentDataCoordinateSystemEnum16</code> data type from the FOM.
        * <br>Description from the FOM: <i>Environment data coordinate system</i>
        */
        enum EnvironmentDataCoordinateSystemEnum {
            /** <code>RightHandedCartesian_localTopographicProjection_East_North_Up_</code> (with ordinal 0) */
            RIGHT_HANDED_CARTESIAN_LOCAL_TOPOGRAPHIC_PROJECTION_EAST_NORTH_UP = 0,
            /** <code>LeftHandedCartesian_localTopographicProjection_East_North_Down_</code> (with ordinal 1) */
            LEFT_HANDED_CARTESIAN_LOCAL_TOPOGRAPHIC_PROJECTION_EAST_NORTH_DOWN = 1,
            /** <code>Latitude_Longitude_Height</code> (with ordinal 2) */
            LATITUDE_LONGITUDE_HEIGHT = 2,
            /** <code>Latitude_Longitude_Depth</code> (with ordinal 3) */
            LATITUDE_LONGITUDE_DEPTH = 3
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to EnvironmentDataCoordinateSystemEnum: static_cast<DevStudio::EnvironmentDataCoordinateSystemEnum::EnvironmentDataCoordinateSystemEnum>(i)
        */
        LIBAPI bool isValid(const EnvironmentDataCoordinateSystemEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::EnvironmentDataCoordinateSystemEnum::EnvironmentDataCoordinateSystemEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::EnvironmentDataCoordinateSystemEnum::EnvironmentDataCoordinateSystemEnum const &);
}


#endif
