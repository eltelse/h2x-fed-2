/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_ARTICULATEDPARTSSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_ARTICULATEDPARTSSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <DevStudio/datatypes/ArticulatedPartsTypeEnum.h>
#include <DevStudio/datatypes/ArticulatedTypeMetricEnum.h>

namespace DevStudio {
   /**
   * Implementation of the <code>ArticulatedPartsStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Structure to represent the state of a movable part of an entity.</i>
   */
   class ArticulatedPartsStruct {

   public:
      /**
      * Description from the FOM: <i>The type class uniquely identifies a particular articulated part on a given entity type. Guidance for uniquely assigning type classes to an entity's articulated parts is given in section 4.8 of the enumeration document (SISO-REF-010).</i>.
      * <br>Description of the data type from the FOM: <i>Articulated part type class</i>
      */
      ArticulatedPartsTypeEnum::ArticulatedPartsTypeEnum class_;
      /**
      * Description from the FOM: <i>The type metric uniquely identifies the transformation to be applied to the articulated part.</i>.
      * <br>Description of the data type from the FOM: <i>Articulated part type metric</i>
      */
      ArticulatedTypeMetricEnum::ArticulatedTypeMetricEnum typeMetric;
      /**
      * Description from the FOM: <i>Value of the transformation to be applied to the articulated part.</i>.
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      */
      float value;

      LIBAPI ArticulatedPartsStruct()
         :
         class_(ArticulatedPartsTypeEnum::ArticulatedPartsTypeEnum()),
         typeMetric(ArticulatedTypeMetricEnum::ArticulatedTypeMetricEnum()),
         value(0)
      {}

      /**
      * Constructor for ArticulatedPartsStruct
      *
      * @param class__ value to set as class_.
      * <br>Description from the FOM: <i>The type class uniquely identifies a particular articulated part on a given entity type. Guidance for uniquely assigning type classes to an entity's articulated parts is given in section 4.8 of the enumeration document (SISO-REF-010).</i>
      * <br>Description of the data type from the FOM: <i>Articulated part type class</i>
      * @param typeMetric_ value to set as typeMetric.
      * <br>Description from the FOM: <i>The type metric uniquely identifies the transformation to be applied to the articulated part.</i>
      * <br>Description of the data type from the FOM: <i>Articulated part type metric</i>
      * @param value_ value to set as value.
      * <br>Description from the FOM: <i>Value of the transformation to be applied to the articulated part.</i>
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      */
      LIBAPI ArticulatedPartsStruct(
         ArticulatedPartsTypeEnum::ArticulatedPartsTypeEnum class__,
         ArticulatedTypeMetricEnum::ArticulatedTypeMetricEnum typeMetric_,
         float value_
         )
         :
         class_(class__),
         typeMetric(typeMetric_),
         value(value_)
      {}



      /**
      * Function to get class_.
      * <br>Description from the FOM: <i>The type class uniquely identifies a particular articulated part on a given entity type. Guidance for uniquely assigning type classes to an entity's articulated parts is given in section 4.8 of the enumeration document (SISO-REF-010).</i>
      * <br>Description of the data type from the FOM: <i>Articulated part type class</i>
      *
      * @return class_
      */
      LIBAPI DevStudio::ArticulatedPartsTypeEnum::ArticulatedPartsTypeEnum & getClass_() {
         return class_;
      }

      /**
      * Function to get typeMetric.
      * <br>Description from the FOM: <i>The type metric uniquely identifies the transformation to be applied to the articulated part.</i>
      * <br>Description of the data type from the FOM: <i>Articulated part type metric</i>
      *
      * @return typeMetric
      */
      LIBAPI DevStudio::ArticulatedTypeMetricEnum::ArticulatedTypeMetricEnum & getTypeMetric() {
         return typeMetric;
      }

      /**
      * Function to get value.
      * <br>Description from the FOM: <i>Value of the transformation to be applied to the articulated part.</i>
      * <br>Description of the data type from the FOM: <i>Single-precision floating point number. [unit: NA, resolution: NA, accuracy: NA]</i>
      *
      * @return value
      */
      LIBAPI float & getValue() {
         return value;
      }

   };


   LIBAPI bool operator ==(const DevStudio::ArticulatedPartsStruct& l, const DevStudio::ArticulatedPartsStruct& r);
   LIBAPI bool operator !=(const DevStudio::ArticulatedPartsStruct& l, const DevStudio::ArticulatedPartsStruct& r);
   LIBAPI bool operator <(const DevStudio::ArticulatedPartsStruct& l, const DevStudio::ArticulatedPartsStruct& r);
   LIBAPI bool operator >(const DevStudio::ArticulatedPartsStruct& l, const DevStudio::ArticulatedPartsStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::ArticulatedPartsStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::ArticulatedPartsStruct const &);
}
#endif
