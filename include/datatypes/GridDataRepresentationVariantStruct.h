/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_GRIDDATAREPRESENTATIONVARIANTSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_GRIDDATAREPRESENTATIONVARIANTSTRUCT_H

#include <DevStudio/datatypes/EnvironmentDataRepresentationEnum.h>
#include <DevStudio/datatypes/GridValueType0Struct.h>
#include <DevStudio/datatypes/GridValueType1Struct.h>
#include <DevStudio/datatypes/GridValueType2Struct.h>

#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
   /**
   * Implementation of the <code>GridDataRepresentationVariantStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Record specifying either type 0 or type 1 or type 2 data representation</i>
   */
   class GridDataRepresentationVariantStruct {

   public:
      LIBAPI GridDataRepresentationVariantStruct()
         : 
         _discriminant(DevStudio::EnvironmentDataRepresentationEnum::EnvironmentDataRepresentationEnum())
      {}

      /** 
      * Create a new alternative GridDataRepresentationVariantStruct, with <code>EnvironmentDataType0</code> as discriminant.
      *
      * @param type0 value of the Type0 field
      *
      * @return a new GridDataRepresentationVariantStruct
      */
      LIBAPI static GridDataRepresentationVariantStruct createType0(DevStudio::GridValueType0Struct type0);

      /** 
      * Create a new alternative GridDataRepresentationVariantStruct, with <code>EnvironmentDataType1</code> as discriminant.
      *
      * @param type1 value of the Type1 field
      *
      * @return a new GridDataRepresentationVariantStruct
      */
      LIBAPI static GridDataRepresentationVariantStruct createType1(DevStudio::GridValueType1Struct type1);

      /** 
      * Create a new alternative GridDataRepresentationVariantStruct, with <code>EnvironmentDataType2</code> as discriminant.
      *
      * @param type2 value of the Type2 field
      *
      * @return a new GridDataRepresentationVariantStruct
      */
      LIBAPI static GridDataRepresentationVariantStruct createType2(DevStudio::GridValueType2Struct type2);



      /**
      * Function to get discriminant
      *
      * @return disciminant
      */
      LIBAPI DevStudio::EnvironmentDataRepresentationEnum::EnvironmentDataRepresentationEnum getDiscriminant() const {
          return _discriminant;
      }

      /**
      * Function to get Type0.
      * Note that this field is only valid of the discriminant is <code>EnvironmentDataType0</code>.
      * <br>Description from the FOM: <i>Specifies type 0 data representation alternative</i>
      * <br>Description of the data type from the FOM: <i>Record specifying type 0 data representation</i>
      *
      * @return Type0 value
      */
      LIBAPI DevStudio::GridValueType0StructPtr getType0() const {
         return _type0;
      }

      /**
      * Function to set Type0.
      * Note that this will set the discriminant to <code>EnvironmentDataType0</code>.
      * <br>Description from the FOM: <i>Specifies type 0 data representation alternative</i>
      * <br>Description of the data type from the FOM: <i>Record specifying type 0 data representation</i>
      *
      * @param type0 value used to create a GridValueType0StructPtr
      */
      LIBAPI void setType0(const DevStudio::GridValueType0Struct type0) {
         _type0 = DevStudio::GridValueType0StructPtr( new DevStudio::GridValueType0Struct (type0));
         _discriminant = EnvironmentDataRepresentationEnum::ENVIRONMENT_DATA_TYPE0;
      }

      /**
      * Function to get Type1.
      * Note that this field is only valid of the discriminant is <code>EnvironmentDataType1</code>.
      * <br>Description from the FOM: <i>Specifies type 1 data representation alternative</i>
      * <br>Description of the data type from the FOM: <i>Record specifying type 1 data representation</i>
      *
      * @return Type1 value
      */
      LIBAPI DevStudio::GridValueType1StructPtr getType1() const {
         return _type1;
      }

      /**
      * Function to set Type1.
      * Note that this will set the discriminant to <code>EnvironmentDataType1</code>.
      * <br>Description from the FOM: <i>Specifies type 1 data representation alternative</i>
      * <br>Description of the data type from the FOM: <i>Record specifying type 1 data representation</i>
      *
      * @param type1 value used to create a GridValueType1StructPtr
      */
      LIBAPI void setType1(const DevStudio::GridValueType1Struct type1) {
         _type1 = DevStudio::GridValueType1StructPtr( new DevStudio::GridValueType1Struct (type1));
         _discriminant = EnvironmentDataRepresentationEnum::ENVIRONMENT_DATA_TYPE1;
      }

      /**
      * Function to get Type2.
      * Note that this field is only valid of the discriminant is <code>EnvironmentDataType2</code>.
      * <br>Description from the FOM: <i>Specifies type 2 data representation alternative</i>
      * <br>Description of the data type from the FOM: <i>Record specifying type 2 data representation</i>
      *
      * @return Type2 value
      */
      LIBAPI DevStudio::GridValueType2StructPtr getType2() const {
         return _type2;
      }

      /**
      * Function to set Type2.
      * Note that this will set the discriminant to <code>EnvironmentDataType2</code>.
      * <br>Description from the FOM: <i>Specifies type 2 data representation alternative</i>
      * <br>Description of the data type from the FOM: <i>Record specifying type 2 data representation</i>
      *
      * @param type2 value used to create a GridValueType2StructPtr
      */
      LIBAPI void setType2(const DevStudio::GridValueType2Struct type2) {
         _type2 = DevStudio::GridValueType2StructPtr( new DevStudio::GridValueType2Struct (type2));
         _discriminant = EnvironmentDataRepresentationEnum::ENVIRONMENT_DATA_TYPE2;
      }

      /** Description from the FOM: <i>Specifies type 0 data representation alternative</i> */
      DevStudio::GridValueType0StructPtr _type0;
      /** Description from the FOM: <i>Specifies type 1 data representation alternative</i> */
      DevStudio::GridValueType1StructPtr _type1;
      /** Description from the FOM: <i>Specifies type 2 data representation alternative</i> */
      DevStudio::GridValueType2StructPtr _type2;

   private:
      DevStudio::EnvironmentDataRepresentationEnum::EnvironmentDataRepresentationEnum _discriminant;

      GridDataRepresentationVariantStruct(
         DevStudio::GridValueType0StructPtr type0,
         DevStudio::GridValueType1StructPtr type1,
         DevStudio::GridValueType2StructPtr type2,
         DevStudio::EnvironmentDataRepresentationEnum::EnvironmentDataRepresentationEnum discriminant
      );
   };

   LIBAPI bool operator ==(const DevStudio::GridDataRepresentationVariantStruct& l, const DevStudio::GridDataRepresentationVariantStruct& r);
   LIBAPI bool operator !=(const DevStudio::GridDataRepresentationVariantStruct& l, const DevStudio::GridDataRepresentationVariantStruct& r);
   LIBAPI bool operator <(const DevStudio::GridDataRepresentationVariantStruct& l, const DevStudio::GridDataRepresentationVariantStruct& r);
   LIBAPI bool operator >(const DevStudio::GridDataRepresentationVariantStruct& l, const DevStudio::GridDataRepresentationVariantStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::GridDataRepresentationVariantStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::GridDataRepresentationVariantStruct const &);
}

#endif
