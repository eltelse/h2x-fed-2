/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_EVENTIDENTIFIERSTRUCT_H
#define DEVELOPER_STUDIO_DATATYPES_EVENTIDENTIFIERSTRUCT_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

#include <string>

namespace DevStudio {
   /**
   * Implementation of the <code>EventIdentifierStruct</code> data type from the FOM.
   * <br>Description from the FOM: <i>Identification of an event. Based on the Event Identifier record as specified in IEEE 1278.1-1995 section 5.2.18.</i>
   */
   class EventIdentifierStruct {

   public:
      /**
      * Description from the FOM: <i>The event number. Uniquely assigned by the simulation application (federate) that initiates the sequence of events. It shall be set to one for each exercise and incremented by one for each event. In the case where all possible values are exhausted, the numbers may be reused beginning again at one.</i>.
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      */
      unsigned short eventCount;
      /**
      * Description from the FOM: <i>Identification of the object issuing the event.</i>.
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      */
      std::string issuingObjectIdentifier;

      LIBAPI EventIdentifierStruct()
         :
         eventCount(0),
         issuingObjectIdentifier("")
      {}

      /**
      * Constructor for EventIdentifierStruct
      *
      * @param eventCount_ value to set as eventCount.
      * <br>Description from the FOM: <i>The event number. Uniquely assigned by the simulation application (federate) that initiates the sequence of events. It shall be set to one for each exercise and incremented by one for each event. In the case where all possible values are exhausted, the numbers may be reused beginning again at one.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      * @param issuingObjectIdentifier_ value to set as issuingObjectIdentifier.
      * <br>Description from the FOM: <i>Identification of the object issuing the event.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      */
      LIBAPI EventIdentifierStruct(
         unsigned short eventCount_,
         std::string issuingObjectIdentifier_
         )
         :
         eventCount(eventCount_),
         issuingObjectIdentifier(issuingObjectIdentifier_)
      {}



      /**
      * Function to get eventCount.
      * <br>Description from the FOM: <i>The event number. Uniquely assigned by the simulation application (federate) that initiates the sequence of events. It shall be set to one for each exercise and incremented by one for each event. In the case where all possible values are exhausted, the numbers may be reused beginning again at one.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return eventCount
      */
      LIBAPI unsigned short & getEventCount() {
         return eventCount;
      }

      /**
      * Function to get issuingObjectIdentifier.
      * <br>Description from the FOM: <i>Identification of the object issuing the event.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @return issuingObjectIdentifier
      */
      LIBAPI std::string & getIssuingObjectIdentifier() {
         return issuingObjectIdentifier;
      }

   };


   LIBAPI bool operator ==(const DevStudio::EventIdentifierStruct& l, const DevStudio::EventIdentifierStruct& r);
   LIBAPI bool operator !=(const DevStudio::EventIdentifierStruct& l, const DevStudio::EventIdentifierStruct& r);
   LIBAPI bool operator <(const DevStudio::EventIdentifierStruct& l, const DevStudio::EventIdentifierStruct& r);
   LIBAPI bool operator >(const DevStudio::EventIdentifierStruct& l, const DevStudio::EventIdentifierStruct& r);

   LIBAPI std::wostream & operator << (std::wostream &, DevStudio::EventIdentifierStruct const &);
   LIBAPI std::ostream & operator << (std::ostream &, DevStudio::EventIdentifierStruct const &);
}
#endif
