/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_NOMENCLATUREENUM_H
#define DEVELOPER_STUDIO_DATATYPES_NOMENCLATUREENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace NomenclatureEnum {
        /**
        * Implementation of the <code>NomenclatureEnum16</code> data type from the FOM.
        * <br>Description from the FOM: <i>The nomenclature for a specific radio system.</i>
        */
        enum NomenclatureEnum {
            /** <code>Other</code> (with ordinal 0) */
            OTHER = 0,
            /** <code>AN_ARN-118</code> (with ordinal 1) */
            AN_ARN_118 = 1,
            /** <code>AN_ARN-139</code> (with ordinal 2) */
            AN_ARN_139 = 2,
            /** <code>GenericGroundFixedTransmitter</code> (with ordinal 3) */
            GENERIC_GROUND_FIXED_TRANSMITTER = 3,
            /** <code>GenericGroundMobileTransmitter</code> (with ordinal 4) */
            GENERIC_GROUND_MOBILE_TRANSMITTER = 4
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to NomenclatureEnum: static_cast<DevStudio::NomenclatureEnum::NomenclatureEnum>(i)
        */
        LIBAPI bool isValid(const NomenclatureEnum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::NomenclatureEnum::NomenclatureEnum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::NomenclatureEnum::NomenclatureEnum const &);
}


#endif
