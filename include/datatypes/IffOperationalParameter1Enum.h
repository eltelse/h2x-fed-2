/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_DATATYPES_IFFOPERATIONALPARAMETER1ENUM_H
#define DEVELOPER_STUDIO_DATATYPES_IFFOPERATIONALPARAMETER1ENUM_H

#include <iostream>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {
    namespace IffOperationalParameter1Enum {
        /**
        * Implementation of the <code>IffOperationalParameter1Enum8</code> data type from the FOM.
        * <br>Description from the FOM: <i>IFF operational parameter 1</i>
        */
        enum IffOperationalParameter1Enum {
            /** <code>Other</code> (with ordinal 0) */
            OTHER = 0
        };

        /**
        * Returns true if the enum is valid, i.e. if it is defined in the FOM.
        *
        * It is possible to explicitly convert an integer value to an enumeration. However, if the integer value
        * does not exist among the enumeration values the resulting enum value is undefined. This function will
        * only return true for defined values.
        *
        * To cast an integer to IffOperationalParameter1Enum: static_cast<DevStudio::IffOperationalParameter1Enum::IffOperationalParameter1Enum>(i)
        */
        LIBAPI bool isValid(const IffOperationalParameter1Enum& data);
    }

    LIBAPI std::wostream & operator << (std::wostream &, DevStudio::IffOperationalParameter1Enum::IffOperationalParameter1Enum const &);
    LIBAPI std::ostream & operator << (std::ostream &, DevStudio::IffOperationalParameter1Enum::IffOperationalParameter1Enum const &);
}


#endif
