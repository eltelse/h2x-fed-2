/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLALIFEFORMMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLALIFEFORMMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaLifeform.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaLifeform instances.
    */
    class HlaLifeformManagerListener {

    public:

        LIBAPI virtual ~HlaLifeformManagerListener() {}

        /**
        * This method is called when a new HlaLifeform instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param lifeform the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaLifeformDiscovered(HlaLifeformPtr lifeform, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaLifeform instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param lifeform the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaLifeformInitialized(HlaLifeformPtr lifeform, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaLifeformManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param lifeform the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaLifeformInInterest(HlaLifeformPtr lifeform, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaLifeformManagerListener instance goes out of interest.
        *
        * @param lifeform the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaLifeformOutOfInterest(HlaLifeformPtr lifeform, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaLifeform instance is deleted.
        *
        * @param lifeform the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaLifeformDeleted(HlaLifeformPtr lifeform, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaLifeformManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaLifeformManagerListener::Adapter : public HlaLifeformManagerListener {

    public:
        LIBAPI virtual void hlaLifeformDiscovered(HlaLifeformPtr lifeform, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaLifeformInitialized(HlaLifeformPtr lifeform, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaLifeformInInterest(HlaLifeformPtr lifeform, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaLifeformOutOfInterest(HlaLifeformPtr lifeform, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaLifeformDeleted(HlaLifeformPtr lifeform, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
