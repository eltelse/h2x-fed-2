/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLARADIOTRANSMITTERUPDATER_H
#define DEVELOPER_STUDIO_HLARADIOTRANSMITTERUPDATER_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <boost/noncopyable.hpp>

#include <DevStudio/datatypes/AntennaPatternVariantStruct.h>
#include <DevStudio/datatypes/AntennaPatternVariantStructLengthlessArray.h>
#include <DevStudio/datatypes/CryptographicModeEnum.h>
#include <DevStudio/datatypes/CryptographicSystemTypeEnum.h>
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/RFModulationSystemTypeEnum.h>
#include <DevStudio/datatypes/RFModulationTypeVariantStruct.h>
#include <DevStudio/datatypes/RadioInputSourceEnum.h>
#include <DevStudio/datatypes/RadioTypeStruct.h>
#include <DevStudio/datatypes/RelativePositionStruct.h>
#include <DevStudio/datatypes/SpreadSpectrumVariantStruct.h>
#include <DevStudio/datatypes/TransmitterOperationalStatusEnum.h>
#include <DevStudio/datatypes/WorldLocationStruct.h>
#include <string>
#include <vector>

#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaException.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaEmbeddedSystemUpdater.h>

namespace DevStudio {

    /**
    * Updater used to update attribute values.
    */
    class HlaRadioTransmitterUpdater : public HlaEmbeddedSystemUpdater {

    public:

    LIBAPI virtual ~HlaRadioTransmitterUpdater() {}

    /**
    * Set the antennaPatternData for this update.
    * <br>Description from the FOM: <i>The radiation pattern of the radio's antenna.</i>
    * <br>Description of the data type from the FOM: <i>Represents an antenna's radiation pattern, its orientation in space, and the polarization of the radiation.</i>
    *
    * @param antennaPatternData the new antennaPatternData
    */
    LIBAPI virtual void setAntennaPatternData(const std::vector<DevStudio::AntennaPatternVariantStruct >& antennaPatternData) = 0;

    /**
    * Set the cryptographicMode for this update.
    * <br>Description from the FOM: <i>The mode of the cryptographic system.</i>
    * <br>Description of the data type from the FOM: <i>Represents the encryption mode of a cryptographic system.</i>
    *
    * @param cryptographicMode the new cryptographicMode
    */
    LIBAPI virtual void setCryptographicMode(const DevStudio::CryptographicModeEnum::CryptographicModeEnum& cryptographicMode) = 0;

    /**
    * Set the cryptoSystem for this update.
    * <br>Description from the FOM: <i>The type of cryptographic equipment in use.</i>
    * <br>Description of the data type from the FOM: <i>Identifies the type of cryptographic equipment</i>
    *
    * @param cryptoSystem the new cryptoSystem
    */
    LIBAPI virtual void setCryptoSystem(const DevStudio::CryptographicSystemTypeEnum::CryptographicSystemTypeEnum& cryptoSystem) = 0;

    /**
    * Set the encryptionKeyIdentifier for this update.
    * <br>Description from the FOM: <i>The identification of the key used to encrypt the radio signals being transmitted. The transmitter and receiver should be considered to be using the same key if these numbers match.</i>
    * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
    *
    * @param encryptionKeyIdentifier the new encryptionKeyIdentifier
    */
    LIBAPI virtual void setEncryptionKeyIdentifier(const unsigned short& encryptionKeyIdentifier) = 0;

    /**
    * Set the frequency for this update.
    * <br>Description from the FOM: <i>The center frequency of the radio transmissions.</i>
    * <br>Description of the data type from the FOM: <i>Frequency of a radio transmission, in hertz. [unit: hertz (Hz), resolution: NA, accuracy: NA]</i>
    *
    * @param frequency the new frequency
    */
    LIBAPI virtual void setFrequency(const unsigned long long& frequency) = 0;

    /**
    * Set the frequencyBandwidth for this update.
    * <br>Description from the FOM: <i>The bandpass of the radio transmissions.</i>
    * <br>Description of the data type from the FOM: <i>Frequency, based on SI derived unit hertz, unit symbol Hz. [unit: hertz (Hz), resolution: NA, accuracy: NA]</i>
    *
    * @param frequencyBandwidth the new frequencyBandwidth
    */
    LIBAPI virtual void setFrequencyBandwidth(const float& frequencyBandwidth) = 0;

    /**
    * Set the radioIndex for this update.
    * <br>Description from the FOM: <i>A number that uniquely identifies this radio from others on the host.</i>
    * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
    *
    * @param radioIndex the new radioIndex
    */
    LIBAPI virtual void setRadioIndex(const unsigned short& radioIndex) = 0;

    /**
    * Set the radioInputSource for this update.
    * <br>Description from the FOM: <i>Specifies which position or data port provided the input for the transmission.</i>
    * <br>Description of the data type from the FOM: <i>Radio input source</i>
    *
    * @param radioInputSource the new radioInputSource
    */
    LIBAPI virtual void setRadioInputSource(const DevStudio::RadioInputSourceEnum::RadioInputSourceEnum& radioInputSource) = 0;

    /**
    * Set the radioSystemType for this update.
    * <br>Description from the FOM: <i>The type of radio transmitter.</i>
    * <br>Description of the data type from the FOM: <i>Specifies the type of a radio.</i>
    *
    * @param radioSystemType the new radioSystemType
    */
    LIBAPI virtual void setRadioSystemType(const DevStudio::RadioTypeStruct& radioSystemType) = 0;

    /**
    * Set the rFModulationSystemType for this update.
    * <br>Description from the FOM: <i>The radio system type associated with this transmitter.</i>
    * <br>Description of the data type from the FOM: <i>The radio system type associated with this transmitter.</i>
    *
    * @param rFModulationSystemType the new rFModulationSystemType
    */
    LIBAPI virtual void setRFModulationSystemType(const DevStudio::RFModulationSystemTypeEnum::RFModulationSystemTypeEnum& rFModulationSystemType) = 0;

    /**
    * Set the rFModulationType for this update.
    * <br>Description from the FOM: <i>Classification of the modulation type.</i>
    * <br>Description of the data type from the FOM: <i>Specifies the major modulation type as well as certain detailed information specific to the type.</i>
    *
    * @param rFModulationType the new rFModulationType
    */
    LIBAPI virtual void setRFModulationType(const DevStudio::RFModulationTypeVariantStruct& rFModulationType) = 0;

    /**
    * Set the spreadSpectrum for this update.
    * <br>Description from the FOM: <i>Describes the spread spectrum characteristics of the transmission, such as frequency hopping or other spread spectrum transmission modes.</i>
    * <br>Description of the data type from the FOM: <i>Identifies the actual spread spectrum technique in use.</i>
    *
    * @param spreadSpectrum the new spreadSpectrum
    */
    LIBAPI virtual void setSpreadSpectrum(const DevStudio::SpreadSpectrumVariantStruct& spreadSpectrum) = 0;

    /**
    * Set the streamTag for this update.
    * <br>Description from the FOM: <i>A globally unique identifier for the associated audio stream</i>
    * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^64-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
    *
    * @param streamTag the new streamTag
    */
    LIBAPI virtual void setStreamTag(const unsigned long long& streamTag) = 0;

    /**
    * Set the timeHopInUse for this update.
    * <br>Description from the FOM: <i>Whether the radio is using time hopping or not.</i>
    * <br>Description of the data type from the FOM: <i></i>
    *
    * @param timeHopInUse the new timeHopInUse
    */
    LIBAPI virtual void setTimeHopInUse(const bool& timeHopInUse) = 0;

    /**
    * Set the transmittedPower for this update.
    * <br>Description from the FOM: <i>The average transmitted power.</i>
    * <br>Description of the data type from the FOM: <i>Power ratio in decibels (dB) of a measured power referenced to 1 milliwatt (mW). [unit: decibel milliwatt (dBm), resolution: NA, accuracy: perfect]</i>
    *
    * @param transmittedPower the new transmittedPower
    */
    LIBAPI virtual void setTransmittedPower(const float& transmittedPower) = 0;

    /**
    * Set the transmitterOperationalStatus for this update.
    * <br>Description from the FOM: <i>The current operational state of the radio transmitter.</i>
    * <br>Description of the data type from the FOM: <i>The current operational state of a radio transmitter.</i>
    *
    * @param transmitterOperationalStatus the new transmitterOperationalStatus
    */
    LIBAPI virtual void setTransmitterOperationalStatus(const DevStudio::TransmitterOperationalStatusEnum::TransmitterOperationalStatusEnum& transmitterOperationalStatus) = 0;

    /**
    * Set the worldLocation for this update.
    * <br>Description from the FOM: <i>The location of the antenna in the world coordinate system.</i>
    * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
    *
    * @param worldLocation the new worldLocation
    */
    LIBAPI virtual void setWorldLocation(const DevStudio::WorldLocationStruct& worldLocation) = 0;

    /**
    * Set the entityIdentifier for this update.
    * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
    * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
    *
    * @param entityIdentifier the new entityIdentifier
    */
    LIBAPI virtual void setEntityIdentifier(const DevStudio::EntityIdentifierStruct& entityIdentifier) = 0;

    /**
    * Set the hostObjectIdentifier for this update.
    * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
    * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
    *
    * @param hostObjectIdentifier the new hostObjectIdentifier
    */
    LIBAPI virtual void setHostObjectIdentifier(const std::string& hostObjectIdentifier) = 0;

    /**
    * Set the relativePosition for this update.
    * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
    * <br>Description of the data type from the FOM: <i>Relative position in right-handed Cartesian coordinates.</i>
    *
    * @param relativePosition the new relativePosition
    */
    LIBAPI virtual void setRelativePosition(const DevStudio::RelativePositionStruct& relativePosition) = 0;

    /**
    * Send all the attributes.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate()
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

    /**
    * Send all the attributes with a specified timestamp.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @param timestamp timestamp to send for this update
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate(HlaTimeStampPtr timestamp)
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

    /**
    * Send all the attributes with a specified timestamp.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @param logicalTime logical time to send for this update
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaInvalidLogicalTimeException if the logical time is not valid.
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate(HlaLogicalTimePtr logicalTime)
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaInvalidLogicalTimeException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

    /**
    * Send all the attributes with a specified timestamp.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @param timestamp timestamp to send for this update
    * @param logicalTime logical time to send for this update
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaInvalidLogicalTimeException if the logical time is not valid.
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate(HlaTimeStampPtr timestamp, HlaLogicalTimePtr logicalTime)
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaInvalidLogicalTimeException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;
    };
}
#endif
