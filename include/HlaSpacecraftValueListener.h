/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLASPACECRAFTVALUELISTENER_H
#define DEVELOPER_STUDIO_HLASPACECRAFTVALUELISTENER_H

#include <memory>

#include <DevStudio/datatypes/ArticulatedParameterStruct.h>
#include <DevStudio/datatypes/ArticulatedParameterStructLengthlessArray.h>
#include <DevStudio/datatypes/CamouflageEnum.h>
#include <DevStudio/datatypes/DamageStatusEnum.h>
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EntityTypeStruct.h>
#include <DevStudio/datatypes/ForceIdentifierEnum.h>
#include <DevStudio/datatypes/HatchStateEnum.h>
#include <DevStudio/datatypes/IsPartOfStruct.h>
#include <DevStudio/datatypes/MarkingStruct.h>
#include <DevStudio/datatypes/PropulsionSystemDataStruct.h>
#include <DevStudio/datatypes/PropulsionSystemDataStructLengthlessArray.h>
#include <DevStudio/datatypes/SpatialVariantStruct.h>
#include <DevStudio/datatypes/TrailingEffectsCodeEnum.h>
#include <DevStudio/datatypes/VectoringNozzleSystemDataStruct.h>
#include <DevStudio/datatypes/VectoringNozzleSystemDataStructLengthlessArray.h>
#include <vector>

#include <DevStudio/HlaLogicalTime.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaSpacecraftAttributes.h>    

namespace DevStudio {

   /**
   * Listener for updates of attributes, with the new updated values.  
   */
   class HlaSpacecraftValueListener {

   public:

      LIBAPI virtual ~HlaSpacecraftValueListener() {}
    
      /**
      * This method is called when the attribute <code>afterburnerOn</code> is updated.
      * <br>Description from the FOM: <i>Whether the entity's afterburner is on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param spacecraft The object which is updated.
      * @param afterburnerOn The new value of the attribute in this update
      * @param validOldAfterburnerOn True if oldAfterburnerOn contains a valid value
      * @param oldAfterburnerOn The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void afterburnerOnUpdated(HlaSpacecraftPtr spacecraft, bool afterburnerOn, bool validOldAfterburnerOn, bool oldAfterburnerOn, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>antiCollisionLightsOn</code> is updated.
      * <br>Description from the FOM: <i>Whether the entity's anti-collision lights are on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param spacecraft The object which is updated.
      * @param antiCollisionLightsOn The new value of the attribute in this update
      * @param validOldAntiCollisionLightsOn True if oldAntiCollisionLightsOn contains a valid value
      * @param oldAntiCollisionLightsOn The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void antiCollisionLightsOnUpdated(HlaSpacecraftPtr spacecraft, bool antiCollisionLightsOn, bool validOldAntiCollisionLightsOn, bool oldAntiCollisionLightsOn, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>blackOutBrakeLightsOn</code> is updated.
      * <br>Description from the FOM: <i>Whether the entity's black out brake lights are on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param spacecraft The object which is updated.
      * @param blackOutBrakeLightsOn The new value of the attribute in this update
      * @param validOldBlackOutBrakeLightsOn True if oldBlackOutBrakeLightsOn contains a valid value
      * @param oldBlackOutBrakeLightsOn The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void blackOutBrakeLightsOnUpdated(HlaSpacecraftPtr spacecraft, bool blackOutBrakeLightsOn, bool validOldBlackOutBrakeLightsOn, bool oldBlackOutBrakeLightsOn, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>blackOutLightsOn</code> is updated.
      * <br>Description from the FOM: <i>Whether the entity's black out lights are on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param spacecraft The object which is updated.
      * @param blackOutLightsOn The new value of the attribute in this update
      * @param validOldBlackOutLightsOn True if oldBlackOutLightsOn contains a valid value
      * @param oldBlackOutLightsOn The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void blackOutLightsOnUpdated(HlaSpacecraftPtr spacecraft, bool blackOutLightsOn, bool validOldBlackOutLightsOn, bool oldBlackOutLightsOn, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>brakeLightsOn</code> is updated.
      * <br>Description from the FOM: <i>Whether the entity's brake lights are on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param spacecraft The object which is updated.
      * @param brakeLightsOn The new value of the attribute in this update
      * @param validOldBrakeLightsOn True if oldBrakeLightsOn contains a valid value
      * @param oldBrakeLightsOn The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void brakeLightsOnUpdated(HlaSpacecraftPtr spacecraft, bool brakeLightsOn, bool validOldBrakeLightsOn, bool oldBrakeLightsOn, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>formationLightsOn</code> is updated.
      * <br>Description from the FOM: <i>Whether the entity's formation lights are on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param spacecraft The object which is updated.
      * @param formationLightsOn The new value of the attribute in this update
      * @param validOldFormationLightsOn True if oldFormationLightsOn contains a valid value
      * @param oldFormationLightsOn The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void formationLightsOnUpdated(HlaSpacecraftPtr spacecraft, bool formationLightsOn, bool validOldFormationLightsOn, bool oldFormationLightsOn, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>hatchState</code> is updated.
      * <br>Description from the FOM: <i>The state of the entity's (main) hatch.</i>
      * <br>Description of the data type from the FOM: <i>Hatch state</i>
      *
      * @param spacecraft The object which is updated.
      * @param hatchState The new value of the attribute in this update
      * @param validOldHatchState True if oldHatchState contains a valid value
      * @param oldHatchState The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void hatchStateUpdated(HlaSpacecraftPtr spacecraft, HatchStateEnum::HatchStateEnum hatchState, bool validOldHatchState, HatchStateEnum::HatchStateEnum oldHatchState, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>headLightsOn</code> is updated.
      * <br>Description from the FOM: <i>Whether the entity's headlights are on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param spacecraft The object which is updated.
      * @param headLightsOn The new value of the attribute in this update
      * @param validOldHeadLightsOn True if oldHeadLightsOn contains a valid value
      * @param oldHeadLightsOn The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void headLightsOnUpdated(HlaSpacecraftPtr spacecraft, bool headLightsOn, bool validOldHeadLightsOn, bool oldHeadLightsOn, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>interiorLightsOn</code> is updated.
      * <br>Description from the FOM: <i>Whether the entity's internal lights are on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param spacecraft The object which is updated.
      * @param interiorLightsOn The new value of the attribute in this update
      * @param validOldInteriorLightsOn True if oldInteriorLightsOn contains a valid value
      * @param oldInteriorLightsOn The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void interiorLightsOnUpdated(HlaSpacecraftPtr spacecraft, bool interiorLightsOn, bool validOldInteriorLightsOn, bool oldInteriorLightsOn, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>landingLightsOn</code> is updated.
      * <br>Description from the FOM: <i>Whether the entity's landing lights are on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param spacecraft The object which is updated.
      * @param landingLightsOn The new value of the attribute in this update
      * @param validOldLandingLightsOn True if oldLandingLightsOn contains a valid value
      * @param oldLandingLightsOn The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void landingLightsOnUpdated(HlaSpacecraftPtr spacecraft, bool landingLightsOn, bool validOldLandingLightsOn, bool oldLandingLightsOn, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>launcherRaised</code> is updated.
      * <br>Description from the FOM: <i>Whether the entity's weapon launcher is in the raised position.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param spacecraft The object which is updated.
      * @param launcherRaised The new value of the attribute in this update
      * @param validOldLauncherRaised True if oldLauncherRaised contains a valid value
      * @param oldLauncherRaised The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void launcherRaisedUpdated(HlaSpacecraftPtr spacecraft, bool launcherRaised, bool validOldLauncherRaised, bool oldLauncherRaised, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>navigationLightsOn</code> is updated.
      * <br>Description from the FOM: <i>Whether the entity's navigation lights are on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param spacecraft The object which is updated.
      * @param navigationLightsOn The new value of the attribute in this update
      * @param validOldNavigationLightsOn True if oldNavigationLightsOn contains a valid value
      * @param oldNavigationLightsOn The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void navigationLightsOnUpdated(HlaSpacecraftPtr spacecraft, bool navigationLightsOn, bool validOldNavigationLightsOn, bool oldNavigationLightsOn, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>rampDeployed</code> is updated.
      * <br>Description from the FOM: <i>Whether the entity has deployed a ramp or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param spacecraft The object which is updated.
      * @param rampDeployed The new value of the attribute in this update
      * @param validOldRampDeployed True if oldRampDeployed contains a valid value
      * @param oldRampDeployed The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void rampDeployedUpdated(HlaSpacecraftPtr spacecraft, bool rampDeployed, bool validOldRampDeployed, bool oldRampDeployed, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>runningLightsOn</code> is updated.
      * <br>Description from the FOM: <i>Whether the entity's running lights are on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param spacecraft The object which is updated.
      * @param runningLightsOn The new value of the attribute in this update
      * @param validOldRunningLightsOn True if oldRunningLightsOn contains a valid value
      * @param oldRunningLightsOn The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void runningLightsOnUpdated(HlaSpacecraftPtr spacecraft, bool runningLightsOn, bool validOldRunningLightsOn, bool oldRunningLightsOn, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>spotLightsOn</code> is updated.
      * <br>Description from the FOM: <i>Whether the entity's spotlights are on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param spacecraft The object which is updated.
      * @param spotLightsOn The new value of the attribute in this update
      * @param validOldSpotLightsOn True if oldSpotLightsOn contains a valid value
      * @param oldSpotLightsOn The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void spotLightsOnUpdated(HlaSpacecraftPtr spacecraft, bool spotLightsOn, bool validOldSpotLightsOn, bool oldSpotLightsOn, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>tailLightsOn</code> is updated.
      * <br>Description from the FOM: <i>Whether the entity's tail lights are on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param spacecraft The object which is updated.
      * @param tailLightsOn The new value of the attribute in this update
      * @param validOldTailLightsOn True if oldTailLightsOn contains a valid value
      * @param oldTailLightsOn The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void tailLightsOnUpdated(HlaSpacecraftPtr spacecraft, bool tailLightsOn, bool validOldTailLightsOn, bool oldTailLightsOn, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>acousticSignatureIndex</code> is updated.
      * <br>Description from the FOM: <i>Index used to obtain the acoustics (sound through air) signature state of the entity.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^15, 2^15-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param spacecraft The object which is updated.
      * @param acousticSignatureIndex The new value of the attribute in this update
      * @param validOldAcousticSignatureIndex True if oldAcousticSignatureIndex contains a valid value
      * @param oldAcousticSignatureIndex The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void acousticSignatureIndexUpdated(HlaSpacecraftPtr spacecraft, short acousticSignatureIndex, bool validOldAcousticSignatureIndex, short oldAcousticSignatureIndex, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>alternateEntityType</code> is updated.
      * <br>Description from the FOM: <i>The category of entity to be used when viewed by entities on the 'opposite' side.</i>
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      *
      * @param spacecraft The object which is updated.
      * @param alternateEntityType The new value of the attribute in this update
      * @param validOldAlternateEntityType True if oldAlternateEntityType contains a valid value
      * @param oldAlternateEntityType The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void alternateEntityTypeUpdated(HlaSpacecraftPtr spacecraft, EntityTypeStruct alternateEntityType, bool validOldAlternateEntityType, EntityTypeStruct oldAlternateEntityType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>articulatedParametersArray</code> is updated.
      * <br>Description from the FOM: <i>Identification of the visible parts, and their states, of the entity which are capable of independent motion.</i>
      * <br>Description of the data type from the FOM: <i>Dynamic array of ArticulatedParameterStruct elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
      *
      * @param spacecraft The object which is updated.
      * @param articulatedParametersArray The new value of the attribute in this update
      * @param validOldArticulatedParametersArray True if oldArticulatedParametersArray contains a valid value
      * @param oldArticulatedParametersArray The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void articulatedParametersArrayUpdated(HlaSpacecraftPtr spacecraft, std::vector<DevStudio::ArticulatedParameterStruct > articulatedParametersArray, bool validOldArticulatedParametersArray, std::vector<DevStudio::ArticulatedParameterStruct > oldArticulatedParametersArray, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>camouflageType</code> is updated.
      * <br>Description from the FOM: <i>The type of camouflage in use (if any).</i>
      * <br>Description of the data type from the FOM: <i>Camouflage type</i>
      *
      * @param spacecraft The object which is updated.
      * @param camouflageType The new value of the attribute in this update
      * @param validOldCamouflageType True if oldCamouflageType contains a valid value
      * @param oldCamouflageType The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void camouflageTypeUpdated(HlaSpacecraftPtr spacecraft, CamouflageEnum::CamouflageEnum camouflageType, bool validOldCamouflageType, CamouflageEnum::CamouflageEnum oldCamouflageType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>damageState</code> is updated.
      * <br>Description from the FOM: <i>The state of damage of the entity.</i>
      * <br>Description of the data type from the FOM: <i>Damaged appearance</i>
      *
      * @param spacecraft The object which is updated.
      * @param damageState The new value of the attribute in this update
      * @param validOldDamageState True if oldDamageState contains a valid value
      * @param oldDamageState The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void damageStateUpdated(HlaSpacecraftPtr spacecraft, DamageStatusEnum::DamageStatusEnum damageState, bool validOldDamageState, DamageStatusEnum::DamageStatusEnum oldDamageState, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>engineSmokeOn</code> is updated.
      * <br>Description from the FOM: <i>Whether the entity's engine is generating smoke or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param spacecraft The object which is updated.
      * @param engineSmokeOn The new value of the attribute in this update
      * @param validOldEngineSmokeOn True if oldEngineSmokeOn contains a valid value
      * @param oldEngineSmokeOn The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void engineSmokeOnUpdated(HlaSpacecraftPtr spacecraft, bool engineSmokeOn, bool validOldEngineSmokeOn, bool oldEngineSmokeOn, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>firePowerDisabled</code> is updated.
      * <br>Description from the FOM: <i>Whether the entity's main weapon system has been disabled or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param spacecraft The object which is updated.
      * @param firePowerDisabled The new value of the attribute in this update
      * @param validOldFirePowerDisabled True if oldFirePowerDisabled contains a valid value
      * @param oldFirePowerDisabled The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void firePowerDisabledUpdated(HlaSpacecraftPtr spacecraft, bool firePowerDisabled, bool validOldFirePowerDisabled, bool oldFirePowerDisabled, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>flamesPresent</code> is updated.
      * <br>Description from the FOM: <i>Whether the entity is on fire (with visible flames) or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param spacecraft The object which is updated.
      * @param flamesPresent The new value of the attribute in this update
      * @param validOldFlamesPresent True if oldFlamesPresent contains a valid value
      * @param oldFlamesPresent The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void flamesPresentUpdated(HlaSpacecraftPtr spacecraft, bool flamesPresent, bool validOldFlamesPresent, bool oldFlamesPresent, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>forceIdentifier</code> is updated.
      * <br>Description from the FOM: <i>The identification of the force that the entity belongs to.</i>
      * <br>Description of the data type from the FOM: <i>Force ID</i>
      *
      * @param spacecraft The object which is updated.
      * @param forceIdentifier The new value of the attribute in this update
      * @param validOldForceIdentifier True if oldForceIdentifier contains a valid value
      * @param oldForceIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void forceIdentifierUpdated(HlaSpacecraftPtr spacecraft, ForceIdentifierEnum::ForceIdentifierEnum forceIdentifier, bool validOldForceIdentifier, ForceIdentifierEnum::ForceIdentifierEnum oldForceIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>hasAmmunitionSupplyCap</code> is updated.
      * <br>Description from the FOM: <i>Whether the entity has the capability to supply other entities with ammunition.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param spacecraft The object which is updated.
      * @param hasAmmunitionSupplyCap The new value of the attribute in this update
      * @param validOldHasAmmunitionSupplyCap True if oldHasAmmunitionSupplyCap contains a valid value
      * @param oldHasAmmunitionSupplyCap The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void hasAmmunitionSupplyCapUpdated(HlaSpacecraftPtr spacecraft, bool hasAmmunitionSupplyCap, bool validOldHasAmmunitionSupplyCap, bool oldHasAmmunitionSupplyCap, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>hasFuelSupplyCap</code> is updated.
      * <br>Description from the FOM: <i>Whether the entity has the capability to supply other entities with fuel or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param spacecraft The object which is updated.
      * @param hasFuelSupplyCap The new value of the attribute in this update
      * @param validOldHasFuelSupplyCap True if oldHasFuelSupplyCap contains a valid value
      * @param oldHasFuelSupplyCap The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void hasFuelSupplyCapUpdated(HlaSpacecraftPtr spacecraft, bool hasFuelSupplyCap, bool validOldHasFuelSupplyCap, bool oldHasFuelSupplyCap, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>hasRecoveryCap</code> is updated.
      * <br>Description from the FOM: <i>Whether the entity has the capability to recover other entities or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param spacecraft The object which is updated.
      * @param hasRecoveryCap The new value of the attribute in this update
      * @param validOldHasRecoveryCap True if oldHasRecoveryCap contains a valid value
      * @param oldHasRecoveryCap The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void hasRecoveryCapUpdated(HlaSpacecraftPtr spacecraft, bool hasRecoveryCap, bool validOldHasRecoveryCap, bool oldHasRecoveryCap, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>hasRepairCap</code> is updated.
      * <br>Description from the FOM: <i>Whether the entity has the capability to repair other entities or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param spacecraft The object which is updated.
      * @param hasRepairCap The new value of the attribute in this update
      * @param validOldHasRepairCap True if oldHasRepairCap contains a valid value
      * @param oldHasRepairCap The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void hasRepairCapUpdated(HlaSpacecraftPtr spacecraft, bool hasRepairCap, bool validOldHasRepairCap, bool oldHasRepairCap, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>immobilized</code> is updated.
      * <br>Description from the FOM: <i>Whether the entity is immobilized or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param spacecraft The object which is updated.
      * @param immobilized The new value of the attribute in this update
      * @param validOldImmobilized True if oldImmobilized contains a valid value
      * @param oldImmobilized The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void immobilizedUpdated(HlaSpacecraftPtr spacecraft, bool immobilized, bool validOldImmobilized, bool oldImmobilized, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>infraredSignatureIndex</code> is updated.
      * <br>Description from the FOM: <i>Index used to obtain the infra-red signature state of the entity.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^15, 2^15-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param spacecraft The object which is updated.
      * @param infraredSignatureIndex The new value of the attribute in this update
      * @param validOldInfraredSignatureIndex True if oldInfraredSignatureIndex contains a valid value
      * @param oldInfraredSignatureIndex The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void infraredSignatureIndexUpdated(HlaSpacecraftPtr spacecraft, short infraredSignatureIndex, bool validOldInfraredSignatureIndex, short oldInfraredSignatureIndex, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>isConcealed</code> is updated.
      * <br>Description from the FOM: <i>Whether the entity is concealed or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param spacecraft The object which is updated.
      * @param isConcealed The new value of the attribute in this update
      * @param validOldIsConcealed True if oldIsConcealed contains a valid value
      * @param oldIsConcealed The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void isConcealedUpdated(HlaSpacecraftPtr spacecraft, bool isConcealed, bool validOldIsConcealed, bool oldIsConcealed, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>liveEntityMeasuredSpeed</code> is updated.
      * <br>Description from the FOM: <i>The entity's own measurement of speed (e.g. air speed for aircraft).</i>
      * <br>Description of the data type from the FOM: <i>Velocity/Speed measured in decimeter per second. [unit: decimeter per second (dm/s), resolution: 1, accuracy: perfect]</i>
      *
      * @param spacecraft The object which is updated.
      * @param liveEntityMeasuredSpeed The new value of the attribute in this update
      * @param validOldLiveEntityMeasuredSpeed True if oldLiveEntityMeasuredSpeed contains a valid value
      * @param oldLiveEntityMeasuredSpeed The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void liveEntityMeasuredSpeedUpdated(HlaSpacecraftPtr spacecraft, unsigned short liveEntityMeasuredSpeed, bool validOldLiveEntityMeasuredSpeed, unsigned short oldLiveEntityMeasuredSpeed, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>marking</code> is updated.
      * <br>Description from the FOM: <i>A unique marking or combination of characters used to distinguish the entity from other entities.</i>
      * <br>Description of the data type from the FOM: <i>Character set used in the marking and the string of characters to be interpreted for display.</i>
      *
      * @param spacecraft The object which is updated.
      * @param marking The new value of the attribute in this update
      * @param validOldMarking True if oldMarking contains a valid value
      * @param oldMarking The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void markingUpdated(HlaSpacecraftPtr spacecraft, MarkingStruct marking, bool validOldMarking, MarkingStruct oldMarking, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>powerPlantOn</code> is updated.
      * <br>Description from the FOM: <i>Whether the entity's power plant is on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param spacecraft The object which is updated.
      * @param powerPlantOn The new value of the attribute in this update
      * @param validOldPowerPlantOn True if oldPowerPlantOn contains a valid value
      * @param oldPowerPlantOn The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void powerPlantOnUpdated(HlaSpacecraftPtr spacecraft, bool powerPlantOn, bool validOldPowerPlantOn, bool oldPowerPlantOn, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>propulsionSystemsData</code> is updated.
      * <br>Description from the FOM: <i>The basic operating data of the propulsion systems aboard the entity.</i>
      * <br>Description of the data type from the FOM: <i>A set of Propulsion System Data descriptions.</i>
      *
      * @param spacecraft The object which is updated.
      * @param propulsionSystemsData The new value of the attribute in this update
      * @param validOldPropulsionSystemsData True if oldPropulsionSystemsData contains a valid value
      * @param oldPropulsionSystemsData The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void propulsionSystemsDataUpdated(HlaSpacecraftPtr spacecraft, std::vector<DevStudio::PropulsionSystemDataStruct > propulsionSystemsData, bool validOldPropulsionSystemsData, std::vector<DevStudio::PropulsionSystemDataStruct > oldPropulsionSystemsData, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>radarCrossSectionSignatureIndex</code> is updated.
      * <br>Description from the FOM: <i>Index used to obtain the radar cross section signature state of the entity.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^15, 2^15-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param spacecraft The object which is updated.
      * @param radarCrossSectionSignatureIndex The new value of the attribute in this update
      * @param validOldRadarCrossSectionSignatureIndex True if oldRadarCrossSectionSignatureIndex contains a valid value
      * @param oldRadarCrossSectionSignatureIndex The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void radarCrossSectionSignatureIndexUpdated(HlaSpacecraftPtr spacecraft, short radarCrossSectionSignatureIndex, bool validOldRadarCrossSectionSignatureIndex, short oldRadarCrossSectionSignatureIndex, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>smokePlumePresent</code> is updated.
      * <br>Description from the FOM: <i>Whether the entity is generating smoke or not (intentional or unintentional).</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param spacecraft The object which is updated.
      * @param smokePlumePresent The new value of the attribute in this update
      * @param validOldSmokePlumePresent True if oldSmokePlumePresent contains a valid value
      * @param oldSmokePlumePresent The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void smokePlumePresentUpdated(HlaSpacecraftPtr spacecraft, bool smokePlumePresent, bool validOldSmokePlumePresent, bool oldSmokePlumePresent, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>tentDeployed</code> is updated.
      * <br>Description from the FOM: <i>Whether the entity has deployed tent or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param spacecraft The object which is updated.
      * @param tentDeployed The new value of the attribute in this update
      * @param validOldTentDeployed True if oldTentDeployed contains a valid value
      * @param oldTentDeployed The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void tentDeployedUpdated(HlaSpacecraftPtr spacecraft, bool tentDeployed, bool validOldTentDeployed, bool oldTentDeployed, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>trailingEffectsCode</code> is updated.
      * <br>Description from the FOM: <i>The type and size of any trail that the entity is making.</i>
      * <br>Description of the data type from the FOM: <i>Size of trailing effect</i>
      *
      * @param spacecraft The object which is updated.
      * @param trailingEffectsCode The new value of the attribute in this update
      * @param validOldTrailingEffectsCode True if oldTrailingEffectsCode contains a valid value
      * @param oldTrailingEffectsCode The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void trailingEffectsCodeUpdated(HlaSpacecraftPtr spacecraft, TrailingEffectsCodeEnum::TrailingEffectsCodeEnum trailingEffectsCode, bool validOldTrailingEffectsCode, TrailingEffectsCodeEnum::TrailingEffectsCodeEnum oldTrailingEffectsCode, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>vectoringNozzleSystemData</code> is updated.
      * <br>Description from the FOM: <i>The basic operational data for the vectoring nozzle systems aboard the entity.</i>
      * <br>Description of the data type from the FOM: <i>A set of Vectoring Nozzle System Data description.</i>
      *
      * @param spacecraft The object which is updated.
      * @param vectoringNozzleSystemData The new value of the attribute in this update
      * @param validOldVectoringNozzleSystemData True if oldVectoringNozzleSystemData contains a valid value
      * @param oldVectoringNozzleSystemData The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void vectoringNozzleSystemDataUpdated(HlaSpacecraftPtr spacecraft, std::vector<DevStudio::VectoringNozzleSystemDataStruct > vectoringNozzleSystemData, bool validOldVectoringNozzleSystemData, std::vector<DevStudio::VectoringNozzleSystemDataStruct > oldVectoringNozzleSystemData, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>entityType</code> is updated.
      * <br>Description from the FOM: <i>The category of the entity.</i>
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      *
      * @param spacecraft The object which is updated.
      * @param entityType The new value of the attribute in this update
      * @param validOldEntityType True if oldEntityType contains a valid value
      * @param oldEntityType The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void entityTypeUpdated(HlaSpacecraftPtr spacecraft, EntityTypeStruct entityType, bool validOldEntityType, EntityTypeStruct oldEntityType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>entityIdentifier</code> is updated.
      * <br>Description from the FOM: <i>The unique identifier for the entity instance.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @param spacecraft The object which is updated.
      * @param entityIdentifier The new value of the attribute in this update
      * @param validOldEntityIdentifier True if oldEntityIdentifier contains a valid value
      * @param oldEntityIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void entityIdentifierUpdated(HlaSpacecraftPtr spacecraft, EntityIdentifierStruct entityIdentifier, bool validOldEntityIdentifier, EntityIdentifierStruct oldEntityIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>isPartOf</code> is updated.
      * <br>Description from the FOM: <i>Defines if the entity if a constituent part of another entity (denoted the host entity). If the entity is a constituent part of another entity then the HostEntityIdentifier shall be set to the EntityIdentifier of the host entity and the HostRTIObjectIdentifier shall be set to the RTI object instance ID of the host entity. If the entity is not a constituent part of another entity then the HostEntityIdentifier shall be set to 0.0.0 and the HostRTIObjectIdentifier shall be set to the empty string.</i>
      * <br>Description of the data type from the FOM: <i>Defines the spatial relationship between two objects.</i>
      *
      * @param spacecraft The object which is updated.
      * @param isPartOf The new value of the attribute in this update
      * @param validOldIsPartOf True if oldIsPartOf contains a valid value
      * @param oldIsPartOf The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void isPartOfUpdated(HlaSpacecraftPtr spacecraft, IsPartOfStruct isPartOf, bool validOldIsPartOf, IsPartOfStruct oldIsPartOf, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>spatial</code> is updated.
      * <br>Description from the FOM: <i>Spatial state stored in one variant record attribute.</i>
      * <br>Description of the data type from the FOM: <i>Variant Record for a single spatial attribute.</i>
      *
      * @param spacecraft The object which is updated.
      * @param spatial The new value of the attribute in this update
      * @param validOldSpatial True if oldSpatial contains a valid value
      * @param oldSpatial The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void spatialUpdated(HlaSpacecraftPtr spacecraft, SpatialVariantStruct spatial, bool validOldSpatial, SpatialVariantStruct oldSpatial, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>relativeSpatial</code> is updated.
      * <br>Description from the FOM: <i>Relative spatial state stored in one variant record attribute.</i>
      * <br>Description of the data type from the FOM: <i>Variant Record for a single spatial attribute.</i>
      *
      * @param spacecraft The object which is updated.
      * @param relativeSpatial The new value of the attribute in this update
      * @param validOldRelativeSpatial True if oldRelativeSpatial contains a valid value
      * @param oldRelativeSpatial The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void relativeSpatialUpdated(HlaSpacecraftPtr spacecraft, SpatialVariantStruct relativeSpatial, bool validOldRelativeSpatial, SpatialVariantStruct oldRelativeSpatial, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
      /**
      * This method is called when the producing federate of an attribute is changed.
      * Only available when using HLA Evolved.
      *
      * @param spacecraft The object which is updated.
      * @param attribute The attribute that now has a new producing federate
      * @param oldProducingFederate The federate handle of the old producing federate
      * @param newProducingFederate The federate handle of the new producing federate
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void producingFederateUpdated(HlaSpacecraftPtr spacecraft, HlaSpacecraftAttributes::Attribute attribute, HlaFederateIdPtr oldProducingFederate, HlaFederateIdPtr newProducingFederate, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;

      class Adapter;
   };

   /**
   * An adapter class that implements the HlaSpacecraftValueListener interface with empty methods.
   * It might be used as a base class for a listener.
   */
   class HlaSpacecraftValueListener::Adapter : public HlaSpacecraftValueListener {

   public:

      LIBAPI virtual void afterburnerOnUpdated(HlaSpacecraftPtr spacecraft, bool afterburnerOn, bool validOldAfterburnerOn, bool oldAfterburnerOn, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void antiCollisionLightsOnUpdated(HlaSpacecraftPtr spacecraft, bool antiCollisionLightsOn, bool validOldAntiCollisionLightsOn, bool oldAntiCollisionLightsOn, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void blackOutBrakeLightsOnUpdated(HlaSpacecraftPtr spacecraft, bool blackOutBrakeLightsOn, bool validOldBlackOutBrakeLightsOn, bool oldBlackOutBrakeLightsOn, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void blackOutLightsOnUpdated(HlaSpacecraftPtr spacecraft, bool blackOutLightsOn, bool validOldBlackOutLightsOn, bool oldBlackOutLightsOn, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void brakeLightsOnUpdated(HlaSpacecraftPtr spacecraft, bool brakeLightsOn, bool validOldBrakeLightsOn, bool oldBrakeLightsOn, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void formationLightsOnUpdated(HlaSpacecraftPtr spacecraft, bool formationLightsOn, bool validOldFormationLightsOn, bool oldFormationLightsOn, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void hatchStateUpdated(HlaSpacecraftPtr spacecraft, HatchStateEnum::HatchStateEnum hatchState, bool validOldHatchState, HatchStateEnum::HatchStateEnum oldHatchState, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void headLightsOnUpdated(HlaSpacecraftPtr spacecraft, bool headLightsOn, bool validOldHeadLightsOn, bool oldHeadLightsOn, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void interiorLightsOnUpdated(HlaSpacecraftPtr spacecraft, bool interiorLightsOn, bool validOldInteriorLightsOn, bool oldInteriorLightsOn, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void landingLightsOnUpdated(HlaSpacecraftPtr spacecraft, bool landingLightsOn, bool validOldLandingLightsOn, bool oldLandingLightsOn, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void launcherRaisedUpdated(HlaSpacecraftPtr spacecraft, bool launcherRaised, bool validOldLauncherRaised, bool oldLauncherRaised, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void navigationLightsOnUpdated(HlaSpacecraftPtr spacecraft, bool navigationLightsOn, bool validOldNavigationLightsOn, bool oldNavigationLightsOn, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void rampDeployedUpdated(HlaSpacecraftPtr spacecraft, bool rampDeployed, bool validOldRampDeployed, bool oldRampDeployed, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void runningLightsOnUpdated(HlaSpacecraftPtr spacecraft, bool runningLightsOn, bool validOldRunningLightsOn, bool oldRunningLightsOn, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void spotLightsOnUpdated(HlaSpacecraftPtr spacecraft, bool spotLightsOn, bool validOldSpotLightsOn, bool oldSpotLightsOn, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void tailLightsOnUpdated(HlaSpacecraftPtr spacecraft, bool tailLightsOn, bool validOldTailLightsOn, bool oldTailLightsOn, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void acousticSignatureIndexUpdated(HlaSpacecraftPtr spacecraft, short acousticSignatureIndex, bool validOldAcousticSignatureIndex, short oldAcousticSignatureIndex, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void alternateEntityTypeUpdated(HlaSpacecraftPtr spacecraft, EntityTypeStruct alternateEntityType, bool validOldAlternateEntityType, EntityTypeStruct oldAlternateEntityType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void articulatedParametersArrayUpdated(HlaSpacecraftPtr spacecraft, std::vector<DevStudio::ArticulatedParameterStruct > articulatedParametersArray, bool validOldArticulatedParametersArray, std::vector<DevStudio::ArticulatedParameterStruct > oldArticulatedParametersArray, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void camouflageTypeUpdated(HlaSpacecraftPtr spacecraft, CamouflageEnum::CamouflageEnum camouflageType, bool validOldCamouflageType, CamouflageEnum::CamouflageEnum oldCamouflageType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void damageStateUpdated(HlaSpacecraftPtr spacecraft, DamageStatusEnum::DamageStatusEnum damageState, bool validOldDamageState, DamageStatusEnum::DamageStatusEnum oldDamageState, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void engineSmokeOnUpdated(HlaSpacecraftPtr spacecraft, bool engineSmokeOn, bool validOldEngineSmokeOn, bool oldEngineSmokeOn, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void firePowerDisabledUpdated(HlaSpacecraftPtr spacecraft, bool firePowerDisabled, bool validOldFirePowerDisabled, bool oldFirePowerDisabled, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void flamesPresentUpdated(HlaSpacecraftPtr spacecraft, bool flamesPresent, bool validOldFlamesPresent, bool oldFlamesPresent, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void forceIdentifierUpdated(HlaSpacecraftPtr spacecraft, ForceIdentifierEnum::ForceIdentifierEnum forceIdentifier, bool validOldForceIdentifier, ForceIdentifierEnum::ForceIdentifierEnum oldForceIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void hasAmmunitionSupplyCapUpdated(HlaSpacecraftPtr spacecraft, bool hasAmmunitionSupplyCap, bool validOldHasAmmunitionSupplyCap, bool oldHasAmmunitionSupplyCap, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void hasFuelSupplyCapUpdated(HlaSpacecraftPtr spacecraft, bool hasFuelSupplyCap, bool validOldHasFuelSupplyCap, bool oldHasFuelSupplyCap, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void hasRecoveryCapUpdated(HlaSpacecraftPtr spacecraft, bool hasRecoveryCap, bool validOldHasRecoveryCap, bool oldHasRecoveryCap, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void hasRepairCapUpdated(HlaSpacecraftPtr spacecraft, bool hasRepairCap, bool validOldHasRepairCap, bool oldHasRepairCap, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void immobilizedUpdated(HlaSpacecraftPtr spacecraft, bool immobilized, bool validOldImmobilized, bool oldImmobilized, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void infraredSignatureIndexUpdated(HlaSpacecraftPtr spacecraft, short infraredSignatureIndex, bool validOldInfraredSignatureIndex, short oldInfraredSignatureIndex, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void isConcealedUpdated(HlaSpacecraftPtr spacecraft, bool isConcealed, bool validOldIsConcealed, bool oldIsConcealed, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void liveEntityMeasuredSpeedUpdated(HlaSpacecraftPtr spacecraft, unsigned short liveEntityMeasuredSpeed, bool validOldLiveEntityMeasuredSpeed, unsigned short oldLiveEntityMeasuredSpeed, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void markingUpdated(HlaSpacecraftPtr spacecraft, MarkingStruct marking, bool validOldMarking, MarkingStruct oldMarking, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void powerPlantOnUpdated(HlaSpacecraftPtr spacecraft, bool powerPlantOn, bool validOldPowerPlantOn, bool oldPowerPlantOn, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void propulsionSystemsDataUpdated(HlaSpacecraftPtr spacecraft, std::vector<DevStudio::PropulsionSystemDataStruct > propulsionSystemsData, bool validOldPropulsionSystemsData, std::vector<DevStudio::PropulsionSystemDataStruct > oldPropulsionSystemsData, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void radarCrossSectionSignatureIndexUpdated(HlaSpacecraftPtr spacecraft, short radarCrossSectionSignatureIndex, bool validOldRadarCrossSectionSignatureIndex, short oldRadarCrossSectionSignatureIndex, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void smokePlumePresentUpdated(HlaSpacecraftPtr spacecraft, bool smokePlumePresent, bool validOldSmokePlumePresent, bool oldSmokePlumePresent, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void tentDeployedUpdated(HlaSpacecraftPtr spacecraft, bool tentDeployed, bool validOldTentDeployed, bool oldTentDeployed, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void trailingEffectsCodeUpdated(HlaSpacecraftPtr spacecraft, TrailingEffectsCodeEnum::TrailingEffectsCodeEnum trailingEffectsCode, bool validOldTrailingEffectsCode, TrailingEffectsCodeEnum::TrailingEffectsCodeEnum oldTrailingEffectsCode, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void vectoringNozzleSystemDataUpdated(HlaSpacecraftPtr spacecraft, std::vector<DevStudio::VectoringNozzleSystemDataStruct > vectoringNozzleSystemData, bool validOldVectoringNozzleSystemData, std::vector<DevStudio::VectoringNozzleSystemDataStruct > oldVectoringNozzleSystemData, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void entityTypeUpdated(HlaSpacecraftPtr spacecraft, EntityTypeStruct entityType, bool validOldEntityType, EntityTypeStruct oldEntityType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void entityIdentifierUpdated(HlaSpacecraftPtr spacecraft, EntityIdentifierStruct entityIdentifier, bool validOldEntityIdentifier, EntityIdentifierStruct oldEntityIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void isPartOfUpdated(HlaSpacecraftPtr spacecraft, IsPartOfStruct isPartOf, bool validOldIsPartOf, IsPartOfStruct oldIsPartOf, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void spatialUpdated(HlaSpacecraftPtr spacecraft, SpatialVariantStruct spatial, bool validOldSpatial, SpatialVariantStruct oldSpatial, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void relativeSpatialUpdated(HlaSpacecraftPtr spacecraft, SpatialVariantStruct relativeSpatial, bool validOldRelativeSpatial, SpatialVariantStruct oldRelativeSpatial, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
      LIBAPI virtual void producingFederateUpdated(HlaSpacecraftPtr spacecraft, HlaSpacecraftAttributes::Attribute attribute, HlaFederateIdPtr oldProducingFederate, HlaFederateIdPtr newProducingFederate, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
   };

}
#endif
