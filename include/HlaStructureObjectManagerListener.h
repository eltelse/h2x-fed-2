/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLASTRUCTUREOBJECTMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLASTRUCTUREOBJECTMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaStructureObject.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaStructureObject instances.
    */
    class HlaStructureObjectManagerListener {

    public:

        LIBAPI virtual ~HlaStructureObjectManagerListener() {}

        /**
        * This method is called when a new HlaStructureObject instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param structureObject the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaStructureObjectDiscovered(HlaStructureObjectPtr structureObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaStructureObject instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param structureObject the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaStructureObjectInitialized(HlaStructureObjectPtr structureObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaStructureObjectManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param structureObject the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaStructureObjectInInterest(HlaStructureObjectPtr structureObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaStructureObjectManagerListener instance goes out of interest.
        *
        * @param structureObject the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaStructureObjectOutOfInterest(HlaStructureObjectPtr structureObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaStructureObject instance is deleted.
        *
        * @param structureObject the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaStructureObjectDeleted(HlaStructureObjectPtr structureObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaStructureObjectManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaStructureObjectManagerListener::Adapter : public HlaStructureObjectManagerListener {

    public:
        LIBAPI virtual void hlaStructureObjectDiscovered(HlaStructureObjectPtr structureObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaStructureObjectInitialized(HlaStructureObjectPtr structureObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaStructureObjectInInterest(HlaStructureObjectPtr structureObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaStructureObjectOutOfInterest(HlaStructureObjectPtr structureObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaStructureObjectDeleted(HlaStructureObjectPtr structureObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
