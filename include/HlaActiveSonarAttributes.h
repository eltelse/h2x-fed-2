/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAACTIVESONARATTRIBUTES_H
#define DEVELOPER_STUDIO_HLAACTIVESONARATTRIBUTES_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <string>
#include <vector>
#include <utility>
    
#include <boost/noncopyable.hpp>
    
#include <DevStudio/datatypes/ActiveSonarEnum.h>
#include <DevStudio/datatypes/ActiveSonarFunctionCodeEnum.h>
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EventIdentifierStruct.h>
#include <DevStudio/datatypes/RelativePositionStruct.h>
#include <string>

#include <DevStudio/HlaException.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaUnderwaterAcousticsEmissionAttributes.h>
#include <DevStudio/HlaTimeStamped.h>

namespace DevStudio {

   /**
   * Interface used to represent all attributes for an object instance.
   */
   class HlaActiveSonarAttributes : public HlaUnderwaterAcousticsEmissionAttributes {
   public:


     /**
      * An enumeration of the attributes of an HlaActiveSonar
      *
      *<table summary="All attributes">
      * <tr><td><b>Enum constant</b></td><td><b>C++ name</b></td><td><b>FOM name</b></td></tr>
      * <tr><td>ACOUSTIC_NAME</td><td>acousticName</td><td><code>AcousticName</code></td></tr>
      * <tr><td>FUNCTION_CODE</td><td>functionCode</td><td><code>FunctionCode</code></td></tr>
      * <tr><td>ACOUSTICS_IDENTIFIER</td><td>acousticsIdentifier</td><td><code>AcousticsIdentifier</code></td></tr>
      * <tr><td>EVENT_IDENTIFIER</td><td>eventIdentifier</td><td><code>EventIdentifier</code></td></tr>
      * <tr><td>ENTITY_IDENTIFIER</td><td>entityIdentifier</td><td><code>EntityIdentifier</code></td></tr>
      * <tr><td>HOST_OBJECT_IDENTIFIER</td><td>hostObjectIdentifier</td><td><code>HostObjectIdentifier</code></td></tr>
      * <tr><td>RELATIVE_POSITION</td><td>relativePosition</td><td><code>RelativePosition</code></td></tr>
      *</table>
      */
      enum Attribute {
        /**
        * acousticName (FOM name: <code>AcousticName</code>).
        * <br>Description from the FOM: <i>Defines the type of sonar being represented.</i>
        */
         ACOUSTIC_NAME,

        /**
        * functionCode (FOM name: <code>FunctionCode</code>).
        * <br>Description from the FOM: <i>Declares the current primary use of the sonar. It may be used by simulatons to infer intent.</i>
        */
         FUNCTION_CODE,

        /**
        * acousticsIdentifier (FOM name: <code>AcousticsIdentifier</code>).
        * <br>Description from the FOM: <i>Unique identifier for the sonar on an entity, Starts with 1</i>
        */
         ACOUSTICS_IDENTIFIER,

        /**
        * eventIdentifier (FOM name: <code>EventIdentifier</code>).
        * <br>Description from the FOM: <i>The generating federate uses the Event Identifier to associate related events. The event number begins at one at the beginning of the exercise and is incremented by one for each event.</i>
        */
         EVENT_IDENTIFIER,

        /**
        * entityIdentifier (FOM name: <code>EntityIdentifier</code>).
        * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
        */
         ENTITY_IDENTIFIER,

        /**
        * hostObjectIdentifier (FOM name: <code>HostObjectIdentifier</code>).
        * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
        */
         HOST_OBJECT_IDENTIFIER,

        /**
        * relativePosition (FOM name: <code>RelativePosition</code>).
        * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
        */
         RELATIVE_POSITION
      };

     /**
      * Gets the name of the attribute.
      *
      * @return The name of the attribute. An empty string will be returned if the attribute does not exist.
      */
      LIBAPI virtual std::wstring getName(Attribute attribute);

     /**
      * Finds the attribute specified in the parameter attributeName.
      * The found enumeration will be stored in the parameter attribute.
      *
      * @return true if the attribute was found, false otherwise.
      */
      LIBAPI virtual bool find(Attribute& attribute, std::wstring attributeName);

      LIBAPI virtual ~HlaActiveSonarAttributes() {}
    
      /**
      * Returns true if the <code>acousticName</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Defines the type of sonar being represented.</i>
      *
      * @return true if <code>acousticName</code> is available.
      */
      LIBAPI virtual bool hasAcousticName() = 0;

      /**
      * Gets the value of the <code>acousticName</code> attribute.
      *
      * <br>Description from the FOM: <i>Defines the type of sonar being represented.</i>
      * <br>Description of the data type from the FOM: <i>Acoustic system name</i>
      *
      * @return the <code>acousticName</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::ActiveSonarEnum::ActiveSonarEnum getAcousticName()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>acousticName</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Defines the type of sonar being represented.</i>
      * <br>Description of the data type from the FOM: <i>Acoustic system name</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>acousticName</code> attribute.
      */
      LIBAPI virtual DevStudio::ActiveSonarEnum::ActiveSonarEnum getAcousticName(DevStudio::ActiveSonarEnum::ActiveSonarEnum defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>acousticName</code> attribute.
      * <br>Description from the FOM: <i>Defines the type of sonar being represented.</i>
      * <br>Description of the data type from the FOM: <i>Acoustic system name</i>
      *
      * @return the time stamped <code>acousticName</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::ActiveSonarEnum::ActiveSonarEnum > getAcousticNameTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>functionCode</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Declares the current primary use of the sonar. It may be used by simulatons to infer intent.</i>
      *
      * @return true if <code>functionCode</code> is available.
      */
      LIBAPI virtual bool hasFunctionCode() = 0;

      /**
      * Gets the value of the <code>functionCode</code> attribute.
      *
      * <br>Description from the FOM: <i>Declares the current primary use of the sonar. It may be used by simulatons to infer intent.</i>
      * <br>Description of the data type from the FOM: <i>The current function being performed by the sonar</i>
      *
      * @return the <code>functionCode</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::ActiveSonarFunctionCodeEnum::ActiveSonarFunctionCodeEnum getFunctionCode()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>functionCode</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Declares the current primary use of the sonar. It may be used by simulatons to infer intent.</i>
      * <br>Description of the data type from the FOM: <i>The current function being performed by the sonar</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>functionCode</code> attribute.
      */
      LIBAPI virtual DevStudio::ActiveSonarFunctionCodeEnum::ActiveSonarFunctionCodeEnum getFunctionCode(DevStudio::ActiveSonarFunctionCodeEnum::ActiveSonarFunctionCodeEnum defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>functionCode</code> attribute.
      * <br>Description from the FOM: <i>Declares the current primary use of the sonar. It may be used by simulatons to infer intent.</i>
      * <br>Description of the data type from the FOM: <i>The current function being performed by the sonar</i>
      *
      * @return the time stamped <code>functionCode</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::ActiveSonarFunctionCodeEnum::ActiveSonarFunctionCodeEnum > getFunctionCodeTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>acousticsIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Unique identifier for the sonar on an entity, Starts with 1</i>
      *
      * @return true if <code>acousticsIdentifier</code> is available.
      */
      LIBAPI virtual bool hasAcousticsIdentifier() = 0;

      /**
      * Gets the value of the <code>acousticsIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>Unique identifier for the sonar on an entity, Starts with 1</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the <code>acousticsIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual char getAcousticsIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>acousticsIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Unique identifier for the sonar on an entity, Starts with 1</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>acousticsIdentifier</code> attribute.
      */
      LIBAPI virtual char getAcousticsIdentifier(char defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>acousticsIdentifier</code> attribute.
      * <br>Description from the FOM: <i>Unique identifier for the sonar on an entity, Starts with 1</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the time stamped <code>acousticsIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< char > getAcousticsIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>eventIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The generating federate uses the Event Identifier to associate related events. The event number begins at one at the beginning of the exercise and is incremented by one for each event.</i>
      *
      * @return true if <code>eventIdentifier</code> is available.
      */
      LIBAPI virtual bool hasEventIdentifier() = 0;

      /**
      * Gets the value of the <code>eventIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>The generating federate uses the Event Identifier to associate related events. The event number begins at one at the beginning of the exercise and is incremented by one for each event.</i>
      * <br>Description of the data type from the FOM: <i>Identification of an event. Based on the Event Identifier record as specified in IEEE 1278.1-1995 section 5.2.18.</i>
      *
      * @return the <code>eventIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::EventIdentifierStruct getEventIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>eventIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The generating federate uses the Event Identifier to associate related events. The event number begins at one at the beginning of the exercise and is incremented by one for each event.</i>
      * <br>Description of the data type from the FOM: <i>Identification of an event. Based on the Event Identifier record as specified in IEEE 1278.1-1995 section 5.2.18.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>eventIdentifier</code> attribute.
      */
      LIBAPI virtual DevStudio::EventIdentifierStruct getEventIdentifier(DevStudio::EventIdentifierStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>eventIdentifier</code> attribute.
      * <br>Description from the FOM: <i>The generating federate uses the Event Identifier to associate related events. The event number begins at one at the beginning of the exercise and is incremented by one for each event.</i>
      * <br>Description of the data type from the FOM: <i>Identification of an event. Based on the Event Identifier record as specified in IEEE 1278.1-1995 section 5.2.18.</i>
      *
      * @return the time stamped <code>eventIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::EventIdentifierStruct > getEventIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>entityIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
      *
      * @return true if <code>entityIdentifier</code> is available.
      */
      LIBAPI virtual bool hasEntityIdentifier() = 0;

      /**
      * Gets the value of the <code>entityIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @return the <code>entityIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::EntityIdentifierStruct getEntityIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>entityIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>entityIdentifier</code> attribute.
      */
      LIBAPI virtual DevStudio::EntityIdentifierStruct getEntityIdentifier(DevStudio::EntityIdentifierStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>entityIdentifier</code> attribute.
      * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @return the time stamped <code>entityIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::EntityIdentifierStruct > getEntityIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>hostObjectIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
      *
      * @return true if <code>hostObjectIdentifier</code> is available.
      */
      LIBAPI virtual bool hasHostObjectIdentifier() = 0;

      /**
      * Gets the value of the <code>hostObjectIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @return the <code>hostObjectIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::string getHostObjectIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>hostObjectIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>hostObjectIdentifier</code> attribute.
      */
      LIBAPI virtual std::string getHostObjectIdentifier(std::string defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>hostObjectIdentifier</code> attribute.
      * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @return the time stamped <code>hostObjectIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::string > getHostObjectIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>relativePosition</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
      *
      * @return true if <code>relativePosition</code> is available.
      */
      LIBAPI virtual bool hasRelativePosition() = 0;

      /**
      * Gets the value of the <code>relativePosition</code> attribute.
      *
      * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
      * <br>Description of the data type from the FOM: <i>Relative position in right-handed Cartesian coordinates.</i>
      *
      * @return the <code>relativePosition</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::RelativePositionStruct getRelativePosition()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>relativePosition</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
      * <br>Description of the data type from the FOM: <i>Relative position in right-handed Cartesian coordinates.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>relativePosition</code> attribute.
      */
      LIBAPI virtual DevStudio::RelativePositionStruct getRelativePosition(DevStudio::RelativePositionStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>relativePosition</code> attribute.
      * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
      * <br>Description of the data type from the FOM: <i>Relative position in right-handed Cartesian coordinates.</i>
      *
      * @return the time stamped <code>relativePosition</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::RelativePositionStruct > getRelativePositionTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
   };
}
#endif
