/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAMULTIDOMAINPLATFORMMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLAMULTIDOMAINPLATFORMMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaMultiDomainPlatform.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaMultiDomainPlatform instances.
    */
    class HlaMultiDomainPlatformManagerListener {

    public:

        LIBAPI virtual ~HlaMultiDomainPlatformManagerListener() {}

        /**
        * This method is called when a new HlaMultiDomainPlatform instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param multiDomainPlatform the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaMultiDomainPlatformDiscovered(HlaMultiDomainPlatformPtr multiDomainPlatform, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaMultiDomainPlatform instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param multiDomainPlatform the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaMultiDomainPlatformInitialized(HlaMultiDomainPlatformPtr multiDomainPlatform, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaMultiDomainPlatformManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param multiDomainPlatform the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaMultiDomainPlatformInInterest(HlaMultiDomainPlatformPtr multiDomainPlatform, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaMultiDomainPlatformManagerListener instance goes out of interest.
        *
        * @param multiDomainPlatform the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaMultiDomainPlatformOutOfInterest(HlaMultiDomainPlatformPtr multiDomainPlatform, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaMultiDomainPlatform instance is deleted.
        *
        * @param multiDomainPlatform the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaMultiDomainPlatformDeleted(HlaMultiDomainPlatformPtr multiDomainPlatform, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaMultiDomainPlatformManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaMultiDomainPlatformManagerListener::Adapter : public HlaMultiDomainPlatformManagerListener {

    public:
        LIBAPI virtual void hlaMultiDomainPlatformDiscovered(HlaMultiDomainPlatformPtr multiDomainPlatform, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaMultiDomainPlatformInitialized(HlaMultiDomainPlatformPtr multiDomainPlatform, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaMultiDomainPlatformInInterest(HlaMultiDomainPlatformPtr multiDomainPlatform, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaMultiDomainPlatformOutOfInterest(HlaMultiDomainPlatformPtr multiDomainPlatform, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaMultiDomainPlatformDeleted(HlaMultiDomainPlatformPtr multiDomainPlatform, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
