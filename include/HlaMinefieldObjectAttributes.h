/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAMINEFIELDOBJECTATTRIBUTES_H
#define DEVELOPER_STUDIO_HLAMINEFIELDOBJECTATTRIBUTES_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <string>
#include <vector>
#include <utility>
    
#include <boost/noncopyable.hpp>
    
#include <DevStudio/datatypes/BreachedStatusEnum.h>
#include <DevStudio/datatypes/DamageStatusEnum.h>
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EnvironmentObjectTypeStruct.h>
#include <DevStudio/datatypes/ForceIdentifierEnum.h>
#include <DevStudio/datatypes/WorldLocationStruct.h>
#include <DevStudio/datatypes/WorldLocationStructLengthlessArray.h>
#include <string>
#include <vector>

#include <DevStudio/HlaException.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaArealObjectAttributes.h>
#include <DevStudio/HlaTimeStamped.h>

namespace DevStudio {

   /**
   * Interface used to represent all attributes for an object instance.
   */
   class HlaMinefieldObjectAttributes : public HlaArealObjectAttributes {
   public:


     /**
      * An enumeration of the attributes of an HlaMinefieldObject
      *
      *<table summary="All attributes">
      * <tr><td><b>Enum constant</b></td><td><b>C++ name</b></td><td><b>FOM name</b></td></tr>
      * <tr><td>BREACHED_STATUS</td><td>breachedStatus</td><td><code>BreachedStatus</code></td></tr>
      * <tr><td>MINE_COUNT</td><td>mineCount</td><td><code>MineCount</code></td></tr>
      * <tr><td>POINTS_DATA</td><td>pointsData</td><td><code>PointsData</code></td></tr>
      * <tr><td>PERCENT_COMPLETE</td><td>percentComplete</td><td><code>PercentComplete</code></td></tr>
      * <tr><td>DAMAGED_APPEARANCE</td><td>damagedAppearance</td><td><code>DamagedAppearance</code></td></tr>
      * <tr><td>OBJECT_PRE_DISTRIBUTED</td><td>objectPreDistributed</td><td><code>ObjectPreDistributed</code></td></tr>
      * <tr><td>DEACTIVATED</td><td>deactivated</td><td><code>Deactivated</code></td></tr>
      * <tr><td>SMOKING</td><td>smoking</td><td><code>Smoking</code></td></tr>
      * <tr><td>FLAMING</td><td>flaming</td><td><code>Flaming</code></td></tr>
      * <tr><td>OBJECT_IDENTIFIER</td><td>objectIdentifier</td><td><code>ObjectIdentifier</code></td></tr>
      * <tr><td>REFERENCED_OBJECT_IDENTIFIER</td><td>referencedObjectIdentifier</td><td><code>ReferencedObjectIdentifier</code></td></tr>
      * <tr><td>FORCE_IDENTIFIER</td><td>forceIdentifier</td><td><code>ForceIdentifier</code></td></tr>
      * <tr><td>OBJECT_TYPE</td><td>objectType</td><td><code>ObjectType</code></td></tr>
      *</table>
      */
      enum Attribute {
        /**
        * breachedStatus (FOM name: <code>BreachedStatus</code>).
        * <br>Description from the FOM: <i>Specifies the breached appearance of the minefield object</i>
        */
         BREACHED_STATUS,

        /**
        * mineCount (FOM name: <code>MineCount</code>).
        * <br>Description from the FOM: <i>Specifies the number of mines in the minefield</i>
        */
         MINE_COUNT,

        /**
        * pointsData (FOM name: <code>PointsData</code>).
        * <br>Description from the FOM: <i>Specifies the physical location (a collection of points) of the object</i>
        */
         POINTS_DATA,

        /**
        * percentComplete (FOM name: <code>PercentComplete</code>).
        * <br>Description from the FOM: <i>Specifies the percent completion of the object</i>
        */
         PERCENT_COMPLETE,

        /**
        * damagedAppearance (FOM name: <code>DamagedAppearance</code>).
        * <br>Description from the FOM: <i>Specifies the damaged appearance of the object</i>
        */
         DAMAGED_APPEARANCE,

        /**
        * objectPreDistributed (FOM name: <code>ObjectPreDistributed</code>).
        * <br>Description from the FOM: <i>Specifies whether or not the object was created before the start of the exercise</i>
        */
         OBJECT_PRE_DISTRIBUTED,

        /**
        * deactivated (FOM name: <code>Deactivated</code>).
        * <br>Description from the FOM: <i>Specifies whether or not the object has been deactivated (it has ceased to exist in the synthetic environment)</i>
        */
         DEACTIVATED,

        /**
        * smoking (FOM name: <code>Smoking</code>).
        * <br>Description from the FOM: <i>Specifies whether or not the object is smoking (creating a smoke plume)</i>
        */
         SMOKING,

        /**
        * flaming (FOM name: <code>Flaming</code>).
        * <br>Description from the FOM: <i>Specifies whether or not the object is aflame</i>
        */
         FLAMING,

        /**
        * objectIdentifier (FOM name: <code>ObjectIdentifier</code>).
        * <br>Description from the FOM: <i>Identifies this EnvironmentObject instance (point, linear or areal)</i>
        */
         OBJECT_IDENTIFIER,

        /**
        * referencedObjectIdentifier (FOM name: <code>ReferencedObjectIdentifier</code>).
        * <br>Description from the FOM: <i>Identifies the Synthetic Environment object instance to which this EnvironmentObject instance is associated</i>
        */
         REFERENCED_OBJECT_IDENTIFIER,

        /**
        * forceIdentifier (FOM name: <code>ForceIdentifier</code>).
        * <br>Description from the FOM: <i>Identifies the force that created or modified this EnvironmentObject instance</i>
        */
         FORCE_IDENTIFIER,

        /**
        * objectType (FOM name: <code>ObjectType</code>).
        * <br>Description from the FOM: <i>Identifies the type of this EnvironmentObject instance</i>
        */
         OBJECT_TYPE
      };

     /**
      * Gets the name of the attribute.
      *
      * @return The name of the attribute. An empty string will be returned if the attribute does not exist.
      */
      LIBAPI virtual std::wstring getName(Attribute attribute);

     /**
      * Finds the attribute specified in the parameter attributeName.
      * The found enumeration will be stored in the parameter attribute.
      *
      * @return true if the attribute was found, false otherwise.
      */
      LIBAPI virtual bool find(Attribute& attribute, std::wstring attributeName);

      LIBAPI virtual ~HlaMinefieldObjectAttributes() {}
    
      /**
      * Returns true if the <code>breachedStatus</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies the breached appearance of the minefield object</i>
      *
      * @return true if <code>breachedStatus</code> is available.
      */
      LIBAPI virtual bool hasBreachedStatus() = 0;

      /**
      * Gets the value of the <code>breachedStatus</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies the breached appearance of the minefield object</i>
      * <br>Description of the data type from the FOM: <i>Breached appearance</i>
      *
      * @return the <code>breachedStatus</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::BreachedStatusEnum::BreachedStatusEnum getBreachedStatus()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>breachedStatus</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies the breached appearance of the minefield object</i>
      * <br>Description of the data type from the FOM: <i>Breached appearance</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>breachedStatus</code> attribute.
      */
      LIBAPI virtual DevStudio::BreachedStatusEnum::BreachedStatusEnum getBreachedStatus(DevStudio::BreachedStatusEnum::BreachedStatusEnum defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>breachedStatus</code> attribute.
      * <br>Description from the FOM: <i>Specifies the breached appearance of the minefield object</i>
      * <br>Description of the data type from the FOM: <i>Breached appearance</i>
      *
      * @return the time stamped <code>breachedStatus</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::BreachedStatusEnum::BreachedStatusEnum > getBreachedStatusTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>mineCount</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies the number of mines in the minefield</i>
      *
      * @return true if <code>mineCount</code> is available.
      */
      LIBAPI virtual bool hasMineCount() = 0;

      /**
      * Gets the value of the <code>mineCount</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies the number of mines in the minefield</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the <code>mineCount</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual unsigned int getMineCount()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>mineCount</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies the number of mines in the minefield</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>mineCount</code> attribute.
      */
      LIBAPI virtual unsigned int getMineCount(unsigned int defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>mineCount</code> attribute.
      * <br>Description from the FOM: <i>Specifies the number of mines in the minefield</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the time stamped <code>mineCount</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< unsigned int > getMineCountTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>pointsData</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies the physical location (a collection of points) of the object</i>
      *
      * @return true if <code>pointsData</code> is available.
      */
      LIBAPI virtual bool hasPointsData() = 0;

      /**
      * Gets the value of the <code>pointsData</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies the physical location (a collection of points) of the object</i>
      * <br>Description of the data type from the FOM: <i>Dynamic array of WorldLocationStruct elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
      *
      * @return the <code>pointsData</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::vector<DevStudio::WorldLocationStruct > getPointsData()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>pointsData</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies the physical location (a collection of points) of the object</i>
      * <br>Description of the data type from the FOM: <i>Dynamic array of WorldLocationStruct elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>pointsData</code> attribute.
      */
      LIBAPI virtual std::vector<DevStudio::WorldLocationStruct > getPointsData(std::vector<DevStudio::WorldLocationStruct > defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>pointsData</code> attribute.
      * <br>Description from the FOM: <i>Specifies the physical location (a collection of points) of the object</i>
      * <br>Description of the data type from the FOM: <i>Dynamic array of WorldLocationStruct elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
      *
      * @return the time stamped <code>pointsData</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::vector<DevStudio::WorldLocationStruct > > getPointsDataTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>percentComplete</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies the percent completion of the object</i>
      *
      * @return true if <code>percentComplete</code> is available.
      */
      LIBAPI virtual bool hasPercentComplete() = 0;

      /**
      * Gets the value of the <code>percentComplete</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies the percent completion of the object</i>
      * <br>Description of the data type from the FOM: <i>Percentage [unit: percent (%), resolution: 1, accuracy: perfect]</i>
      *
      * @return the <code>percentComplete</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual unsigned int getPercentComplete()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>percentComplete</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies the percent completion of the object</i>
      * <br>Description of the data type from the FOM: <i>Percentage [unit: percent (%), resolution: 1, accuracy: perfect]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>percentComplete</code> attribute.
      */
      LIBAPI virtual unsigned int getPercentComplete(unsigned int defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>percentComplete</code> attribute.
      * <br>Description from the FOM: <i>Specifies the percent completion of the object</i>
      * <br>Description of the data type from the FOM: <i>Percentage [unit: percent (%), resolution: 1, accuracy: perfect]</i>
      *
      * @return the time stamped <code>percentComplete</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< unsigned int > getPercentCompleteTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>damagedAppearance</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies the damaged appearance of the object</i>
      *
      * @return true if <code>damagedAppearance</code> is available.
      */
      LIBAPI virtual bool hasDamagedAppearance() = 0;

      /**
      * Gets the value of the <code>damagedAppearance</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies the damaged appearance of the object</i>
      * <br>Description of the data type from the FOM: <i>Damaged appearance</i>
      *
      * @return the <code>damagedAppearance</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::DamageStatusEnum::DamageStatusEnum getDamagedAppearance()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>damagedAppearance</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies the damaged appearance of the object</i>
      * <br>Description of the data type from the FOM: <i>Damaged appearance</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>damagedAppearance</code> attribute.
      */
      LIBAPI virtual DevStudio::DamageStatusEnum::DamageStatusEnum getDamagedAppearance(DevStudio::DamageStatusEnum::DamageStatusEnum defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>damagedAppearance</code> attribute.
      * <br>Description from the FOM: <i>Specifies the damaged appearance of the object</i>
      * <br>Description of the data type from the FOM: <i>Damaged appearance</i>
      *
      * @return the time stamped <code>damagedAppearance</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::DamageStatusEnum::DamageStatusEnum > getDamagedAppearanceTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>objectPreDistributed</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies whether or not the object was created before the start of the exercise</i>
      *
      * @return true if <code>objectPreDistributed</code> is available.
      */
      LIBAPI virtual bool hasObjectPreDistributed() = 0;

      /**
      * Gets the value of the <code>objectPreDistributed</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies whether or not the object was created before the start of the exercise</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>objectPreDistributed</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual bool getObjectPreDistributed()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>objectPreDistributed</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies whether or not the object was created before the start of the exercise</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>objectPreDistributed</code> attribute.
      */
      LIBAPI virtual bool getObjectPreDistributed(bool defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>objectPreDistributed</code> attribute.
      * <br>Description from the FOM: <i>Specifies whether or not the object was created before the start of the exercise</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>objectPreDistributed</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< bool > getObjectPreDistributedTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>deactivated</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies whether or not the object has been deactivated (it has ceased to exist in the synthetic environment)</i>
      *
      * @return true if <code>deactivated</code> is available.
      */
      LIBAPI virtual bool hasDeactivated() = 0;

      /**
      * Gets the value of the <code>deactivated</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies whether or not the object has been deactivated (it has ceased to exist in the synthetic environment)</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>deactivated</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual bool getDeactivated()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>deactivated</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies whether or not the object has been deactivated (it has ceased to exist in the synthetic environment)</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>deactivated</code> attribute.
      */
      LIBAPI virtual bool getDeactivated(bool defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>deactivated</code> attribute.
      * <br>Description from the FOM: <i>Specifies whether or not the object has been deactivated (it has ceased to exist in the synthetic environment)</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>deactivated</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< bool > getDeactivatedTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>smoking</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies whether or not the object is smoking (creating a smoke plume)</i>
      *
      * @return true if <code>smoking</code> is available.
      */
      LIBAPI virtual bool hasSmoking() = 0;

      /**
      * Gets the value of the <code>smoking</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies whether or not the object is smoking (creating a smoke plume)</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>smoking</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual bool getSmoking()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>smoking</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies whether or not the object is smoking (creating a smoke plume)</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>smoking</code> attribute.
      */
      LIBAPI virtual bool getSmoking(bool defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>smoking</code> attribute.
      * <br>Description from the FOM: <i>Specifies whether or not the object is smoking (creating a smoke plume)</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>smoking</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< bool > getSmokingTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>flaming</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies whether or not the object is aflame</i>
      *
      * @return true if <code>flaming</code> is available.
      */
      LIBAPI virtual bool hasFlaming() = 0;

      /**
      * Gets the value of the <code>flaming</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies whether or not the object is aflame</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>flaming</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual bool getFlaming()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>flaming</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies whether or not the object is aflame</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>flaming</code> attribute.
      */
      LIBAPI virtual bool getFlaming(bool defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>flaming</code> attribute.
      * <br>Description from the FOM: <i>Specifies whether or not the object is aflame</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>flaming</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< bool > getFlamingTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>objectIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Identifies this EnvironmentObject instance (point, linear or areal)</i>
      *
      * @return true if <code>objectIdentifier</code> is available.
      */
      LIBAPI virtual bool hasObjectIdentifier() = 0;

      /**
      * Gets the value of the <code>objectIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>Identifies this EnvironmentObject instance (point, linear or areal)</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @return the <code>objectIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::EntityIdentifierStruct getObjectIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>objectIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Identifies this EnvironmentObject instance (point, linear or areal)</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>objectIdentifier</code> attribute.
      */
      LIBAPI virtual DevStudio::EntityIdentifierStruct getObjectIdentifier(DevStudio::EntityIdentifierStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>objectIdentifier</code> attribute.
      * <br>Description from the FOM: <i>Identifies this EnvironmentObject instance (point, linear or areal)</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @return the time stamped <code>objectIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::EntityIdentifierStruct > getObjectIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>referencedObjectIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Identifies the Synthetic Environment object instance to which this EnvironmentObject instance is associated</i>
      *
      * @return true if <code>referencedObjectIdentifier</code> is available.
      */
      LIBAPI virtual bool hasReferencedObjectIdentifier() = 0;

      /**
      * Gets the value of the <code>referencedObjectIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>Identifies the Synthetic Environment object instance to which this EnvironmentObject instance is associated</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @return the <code>referencedObjectIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::string getReferencedObjectIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>referencedObjectIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Identifies the Synthetic Environment object instance to which this EnvironmentObject instance is associated</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>referencedObjectIdentifier</code> attribute.
      */
      LIBAPI virtual std::string getReferencedObjectIdentifier(std::string defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>referencedObjectIdentifier</code> attribute.
      * <br>Description from the FOM: <i>Identifies the Synthetic Environment object instance to which this EnvironmentObject instance is associated</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @return the time stamped <code>referencedObjectIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::string > getReferencedObjectIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>forceIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Identifies the force that created or modified this EnvironmentObject instance</i>
      *
      * @return true if <code>forceIdentifier</code> is available.
      */
      LIBAPI virtual bool hasForceIdentifier() = 0;

      /**
      * Gets the value of the <code>forceIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>Identifies the force that created or modified this EnvironmentObject instance</i>
      * <br>Description of the data type from the FOM: <i>Force ID</i>
      *
      * @return the <code>forceIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::ForceIdentifierEnum::ForceIdentifierEnum getForceIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>forceIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Identifies the force that created or modified this EnvironmentObject instance</i>
      * <br>Description of the data type from the FOM: <i>Force ID</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>forceIdentifier</code> attribute.
      */
      LIBAPI virtual DevStudio::ForceIdentifierEnum::ForceIdentifierEnum getForceIdentifier(DevStudio::ForceIdentifierEnum::ForceIdentifierEnum defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>forceIdentifier</code> attribute.
      * <br>Description from the FOM: <i>Identifies the force that created or modified this EnvironmentObject instance</i>
      * <br>Description of the data type from the FOM: <i>Force ID</i>
      *
      * @return the time stamped <code>forceIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::ForceIdentifierEnum::ForceIdentifierEnum > getForceIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>objectType</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Identifies the type of this EnvironmentObject instance</i>
      *
      * @return true if <code>objectType</code> is available.
      */
      LIBAPI virtual bool hasObjectType() = 0;

      /**
      * Gets the value of the <code>objectType</code> attribute.
      *
      * <br>Description from the FOM: <i>Identifies the type of this EnvironmentObject instance</i>
      * <br>Description of the data type from the FOM: <i>Record specifying the domain, the kind and the specific identification of the environment object</i>
      *
      * @return the <code>objectType</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::EnvironmentObjectTypeStruct getObjectType()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>objectType</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Identifies the type of this EnvironmentObject instance</i>
      * <br>Description of the data type from the FOM: <i>Record specifying the domain, the kind and the specific identification of the environment object</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>objectType</code> attribute.
      */
      LIBAPI virtual DevStudio::EnvironmentObjectTypeStruct getObjectType(DevStudio::EnvironmentObjectTypeStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>objectType</code> attribute.
      * <br>Description from the FOM: <i>Identifies the type of this EnvironmentObject instance</i>
      * <br>Description of the data type from the FOM: <i>Record specifying the domain, the kind and the specific identification of the environment object</i>
      *
      * @return the time stamped <code>objectType</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::EnvironmentObjectTypeStruct > getObjectTypeTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
   };
}
#endif
