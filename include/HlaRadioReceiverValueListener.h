/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLARADIORECEIVERVALUELISTENER_H
#define DEVELOPER_STUDIO_HLARADIORECEIVERVALUELISTENER_H

#include <memory>

#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/ReceiverOperationalStatusEnum.h>
#include <DevStudio/datatypes/RelativePositionStruct.h>
#include <string>

#include <DevStudio/HlaLogicalTime.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaRadioReceiverAttributes.h>    

namespace DevStudio {

   /**
   * Listener for updates of attributes, with the new updated values.  
   */
   class HlaRadioReceiverValueListener {

   public:

      LIBAPI virtual ~HlaRadioReceiverValueListener() {}
    
      /**
      * This method is called when the attribute <code>radioIndex</code> is updated.
      * <br>Description from the FOM: <i>A number that uniquely identifies this radio receiver from other receivers on the host object.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param radioReceiver The object which is updated.
      * @param radioIndex The new value of the attribute in this update
      * @param validOldRadioIndex True if oldRadioIndex contains a valid value
      * @param oldRadioIndex The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void radioIndexUpdated(HlaRadioReceiverPtr radioReceiver, unsigned short radioIndex, bool validOldRadioIndex, unsigned short oldRadioIndex, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>receivedPower</code> is updated.
      * <br>Description from the FOM: <i>The radio frequency power received, after applying any propagation loss and antenna gain.</i>
      * <br>Description of the data type from the FOM: <i>Power ratio in decibels (dB) of a measured power referenced to 1 milliwatt (mW). [unit: decibel milliwatt (dBm), resolution: NA, accuracy: perfect]</i>
      *
      * @param radioReceiver The object which is updated.
      * @param receivedPower The new value of the attribute in this update
      * @param validOldReceivedPower True if oldReceivedPower contains a valid value
      * @param oldReceivedPower The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void receivedPowerUpdated(HlaRadioReceiverPtr radioReceiver, float receivedPower, bool validOldReceivedPower, float oldReceivedPower, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>receivedTransmitterIdentifier</code> is updated.
      * <br>Description from the FOM: <i>The object instance ID of the transmitter that generated the received radio signal.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @param radioReceiver The object which is updated.
      * @param receivedTransmitterIdentifier The new value of the attribute in this update
      * @param validOldReceivedTransmitterIdentifier True if oldReceivedTransmitterIdentifier contains a valid value
      * @param oldReceivedTransmitterIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void receivedTransmitterIdentifierUpdated(HlaRadioReceiverPtr radioReceiver, std::string receivedTransmitterIdentifier, bool validOldReceivedTransmitterIdentifier, std::string oldReceivedTransmitterIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>receiverOperationalStatus</code> is updated.
      * <br>Description from the FOM: <i>The operational state of the radio receiver.</i>
      * <br>Description of the data type from the FOM: <i>The operational state of a radio receiver.</i>
      *
      * @param radioReceiver The object which is updated.
      * @param receiverOperationalStatus The new value of the attribute in this update
      * @param validOldReceiverOperationalStatus True if oldReceiverOperationalStatus contains a valid value
      * @param oldReceiverOperationalStatus The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void receiverOperationalStatusUpdated(HlaRadioReceiverPtr radioReceiver, ReceiverOperationalStatusEnum::ReceiverOperationalStatusEnum receiverOperationalStatus, bool validOldReceiverOperationalStatus, ReceiverOperationalStatusEnum::ReceiverOperationalStatusEnum oldReceiverOperationalStatus, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>entityIdentifier</code> is updated.
      * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @param radioReceiver The object which is updated.
      * @param entityIdentifier The new value of the attribute in this update
      * @param validOldEntityIdentifier True if oldEntityIdentifier contains a valid value
      * @param oldEntityIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void entityIdentifierUpdated(HlaRadioReceiverPtr radioReceiver, EntityIdentifierStruct entityIdentifier, bool validOldEntityIdentifier, EntityIdentifierStruct oldEntityIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>hostObjectIdentifier</code> is updated.
      * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @param radioReceiver The object which is updated.
      * @param hostObjectIdentifier The new value of the attribute in this update
      * @param validOldHostObjectIdentifier True if oldHostObjectIdentifier contains a valid value
      * @param oldHostObjectIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void hostObjectIdentifierUpdated(HlaRadioReceiverPtr radioReceiver, std::string hostObjectIdentifier, bool validOldHostObjectIdentifier, std::string oldHostObjectIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>relativePosition</code> is updated.
      * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
      * <br>Description of the data type from the FOM: <i>Relative position in right-handed Cartesian coordinates.</i>
      *
      * @param radioReceiver The object which is updated.
      * @param relativePosition The new value of the attribute in this update
      * @param validOldRelativePosition True if oldRelativePosition contains a valid value
      * @param oldRelativePosition The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void relativePositionUpdated(HlaRadioReceiverPtr radioReceiver, RelativePositionStruct relativePosition, bool validOldRelativePosition, RelativePositionStruct oldRelativePosition, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
      /**
      * This method is called when the producing federate of an attribute is changed.
      * Only available when using HLA Evolved.
      *
      * @param radioReceiver The object which is updated.
      * @param attribute The attribute that now has a new producing federate
      * @param oldProducingFederate The federate handle of the old producing federate
      * @param newProducingFederate The federate handle of the new producing federate
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void producingFederateUpdated(HlaRadioReceiverPtr radioReceiver, HlaRadioReceiverAttributes::Attribute attribute, HlaFederateIdPtr oldProducingFederate, HlaFederateIdPtr newProducingFederate, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;

      class Adapter;
   };

   /**
   * An adapter class that implements the HlaRadioReceiverValueListener interface with empty methods.
   * It might be used as a base class for a listener.
   */
   class HlaRadioReceiverValueListener::Adapter : public HlaRadioReceiverValueListener {

   public:

      LIBAPI virtual void radioIndexUpdated(HlaRadioReceiverPtr radioReceiver, unsigned short radioIndex, bool validOldRadioIndex, unsigned short oldRadioIndex, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void receivedPowerUpdated(HlaRadioReceiverPtr radioReceiver, float receivedPower, bool validOldReceivedPower, float oldReceivedPower, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void receivedTransmitterIdentifierUpdated(HlaRadioReceiverPtr radioReceiver, std::string receivedTransmitterIdentifier, bool validOldReceivedTransmitterIdentifier, std::string oldReceivedTransmitterIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void receiverOperationalStatusUpdated(HlaRadioReceiverPtr radioReceiver, ReceiverOperationalStatusEnum::ReceiverOperationalStatusEnum receiverOperationalStatus, bool validOldReceiverOperationalStatus, ReceiverOperationalStatusEnum::ReceiverOperationalStatusEnum oldReceiverOperationalStatus, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void entityIdentifierUpdated(HlaRadioReceiverPtr radioReceiver, EntityIdentifierStruct entityIdentifier, bool validOldEntityIdentifier, EntityIdentifierStruct oldEntityIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void hostObjectIdentifierUpdated(HlaRadioReceiverPtr radioReceiver, std::string hostObjectIdentifier, bool validOldHostObjectIdentifier, std::string oldHostObjectIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void relativePositionUpdated(HlaRadioReceiverPtr radioReceiver, RelativePositionStruct relativePosition, bool validOldRelativePosition, RelativePositionStruct oldRelativePosition, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
      LIBAPI virtual void producingFederateUpdated(HlaRadioReceiverPtr radioReceiver, HlaRadioReceiverAttributes::Attribute attribute, HlaFederateIdPtr oldProducingFederate, HlaFederateIdPtr newProducingFederate, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
   };

}
#endif
