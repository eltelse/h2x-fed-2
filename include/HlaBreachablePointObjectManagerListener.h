/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLABREACHABLEPOINTOBJECTMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLABREACHABLEPOINTOBJECTMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaBreachablePointObject.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaBreachablePointObject instances.
    */
    class HlaBreachablePointObjectManagerListener {

    public:

        LIBAPI virtual ~HlaBreachablePointObjectManagerListener() {}

        /**
        * This method is called when a new HlaBreachablePointObject instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param breachablePointObject the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaBreachablePointObjectDiscovered(HlaBreachablePointObjectPtr breachablePointObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaBreachablePointObject instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param breachablePointObject the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaBreachablePointObjectInitialized(HlaBreachablePointObjectPtr breachablePointObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaBreachablePointObjectManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param breachablePointObject the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaBreachablePointObjectInInterest(HlaBreachablePointObjectPtr breachablePointObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaBreachablePointObjectManagerListener instance goes out of interest.
        *
        * @param breachablePointObject the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaBreachablePointObjectOutOfInterest(HlaBreachablePointObjectPtr breachablePointObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaBreachablePointObject instance is deleted.
        *
        * @param breachablePointObject the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaBreachablePointObjectDeleted(HlaBreachablePointObjectPtr breachablePointObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaBreachablePointObjectManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaBreachablePointObjectManagerListener::Adapter : public HlaBreachablePointObjectManagerListener {

    public:
        LIBAPI virtual void hlaBreachablePointObjectDiscovered(HlaBreachablePointObjectPtr breachablePointObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaBreachablePointObjectInitialized(HlaBreachablePointObjectPtr breachablePointObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaBreachablePointObjectInInterest(HlaBreachablePointObjectPtr breachablePointObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaBreachablePointObjectOutOfInterest(HlaBreachablePointObjectPtr breachablePointObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaBreachablePointObjectDeleted(HlaBreachablePointObjectPtr breachablePointObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
