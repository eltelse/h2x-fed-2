/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLASPOTOBJECTATTRIBUTES_H
#define DEVELOPER_STUDIO_HLASPOTOBJECTATTRIBUTES_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <string>
#include <vector>
#include <utility>
    
#include <boost/noncopyable.hpp>
    
#include <DevStudio/datatypes/AcquisitionTypeEnum.h>
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EntityTypeStruct.h>
#include <DevStudio/datatypes/FortificationsEnum.h>
#include <DevStudio/datatypes/GeographicShapeStruct.h>
#include <DevStudio/datatypes/HostilityTypeEnum.h>
#include <DevStudio/datatypes/IsPartOfStruct.h>
#include <DevStudio/datatypes/PositionStruct.h>
#include <DevStudio/datatypes/SpatialVariantStruct.h>
#include <DevStudio/datatypes/SpotCategoryEnum.h>
#include <string>

#include <DevStudio/HlaException.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaBaseEntityAttributes.h>
#include <DevStudio/HlaTimeStamped.h>

namespace DevStudio {

   /**
   * Interface used to represent all attributes for an object instance.
   */
   class HlaSpotObjectAttributes : public HlaBaseEntityAttributes {
   public:


     /**
      * An enumeration of the attributes of an HlaSpotObject
      *
      *<table summary="All attributes">
      * <tr><td><b>Enum constant</b></td><td><b>C++ name</b></td><td><b>FOM name</b></td></tr>
      * <tr><td>QUANTITY</td><td>quantity</td><td><code>Quantity</code></td></tr>
      * <tr><td>REPORTING_ENTITY</td><td>reportingEntity</td><td><code>ReportingEntity</code></td></tr>
      * <tr><td>C2REPORTING_ENTITY</td><td>c2ReportingEntity</td><td><code>C2ReportingEntity</code></td></tr>
      * <tr><td>DETECTION_MEANS</td><td>detectionMeans</td><td><code>DetectionMeans</code></td></tr>
      * <tr><td>HOSTILITY_TYPE</td><td>hostilityType</td><td><code>HostilityType</code></td></tr>
      * <tr><td>DESCRIPTION</td><td>description</td><td><code>Description</code></td></tr>
      * <tr><td>LAST_UPDATE_TIME</td><td>lastUpdateTime</td><td><code>LastUpdateTime</code></td></tr>
      * <tr><td>GEOGRAPHIC_SHAPE</td><td>geographicShape</td><td><code>GeographicShape</code></td></tr>
      * <tr><td>SPOTTED_ENTITY_IDENTIFIER</td><td>spottedEntityIdentifier</td><td><code>SpottedEntityIdentifier</code></td></tr>
      * <tr><td>POSITION</td><td>position</td><td><code>Position</code></td></tr>
      * <tr><td>WIDTH_ACCURACY</td><td>widthAccuracy</td><td><code>WidthAccuracy</code></td></tr>
      * <tr><td>LENGTH_ACCURACY</td><td>lengthAccuracy</td><td><code>LengthAccuracy</code></td></tr>
      * <tr><td>ALTITUDE_ACCURACY</td><td>altitudeAccuracy</td><td><code>AltitudeAccuracy</code></td></tr>
      * <tr><td>SPOT_TYPE</td><td>spotType</td><td><code>SpotType</code></td></tr>
      * <tr><td>WEAPON_TYPE</td><td>weaponType</td><td><code>WeaponType</code></td></tr>
      * <tr><td>SPOT_STATUS</td><td>spotStatus</td><td><code>SpotStatus</code></td></tr>
      * <tr><td>HOSTILE</td><td>hostile</td><td><code>Hostile</code></td></tr>
      * <tr><td>INFRASTRUCTURE</td><td>infrastructure</td><td><code>Infrastructure</code></td></tr>
      * <tr><td>FORTIFICATIONS</td><td>fortifications</td><td><code>Fortifications</code></td></tr>
      * <tr><td>FLOORS</td><td>floors</td><td><code>Floors</code></td></tr>
      * <tr><td>TOTAL_FLOORS</td><td>totalFloors</td><td><code>TotalFloors</code></td></tr>
      * <tr><td>ACQUISITION_TYPE</td><td>acquisitionType</td><td><code>AcquisitionType</code></td></tr>
      * <tr><td>REPORTING_ENTITY_NAME</td><td>reportingEntityName</td><td><code>ReportingEntityName</code></td></tr>
      * <tr><td>ARMY_ORG</td><td>armyOrg</td><td><code>ArmyOrg</code></td></tr>
      * <tr><td>SPOT_CATEGORY</td><td>spotCategory</td><td><code>SpotCategory</code></td></tr>
      * <tr><td>CASUALTIES_NUMBER</td><td>casualtiesNumber</td><td><code>CasualtiesNumber</code></td></tr>
      * <tr><td>SPOTTED_ENTITY_ACTIVITY_STATUS</td><td>spottedEntityActivityStatus</td><td><code>SpottedEntityActivityStatus</code></td></tr>
      * <tr><td>ENTITY_TYPE</td><td>entityType</td><td><code>EntityType</code></td></tr>
      * <tr><td>ENTITY_IDENTIFIER</td><td>entityIdentifier</td><td><code>EntityIdentifier</code></td></tr>
      * <tr><td>IS_PART_OF</td><td>isPartOf</td><td><code>IsPartOf</code></td></tr>
      * <tr><td>SPATIAL</td><td>spatial</td><td><code>Spatial</code></td></tr>
      * <tr><td>RELATIVE_SPATIAL</td><td>relativeSpatial</td><td><code>RelativeSpatial</code></td></tr>
      *</table>
      */
      enum Attribute {
        /**
        * quantity (FOM name: <code>Quantity</code>).
        * <br>Description from the FOM: <i>Quantity of spotted targets.</i>
        */
         QUANTITY,

        /**
        * reportingEntity (FOM name: <code>ReportingEntity</code>).
        * <br>Description from the FOM: <i>This field is holding a unique identifier for the reporting entity.</i>
        */
         REPORTING_ENTITY,

        /**
        * c2ReportingEntity (FOM name: <code>C2ReportingEntity</code>).
        * <br>Description from the FOM: <i>This field is holding a unique c2 identifier for the reporting entity.</i>
        */
         C2REPORTING_ENTITY,

        /**
        * detectionMeans (FOM name: <code>DetectionMeans</code>).
        * <br>Description from the FOM: <i>Which kind of detection device spotted the target.</i>
        */
         DETECTION_MEANS,

        /**
        * hostilityType (FOM name: <code>HostilityType</code>).
        * <br>Description from the FOM: <i>Hostility type of spotted target.</i>
        */
         HOSTILITY_TYPE,

        /**
        * description (FOM name: <code>Description</code>).
        * <br>Description from the FOM: <i>The description of spotted event.</i>
        */
         DESCRIPTION,

        /**
        * lastUpdateTime (FOM name: <code>LastUpdateTime</code>).
        * <br>Description from the FOM: <i>The last time the spot object was updated.</i>
        */
         LAST_UPDATE_TIME,

        /**
        * geographicShape (FOM name: <code>GeographicShape</code>).
        * <br>Description from the FOM: <i>Representation of 2D Force geographic shape.</i>
        */
         GEOGRAPHIC_SHAPE,

        /**
        * spottedEntityIdentifier (FOM name: <code>SpottedEntityIdentifier</code>).
        * <br>Description from the FOM: <i>Entity identifier of spotted entity.</i>
        */
         SPOTTED_ENTITY_IDENTIFIER,

        /**
        * position (FOM name: <code>Position</code>).
        * <br>Description from the FOM: <i>Position of the spot object.</i>
        */
         POSITION,

        /**
        * widthAccuracy (FOM name: <code>WidthAccuracy</code>).
        * <br>Description from the FOM: <i>Width accuracy of the spot object. Will be filled using meters between 0-999.</i>
        */
         WIDTH_ACCURACY,

        /**
        * lengthAccuracy (FOM name: <code>LengthAccuracy</code>).
        * <br>Description from the FOM: <i>Length accuracy of the spot object. Will be filled using meters between 0-999.</i>
        */
         LENGTH_ACCURACY,

        /**
        * altitudeAccuracy (FOM name: <code>AltitudeAccuracy</code>).
        * <br>Description from the FOM: <i>Altitude accuracy of the spot object. Will be filled using meters between 0-100.</i>
        */
         ALTITUDE_ACCURACY,

        /**
        * spotType (FOM name: <code>SpotType</code>).
        * <br>Description from the FOM: <i>Spot object Type. Represented as an integer which its value is directing to an external enumeration.</i>
        */
         SPOT_TYPE,

        /**
        * weaponType (FOM name: <code>WeaponType</code>).
        * <br>Description from the FOM: <i>Defines main weapon type</i>
        */
         WEAPON_TYPE,

        /**
        * spotStatus (FOM name: <code>SpotStatus</code>).
        * <br>Description from the FOM: <i>Spot object status (e.g: Alive, Destroyed and etc.). Represented as an integer which its value is directing to an external enumeration.</i>
        */
         SPOT_STATUS,

        /**
        * hostile (FOM name: <code>Hostile</code>).
        * <br>Description from the FOM: <i>Hostile (yes/no)</i>
        */
         HOSTILE,

        /**
        * infrastructure (FOM name: <code>Infrastructure</code>).
        * <br>Description from the FOM: <i>Spot Infrastructure. Represented as an integer which its value is directing to an external enumeration.</i>
        */
         INFRASTRUCTURE,

        /**
        * fortifications (FOM name: <code>Fortifications</code>).
        * <br>Description from the FOM: <i>Defines fortification type .</i>
        */
         FORTIFICATIONS,

        /**
        * floors (FOM name: <code>Floors</code>).
        * <br>Description from the FOM: <i>Floor Number. (Max 8 characters).</i>
        */
         FLOORS,

        /**
        * totalFloors (FOM name: <code>TotalFloors</code>).
        * <br>Description from the FOM: <i>Number of total floors. (Max 8 characters).</i>
        */
         TOTAL_FLOORS,

        /**
        * acquisitionType (FOM name: <code>AcquisitionType</code>).
        * <br>Description from the FOM: <i>Spot object acquisition Type.</i>
        */
         ACQUISITION_TYPE,

        /**
        * reportingEntityName (FOM name: <code>ReportingEntityName</code>).
        * <br>Description from the FOM: <i>The name of the reporting unit/entity. (Max 17 characters).</i>
        */
         REPORTING_ENTITY_NAME,

        /**
        * armyOrg (FOM name: <code>ArmyOrg</code>).
        * <br>Description from the FOM: <i>Defines of which army organization the spotted entity belongs. Represented as an integer which its value is directing to an external enumeration.</i>
        */
         ARMY_ORG,

        /**
        * spotCategory (FOM name: <code>SpotCategory</code>).
        * <br>Description from the FOM: <i>Spot object category.</i>
        */
         SPOT_CATEGORY,

        /**
        * casualtiesNumber (FOM name: <code>CasualtiesNumber</code>).
        * <br>Description from the FOM: <i>Number of casualties in the spotted entity.</i>
        */
         CASUALTIES_NUMBER,

        /**
        * spottedEntityActivityStatus (FOM name: <code>SpottedEntityActivityStatus</code>).
        * <br>Description from the FOM: <i>Spotted entity activity status. Represented as an integer which its value is directing to an external enumeration.</i>
        */
         SPOTTED_ENTITY_ACTIVITY_STATUS,

        /**
        * entityType (FOM name: <code>EntityType</code>).
        * <br>Description from the FOM: <i>The category of the entity.</i>
        */
         ENTITY_TYPE,

        /**
        * entityIdentifier (FOM name: <code>EntityIdentifier</code>).
        * <br>Description from the FOM: <i>The unique identifier for the entity instance.</i>
        */
         ENTITY_IDENTIFIER,

        /**
        * isPartOf (FOM name: <code>IsPartOf</code>).
        * <br>Description from the FOM: <i>Defines if the entity if a constituent part of another entity (denoted the host entity). If the entity is a constituent part of another entity then the HostEntityIdentifier shall be set to the EntityIdentifier of the host entity and the HostRTIObjectIdentifier shall be set to the RTI object instance ID of the host entity. If the entity is not a constituent part of another entity then the HostEntityIdentifier shall be set to 0.0.0 and the HostRTIObjectIdentifier shall be set to the empty string.</i>
        */
         IS_PART_OF,

        /**
        * spatial (FOM name: <code>Spatial</code>).
        * <br>Description from the FOM: <i>Spatial state stored in one variant record attribute.</i>
        */
         SPATIAL,

        /**
        * relativeSpatial (FOM name: <code>RelativeSpatial</code>).
        * <br>Description from the FOM: <i>Relative spatial state stored in one variant record attribute.</i>
        */
         RELATIVE_SPATIAL
      };

     /**
      * Gets the name of the attribute.
      *
      * @return The name of the attribute. An empty string will be returned if the attribute does not exist.
      */
      LIBAPI virtual std::wstring getName(Attribute attribute);

     /**
      * Finds the attribute specified in the parameter attributeName.
      * The found enumeration will be stored in the parameter attribute.
      *
      * @return true if the attribute was found, false otherwise.
      */
      LIBAPI virtual bool find(Attribute& attribute, std::wstring attributeName);

      LIBAPI virtual ~HlaSpotObjectAttributes() {}
    
      /**
      * Returns true if the <code>quantity</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Quantity of spotted targets.</i>
      *
      * @return true if <code>quantity</code> is available.
      */
      LIBAPI virtual bool hasQuantity() = 0;

      /**
      * Gets the value of the <code>quantity</code> attribute.
      *
      * <br>Description from the FOM: <i>Quantity of spotted targets.</i>
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      *
      * @return the <code>quantity</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual short getQuantity()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>quantity</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Quantity of spotted targets.</i>
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>quantity</code> attribute.
      */
      LIBAPI virtual short getQuantity(short defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>quantity</code> attribute.
      * <br>Description from the FOM: <i>Quantity of spotted targets.</i>
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      *
      * @return the time stamped <code>quantity</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< short > getQuantityTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>reportingEntity</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>This field is holding a unique identifier for the reporting entity.</i>
      *
      * @return true if <code>reportingEntity</code> is available.
      */
      LIBAPI virtual bool hasReportingEntity() = 0;

      /**
      * Gets the value of the <code>reportingEntity</code> attribute.
      *
      * <br>Description from the FOM: <i>This field is holding a unique identifier for the reporting entity.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @return the <code>reportingEntity</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::string getReportingEntity()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>reportingEntity</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>This field is holding a unique identifier for the reporting entity.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>reportingEntity</code> attribute.
      */
      LIBAPI virtual std::string getReportingEntity(std::string defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>reportingEntity</code> attribute.
      * <br>Description from the FOM: <i>This field is holding a unique identifier for the reporting entity.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @return the time stamped <code>reportingEntity</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::string > getReportingEntityTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>c2ReportingEntity</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>This field is holding a unique c2 identifier for the reporting entity.</i>
      *
      * @return true if <code>c2ReportingEntity</code> is available.
      */
      LIBAPI virtual bool hasC2ReportingEntity() = 0;

      /**
      * Gets the value of the <code>c2ReportingEntity</code> attribute.
      *
      * <br>Description from the FOM: <i>This field is holding a unique c2 identifier for the reporting entity.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @return the <code>c2ReportingEntity</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::string getC2ReportingEntity()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>c2ReportingEntity</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>This field is holding a unique c2 identifier for the reporting entity.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>c2ReportingEntity</code> attribute.
      */
      LIBAPI virtual std::string getC2ReportingEntity(std::string defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>c2ReportingEntity</code> attribute.
      * <br>Description from the FOM: <i>This field is holding a unique c2 identifier for the reporting entity.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @return the time stamped <code>c2ReportingEntity</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::string > getC2ReportingEntityTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>detectionMeans</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Which kind of detection device spotted the target.</i>
      *
      * @return true if <code>detectionMeans</code> is available.
      */
      LIBAPI virtual bool hasDetectionMeans() = 0;

      /**
      * Gets the value of the <code>detectionMeans</code> attribute.
      *
      * <br>Description from the FOM: <i>Which kind of detection device spotted the target.</i>
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      *
      * @return the <code>detectionMeans</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::EntityTypeStruct getDetectionMeans()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>detectionMeans</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Which kind of detection device spotted the target.</i>
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>detectionMeans</code> attribute.
      */
      LIBAPI virtual DevStudio::EntityTypeStruct getDetectionMeans(DevStudio::EntityTypeStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>detectionMeans</code> attribute.
      * <br>Description from the FOM: <i>Which kind of detection device spotted the target.</i>
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      *
      * @return the time stamped <code>detectionMeans</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::EntityTypeStruct > getDetectionMeansTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>hostilityType</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Hostility type of spotted target.</i>
      *
      * @return true if <code>hostilityType</code> is available.
      */
      LIBAPI virtual bool hasHostilityType() = 0;

      /**
      * Gets the value of the <code>hostilityType</code> attribute.
      *
      * <br>Description from the FOM: <i>Hostility type of spotted target.</i>
      * <br>Description of the data type from the FOM: <i>-NULL-</i>
      *
      * @return the <code>hostilityType</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HostilityTypeEnum::HostilityTypeEnum getHostilityType()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>hostilityType</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Hostility type of spotted target.</i>
      * <br>Description of the data type from the FOM: <i>-NULL-</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>hostilityType</code> attribute.
      */
      LIBAPI virtual DevStudio::HostilityTypeEnum::HostilityTypeEnum getHostilityType(DevStudio::HostilityTypeEnum::HostilityTypeEnum defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>hostilityType</code> attribute.
      * <br>Description from the FOM: <i>Hostility type of spotted target.</i>
      * <br>Description of the data type from the FOM: <i>-NULL-</i>
      *
      * @return the time stamped <code>hostilityType</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::HostilityTypeEnum::HostilityTypeEnum > getHostilityTypeTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>description</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The description of spotted event.</i>
      *
      * @return true if <code>description</code> is available.
      */
      LIBAPI virtual bool hasDescription() = 0;

      /**
      * Gets the value of the <code>description</code> attribute.
      *
      * <br>Description from the FOM: <i>The description of spotted event.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>description</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::wstring getDescription()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>description</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The description of spotted event.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>description</code> attribute.
      */
      LIBAPI virtual std::wstring getDescription(std::wstring defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>description</code> attribute.
      * <br>Description from the FOM: <i>The description of spotted event.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>description</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::wstring > getDescriptionTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>lastUpdateTime</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The last time the spot object was updated.</i>
      *
      * @return true if <code>lastUpdateTime</code> is available.
      */
      LIBAPI virtual bool hasLastUpdateTime() = 0;

      /**
      * Gets the value of the <code>lastUpdateTime</code> attribute.
      *
      * <br>Description from the FOM: <i>The last time the spot object was updated.</i>
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      *
      * @return the <code>lastUpdateTime</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual short getLastUpdateTime()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>lastUpdateTime</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The last time the spot object was updated.</i>
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>lastUpdateTime</code> attribute.
      */
      LIBAPI virtual short getLastUpdateTime(short defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>lastUpdateTime</code> attribute.
      * <br>Description from the FOM: <i>The last time the spot object was updated.</i>
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      *
      * @return the time stamped <code>lastUpdateTime</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< short > getLastUpdateTimeTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>geographicShape</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Representation of 2D Force geographic shape.</i>
      *
      * @return true if <code>geographicShape</code> is available.
      */
      LIBAPI virtual bool hasGeographicShape() = 0;

      /**
      * Gets the value of the <code>geographicShape</code> attribute.
      *
      * <br>Description from the FOM: <i>Representation of 2D Force geographic shape.</i>
      * <br>Description of the data type from the FOM: <i>-NULL-</i>
      *
      * @return the <code>geographicShape</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::GeographicShapeStruct getGeographicShape()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>geographicShape</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Representation of 2D Force geographic shape.</i>
      * <br>Description of the data type from the FOM: <i>-NULL-</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>geographicShape</code> attribute.
      */
      LIBAPI virtual DevStudio::GeographicShapeStruct getGeographicShape(DevStudio::GeographicShapeStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>geographicShape</code> attribute.
      * <br>Description from the FOM: <i>Representation of 2D Force geographic shape.</i>
      * <br>Description of the data type from the FOM: <i>-NULL-</i>
      *
      * @return the time stamped <code>geographicShape</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::GeographicShapeStruct > getGeographicShapeTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>spottedEntityIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Entity identifier of spotted entity.</i>
      *
      * @return true if <code>spottedEntityIdentifier</code> is available.
      */
      LIBAPI virtual bool hasSpottedEntityIdentifier() = 0;

      /**
      * Gets the value of the <code>spottedEntityIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>Entity identifier of spotted entity.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @return the <code>spottedEntityIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::string getSpottedEntityIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>spottedEntityIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Entity identifier of spotted entity.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>spottedEntityIdentifier</code> attribute.
      */
      LIBAPI virtual std::string getSpottedEntityIdentifier(std::string defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>spottedEntityIdentifier</code> attribute.
      * <br>Description from the FOM: <i>Entity identifier of spotted entity.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @return the time stamped <code>spottedEntityIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::string > getSpottedEntityIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>position</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Position of the spot object.</i>
      *
      * @return true if <code>position</code> is available.
      */
      LIBAPI virtual bool hasPosition() = 0;

      /**
      * Gets the value of the <code>position</code> attribute.
      *
      * <br>Description from the FOM: <i>Position of the spot object.</i>
      * <br>Description of the data type from the FOM: <i>-NULL-</i>
      *
      * @return the <code>position</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::PositionStruct getPosition()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>position</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Position of the spot object.</i>
      * <br>Description of the data type from the FOM: <i>-NULL-</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>position</code> attribute.
      */
      LIBAPI virtual DevStudio::PositionStruct getPosition(DevStudio::PositionStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>position</code> attribute.
      * <br>Description from the FOM: <i>Position of the spot object.</i>
      * <br>Description of the data type from the FOM: <i>-NULL-</i>
      *
      * @return the time stamped <code>position</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::PositionStruct > getPositionTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>widthAccuracy</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Width accuracy of the spot object. Will be filled using meters between 0-999.</i>
      *
      * @return true if <code>widthAccuracy</code> is available.
      */
      LIBAPI virtual bool hasWidthAccuracy() = 0;

      /**
      * Gets the value of the <code>widthAccuracy</code> attribute.
      *
      * <br>Description from the FOM: <i>Width accuracy of the spot object. Will be filled using meters between 0-999.</i>
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      *
      * @return the <code>widthAccuracy</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual float getWidthAccuracy()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>widthAccuracy</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Width accuracy of the spot object. Will be filled using meters between 0-999.</i>
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>widthAccuracy</code> attribute.
      */
      LIBAPI virtual float getWidthAccuracy(float defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>widthAccuracy</code> attribute.
      * <br>Description from the FOM: <i>Width accuracy of the spot object. Will be filled using meters between 0-999.</i>
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      *
      * @return the time stamped <code>widthAccuracy</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< float > getWidthAccuracyTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>lengthAccuracy</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Length accuracy of the spot object. Will be filled using meters between 0-999.</i>
      *
      * @return true if <code>lengthAccuracy</code> is available.
      */
      LIBAPI virtual bool hasLengthAccuracy() = 0;

      /**
      * Gets the value of the <code>lengthAccuracy</code> attribute.
      *
      * <br>Description from the FOM: <i>Length accuracy of the spot object. Will be filled using meters between 0-999.</i>
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      *
      * @return the <code>lengthAccuracy</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual float getLengthAccuracy()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>lengthAccuracy</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Length accuracy of the spot object. Will be filled using meters between 0-999.</i>
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>lengthAccuracy</code> attribute.
      */
      LIBAPI virtual float getLengthAccuracy(float defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>lengthAccuracy</code> attribute.
      * <br>Description from the FOM: <i>Length accuracy of the spot object. Will be filled using meters between 0-999.</i>
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      *
      * @return the time stamped <code>lengthAccuracy</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< float > getLengthAccuracyTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>altitudeAccuracy</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Altitude accuracy of the spot object. Will be filled using meters between 0-100.</i>
      *
      * @return true if <code>altitudeAccuracy</code> is available.
      */
      LIBAPI virtual bool hasAltitudeAccuracy() = 0;

      /**
      * Gets the value of the <code>altitudeAccuracy</code> attribute.
      *
      * <br>Description from the FOM: <i>Altitude accuracy of the spot object. Will be filled using meters between 0-100.</i>
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      *
      * @return the <code>altitudeAccuracy</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual float getAltitudeAccuracy()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>altitudeAccuracy</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Altitude accuracy of the spot object. Will be filled using meters between 0-100.</i>
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>altitudeAccuracy</code> attribute.
      */
      LIBAPI virtual float getAltitudeAccuracy(float defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>altitudeAccuracy</code> attribute.
      * <br>Description from the FOM: <i>Altitude accuracy of the spot object. Will be filled using meters between 0-100.</i>
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      *
      * @return the time stamped <code>altitudeAccuracy</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< float > getAltitudeAccuracyTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>spotType</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Spot object Type. Represented as an integer which its value is directing to an external enumeration.</i>
      *
      * @return true if <code>spotType</code> is available.
      */
      LIBAPI virtual bool hasSpotType() = 0;

      /**
      * Gets the value of the <code>spotType</code> attribute.
      *
      * <br>Description from the FOM: <i>Spot object Type. Represented as an integer which its value is directing to an external enumeration.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^31, 2^31 - 1]</i>
      *
      * @return the <code>spotType</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual int getSpotType()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>spotType</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Spot object Type. Represented as an integer which its value is directing to an external enumeration.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^31, 2^31 - 1]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>spotType</code> attribute.
      */
      LIBAPI virtual int getSpotType(int defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>spotType</code> attribute.
      * <br>Description from the FOM: <i>Spot object Type. Represented as an integer which its value is directing to an external enumeration.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^31, 2^31 - 1]</i>
      *
      * @return the time stamped <code>spotType</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< int > getSpotTypeTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>weaponType</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Defines main weapon type</i>
      *
      * @return true if <code>weaponType</code> is available.
      */
      LIBAPI virtual bool hasWeaponType() = 0;

      /**
      * Gets the value of the <code>weaponType</code> attribute.
      *
      * <br>Description from the FOM: <i>Defines main weapon type</i>
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      *
      * @return the <code>weaponType</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::EntityTypeStruct getWeaponType()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>weaponType</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Defines main weapon type</i>
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>weaponType</code> attribute.
      */
      LIBAPI virtual DevStudio::EntityTypeStruct getWeaponType(DevStudio::EntityTypeStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>weaponType</code> attribute.
      * <br>Description from the FOM: <i>Defines main weapon type</i>
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      *
      * @return the time stamped <code>weaponType</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::EntityTypeStruct > getWeaponTypeTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>spotStatus</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Spot object status (e.g: Alive, Destroyed and etc.). Represented as an integer which its value is directing to an external enumeration.</i>
      *
      * @return true if <code>spotStatus</code> is available.
      */
      LIBAPI virtual bool hasSpotStatus() = 0;

      /**
      * Gets the value of the <code>spotStatus</code> attribute.
      *
      * <br>Description from the FOM: <i>Spot object status (e.g: Alive, Destroyed and etc.). Represented as an integer which its value is directing to an external enumeration.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^31, 2^31 - 1]</i>
      *
      * @return the <code>spotStatus</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual int getSpotStatus()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>spotStatus</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Spot object status (e.g: Alive, Destroyed and etc.). Represented as an integer which its value is directing to an external enumeration.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^31, 2^31 - 1]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>spotStatus</code> attribute.
      */
      LIBAPI virtual int getSpotStatus(int defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>spotStatus</code> attribute.
      * <br>Description from the FOM: <i>Spot object status (e.g: Alive, Destroyed and etc.). Represented as an integer which its value is directing to an external enumeration.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^31, 2^31 - 1]</i>
      *
      * @return the time stamped <code>spotStatus</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< int > getSpotStatusTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>hostile</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Hostile (yes/no)</i>
      *
      * @return true if <code>hostile</code> is available.
      */
      LIBAPI virtual bool hasHostile() = 0;

      /**
      * Gets the value of the <code>hostile</code> attribute.
      *
      * <br>Description from the FOM: <i>Hostile (yes/no)</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>hostile</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual bool getHostile()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>hostile</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Hostile (yes/no)</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>hostile</code> attribute.
      */
      LIBAPI virtual bool getHostile(bool defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>hostile</code> attribute.
      * <br>Description from the FOM: <i>Hostile (yes/no)</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>hostile</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< bool > getHostileTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>infrastructure</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Spot Infrastructure. Represented as an integer which its value is directing to an external enumeration.</i>
      *
      * @return true if <code>infrastructure</code> is available.
      */
      LIBAPI virtual bool hasInfrastructure() = 0;

      /**
      * Gets the value of the <code>infrastructure</code> attribute.
      *
      * <br>Description from the FOM: <i>Spot Infrastructure. Represented as an integer which its value is directing to an external enumeration.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^31, 2^31 - 1]</i>
      *
      * @return the <code>infrastructure</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual int getInfrastructure()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>infrastructure</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Spot Infrastructure. Represented as an integer which its value is directing to an external enumeration.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^31, 2^31 - 1]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>infrastructure</code> attribute.
      */
      LIBAPI virtual int getInfrastructure(int defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>infrastructure</code> attribute.
      * <br>Description from the FOM: <i>Spot Infrastructure. Represented as an integer which its value is directing to an external enumeration.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^31, 2^31 - 1]</i>
      *
      * @return the time stamped <code>infrastructure</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< int > getInfrastructureTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>fortifications</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Defines fortification type .</i>
      *
      * @return true if <code>fortifications</code> is available.
      */
      LIBAPI virtual bool hasFortifications() = 0;

      /**
      * Gets the value of the <code>fortifications</code> attribute.
      *
      * <br>Description from the FOM: <i>Defines fortification type .</i>
      * <br>Description of the data type from the FOM: <i>-NULL-</i>
      *
      * @return the <code>fortifications</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::FortificationsEnum::FortificationsEnum getFortifications()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>fortifications</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Defines fortification type .</i>
      * <br>Description of the data type from the FOM: <i>-NULL-</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>fortifications</code> attribute.
      */
      LIBAPI virtual DevStudio::FortificationsEnum::FortificationsEnum getFortifications(DevStudio::FortificationsEnum::FortificationsEnum defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>fortifications</code> attribute.
      * <br>Description from the FOM: <i>Defines fortification type .</i>
      * <br>Description of the data type from the FOM: <i>-NULL-</i>
      *
      * @return the time stamped <code>fortifications</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::FortificationsEnum::FortificationsEnum > getFortificationsTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>floors</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Floor Number. (Max 8 characters).</i>
      *
      * @return true if <code>floors</code> is available.
      */
      LIBAPI virtual bool hasFloors() = 0;

      /**
      * Gets the value of the <code>floors</code> attribute.
      *
      * <br>Description from the FOM: <i>Floor Number. (Max 8 characters).</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>floors</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::wstring getFloors()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>floors</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Floor Number. (Max 8 characters).</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>floors</code> attribute.
      */
      LIBAPI virtual std::wstring getFloors(std::wstring defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>floors</code> attribute.
      * <br>Description from the FOM: <i>Floor Number. (Max 8 characters).</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>floors</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::wstring > getFloorsTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>totalFloors</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Number of total floors. (Max 8 characters).</i>
      *
      * @return true if <code>totalFloors</code> is available.
      */
      LIBAPI virtual bool hasTotalFloors() = 0;

      /**
      * Gets the value of the <code>totalFloors</code> attribute.
      *
      * <br>Description from the FOM: <i>Number of total floors. (Max 8 characters).</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>totalFloors</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::wstring getTotalFloors()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>totalFloors</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Number of total floors. (Max 8 characters).</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>totalFloors</code> attribute.
      */
      LIBAPI virtual std::wstring getTotalFloors(std::wstring defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>totalFloors</code> attribute.
      * <br>Description from the FOM: <i>Number of total floors. (Max 8 characters).</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>totalFloors</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::wstring > getTotalFloorsTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>acquisitionType</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Spot object acquisition Type.</i>
      *
      * @return true if <code>acquisitionType</code> is available.
      */
      LIBAPI virtual bool hasAcquisitionType() = 0;

      /**
      * Gets the value of the <code>acquisitionType</code> attribute.
      *
      * <br>Description from the FOM: <i>Spot object acquisition Type.</i>
      * <br>Description of the data type from the FOM: <i>-NULL-</i>
      *
      * @return the <code>acquisitionType</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::AcquisitionTypeEnum::AcquisitionTypeEnum getAcquisitionType()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>acquisitionType</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Spot object acquisition Type.</i>
      * <br>Description of the data type from the FOM: <i>-NULL-</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>acquisitionType</code> attribute.
      */
      LIBAPI virtual DevStudio::AcquisitionTypeEnum::AcquisitionTypeEnum getAcquisitionType(DevStudio::AcquisitionTypeEnum::AcquisitionTypeEnum defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>acquisitionType</code> attribute.
      * <br>Description from the FOM: <i>Spot object acquisition Type.</i>
      * <br>Description of the data type from the FOM: <i>-NULL-</i>
      *
      * @return the time stamped <code>acquisitionType</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::AcquisitionTypeEnum::AcquisitionTypeEnum > getAcquisitionTypeTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>reportingEntityName</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The name of the reporting unit/entity. (Max 17 characters).</i>
      *
      * @return true if <code>reportingEntityName</code> is available.
      */
      LIBAPI virtual bool hasReportingEntityName() = 0;

      /**
      * Gets the value of the <code>reportingEntityName</code> attribute.
      *
      * <br>Description from the FOM: <i>The name of the reporting unit/entity. (Max 17 characters).</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>reportingEntityName</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::wstring getReportingEntityName()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>reportingEntityName</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The name of the reporting unit/entity. (Max 17 characters).</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>reportingEntityName</code> attribute.
      */
      LIBAPI virtual std::wstring getReportingEntityName(std::wstring defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>reportingEntityName</code> attribute.
      * <br>Description from the FOM: <i>The name of the reporting unit/entity. (Max 17 characters).</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>reportingEntityName</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::wstring > getReportingEntityNameTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>armyOrg</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Defines of which army organization the spotted entity belongs. Represented as an integer which its value is directing to an external enumeration.</i>
      *
      * @return true if <code>armyOrg</code> is available.
      */
      LIBAPI virtual bool hasArmyOrg() = 0;

      /**
      * Gets the value of the <code>armyOrg</code> attribute.
      *
      * <br>Description from the FOM: <i>Defines of which army organization the spotted entity belongs. Represented as an integer which its value is directing to an external enumeration.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^31, 2^31 - 1]</i>
      *
      * @return the <code>armyOrg</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual int getArmyOrg()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>armyOrg</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Defines of which army organization the spotted entity belongs. Represented as an integer which its value is directing to an external enumeration.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^31, 2^31 - 1]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>armyOrg</code> attribute.
      */
      LIBAPI virtual int getArmyOrg(int defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>armyOrg</code> attribute.
      * <br>Description from the FOM: <i>Defines of which army organization the spotted entity belongs. Represented as an integer which its value is directing to an external enumeration.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^31, 2^31 - 1]</i>
      *
      * @return the time stamped <code>armyOrg</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< int > getArmyOrgTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>spotCategory</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Spot object category.</i>
      *
      * @return true if <code>spotCategory</code> is available.
      */
      LIBAPI virtual bool hasSpotCategory() = 0;

      /**
      * Gets the value of the <code>spotCategory</code> attribute.
      *
      * <br>Description from the FOM: <i>Spot object category.</i>
      * <br>Description of the data type from the FOM: <i>-NULL-</i>
      *
      * @return the <code>spotCategory</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::SpotCategoryEnum::SpotCategoryEnum getSpotCategory()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>spotCategory</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Spot object category.</i>
      * <br>Description of the data type from the FOM: <i>-NULL-</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>spotCategory</code> attribute.
      */
      LIBAPI virtual DevStudio::SpotCategoryEnum::SpotCategoryEnum getSpotCategory(DevStudio::SpotCategoryEnum::SpotCategoryEnum defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>spotCategory</code> attribute.
      * <br>Description from the FOM: <i>Spot object category.</i>
      * <br>Description of the data type from the FOM: <i>-NULL-</i>
      *
      * @return the time stamped <code>spotCategory</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::SpotCategoryEnum::SpotCategoryEnum > getSpotCategoryTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>casualtiesNumber</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Number of casualties in the spotted entity.</i>
      *
      * @return true if <code>casualtiesNumber</code> is available.
      */
      LIBAPI virtual bool hasCasualtiesNumber() = 0;

      /**
      * Gets the value of the <code>casualtiesNumber</code> attribute.
      *
      * <br>Description from the FOM: <i>Number of casualties in the spotted entity.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^31, 2^31 - 1]</i>
      *
      * @return the <code>casualtiesNumber</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual int getCasualtiesNumber()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>casualtiesNumber</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Number of casualties in the spotted entity.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^31, 2^31 - 1]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>casualtiesNumber</code> attribute.
      */
      LIBAPI virtual int getCasualtiesNumber(int defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>casualtiesNumber</code> attribute.
      * <br>Description from the FOM: <i>Number of casualties in the spotted entity.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^31, 2^31 - 1]</i>
      *
      * @return the time stamped <code>casualtiesNumber</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< int > getCasualtiesNumberTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>spottedEntityActivityStatus</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Spotted entity activity status. Represented as an integer which its value is directing to an external enumeration.</i>
      *
      * @return true if <code>spottedEntityActivityStatus</code> is available.
      */
      LIBAPI virtual bool hasSpottedEntityActivityStatus() = 0;

      /**
      * Gets the value of the <code>spottedEntityActivityStatus</code> attribute.
      *
      * <br>Description from the FOM: <i>Spotted entity activity status. Represented as an integer which its value is directing to an external enumeration.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^31, 2^31 - 1]</i>
      *
      * @return the <code>spottedEntityActivityStatus</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual int getSpottedEntityActivityStatus()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>spottedEntityActivityStatus</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Spotted entity activity status. Represented as an integer which its value is directing to an external enumeration.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^31, 2^31 - 1]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>spottedEntityActivityStatus</code> attribute.
      */
      LIBAPI virtual int getSpottedEntityActivityStatus(int defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>spottedEntityActivityStatus</code> attribute.
      * <br>Description from the FOM: <i>Spotted entity activity status. Represented as an integer which its value is directing to an external enumeration.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^31, 2^31 - 1]</i>
      *
      * @return the time stamped <code>spottedEntityActivityStatus</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< int > getSpottedEntityActivityStatusTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>entityType</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The category of the entity.</i>
      *
      * @return true if <code>entityType</code> is available.
      */
      LIBAPI virtual bool hasEntityType() = 0;

      /**
      * Gets the value of the <code>entityType</code> attribute.
      *
      * <br>Description from the FOM: <i>The category of the entity.</i>
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      *
      * @return the <code>entityType</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::EntityTypeStruct getEntityType()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>entityType</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The category of the entity.</i>
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>entityType</code> attribute.
      */
      LIBAPI virtual DevStudio::EntityTypeStruct getEntityType(DevStudio::EntityTypeStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>entityType</code> attribute.
      * <br>Description from the FOM: <i>The category of the entity.</i>
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      *
      * @return the time stamped <code>entityType</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::EntityTypeStruct > getEntityTypeTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>entityIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The unique identifier for the entity instance.</i>
      *
      * @return true if <code>entityIdentifier</code> is available.
      */
      LIBAPI virtual bool hasEntityIdentifier() = 0;

      /**
      * Gets the value of the <code>entityIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>The unique identifier for the entity instance.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @return the <code>entityIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::EntityIdentifierStruct getEntityIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>entityIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The unique identifier for the entity instance.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>entityIdentifier</code> attribute.
      */
      LIBAPI virtual DevStudio::EntityIdentifierStruct getEntityIdentifier(DevStudio::EntityIdentifierStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>entityIdentifier</code> attribute.
      * <br>Description from the FOM: <i>The unique identifier for the entity instance.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @return the time stamped <code>entityIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::EntityIdentifierStruct > getEntityIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>isPartOf</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Defines if the entity if a constituent part of another entity (denoted the host entity). If the entity is a constituent part of another entity then the HostEntityIdentifier shall be set to the EntityIdentifier of the host entity and the HostRTIObjectIdentifier shall be set to the RTI object instance ID of the host entity. If the entity is not a constituent part of another entity then the HostEntityIdentifier shall be set to 0.0.0 and the HostRTIObjectIdentifier shall be set to the empty string.</i>
      *
      * @return true if <code>isPartOf</code> is available.
      */
      LIBAPI virtual bool hasIsPartOf() = 0;

      /**
      * Gets the value of the <code>isPartOf</code> attribute.
      *
      * <br>Description from the FOM: <i>Defines if the entity if a constituent part of another entity (denoted the host entity). If the entity is a constituent part of another entity then the HostEntityIdentifier shall be set to the EntityIdentifier of the host entity and the HostRTIObjectIdentifier shall be set to the RTI object instance ID of the host entity. If the entity is not a constituent part of another entity then the HostEntityIdentifier shall be set to 0.0.0 and the HostRTIObjectIdentifier shall be set to the empty string.</i>
      * <br>Description of the data type from the FOM: <i>Defines the spatial relationship between two objects.</i>
      *
      * @return the <code>isPartOf</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::IsPartOfStruct getIsPartOf()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>isPartOf</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Defines if the entity if a constituent part of another entity (denoted the host entity). If the entity is a constituent part of another entity then the HostEntityIdentifier shall be set to the EntityIdentifier of the host entity and the HostRTIObjectIdentifier shall be set to the RTI object instance ID of the host entity. If the entity is not a constituent part of another entity then the HostEntityIdentifier shall be set to 0.0.0 and the HostRTIObjectIdentifier shall be set to the empty string.</i>
      * <br>Description of the data type from the FOM: <i>Defines the spatial relationship between two objects.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>isPartOf</code> attribute.
      */
      LIBAPI virtual DevStudio::IsPartOfStruct getIsPartOf(DevStudio::IsPartOfStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>isPartOf</code> attribute.
      * <br>Description from the FOM: <i>Defines if the entity if a constituent part of another entity (denoted the host entity). If the entity is a constituent part of another entity then the HostEntityIdentifier shall be set to the EntityIdentifier of the host entity and the HostRTIObjectIdentifier shall be set to the RTI object instance ID of the host entity. If the entity is not a constituent part of another entity then the HostEntityIdentifier shall be set to 0.0.0 and the HostRTIObjectIdentifier shall be set to the empty string.</i>
      * <br>Description of the data type from the FOM: <i>Defines the spatial relationship between two objects.</i>
      *
      * @return the time stamped <code>isPartOf</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::IsPartOfStruct > getIsPartOfTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>spatial</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Spatial state stored in one variant record attribute.</i>
      *
      * @return true if <code>spatial</code> is available.
      */
      LIBAPI virtual bool hasSpatial() = 0;

      /**
      * Gets the value of the <code>spatial</code> attribute.
      *
      * <br>Description from the FOM: <i>Spatial state stored in one variant record attribute.</i>
      * <br>Description of the data type from the FOM: <i>Variant Record for a single spatial attribute.</i>
      *
      * @return the <code>spatial</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::SpatialVariantStruct getSpatial()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>spatial</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Spatial state stored in one variant record attribute.</i>
      * <br>Description of the data type from the FOM: <i>Variant Record for a single spatial attribute.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>spatial</code> attribute.
      */
      LIBAPI virtual DevStudio::SpatialVariantStruct getSpatial(DevStudio::SpatialVariantStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>spatial</code> attribute.
      * <br>Description from the FOM: <i>Spatial state stored in one variant record attribute.</i>
      * <br>Description of the data type from the FOM: <i>Variant Record for a single spatial attribute.</i>
      *
      * @return the time stamped <code>spatial</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::SpatialVariantStruct > getSpatialTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>relativeSpatial</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Relative spatial state stored in one variant record attribute.</i>
      *
      * @return true if <code>relativeSpatial</code> is available.
      */
      LIBAPI virtual bool hasRelativeSpatial() = 0;

      /**
      * Gets the value of the <code>relativeSpatial</code> attribute.
      *
      * <br>Description from the FOM: <i>Relative spatial state stored in one variant record attribute.</i>
      * <br>Description of the data type from the FOM: <i>Variant Record for a single spatial attribute.</i>
      *
      * @return the <code>relativeSpatial</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::SpatialVariantStruct getRelativeSpatial()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>relativeSpatial</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Relative spatial state stored in one variant record attribute.</i>
      * <br>Description of the data type from the FOM: <i>Variant Record for a single spatial attribute.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>relativeSpatial</code> attribute.
      */
      LIBAPI virtual DevStudio::SpatialVariantStruct getRelativeSpatial(DevStudio::SpatialVariantStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>relativeSpatial</code> attribute.
      * <br>Description from the FOM: <i>Relative spatial state stored in one variant record attribute.</i>
      * <br>Description of the data type from the FOM: <i>Variant Record for a single spatial attribute.</i>
      *
      * @return the time stamped <code>relativeSpatial</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::SpatialVariantStruct > getRelativeSpatialTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
   };
}
#endif
