/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAAGGREGATEENTITYATTRIBUTES_H
#define DEVELOPER_STUDIO_HLAAGGREGATEENTITYATTRIBUTES_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <string>
#include <vector>
#include <utility>
    
#include <boost/noncopyable.hpp>
    
#include <DevStudio/datatypes/AggregateMarkingStruct.h>
#include <DevStudio/datatypes/AggregateStateEnum.h>
#include <DevStudio/datatypes/DimensionStruct.h>
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EntityTypeStruct.h>
#include <DevStudio/datatypes/ForceIdentifierEnum.h>
#include <DevStudio/datatypes/FormationEnum.h>
#include <DevStudio/datatypes/IsPartOfStruct.h>
#include <DevStudio/datatypes/RTIobjectIdArray.h>
#include <DevStudio/datatypes/SilentAggregateStruct.h>
#include <DevStudio/datatypes/SilentAggregateStructLengthlessArray.h>
#include <DevStudio/datatypes/SilentEntityStruct.h>
#include <DevStudio/datatypes/SilentEntityStructLengthlessArray.h>
#include <DevStudio/datatypes/SpatialVariantStruct.h>
#include <DevStudio/datatypes/VariableDatumStruct.h>
#include <DevStudio/datatypes/VariableDatumStructLengthlessArray.h>
#include <string>
#include <vector>

#include <DevStudio/HlaException.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaBaseEntityAttributes.h>
#include <DevStudio/HlaTimeStamped.h>

namespace DevStudio {

   /**
   * Interface used to represent all attributes for an object instance.
   */
   class HlaAggregateEntityAttributes : public HlaBaseEntityAttributes {
   public:


     /**
      * An enumeration of the attributes of an HlaAggregateEntity
      *
      *<table summary="All attributes">
      * <tr><td><b>Enum constant</b></td><td><b>C++ name</b></td><td><b>FOM name</b></td></tr>
      * <tr><td>AGGREGATE_MARKING</td><td>aggregateMarking</td><td><code>AggregateMarking</code></td></tr>
      * <tr><td>AGGREGATE_STATE</td><td>aggregateState</td><td><code>AggregateState</code></td></tr>
      * <tr><td>DIMENSIONS</td><td>dimensions</td><td><code>Dimensions</code></td></tr>
      * <tr><td>ENTITY_IDENTIFIERS</td><td>entityIdentifiers</td><td><code>EntityIdentifiers</code></td></tr>
      * <tr><td>FORCE_IDENTIFIER</td><td>forceIdentifier</td><td><code>ForceIdentifier</code></td></tr>
      * <tr><td>FORMATION</td><td>formation</td><td><code>Formation</code></td></tr>
      * <tr><td>NUMBER_OF_SILENT_ENTITIES</td><td>numberOfSilentEntities</td><td><code>NumberOfSilentEntities</code></td></tr>
      * <tr><td>NUMBER_OF_VARIABLE_DATUMS</td><td>numberOfVariableDatums</td><td><code>NumberOfVariableDatums</code></td></tr>
      * <tr><td>SILENT_AGGREGATES</td><td>silentAggregates</td><td><code>SilentAggregates</code></td></tr>
      * <tr><td>SILENT_ENTITIES</td><td>silentEntities</td><td><code>SilentEntities</code></td></tr>
      * <tr><td>SUB_AGGREGATE_IDENTIFIERS</td><td>subAggregateIdentifiers</td><td><code>SubAggregateIdentifiers</code></td></tr>
      * <tr><td>VARIABLE_DATUMS</td><td>variableDatums</td><td><code>VariableDatums</code></td></tr>
      * <tr><td>ENTITY_TYPE</td><td>entityType</td><td><code>EntityType</code></td></tr>
      * <tr><td>ENTITY_IDENTIFIER</td><td>entityIdentifier</td><td><code>EntityIdentifier</code></td></tr>
      * <tr><td>IS_PART_OF</td><td>isPartOf</td><td><code>IsPartOf</code></td></tr>
      * <tr><td>SPATIAL</td><td>spatial</td><td><code>Spatial</code></td></tr>
      * <tr><td>RELATIVE_SPATIAL</td><td>relativeSpatial</td><td><code>RelativeSpatial</code></td></tr>
      *</table>
      */
      enum Attribute {
        /**
        * aggregateMarking (FOM name: <code>AggregateMarking</code>).
        * <br>Description from the FOM: <i>A unique marking or combination of characters used to distinguish the aggregate from other aggregates.</i>
        */
         AGGREGATE_MARKING,

        /**
        * aggregateState (FOM name: <code>AggregateState</code>).
        * <br>Description from the FOM: <i>An indicator of the extent of association of objects form an operating group.</i>
        */
         AGGREGATE_STATE,

        /**
        * dimensions (FOM name: <code>Dimensions</code>).
        * <br>Description from the FOM: <i>The size of the area covered by the units in the aggregate.</i>
        */
         DIMENSIONS,

        /**
        * entityIdentifiers (FOM name: <code>EntityIdentifiers</code>).
        * <br>Description from the FOM: <i>The identification of entities that are contained within the aggregate.</i>
        */
         ENTITY_IDENTIFIERS,

        /**
        * forceIdentifier (FOM name: <code>ForceIdentifier</code>).
        * <br>Description from the FOM: <i>The identification of the force that the aggregate belongs to.</i>
        */
         FORCE_IDENTIFIER,

        /**
        * formation (FOM name: <code>Formation</code>).
        * <br>Description from the FOM: <i>The category of positional arrangement of the entities within the aggregate.</i>
        */
         FORMATION,

        /**
        * numberOfSilentEntities (FOM name: <code>NumberOfSilentEntities</code>).
        * <br>Description from the FOM: <i>The number of elements in the SilentEntities list.</i>
        */
         NUMBER_OF_SILENT_ENTITIES,

        /**
        * numberOfVariableDatums (FOM name: <code>NumberOfVariableDatums</code>).
        * <br>Description from the FOM: <i>The number of records in the VariableDatums structure.</i>
        */
         NUMBER_OF_VARIABLE_DATUMS,

        /**
        * silentAggregates (FOM name: <code>SilentAggregates</code>).
        * <br>Description from the FOM: <i>The numbers and types, of silent aggregates contained in the aggregate. Silent aggregates are sub-aggregates that are in the aggregate, but that are not separately represented in the virtual world.</i>
        */
         SILENT_AGGREGATES,

        /**
        * silentEntities (FOM name: <code>SilentEntities</code>).
        * <br>Description from the FOM: <i>The numbers and types, of silent entities in the aggregate. Silent entities are entities that are in the aggregate, but that are not separately represented in the virtual world.</i>
        */
         SILENT_ENTITIES,

        /**
        * subAggregateIdentifiers (FOM name: <code>SubAggregateIdentifiers</code>).
        * <br>Description from the FOM: <i>The identifications of aggregates represented in the virtual world that are contained in the aggregate.</i>
        */
         SUB_AGGREGATE_IDENTIFIERS,

        /**
        * variableDatums (FOM name: <code>VariableDatums</code>).
        * <br>Description from the FOM: <i>Extra data that describes the aggregate.</i>
        */
         VARIABLE_DATUMS,

        /**
        * entityType (FOM name: <code>EntityType</code>).
        * <br>Description from the FOM: <i>The category of the entity.</i>
        */
         ENTITY_TYPE,

        /**
        * entityIdentifier (FOM name: <code>EntityIdentifier</code>).
        * <br>Description from the FOM: <i>The unique identifier for the entity instance.</i>
        */
         ENTITY_IDENTIFIER,

        /**
        * isPartOf (FOM name: <code>IsPartOf</code>).
        * <br>Description from the FOM: <i>Defines if the entity if a constituent part of another entity (denoted the host entity). If the entity is a constituent part of another entity then the HostEntityIdentifier shall be set to the EntityIdentifier of the host entity and the HostRTIObjectIdentifier shall be set to the RTI object instance ID of the host entity. If the entity is not a constituent part of another entity then the HostEntityIdentifier shall be set to 0.0.0 and the HostRTIObjectIdentifier shall be set to the empty string.</i>
        */
         IS_PART_OF,

        /**
        * spatial (FOM name: <code>Spatial</code>).
        * <br>Description from the FOM: <i>Spatial state stored in one variant record attribute.</i>
        */
         SPATIAL,

        /**
        * relativeSpatial (FOM name: <code>RelativeSpatial</code>).
        * <br>Description from the FOM: <i>Relative spatial state stored in one variant record attribute.</i>
        */
         RELATIVE_SPATIAL
      };

     /**
      * Gets the name of the attribute.
      *
      * @return The name of the attribute. An empty string will be returned if the attribute does not exist.
      */
      LIBAPI virtual std::wstring getName(Attribute attribute);

     /**
      * Finds the attribute specified in the parameter attributeName.
      * The found enumeration will be stored in the parameter attribute.
      *
      * @return true if the attribute was found, false otherwise.
      */
      LIBAPI virtual bool find(Attribute& attribute, std::wstring attributeName);

      LIBAPI virtual ~HlaAggregateEntityAttributes() {}
    
      /**
      * Returns true if the <code>aggregateMarking</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>A unique marking or combination of characters used to distinguish the aggregate from other aggregates.</i>
      *
      * @return true if <code>aggregateMarking</code> is available.
      */
      LIBAPI virtual bool hasAggregateMarking() = 0;

      /**
      * Gets the value of the <code>aggregateMarking</code> attribute.
      *
      * <br>Description from the FOM: <i>A unique marking or combination of characters used to distinguish the aggregate from other aggregates.</i>
      * <br>Description of the data type from the FOM: <i>Unique marking associated with an aggregate.</i>
      *
      * @return the <code>aggregateMarking</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::AggregateMarkingStruct getAggregateMarking()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>aggregateMarking</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>A unique marking or combination of characters used to distinguish the aggregate from other aggregates.</i>
      * <br>Description of the data type from the FOM: <i>Unique marking associated with an aggregate.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>aggregateMarking</code> attribute.
      */
      LIBAPI virtual DevStudio::AggregateMarkingStruct getAggregateMarking(DevStudio::AggregateMarkingStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>aggregateMarking</code> attribute.
      * <br>Description from the FOM: <i>A unique marking or combination of characters used to distinguish the aggregate from other aggregates.</i>
      * <br>Description of the data type from the FOM: <i>Unique marking associated with an aggregate.</i>
      *
      * @return the time stamped <code>aggregateMarking</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::AggregateMarkingStruct > getAggregateMarkingTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>aggregateState</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>An indicator of the extent of association of objects form an operating group.</i>
      *
      * @return true if <code>aggregateState</code> is available.
      */
      LIBAPI virtual bool hasAggregateState() = 0;

      /**
      * Gets the value of the <code>aggregateState</code> attribute.
      *
      * <br>Description from the FOM: <i>An indicator of the extent of association of objects form an operating group.</i>
      * <br>Description of the data type from the FOM: <i>Aggregate state</i>
      *
      * @return the <code>aggregateState</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::AggregateStateEnum::AggregateStateEnum getAggregateState()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>aggregateState</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>An indicator of the extent of association of objects form an operating group.</i>
      * <br>Description of the data type from the FOM: <i>Aggregate state</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>aggregateState</code> attribute.
      */
      LIBAPI virtual DevStudio::AggregateStateEnum::AggregateStateEnum getAggregateState(DevStudio::AggregateStateEnum::AggregateStateEnum defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>aggregateState</code> attribute.
      * <br>Description from the FOM: <i>An indicator of the extent of association of objects form an operating group.</i>
      * <br>Description of the data type from the FOM: <i>Aggregate state</i>
      *
      * @return the time stamped <code>aggregateState</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::AggregateStateEnum::AggregateStateEnum > getAggregateStateTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>dimensions</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The size of the area covered by the units in the aggregate.</i>
      *
      * @return true if <code>dimensions</code> is available.
      */
      LIBAPI virtual bool hasDimensions() = 0;

      /**
      * Gets the value of the <code>dimensions</code> attribute.
      *
      * <br>Description from the FOM: <i>The size of the area covered by the units in the aggregate.</i>
      * <br>Description of the data type from the FOM: <i>Bounding box in X,Y,Z axis.</i>
      *
      * @return the <code>dimensions</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::DimensionStruct getDimensions()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>dimensions</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The size of the area covered by the units in the aggregate.</i>
      * <br>Description of the data type from the FOM: <i>Bounding box in X,Y,Z axis.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>dimensions</code> attribute.
      */
      LIBAPI virtual DevStudio::DimensionStruct getDimensions(DevStudio::DimensionStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>dimensions</code> attribute.
      * <br>Description from the FOM: <i>The size of the area covered by the units in the aggregate.</i>
      * <br>Description of the data type from the FOM: <i>Bounding box in X,Y,Z axis.</i>
      *
      * @return the time stamped <code>dimensions</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::DimensionStruct > getDimensionsTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>entityIdentifiers</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The identification of entities that are contained within the aggregate.</i>
      *
      * @return true if <code>entityIdentifiers</code> is available.
      */
      LIBAPI virtual bool hasEntityIdentifiers() = 0;

      /**
      * Gets the value of the <code>entityIdentifiers</code> attribute.
      *
      * <br>Description from the FOM: <i>The identification of entities that are contained within the aggregate.</i>
      * <br>Description of the data type from the FOM: <i>Set of ID's of registered object instances.</i>
      *
      * @return the <code>entityIdentifiers</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::vector<std::string > getEntityIdentifiers()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>entityIdentifiers</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The identification of entities that are contained within the aggregate.</i>
      * <br>Description of the data type from the FOM: <i>Set of ID's of registered object instances.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>entityIdentifiers</code> attribute.
      */
      LIBAPI virtual std::vector<std::string > getEntityIdentifiers(std::vector<std::string > defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>entityIdentifiers</code> attribute.
      * <br>Description from the FOM: <i>The identification of entities that are contained within the aggregate.</i>
      * <br>Description of the data type from the FOM: <i>Set of ID's of registered object instances.</i>
      *
      * @return the time stamped <code>entityIdentifiers</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::vector<std::string > > getEntityIdentifiersTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>forceIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The identification of the force that the aggregate belongs to.</i>
      *
      * @return true if <code>forceIdentifier</code> is available.
      */
      LIBAPI virtual bool hasForceIdentifier() = 0;

      /**
      * Gets the value of the <code>forceIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>The identification of the force that the aggregate belongs to.</i>
      * <br>Description of the data type from the FOM: <i>Force ID</i>
      *
      * @return the <code>forceIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::ForceIdentifierEnum::ForceIdentifierEnum getForceIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>forceIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The identification of the force that the aggregate belongs to.</i>
      * <br>Description of the data type from the FOM: <i>Force ID</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>forceIdentifier</code> attribute.
      */
      LIBAPI virtual DevStudio::ForceIdentifierEnum::ForceIdentifierEnum getForceIdentifier(DevStudio::ForceIdentifierEnum::ForceIdentifierEnum defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>forceIdentifier</code> attribute.
      * <br>Description from the FOM: <i>The identification of the force that the aggregate belongs to.</i>
      * <br>Description of the data type from the FOM: <i>Force ID</i>
      *
      * @return the time stamped <code>forceIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::ForceIdentifierEnum::ForceIdentifierEnum > getForceIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>formation</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The category of positional arrangement of the entities within the aggregate.</i>
      *
      * @return true if <code>formation</code> is available.
      */
      LIBAPI virtual bool hasFormation() = 0;

      /**
      * Gets the value of the <code>formation</code> attribute.
      *
      * <br>Description from the FOM: <i>The category of positional arrangement of the entities within the aggregate.</i>
      * <br>Description of the data type from the FOM: <i>Formation</i>
      *
      * @return the <code>formation</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::FormationEnum::FormationEnum getFormation()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>formation</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The category of positional arrangement of the entities within the aggregate.</i>
      * <br>Description of the data type from the FOM: <i>Formation</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>formation</code> attribute.
      */
      LIBAPI virtual DevStudio::FormationEnum::FormationEnum getFormation(DevStudio::FormationEnum::FormationEnum defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>formation</code> attribute.
      * <br>Description from the FOM: <i>The category of positional arrangement of the entities within the aggregate.</i>
      * <br>Description of the data type from the FOM: <i>Formation</i>
      *
      * @return the time stamped <code>formation</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::FormationEnum::FormationEnum > getFormationTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>numberOfSilentEntities</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The number of elements in the SilentEntities list.</i>
      *
      * @return true if <code>numberOfSilentEntities</code> is available.
      */
      LIBAPI virtual bool hasNumberOfSilentEntities() = 0;

      /**
      * Gets the value of the <code>numberOfSilentEntities</code> attribute.
      *
      * <br>Description from the FOM: <i>The number of elements in the SilentEntities list.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^15, 2^15-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the <code>numberOfSilentEntities</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual short getNumberOfSilentEntities()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>numberOfSilentEntities</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The number of elements in the SilentEntities list.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^15, 2^15-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>numberOfSilentEntities</code> attribute.
      */
      LIBAPI virtual short getNumberOfSilentEntities(short defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>numberOfSilentEntities</code> attribute.
      * <br>Description from the FOM: <i>The number of elements in the SilentEntities list.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^15, 2^15-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the time stamped <code>numberOfSilentEntities</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< short > getNumberOfSilentEntitiesTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>numberOfVariableDatums</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The number of records in the VariableDatums structure.</i>
      *
      * @return true if <code>numberOfVariableDatums</code> is available.
      */
      LIBAPI virtual bool hasNumberOfVariableDatums() = 0;

      /**
      * Gets the value of the <code>numberOfVariableDatums</code> attribute.
      *
      * <br>Description from the FOM: <i>The number of records in the VariableDatums structure.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the <code>numberOfVariableDatums</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual unsigned int getNumberOfVariableDatums()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>numberOfVariableDatums</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The number of records in the VariableDatums structure.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>numberOfVariableDatums</code> attribute.
      */
      LIBAPI virtual unsigned int getNumberOfVariableDatums(unsigned int defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>numberOfVariableDatums</code> attribute.
      * <br>Description from the FOM: <i>The number of records in the VariableDatums structure.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the time stamped <code>numberOfVariableDatums</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< unsigned int > getNumberOfVariableDatumsTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>silentAggregates</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The numbers and types, of silent aggregates contained in the aggregate. Silent aggregates are sub-aggregates that are in the aggregate, but that are not separately represented in the virtual world.</i>
      *
      * @return true if <code>silentAggregates</code> is available.
      */
      LIBAPI virtual bool hasSilentAggregates() = 0;

      /**
      * Gets the value of the <code>silentAggregates</code> attribute.
      *
      * <br>Description from the FOM: <i>The numbers and types, of silent aggregates contained in the aggregate. Silent aggregates are sub-aggregates that are in the aggregate, but that are not separately represented in the virtual world.</i>
      * <br>Description of the data type from the FOM: <i>Set of silent aggregates (aggregates not registered in the federation).</i>
      *
      * @return the <code>silentAggregates</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::vector<DevStudio::SilentAggregateStruct > getSilentAggregates()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>silentAggregates</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The numbers and types, of silent aggregates contained in the aggregate. Silent aggregates are sub-aggregates that are in the aggregate, but that are not separately represented in the virtual world.</i>
      * <br>Description of the data type from the FOM: <i>Set of silent aggregates (aggregates not registered in the federation).</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>silentAggregates</code> attribute.
      */
      LIBAPI virtual std::vector<DevStudio::SilentAggregateStruct > getSilentAggregates(std::vector<DevStudio::SilentAggregateStruct > defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>silentAggregates</code> attribute.
      * <br>Description from the FOM: <i>The numbers and types, of silent aggregates contained in the aggregate. Silent aggregates are sub-aggregates that are in the aggregate, but that are not separately represented in the virtual world.</i>
      * <br>Description of the data type from the FOM: <i>Set of silent aggregates (aggregates not registered in the federation).</i>
      *
      * @return the time stamped <code>silentAggregates</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::vector<DevStudio::SilentAggregateStruct > > getSilentAggregatesTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>silentEntities</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The numbers and types, of silent entities in the aggregate. Silent entities are entities that are in the aggregate, but that are not separately represented in the virtual world.</i>
      *
      * @return true if <code>silentEntities</code> is available.
      */
      LIBAPI virtual bool hasSilentEntities() = 0;

      /**
      * Gets the value of the <code>silentEntities</code> attribute.
      *
      * <br>Description from the FOM: <i>The numbers and types, of silent entities in the aggregate. Silent entities are entities that are in the aggregate, but that are not separately represented in the virtual world.</i>
      * <br>Description of the data type from the FOM: <i>A set of silent entities (entities not registered in the federation).</i>
      *
      * @return the <code>silentEntities</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::vector<DevStudio::SilentEntityStruct > getSilentEntities()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>silentEntities</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The numbers and types, of silent entities in the aggregate. Silent entities are entities that are in the aggregate, but that are not separately represented in the virtual world.</i>
      * <br>Description of the data type from the FOM: <i>A set of silent entities (entities not registered in the federation).</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>silentEntities</code> attribute.
      */
      LIBAPI virtual std::vector<DevStudio::SilentEntityStruct > getSilentEntities(std::vector<DevStudio::SilentEntityStruct > defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>silentEntities</code> attribute.
      * <br>Description from the FOM: <i>The numbers and types, of silent entities in the aggregate. Silent entities are entities that are in the aggregate, but that are not separately represented in the virtual world.</i>
      * <br>Description of the data type from the FOM: <i>A set of silent entities (entities not registered in the federation).</i>
      *
      * @return the time stamped <code>silentEntities</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::vector<DevStudio::SilentEntityStruct > > getSilentEntitiesTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>subAggregateIdentifiers</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The identifications of aggregates represented in the virtual world that are contained in the aggregate.</i>
      *
      * @return true if <code>subAggregateIdentifiers</code> is available.
      */
      LIBAPI virtual bool hasSubAggregateIdentifiers() = 0;

      /**
      * Gets the value of the <code>subAggregateIdentifiers</code> attribute.
      *
      * <br>Description from the FOM: <i>The identifications of aggregates represented in the virtual world that are contained in the aggregate.</i>
      * <br>Description of the data type from the FOM: <i>Set of ID's of registered object instances.</i>
      *
      * @return the <code>subAggregateIdentifiers</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::vector<std::string > getSubAggregateIdentifiers()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>subAggregateIdentifiers</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The identifications of aggregates represented in the virtual world that are contained in the aggregate.</i>
      * <br>Description of the data type from the FOM: <i>Set of ID's of registered object instances.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>subAggregateIdentifiers</code> attribute.
      */
      LIBAPI virtual std::vector<std::string > getSubAggregateIdentifiers(std::vector<std::string > defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>subAggregateIdentifiers</code> attribute.
      * <br>Description from the FOM: <i>The identifications of aggregates represented in the virtual world that are contained in the aggregate.</i>
      * <br>Description of the data type from the FOM: <i>Set of ID's of registered object instances.</i>
      *
      * @return the time stamped <code>subAggregateIdentifiers</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::vector<std::string > > getSubAggregateIdentifiersTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>variableDatums</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Extra data that describes the aggregate.</i>
      *
      * @return true if <code>variableDatums</code> is available.
      */
      LIBAPI virtual bool hasVariableDatums() = 0;

      /**
      * Gets the value of the <code>variableDatums</code> attribute.
      *
      * <br>Description from the FOM: <i>Extra data that describes the aggregate.</i>
      * <br>Description of the data type from the FOM: <i>Set of additional data associated with an aggregate.</i>
      *
      * @return the <code>variableDatums</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::vector<DevStudio::VariableDatumStruct > getVariableDatums()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>variableDatums</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Extra data that describes the aggregate.</i>
      * <br>Description of the data type from the FOM: <i>Set of additional data associated with an aggregate.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>variableDatums</code> attribute.
      */
      LIBAPI virtual std::vector<DevStudio::VariableDatumStruct > getVariableDatums(std::vector<DevStudio::VariableDatumStruct > defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>variableDatums</code> attribute.
      * <br>Description from the FOM: <i>Extra data that describes the aggregate.</i>
      * <br>Description of the data type from the FOM: <i>Set of additional data associated with an aggregate.</i>
      *
      * @return the time stamped <code>variableDatums</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::vector<DevStudio::VariableDatumStruct > > getVariableDatumsTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>entityType</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The category of the entity.</i>
      *
      * @return true if <code>entityType</code> is available.
      */
      LIBAPI virtual bool hasEntityType() = 0;

      /**
      * Gets the value of the <code>entityType</code> attribute.
      *
      * <br>Description from the FOM: <i>The category of the entity.</i>
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      *
      * @return the <code>entityType</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::EntityTypeStruct getEntityType()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>entityType</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The category of the entity.</i>
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>entityType</code> attribute.
      */
      LIBAPI virtual DevStudio::EntityTypeStruct getEntityType(DevStudio::EntityTypeStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>entityType</code> attribute.
      * <br>Description from the FOM: <i>The category of the entity.</i>
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      *
      * @return the time stamped <code>entityType</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::EntityTypeStruct > getEntityTypeTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>entityIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The unique identifier for the entity instance.</i>
      *
      * @return true if <code>entityIdentifier</code> is available.
      */
      LIBAPI virtual bool hasEntityIdentifier() = 0;

      /**
      * Gets the value of the <code>entityIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>The unique identifier for the entity instance.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @return the <code>entityIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::EntityIdentifierStruct getEntityIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>entityIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The unique identifier for the entity instance.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>entityIdentifier</code> attribute.
      */
      LIBAPI virtual DevStudio::EntityIdentifierStruct getEntityIdentifier(DevStudio::EntityIdentifierStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>entityIdentifier</code> attribute.
      * <br>Description from the FOM: <i>The unique identifier for the entity instance.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @return the time stamped <code>entityIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::EntityIdentifierStruct > getEntityIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>isPartOf</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Defines if the entity if a constituent part of another entity (denoted the host entity). If the entity is a constituent part of another entity then the HostEntityIdentifier shall be set to the EntityIdentifier of the host entity and the HostRTIObjectIdentifier shall be set to the RTI object instance ID of the host entity. If the entity is not a constituent part of another entity then the HostEntityIdentifier shall be set to 0.0.0 and the HostRTIObjectIdentifier shall be set to the empty string.</i>
      *
      * @return true if <code>isPartOf</code> is available.
      */
      LIBAPI virtual bool hasIsPartOf() = 0;

      /**
      * Gets the value of the <code>isPartOf</code> attribute.
      *
      * <br>Description from the FOM: <i>Defines if the entity if a constituent part of another entity (denoted the host entity). If the entity is a constituent part of another entity then the HostEntityIdentifier shall be set to the EntityIdentifier of the host entity and the HostRTIObjectIdentifier shall be set to the RTI object instance ID of the host entity. If the entity is not a constituent part of another entity then the HostEntityIdentifier shall be set to 0.0.0 and the HostRTIObjectIdentifier shall be set to the empty string.</i>
      * <br>Description of the data type from the FOM: <i>Defines the spatial relationship between two objects.</i>
      *
      * @return the <code>isPartOf</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::IsPartOfStruct getIsPartOf()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>isPartOf</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Defines if the entity if a constituent part of another entity (denoted the host entity). If the entity is a constituent part of another entity then the HostEntityIdentifier shall be set to the EntityIdentifier of the host entity and the HostRTIObjectIdentifier shall be set to the RTI object instance ID of the host entity. If the entity is not a constituent part of another entity then the HostEntityIdentifier shall be set to 0.0.0 and the HostRTIObjectIdentifier shall be set to the empty string.</i>
      * <br>Description of the data type from the FOM: <i>Defines the spatial relationship between two objects.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>isPartOf</code> attribute.
      */
      LIBAPI virtual DevStudio::IsPartOfStruct getIsPartOf(DevStudio::IsPartOfStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>isPartOf</code> attribute.
      * <br>Description from the FOM: <i>Defines if the entity if a constituent part of another entity (denoted the host entity). If the entity is a constituent part of another entity then the HostEntityIdentifier shall be set to the EntityIdentifier of the host entity and the HostRTIObjectIdentifier shall be set to the RTI object instance ID of the host entity. If the entity is not a constituent part of another entity then the HostEntityIdentifier shall be set to 0.0.0 and the HostRTIObjectIdentifier shall be set to the empty string.</i>
      * <br>Description of the data type from the FOM: <i>Defines the spatial relationship between two objects.</i>
      *
      * @return the time stamped <code>isPartOf</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::IsPartOfStruct > getIsPartOfTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>spatial</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Spatial state stored in one variant record attribute.</i>
      *
      * @return true if <code>spatial</code> is available.
      */
      LIBAPI virtual bool hasSpatial() = 0;

      /**
      * Gets the value of the <code>spatial</code> attribute.
      *
      * <br>Description from the FOM: <i>Spatial state stored in one variant record attribute.</i>
      * <br>Description of the data type from the FOM: <i>Variant Record for a single spatial attribute.</i>
      *
      * @return the <code>spatial</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::SpatialVariantStruct getSpatial()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>spatial</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Spatial state stored in one variant record attribute.</i>
      * <br>Description of the data type from the FOM: <i>Variant Record for a single spatial attribute.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>spatial</code> attribute.
      */
      LIBAPI virtual DevStudio::SpatialVariantStruct getSpatial(DevStudio::SpatialVariantStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>spatial</code> attribute.
      * <br>Description from the FOM: <i>Spatial state stored in one variant record attribute.</i>
      * <br>Description of the data type from the FOM: <i>Variant Record for a single spatial attribute.</i>
      *
      * @return the time stamped <code>spatial</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::SpatialVariantStruct > getSpatialTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>relativeSpatial</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Relative spatial state stored in one variant record attribute.</i>
      *
      * @return true if <code>relativeSpatial</code> is available.
      */
      LIBAPI virtual bool hasRelativeSpatial() = 0;

      /**
      * Gets the value of the <code>relativeSpatial</code> attribute.
      *
      * <br>Description from the FOM: <i>Relative spatial state stored in one variant record attribute.</i>
      * <br>Description of the data type from the FOM: <i>Variant Record for a single spatial attribute.</i>
      *
      * @return the <code>relativeSpatial</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::SpatialVariantStruct getRelativeSpatial()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>relativeSpatial</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Relative spatial state stored in one variant record attribute.</i>
      * <br>Description of the data type from the FOM: <i>Variant Record for a single spatial attribute.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>relativeSpatial</code> attribute.
      */
      LIBAPI virtual DevStudio::SpatialVariantStruct getRelativeSpatial(DevStudio::SpatialVariantStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>relativeSpatial</code> attribute.
      * <br>Description from the FOM: <i>Relative spatial state stored in one variant record attribute.</i>
      * <br>Description of the data type from the FOM: <i>Variant Record for a single spatial attribute.</i>
      *
      * @return the time stamped <code>relativeSpatial</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::SpatialVariantStruct > getRelativeSpatialTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
   };
}
#endif
