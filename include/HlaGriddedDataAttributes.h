/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAGRIDDEDDATAATTRIBUTES_H
#define DEVELOPER_STUDIO_HLAGRIDDEDDATAATTRIBUTES_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <string>
#include <vector>
#include <utility>
    
#include <boost/noncopyable.hpp>
    
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EnvironmentDataCoordinateSystemEnum.h>
#include <DevStudio/datatypes/EnvironmentGridTypeEnum.h>
#include <DevStudio/datatypes/EnvironmentTypeStruct.h>
#include <DevStudio/datatypes/GridAxisStruct.h>
#include <DevStudio/datatypes/GridAxisStructLengthlessArray.h>
#include <DevStudio/datatypes/GridDataStruct.h>
#include <DevStudio/datatypes/GridDataStructLengthlessArray.h>
#include <DevStudio/datatypes/OrientationStruct.h>
#include <vector>

#include <DevStudio/HlaException.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaHLAobjectRootAttributes.h>
#include <DevStudio/HlaTimeStamped.h>

namespace DevStudio {

   /**
   * Interface used to represent all attributes for an object instance.
   */
   class HlaGriddedDataAttributes : public HlaHLAobjectRootAttributes {
   public:


     /**
      * An enumeration of the attributes of an HlaGriddedData
      *
      *<table summary="All attributes">
      * <tr><td><b>Enum constant</b></td><td><b>C++ name</b></td><td><b>FOM name</b></td></tr>
      * <tr><td>GRID_IDENTIFIER</td><td>gridIdentifier</td><td><code>GridIdentifier</code></td></tr>
      * <tr><td>COORDINATE_SYSTEM</td><td>coordinateSystem</td><td><code>CoordinateSystem</code></td></tr>
      * <tr><td>NUMBER_OF_GRID_AXES</td><td>numberOfGridAxes</td><td><code>NumberOfGridAxes</code></td></tr>
      * <tr><td>CONSTANT_GRID</td><td>constantGrid</td><td><code>ConstantGrid</code></td></tr>
      * <tr><td>ENVIRONMENT_TYPE</td><td>environmentType</td><td><code>EnvironmentType</code></td></tr>
      * <tr><td>ORIENTATION</td><td>orientation</td><td><code>Orientation</code></td></tr>
      * <tr><td>SAMPLE_TIME</td><td>sampleTime</td><td><code>SampleTime</code></td></tr>
      * <tr><td>TOTAL_VALUES</td><td>totalValues</td><td><code>TotalValues</code></td></tr>
      * <tr><td>VECTOR_DIMENSION</td><td>vectorDimension</td><td><code>VectorDimension</code></td></tr>
      * <tr><td>GRID_AXIS_INFO</td><td>gridAxisInfo</td><td><code>GridAxisInfo</code></td></tr>
      * <tr><td>GRID_DATA_INFO</td><td>gridDataInfo</td><td><code>GridDataInfo</code></td></tr>
      *</table>
      */
      enum Attribute {
        /**
        * gridIdentifier (FOM name: <code>GridIdentifier</code>).
        * <br>Description from the FOM: <i>Identifies the environmental simulation application</i>
        */
         GRID_IDENTIFIER,

        /**
        * coordinateSystem (FOM name: <code>CoordinateSystem</code>).
        * <br>Description from the FOM: <i>Specifies the coordinate system used to locate the data grid</i>
        */
         COORDINATE_SYSTEM,

        /**
        * numberOfGridAxes (FOM name: <code>NumberOfGridAxes</code>).
        * <br>Description from the FOM: <i>Specifies the number of grid axes used to define the data grid (e.g. three grid axes for an x, y, z coordinate system)</i>
        */
         NUMBER_OF_GRID_AXES,

        /**
        * constantGrid (FOM name: <code>ConstantGrid</code>).
        * <br>Description from the FOM: <i>Specifies whether the grid axes remain constant for the life of the data grid</i>
        */
         CONSTANT_GRID,

        /**
        * environmentType (FOM name: <code>EnvironmentType</code>).
        * <br>Description from the FOM: <i>Identifies the type of environmental entity being described</i>
        */
         ENVIRONMENT_TYPE,

        /**
        * orientation (FOM name: <code>Orientation</code>).
        * <br>Description from the FOM: <i>Specifies the orientation of the data grid, with Euler angles</i>
        */
         ORIENTATION,

        /**
        * sampleTime (FOM name: <code>SampleTime</code>).
        * <br>Description from the FOM: <i>Specifies the valid time of the environmental data sample</i>
        */
         SAMPLE_TIME,

        /**
        * totalValues (FOM name: <code>TotalValues</code>).
        * <br>Description from the FOM: <i>Specifies the number of data values that make up this grid</i>
        */
         TOTAL_VALUES,

        /**
        * vectorDimension (FOM name: <code>VectorDimension</code>).
        * <br>Description from the FOM: <i>Specifies the number of data values at each grid point ; VectorDimension shall be one for scalar data, and shall be greater than one when multiple enumerated environmental data values are sent for each grid point (e.g. u, v, w wind components have VectorDimension = 3)</i>
        */
         VECTOR_DIMENSION,

        /**
        * gridAxisInfo (FOM name: <code>GridAxisInfo</code>).
        * <br>Description from the FOM: <i>Specifies information on grid axes</i>
        */
         GRID_AXIS_INFO,

        /**
        * gridDataInfo (FOM name: <code>GridDataInfo</code>).
        * <br>Description from the FOM: <i>Specifies information on grid data representations</i>
        */
         GRID_DATA_INFO
      };

     /**
      * Gets the name of the attribute.
      *
      * @return The name of the attribute. An empty string will be returned if the attribute does not exist.
      */
      LIBAPI virtual std::wstring getName(Attribute attribute);

     /**
      * Finds the attribute specified in the parameter attributeName.
      * The found enumeration will be stored in the parameter attribute.
      *
      * @return true if the attribute was found, false otherwise.
      */
      LIBAPI virtual bool find(Attribute& attribute, std::wstring attributeName);

      LIBAPI virtual ~HlaGriddedDataAttributes() {}
    
      /**
      * Returns true if the <code>gridIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Identifies the environmental simulation application</i>
      *
      * @return true if <code>gridIdentifier</code> is available.
      */
      LIBAPI virtual bool hasGridIdentifier() = 0;

      /**
      * Gets the value of the <code>gridIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>Identifies the environmental simulation application</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @return the <code>gridIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::EntityIdentifierStruct getGridIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>gridIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Identifies the environmental simulation application</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>gridIdentifier</code> attribute.
      */
      LIBAPI virtual DevStudio::EntityIdentifierStruct getGridIdentifier(DevStudio::EntityIdentifierStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>gridIdentifier</code> attribute.
      * <br>Description from the FOM: <i>Identifies the environmental simulation application</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @return the time stamped <code>gridIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::EntityIdentifierStruct > getGridIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>coordinateSystem</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies the coordinate system used to locate the data grid</i>
      *
      * @return true if <code>coordinateSystem</code> is available.
      */
      LIBAPI virtual bool hasCoordinateSystem() = 0;

      /**
      * Gets the value of the <code>coordinateSystem</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies the coordinate system used to locate the data grid</i>
      * <br>Description of the data type from the FOM: <i>Environment data coordinate system</i>
      *
      * @return the <code>coordinateSystem</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::EnvironmentDataCoordinateSystemEnum::EnvironmentDataCoordinateSystemEnum getCoordinateSystem()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>coordinateSystem</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies the coordinate system used to locate the data grid</i>
      * <br>Description of the data type from the FOM: <i>Environment data coordinate system</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>coordinateSystem</code> attribute.
      */
      LIBAPI virtual DevStudio::EnvironmentDataCoordinateSystemEnum::EnvironmentDataCoordinateSystemEnum getCoordinateSystem(DevStudio::EnvironmentDataCoordinateSystemEnum::EnvironmentDataCoordinateSystemEnum defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>coordinateSystem</code> attribute.
      * <br>Description from the FOM: <i>Specifies the coordinate system used to locate the data grid</i>
      * <br>Description of the data type from the FOM: <i>Environment data coordinate system</i>
      *
      * @return the time stamped <code>coordinateSystem</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::EnvironmentDataCoordinateSystemEnum::EnvironmentDataCoordinateSystemEnum > getCoordinateSystemTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>numberOfGridAxes</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies the number of grid axes used to define the data grid (e.g. three grid axes for an x, y, z coordinate system)</i>
      *
      * @return true if <code>numberOfGridAxes</code> is available.
      */
      LIBAPI virtual bool hasNumberOfGridAxes() = 0;

      /**
      * Gets the value of the <code>numberOfGridAxes</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies the number of grid axes used to define the data grid (e.g. three grid axes for an x, y, z coordinate system)</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the <code>numberOfGridAxes</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual char getNumberOfGridAxes()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>numberOfGridAxes</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies the number of grid axes used to define the data grid (e.g. three grid axes for an x, y, z coordinate system)</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>numberOfGridAxes</code> attribute.
      */
      LIBAPI virtual char getNumberOfGridAxes(char defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>numberOfGridAxes</code> attribute.
      * <br>Description from the FOM: <i>Specifies the number of grid axes used to define the data grid (e.g. three grid axes for an x, y, z coordinate system)</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the time stamped <code>numberOfGridAxes</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< char > getNumberOfGridAxesTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>constantGrid</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies whether the grid axes remain constant for the life of the data grid</i>
      *
      * @return true if <code>constantGrid</code> is available.
      */
      LIBAPI virtual bool hasConstantGrid() = 0;

      /**
      * Gets the value of the <code>constantGrid</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies whether the grid axes remain constant for the life of the data grid</i>
      * <br>Description of the data type from the FOM: <i>Environment data grid type</i>
      *
      * @return the <code>constantGrid</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::EnvironmentGridTypeEnum::EnvironmentGridTypeEnum getConstantGrid()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>constantGrid</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies whether the grid axes remain constant for the life of the data grid</i>
      * <br>Description of the data type from the FOM: <i>Environment data grid type</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>constantGrid</code> attribute.
      */
      LIBAPI virtual DevStudio::EnvironmentGridTypeEnum::EnvironmentGridTypeEnum getConstantGrid(DevStudio::EnvironmentGridTypeEnum::EnvironmentGridTypeEnum defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>constantGrid</code> attribute.
      * <br>Description from the FOM: <i>Specifies whether the grid axes remain constant for the life of the data grid</i>
      * <br>Description of the data type from the FOM: <i>Environment data grid type</i>
      *
      * @return the time stamped <code>constantGrid</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::EnvironmentGridTypeEnum::EnvironmentGridTypeEnum > getConstantGridTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>environmentType</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Identifies the type of environmental entity being described</i>
      *
      * @return true if <code>environmentType</code> is available.
      */
      LIBAPI virtual bool hasEnvironmentType() = 0;

      /**
      * Gets the value of the <code>environmentType</code> attribute.
      *
      * <br>Description from the FOM: <i>Identifies the type of environmental entity being described</i>
      * <br>Description of the data type from the FOM: <i>Record specifying the kind of environment, the domain and any extra information necessary for describing the environmental entity</i>
      *
      * @return the <code>environmentType</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::EnvironmentTypeStruct getEnvironmentType()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>environmentType</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Identifies the type of environmental entity being described</i>
      * <br>Description of the data type from the FOM: <i>Record specifying the kind of environment, the domain and any extra information necessary for describing the environmental entity</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>environmentType</code> attribute.
      */
      LIBAPI virtual DevStudio::EnvironmentTypeStruct getEnvironmentType(DevStudio::EnvironmentTypeStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>environmentType</code> attribute.
      * <br>Description from the FOM: <i>Identifies the type of environmental entity being described</i>
      * <br>Description of the data type from the FOM: <i>Record specifying the kind of environment, the domain and any extra information necessary for describing the environmental entity</i>
      *
      * @return the time stamped <code>environmentType</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::EnvironmentTypeStruct > getEnvironmentTypeTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>orientation</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies the orientation of the data grid, with Euler angles</i>
      *
      * @return true if <code>orientation</code> is available.
      */
      LIBAPI virtual bool hasOrientation() = 0;

      /**
      * Gets the value of the <code>orientation</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies the orientation of the data grid, with Euler angles</i>
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @return the <code>orientation</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::OrientationStruct getOrientation()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>orientation</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies the orientation of the data grid, with Euler angles</i>
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>orientation</code> attribute.
      */
      LIBAPI virtual DevStudio::OrientationStruct getOrientation(DevStudio::OrientationStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>orientation</code> attribute.
      * <br>Description from the FOM: <i>Specifies the orientation of the data grid, with Euler angles</i>
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @return the time stamped <code>orientation</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::OrientationStruct > getOrientationTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>sampleTime</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies the valid time of the environmental data sample</i>
      *
      * @return true if <code>sampleTime</code> is available.
      */
      LIBAPI virtual bool hasSampleTime() = 0;

      /**
      * Gets the value of the <code>sampleTime</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies the valid time of the environmental data sample</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^64-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the <code>sampleTime</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual unsigned long long getSampleTime()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>sampleTime</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies the valid time of the environmental data sample</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^64-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>sampleTime</code> attribute.
      */
      LIBAPI virtual unsigned long long getSampleTime(unsigned long long defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>sampleTime</code> attribute.
      * <br>Description from the FOM: <i>Specifies the valid time of the environmental data sample</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^64-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the time stamped <code>sampleTime</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< unsigned long long > getSampleTimeTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>totalValues</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies the number of data values that make up this grid</i>
      *
      * @return true if <code>totalValues</code> is available.
      */
      LIBAPI virtual bool hasTotalValues() = 0;

      /**
      * Gets the value of the <code>totalValues</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies the number of data values that make up this grid</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the <code>totalValues</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual unsigned int getTotalValues()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>totalValues</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies the number of data values that make up this grid</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>totalValues</code> attribute.
      */
      LIBAPI virtual unsigned int getTotalValues(unsigned int defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>totalValues</code> attribute.
      * <br>Description from the FOM: <i>Specifies the number of data values that make up this grid</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the time stamped <code>totalValues</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< unsigned int > getTotalValuesTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>vectorDimension</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies the number of data values at each grid point ; VectorDimension shall be one for scalar data, and shall be greater than one when multiple enumerated environmental data values are sent for each grid point (e.g. u, v, w wind components have VectorDimension = 3)</i>
      *
      * @return true if <code>vectorDimension</code> is available.
      */
      LIBAPI virtual bool hasVectorDimension() = 0;

      /**
      * Gets the value of the <code>vectorDimension</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies the number of data values at each grid point ; VectorDimension shall be one for scalar data, and shall be greater than one when multiple enumerated environmental data values are sent for each grid point (e.g. u, v, w wind components have VectorDimension = 3)</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the <code>vectorDimension</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual char getVectorDimension()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>vectorDimension</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies the number of data values at each grid point ; VectorDimension shall be one for scalar data, and shall be greater than one when multiple enumerated environmental data values are sent for each grid point (e.g. u, v, w wind components have VectorDimension = 3)</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>vectorDimension</code> attribute.
      */
      LIBAPI virtual char getVectorDimension(char defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>vectorDimension</code> attribute.
      * <br>Description from the FOM: <i>Specifies the number of data values at each grid point ; VectorDimension shall be one for scalar data, and shall be greater than one when multiple enumerated environmental data values are sent for each grid point (e.g. u, v, w wind components have VectorDimension = 3)</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the time stamped <code>vectorDimension</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< char > getVectorDimensionTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>gridAxisInfo</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies information on grid axes</i>
      *
      * @return true if <code>gridAxisInfo</code> is available.
      */
      LIBAPI virtual bool hasGridAxisInfo() = 0;

      /**
      * Gets the value of the <code>gridAxisInfo</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies information on grid axes</i>
      * <br>Description of the data type from the FOM: <i>Specifies detailed information for a collection of grid axes</i>
      *
      * @return the <code>gridAxisInfo</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::vector<DevStudio::GridAxisStruct > getGridAxisInfo()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>gridAxisInfo</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies information on grid axes</i>
      * <br>Description of the data type from the FOM: <i>Specifies detailed information for a collection of grid axes</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>gridAxisInfo</code> attribute.
      */
      LIBAPI virtual std::vector<DevStudio::GridAxisStruct > getGridAxisInfo(std::vector<DevStudio::GridAxisStruct > defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>gridAxisInfo</code> attribute.
      * <br>Description from the FOM: <i>Specifies information on grid axes</i>
      * <br>Description of the data type from the FOM: <i>Specifies detailed information for a collection of grid axes</i>
      *
      * @return the time stamped <code>gridAxisInfo</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::vector<DevStudio::GridAxisStruct > > getGridAxisInfoTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>gridDataInfo</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies information on grid data representations</i>
      *
      * @return true if <code>gridDataInfo</code> is available.
      */
      LIBAPI virtual bool hasGridDataInfo() = 0;

      /**
      * Gets the value of the <code>gridDataInfo</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies information on grid data representations</i>
      * <br>Description of the data type from the FOM: <i>Specifies detailed information for a collection of grid data representations</i>
      *
      * @return the <code>gridDataInfo</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::vector<DevStudio::GridDataStruct > getGridDataInfo()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>gridDataInfo</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies information on grid data representations</i>
      * <br>Description of the data type from the FOM: <i>Specifies detailed information for a collection of grid data representations</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>gridDataInfo</code> attribute.
      */
      LIBAPI virtual std::vector<DevStudio::GridDataStruct > getGridDataInfo(std::vector<DevStudio::GridDataStruct > defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>gridDataInfo</code> attribute.
      * <br>Description from the FOM: <i>Specifies information on grid data representations</i>
      * <br>Description of the data type from the FOM: <i>Specifies detailed information for a collection of grid data representations</i>
      *
      * @return the time stamped <code>gridDataInfo</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::vector<DevStudio::GridDataStruct > > getGridDataInfoTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
   };
}
#endif
