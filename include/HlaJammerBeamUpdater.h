/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAJAMMERBEAMUPDATER_H
#define DEVELOPER_STUDIO_HLAJAMMERBEAMUPDATER_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <boost/noncopyable.hpp>

#include <DevStudio/datatypes/BeamFunctionCodeEnum.h>
#include <DevStudio/datatypes/EventIdentifierStruct.h>
#include <DevStudio/datatypes/RTIobjectIdArray.h>
#include <string>
#include <vector>

#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaException.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaEmitterBeamUpdater.h>

namespace DevStudio {

    /**
    * Updater used to update attribute values.
    */
    class HlaJammerBeamUpdater : public HlaEmitterBeamUpdater {

    public:

    LIBAPI virtual ~HlaJammerBeamUpdater() {}

    /**
    * Set the jammingModeSequence for this update.
    * <br>Description from the FOM: <i>The jamming mode technique or series of techniques being applied.</i>
    * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
    *
    * @param jammingModeSequence the new jammingModeSequence
    */
    LIBAPI virtual void setJammingModeSequence(const unsigned int& jammingModeSequence) = 0;

    /**
    * Set the jammedObjectIdentifiers for this update.
    * <br>Description from the FOM: <i>Identification of the emitter beams being jammed.</i>
    * <br>Description of the data type from the FOM: <i>Set of ID's of registered object instances.</i>
    *
    * @param jammedObjectIdentifiers the new jammedObjectIdentifiers
    */
    LIBAPI virtual void setJammedObjectIdentifiers(const std::vector<std::string >& jammedObjectIdentifiers) = 0;

    /**
    * Set the highDensityJam for this update.
    * <br>Description from the FOM: <i>When TRUE the receiving simulation can assume that all emitter beams that are in the scan pattern of the jammer beam are being jammed</i>
    * <br>Description of the data type from the FOM: <i></i>
    *
    * @param highDensityJam the new highDensityJam
    */
    LIBAPI virtual void setHighDensityJam(const bool& highDensityJam) = 0;

    /**
    * Set the beamAzimuthCenter for this update.
    * <br>Description from the FOM: <i>The azimuth center of the emitter beam's scan volume relative to the emitter system.</i>
    * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
    *
    * @param beamAzimuthCenter the new beamAzimuthCenter
    */
    LIBAPI virtual void setBeamAzimuthCenter(const float& beamAzimuthCenter) = 0;

    /**
    * Set the beamAzimuthSweep for this update.
    * <br>Description from the FOM: <i>The azimuth half-angle of the emitter beam's scan volume relative to the emitter system.</i>
    * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
    *
    * @param beamAzimuthSweep the new beamAzimuthSweep
    */
    LIBAPI virtual void setBeamAzimuthSweep(const float& beamAzimuthSweep) = 0;

    /**
    * Set the beamElevationCenter for this update.
    * <br>Description from the FOM: <i>The elevation center of the emitter beam's scan volume relative to the emitter system.</i>
    * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
    *
    * @param beamElevationCenter the new beamElevationCenter
    */
    LIBAPI virtual void setBeamElevationCenter(const float& beamElevationCenter) = 0;

    /**
    * Set the beamElevationSweep for this update.
    * <br>Description from the FOM: <i>The elevation half-angle of the emitter beam's scan volume relative to the emitter system.</i>
    * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
    *
    * @param beamElevationSweep the new beamElevationSweep
    */
    LIBAPI virtual void setBeamElevationSweep(const float& beamElevationSweep) = 0;

    /**
    * Set the beamFunctionCode for this update.
    * <br>Description from the FOM: <i>The function of the emitter beam.</i>
    * <br>Description of the data type from the FOM: <i>Beam function</i>
    *
    * @param beamFunctionCode the new beamFunctionCode
    */
    LIBAPI virtual void setBeamFunctionCode(const DevStudio::BeamFunctionCodeEnum::BeamFunctionCodeEnum& beamFunctionCode) = 0;

    /**
    * Set the beamIdentifier for this update.
    * <br>Description from the FOM: <i>The identification of the emitter beam (must be unique on the emitter system).</i>
    * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
    *
    * @param beamIdentifier the new beamIdentifier
    */
    LIBAPI virtual void setBeamIdentifier(const char& beamIdentifier) = 0;

    /**
    * Set the beamParameterIndex for this update.
    * <br>Description from the FOM: <i>The index, into the federation specific emissions database, of the current operating mode of the emitter beam.</i>
    * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
    *
    * @param beamParameterIndex the new beamParameterIndex
    */
    LIBAPI virtual void setBeamParameterIndex(const unsigned short& beamParameterIndex) = 0;

    /**
    * Set the effectiveRadiatedPower for this update.
    * <br>Description from the FOM: <i>The effective radiated power of the emitter beam.</i>
    * <br>Description of the data type from the FOM: <i>Power ratio in decibels (dB) of a measured power referenced to 1 milliwatt (mW). [unit: decibel milliwatt (dBm), resolution: NA, accuracy: perfect]</i>
    *
    * @param effectiveRadiatedPower the new effectiveRadiatedPower
    */
    LIBAPI virtual void setEffectiveRadiatedPower(const float& effectiveRadiatedPower) = 0;

    /**
    * Set the emissionFrequency for this update.
    * <br>Description from the FOM: <i>The center frequency of the emitter beam.</i>
    * <br>Description of the data type from the FOM: <i>Frequency, based on SI derived unit hertz, unit symbol Hz. [unit: hertz (Hz), resolution: NA, accuracy: NA]</i>
    *
    * @param emissionFrequency the new emissionFrequency
    */
    LIBAPI virtual void setEmissionFrequency(const float& emissionFrequency) = 0;

    /**
    * Set the emitterSystemIdentifier for this update.
    * <br>Description from the FOM: <i>The identification of the emitter system that is generating this emitter beam.</i>
    * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
    *
    * @param emitterSystemIdentifier the new emitterSystemIdentifier
    */
    LIBAPI virtual void setEmitterSystemIdentifier(const std::string& emitterSystemIdentifier) = 0;

    /**
    * Set the eventIdentifier for this update.
    * <br>Description from the FOM: <i>The EventIdentifier is used by the generating federate to associate related events. The event number shall start at one at the beginning of the exercise, and be incremented by one for each event.</i>
    * <br>Description of the data type from the FOM: <i>Identification of an event. Based on the Event Identifier record as specified in IEEE 1278.1-1995 section 5.2.18.</i>
    *
    * @param eventIdentifier the new eventIdentifier
    */
    LIBAPI virtual void setEventIdentifier(const DevStudio::EventIdentifierStruct& eventIdentifier) = 0;

    /**
    * Set the frequencyRange for this update.
    * <br>Description from the FOM: <i>The bandwidth of the frequencies covered by the emitter beam.</i>
    * <br>Description of the data type from the FOM: <i>Frequency, based on SI derived unit hertz, unit symbol Hz. [unit: hertz (Hz), resolution: NA, accuracy: NA]</i>
    *
    * @param frequencyRange the new frequencyRange
    */
    LIBAPI virtual void setFrequencyRange(const float& frequencyRange) = 0;

    /**
    * Set the pulseRepetitionFrequency for this update.
    * <br>Description from the FOM: <i>The Pulse Repetition Frequency of the emitter beam.</i>
    * <br>Description of the data type from the FOM: <i>Frequency, based on SI derived unit hertz, unit symbol Hz. [unit: hertz (Hz), resolution: NA, accuracy: NA]</i>
    *
    * @param pulseRepetitionFrequency the new pulseRepetitionFrequency
    */
    LIBAPI virtual void setPulseRepetitionFrequency(const float& pulseRepetitionFrequency) = 0;

    /**
    * Set the pulseWidth for this update.
    * <br>Description from the FOM: <i>The pulse width of the emitter beam.</i>
    * <br>Description of the data type from the FOM: <i>Time, based on SI base unit second, expressed in microsecond, unit symbol μs. [unit: microsecond, resolution: NA, accuracy: NA]</i>
    *
    * @param pulseWidth the new pulseWidth
    */
    LIBAPI virtual void setPulseWidth(const float& pulseWidth) = 0;

    /**
    * Set the sweepSynch for this update.
    * <br>Description from the FOM: <i>The percentage of time a scan is through its pattern from its origin.</i>
    * <br>Description of the data type from the FOM: <i>Percentage [unit: percent (%), resolution: NA, accuracy: NA]</i>
    *
    * @param sweepSynch the new sweepSynch
    */
    LIBAPI virtual void setSweepSynch(const float& sweepSynch) = 0;

    /**
    * Send all the attributes.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate()
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

    /**
    * Send all the attributes with a specified timestamp.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @param timestamp timestamp to send for this update
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate(HlaTimeStampPtr timestamp)
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

    /**
    * Send all the attributes with a specified timestamp.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @param logicalTime logical time to send for this update
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaInvalidLogicalTimeException if the logical time is not valid.
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate(HlaLogicalTimePtr logicalTime)
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaInvalidLogicalTimeException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

    /**
    * Send all the attributes with a specified timestamp.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @param timestamp timestamp to send for this update
    * @param logicalTime logical time to send for this update
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaInvalidLogicalTimeException if the logical time is not valid.
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate(HlaTimeStampPtr timestamp, HlaLogicalTimePtr logicalTime)
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaInvalidLogicalTimeException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;
    };
}
#endif
