/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLASTRUCTUREOBJECTVALUELISTENER_H
#define DEVELOPER_STUDIO_HLASTRUCTUREOBJECTVALUELISTENER_H

#include <memory>

#include <DevStudio/datatypes/DamageStatusEnum.h>
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EnvironmentObjectTypeStruct.h>
#include <DevStudio/datatypes/ForceIdentifierEnum.h>
#include <DevStudio/datatypes/OrientationStruct.h>
#include <DevStudio/datatypes/WorldLocationStruct.h>
#include <string>

#include <DevStudio/HlaLogicalTime.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaStructureObjectAttributes.h>    

namespace DevStudio {

   /**
   * Listener for updates of attributes, with the new updated values.  
   */
   class HlaStructureObjectValueListener {

   public:

      LIBAPI virtual ~HlaStructureObjectValueListener() {}
    
      /**
      * This method is called when the attribute <code>location</code> is updated.
      * <br>Description from the FOM: <i>Specifies the location of the object based on x, y and z coordinates</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @param structureObject The object which is updated.
      * @param location The new value of the attribute in this update
      * @param validOldLocation True if oldLocation contains a valid value
      * @param oldLocation The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void locationUpdated(HlaStructureObjectPtr structureObject, WorldLocationStruct location, bool validOldLocation, WorldLocationStruct oldLocation, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>orientation</code> is updated.
      * <br>Description from the FOM: <i>Specifies the angles of rotation around the coordinate axis between the object's attitude and the reference coordinate system axes ; these are calculated as the Tait-Bryan Euler angles, specifying the successive rotations needed to transform from the world coordinate system to the object coordinate system</i>
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @param structureObject The object which is updated.
      * @param orientation The new value of the attribute in this update
      * @param validOldOrientation True if oldOrientation contains a valid value
      * @param oldOrientation The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void orientationUpdated(HlaStructureObjectPtr structureObject, OrientationStruct orientation, bool validOldOrientation, OrientationStruct oldOrientation, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>percentComplete</code> is updated.
      * <br>Description from the FOM: <i>Specifies the percent completion of the object</i>
      * <br>Description of the data type from the FOM: <i>Percentage [unit: percent (%), resolution: 1, accuracy: perfect]</i>
      *
      * @param structureObject The object which is updated.
      * @param percentComplete The new value of the attribute in this update
      * @param validOldPercentComplete True if oldPercentComplete contains a valid value
      * @param oldPercentComplete The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void percentCompleteUpdated(HlaStructureObjectPtr structureObject, unsigned int percentComplete, bool validOldPercentComplete, unsigned int oldPercentComplete, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>damagedAppearance</code> is updated.
      * <br>Description from the FOM: <i>Specifies the damaged appearance of the object instance</i>
      * <br>Description of the data type from the FOM: <i>Damaged appearance</i>
      *
      * @param structureObject The object which is updated.
      * @param damagedAppearance The new value of the attribute in this update
      * @param validOldDamagedAppearance True if oldDamagedAppearance contains a valid value
      * @param oldDamagedAppearance The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void damagedAppearanceUpdated(HlaStructureObjectPtr structureObject, DamageStatusEnum::DamageStatusEnum damagedAppearance, bool validOldDamagedAppearance, DamageStatusEnum::DamageStatusEnum oldDamagedAppearance, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>objectPreDistributed</code> is updated.
      * <br>Description from the FOM: <i>Specifies whether or not the object was created before the start of the exercise</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param structureObject The object which is updated.
      * @param objectPreDistributed The new value of the attribute in this update
      * @param validOldObjectPreDistributed True if oldObjectPreDistributed contains a valid value
      * @param oldObjectPreDistributed The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void objectPreDistributedUpdated(HlaStructureObjectPtr structureObject, bool objectPreDistributed, bool validOldObjectPreDistributed, bool oldObjectPreDistributed, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>deactivated</code> is updated.
      * <br>Description from the FOM: <i>Specifies whether or not the object has been deactivated (it has ceased to exist in the synthetic environment)</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param structureObject The object which is updated.
      * @param deactivated The new value of the attribute in this update
      * @param validOldDeactivated True if oldDeactivated contains a valid value
      * @param oldDeactivated The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void deactivatedUpdated(HlaStructureObjectPtr structureObject, bool deactivated, bool validOldDeactivated, bool oldDeactivated, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>smoking</code> is updated.
      * <br>Description from the FOM: <i>Specifies whether or not the object is smoking (creating a smoke plume)</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param structureObject The object which is updated.
      * @param smoking The new value of the attribute in this update
      * @param validOldSmoking True if oldSmoking contains a valid value
      * @param oldSmoking The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void smokingUpdated(HlaStructureObjectPtr structureObject, bool smoking, bool validOldSmoking, bool oldSmoking, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>flaming</code> is updated.
      * <br>Description from the FOM: <i>Specifies whether or not the object is aflame</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param structureObject The object which is updated.
      * @param flaming The new value of the attribute in this update
      * @param validOldFlaming True if oldFlaming contains a valid value
      * @param oldFlaming The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void flamingUpdated(HlaStructureObjectPtr structureObject, bool flaming, bool validOldFlaming, bool oldFlaming, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>objectIdentifier</code> is updated.
      * <br>Description from the FOM: <i>Identifies this EnvironmentObject instance (point, linear or areal)</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @param structureObject The object which is updated.
      * @param objectIdentifier The new value of the attribute in this update
      * @param validOldObjectIdentifier True if oldObjectIdentifier contains a valid value
      * @param oldObjectIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void objectIdentifierUpdated(HlaStructureObjectPtr structureObject, EntityIdentifierStruct objectIdentifier, bool validOldObjectIdentifier, EntityIdentifierStruct oldObjectIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>referencedObjectIdentifier</code> is updated.
      * <br>Description from the FOM: <i>Identifies the Synthetic Environment object instance to which this EnvironmentObject instance is associated</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @param structureObject The object which is updated.
      * @param referencedObjectIdentifier The new value of the attribute in this update
      * @param validOldReferencedObjectIdentifier True if oldReferencedObjectIdentifier contains a valid value
      * @param oldReferencedObjectIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void referencedObjectIdentifierUpdated(HlaStructureObjectPtr structureObject, std::string referencedObjectIdentifier, bool validOldReferencedObjectIdentifier, std::string oldReferencedObjectIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>forceIdentifier</code> is updated.
      * <br>Description from the FOM: <i>Identifies the force that created or modified this EnvironmentObject instance</i>
      * <br>Description of the data type from the FOM: <i>Force ID</i>
      *
      * @param structureObject The object which is updated.
      * @param forceIdentifier The new value of the attribute in this update
      * @param validOldForceIdentifier True if oldForceIdentifier contains a valid value
      * @param oldForceIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void forceIdentifierUpdated(HlaStructureObjectPtr structureObject, ForceIdentifierEnum::ForceIdentifierEnum forceIdentifier, bool validOldForceIdentifier, ForceIdentifierEnum::ForceIdentifierEnum oldForceIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>objectType</code> is updated.
      * <br>Description from the FOM: <i>Identifies the type of this EnvironmentObject instance</i>
      * <br>Description of the data type from the FOM: <i>Record specifying the domain, the kind and the specific identification of the environment object</i>
      *
      * @param structureObject The object which is updated.
      * @param objectType The new value of the attribute in this update
      * @param validOldObjectType True if oldObjectType contains a valid value
      * @param oldObjectType The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void objectTypeUpdated(HlaStructureObjectPtr structureObject, EnvironmentObjectTypeStruct objectType, bool validOldObjectType, EnvironmentObjectTypeStruct oldObjectType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
      /**
      * This method is called when the producing federate of an attribute is changed.
      * Only available when using HLA Evolved.
      *
      * @param structureObject The object which is updated.
      * @param attribute The attribute that now has a new producing federate
      * @param oldProducingFederate The federate handle of the old producing federate
      * @param newProducingFederate The federate handle of the new producing federate
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void producingFederateUpdated(HlaStructureObjectPtr structureObject, HlaStructureObjectAttributes::Attribute attribute, HlaFederateIdPtr oldProducingFederate, HlaFederateIdPtr newProducingFederate, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;

      class Adapter;
   };

   /**
   * An adapter class that implements the HlaStructureObjectValueListener interface with empty methods.
   * It might be used as a base class for a listener.
   */
   class HlaStructureObjectValueListener::Adapter : public HlaStructureObjectValueListener {

   public:

      LIBAPI virtual void locationUpdated(HlaStructureObjectPtr structureObject, WorldLocationStruct location, bool validOldLocation, WorldLocationStruct oldLocation, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void orientationUpdated(HlaStructureObjectPtr structureObject, OrientationStruct orientation, bool validOldOrientation, OrientationStruct oldOrientation, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void percentCompleteUpdated(HlaStructureObjectPtr structureObject, unsigned int percentComplete, bool validOldPercentComplete, unsigned int oldPercentComplete, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void damagedAppearanceUpdated(HlaStructureObjectPtr structureObject, DamageStatusEnum::DamageStatusEnum damagedAppearance, bool validOldDamagedAppearance, DamageStatusEnum::DamageStatusEnum oldDamagedAppearance, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void objectPreDistributedUpdated(HlaStructureObjectPtr structureObject, bool objectPreDistributed, bool validOldObjectPreDistributed, bool oldObjectPreDistributed, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void deactivatedUpdated(HlaStructureObjectPtr structureObject, bool deactivated, bool validOldDeactivated, bool oldDeactivated, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void smokingUpdated(HlaStructureObjectPtr structureObject, bool smoking, bool validOldSmoking, bool oldSmoking, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void flamingUpdated(HlaStructureObjectPtr structureObject, bool flaming, bool validOldFlaming, bool oldFlaming, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void objectIdentifierUpdated(HlaStructureObjectPtr structureObject, EntityIdentifierStruct objectIdentifier, bool validOldObjectIdentifier, EntityIdentifierStruct oldObjectIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void referencedObjectIdentifierUpdated(HlaStructureObjectPtr structureObject, std::string referencedObjectIdentifier, bool validOldReferencedObjectIdentifier, std::string oldReferencedObjectIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void forceIdentifierUpdated(HlaStructureObjectPtr structureObject, ForceIdentifierEnum::ForceIdentifierEnum forceIdentifier, bool validOldForceIdentifier, ForceIdentifierEnum::ForceIdentifierEnum oldForceIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void objectTypeUpdated(HlaStructureObjectPtr structureObject, EnvironmentObjectTypeStruct objectType, bool validOldObjectType, EnvironmentObjectTypeStruct oldObjectType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
      LIBAPI virtual void producingFederateUpdated(HlaStructureObjectPtr structureObject, HlaStructureObjectAttributes::Attribute attribute, HlaFederateIdPtr oldProducingFederate, HlaFederateIdPtr newProducingFederate, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
   };

}
#endif
