/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAACTIVESONARMANAGER_H
#define DEVELOPER_STUDIO_HLAACTIVESONARMANAGER_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <DevStudio/datatypes/ActiveSonarEnum.h>
#include <DevStudio/datatypes/ActiveSonarFunctionCodeEnum.h>
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EventIdentifierStruct.h>
#include <DevStudio/datatypes/RelativePositionStruct.h>
#include <string>

#include <list>
#include <vector>

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaActiveSonarManagerListener.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaException.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

   /**
   * Manager used to manage HlaActiveSonars.
   */
   class HlaActiveSonarManager : private boost::noncopyable {

   public:

      LIBAPI virtual ~HlaActiveSonarManager() {}

      /**
      * Gets a list of all HlaActiveSonars within interest, both local and remote.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of HlaActiveSonars
      */
      LIBAPI virtual std::list<HlaActiveSonarPtr> getHlaActiveSonars() = 0;

      /**
      * Gets a list of all HlaActiveSonars, both local and remote.
      * HlaActiveSonars not within interest are also included.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of all HlaActiveSonars
      */
      LIBAPI virtual std::list<HlaActiveSonarPtr> getAllHlaActiveSonars() = 0;

      /**
      * Gets a list of local HlaActiveSonars within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of local HlaActiveSonars
      */
      LIBAPI virtual std::list<HlaActiveSonarPtr> getLocalHlaActiveSonars() = 0;

      /**
      * Gets a list of remote HlaActiveSonars within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of remote HlaActiveSonars
      */
      LIBAPI virtual std::list<HlaActiveSonarPtr> getRemoteHlaActiveSonars() = 0;

      /**
      * Find a HlaActiveSonar with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> of the HlaActiveSonar to find
      *
      * @return the specified HlaActiveSonar, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaActiveSonarPtr getActiveSonarByHlaInstanceName(const std::wstring& hlaInstanceName) = 0;

      /**
      * Find a HlaActiveSonar with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param encodedHlaInstanceHandle the encoded <code>HLA instance handle</code> of the HlaActiveSonar to find
      *
      * @return the specified HlaActiveSonar, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaActiveSonarPtr getActiveSonarByHlaInstanceHandle(const std::vector<char>& encodedHlaInstanceHandle) = 0;

      /**
      * Creates a new HlaActiveSonar, and sets all <i>Create</i> attributes.
      *
      * @return Returns a new local HlaActiveSonar.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaActiveSonarPtr createLocalHlaActiveSonar(
      ) THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Creates a new HlaActiveSonar with a specified <code>HLA instance name</code>, and sets all
      * <i>Create</i> attributes.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> to register for this instance
      *
      * @return Returns a new local HlaActiveSonar.
      *
      * @throws HlaIllegalInstanceNameException if the name is illegal
      * @throws HlaInstanceNameInUseException if the name is already in use
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaActiveSonarPtr createLocalHlaActiveSonar(
         const std::wstring& hlaInstanceName
      ) THROW_SPEC (HlaIllegalInstanceNameException, HlaInstanceNameInUseException,
                    HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaActiveSonar and removes it from the federation.
      *
      * @param activeSonar The HlaActiveSonar to delete
      *
      * @return the HlaActiveSonar deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaActiveSonarPtr deleteLocalHlaActiveSonar(HlaActiveSonarPtr activeSonar)
          THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                      HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaActiveSonar and removes it from the federation.
      *
      * @param activeSonar The HlaActiveSonar to delete
      * @param timestamp timestamp when the instance was removed.
      *
      * @return the HlaActiveSonar deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaActiveSonarPtr deleteLocalHlaActiveSonar(HlaActiveSonarPtr activeSonar, HlaTimeStampPtr timestamp)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaActiveSonar and removes it from the federation.
      *
      * @param activeSonar The HlaActiveSonar to delete
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaActiveSonar deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaActiveSonarPtr deleteLocalHlaActiveSonar(HlaActiveSonarPtr activeSonar, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaActiveSonar and removes it from the federation.
      *
      * @param activeSonar The HlaActiveSonar to delete
      * @param timestamp timestamp when the instance was removed.
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaActiveSonar deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaActiveSonarPtr deleteLocalHlaActiveSonar(HlaActiveSonarPtr activeSonar, HlaTimeStampPtr timestamp, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Add a HlaActiveSonar manager listener.
      *
      * @param managerListener listener to add
      */
      LIBAPI virtual void addHlaActiveSonarManagerListener(HlaActiveSonarManagerListenerPtr managerListener) = 0;

      /**
      * Removed a HlaActiveSonar manager listener.
      *
      * @param managerListener listener to remove
      */
      LIBAPI virtual void removeHlaActiveSonarManagerListener(HlaActiveSonarManagerListenerPtr managerListener) = 0;

     /**
      * Add a listener that will automatically be added to all instances of HlaActiveSonar (Both local and remote).
      * The listener is notified when any attribute of an instance of HlaActiveSonar is updated.
      * The listener is also called when an interaction is sent to an instance of HlaActiveSonar.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param listener The listener to add.
      */
      LIBAPI virtual void addHlaActiveSonarDefaultInstanceListener(HlaActiveSonarListenerPtr listener) = 0;

     /**
      * Remove a listener from the set of listeners that are automatically added to new instances of HlaActiveSonar.
      * Note: The listener will not be removed from already existing instances of HlaActiveSonar.
      * This method is idempotent.
      *
      * @param listener The listener to remove.
      */
      LIBAPI virtual void removeHlaActiveSonarDefaultInstanceListener(HlaActiveSonarListenerPtr listener) = 0;

     /**
      * Add a valueListener that will automatically be added to all instances of HlaActiveSonar (Both local and remote).
      * The  Valuelistener is notified when any attribute of an instance of HlaActiveSonar is updated.
      * Note that the attribute values that are notified are not guaranteed to
      * be the latest available values for that attribute.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to add.
      */
      LIBAPI virtual void addHlaActiveSonarDefaultInstanceValueListener(HlaActiveSonarValueListenerPtr valueListener) = 0;

     /**
      * Remove a valueListener from the set of listeners that are automatically added to new instances of HlaActiveSonar.
      * Note: The valueListener will not be removed from already existing instances of HlaActiveSonar.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to remove.
      */
      LIBAPI virtual void removeHlaActiveSonarDefaultInstanceValueListener(HlaActiveSonarValueListenerPtr valueListener) = 0;

     /**
      * Enables or disables the HlaActiveSonar manager.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaActiveSonar manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      *
      * @param enabled <code>true</code> to enable the HlaActiveSonar manager, otherwise <code>false</code>
      */
      LIBAPI virtual void setEnabled(bool enabled) = 0;

     /**
      * Enables the HlaActiveSonar manager if it is avalable in the FOM at runtime.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaActiveSonar manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      */
      LIBAPI virtual void setEnabledIfAvailableInFom() = 0;

     /**
      * Check if the HlaActiveSonar manager is actually enabled when connected.
      * An HlaActiveSonar manager may be disabled with the <code>setEnabled(false)</code> function, or it may be configured
      * as <i>if available in the FOM</i> by the <code>setEnabledIfAvailableInFom()</code> function and
      * then not available in the FOM at runtime.
      * <p>
      * This method should only be used when connected, it will always return <code>false</code>
      * when not connected.
      *
      * @return <code>true</code> if connected and the HlaActiveSonar manager is enabled.
      */
      LIBAPI virtual bool isEnabled() = 0;
   };
}
#endif
