/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLABREACHABLELINEAROBJECTMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLABREACHABLELINEAROBJECTMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaBreachableLinearObject.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaBreachableLinearObject instances.
    */
    class HlaBreachableLinearObjectManagerListener {

    public:

        LIBAPI virtual ~HlaBreachableLinearObjectManagerListener() {}

        /**
        * This method is called when a new HlaBreachableLinearObject instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param breachableLinearObject the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaBreachableLinearObjectDiscovered(HlaBreachableLinearObjectPtr breachableLinearObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaBreachableLinearObject instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param breachableLinearObject the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaBreachableLinearObjectInitialized(HlaBreachableLinearObjectPtr breachableLinearObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaBreachableLinearObjectManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param breachableLinearObject the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaBreachableLinearObjectInInterest(HlaBreachableLinearObjectPtr breachableLinearObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaBreachableLinearObjectManagerListener instance goes out of interest.
        *
        * @param breachableLinearObject the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaBreachableLinearObjectOutOfInterest(HlaBreachableLinearObjectPtr breachableLinearObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaBreachableLinearObject instance is deleted.
        *
        * @param breachableLinearObject the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaBreachableLinearObjectDeleted(HlaBreachableLinearObjectPtr breachableLinearObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaBreachableLinearObjectManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaBreachableLinearObjectManagerListener::Adapter : public HlaBreachableLinearObjectManagerListener {

    public:
        LIBAPI virtual void hlaBreachableLinearObjectDiscovered(HlaBreachableLinearObjectPtr breachableLinearObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaBreachableLinearObjectInitialized(HlaBreachableLinearObjectPtr breachableLinearObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaBreachableLinearObjectInInterest(HlaBreachableLinearObjectPtr breachableLinearObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaBreachableLinearObjectOutOfInterest(HlaBreachableLinearObjectPtr breachableLinearObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaBreachableLinearObjectDeleted(HlaBreachableLinearObjectPtr breachableLinearObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
