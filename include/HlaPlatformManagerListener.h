/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAPLATFORMMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLAPLATFORMMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaPlatform.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaPlatform instances.
    */
    class HlaPlatformManagerListener {

    public:

        LIBAPI virtual ~HlaPlatformManagerListener() {}

        /**
        * This method is called when a new HlaPlatform instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param platform the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaPlatformDiscovered(HlaPlatformPtr platform, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaPlatform instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param platform the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaPlatformInitialized(HlaPlatformPtr platform, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaPlatformManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param platform the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaPlatformInInterest(HlaPlatformPtr platform, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaPlatformManagerListener instance goes out of interest.
        *
        * @param platform the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaPlatformOutOfInterest(HlaPlatformPtr platform, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaPlatform instance is deleted.
        *
        * @param platform the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaPlatformDeleted(HlaPlatformPtr platform, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaPlatformManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaPlatformManagerListener::Adapter : public HlaPlatformManagerListener {

    public:
        LIBAPI virtual void hlaPlatformDiscovered(HlaPlatformPtr platform, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaPlatformInitialized(HlaPlatformPtr platform, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaPlatformInInterest(HlaPlatformPtr platform, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaPlatformOutOfInterest(HlaPlatformPtr platform, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaPlatformDeleted(HlaPlatformPtr platform, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
