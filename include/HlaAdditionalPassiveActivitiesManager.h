/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAADDITIONALPASSIVEACTIVITIESMANAGER_H
#define DEVELOPER_STUDIO_HLAADDITIONALPASSIVEACTIVITIESMANAGER_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EventIdentifierStruct.h>
#include <DevStudio/datatypes/RelativePositionStruct.h>
#include <string>

#include <list>
#include <vector>

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaAdditionalPassiveActivitiesManagerListener.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaException.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

   /**
   * Manager used to manage HlaAdditionalPassiveActivities.
   */
   class HlaAdditionalPassiveActivitiesManager : private boost::noncopyable {

   public:

      LIBAPI virtual ~HlaAdditionalPassiveActivitiesManager() {}

      /**
      * Gets a list of all HlaAdditionalPassiveActivities within interest, both local and remote.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of HlaAdditionalPassiveActivities
      */
      LIBAPI virtual std::list<HlaAdditionalPassiveActivitiesPtr> getHlaAdditionalPassiveActivities() = 0;

      /**
      * Gets a list of all HlaAdditionalPassiveActivities, both local and remote.
      * HlaAdditionalPassiveActivities not within interest are also included.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of all HlaAdditionalPassiveActivities
      */
      LIBAPI virtual std::list<HlaAdditionalPassiveActivitiesPtr> getAllHlaAdditionalPassiveActivities() = 0;

      /**
      * Gets a list of local HlaAdditionalPassiveActivities within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of local HlaAdditionalPassiveActivities
      */
      LIBAPI virtual std::list<HlaAdditionalPassiveActivitiesPtr> getLocalHlaAdditionalPassiveActivities() = 0;

      /**
      * Gets a list of remote HlaAdditionalPassiveActivitiess within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of remote HlaAdditionalPassiveActivitiess
      */
      LIBAPI virtual std::list<HlaAdditionalPassiveActivitiesPtr> getRemoteHlaAdditionalPassiveActivities() = 0;

      /**
      * Find a HlaAdditionalPassiveActivities with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> of the HlaAdditionalPassiveActivities to find
      *
      * @return the specified HlaAdditionalPassiveActivities, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaAdditionalPassiveActivitiesPtr getAdditionalPassiveActivitiesByHlaInstanceName(const std::wstring& hlaInstanceName) = 0;

      /**
      * Find a HlaAdditionalPassiveActivities with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param encodedHlaInstanceHandle the encoded <code>HLA instance handle</code> of the HlaAdditionalPassiveActivities to find
      *
      * @return the specified HlaAdditionalPassiveActivities, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaAdditionalPassiveActivitiesPtr getAdditionalPassiveActivitiesByHlaInstanceHandle(const std::vector<char>& encodedHlaInstanceHandle) = 0;

      /**
      * Creates a new HlaAdditionalPassiveActivities, and sets all <i>Create</i> attributes.
      *
      * @return Returns a new local HlaAdditionalPassiveActivities.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaAdditionalPassiveActivitiesPtr createLocalHlaAdditionalPassiveActivities(
      ) THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Creates a new HlaAdditionalPassiveActivities with a specified <code>HLA instance name</code>, and sets all
      * <i>Create</i> attributes.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> to register for this instance
      *
      * @return Returns a new local HlaAdditionalPassiveActivities.
      *
      * @throws HlaIllegalInstanceNameException if the name is illegal
      * @throws HlaInstanceNameInUseException if the name is already in use
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaAdditionalPassiveActivitiesPtr createLocalHlaAdditionalPassiveActivities(
         const std::wstring& hlaInstanceName
      ) THROW_SPEC (HlaIllegalInstanceNameException, HlaInstanceNameInUseException,
                    HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaAdditionalPassiveActivities and removes it from the federation.
      *
      * @param additionalPassiveActivities The HlaAdditionalPassiveActivities to delete
      *
      * @return the HlaAdditionalPassiveActivities deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaAdditionalPassiveActivitiesPtr deleteLocalHlaAdditionalPassiveActivities(HlaAdditionalPassiveActivitiesPtr additionalPassiveActivities)
          THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                      HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaAdditionalPassiveActivities and removes it from the federation.
      *
      * @param additionalPassiveActivities The HlaAdditionalPassiveActivities to delete
      * @param timestamp timestamp when the instance was removed.
      *
      * @return the HlaAdditionalPassiveActivities deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaAdditionalPassiveActivitiesPtr deleteLocalHlaAdditionalPassiveActivities(HlaAdditionalPassiveActivitiesPtr additionalPassiveActivities, HlaTimeStampPtr timestamp)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaAdditionalPassiveActivities and removes it from the federation.
      *
      * @param additionalPassiveActivities The HlaAdditionalPassiveActivities to delete
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaAdditionalPassiveActivities deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaAdditionalPassiveActivitiesPtr deleteLocalHlaAdditionalPassiveActivities(HlaAdditionalPassiveActivitiesPtr additionalPassiveActivities, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaAdditionalPassiveActivities and removes it from the federation.
      *
      * @param additionalPassiveActivities The HlaAdditionalPassiveActivities to delete
      * @param timestamp timestamp when the instance was removed.
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaAdditionalPassiveActivities deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaAdditionalPassiveActivitiesPtr deleteLocalHlaAdditionalPassiveActivities(HlaAdditionalPassiveActivitiesPtr additionalPassiveActivities, HlaTimeStampPtr timestamp, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Add a HlaAdditionalPassiveActivities manager listener.
      *
      * @param managerListener listener to add
      */
      LIBAPI virtual void addHlaAdditionalPassiveActivitiesManagerListener(HlaAdditionalPassiveActivitiesManagerListenerPtr managerListener) = 0;

      /**
      * Removed a HlaAdditionalPassiveActivities manager listener.
      *
      * @param managerListener listener to remove
      */
      LIBAPI virtual void removeHlaAdditionalPassiveActivitiesManagerListener(HlaAdditionalPassiveActivitiesManagerListenerPtr managerListener) = 0;

     /**
      * Add a listener that will automatically be added to all instances of HlaAdditionalPassiveActivities (Both local and remote).
      * The listener is notified when any attribute of an instance of HlaAdditionalPassiveActivities is updated.
      * The listener is also called when an interaction is sent to an instance of HlaAdditionalPassiveActivities.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param listener The listener to add.
      */
      LIBAPI virtual void addHlaAdditionalPassiveActivitiesDefaultInstanceListener(HlaAdditionalPassiveActivitiesListenerPtr listener) = 0;

     /**
      * Remove a listener from the set of listeners that are automatically added to new instances of HlaAdditionalPassiveActivities.
      * Note: The listener will not be removed from already existing instances of HlaAdditionalPassiveActivities.
      * This method is idempotent.
      *
      * @param listener The listener to remove.
      */
      LIBAPI virtual void removeHlaAdditionalPassiveActivitiesDefaultInstanceListener(HlaAdditionalPassiveActivitiesListenerPtr listener) = 0;

     /**
      * Add a valueListener that will automatically be added to all instances of HlaAdditionalPassiveActivities (Both local and remote).
      * The  Valuelistener is notified when any attribute of an instance of HlaAdditionalPassiveActivities is updated.
      * Note that the attribute values that are notified are not guaranteed to
      * be the latest available values for that attribute.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to add.
      */
      LIBAPI virtual void addHlaAdditionalPassiveActivitiesDefaultInstanceValueListener(HlaAdditionalPassiveActivitiesValueListenerPtr valueListener) = 0;

     /**
      * Remove a valueListener from the set of listeners that are automatically added to new instances of HlaAdditionalPassiveActivities.
      * Note: The valueListener will not be removed from already existing instances of HlaAdditionalPassiveActivities.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to remove.
      */
      LIBAPI virtual void removeHlaAdditionalPassiveActivitiesDefaultInstanceValueListener(HlaAdditionalPassiveActivitiesValueListenerPtr valueListener) = 0;

     /**
      * Enables or disables the HlaAdditionalPassiveActivities manager.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaAdditionalPassiveActivities manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      *
      * @param enabled <code>true</code> to enable the HlaAdditionalPassiveActivities manager, otherwise <code>false</code>
      */
      LIBAPI virtual void setEnabled(bool enabled) = 0;

     /**
      * Enables the HlaAdditionalPassiveActivities manager if it is avalable in the FOM at runtime.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaAdditionalPassiveActivities manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      */
      LIBAPI virtual void setEnabledIfAvailableInFom() = 0;

     /**
      * Check if the HlaAdditionalPassiveActivities manager is actually enabled when connected.
      * An HlaAdditionalPassiveActivities manager may be disabled with the <code>setEnabled(false)</code> function, or it may be configured
      * as <i>if available in the FOM</i> by the <code>setEnabledIfAvailableInFom()</code> function and
      * then not available in the FOM at runtime.
      * <p>
      * This method should only be used when connected, it will always return <code>false</code>
      * when not connected.
      *
      * @return <code>true</code> if connected and the HlaAdditionalPassiveActivities manager is enabled.
      */
      LIBAPI virtual bool isEnabled() = 0;
   };
}
#endif
