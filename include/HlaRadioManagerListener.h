/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLARADIOMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLARADIOMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaRadio.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaRadio instances.
    */
    class HlaRadioManagerListener {

    public:

        LIBAPI virtual ~HlaRadioManagerListener() {}

        /**
        * This method is called when a new HlaRadio instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param radio the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaRadioDiscovered(HlaRadioPtr radio, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaRadio instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param radio the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaRadioInitialized(HlaRadioPtr radio, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaRadioManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param radio the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaRadioInInterest(HlaRadioPtr radio, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaRadioManagerListener instance goes out of interest.
        *
        * @param radio the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaRadioOutOfInterest(HlaRadioPtr radio, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaRadio instance is deleted.
        *
        * @param radio the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaRadioDeleted(HlaRadioPtr radio, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaRadioManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaRadioManagerListener::Adapter : public HlaRadioManagerListener {

    public:
        LIBAPI virtual void hlaRadioDiscovered(HlaRadioPtr radio, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaRadioInitialized(HlaRadioPtr radio, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaRadioInInterest(HlaRadioPtr radio, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaRadioOutOfInterest(HlaRadioPtr radio, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaRadioDeleted(HlaRadioPtr radio, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
