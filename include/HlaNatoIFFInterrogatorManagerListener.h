/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLANATOIFFINTERROGATORMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLANATOIFFINTERROGATORMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaNatoIFFInterrogator.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaNatoIFFInterrogator instances.
    */
    class HlaNatoIFFInterrogatorManagerListener {

    public:

        LIBAPI virtual ~HlaNatoIFFInterrogatorManagerListener() {}

        /**
        * This method is called when a new HlaNatoIFFInterrogator instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param natoIFFInterrogator the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaNatoIFFInterrogatorDiscovered(HlaNatoIFFInterrogatorPtr natoIFFInterrogator, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaNatoIFFInterrogator instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param natoIFFInterrogator the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaNatoIFFInterrogatorInitialized(HlaNatoIFFInterrogatorPtr natoIFFInterrogator, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaNatoIFFInterrogatorManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param natoIFFInterrogator the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaNatoIFFInterrogatorInInterest(HlaNatoIFFInterrogatorPtr natoIFFInterrogator, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaNatoIFFInterrogatorManagerListener instance goes out of interest.
        *
        * @param natoIFFInterrogator the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaNatoIFFInterrogatorOutOfInterest(HlaNatoIFFInterrogatorPtr natoIFFInterrogator, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaNatoIFFInterrogator instance is deleted.
        *
        * @param natoIFFInterrogator the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaNatoIFFInterrogatorDeleted(HlaNatoIFFInterrogatorPtr natoIFFInterrogator, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaNatoIFFInterrogatorManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaNatoIFFInterrogatorManagerListener::Adapter : public HlaNatoIFFInterrogatorManagerListener {

    public:
        LIBAPI virtual void hlaNatoIFFInterrogatorDiscovered(HlaNatoIFFInterrogatorPtr natoIFFInterrogator, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaNatoIFFInterrogatorInitialized(HlaNatoIFFInterrogatorPtr natoIFFInterrogator, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaNatoIFFInterrogatorInInterest(HlaNatoIFFInterrogatorPtr natoIFFInterrogator, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaNatoIFFInterrogatorOutOfInterest(HlaNatoIFFInterrogatorPtr natoIFFInterrogator, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaNatoIFFInterrogatorDeleted(HlaNatoIFFInterrogatorPtr natoIFFInterrogator, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
