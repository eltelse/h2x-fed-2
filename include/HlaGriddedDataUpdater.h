/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAGRIDDEDDATAUPDATER_H
#define DEVELOPER_STUDIO_HLAGRIDDEDDATAUPDATER_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <boost/noncopyable.hpp>

#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EnvironmentDataCoordinateSystemEnum.h>
#include <DevStudio/datatypes/EnvironmentGridTypeEnum.h>
#include <DevStudio/datatypes/EnvironmentTypeStruct.h>
#include <DevStudio/datatypes/GridAxisStruct.h>
#include <DevStudio/datatypes/GridAxisStructLengthlessArray.h>
#include <DevStudio/datatypes/GridDataStruct.h>
#include <DevStudio/datatypes/GridDataStructLengthlessArray.h>
#include <DevStudio/datatypes/OrientationStruct.h>
#include <vector>

#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaException.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaHLAobjectRootUpdater.h>

namespace DevStudio {

    /**
    * Updater used to update attribute values.
    */
    class HlaGriddedDataUpdater : public HlaHLAobjectRootUpdater {

    public:

    LIBAPI virtual ~HlaGriddedDataUpdater() {}

    /**
    * Set the gridIdentifier for this update.
    * <br>Description from the FOM: <i>Identifies the environmental simulation application</i>
    * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
    *
    * @param gridIdentifier the new gridIdentifier
    */
    LIBAPI virtual void setGridIdentifier(const DevStudio::EntityIdentifierStruct& gridIdentifier) = 0;

    /**
    * Set the coordinateSystem for this update.
    * <br>Description from the FOM: <i>Specifies the coordinate system used to locate the data grid</i>
    * <br>Description of the data type from the FOM: <i>Environment data coordinate system</i>
    *
    * @param coordinateSystem the new coordinateSystem
    */
    LIBAPI virtual void setCoordinateSystem(const DevStudio::EnvironmentDataCoordinateSystemEnum::EnvironmentDataCoordinateSystemEnum& coordinateSystem) = 0;

    /**
    * Set the numberOfGridAxes for this update.
    * <br>Description from the FOM: <i>Specifies the number of grid axes used to define the data grid (e.g. three grid axes for an x, y, z coordinate system)</i>
    * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
    *
    * @param numberOfGridAxes the new numberOfGridAxes
    */
    LIBAPI virtual void setNumberOfGridAxes(const char& numberOfGridAxes) = 0;

    /**
    * Set the constantGrid for this update.
    * <br>Description from the FOM: <i>Specifies whether the grid axes remain constant for the life of the data grid</i>
    * <br>Description of the data type from the FOM: <i>Environment data grid type</i>
    *
    * @param constantGrid the new constantGrid
    */
    LIBAPI virtual void setConstantGrid(const DevStudio::EnvironmentGridTypeEnum::EnvironmentGridTypeEnum& constantGrid) = 0;

    /**
    * Set the environmentType for this update.
    * <br>Description from the FOM: <i>Identifies the type of environmental entity being described</i>
    * <br>Description of the data type from the FOM: <i>Record specifying the kind of environment, the domain and any extra information necessary for describing the environmental entity</i>
    *
    * @param environmentType the new environmentType
    */
    LIBAPI virtual void setEnvironmentType(const DevStudio::EnvironmentTypeStruct& environmentType) = 0;

    /**
    * Set the orientation for this update.
    * <br>Description from the FOM: <i>Specifies the orientation of the data grid, with Euler angles</i>
    * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
    *
    * @param orientation the new orientation
    */
    LIBAPI virtual void setOrientation(const DevStudio::OrientationStruct& orientation) = 0;

    /**
    * Set the sampleTime for this update.
    * <br>Description from the FOM: <i>Specifies the valid time of the environmental data sample</i>
    * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^64-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
    *
    * @param sampleTime the new sampleTime
    */
    LIBAPI virtual void setSampleTime(const unsigned long long& sampleTime) = 0;

    /**
    * Set the totalValues for this update.
    * <br>Description from the FOM: <i>Specifies the number of data values that make up this grid</i>
    * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
    *
    * @param totalValues the new totalValues
    */
    LIBAPI virtual void setTotalValues(const unsigned int& totalValues) = 0;

    /**
    * Set the vectorDimension for this update.
    * <br>Description from the FOM: <i>Specifies the number of data values at each grid point ; VectorDimension shall be one for scalar data, and shall be greater than one when multiple enumerated environmental data values are sent for each grid point (e.g. u, v, w wind components have VectorDimension = 3)</i>
    * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
    *
    * @param vectorDimension the new vectorDimension
    */
    LIBAPI virtual void setVectorDimension(const char& vectorDimension) = 0;

    /**
    * Set the gridAxisInfo for this update.
    * <br>Description from the FOM: <i>Specifies information on grid axes</i>
    * <br>Description of the data type from the FOM: <i>Specifies detailed information for a collection of grid axes</i>
    *
    * @param gridAxisInfo the new gridAxisInfo
    */
    LIBAPI virtual void setGridAxisInfo(const std::vector<DevStudio::GridAxisStruct >& gridAxisInfo) = 0;

    /**
    * Set the gridDataInfo for this update.
    * <br>Description from the FOM: <i>Specifies information on grid data representations</i>
    * <br>Description of the data type from the FOM: <i>Specifies detailed information for a collection of grid data representations</i>
    *
    * @param gridDataInfo the new gridDataInfo
    */
    LIBAPI virtual void setGridDataInfo(const std::vector<DevStudio::GridDataStruct >& gridDataInfo) = 0;

    /**
    * Send all the attributes.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate()
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

    /**
    * Send all the attributes with a specified timestamp.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @param timestamp timestamp to send for this update
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate(HlaTimeStampPtr timestamp)
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

    /**
    * Send all the attributes with a specified timestamp.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @param logicalTime logical time to send for this update
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaInvalidLogicalTimeException if the logical time is not valid.
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate(HlaLogicalTimePtr logicalTime)
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaInvalidLogicalTimeException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

    /**
    * Send all the attributes with a specified timestamp.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @param timestamp timestamp to send for this update
    * @param logicalTime logical time to send for this update
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaInvalidLogicalTimeException if the logical time is not valid.
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate(HlaTimeStampPtr timestamp, HlaLogicalTimePtr logicalTime)
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaInvalidLogicalTimeException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;
    };
}
#endif
