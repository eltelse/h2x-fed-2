/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAJAMMERBEAMMANAGER_H
#define DEVELOPER_STUDIO_HLAJAMMERBEAMMANAGER_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <DevStudio/datatypes/BeamFunctionCodeEnum.h>
#include <DevStudio/datatypes/EventIdentifierStruct.h>
#include <DevStudio/datatypes/RTIobjectIdArray.h>
#include <string>
#include <vector>

#include <list>
#include <vector>

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaJammerBeamManagerListener.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaException.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

   /**
   * Manager used to manage HlaJammerBeams.
   */
   class HlaJammerBeamManager : private boost::noncopyable {

   public:

      LIBAPI virtual ~HlaJammerBeamManager() {}

      /**
      * Gets a list of all HlaJammerBeams within interest, both local and remote.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of HlaJammerBeams
      */
      LIBAPI virtual std::list<HlaJammerBeamPtr> getHlaJammerBeams() = 0;

      /**
      * Gets a list of all HlaJammerBeams, both local and remote.
      * HlaJammerBeams not within interest are also included.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of all HlaJammerBeams
      */
      LIBAPI virtual std::list<HlaJammerBeamPtr> getAllHlaJammerBeams() = 0;

      /**
      * Gets a list of local HlaJammerBeams within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of local HlaJammerBeams
      */
      LIBAPI virtual std::list<HlaJammerBeamPtr> getLocalHlaJammerBeams() = 0;

      /**
      * Gets a list of remote HlaJammerBeams within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of remote HlaJammerBeams
      */
      LIBAPI virtual std::list<HlaJammerBeamPtr> getRemoteHlaJammerBeams() = 0;

      /**
      * Find a HlaJammerBeam with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> of the HlaJammerBeam to find
      *
      * @return the specified HlaJammerBeam, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaJammerBeamPtr getJammerBeamByHlaInstanceName(const std::wstring& hlaInstanceName) = 0;

      /**
      * Find a HlaJammerBeam with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param encodedHlaInstanceHandle the encoded <code>HLA instance handle</code> of the HlaJammerBeam to find
      *
      * @return the specified HlaJammerBeam, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaJammerBeamPtr getJammerBeamByHlaInstanceHandle(const std::vector<char>& encodedHlaInstanceHandle) = 0;

      /**
      * Creates a new HlaJammerBeam, and sets all <i>Create</i> attributes.
      *
      * @return Returns a new local HlaJammerBeam.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaJammerBeamPtr createLocalHlaJammerBeam(
      ) THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Creates a new HlaJammerBeam with a specified <code>HLA instance name</code>, and sets all
      * <i>Create</i> attributes.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> to register for this instance
      *
      * @return Returns a new local HlaJammerBeam.
      *
      * @throws HlaIllegalInstanceNameException if the name is illegal
      * @throws HlaInstanceNameInUseException if the name is already in use
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaJammerBeamPtr createLocalHlaJammerBeam(
         const std::wstring& hlaInstanceName
      ) THROW_SPEC (HlaIllegalInstanceNameException, HlaInstanceNameInUseException,
                    HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaJammerBeam and removes it from the federation.
      *
      * @param jammerBeam The HlaJammerBeam to delete
      *
      * @return the HlaJammerBeam deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaJammerBeamPtr deleteLocalHlaJammerBeam(HlaJammerBeamPtr jammerBeam)
          THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                      HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaJammerBeam and removes it from the federation.
      *
      * @param jammerBeam The HlaJammerBeam to delete
      * @param timestamp timestamp when the instance was removed.
      *
      * @return the HlaJammerBeam deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaJammerBeamPtr deleteLocalHlaJammerBeam(HlaJammerBeamPtr jammerBeam, HlaTimeStampPtr timestamp)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaJammerBeam and removes it from the federation.
      *
      * @param jammerBeam The HlaJammerBeam to delete
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaJammerBeam deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaJammerBeamPtr deleteLocalHlaJammerBeam(HlaJammerBeamPtr jammerBeam, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaJammerBeam and removes it from the federation.
      *
      * @param jammerBeam The HlaJammerBeam to delete
      * @param timestamp timestamp when the instance was removed.
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaJammerBeam deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaJammerBeamPtr deleteLocalHlaJammerBeam(HlaJammerBeamPtr jammerBeam, HlaTimeStampPtr timestamp, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Add a HlaJammerBeam manager listener.
      *
      * @param managerListener listener to add
      */
      LIBAPI virtual void addHlaJammerBeamManagerListener(HlaJammerBeamManagerListenerPtr managerListener) = 0;

      /**
      * Removed a HlaJammerBeam manager listener.
      *
      * @param managerListener listener to remove
      */
      LIBAPI virtual void removeHlaJammerBeamManagerListener(HlaJammerBeamManagerListenerPtr managerListener) = 0;

     /**
      * Add a listener that will automatically be added to all instances of HlaJammerBeam (Both local and remote).
      * The listener is notified when any attribute of an instance of HlaJammerBeam is updated.
      * The listener is also called when an interaction is sent to an instance of HlaJammerBeam.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param listener The listener to add.
      */
      LIBAPI virtual void addHlaJammerBeamDefaultInstanceListener(HlaJammerBeamListenerPtr listener) = 0;

     /**
      * Remove a listener from the set of listeners that are automatically added to new instances of HlaJammerBeam.
      * Note: The listener will not be removed from already existing instances of HlaJammerBeam.
      * This method is idempotent.
      *
      * @param listener The listener to remove.
      */
      LIBAPI virtual void removeHlaJammerBeamDefaultInstanceListener(HlaJammerBeamListenerPtr listener) = 0;

     /**
      * Add a valueListener that will automatically be added to all instances of HlaJammerBeam (Both local and remote).
      * The  Valuelistener is notified when any attribute of an instance of HlaJammerBeam is updated.
      * Note that the attribute values that are notified are not guaranteed to
      * be the latest available values for that attribute.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to add.
      */
      LIBAPI virtual void addHlaJammerBeamDefaultInstanceValueListener(HlaJammerBeamValueListenerPtr valueListener) = 0;

     /**
      * Remove a valueListener from the set of listeners that are automatically added to new instances of HlaJammerBeam.
      * Note: The valueListener will not be removed from already existing instances of HlaJammerBeam.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to remove.
      */
      LIBAPI virtual void removeHlaJammerBeamDefaultInstanceValueListener(HlaJammerBeamValueListenerPtr valueListener) = 0;

     /**
      * Enables or disables the HlaJammerBeam manager.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaJammerBeam manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      *
      * @param enabled <code>true</code> to enable the HlaJammerBeam manager, otherwise <code>false</code>
      */
      LIBAPI virtual void setEnabled(bool enabled) = 0;

     /**
      * Enables the HlaJammerBeam manager if it is avalable in the FOM at runtime.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaJammerBeam manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      */
      LIBAPI virtual void setEnabledIfAvailableInFom() = 0;

     /**
      * Check if the HlaJammerBeam manager is actually enabled when connected.
      * An HlaJammerBeam manager may be disabled with the <code>setEnabled(false)</code> function, or it may be configured
      * as <i>if available in the FOM</i> by the <code>setEnabledIfAvailableInFom()</code> function and
      * then not available in the FOM at runtime.
      * <p>
      * This method should only be used when connected, it will always return <code>false</code>
      * when not connected.
      *
      * @return <code>true</code> if connected and the HlaJammerBeam manager is enabled.
      */
      LIBAPI virtual bool isEnabled() = 0;
   };
}
#endif
