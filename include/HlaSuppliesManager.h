/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLASUPPLIESMANAGER_H
#define DEVELOPER_STUDIO_HLASUPPLIESMANAGER_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <DevStudio/datatypes/ArticulatedParameterStruct.h>
#include <DevStudio/datatypes/ArticulatedParameterStructLengthlessArray.h>
#include <DevStudio/datatypes/CamouflageEnum.h>
#include <DevStudio/datatypes/DamageStatusEnum.h>
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EntityTypeStruct.h>
#include <DevStudio/datatypes/ForceIdentifierEnum.h>
#include <DevStudio/datatypes/IsPartOfStruct.h>
#include <DevStudio/datatypes/MarkingStruct.h>
#include <DevStudio/datatypes/PropulsionSystemDataStruct.h>
#include <DevStudio/datatypes/PropulsionSystemDataStructLengthlessArray.h>
#include <DevStudio/datatypes/SpatialVariantStruct.h>
#include <DevStudio/datatypes/TrailingEffectsCodeEnum.h>
#include <DevStudio/datatypes/VectoringNozzleSystemDataStruct.h>
#include <DevStudio/datatypes/VectoringNozzleSystemDataStructLengthlessArray.h>
#include <vector>

#include <list>
#include <vector>

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaSuppliesManagerListener.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaException.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

   /**
   * Manager used to manage HlaSupplies.
   */
   class HlaSuppliesManager : private boost::noncopyable {

   public:

      LIBAPI virtual ~HlaSuppliesManager() {}

      /**
      * Gets a list of all HlaSupplies within interest, both local and remote.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of HlaSupplies
      */
      LIBAPI virtual std::list<HlaSuppliesPtr> getHlaSupplies() = 0;

      /**
      * Gets a list of all HlaSupplies, both local and remote.
      * HlaSupplies not within interest are also included.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of all HlaSupplies
      */
      LIBAPI virtual std::list<HlaSuppliesPtr> getAllHlaSupplies() = 0;

      /**
      * Gets a list of local HlaSupplies within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of local HlaSupplies
      */
      LIBAPI virtual std::list<HlaSuppliesPtr> getLocalHlaSupplies() = 0;

      /**
      * Gets a list of remote HlaSuppliess within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of remote HlaSuppliess
      */
      LIBAPI virtual std::list<HlaSuppliesPtr> getRemoteHlaSupplies() = 0;

      /**
      * Find a HlaSupplies with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> of the HlaSupplies to find
      *
      * @return the specified HlaSupplies, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaSuppliesPtr getSuppliesByHlaInstanceName(const std::wstring& hlaInstanceName) = 0;

      /**
      * Find a HlaSupplies with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param encodedHlaInstanceHandle the encoded <code>HLA instance handle</code> of the HlaSupplies to find
      *
      * @return the specified HlaSupplies, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaSuppliesPtr getSuppliesByHlaInstanceHandle(const std::vector<char>& encodedHlaInstanceHandle) = 0;

      /**
      * Creates a new HlaSupplies, and sets all <i>Create</i> attributes.
      *
      * @return Returns a new local HlaSupplies.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaSuppliesPtr createLocalHlaSupplies(
      ) THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Creates a new HlaSupplies with a specified <code>HLA instance name</code>, and sets all
      * <i>Create</i> attributes.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> to register for this instance
      *
      * @return Returns a new local HlaSupplies.
      *
      * @throws HlaIllegalInstanceNameException if the name is illegal
      * @throws HlaInstanceNameInUseException if the name is already in use
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaSuppliesPtr createLocalHlaSupplies(
         const std::wstring& hlaInstanceName
      ) THROW_SPEC (HlaIllegalInstanceNameException, HlaInstanceNameInUseException,
                    HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaSupplies and removes it from the federation.
      *
      * @param supplies The HlaSupplies to delete
      *
      * @return the HlaSupplies deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaSuppliesPtr deleteLocalHlaSupplies(HlaSuppliesPtr supplies)
          THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                      HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaSupplies and removes it from the federation.
      *
      * @param supplies The HlaSupplies to delete
      * @param timestamp timestamp when the instance was removed.
      *
      * @return the HlaSupplies deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaSuppliesPtr deleteLocalHlaSupplies(HlaSuppliesPtr supplies, HlaTimeStampPtr timestamp)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaSupplies and removes it from the federation.
      *
      * @param supplies The HlaSupplies to delete
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaSupplies deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaSuppliesPtr deleteLocalHlaSupplies(HlaSuppliesPtr supplies, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaSupplies and removes it from the federation.
      *
      * @param supplies The HlaSupplies to delete
      * @param timestamp timestamp when the instance was removed.
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaSupplies deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaSuppliesPtr deleteLocalHlaSupplies(HlaSuppliesPtr supplies, HlaTimeStampPtr timestamp, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Add a HlaSupplies manager listener.
      *
      * @param managerListener listener to add
      */
      LIBAPI virtual void addHlaSuppliesManagerListener(HlaSuppliesManagerListenerPtr managerListener) = 0;

      /**
      * Removed a HlaSupplies manager listener.
      *
      * @param managerListener listener to remove
      */
      LIBAPI virtual void removeHlaSuppliesManagerListener(HlaSuppliesManagerListenerPtr managerListener) = 0;

     /**
      * Add a listener that will automatically be added to all instances of HlaSupplies (Both local and remote).
      * The listener is notified when any attribute of an instance of HlaSupplies is updated.
      * The listener is also called when an interaction is sent to an instance of HlaSupplies.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param listener The listener to add.
      */
      LIBAPI virtual void addHlaSuppliesDefaultInstanceListener(HlaSuppliesListenerPtr listener) = 0;

     /**
      * Remove a listener from the set of listeners that are automatically added to new instances of HlaSupplies.
      * Note: The listener will not be removed from already existing instances of HlaSupplies.
      * This method is idempotent.
      *
      * @param listener The listener to remove.
      */
      LIBAPI virtual void removeHlaSuppliesDefaultInstanceListener(HlaSuppliesListenerPtr listener) = 0;

     /**
      * Add a valueListener that will automatically be added to all instances of HlaSupplies (Both local and remote).
      * The  Valuelistener is notified when any attribute of an instance of HlaSupplies is updated.
      * Note that the attribute values that are notified are not guaranteed to
      * be the latest available values for that attribute.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to add.
      */
      LIBAPI virtual void addHlaSuppliesDefaultInstanceValueListener(HlaSuppliesValueListenerPtr valueListener) = 0;

     /**
      * Remove a valueListener from the set of listeners that are automatically added to new instances of HlaSupplies.
      * Note: The valueListener will not be removed from already existing instances of HlaSupplies.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to remove.
      */
      LIBAPI virtual void removeHlaSuppliesDefaultInstanceValueListener(HlaSuppliesValueListenerPtr valueListener) = 0;

     /**
      * Enables or disables the HlaSupplies manager.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaSupplies manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      *
      * @param enabled <code>true</code> to enable the HlaSupplies manager, otherwise <code>false</code>
      */
      LIBAPI virtual void setEnabled(bool enabled) = 0;

     /**
      * Enables the HlaSupplies manager if it is avalable in the FOM at runtime.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaSupplies manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      */
      LIBAPI virtual void setEnabledIfAvailableInFom() = 0;

     /**
      * Check if the HlaSupplies manager is actually enabled when connected.
      * An HlaSupplies manager may be disabled with the <code>setEnabled(false)</code> function, or it may be configured
      * as <i>if available in the FOM</i> by the <code>setEnabledIfAvailableInFom()</code> function and
      * then not available in the FOM at runtime.
      * <p>
      * This method should only be used when connected, it will always return <code>false</code>
      * when not connected.
      *
      * @return <code>true</code> if connected and the HlaSupplies manager is enabled.
      */
      LIBAPI virtual bool isEnabled() = 0;
   };
}
#endif
