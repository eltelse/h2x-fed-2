/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAPROPULSIONNOISEATTRIBUTES_H
#define DEVELOPER_STUDIO_HLAPROPULSIONNOISEATTRIBUTES_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <string>
#include <vector>
#include <utility>
    
#include <boost/noncopyable.hpp>
    
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EventIdentifierStruct.h>
#include <DevStudio/datatypes/PropulsionPlantEnum.h>
#include <DevStudio/datatypes/RelativePositionStruct.h>
#include <DevStudio/datatypes/ShaftDataStruct.h>
#include <DevStudio/datatypes/ShaftDataStructLengthlessArray1Plus.h>
#include <string>
#include <vector>

#include <DevStudio/HlaException.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaUnderwaterAcousticsEmissionAttributes.h>
#include <DevStudio/HlaTimeStamped.h>

namespace DevStudio {

   /**
   * Interface used to represent all attributes for an object instance.
   */
   class HlaPropulsionNoiseAttributes : public HlaUnderwaterAcousticsEmissionAttributes {
   public:


     /**
      * An enumeration of the attributes of an HlaPropulsionNoise
      *
      *<table summary="All attributes">
      * <tr><td><b>Enum constant</b></td><td><b>C++ name</b></td><td><b>FOM name</b></td></tr>
      * <tr><td>HULL_MASKER_ON</td><td>hullMaskerOn</td><td><code>HullMaskerOn</code></td></tr>
      * <tr><td>PASSIVE_PARAMETER_INDEX</td><td>passiveParameterIndex</td><td><code>PassiveParameterIndex</code></td></tr>
      * <tr><td>PROPULSION_PLANT_CONFIGURATION</td><td>propulsionPlantConfiguration</td><td><code>PropulsionPlantConfiguration</code></td></tr>
      * <tr><td>SHAFT_RATE_DATA</td><td>shaftRateData</td><td><code>ShaftRateData</code></td></tr>
      * <tr><td>EVENT_IDENTIFIER</td><td>eventIdentifier</td><td><code>EventIdentifier</code></td></tr>
      * <tr><td>ENTITY_IDENTIFIER</td><td>entityIdentifier</td><td><code>EntityIdentifier</code></td></tr>
      * <tr><td>HOST_OBJECT_IDENTIFIER</td><td>hostObjectIdentifier</td><td><code>HostObjectIdentifier</code></td></tr>
      * <tr><td>RELATIVE_POSITION</td><td>relativePosition</td><td><code>RelativePosition</code></td></tr>
      *</table>
      */
      enum Attribute {
        /**
        * hullMaskerOn (FOM name: <code>HullMaskerOn</code>).
        * <br>Description from the FOM: <i>Whether the hull masker silencing system is on.</i>
        */
         HULL_MASKER_ON,

        /**
        * passiveParameterIndex (FOM name: <code>PassiveParameterIndex</code>).
        * <br>Description from the FOM: <i>Database index to look up the entities acoustic signature</i>
        */
         PASSIVE_PARAMETER_INDEX,

        /**
        * propulsionPlantConfiguration (FOM name: <code>PropulsionPlantConfiguration</code>).
        * <br>Description from the FOM: <i>The operating mode of the propulsion plant</i>
        */
         PROPULSION_PLANT_CONFIGURATION,

        /**
        * shaftRateData (FOM name: <code>ShaftRateData</code>).
        * <br>Description from the FOM: <i>Information about each of the propulsion shafts associated with the entity. Shafts are ordered from port to starboard, when looking from the stern to the bow.</i>
        */
         SHAFT_RATE_DATA,

        /**
        * eventIdentifier (FOM name: <code>EventIdentifier</code>).
        * <br>Description from the FOM: <i>The generating federate uses the Event Identifier to associate related events. The event number begins at one at the beginning of the exercise and is incremented by one for each event.</i>
        */
         EVENT_IDENTIFIER,

        /**
        * entityIdentifier (FOM name: <code>EntityIdentifier</code>).
        * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
        */
         ENTITY_IDENTIFIER,

        /**
        * hostObjectIdentifier (FOM name: <code>HostObjectIdentifier</code>).
        * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
        */
         HOST_OBJECT_IDENTIFIER,

        /**
        * relativePosition (FOM name: <code>RelativePosition</code>).
        * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
        */
         RELATIVE_POSITION
      };

     /**
      * Gets the name of the attribute.
      *
      * @return The name of the attribute. An empty string will be returned if the attribute does not exist.
      */
      LIBAPI virtual std::wstring getName(Attribute attribute);

     /**
      * Finds the attribute specified in the parameter attributeName.
      * The found enumeration will be stored in the parameter attribute.
      *
      * @return true if the attribute was found, false otherwise.
      */
      LIBAPI virtual bool find(Attribute& attribute, std::wstring attributeName);

      LIBAPI virtual ~HlaPropulsionNoiseAttributes() {}
    
      /**
      * Returns true if the <code>hullMaskerOn</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Whether the hull masker silencing system is on.</i>
      *
      * @return true if <code>hullMaskerOn</code> is available.
      */
      LIBAPI virtual bool hasHullMaskerOn() = 0;

      /**
      * Gets the value of the <code>hullMaskerOn</code> attribute.
      *
      * <br>Description from the FOM: <i>Whether the hull masker silencing system is on.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>hullMaskerOn</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual bool getHullMaskerOn()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>hullMaskerOn</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Whether the hull masker silencing system is on.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>hullMaskerOn</code> attribute.
      */
      LIBAPI virtual bool getHullMaskerOn(bool defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>hullMaskerOn</code> attribute.
      * <br>Description from the FOM: <i>Whether the hull masker silencing system is on.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>hullMaskerOn</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< bool > getHullMaskerOnTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>passiveParameterIndex</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Database index to look up the entities acoustic signature</i>
      *
      * @return true if <code>passiveParameterIndex</code> is available.
      */
      LIBAPI virtual bool hasPassiveParameterIndex() = 0;

      /**
      * Gets the value of the <code>passiveParameterIndex</code> attribute.
      *
      * <br>Description from the FOM: <i>Database index to look up the entities acoustic signature</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the <code>passiveParameterIndex</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual unsigned short getPassiveParameterIndex()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>passiveParameterIndex</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Database index to look up the entities acoustic signature</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>passiveParameterIndex</code> attribute.
      */
      LIBAPI virtual unsigned short getPassiveParameterIndex(unsigned short defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>passiveParameterIndex</code> attribute.
      * <br>Description from the FOM: <i>Database index to look up the entities acoustic signature</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the time stamped <code>passiveParameterIndex</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< unsigned short > getPassiveParameterIndexTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>propulsionPlantConfiguration</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The operating mode of the propulsion plant</i>
      *
      * @return true if <code>propulsionPlantConfiguration</code> is available.
      */
      LIBAPI virtual bool hasPropulsionPlantConfiguration() = 0;

      /**
      * Gets the value of the <code>propulsionPlantConfiguration</code> attribute.
      *
      * <br>Description from the FOM: <i>The operating mode of the propulsion plant</i>
      * <br>Description of the data type from the FOM: <i>Propulsion plant configuration</i>
      *
      * @return the <code>propulsionPlantConfiguration</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::PropulsionPlantEnum::PropulsionPlantEnum getPropulsionPlantConfiguration()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>propulsionPlantConfiguration</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The operating mode of the propulsion plant</i>
      * <br>Description of the data type from the FOM: <i>Propulsion plant configuration</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>propulsionPlantConfiguration</code> attribute.
      */
      LIBAPI virtual DevStudio::PropulsionPlantEnum::PropulsionPlantEnum getPropulsionPlantConfiguration(DevStudio::PropulsionPlantEnum::PropulsionPlantEnum defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>propulsionPlantConfiguration</code> attribute.
      * <br>Description from the FOM: <i>The operating mode of the propulsion plant</i>
      * <br>Description of the data type from the FOM: <i>Propulsion plant configuration</i>
      *
      * @return the time stamped <code>propulsionPlantConfiguration</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::PropulsionPlantEnum::PropulsionPlantEnum > getPropulsionPlantConfigurationTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>shaftRateData</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Information about each of the propulsion shafts associated with the entity. Shafts are ordered from port to starboard, when looking from the stern to the bow.</i>
      *
      * @return true if <code>shaftRateData</code> is available.
      */
      LIBAPI virtual bool hasShaftRateData() = 0;

      /**
      * Gets the value of the <code>shaftRateData</code> attribute.
      *
      * <br>Description from the FOM: <i>Information about each of the propulsion shafts associated with the entity. Shafts are ordered from port to starboard, when looking from the stern to the bow.</i>
      * <br>Description of the data type from the FOM: <i>Array of propulsion shaft states, one per shaft</i>
      *
      * @return the <code>shaftRateData</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::vector</* not empty */ DevStudio::ShaftDataStruct > getShaftRateData()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>shaftRateData</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Information about each of the propulsion shafts associated with the entity. Shafts are ordered from port to starboard, when looking from the stern to the bow.</i>
      * <br>Description of the data type from the FOM: <i>Array of propulsion shaft states, one per shaft</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>shaftRateData</code> attribute.
      */
      LIBAPI virtual std::vector</* not empty */ DevStudio::ShaftDataStruct > getShaftRateData(std::vector</* not empty */ DevStudio::ShaftDataStruct > defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>shaftRateData</code> attribute.
      * <br>Description from the FOM: <i>Information about each of the propulsion shafts associated with the entity. Shafts are ordered from port to starboard, when looking from the stern to the bow.</i>
      * <br>Description of the data type from the FOM: <i>Array of propulsion shaft states, one per shaft</i>
      *
      * @return the time stamped <code>shaftRateData</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::vector</* not empty */ DevStudio::ShaftDataStruct > > getShaftRateDataTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>eventIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The generating federate uses the Event Identifier to associate related events. The event number begins at one at the beginning of the exercise and is incremented by one for each event.</i>
      *
      * @return true if <code>eventIdentifier</code> is available.
      */
      LIBAPI virtual bool hasEventIdentifier() = 0;

      /**
      * Gets the value of the <code>eventIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>The generating federate uses the Event Identifier to associate related events. The event number begins at one at the beginning of the exercise and is incremented by one for each event.</i>
      * <br>Description of the data type from the FOM: <i>Identification of an event. Based on the Event Identifier record as specified in IEEE 1278.1-1995 section 5.2.18.</i>
      *
      * @return the <code>eventIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::EventIdentifierStruct getEventIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>eventIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The generating federate uses the Event Identifier to associate related events. The event number begins at one at the beginning of the exercise and is incremented by one for each event.</i>
      * <br>Description of the data type from the FOM: <i>Identification of an event. Based on the Event Identifier record as specified in IEEE 1278.1-1995 section 5.2.18.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>eventIdentifier</code> attribute.
      */
      LIBAPI virtual DevStudio::EventIdentifierStruct getEventIdentifier(DevStudio::EventIdentifierStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>eventIdentifier</code> attribute.
      * <br>Description from the FOM: <i>The generating federate uses the Event Identifier to associate related events. The event number begins at one at the beginning of the exercise and is incremented by one for each event.</i>
      * <br>Description of the data type from the FOM: <i>Identification of an event. Based on the Event Identifier record as specified in IEEE 1278.1-1995 section 5.2.18.</i>
      *
      * @return the time stamped <code>eventIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::EventIdentifierStruct > getEventIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>entityIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
      *
      * @return true if <code>entityIdentifier</code> is available.
      */
      LIBAPI virtual bool hasEntityIdentifier() = 0;

      /**
      * Gets the value of the <code>entityIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @return the <code>entityIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::EntityIdentifierStruct getEntityIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>entityIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>entityIdentifier</code> attribute.
      */
      LIBAPI virtual DevStudio::EntityIdentifierStruct getEntityIdentifier(DevStudio::EntityIdentifierStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>entityIdentifier</code> attribute.
      * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @return the time stamped <code>entityIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::EntityIdentifierStruct > getEntityIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>hostObjectIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
      *
      * @return true if <code>hostObjectIdentifier</code> is available.
      */
      LIBAPI virtual bool hasHostObjectIdentifier() = 0;

      /**
      * Gets the value of the <code>hostObjectIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @return the <code>hostObjectIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::string getHostObjectIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>hostObjectIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>hostObjectIdentifier</code> attribute.
      */
      LIBAPI virtual std::string getHostObjectIdentifier(std::string defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>hostObjectIdentifier</code> attribute.
      * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @return the time stamped <code>hostObjectIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::string > getHostObjectIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>relativePosition</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
      *
      * @return true if <code>relativePosition</code> is available.
      */
      LIBAPI virtual bool hasRelativePosition() = 0;

      /**
      * Gets the value of the <code>relativePosition</code> attribute.
      *
      * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
      * <br>Description of the data type from the FOM: <i>Relative position in right-handed Cartesian coordinates.</i>
      *
      * @return the <code>relativePosition</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::RelativePositionStruct getRelativePosition()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>relativePosition</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
      * <br>Description of the data type from the FOM: <i>Relative position in right-handed Cartesian coordinates.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>relativePosition</code> attribute.
      */
      LIBAPI virtual DevStudio::RelativePositionStruct getRelativePosition(DevStudio::RelativePositionStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>relativePosition</code> attribute.
      * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
      * <br>Description of the data type from the FOM: <i>Relative position in right-handed Cartesian coordinates.</i>
      *
      * @return the time stamped <code>relativePosition</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::RelativePositionStruct > getRelativePositionTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
   };
}
#endif
