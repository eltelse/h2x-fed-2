/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAEMITTERBEAMMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLAEMITTERBEAMMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaEmitterBeam.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaEmitterBeam instances.
    */
    class HlaEmitterBeamManagerListener {

    public:

        LIBAPI virtual ~HlaEmitterBeamManagerListener() {}

        /**
        * This method is called when a new HlaEmitterBeam instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param emitterBeam the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaEmitterBeamDiscovered(HlaEmitterBeamPtr emitterBeam, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaEmitterBeam instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param emitterBeam the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaEmitterBeamInitialized(HlaEmitterBeamPtr emitterBeam, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaEmitterBeamManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param emitterBeam the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaEmitterBeamInInterest(HlaEmitterBeamPtr emitterBeam, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaEmitterBeamManagerListener instance goes out of interest.
        *
        * @param emitterBeam the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaEmitterBeamOutOfInterest(HlaEmitterBeamPtr emitterBeam, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaEmitterBeam instance is deleted.
        *
        * @param emitterBeam the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaEmitterBeamDeleted(HlaEmitterBeamPtr emitterBeam, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaEmitterBeamManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaEmitterBeamManagerListener::Adapter : public HlaEmitterBeamManagerListener {

    public:
        LIBAPI virtual void hlaEmitterBeamDiscovered(HlaEmitterBeamPtr emitterBeam, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaEmitterBeamInitialized(HlaEmitterBeamPtr emitterBeam, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaEmitterBeamInInterest(HlaEmitterBeamPtr emitterBeam, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaEmitterBeamOutOfInterest(HlaEmitterBeamPtr emitterBeam, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaEmitterBeamDeleted(HlaEmitterBeamPtr emitterBeam, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
