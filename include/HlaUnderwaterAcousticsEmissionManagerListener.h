/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAUNDERWATERACOUSTICSEMISSIONMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLAUNDERWATERACOUSTICSEMISSIONMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaUnderwaterAcousticsEmission.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaUnderwaterAcousticsEmission instances.
    */
    class HlaUnderwaterAcousticsEmissionManagerListener {

    public:

        LIBAPI virtual ~HlaUnderwaterAcousticsEmissionManagerListener() {}

        /**
        * This method is called when a new HlaUnderwaterAcousticsEmission instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param underwaterAcousticsEmission the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaUnderwaterAcousticsEmissionDiscovered(HlaUnderwaterAcousticsEmissionPtr underwaterAcousticsEmission, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaUnderwaterAcousticsEmission instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param underwaterAcousticsEmission the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaUnderwaterAcousticsEmissionInitialized(HlaUnderwaterAcousticsEmissionPtr underwaterAcousticsEmission, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaUnderwaterAcousticsEmissionManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param underwaterAcousticsEmission the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaUnderwaterAcousticsEmissionInInterest(HlaUnderwaterAcousticsEmissionPtr underwaterAcousticsEmission, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaUnderwaterAcousticsEmissionManagerListener instance goes out of interest.
        *
        * @param underwaterAcousticsEmission the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaUnderwaterAcousticsEmissionOutOfInterest(HlaUnderwaterAcousticsEmissionPtr underwaterAcousticsEmission, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaUnderwaterAcousticsEmission instance is deleted.
        *
        * @param underwaterAcousticsEmission the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaUnderwaterAcousticsEmissionDeleted(HlaUnderwaterAcousticsEmissionPtr underwaterAcousticsEmission, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaUnderwaterAcousticsEmissionManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaUnderwaterAcousticsEmissionManagerListener::Adapter : public HlaUnderwaterAcousticsEmissionManagerListener {

    public:
        LIBAPI virtual void hlaUnderwaterAcousticsEmissionDiscovered(HlaUnderwaterAcousticsEmissionPtr underwaterAcousticsEmission, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaUnderwaterAcousticsEmissionInitialized(HlaUnderwaterAcousticsEmissionPtr underwaterAcousticsEmission, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaUnderwaterAcousticsEmissionInInterest(HlaUnderwaterAcousticsEmissionPtr underwaterAcousticsEmission, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaUnderwaterAcousticsEmissionOutOfInterest(HlaUnderwaterAcousticsEmissionPtr underwaterAcousticsEmission, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaUnderwaterAcousticsEmissionDeleted(HlaUnderwaterAcousticsEmissionPtr underwaterAcousticsEmission, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
