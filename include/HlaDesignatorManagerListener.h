/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLADESIGNATORMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLADESIGNATORMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaDesignator.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaDesignator instances.
    */
    class HlaDesignatorManagerListener {

    public:

        LIBAPI virtual ~HlaDesignatorManagerListener() {}

        /**
        * This method is called when a new HlaDesignator instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param designator the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaDesignatorDiscovered(HlaDesignatorPtr designator, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaDesignator instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param designator the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaDesignatorInitialized(HlaDesignatorPtr designator, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaDesignatorManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param designator the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaDesignatorInInterest(HlaDesignatorPtr designator, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaDesignatorManagerListener instance goes out of interest.
        *
        * @param designator the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaDesignatorOutOfInterest(HlaDesignatorPtr designator, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaDesignator instance is deleted.
        *
        * @param designator the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaDesignatorDeleted(HlaDesignatorPtr designator, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaDesignatorManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaDesignatorManagerListener::Adapter : public HlaDesignatorManagerListener {

    public:
        LIBAPI virtual void hlaDesignatorDiscovered(HlaDesignatorPtr designator, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaDesignatorInitialized(HlaDesignatorPtr designator, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaDesignatorInInterest(HlaDesignatorPtr designator, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaDesignatorOutOfInterest(HlaDesignatorPtr designator, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaDesignatorDeleted(HlaDesignatorPtr designator, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
