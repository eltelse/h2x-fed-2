/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLARIBBONBRIDGEOBJECTMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLARIBBONBRIDGEOBJECTMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaRibbonBridgeObject.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaRibbonBridgeObject instances.
    */
    class HlaRibbonBridgeObjectManagerListener {

    public:

        LIBAPI virtual ~HlaRibbonBridgeObjectManagerListener() {}

        /**
        * This method is called when a new HlaRibbonBridgeObject instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param ribbonBridgeObject the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaRibbonBridgeObjectDiscovered(HlaRibbonBridgeObjectPtr ribbonBridgeObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaRibbonBridgeObject instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param ribbonBridgeObject the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaRibbonBridgeObjectInitialized(HlaRibbonBridgeObjectPtr ribbonBridgeObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaRibbonBridgeObjectManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param ribbonBridgeObject the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaRibbonBridgeObjectInInterest(HlaRibbonBridgeObjectPtr ribbonBridgeObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaRibbonBridgeObjectManagerListener instance goes out of interest.
        *
        * @param ribbonBridgeObject the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaRibbonBridgeObjectOutOfInterest(HlaRibbonBridgeObjectPtr ribbonBridgeObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaRibbonBridgeObject instance is deleted.
        *
        * @param ribbonBridgeObject the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaRibbonBridgeObjectDeleted(HlaRibbonBridgeObjectPtr ribbonBridgeObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaRibbonBridgeObjectManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaRibbonBridgeObjectManagerListener::Adapter : public HlaRibbonBridgeObjectManagerListener {

    public:
        LIBAPI virtual void hlaRibbonBridgeObjectDiscovered(HlaRibbonBridgeObjectPtr ribbonBridgeObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaRibbonBridgeObjectInitialized(HlaRibbonBridgeObjectPtr ribbonBridgeObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaRibbonBridgeObjectInInterest(HlaRibbonBridgeObjectPtr ribbonBridgeObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaRibbonBridgeObjectOutOfInterest(HlaRibbonBridgeObjectPtr ribbonBridgeObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaRibbonBridgeObjectDeleted(HlaRibbonBridgeObjectPtr ribbonBridgeObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
