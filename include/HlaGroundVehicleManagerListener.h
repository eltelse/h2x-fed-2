/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAGROUNDVEHICLEMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLAGROUNDVEHICLEMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaGroundVehicle.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaGroundVehicle instances.
    */
    class HlaGroundVehicleManagerListener {

    public:

        LIBAPI virtual ~HlaGroundVehicleManagerListener() {}

        /**
        * This method is called when a new HlaGroundVehicle instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param groundVehicle the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaGroundVehicleDiscovered(HlaGroundVehiclePtr groundVehicle, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaGroundVehicle instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param groundVehicle the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaGroundVehicleInitialized(HlaGroundVehiclePtr groundVehicle, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaGroundVehicleManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param groundVehicle the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaGroundVehicleInInterest(HlaGroundVehiclePtr groundVehicle, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaGroundVehicleManagerListener instance goes out of interest.
        *
        * @param groundVehicle the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaGroundVehicleOutOfInterest(HlaGroundVehiclePtr groundVehicle, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaGroundVehicle instance is deleted.
        *
        * @param groundVehicle the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaGroundVehicleDeleted(HlaGroundVehiclePtr groundVehicle, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaGroundVehicleManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaGroundVehicleManagerListener::Adapter : public HlaGroundVehicleManagerListener {

    public:
        LIBAPI virtual void hlaGroundVehicleDiscovered(HlaGroundVehiclePtr groundVehicle, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaGroundVehicleInitialized(HlaGroundVehiclePtr groundVehicle, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaGroundVehicleInInterest(HlaGroundVehiclePtr groundVehicle, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaGroundVehicleOutOfInterest(HlaGroundVehiclePtr groundVehicle, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaGroundVehicleDeleted(HlaGroundVehiclePtr groundVehicle, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
