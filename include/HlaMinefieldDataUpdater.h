/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAMINEFIELDDATAUPDATER_H
#define DEVELOPER_STUDIO_HLAMINEFIELDDATAUPDATER_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <boost/noncopyable.hpp>

#include <DevStudio/datatypes/ClockTimeStruct.h>
#include <DevStudio/datatypes/ClockTimeStructLengthlessArray.h>
#include <DevStudio/datatypes/DepthMeterFloat32LengthlessArray.h>
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EntityTypeStruct.h>
#include <DevStudio/datatypes/MineDielectricDifferenceLengthlessArray.h>
#include <DevStudio/datatypes/MineFusingStruct.h>
#include <DevStudio/datatypes/MineFusingStructLengthlessArray.h>
#include <DevStudio/datatypes/MineIdentifierLengthlessArray.h>
#include <DevStudio/datatypes/MinefieldPaintSchemeEnum.h>
#include <DevStudio/datatypes/MinefieldPaintSchemeLengthlessArray.h>
#include <DevStudio/datatypes/MinefieldSensorTypeEnum.h>
#include <DevStudio/datatypes/MinefieldSensorTypeLengthlessArray.h>
#include <DevStudio/datatypes/OrientationStruct.h>
#include <DevStudio/datatypes/OrientationStructLengthlessArray.h>
#include <DevStudio/datatypes/RelativePositionStruct.h>
#include <DevStudio/datatypes/TemperatureDegreeCelsiusFloat32LengthlessArray.h>
#include <DevStudio/datatypes/UnsignedInteger8LengthlessArray.h>
#include <DevStudio/datatypes/WorldLocationStruct.h>
#include <DevStudio/datatypes/WorldLocationStructLengthlessArray.h>
#include <string>
#include <vector>

#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaException.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaEmbeddedSystemUpdater.h>

namespace DevStudio {

    /**
    * Updater used to update attribute values.
    */
    class HlaMinefieldDataUpdater : public HlaEmbeddedSystemUpdater {

    public:

    LIBAPI virtual ~HlaMinefieldDataUpdater() {}

    /**
    * Set the groundBurialDepthOffset for this update.
    * <br>Description from the FOM: <i>Specifies the offset of the origin of the mine coordinate system with respect to the ground surface</i>
    * <br>Description of the data type from the FOM: <i>Specifies ground - snow - water burial depth offset for each mine in a collection of mines</i>
    *
    * @param groundBurialDepthOffset the new groundBurialDepthOffset
    */
    LIBAPI virtual void setGroundBurialDepthOffset(const std::vector<float >& groundBurialDepthOffset) = 0;

    /**
    * Set the fusing for this update.
    * <br>Description from the FOM: <i>Specifies the primary and secondary fuse and anti-handling device for each mine in a collection of mines</i>
    * <br>Description of the data type from the FOM: <i>Specifies the type of primary fuse, the type of the secondary fuse and the anti-handling device status for a collection of mines</i>
    *
    * @param fusing the new fusing
    */
    LIBAPI virtual void setFusing(const std::vector<DevStudio::MineFusingStruct >& fusing) = 0;

    /**
    * Set the mineEmplacementTime for this update.
    * <br>Description from the FOM: <i>Specifies the real-world (UTC) emplacement time of each mine in a collection of mines</i>
    * <br>Description of the data type from the FOM: <i>Dynamic array of ClockTimeStruct elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
    *
    * @param mineEmplacementTime the new mineEmplacementTime
    */
    LIBAPI virtual void setMineEmplacementTime(const std::vector<DevStudio::ClockTimeStruct >& mineEmplacementTime) = 0;

    /**
    * Set the mineEntityIdentifier for this update.
    * <br>Description from the FOM: <i>Identifies the mine entity identifier; the MineEntityID in conjunction with the MinefieldID form the unique identifier for each mine</i>
    * <br>Description of the data type from the FOM: <i>Identifies the mine entity identifier for each mine in a collection of mines</i>
    *
    * @param mineEntityIdentifier the new mineEntityIdentifier
    */
    LIBAPI virtual void setMineEntityIdentifier(const std::vector<unsigned short >& mineEntityIdentifier) = 0;

    /**
    * Set the minefieldIdentifier for this update.
    * <br>Description from the FOM: <i>Identifies the minefield to which the mines belong</i>
    * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
    *
    * @param minefieldIdentifier the new minefieldIdentifier
    */
    LIBAPI virtual void setMinefieldIdentifier(const std::string& minefieldIdentifier) = 0;

    /**
    * Set the mineLocation for this update.
    * <br>Description from the FOM: <i>Specifies the location of the mine relative to the minefield location for each mine in a collection of mines</i>
    * <br>Description of the data type from the FOM: <i>Dynamic array of WorldLocationStruct elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
    *
    * @param mineLocation the new mineLocation
    */
    LIBAPI virtual void setMineLocation(const std::vector<DevStudio::WorldLocationStruct >& mineLocation) = 0;

    /**
    * Set the mineOrientation for this update.
    * <br>Description from the FOM: <i>Specifies the orientation of the center axis direction of fire of the mine, relative to the minefield Coordinate System for each mine in a collection of mines</i>
    * <br>Description of the data type from the FOM: <i>Dynamic array of OrientationStruct elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
    *
    * @param mineOrientation the new mineOrientation
    */
    LIBAPI virtual void setMineOrientation(const std::vector<DevStudio::OrientationStruct >& mineOrientation) = 0;

    /**
    * Set the mineType for this update.
    * <br>Description from the FOM: <i>Specifies the type of mine for the collection of mines contained within the MinefieldData object</i>
    * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
    *
    * @param mineType the new mineType
    */
    LIBAPI virtual void setMineType(const DevStudio::EntityTypeStruct& mineType) = 0;

    /**
    * Set the numberTripDetonationWires for this update.
    * <br>Description from the FOM: <i>Specifies the number of trip detonation wires that exist for each mine in a collection of mines</i>
    * <br>Description of the data type from the FOM: <i>Generic dynamic array of UnsignedInteger8 elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
    *
    * @param numberTripDetonationWires the new numberTripDetonationWires
    */
    LIBAPI virtual void setNumberTripDetonationWires(const std::vector<char >& numberTripDetonationWires) = 0;

    /**
    * Set the numberWireVertices for this update.
    * <br>Description from the FOM: <i>Specifies the number of vertices for each trip wire of each mine in a collection of mines</i>
    * <br>Description of the data type from the FOM: <i>Generic dynamic array of UnsignedInteger8 elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
    *
    * @param numberWireVertices the new numberWireVertices
    */
    LIBAPI virtual void setNumberWireVertices(const std::vector<char >& numberWireVertices) = 0;

    /**
    * Set the paintScheme for this update.
    * <br>Description from the FOM: <i>Specifies the camouflage scheme/color of the mine</i>
    * <br>Description of the data type from the FOM: <i>Specifies the camouflage scheme/color for each mine in a collection of mines</i>
    *
    * @param paintScheme the new paintScheme
    */
    LIBAPI virtual void setPaintScheme(const std::vector<DevStudio::MinefieldPaintSchemeEnum::MinefieldPaintSchemeEnum >& paintScheme) = 0;

    /**
    * Set the reflectance for this update.
    * <br>Description from the FOM: <i>Specifies the local dielectric difference between the mine and the surrounding soil</i>
    * <br>Description of the data type from the FOM: <i>Specifies local dielectric difference between the mine and the surrounding soil (reflectance) for each mine in a collection of mines</i>
    *
    * @param reflectance the new reflectance
    */
    LIBAPI virtual void setReflectance(const std::vector<float >& reflectance) = 0;

    /**
    * Set the scalarDetectionCoefficient for this update.
    * <br>Description from the FOM: <i>Specifies the coefficient to be utilized to insure proper correlation between detectors located on different simulation platforms</i>
    * <br>Description of the data type from the FOM: <i>Generic dynamic array of UnsignedInteger8 elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
    *
    * @param scalarDetectionCoefficient the new scalarDetectionCoefficient
    */
    LIBAPI virtual void setScalarDetectionCoefficient(const std::vector<char >& scalarDetectionCoefficient) = 0;

    /**
    * Set the sensorTypes for this update.
    * <br>Description from the FOM: <i>In QRP mode, specifies the requesting sensor types which were specified in the minefield query whereas in Heartbeat mode, specifies the sensor types that are being served by the minefield</i>
    * <br>Description of the data type from the FOM: <i>Specifies a collection of minefield sensor types</i>
    *
    * @param sensorTypes the new sensorTypes
    */
    LIBAPI virtual void setSensorTypes(const std::vector<DevStudio::MinefieldSensorTypeEnum::MinefieldSensorTypeEnum >& sensorTypes) = 0;

    /**
    * Set the snowBurialDepthOffset for this update.
    * <br>Description from the FOM: <i>Specifies the offset of the origin of the mine coordinate system with respect to the snow surface</i>
    * <br>Description of the data type from the FOM: <i>Specifies ground - snow - water burial depth offset for each mine in a collection of mines</i>
    *
    * @param snowBurialDepthOffset the new snowBurialDepthOffset
    */
    LIBAPI virtual void setSnowBurialDepthOffset(const std::vector<float >& snowBurialDepthOffset) = 0;

    /**
    * Set the thermalContrast for this update.
    * <br>Description from the FOM: <i>Specifies the temperature difference between the mine and the surround soil in degrees Centigrade</i>
    * <br>Description of the data type from the FOM: <i>Specifies thermal contrast for each mine in a collection of mines</i>
    *
    * @param thermalContrast the new thermalContrast
    */
    LIBAPI virtual void setThermalContrast(const std::vector<float >& thermalContrast) = 0;

    /**
    * Set the waterBurialDepthOffset for this update.
    * <br>Description from the FOM: <i>Specifies the offset of the origin of the mine coordinate system with respect to the water surface</i>
    * <br>Description of the data type from the FOM: <i>Specifies ground - snow - water burial depth offset for each mine in a collection of mines</i>
    *
    * @param waterBurialDepthOffset the new waterBurialDepthOffset
    */
    LIBAPI virtual void setWaterBurialDepthOffset(const std::vector<float >& waterBurialDepthOffset) = 0;

    /**
    * Set the wireVertices for this update.
    * <br>Description from the FOM: <i>Specifies the locations of vertices in a trip wire</i>
    * <br>Description of the data type from the FOM: <i>Dynamic array of WorldLocationStruct elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
    *
    * @param wireVertices the new wireVertices
    */
    LIBAPI virtual void setWireVertices(const std::vector<DevStudio::WorldLocationStruct >& wireVertices) = 0;

    /**
    * Set the entityIdentifier for this update.
    * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
    * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
    *
    * @param entityIdentifier the new entityIdentifier
    */
    LIBAPI virtual void setEntityIdentifier(const DevStudio::EntityIdentifierStruct& entityIdentifier) = 0;

    /**
    * Set the hostObjectIdentifier for this update.
    * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
    * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
    *
    * @param hostObjectIdentifier the new hostObjectIdentifier
    */
    LIBAPI virtual void setHostObjectIdentifier(const std::string& hostObjectIdentifier) = 0;

    /**
    * Set the relativePosition for this update.
    * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
    * <br>Description of the data type from the FOM: <i>Relative position in right-handed Cartesian coordinates.</i>
    *
    * @param relativePosition the new relativePosition
    */
    LIBAPI virtual void setRelativePosition(const DevStudio::RelativePositionStruct& relativePosition) = 0;

    /**
    * Send all the attributes.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate()
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

    /**
    * Send all the attributes with a specified timestamp.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @param timestamp timestamp to send for this update
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate(HlaTimeStampPtr timestamp)
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

    /**
    * Send all the attributes with a specified timestamp.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @param logicalTime logical time to send for this update
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaInvalidLogicalTimeException if the logical time is not valid.
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate(HlaLogicalTimePtr logicalTime)
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaInvalidLogicalTimeException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

    /**
    * Send all the attributes with a specified timestamp.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @param timestamp timestamp to send for this update
    * @param logicalTime logical time to send for this update
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaInvalidLogicalTimeException if the logical time is not valid.
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate(HlaTimeStampPtr timestamp, HlaLogicalTimePtr logicalTime)
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaInvalidLogicalTimeException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;
    };
}
#endif
