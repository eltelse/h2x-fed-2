/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAPROPULSIONNOISEMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLAPROPULSIONNOISEMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaPropulsionNoise.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaPropulsionNoise instances.
    */
    class HlaPropulsionNoiseManagerListener {

    public:

        LIBAPI virtual ~HlaPropulsionNoiseManagerListener() {}

        /**
        * This method is called when a new HlaPropulsionNoise instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param propulsionNoise the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaPropulsionNoiseDiscovered(HlaPropulsionNoisePtr propulsionNoise, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaPropulsionNoise instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param propulsionNoise the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaPropulsionNoiseInitialized(HlaPropulsionNoisePtr propulsionNoise, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaPropulsionNoiseManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param propulsionNoise the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaPropulsionNoiseInInterest(HlaPropulsionNoisePtr propulsionNoise, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaPropulsionNoiseManagerListener instance goes out of interest.
        *
        * @param propulsionNoise the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaPropulsionNoiseOutOfInterest(HlaPropulsionNoisePtr propulsionNoise, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaPropulsionNoise instance is deleted.
        *
        * @param propulsionNoise the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaPropulsionNoiseDeleted(HlaPropulsionNoisePtr propulsionNoise, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaPropulsionNoiseManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaPropulsionNoiseManagerListener::Adapter : public HlaPropulsionNoiseManagerListener {

    public:
        LIBAPI virtual void hlaPropulsionNoiseDiscovered(HlaPropulsionNoisePtr propulsionNoise, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaPropulsionNoiseInitialized(HlaPropulsionNoisePtr propulsionNoise, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaPropulsionNoiseInInterest(HlaPropulsionNoisePtr propulsionNoise, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaPropulsionNoiseOutOfInterest(HlaPropulsionNoisePtr propulsionNoise, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaPropulsionNoiseDeleted(HlaPropulsionNoisePtr propulsionNoise, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
