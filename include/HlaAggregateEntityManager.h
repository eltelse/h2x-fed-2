/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAAGGREGATEENTITYMANAGER_H
#define DEVELOPER_STUDIO_HLAAGGREGATEENTITYMANAGER_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <DevStudio/datatypes/AggregateMarkingStruct.h>
#include <DevStudio/datatypes/AggregateStateEnum.h>
#include <DevStudio/datatypes/DimensionStruct.h>
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EntityTypeStruct.h>
#include <DevStudio/datatypes/ForceIdentifierEnum.h>
#include <DevStudio/datatypes/FormationEnum.h>
#include <DevStudio/datatypes/IsPartOfStruct.h>
#include <DevStudio/datatypes/RTIobjectIdArray.h>
#include <DevStudio/datatypes/SilentAggregateStruct.h>
#include <DevStudio/datatypes/SilentAggregateStructLengthlessArray.h>
#include <DevStudio/datatypes/SilentEntityStruct.h>
#include <DevStudio/datatypes/SilentEntityStructLengthlessArray.h>
#include <DevStudio/datatypes/SpatialVariantStruct.h>
#include <DevStudio/datatypes/VariableDatumStruct.h>
#include <DevStudio/datatypes/VariableDatumStructLengthlessArray.h>
#include <string>
#include <vector>

#include <list>
#include <vector>

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaAggregateEntityManagerListener.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaException.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

   /**
   * Manager used to manage HlaAggregateEntitys.
   */
   class HlaAggregateEntityManager : private boost::noncopyable {

   public:

      LIBAPI virtual ~HlaAggregateEntityManager() {}

      /**
      * Gets a list of all HlaAggregateEntitys within interest, both local and remote.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of HlaAggregateEntitys
      */
      LIBAPI virtual std::list<HlaAggregateEntityPtr> getHlaAggregateEntitys() = 0;

      /**
      * Gets a list of all HlaAggregateEntitys, both local and remote.
      * HlaAggregateEntitys not within interest are also included.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of all HlaAggregateEntitys
      */
      LIBAPI virtual std::list<HlaAggregateEntityPtr> getAllHlaAggregateEntitys() = 0;

      /**
      * Gets a list of local HlaAggregateEntitys within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of local HlaAggregateEntitys
      */
      LIBAPI virtual std::list<HlaAggregateEntityPtr> getLocalHlaAggregateEntitys() = 0;

      /**
      * Gets a list of remote HlaAggregateEntitys within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of remote HlaAggregateEntitys
      */
      LIBAPI virtual std::list<HlaAggregateEntityPtr> getRemoteHlaAggregateEntitys() = 0;

      /**
      * Find a HlaAggregateEntity with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> of the HlaAggregateEntity to find
      *
      * @return the specified HlaAggregateEntity, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaAggregateEntityPtr getAggregateEntityByHlaInstanceName(const std::wstring& hlaInstanceName) = 0;

      /**
      * Find a HlaAggregateEntity with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param encodedHlaInstanceHandle the encoded <code>HLA instance handle</code> of the HlaAggregateEntity to find
      *
      * @return the specified HlaAggregateEntity, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaAggregateEntityPtr getAggregateEntityByHlaInstanceHandle(const std::vector<char>& encodedHlaInstanceHandle) = 0;

      /**
      * Creates a new HlaAggregateEntity, and sets all <i>Create</i> attributes.
      *
      * @return Returns a new local HlaAggregateEntity.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaAggregateEntityPtr createLocalHlaAggregateEntity(
      ) THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Creates a new HlaAggregateEntity with a specified <code>HLA instance name</code>, and sets all
      * <i>Create</i> attributes.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> to register for this instance
      *
      * @return Returns a new local HlaAggregateEntity.
      *
      * @throws HlaIllegalInstanceNameException if the name is illegal
      * @throws HlaInstanceNameInUseException if the name is already in use
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaAggregateEntityPtr createLocalHlaAggregateEntity(
         const std::wstring& hlaInstanceName
      ) THROW_SPEC (HlaIllegalInstanceNameException, HlaInstanceNameInUseException,
                    HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaAggregateEntity and removes it from the federation.
      *
      * @param aggregateEntity The HlaAggregateEntity to delete
      *
      * @return the HlaAggregateEntity deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaAggregateEntityPtr deleteLocalHlaAggregateEntity(HlaAggregateEntityPtr aggregateEntity)
          THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                      HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaAggregateEntity and removes it from the federation.
      *
      * @param aggregateEntity The HlaAggregateEntity to delete
      * @param timestamp timestamp when the instance was removed.
      *
      * @return the HlaAggregateEntity deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaAggregateEntityPtr deleteLocalHlaAggregateEntity(HlaAggregateEntityPtr aggregateEntity, HlaTimeStampPtr timestamp)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaAggregateEntity and removes it from the federation.
      *
      * @param aggregateEntity The HlaAggregateEntity to delete
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaAggregateEntity deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaAggregateEntityPtr deleteLocalHlaAggregateEntity(HlaAggregateEntityPtr aggregateEntity, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaAggregateEntity and removes it from the federation.
      *
      * @param aggregateEntity The HlaAggregateEntity to delete
      * @param timestamp timestamp when the instance was removed.
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaAggregateEntity deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaAggregateEntityPtr deleteLocalHlaAggregateEntity(HlaAggregateEntityPtr aggregateEntity, HlaTimeStampPtr timestamp, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Add a HlaAggregateEntity manager listener.
      *
      * @param managerListener listener to add
      */
      LIBAPI virtual void addHlaAggregateEntityManagerListener(HlaAggregateEntityManagerListenerPtr managerListener) = 0;

      /**
      * Removed a HlaAggregateEntity manager listener.
      *
      * @param managerListener listener to remove
      */
      LIBAPI virtual void removeHlaAggregateEntityManagerListener(HlaAggregateEntityManagerListenerPtr managerListener) = 0;

     /**
      * Add a listener that will automatically be added to all instances of HlaAggregateEntity (Both local and remote).
      * The listener is notified when any attribute of an instance of HlaAggregateEntity is updated.
      * The listener is also called when an interaction is sent to an instance of HlaAggregateEntity.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param listener The listener to add.
      */
      LIBAPI virtual void addHlaAggregateEntityDefaultInstanceListener(HlaAggregateEntityListenerPtr listener) = 0;

     /**
      * Remove a listener from the set of listeners that are automatically added to new instances of HlaAggregateEntity.
      * Note: The listener will not be removed from already existing instances of HlaAggregateEntity.
      * This method is idempotent.
      *
      * @param listener The listener to remove.
      */
      LIBAPI virtual void removeHlaAggregateEntityDefaultInstanceListener(HlaAggregateEntityListenerPtr listener) = 0;

     /**
      * Add a valueListener that will automatically be added to all instances of HlaAggregateEntity (Both local and remote).
      * The  Valuelistener is notified when any attribute of an instance of HlaAggregateEntity is updated.
      * Note that the attribute values that are notified are not guaranteed to
      * be the latest available values for that attribute.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to add.
      */
      LIBAPI virtual void addHlaAggregateEntityDefaultInstanceValueListener(HlaAggregateEntityValueListenerPtr valueListener) = 0;

     /**
      * Remove a valueListener from the set of listeners that are automatically added to new instances of HlaAggregateEntity.
      * Note: The valueListener will not be removed from already existing instances of HlaAggregateEntity.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to remove.
      */
      LIBAPI virtual void removeHlaAggregateEntityDefaultInstanceValueListener(HlaAggregateEntityValueListenerPtr valueListener) = 0;

     /**
      * Enables or disables the HlaAggregateEntity manager.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaAggregateEntity manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      *
      * @param enabled <code>true</code> to enable the HlaAggregateEntity manager, otherwise <code>false</code>
      */
      LIBAPI virtual void setEnabled(bool enabled) = 0;

     /**
      * Enables the HlaAggregateEntity manager if it is avalable in the FOM at runtime.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaAggregateEntity manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      */
      LIBAPI virtual void setEnabledIfAvailableInFom() = 0;

     /**
      * Check if the HlaAggregateEntity manager is actually enabled when connected.
      * An HlaAggregateEntity manager may be disabled with the <code>setEnabled(false)</code> function, or it may be configured
      * as <i>if available in the FOM</i> by the <code>setEnabledIfAvailableInFom()</code> function and
      * then not available in the FOM at runtime.
      * <p>
      * This method should only be used when connected, it will always return <code>false</code>
      * when not connected.
      *
      * @return <code>true</code> if connected and the HlaAggregateEntity manager is enabled.
      */
      LIBAPI virtual bool isEnabled() = 0;
   };
}
#endif
