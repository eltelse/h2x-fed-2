/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAACTIVESONARBEAMMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLAACTIVESONARBEAMMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaActiveSonarBeam.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaActiveSonarBeam instances.
    */
    class HlaActiveSonarBeamManagerListener {

    public:

        LIBAPI virtual ~HlaActiveSonarBeamManagerListener() {}

        /**
        * This method is called when a new HlaActiveSonarBeam instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param activeSonarBeam the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaActiveSonarBeamDiscovered(HlaActiveSonarBeamPtr activeSonarBeam, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaActiveSonarBeam instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param activeSonarBeam the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaActiveSonarBeamInitialized(HlaActiveSonarBeamPtr activeSonarBeam, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaActiveSonarBeamManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param activeSonarBeam the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaActiveSonarBeamInInterest(HlaActiveSonarBeamPtr activeSonarBeam, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaActiveSonarBeamManagerListener instance goes out of interest.
        *
        * @param activeSonarBeam the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaActiveSonarBeamOutOfInterest(HlaActiveSonarBeamPtr activeSonarBeam, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaActiveSonarBeam instance is deleted.
        *
        * @param activeSonarBeam the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaActiveSonarBeamDeleted(HlaActiveSonarBeamPtr activeSonarBeam, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaActiveSonarBeamManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaActiveSonarBeamManagerListener::Adapter : public HlaActiveSonarBeamManagerListener {

    public:
        LIBAPI virtual void hlaActiveSonarBeamDiscovered(HlaActiveSonarBeamPtr activeSonarBeam, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaActiveSonarBeamInitialized(HlaActiveSonarBeamPtr activeSonarBeam, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaActiveSonarBeamInInterest(HlaActiveSonarBeamPtr activeSonarBeam, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaActiveSonarBeamOutOfInterest(HlaActiveSonarBeamPtr activeSonarBeam, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaActiveSonarBeamDeleted(HlaActiveSonarBeamPtr activeSonarBeam, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
