/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLARRBVALUELISTENER_H
#define DEVELOPER_STUDIO_HLARRBVALUELISTENER_H

#include <memory>

#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EventIdentifierStruct.h>
#include <DevStudio/datatypes/FundamentalParameterDataStruct.h>
#include <DevStudio/datatypes/FundamentalParameterDataStructLengthlessArray.h>
#include <DevStudio/datatypes/IffOperationalParameter1Enum.h>
#include <DevStudio/datatypes/IffOperationalParameter2Enum.h>
#include <DevStudio/datatypes/IffSystemModeEnum.h>
#include <DevStudio/datatypes/IffSystemNameEnum.h>
#include <DevStudio/datatypes/IffSystemTypeEnum.h>
#include <DevStudio/datatypes/RelativePositionStruct.h>
#include <string>
#include <vector>

#include <DevStudio/HlaLogicalTime.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaRRBAttributes.h>    

namespace DevStudio {

   /**
   * Listener for updates of attributes, with the new updated values.  
   */
   class HlaRRBValueListener {

   public:

      LIBAPI virtual ~HlaRRBValueListener() {}
    
      /**
      * This method is called when the attribute <code>code</code> is updated.
      * <br>Description from the FOM: <i>RRB Code (range 0-16)</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param rRB The object which is updated.
      * @param code The new value of the attribute in this update
      * @param validOldCode True if oldCode contains a valid value
      * @param oldCode The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void codeUpdated(HlaRRBPtr rRB, char code, bool validOldCode, char oldCode, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>powerReduction</code> is updated.
      * <br>Description from the FOM: <i>Specifies whether or not power reduction is on.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param rRB The object which is updated.
      * @param powerReduction The new value of the attribute in this update
      * @param validOldPowerReduction True if oldPowerReduction contains a valid value
      * @param oldPowerReduction The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void powerReductionUpdated(HlaRRBPtr rRB, bool powerReduction, bool validOldPowerReduction, bool oldPowerReduction, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>isDamaged</code> is updated.
      * <br>Description from the FOM: <i>Specifies whether or not the system is damaged.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param rRB The object which is updated.
      * @param isDamaged The new value of the attribute in this update
      * @param validOldIsDamaged True if oldIsDamaged contains a valid value
      * @param oldIsDamaged The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void isDamagedUpdated(HlaRRBPtr rRB, bool isDamaged, bool validOldIsDamaged, bool oldIsDamaged, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>isMalfunctioning</code> is updated.
      * <br>Description from the FOM: <i>Specifies whether or not the system is malfunctioning.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param rRB The object which is updated.
      * @param isMalfunctioning The new value of the attribute in this update
      * @param validOldIsMalfunctioning True if oldIsMalfunctioning contains a valid value
      * @param oldIsMalfunctioning The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void isMalfunctioningUpdated(HlaRRBPtr rRB, bool isMalfunctioning, bool validOldIsMalfunctioning, bool oldIsMalfunctioning, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>isOn</code> is updated.
      * <br>Description from the FOM: <i>Specifies whether or not the system is on.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param rRB The object which is updated.
      * @param isOn The new value of the attribute in this update
      * @param validOldIsOn True if oldIsOn contains a valid value
      * @param oldIsOn The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void isOnUpdated(HlaRRBPtr rRB, bool isOn, bool validOldIsOn, bool oldIsOn, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>radarEnhancement</code> is updated.
      * <br>Description from the FOM: <i>Specifies whether or not the radar enhancement is on.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param rRB The object which is updated.
      * @param radarEnhancement The new value of the attribute in this update
      * @param validOldRadarEnhancement True if oldRadarEnhancement contains a valid value
      * @param oldRadarEnhancement The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void radarEnhancementUpdated(HlaRRBPtr rRB, bool radarEnhancement, bool validOldRadarEnhancement, bool oldRadarEnhancement, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>beamAzimuthCenter</code> is updated.
      * <br>Description from the FOM: <i>The azimuth center of the IFF beam's scan volume relative to the IFF system.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @param rRB The object which is updated.
      * @param beamAzimuthCenter The new value of the attribute in this update
      * @param validOldBeamAzimuthCenter True if oldBeamAzimuthCenter contains a valid value
      * @param oldBeamAzimuthCenter The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void beamAzimuthCenterUpdated(HlaRRBPtr rRB, float beamAzimuthCenter, bool validOldBeamAzimuthCenter, float oldBeamAzimuthCenter, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>beamAzimuthSweep</code> is updated.
      * <br>Description from the FOM: <i>The azimuth half-angle of the IFF beam's scan volume relative to the IFF system.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @param rRB The object which is updated.
      * @param beamAzimuthSweep The new value of the attribute in this update
      * @param validOldBeamAzimuthSweep True if oldBeamAzimuthSweep contains a valid value
      * @param oldBeamAzimuthSweep The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void beamAzimuthSweepUpdated(HlaRRBPtr rRB, float beamAzimuthSweep, bool validOldBeamAzimuthSweep, float oldBeamAzimuthSweep, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>beamElevationCenter</code> is updated.
      * <br>Description from the FOM: <i>The elevation center of the IFF beam's scan volume relative to the IFF system.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @param rRB The object which is updated.
      * @param beamElevationCenter The new value of the attribute in this update
      * @param validOldBeamElevationCenter True if oldBeamElevationCenter contains a valid value
      * @param oldBeamElevationCenter The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void beamElevationCenterUpdated(HlaRRBPtr rRB, float beamElevationCenter, bool validOldBeamElevationCenter, float oldBeamElevationCenter, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>beamElevationSweep</code> is updated.
      * <br>Description from the FOM: <i>The elevation half-angle of the IFF beam's scan volume relative to the IFF system.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @param rRB The object which is updated.
      * @param beamElevationSweep The new value of the attribute in this update
      * @param validOldBeamElevationSweep True if oldBeamElevationSweep contains a valid value
      * @param oldBeamElevationSweep The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void beamElevationSweepUpdated(HlaRRBPtr rRB, float beamElevationSweep, bool validOldBeamElevationSweep, float oldBeamElevationSweep, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>beamSweepSync</code> is updated.
      * <br>Description from the FOM: <i>The percentage of time a scan is through its pattern from its origin.</i>
      * <br>Description of the data type from the FOM: <i>Percentage [unit: percent (%), resolution: NA, accuracy: NA]</i>
      *
      * @param rRB The object which is updated.
      * @param beamSweepSync The new value of the attribute in this update
      * @param validOldBeamSweepSync True if oldBeamSweepSync contains a valid value
      * @param oldBeamSweepSync The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void beamSweepSyncUpdated(HlaRRBPtr rRB, float beamSweepSync, bool validOldBeamSweepSync, float oldBeamSweepSync, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>eventIdentifier</code> is updated.
      * <br>Description from the FOM: <i>Used to associate related events.</i>
      * <br>Description of the data type from the FOM: <i>Identification of an event. Based on the Event Identifier record as specified in IEEE 1278.1-1995 section 5.2.18.</i>
      *
      * @param rRB The object which is updated.
      * @param eventIdentifier The new value of the attribute in this update
      * @param validOldEventIdentifier True if oldEventIdentifier contains a valid value
      * @param oldEventIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void eventIdentifierUpdated(HlaRRBPtr rRB, EventIdentifierStruct eventIdentifier, bool validOldEventIdentifier, EventIdentifierStruct oldEventIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>fundamentalParameterData</code> is updated.
      * <br>Description from the FOM: <i>The fundamental energy radiation characteristics of the IFF/ATC/NAVAIDS system.</i>
      * <br>Description of the data type from the FOM: <i>Array of Fundamental Parameter Data records.</i>
      *
      * @param rRB The object which is updated.
      * @param fundamentalParameterData The new value of the attribute in this update
      * @param validOldFundamentalParameterData True if oldFundamentalParameterData contains a valid value
      * @param oldFundamentalParameterData The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void fundamentalParameterDataUpdated(HlaRRBPtr rRB, std::vector<DevStudio::FundamentalParameterDataStruct > fundamentalParameterData, bool validOldFundamentalParameterData, std::vector<DevStudio::FundamentalParameterDataStruct > oldFundamentalParameterData, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>layer2DataAvailable</code> is updated.
      * <br>Description from the FOM: <i>Specifies if level 2 data is available for this IFF system. If level 2 data is available then the BeamAzimuthCenter, BeamAzimuthSweep, BeamElevationCenter, BeamElevationSweep, BeamSweepSync, FundamentalParameterData, SecondaryOperationalDataParameter1, and SecondaryOperationalDataParameter2 attributes shall be generated.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param rRB The object which is updated.
      * @param layer2DataAvailable The new value of the attribute in this update
      * @param validOldLayer2DataAvailable True if oldLayer2DataAvailable contains a valid value
      * @param oldLayer2DataAvailable The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void layer2DataAvailableUpdated(HlaRRBPtr rRB, bool layer2DataAvailable, bool validOldLayer2DataAvailable, bool oldLayer2DataAvailable, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>secondaryOperationalDataParameter1</code> is updated.
      * <br>Description from the FOM: <i>Additional characteristics of the IFF/ATC/NAVAIDS emitting system.</i>
      * <br>Description of the data type from the FOM: <i>IFF operational parameter 1</i>
      *
      * @param rRB The object which is updated.
      * @param secondaryOperationalDataParameter1 The new value of the attribute in this update
      * @param validOldSecondaryOperationalDataParameter1 True if oldSecondaryOperationalDataParameter1 contains a valid value
      * @param oldSecondaryOperationalDataParameter1 The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void secondaryOperationalDataParameter1Updated(HlaRRBPtr rRB, IffOperationalParameter1Enum::IffOperationalParameter1Enum secondaryOperationalDataParameter1, bool validOldSecondaryOperationalDataParameter1, IffOperationalParameter1Enum::IffOperationalParameter1Enum oldSecondaryOperationalDataParameter1, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>secondaryOperationalDataParameter2</code> is updated.
      * <br>Description from the FOM: <i>Additional characteristics of the IFF/ATC/NAVAIDS emitting system.</i>
      * <br>Description of the data type from the FOM: <i>IFF operational parameter 2</i>
      *
      * @param rRB The object which is updated.
      * @param secondaryOperationalDataParameter2 The new value of the attribute in this update
      * @param validOldSecondaryOperationalDataParameter2 True if oldSecondaryOperationalDataParameter2 contains a valid value
      * @param oldSecondaryOperationalDataParameter2 The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void secondaryOperationalDataParameter2Updated(HlaRRBPtr rRB, IffOperationalParameter2Enum::IffOperationalParameter2Enum secondaryOperationalDataParameter2, bool validOldSecondaryOperationalDataParameter2, IffOperationalParameter2Enum::IffOperationalParameter2Enum oldSecondaryOperationalDataParameter2, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>systemMode</code> is updated.
      * <br>Description from the FOM: <i>Mode of operation.</i>
      * <br>Description of the data type from the FOM: <i>IFF system mode</i>
      *
      * @param rRB The object which is updated.
      * @param systemMode The new value of the attribute in this update
      * @param validOldSystemMode True if oldSystemMode contains a valid value
      * @param oldSystemMode The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void systemModeUpdated(HlaRRBPtr rRB, IffSystemModeEnum::IffSystemModeEnum systemMode, bool validOldSystemMode, IffSystemModeEnum::IffSystemModeEnum oldSystemMode, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>systemName</code> is updated.
      * <br>Description from the FOM: <i>Particular named type of the IFF system in use.</i>
      * <br>Description of the data type from the FOM: <i>IFF system name</i>
      *
      * @param rRB The object which is updated.
      * @param systemName The new value of the attribute in this update
      * @param validOldSystemName True if oldSystemName contains a valid value
      * @param oldSystemName The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void systemNameUpdated(HlaRRBPtr rRB, IffSystemNameEnum::IffSystemNameEnum systemName, bool validOldSystemName, IffSystemNameEnum::IffSystemNameEnum oldSystemName, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>systemType</code> is updated.
      * <br>Description from the FOM: <i>General type of IFF system in use.</i>
      * <br>Description of the data type from the FOM: <i>IFF system type</i>
      *
      * @param rRB The object which is updated.
      * @param systemType The new value of the attribute in this update
      * @param validOldSystemType True if oldSystemType contains a valid value
      * @param oldSystemType The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void systemTypeUpdated(HlaRRBPtr rRB, IffSystemTypeEnum::IffSystemTypeEnum systemType, bool validOldSystemType, IffSystemTypeEnum::IffSystemTypeEnum oldSystemType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>systemIsOn</code> is updated.
      * <br>Description from the FOM: <i>Whether or not the system is on.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param rRB The object which is updated.
      * @param systemIsOn The new value of the attribute in this update
      * @param validOldSystemIsOn True if oldSystemIsOn contains a valid value
      * @param oldSystemIsOn The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void systemIsOnUpdated(HlaRRBPtr rRB, bool systemIsOn, bool validOldSystemIsOn, bool oldSystemIsOn, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>systemIsOperational</code> is updated.
      * <br>Description from the FOM: <i>Whether or not the system is operational.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param rRB The object which is updated.
      * @param systemIsOperational The new value of the attribute in this update
      * @param validOldSystemIsOperational True if oldSystemIsOperational contains a valid value
      * @param oldSystemIsOperational The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void systemIsOperationalUpdated(HlaRRBPtr rRB, bool systemIsOperational, bool validOldSystemIsOperational, bool oldSystemIsOperational, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>entityIdentifier</code> is updated.
      * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @param rRB The object which is updated.
      * @param entityIdentifier The new value of the attribute in this update
      * @param validOldEntityIdentifier True if oldEntityIdentifier contains a valid value
      * @param oldEntityIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void entityIdentifierUpdated(HlaRRBPtr rRB, EntityIdentifierStruct entityIdentifier, bool validOldEntityIdentifier, EntityIdentifierStruct oldEntityIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>hostObjectIdentifier</code> is updated.
      * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @param rRB The object which is updated.
      * @param hostObjectIdentifier The new value of the attribute in this update
      * @param validOldHostObjectIdentifier True if oldHostObjectIdentifier contains a valid value
      * @param oldHostObjectIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void hostObjectIdentifierUpdated(HlaRRBPtr rRB, std::string hostObjectIdentifier, bool validOldHostObjectIdentifier, std::string oldHostObjectIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>relativePosition</code> is updated.
      * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
      * <br>Description of the data type from the FOM: <i>Relative position in right-handed Cartesian coordinates.</i>
      *
      * @param rRB The object which is updated.
      * @param relativePosition The new value of the attribute in this update
      * @param validOldRelativePosition True if oldRelativePosition contains a valid value
      * @param oldRelativePosition The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void relativePositionUpdated(HlaRRBPtr rRB, RelativePositionStruct relativePosition, bool validOldRelativePosition, RelativePositionStruct oldRelativePosition, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
      /**
      * This method is called when the producing federate of an attribute is changed.
      * Only available when using HLA Evolved.
      *
      * @param rRB The object which is updated.
      * @param attribute The attribute that now has a new producing federate
      * @param oldProducingFederate The federate handle of the old producing federate
      * @param newProducingFederate The federate handle of the new producing federate
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void producingFederateUpdated(HlaRRBPtr rRB, HlaRRBAttributes::Attribute attribute, HlaFederateIdPtr oldProducingFederate, HlaFederateIdPtr newProducingFederate, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;

      class Adapter;
   };

   /**
   * An adapter class that implements the HlaRRBValueListener interface with empty methods.
   * It might be used as a base class for a listener.
   */
   class HlaRRBValueListener::Adapter : public HlaRRBValueListener {

   public:

      LIBAPI virtual void codeUpdated(HlaRRBPtr rRB, char code, bool validOldCode, char oldCode, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void powerReductionUpdated(HlaRRBPtr rRB, bool powerReduction, bool validOldPowerReduction, bool oldPowerReduction, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void isDamagedUpdated(HlaRRBPtr rRB, bool isDamaged, bool validOldIsDamaged, bool oldIsDamaged, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void isMalfunctioningUpdated(HlaRRBPtr rRB, bool isMalfunctioning, bool validOldIsMalfunctioning, bool oldIsMalfunctioning, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void isOnUpdated(HlaRRBPtr rRB, bool isOn, bool validOldIsOn, bool oldIsOn, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void radarEnhancementUpdated(HlaRRBPtr rRB, bool radarEnhancement, bool validOldRadarEnhancement, bool oldRadarEnhancement, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void beamAzimuthCenterUpdated(HlaRRBPtr rRB, float beamAzimuthCenter, bool validOldBeamAzimuthCenter, float oldBeamAzimuthCenter, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void beamAzimuthSweepUpdated(HlaRRBPtr rRB, float beamAzimuthSweep, bool validOldBeamAzimuthSweep, float oldBeamAzimuthSweep, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void beamElevationCenterUpdated(HlaRRBPtr rRB, float beamElevationCenter, bool validOldBeamElevationCenter, float oldBeamElevationCenter, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void beamElevationSweepUpdated(HlaRRBPtr rRB, float beamElevationSweep, bool validOldBeamElevationSweep, float oldBeamElevationSweep, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void beamSweepSyncUpdated(HlaRRBPtr rRB, float beamSweepSync, bool validOldBeamSweepSync, float oldBeamSweepSync, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void eventIdentifierUpdated(HlaRRBPtr rRB, EventIdentifierStruct eventIdentifier, bool validOldEventIdentifier, EventIdentifierStruct oldEventIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void fundamentalParameterDataUpdated(HlaRRBPtr rRB, std::vector<DevStudio::FundamentalParameterDataStruct > fundamentalParameterData, bool validOldFundamentalParameterData, std::vector<DevStudio::FundamentalParameterDataStruct > oldFundamentalParameterData, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void layer2DataAvailableUpdated(HlaRRBPtr rRB, bool layer2DataAvailable, bool validOldLayer2DataAvailable, bool oldLayer2DataAvailable, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void secondaryOperationalDataParameter1Updated(HlaRRBPtr rRB, IffOperationalParameter1Enum::IffOperationalParameter1Enum secondaryOperationalDataParameter1, bool validOldSecondaryOperationalDataParameter1, IffOperationalParameter1Enum::IffOperationalParameter1Enum oldSecondaryOperationalDataParameter1, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void secondaryOperationalDataParameter2Updated(HlaRRBPtr rRB, IffOperationalParameter2Enum::IffOperationalParameter2Enum secondaryOperationalDataParameter2, bool validOldSecondaryOperationalDataParameter2, IffOperationalParameter2Enum::IffOperationalParameter2Enum oldSecondaryOperationalDataParameter2, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void systemModeUpdated(HlaRRBPtr rRB, IffSystemModeEnum::IffSystemModeEnum systemMode, bool validOldSystemMode, IffSystemModeEnum::IffSystemModeEnum oldSystemMode, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void systemNameUpdated(HlaRRBPtr rRB, IffSystemNameEnum::IffSystemNameEnum systemName, bool validOldSystemName, IffSystemNameEnum::IffSystemNameEnum oldSystemName, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void systemTypeUpdated(HlaRRBPtr rRB, IffSystemTypeEnum::IffSystemTypeEnum systemType, bool validOldSystemType, IffSystemTypeEnum::IffSystemTypeEnum oldSystemType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void systemIsOnUpdated(HlaRRBPtr rRB, bool systemIsOn, bool validOldSystemIsOn, bool oldSystemIsOn, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void systemIsOperationalUpdated(HlaRRBPtr rRB, bool systemIsOperational, bool validOldSystemIsOperational, bool oldSystemIsOperational, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void entityIdentifierUpdated(HlaRRBPtr rRB, EntityIdentifierStruct entityIdentifier, bool validOldEntityIdentifier, EntityIdentifierStruct oldEntityIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void hostObjectIdentifierUpdated(HlaRRBPtr rRB, std::string hostObjectIdentifier, bool validOldHostObjectIdentifier, std::string oldHostObjectIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void relativePositionUpdated(HlaRRBPtr rRB, RelativePositionStruct relativePosition, bool validOldRelativePosition, RelativePositionStruct oldRelativePosition, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
      LIBAPI virtual void producingFederateUpdated(HlaRRBPtr rRB, HlaRRBAttributes::Attribute attribute, HlaFederateIdPtr oldProducingFederate, HlaFederateIdPtr newProducingFederate, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
   };

}
#endif
