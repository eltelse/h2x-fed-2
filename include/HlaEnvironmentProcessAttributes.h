/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAENVIRONMENTPROCESSATTRIBUTES_H
#define DEVELOPER_STUDIO_HLAENVIRONMENTPROCESSATTRIBUTES_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <string>
#include <vector>
#include <utility>
    
#include <boost/noncopyable.hpp>
    
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EnvironmentModelTypeEnum.h>
#include <DevStudio/datatypes/EnvironmentRecStruct.h>
#include <DevStudio/datatypes/EnvironmentRecStructArray.h>
#include <DevStudio/datatypes/EnvironmentTypeStruct.h>
#include <vector>

#include <DevStudio/HlaException.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaHLAobjectRootAttributes.h>
#include <DevStudio/HlaTimeStamped.h>

namespace DevStudio {

   /**
   * Interface used to represent all attributes for an object instance.
   */
   class HlaEnvironmentProcessAttributes : public HlaHLAobjectRootAttributes {
   public:


     /**
      * An enumeration of the attributes of an HlaEnvironmentProcess
      *
      *<table summary="All attributes">
      * <tr><td><b>Enum constant</b></td><td><b>C++ name</b></td><td><b>FOM name</b></td></tr>
      * <tr><td>PROCESS_IDENTIFIER</td><td>processIdentifier</td><td><code>ProcessIdentifier</code></td></tr>
      * <tr><td>TYPE</td><td>type</td><td><code>Type</code></td></tr>
      * <tr><td>MODEL_TYPE</td><td>modelType</td><td><code>ModelType</code></td></tr>
      * <tr><td>ENVIRONMENT_PROCESS_ACTIVE</td><td>environmentProcessActive</td><td><code>EnvironmentProcessActive</code></td></tr>
      * <tr><td>SEQUENCE_NUMBER</td><td>sequenceNumber</td><td><code>SequenceNumber</code></td></tr>
      * <tr><td>ENVIRONMENT_REC_DATA</td><td>environmentRecData</td><td><code>EnvironmentRecData</code></td></tr>
      *</table>
      */
      enum Attribute {
        /**
        * processIdentifier (FOM name: <code>ProcessIdentifier</code>).
        * <br>Description from the FOM: <i>Identifies which process issued the environmental process update</i>
        */
         PROCESS_IDENTIFIER,

        /**
        * type (FOM name: <code>Type</code>).
        * <br>Description from the FOM: <i>Identifies the environmental process type</i>
        */
         TYPE,

        /**
        * modelType (FOM name: <code>ModelType</code>).
        * <br>Description from the FOM: <i>Identifies the model used for generating this environmental process update ; defined in 1278.1a as being exercise specific</i>
        */
         MODEL_TYPE,

        /**
        * environmentProcessActive (FOM name: <code>EnvironmentProcessActive</code>).
        * <br>Description from the FOM: <i>Specifies the status of the environmental process (active or inactive)</i>
        */
         ENVIRONMENT_PROCESS_ACTIVE,

        /**
        * sequenceNumber (FOM name: <code>SequenceNumber</code>).
        * <br>Description from the FOM: <i>Optional, used to support update sequencing ; if not used, set to EP_NO_SEQUENCE ; when used, set to zero for each exercise and incremented by one for each update sent</i>
        */
         SEQUENCE_NUMBER,

        /**
        * environmentRecData (FOM name: <code>EnvironmentRecData</code>).
        * <br>Description from the FOM: <i>Specifies environment records</i>
        */
         ENVIRONMENT_REC_DATA
      };

     /**
      * Gets the name of the attribute.
      *
      * @return The name of the attribute. An empty string will be returned if the attribute does not exist.
      */
      LIBAPI virtual std::wstring getName(Attribute attribute);

     /**
      * Finds the attribute specified in the parameter attributeName.
      * The found enumeration will be stored in the parameter attribute.
      *
      * @return true if the attribute was found, false otherwise.
      */
      LIBAPI virtual bool find(Attribute& attribute, std::wstring attributeName);

      LIBAPI virtual ~HlaEnvironmentProcessAttributes() {}
    
      /**
      * Returns true if the <code>processIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Identifies which process issued the environmental process update</i>
      *
      * @return true if <code>processIdentifier</code> is available.
      */
      LIBAPI virtual bool hasProcessIdentifier() = 0;

      /**
      * Gets the value of the <code>processIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>Identifies which process issued the environmental process update</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @return the <code>processIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::EntityIdentifierStruct getProcessIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>processIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Identifies which process issued the environmental process update</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>processIdentifier</code> attribute.
      */
      LIBAPI virtual DevStudio::EntityIdentifierStruct getProcessIdentifier(DevStudio::EntityIdentifierStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>processIdentifier</code> attribute.
      * <br>Description from the FOM: <i>Identifies which process issued the environmental process update</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @return the time stamped <code>processIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::EntityIdentifierStruct > getProcessIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>type</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Identifies the environmental process type</i>
      *
      * @return true if <code>type</code> is available.
      */
      LIBAPI virtual bool hasType() = 0;

      /**
      * Gets the value of the <code>type</code> attribute.
      *
      * <br>Description from the FOM: <i>Identifies the environmental process type</i>
      * <br>Description of the data type from the FOM: <i>Record specifying the kind of environment, the domain and any extra information necessary for describing the environmental entity</i>
      *
      * @return the <code>type</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::EnvironmentTypeStruct getType()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>type</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Identifies the environmental process type</i>
      * <br>Description of the data type from the FOM: <i>Record specifying the kind of environment, the domain and any extra information necessary for describing the environmental entity</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>type</code> attribute.
      */
      LIBAPI virtual DevStudio::EnvironmentTypeStruct getType(DevStudio::EnvironmentTypeStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>type</code> attribute.
      * <br>Description from the FOM: <i>Identifies the environmental process type</i>
      * <br>Description of the data type from the FOM: <i>Record specifying the kind of environment, the domain and any extra information necessary for describing the environmental entity</i>
      *
      * @return the time stamped <code>type</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::EnvironmentTypeStruct > getTypeTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>modelType</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Identifies the model used for generating this environmental process update ; defined in 1278.1a as being exercise specific</i>
      *
      * @return true if <code>modelType</code> is available.
      */
      LIBAPI virtual bool hasModelType() = 0;

      /**
      * Gets the value of the <code>modelType</code> attribute.
      *
      * <br>Description from the FOM: <i>Identifies the model used for generating this environmental process update ; defined in 1278.1a as being exercise specific</i>
      * <br>Description of the data type from the FOM: <i>Environment process model type</i>
      *
      * @return the <code>modelType</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::EnvironmentModelTypeEnum::EnvironmentModelTypeEnum getModelType()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>modelType</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Identifies the model used for generating this environmental process update ; defined in 1278.1a as being exercise specific</i>
      * <br>Description of the data type from the FOM: <i>Environment process model type</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>modelType</code> attribute.
      */
      LIBAPI virtual DevStudio::EnvironmentModelTypeEnum::EnvironmentModelTypeEnum getModelType(DevStudio::EnvironmentModelTypeEnum::EnvironmentModelTypeEnum defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>modelType</code> attribute.
      * <br>Description from the FOM: <i>Identifies the model used for generating this environmental process update ; defined in 1278.1a as being exercise specific</i>
      * <br>Description of the data type from the FOM: <i>Environment process model type</i>
      *
      * @return the time stamped <code>modelType</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::EnvironmentModelTypeEnum::EnvironmentModelTypeEnum > getModelTypeTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>environmentProcessActive</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies the status of the environmental process (active or inactive)</i>
      *
      * @return true if <code>environmentProcessActive</code> is available.
      */
      LIBAPI virtual bool hasEnvironmentProcessActive() = 0;

      /**
      * Gets the value of the <code>environmentProcessActive</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies the status of the environmental process (active or inactive)</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>environmentProcessActive</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual bool getEnvironmentProcessActive()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>environmentProcessActive</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies the status of the environmental process (active or inactive)</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>environmentProcessActive</code> attribute.
      */
      LIBAPI virtual bool getEnvironmentProcessActive(bool defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>environmentProcessActive</code> attribute.
      * <br>Description from the FOM: <i>Specifies the status of the environmental process (active or inactive)</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>environmentProcessActive</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< bool > getEnvironmentProcessActiveTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>sequenceNumber</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Optional, used to support update sequencing ; if not used, set to EP_NO_SEQUENCE ; when used, set to zero for each exercise and incremented by one for each update sent</i>
      *
      * @return true if <code>sequenceNumber</code> is available.
      */
      LIBAPI virtual bool hasSequenceNumber() = 0;

      /**
      * Gets the value of the <code>sequenceNumber</code> attribute.
      *
      * <br>Description from the FOM: <i>Optional, used to support update sequencing ; if not used, set to EP_NO_SEQUENCE ; when used, set to zero for each exercise and incremented by one for each update sent</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the <code>sequenceNumber</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual unsigned short getSequenceNumber()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>sequenceNumber</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Optional, used to support update sequencing ; if not used, set to EP_NO_SEQUENCE ; when used, set to zero for each exercise and incremented by one for each update sent</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>sequenceNumber</code> attribute.
      */
      LIBAPI virtual unsigned short getSequenceNumber(unsigned short defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>sequenceNumber</code> attribute.
      * <br>Description from the FOM: <i>Optional, used to support update sequencing ; if not used, set to EP_NO_SEQUENCE ; when used, set to zero for each exercise and incremented by one for each update sent</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the time stamped <code>sequenceNumber</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< unsigned short > getSequenceNumberTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>environmentRecData</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies environment records</i>
      *
      * @return true if <code>environmentRecData</code> is available.
      */
      LIBAPI virtual bool hasEnvironmentRecData() = 0;

      /**
      * Gets the value of the <code>environmentRecData</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies environment records</i>
      * <br>Description of the data type from the FOM: <i>Specifies environment records as a collection of geometry and state records</i>
      *
      * @return the <code>environmentRecData</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::vector<DevStudio::EnvironmentRecStruct > getEnvironmentRecData()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>environmentRecData</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies environment records</i>
      * <br>Description of the data type from the FOM: <i>Specifies environment records as a collection of geometry and state records</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>environmentRecData</code> attribute.
      */
      LIBAPI virtual std::vector<DevStudio::EnvironmentRecStruct > getEnvironmentRecData(std::vector<DevStudio::EnvironmentRecStruct > defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>environmentRecData</code> attribute.
      * <br>Description from the FOM: <i>Specifies environment records</i>
      * <br>Description of the data type from the FOM: <i>Specifies environment records as a collection of geometry and state records</i>
      *
      * @return the time stamped <code>environmentRecData</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::vector<DevStudio::EnvironmentRecStruct > > getEnvironmentRecDataTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
   };
}
#endif
