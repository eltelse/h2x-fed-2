/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAEMITTERSYSTEMVALUELISTENER_H
#define DEVELOPER_STUDIO_HLAEMITTERSYSTEMVALUELISTENER_H

#include <memory>

#include <DevStudio/datatypes/EmitterFunctionEnum.h>
#include <DevStudio/datatypes/EmitterTypeEnum.h>
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EventIdentifierStruct.h>
#include <DevStudio/datatypes/RelativePositionStruct.h>
#include <string>

#include <DevStudio/HlaLogicalTime.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaEmitterSystemAttributes.h>    

namespace DevStudio {

   /**
   * Listener for updates of attributes, with the new updated values.  
   */
   class HlaEmitterSystemValueListener {

   public:

      LIBAPI virtual ~HlaEmitterSystemValueListener() {}
    
      /**
      * This method is called when the attribute <code>emitterFunctionCode</code> is updated.
      * <br>Description from the FOM: <i>The function of the emitter system.</i>
      * <br>Description of the data type from the FOM: <i>Emitter system function</i>
      *
      * @param emitterSystem The object which is updated.
      * @param emitterFunctionCode The new value of the attribute in this update
      * @param validOldEmitterFunctionCode True if oldEmitterFunctionCode contains a valid value
      * @param oldEmitterFunctionCode The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void emitterFunctionCodeUpdated(HlaEmitterSystemPtr emitterSystem, EmitterFunctionEnum::EmitterFunctionEnum emitterFunctionCode, bool validOldEmitterFunctionCode, EmitterFunctionEnum::EmitterFunctionEnum oldEmitterFunctionCode, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>emitterType</code> is updated.
      * <br>Description from the FOM: <i>The type of the emitter system.</i>
      * <br>Description of the data type from the FOM: <i>Emitter name</i>
      *
      * @param emitterSystem The object which is updated.
      * @param emitterType The new value of the attribute in this update
      * @param validOldEmitterType True if oldEmitterType contains a valid value
      * @param oldEmitterType The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void emitterTypeUpdated(HlaEmitterSystemPtr emitterSystem, EmitterTypeEnum::EmitterTypeEnum emitterType, bool validOldEmitterType, EmitterTypeEnum::EmitterTypeEnum oldEmitterType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>emitterIndex</code> is updated.
      * <br>Description from the FOM: <i>A unique number, which uniquely identifies the emitter system from other on the same host entity.</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param emitterSystem The object which is updated.
      * @param emitterIndex The new value of the attribute in this update
      * @param validOldEmitterIndex True if oldEmitterIndex contains a valid value
      * @param oldEmitterIndex The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void emitterIndexUpdated(HlaEmitterSystemPtr emitterSystem, char emitterIndex, bool validOldEmitterIndex, char oldEmitterIndex, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>eventIdentifier</code> is updated.
      * <br>Description from the FOM: <i>The EventIdentifier is used by the generating federate to associate related events. The event number shall start at one at the beginning of the exercise, and be incremented by one for each event.</i>
      * <br>Description of the data type from the FOM: <i>Identification of an event. Based on the Event Identifier record as specified in IEEE 1278.1-1995 section 5.2.18.</i>
      *
      * @param emitterSystem The object which is updated.
      * @param eventIdentifier The new value of the attribute in this update
      * @param validOldEventIdentifier True if oldEventIdentifier contains a valid value
      * @param oldEventIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void eventIdentifierUpdated(HlaEmitterSystemPtr emitterSystem, EventIdentifierStruct eventIdentifier, bool validOldEventIdentifier, EventIdentifierStruct oldEventIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>entityIdentifier</code> is updated.
      * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @param emitterSystem The object which is updated.
      * @param entityIdentifier The new value of the attribute in this update
      * @param validOldEntityIdentifier True if oldEntityIdentifier contains a valid value
      * @param oldEntityIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void entityIdentifierUpdated(HlaEmitterSystemPtr emitterSystem, EntityIdentifierStruct entityIdentifier, bool validOldEntityIdentifier, EntityIdentifierStruct oldEntityIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>hostObjectIdentifier</code> is updated.
      * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @param emitterSystem The object which is updated.
      * @param hostObjectIdentifier The new value of the attribute in this update
      * @param validOldHostObjectIdentifier True if oldHostObjectIdentifier contains a valid value
      * @param oldHostObjectIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void hostObjectIdentifierUpdated(HlaEmitterSystemPtr emitterSystem, std::string hostObjectIdentifier, bool validOldHostObjectIdentifier, std::string oldHostObjectIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>relativePosition</code> is updated.
      * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
      * <br>Description of the data type from the FOM: <i>Relative position in right-handed Cartesian coordinates.</i>
      *
      * @param emitterSystem The object which is updated.
      * @param relativePosition The new value of the attribute in this update
      * @param validOldRelativePosition True if oldRelativePosition contains a valid value
      * @param oldRelativePosition The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void relativePositionUpdated(HlaEmitterSystemPtr emitterSystem, RelativePositionStruct relativePosition, bool validOldRelativePosition, RelativePositionStruct oldRelativePosition, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
      /**
      * This method is called when the producing federate of an attribute is changed.
      * Only available when using HLA Evolved.
      *
      * @param emitterSystem The object which is updated.
      * @param attribute The attribute that now has a new producing federate
      * @param oldProducingFederate The federate handle of the old producing federate
      * @param newProducingFederate The federate handle of the new producing federate
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void producingFederateUpdated(HlaEmitterSystemPtr emitterSystem, HlaEmitterSystemAttributes::Attribute attribute, HlaFederateIdPtr oldProducingFederate, HlaFederateIdPtr newProducingFederate, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;

      class Adapter;
   };

   /**
   * An adapter class that implements the HlaEmitterSystemValueListener interface with empty methods.
   * It might be used as a base class for a listener.
   */
   class HlaEmitterSystemValueListener::Adapter : public HlaEmitterSystemValueListener {

   public:

      LIBAPI virtual void emitterFunctionCodeUpdated(HlaEmitterSystemPtr emitterSystem, EmitterFunctionEnum::EmitterFunctionEnum emitterFunctionCode, bool validOldEmitterFunctionCode, EmitterFunctionEnum::EmitterFunctionEnum oldEmitterFunctionCode, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void emitterTypeUpdated(HlaEmitterSystemPtr emitterSystem, EmitterTypeEnum::EmitterTypeEnum emitterType, bool validOldEmitterType, EmitterTypeEnum::EmitterTypeEnum oldEmitterType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void emitterIndexUpdated(HlaEmitterSystemPtr emitterSystem, char emitterIndex, bool validOldEmitterIndex, char oldEmitterIndex, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void eventIdentifierUpdated(HlaEmitterSystemPtr emitterSystem, EventIdentifierStruct eventIdentifier, bool validOldEventIdentifier, EventIdentifierStruct oldEventIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void entityIdentifierUpdated(HlaEmitterSystemPtr emitterSystem, EntityIdentifierStruct entityIdentifier, bool validOldEntityIdentifier, EntityIdentifierStruct oldEntityIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void hostObjectIdentifierUpdated(HlaEmitterSystemPtr emitterSystem, std::string hostObjectIdentifier, bool validOldHostObjectIdentifier, std::string oldHostObjectIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void relativePositionUpdated(HlaEmitterSystemPtr emitterSystem, RelativePositionStruct relativePosition, bool validOldRelativePosition, RelativePositionStruct oldRelativePosition, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
      LIBAPI virtual void producingFederateUpdated(HlaEmitterSystemPtr emitterSystem, HlaEmitterSystemAttributes::Attribute attribute, HlaFederateIdPtr oldProducingFederate, HlaFederateIdPtr newProducingFederate, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
   };

}
#endif
