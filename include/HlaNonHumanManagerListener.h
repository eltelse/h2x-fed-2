/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLANONHUMANMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLANONHUMANMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaNonHuman.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaNonHuman instances.
    */
    class HlaNonHumanManagerListener {

    public:

        LIBAPI virtual ~HlaNonHumanManagerListener() {}

        /**
        * This method is called when a new HlaNonHuman instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param nonHuman the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaNonHumanDiscovered(HlaNonHumanPtr nonHuman, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaNonHuman instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param nonHuman the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaNonHumanInitialized(HlaNonHumanPtr nonHuman, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaNonHumanManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param nonHuman the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaNonHumanInInterest(HlaNonHumanPtr nonHuman, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaNonHumanManagerListener instance goes out of interest.
        *
        * @param nonHuman the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaNonHumanOutOfInterest(HlaNonHumanPtr nonHuman, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaNonHuman instance is deleted.
        *
        * @param nonHuman the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaNonHumanDeleted(HlaNonHumanPtr nonHuman, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaNonHumanManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaNonHumanManagerListener::Adapter : public HlaNonHumanManagerListener {

    public:
        LIBAPI virtual void hlaNonHumanDiscovered(HlaNonHumanPtr nonHuman, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaNonHumanInitialized(HlaNonHumanPtr nonHuman, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaNonHumanInInterest(HlaNonHumanPtr nonHuman, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaNonHumanOutOfInterest(HlaNonHumanPtr nonHuman, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaNonHumanDeleted(HlaNonHumanPtr nonHuman, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
