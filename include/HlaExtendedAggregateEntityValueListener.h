/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAEXTENDEDAGGREGATEENTITYVALUELISTENER_H
#define DEVELOPER_STUDIO_HLAEXTENDEDAGGREGATEENTITYVALUELISTENER_H

#include <memory>

#include <DevStudio/datatypes/ActivityStatusEnum.h>
#include <DevStudio/datatypes/AggregateMarkingStruct.h>
#include <DevStudio/datatypes/AggregateSizeStruct.h>
#include <DevStudio/datatypes/AggregateStateEnum.h>
#include <DevStudio/datatypes/DimensionStruct.h>
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EntityTypeStruct.h>
#include <DevStudio/datatypes/EntityVirtualityTypeEnum.h>
#include <DevStudio/datatypes/ForceIdentifierEnum.h>
#include <DevStudio/datatypes/FormationEnum.h>
#include <DevStudio/datatypes/FortificationsEnum.h>
#include <DevStudio/datatypes/GeographicShapeStruct.h>
#include <DevStudio/datatypes/IsPartOfStruct.h>
#include <DevStudio/datatypes/OperationalCompetenceEnum.h>
#include <DevStudio/datatypes/PositionStruct.h>
#include <DevStudio/datatypes/RTIobjectIdArray.h>
#include <DevStudio/datatypes/SilentAggregateStruct.h>
#include <DevStudio/datatypes/SilentAggregateStructLengthlessArray.h>
#include <DevStudio/datatypes/SilentEntityStruct.h>
#include <DevStudio/datatypes/SilentEntityStructLengthlessArray.h>
#include <DevStudio/datatypes/SpatialVariantStruct.h>
#include <DevStudio/datatypes/VariableDatumStruct.h>
#include <DevStudio/datatypes/VariableDatumStructLengthlessArray.h>
#include <DevStudio/datatypes/WorldLocationStruct.h>
#include <string>
#include <vector>

#include <DevStudio/HlaLogicalTime.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaExtendedAggregateEntityAttributes.h>    

namespace DevStudio {

   /**
   * Listener for updates of attributes, with the new updated values.  
   */
   class HlaExtendedAggregateEntityValueListener {

   public:

      LIBAPI virtual ~HlaExtendedAggregateEntityValueListener() {}
    
      /**
      * This method is called when the attribute <code>c2Identifier</code> is updated.
      * <br>Description from the FOM: <i>Internal C2 identifier</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @param extendedAggregateEntity The object which is updated.
      * @param c2Identifier The new value of the attribute in this update
      * @param validOldC2Identifier True if oldC2Identifier contains a valid value
      * @param oldC2Identifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void c2IdentifierUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, std::string c2Identifier, bool validOldC2Identifier, std::string oldC2Identifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>playerID</code> is updated.
      * <br>Description from the FOM: <i>IDF player ID.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param extendedAggregateEntity The object which is updated.
      * @param playerID The new value of the attribute in this update
      * @param validOldPlayerID True if oldPlayerID contains a valid value
      * @param oldPlayerID The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void playerIDUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, std::wstring playerID, bool validOldPlayerID, std::wstring oldPlayerID, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>alias</code> is updated.
      * <br>Description from the FOM: <i>Defines the aggregate name.(Max 17 characters).</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param extendedAggregateEntity The object which is updated.
      * @param alias The new value of the attribute in this update
      * @param validOldAlias True if oldAlias contains a valid value
      * @param oldAlias The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void aliasUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, std::wstring alias, bool validOldAlias, std::wstring oldAlias, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>aggregateSize</code> is updated.
      * <br>Description from the FOM: <i>The size of force and reinforcement level .</i>
      * <br>Description of the data type from the FOM: <i>-NULL-</i>
      *
      * @param extendedAggregateEntity The object which is updated.
      * @param aggregateSize The new value of the attribute in this update
      * @param validOldAggregateSize True if oldAggregateSize contains a valid value
      * @param oldAggregateSize The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void aggregateSizeUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, AggregateSizeStruct aggregateSize, bool validOldAggregateSize, AggregateSizeStruct oldAggregateSize, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>lastUpdateTime</code> is updated.
      * <br>Description from the FOM: <i>The last time the entity was updated.</i>
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      *
      * @param extendedAggregateEntity The object which is updated.
      * @param lastUpdateTime The new value of the attribute in this update
      * @param validOldLastUpdateTime True if oldLastUpdateTime contains a valid value
      * @param oldLastUpdateTime The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void lastUpdateTimeUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, short lastUpdateTime, bool validOldLastUpdateTime, short oldLastUpdateTime, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>activityStatus</code> is updated.
      * <br>Description from the FOM: <i>The activity status.</i>
      * <br>Description of the data type from the FOM: <i>-NULL-</i>
      *
      * @param extendedAggregateEntity The object which is updated.
      * @param activityStatus The new value of the attribute in this update
      * @param validOldActivityStatus True if oldActivityStatus contains a valid value
      * @param oldActivityStatus The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void activityStatusUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, ActivityStatusEnum::ActivityStatusEnum activityStatus, bool validOldActivityStatus, ActivityStatusEnum::ActivityStatusEnum oldActivityStatus, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>geographicShape</code> is updated.
      * <br>Description from the FOM: <i>Representation of 2D Force geographic shape.</i>
      * <br>Description of the data type from the FOM: <i>-NULL-</i>
      *
      * @param extendedAggregateEntity The object which is updated.
      * @param geographicShape The new value of the attribute in this update
      * @param validOldGeographicShape True if oldGeographicShape contains a valid value
      * @param oldGeographicShape The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void geographicShapeUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, GeographicShapeStruct geographicShape, bool validOldGeographicShape, GeographicShapeStruct oldGeographicShape, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>hierarchyPath</code> is updated.
      * <br>Description from the FOM: <i>Defines the hierarchy of an entity - used for Initialization process.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param extendedAggregateEntity The object which is updated.
      * @param hierarchyPath The new value of the attribute in this update
      * @param validOldHierarchyPath True if oldHierarchyPath contains a valid value
      * @param oldHierarchyPath The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void hierarchyPathUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, std::wstring hierarchyPath, bool validOldHierarchyPath, std::wstring oldHierarchyPath, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>entityVirtualityType</code> is updated.
      * <br>Description from the FOM: <i>Defines whether the entity is virtual or real.</i>
      * <br>Description of the data type from the FOM: <i>-NULL-</i>
      *
      * @param extendedAggregateEntity The object which is updated.
      * @param entityVirtualityType The new value of the attribute in this update
      * @param validOldEntityVirtualityType True if oldEntityVirtualityType contains a valid value
      * @param oldEntityVirtualityType The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void entityVirtualityTypeUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, EntityVirtualityTypeEnum::EntityVirtualityTypeEnum entityVirtualityType, bool validOldEntityVirtualityType, EntityVirtualityTypeEnum::EntityVirtualityTypeEnum oldEntityVirtualityType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>c2Position</code> is updated.
      * <br>Description from the FOM: <i>Defines the real C2 Position of the aggregate.</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @param extendedAggregateEntity The object which is updated.
      * @param c2Position The new value of the attribute in this update
      * @param validOldC2Position True if oldC2Position contains a valid value
      * @param oldC2Position The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void c2PositionUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, WorldLocationStruct c2Position, bool validOldC2Position, WorldLocationStruct oldC2Position, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>armyOrg</code> is updated.
      * <br>Description from the FOM: <i>Defines of which army organization the force belongs. Represented as an integer which its value is directing to an external enumeration.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^31, 2^31 - 1]</i>
      *
      * @param extendedAggregateEntity The object which is updated.
      * @param armyOrg The new value of the attribute in this update
      * @param validOldArmyOrg True if oldArmyOrg contains a valid value
      * @param oldArmyOrg The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void armyOrgUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, int armyOrg, bool validOldArmyOrg, int oldArmyOrg, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>fortifications</code> is updated.
      * <br>Description from the FOM: <i>Defines fortification type.</i>
      * <br>Description of the data type from the FOM: <i>-NULL-</i>
      *
      * @param extendedAggregateEntity The object which is updated.
      * @param fortifications The new value of the attribute in this update
      * @param validOldFortifications True if oldFortifications contains a valid value
      * @param oldFortifications The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void fortificationsUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, FortificationsEnum::FortificationsEnum fortifications, bool validOldFortifications, FortificationsEnum::FortificationsEnum oldFortifications, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>weaponType</code> is updated.
      * <br>Description from the FOM: <i>Defines main weapon type.</i>
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      *
      * @param extendedAggregateEntity The object which is updated.
      * @param weaponType The new value of the attribute in this update
      * @param validOldWeaponType True if oldWeaponType contains a valid value
      * @param oldWeaponType The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void weaponTypeUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, EntityTypeStruct weaponType, bool validOldWeaponType, EntityTypeStruct oldWeaponType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>operationalCompetence</code> is updated.
      * <br>Description from the FOM: <i>Defines the operational competence of the aggregate</i>
      * <br>Description of the data type from the FOM: <i>-NULL-</i>
      *
      * @param extendedAggregateEntity The object which is updated.
      * @param operationalCompetence The new value of the attribute in this update
      * @param validOldOperationalCompetence True if oldOperationalCompetence contains a valid value
      * @param oldOperationalCompetence The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void operationalCompetenceUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, OperationalCompetenceEnum::OperationalCompetenceEnum operationalCompetence, bool validOldOperationalCompetence, OperationalCompetenceEnum::OperationalCompetenceEnum oldOperationalCompetence, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>quantity</code> is updated.
      * <br>Description from the FOM: <i>Defines the quantity of physical entities that are belonged to the aggregate. (0-99999)</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^31, 2^31 - 1]</i>
      *
      * @param extendedAggregateEntity The object which is updated.
      * @param quantity The new value of the attribute in this update
      * @param validOldQuantity True if oldQuantity contains a valid value
      * @param oldQuantity The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void quantityUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, int quantity, bool validOldQuantity, int oldQuantity, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>unitOrganizationID</code> is updated.
      * <br>Description from the FOM: <i>Defines unique ID in the army organization book.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^31, 2^31 - 1]</i>
      *
      * @param extendedAggregateEntity The object which is updated.
      * @param unitOrganizationID The new value of the attribute in this update
      * @param validOldUnitOrganizationID True if oldUnitOrganizationID contains a valid value
      * @param oldUnitOrganizationID The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void unitOrganizationIDUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, int unitOrganizationID, bool validOldUnitOrganizationID, int oldUnitOrganizationID, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>operationalParent</code> is updated.
      * <br>Description from the FOM: <i>This field is holding a unique identifier for the aggregate's parent instance.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @param extendedAggregateEntity The object which is updated.
      * @param operationalParent The new value of the attribute in this update
      * @param validOldOperationalParent True if oldOperationalParent contains a valid value
      * @param oldOperationalParent The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void operationalParentUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, std::string operationalParent, bool validOldOperationalParent, std::string oldOperationalParent, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>c2OperationalParent</code> is updated.
      * <br>Description from the FOM: <i>This field is holding a unique identifier for the aggregate's c2 parent instance.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @param extendedAggregateEntity The object which is updated.
      * @param c2OperationalParent The new value of the attribute in this update
      * @param validOldC2OperationalParent True if oldC2OperationalParent contains a valid value
      * @param oldC2OperationalParent The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void c2OperationalParentUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, std::string c2OperationalParent, bool validOldC2OperationalParent, std::string oldC2OperationalParent, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>sector</code> is updated.
      * <br>Description from the FOM: <i>Defines fire sector of the aggregate. Mostly relevant for artillery simulators.</i>
      * <br>Description of the data type from the FOM: <i>-NULL-</i>
      *
      * @param extendedAggregateEntity The object which is updated.
      * @param sector The new value of the attribute in this update
      * @param validOldSector True if oldSector contains a valid value
      * @param oldSector The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void sectorUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, PositionStruct sector, bool validOldSector, PositionStruct oldSector, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>aggregateMarking</code> is updated.
      * <br>Description from the FOM: <i>A unique marking or combination of characters used to distinguish the aggregate from other aggregates.</i>
      * <br>Description of the data type from the FOM: <i>Unique marking associated with an aggregate.</i>
      *
      * @param extendedAggregateEntity The object which is updated.
      * @param aggregateMarking The new value of the attribute in this update
      * @param validOldAggregateMarking True if oldAggregateMarking contains a valid value
      * @param oldAggregateMarking The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void aggregateMarkingUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, AggregateMarkingStruct aggregateMarking, bool validOldAggregateMarking, AggregateMarkingStruct oldAggregateMarking, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>aggregateState</code> is updated.
      * <br>Description from the FOM: <i>An indicator of the extent of association of objects form an operating group.</i>
      * <br>Description of the data type from the FOM: <i>Aggregate state</i>
      *
      * @param extendedAggregateEntity The object which is updated.
      * @param aggregateState The new value of the attribute in this update
      * @param validOldAggregateState True if oldAggregateState contains a valid value
      * @param oldAggregateState The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void aggregateStateUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, AggregateStateEnum::AggregateStateEnum aggregateState, bool validOldAggregateState, AggregateStateEnum::AggregateStateEnum oldAggregateState, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>dimensions</code> is updated.
      * <br>Description from the FOM: <i>The size of the area covered by the units in the aggregate.</i>
      * <br>Description of the data type from the FOM: <i>Bounding box in X,Y,Z axis.</i>
      *
      * @param extendedAggregateEntity The object which is updated.
      * @param dimensions The new value of the attribute in this update
      * @param validOldDimensions True if oldDimensions contains a valid value
      * @param oldDimensions The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void dimensionsUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, DimensionStruct dimensions, bool validOldDimensions, DimensionStruct oldDimensions, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>entityIdentifiers</code> is updated.
      * <br>Description from the FOM: <i>The identification of entities that are contained within the aggregate.</i>
      * <br>Description of the data type from the FOM: <i>Set of ID's of registered object instances.</i>
      *
      * @param extendedAggregateEntity The object which is updated.
      * @param entityIdentifiers The new value of the attribute in this update
      * @param validOldEntityIdentifiers True if oldEntityIdentifiers contains a valid value
      * @param oldEntityIdentifiers The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void entityIdentifiersUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, std::vector<std::string > entityIdentifiers, bool validOldEntityIdentifiers, std::vector<std::string > oldEntityIdentifiers, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>forceIdentifier</code> is updated.
      * <br>Description from the FOM: <i>The identification of the force that the aggregate belongs to.</i>
      * <br>Description of the data type from the FOM: <i>Force ID</i>
      *
      * @param extendedAggregateEntity The object which is updated.
      * @param forceIdentifier The new value of the attribute in this update
      * @param validOldForceIdentifier True if oldForceIdentifier contains a valid value
      * @param oldForceIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void forceIdentifierUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, ForceIdentifierEnum::ForceIdentifierEnum forceIdentifier, bool validOldForceIdentifier, ForceIdentifierEnum::ForceIdentifierEnum oldForceIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>formation</code> is updated.
      * <br>Description from the FOM: <i>The category of positional arrangement of the entities within the aggregate.</i>
      * <br>Description of the data type from the FOM: <i>Formation</i>
      *
      * @param extendedAggregateEntity The object which is updated.
      * @param formation The new value of the attribute in this update
      * @param validOldFormation True if oldFormation contains a valid value
      * @param oldFormation The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void formationUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, FormationEnum::FormationEnum formation, bool validOldFormation, FormationEnum::FormationEnum oldFormation, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>numberOfSilentEntities</code> is updated.
      * <br>Description from the FOM: <i>The number of elements in the SilentEntities list.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^15, 2^15-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param extendedAggregateEntity The object which is updated.
      * @param numberOfSilentEntities The new value of the attribute in this update
      * @param validOldNumberOfSilentEntities True if oldNumberOfSilentEntities contains a valid value
      * @param oldNumberOfSilentEntities The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void numberOfSilentEntitiesUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, short numberOfSilentEntities, bool validOldNumberOfSilentEntities, short oldNumberOfSilentEntities, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>numberOfVariableDatums</code> is updated.
      * <br>Description from the FOM: <i>The number of records in the VariableDatums structure.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param extendedAggregateEntity The object which is updated.
      * @param numberOfVariableDatums The new value of the attribute in this update
      * @param validOldNumberOfVariableDatums True if oldNumberOfVariableDatums contains a valid value
      * @param oldNumberOfVariableDatums The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void numberOfVariableDatumsUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, unsigned int numberOfVariableDatums, bool validOldNumberOfVariableDatums, unsigned int oldNumberOfVariableDatums, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>silentAggregates</code> is updated.
      * <br>Description from the FOM: <i>The numbers and types, of silent aggregates contained in the aggregate. Silent aggregates are sub-aggregates that are in the aggregate, but that are not separately represented in the virtual world.</i>
      * <br>Description of the data type from the FOM: <i>Set of silent aggregates (aggregates not registered in the federation).</i>
      *
      * @param extendedAggregateEntity The object which is updated.
      * @param silentAggregates The new value of the attribute in this update
      * @param validOldSilentAggregates True if oldSilentAggregates contains a valid value
      * @param oldSilentAggregates The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void silentAggregatesUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, std::vector<DevStudio::SilentAggregateStruct > silentAggregates, bool validOldSilentAggregates, std::vector<DevStudio::SilentAggregateStruct > oldSilentAggregates, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>silentEntities</code> is updated.
      * <br>Description from the FOM: <i>The numbers and types, of silent entities in the aggregate. Silent entities are entities that are in the aggregate, but that are not separately represented in the virtual world.</i>
      * <br>Description of the data type from the FOM: <i>A set of silent entities (entities not registered in the federation).</i>
      *
      * @param extendedAggregateEntity The object which is updated.
      * @param silentEntities The new value of the attribute in this update
      * @param validOldSilentEntities True if oldSilentEntities contains a valid value
      * @param oldSilentEntities The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void silentEntitiesUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, std::vector<DevStudio::SilentEntityStruct > silentEntities, bool validOldSilentEntities, std::vector<DevStudio::SilentEntityStruct > oldSilentEntities, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>subAggregateIdentifiers</code> is updated.
      * <br>Description from the FOM: <i>The identifications of aggregates represented in the virtual world that are contained in the aggregate.</i>
      * <br>Description of the data type from the FOM: <i>Set of ID's of registered object instances.</i>
      *
      * @param extendedAggregateEntity The object which is updated.
      * @param subAggregateIdentifiers The new value of the attribute in this update
      * @param validOldSubAggregateIdentifiers True if oldSubAggregateIdentifiers contains a valid value
      * @param oldSubAggregateIdentifiers The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void subAggregateIdentifiersUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, std::vector<std::string > subAggregateIdentifiers, bool validOldSubAggregateIdentifiers, std::vector<std::string > oldSubAggregateIdentifiers, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>variableDatums</code> is updated.
      * <br>Description from the FOM: <i>Extra data that describes the aggregate.</i>
      * <br>Description of the data type from the FOM: <i>Set of additional data associated with an aggregate.</i>
      *
      * @param extendedAggregateEntity The object which is updated.
      * @param variableDatums The new value of the attribute in this update
      * @param validOldVariableDatums True if oldVariableDatums contains a valid value
      * @param oldVariableDatums The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void variableDatumsUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, std::vector<DevStudio::VariableDatumStruct > variableDatums, bool validOldVariableDatums, std::vector<DevStudio::VariableDatumStruct > oldVariableDatums, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>entityType</code> is updated.
      * <br>Description from the FOM: <i>The category of the entity.</i>
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      *
      * @param extendedAggregateEntity The object which is updated.
      * @param entityType The new value of the attribute in this update
      * @param validOldEntityType True if oldEntityType contains a valid value
      * @param oldEntityType The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void entityTypeUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, EntityTypeStruct entityType, bool validOldEntityType, EntityTypeStruct oldEntityType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>entityIdentifier</code> is updated.
      * <br>Description from the FOM: <i>The unique identifier for the entity instance.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @param extendedAggregateEntity The object which is updated.
      * @param entityIdentifier The new value of the attribute in this update
      * @param validOldEntityIdentifier True if oldEntityIdentifier contains a valid value
      * @param oldEntityIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void entityIdentifierUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, EntityIdentifierStruct entityIdentifier, bool validOldEntityIdentifier, EntityIdentifierStruct oldEntityIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>isPartOf</code> is updated.
      * <br>Description from the FOM: <i>Defines if the entity if a constituent part of another entity (denoted the host entity). If the entity is a constituent part of another entity then the HostEntityIdentifier shall be set to the EntityIdentifier of the host entity and the HostRTIObjectIdentifier shall be set to the RTI object instance ID of the host entity. If the entity is not a constituent part of another entity then the HostEntityIdentifier shall be set to 0.0.0 and the HostRTIObjectIdentifier shall be set to the empty string.</i>
      * <br>Description of the data type from the FOM: <i>Defines the spatial relationship between two objects.</i>
      *
      * @param extendedAggregateEntity The object which is updated.
      * @param isPartOf The new value of the attribute in this update
      * @param validOldIsPartOf True if oldIsPartOf contains a valid value
      * @param oldIsPartOf The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void isPartOfUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, IsPartOfStruct isPartOf, bool validOldIsPartOf, IsPartOfStruct oldIsPartOf, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>spatial</code> is updated.
      * <br>Description from the FOM: <i>Spatial state stored in one variant record attribute.</i>
      * <br>Description of the data type from the FOM: <i>Variant Record for a single spatial attribute.</i>
      *
      * @param extendedAggregateEntity The object which is updated.
      * @param spatial The new value of the attribute in this update
      * @param validOldSpatial True if oldSpatial contains a valid value
      * @param oldSpatial The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void spatialUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, SpatialVariantStruct spatial, bool validOldSpatial, SpatialVariantStruct oldSpatial, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>relativeSpatial</code> is updated.
      * <br>Description from the FOM: <i>Relative spatial state stored in one variant record attribute.</i>
      * <br>Description of the data type from the FOM: <i>Variant Record for a single spatial attribute.</i>
      *
      * @param extendedAggregateEntity The object which is updated.
      * @param relativeSpatial The new value of the attribute in this update
      * @param validOldRelativeSpatial True if oldRelativeSpatial contains a valid value
      * @param oldRelativeSpatial The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void relativeSpatialUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, SpatialVariantStruct relativeSpatial, bool validOldRelativeSpatial, SpatialVariantStruct oldRelativeSpatial, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
      /**
      * This method is called when the producing federate of an attribute is changed.
      * Only available when using HLA Evolved.
      *
      * @param extendedAggregateEntity The object which is updated.
      * @param attribute The attribute that now has a new producing federate
      * @param oldProducingFederate The federate handle of the old producing federate
      * @param newProducingFederate The federate handle of the new producing federate
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void producingFederateUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, HlaExtendedAggregateEntityAttributes::Attribute attribute, HlaFederateIdPtr oldProducingFederate, HlaFederateIdPtr newProducingFederate, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;

      class Adapter;
   };

   /**
   * An adapter class that implements the HlaExtendedAggregateEntityValueListener interface with empty methods.
   * It might be used as a base class for a listener.
   */
   class HlaExtendedAggregateEntityValueListener::Adapter : public HlaExtendedAggregateEntityValueListener {

   public:

      LIBAPI virtual void c2IdentifierUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, std::string c2Identifier, bool validOldC2Identifier, std::string oldC2Identifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void playerIDUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, std::wstring playerID, bool validOldPlayerID, std::wstring oldPlayerID, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void aliasUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, std::wstring alias, bool validOldAlias, std::wstring oldAlias, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void aggregateSizeUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, AggregateSizeStruct aggregateSize, bool validOldAggregateSize, AggregateSizeStruct oldAggregateSize, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void lastUpdateTimeUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, short lastUpdateTime, bool validOldLastUpdateTime, short oldLastUpdateTime, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void activityStatusUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, ActivityStatusEnum::ActivityStatusEnum activityStatus, bool validOldActivityStatus, ActivityStatusEnum::ActivityStatusEnum oldActivityStatus, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void geographicShapeUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, GeographicShapeStruct geographicShape, bool validOldGeographicShape, GeographicShapeStruct oldGeographicShape, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void hierarchyPathUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, std::wstring hierarchyPath, bool validOldHierarchyPath, std::wstring oldHierarchyPath, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void entityVirtualityTypeUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, EntityVirtualityTypeEnum::EntityVirtualityTypeEnum entityVirtualityType, bool validOldEntityVirtualityType, EntityVirtualityTypeEnum::EntityVirtualityTypeEnum oldEntityVirtualityType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void c2PositionUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, WorldLocationStruct c2Position, bool validOldC2Position, WorldLocationStruct oldC2Position, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void armyOrgUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, int armyOrg, bool validOldArmyOrg, int oldArmyOrg, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void fortificationsUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, FortificationsEnum::FortificationsEnum fortifications, bool validOldFortifications, FortificationsEnum::FortificationsEnum oldFortifications, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void weaponTypeUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, EntityTypeStruct weaponType, bool validOldWeaponType, EntityTypeStruct oldWeaponType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void operationalCompetenceUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, OperationalCompetenceEnum::OperationalCompetenceEnum operationalCompetence, bool validOldOperationalCompetence, OperationalCompetenceEnum::OperationalCompetenceEnum oldOperationalCompetence, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void quantityUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, int quantity, bool validOldQuantity, int oldQuantity, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void unitOrganizationIDUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, int unitOrganizationID, bool validOldUnitOrganizationID, int oldUnitOrganizationID, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void operationalParentUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, std::string operationalParent, bool validOldOperationalParent, std::string oldOperationalParent, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void c2OperationalParentUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, std::string c2OperationalParent, bool validOldC2OperationalParent, std::string oldC2OperationalParent, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void sectorUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, PositionStruct sector, bool validOldSector, PositionStruct oldSector, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void aggregateMarkingUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, AggregateMarkingStruct aggregateMarking, bool validOldAggregateMarking, AggregateMarkingStruct oldAggregateMarking, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void aggregateStateUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, AggregateStateEnum::AggregateStateEnum aggregateState, bool validOldAggregateState, AggregateStateEnum::AggregateStateEnum oldAggregateState, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void dimensionsUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, DimensionStruct dimensions, bool validOldDimensions, DimensionStruct oldDimensions, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void entityIdentifiersUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, std::vector<std::string > entityIdentifiers, bool validOldEntityIdentifiers, std::vector<std::string > oldEntityIdentifiers, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void forceIdentifierUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, ForceIdentifierEnum::ForceIdentifierEnum forceIdentifier, bool validOldForceIdentifier, ForceIdentifierEnum::ForceIdentifierEnum oldForceIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void formationUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, FormationEnum::FormationEnum formation, bool validOldFormation, FormationEnum::FormationEnum oldFormation, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void numberOfSilentEntitiesUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, short numberOfSilentEntities, bool validOldNumberOfSilentEntities, short oldNumberOfSilentEntities, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void numberOfVariableDatumsUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, unsigned int numberOfVariableDatums, bool validOldNumberOfVariableDatums, unsigned int oldNumberOfVariableDatums, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void silentAggregatesUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, std::vector<DevStudio::SilentAggregateStruct > silentAggregates, bool validOldSilentAggregates, std::vector<DevStudio::SilentAggregateStruct > oldSilentAggregates, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void silentEntitiesUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, std::vector<DevStudio::SilentEntityStruct > silentEntities, bool validOldSilentEntities, std::vector<DevStudio::SilentEntityStruct > oldSilentEntities, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void subAggregateIdentifiersUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, std::vector<std::string > subAggregateIdentifiers, bool validOldSubAggregateIdentifiers, std::vector<std::string > oldSubAggregateIdentifiers, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void variableDatumsUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, std::vector<DevStudio::VariableDatumStruct > variableDatums, bool validOldVariableDatums, std::vector<DevStudio::VariableDatumStruct > oldVariableDatums, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void entityTypeUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, EntityTypeStruct entityType, bool validOldEntityType, EntityTypeStruct oldEntityType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void entityIdentifierUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, EntityIdentifierStruct entityIdentifier, bool validOldEntityIdentifier, EntityIdentifierStruct oldEntityIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void isPartOfUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, IsPartOfStruct isPartOf, bool validOldIsPartOf, IsPartOfStruct oldIsPartOf, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void spatialUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, SpatialVariantStruct spatial, bool validOldSpatial, SpatialVariantStruct oldSpatial, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void relativeSpatialUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, SpatialVariantStruct relativeSpatial, bool validOldRelativeSpatial, SpatialVariantStruct oldRelativeSpatial, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
      LIBAPI virtual void producingFederateUpdated(HlaExtendedAggregateEntityPtr extendedAggregateEntity, HlaExtendedAggregateEntityAttributes::Attribute attribute, HlaFederateIdPtr oldProducingFederate, HlaFederateIdPtr newProducingFederate, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
   };

}
#endif
