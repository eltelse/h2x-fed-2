/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAEXPENDABLESMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLAEXPENDABLESMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaExpendables.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaExpendables instances.
    */
    class HlaExpendablesManagerListener {

    public:

        LIBAPI virtual ~HlaExpendablesManagerListener() {}

        /**
        * This method is called when a new HlaExpendables instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param expendables the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaExpendablesDiscovered(HlaExpendablesPtr expendables, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaExpendables instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param expendables the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaExpendablesInitialized(HlaExpendablesPtr expendables, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaExpendablesManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param expendables the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaExpendablesInInterest(HlaExpendablesPtr expendables, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaExpendablesManagerListener instance goes out of interest.
        *
        * @param expendables the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaExpendablesOutOfInterest(HlaExpendablesPtr expendables, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaExpendables instance is deleted.
        *
        * @param expendables the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaExpendablesDeleted(HlaExpendablesPtr expendables, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaExpendablesManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaExpendablesManagerListener::Adapter : public HlaExpendablesManagerListener {

    public:
        LIBAPI virtual void hlaExpendablesDiscovered(HlaExpendablesPtr expendables, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaExpendablesInitialized(HlaExpendablesPtr expendables, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaExpendablesInInterest(HlaExpendablesPtr expendables, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaExpendablesOutOfInterest(HlaExpendablesPtr expendables, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaExpendablesDeleted(HlaExpendablesPtr expendables, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
