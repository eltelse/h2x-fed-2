/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAADDITIONALPASSIVEACTIVITIESVALUELISTENER_H
#define DEVELOPER_STUDIO_HLAADDITIONALPASSIVEACTIVITIESVALUELISTENER_H

#include <memory>

#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EventIdentifierStruct.h>
#include <DevStudio/datatypes/RelativePositionStruct.h>
#include <string>

#include <DevStudio/HlaLogicalTime.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaAdditionalPassiveActivitiesAttributes.h>    

namespace DevStudio {

   /**
   * Listener for updates of attributes, with the new updated values.  
   */
   class HlaAdditionalPassiveActivitiesValueListener {

   public:

      LIBAPI virtual ~HlaAdditionalPassiveActivitiesValueListener() {}
    
      /**
      * This method is called when the attribute <code>activityCode</code> is updated.
      * <br>Description from the FOM: <i>Database index used to indicate the passive signature of the entity</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param additionalPassiveActivities The object which is updated.
      * @param activityCode The new value of the attribute in this update
      * @param validOldActivityCode True if oldActivityCode contains a valid value
      * @param oldActivityCode The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void activityCodeUpdated(HlaAdditionalPassiveActivitiesPtr additionalPassiveActivities, unsigned short activityCode, bool validOldActivityCode, unsigned short oldActivityCode, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>activityParameter</code> is updated.
      * <br>Description from the FOM: <i>The current state of this activity.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^15, 2^15-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param additionalPassiveActivities The object which is updated.
      * @param activityParameter The new value of the attribute in this update
      * @param validOldActivityParameter True if oldActivityParameter contains a valid value
      * @param oldActivityParameter The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void activityParameterUpdated(HlaAdditionalPassiveActivitiesPtr additionalPassiveActivities, short activityParameter, bool validOldActivityParameter, short oldActivityParameter, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>isSilent</code> is updated.
      * <br>Description from the FOM: <i>Used to silence an additional passive activity without destroying this object.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param additionalPassiveActivities The object which is updated.
      * @param isSilent The new value of the attribute in this update
      * @param validOldIsSilent True if oldIsSilent contains a valid value
      * @param oldIsSilent The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void isSilentUpdated(HlaAdditionalPassiveActivitiesPtr additionalPassiveActivities, bool isSilent, bool validOldIsSilent, bool oldIsSilent, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>eventIdentifier</code> is updated.
      * <br>Description from the FOM: <i>The generating federate uses the Event Identifier to associate related events. The event number begins at one at the beginning of the exercise and is incremented by one for each event.</i>
      * <br>Description of the data type from the FOM: <i>Identification of an event. Based on the Event Identifier record as specified in IEEE 1278.1-1995 section 5.2.18.</i>
      *
      * @param additionalPassiveActivities The object which is updated.
      * @param eventIdentifier The new value of the attribute in this update
      * @param validOldEventIdentifier True if oldEventIdentifier contains a valid value
      * @param oldEventIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void eventIdentifierUpdated(HlaAdditionalPassiveActivitiesPtr additionalPassiveActivities, EventIdentifierStruct eventIdentifier, bool validOldEventIdentifier, EventIdentifierStruct oldEventIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>entityIdentifier</code> is updated.
      * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @param additionalPassiveActivities The object which is updated.
      * @param entityIdentifier The new value of the attribute in this update
      * @param validOldEntityIdentifier True if oldEntityIdentifier contains a valid value
      * @param oldEntityIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void entityIdentifierUpdated(HlaAdditionalPassiveActivitiesPtr additionalPassiveActivities, EntityIdentifierStruct entityIdentifier, bool validOldEntityIdentifier, EntityIdentifierStruct oldEntityIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>hostObjectIdentifier</code> is updated.
      * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @param additionalPassiveActivities The object which is updated.
      * @param hostObjectIdentifier The new value of the attribute in this update
      * @param validOldHostObjectIdentifier True if oldHostObjectIdentifier contains a valid value
      * @param oldHostObjectIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void hostObjectIdentifierUpdated(HlaAdditionalPassiveActivitiesPtr additionalPassiveActivities, std::string hostObjectIdentifier, bool validOldHostObjectIdentifier, std::string oldHostObjectIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>relativePosition</code> is updated.
      * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
      * <br>Description of the data type from the FOM: <i>Relative position in right-handed Cartesian coordinates.</i>
      *
      * @param additionalPassiveActivities The object which is updated.
      * @param relativePosition The new value of the attribute in this update
      * @param validOldRelativePosition True if oldRelativePosition contains a valid value
      * @param oldRelativePosition The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void relativePositionUpdated(HlaAdditionalPassiveActivitiesPtr additionalPassiveActivities, RelativePositionStruct relativePosition, bool validOldRelativePosition, RelativePositionStruct oldRelativePosition, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
      /**
      * This method is called when the producing federate of an attribute is changed.
      * Only available when using HLA Evolved.
      *
      * @param additionalPassiveActivities The object which is updated.
      * @param attribute The attribute that now has a new producing federate
      * @param oldProducingFederate The federate handle of the old producing federate
      * @param newProducingFederate The federate handle of the new producing federate
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void producingFederateUpdated(HlaAdditionalPassiveActivitiesPtr additionalPassiveActivities, HlaAdditionalPassiveActivitiesAttributes::Attribute attribute, HlaFederateIdPtr oldProducingFederate, HlaFederateIdPtr newProducingFederate, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;

      class Adapter;
   };

   /**
   * An adapter class that implements the HlaAdditionalPassiveActivitiesValueListener interface with empty methods.
   * It might be used as a base class for a listener.
   */
   class HlaAdditionalPassiveActivitiesValueListener::Adapter : public HlaAdditionalPassiveActivitiesValueListener {

   public:

      LIBAPI virtual void activityCodeUpdated(HlaAdditionalPassiveActivitiesPtr additionalPassiveActivities, unsigned short activityCode, bool validOldActivityCode, unsigned short oldActivityCode, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void activityParameterUpdated(HlaAdditionalPassiveActivitiesPtr additionalPassiveActivities, short activityParameter, bool validOldActivityParameter, short oldActivityParameter, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void isSilentUpdated(HlaAdditionalPassiveActivitiesPtr additionalPassiveActivities, bool isSilent, bool validOldIsSilent, bool oldIsSilent, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void eventIdentifierUpdated(HlaAdditionalPassiveActivitiesPtr additionalPassiveActivities, EventIdentifierStruct eventIdentifier, bool validOldEventIdentifier, EventIdentifierStruct oldEventIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void entityIdentifierUpdated(HlaAdditionalPassiveActivitiesPtr additionalPassiveActivities, EntityIdentifierStruct entityIdentifier, bool validOldEntityIdentifier, EntityIdentifierStruct oldEntityIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void hostObjectIdentifierUpdated(HlaAdditionalPassiveActivitiesPtr additionalPassiveActivities, std::string hostObjectIdentifier, bool validOldHostObjectIdentifier, std::string oldHostObjectIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void relativePositionUpdated(HlaAdditionalPassiveActivitiesPtr additionalPassiveActivities, RelativePositionStruct relativePosition, bool validOldRelativePosition, RelativePositionStruct oldRelativePosition, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
      LIBAPI virtual void producingFederateUpdated(HlaAdditionalPassiveActivitiesPtr additionalPassiveActivities, HlaAdditionalPassiveActivitiesAttributes::Attribute attribute, HlaFederateIdPtr oldProducingFederate, HlaFederateIdPtr newProducingFederate, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
   };

}
#endif
