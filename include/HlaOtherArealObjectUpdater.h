/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAOTHERAREALOBJECTUPDATER_H
#define DEVELOPER_STUDIO_HLAOTHERAREALOBJECTUPDATER_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <boost/noncopyable.hpp>

#include <DevStudio/datatypes/DamageStatusEnum.h>
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EnvironmentObjectTypeStruct.h>
#include <DevStudio/datatypes/ForceIdentifierEnum.h>
#include <DevStudio/datatypes/WorldLocationStruct.h>
#include <DevStudio/datatypes/WorldLocationStructLengthlessArray.h>
#include <string>
#include <vector>

#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaException.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaArealObjectUpdater.h>

namespace DevStudio {

    /**
    * Updater used to update attribute values.
    */
    class HlaOtherArealObjectUpdater : public HlaArealObjectUpdater {

    public:

    LIBAPI virtual ~HlaOtherArealObjectUpdater() {}

    /**
    * Set the pointsData for this update.
    * <br>Description from the FOM: <i>Specifies the physical location (a collection of points) of the object</i>
    * <br>Description of the data type from the FOM: <i>Dynamic array of WorldLocationStruct elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
    *
    * @param pointsData the new pointsData
    */
    LIBAPI virtual void setPointsData(const std::vector<DevStudio::WorldLocationStruct >& pointsData) = 0;

    /**
    * Set the percentComplete for this update.
    * <br>Description from the FOM: <i>Specifies the percent completion of the object</i>
    * <br>Description of the data type from the FOM: <i>Percentage [unit: percent (%), resolution: 1, accuracy: perfect]</i>
    *
    * @param percentComplete the new percentComplete
    */
    LIBAPI virtual void setPercentComplete(const unsigned int& percentComplete) = 0;

    /**
    * Set the damagedAppearance for this update.
    * <br>Description from the FOM: <i>Specifies the damaged appearance of the object</i>
    * <br>Description of the data type from the FOM: <i>Damaged appearance</i>
    *
    * @param damagedAppearance the new damagedAppearance
    */
    LIBAPI virtual void setDamagedAppearance(const DevStudio::DamageStatusEnum::DamageStatusEnum& damagedAppearance) = 0;

    /**
    * Set the objectPreDistributed for this update.
    * <br>Description from the FOM: <i>Specifies whether or not the object was created before the start of the exercise</i>
    * <br>Description of the data type from the FOM: <i></i>
    *
    * @param objectPreDistributed the new objectPreDistributed
    */
    LIBAPI virtual void setObjectPreDistributed(const bool& objectPreDistributed) = 0;

    /**
    * Set the deactivated for this update.
    * <br>Description from the FOM: <i>Specifies whether or not the object has been deactivated (it has ceased to exist in the synthetic environment)</i>
    * <br>Description of the data type from the FOM: <i></i>
    *
    * @param deactivated the new deactivated
    */
    LIBAPI virtual void setDeactivated(const bool& deactivated) = 0;

    /**
    * Set the smoking for this update.
    * <br>Description from the FOM: <i>Specifies whether or not the object is smoking (creating a smoke plume)</i>
    * <br>Description of the data type from the FOM: <i></i>
    *
    * @param smoking the new smoking
    */
    LIBAPI virtual void setSmoking(const bool& smoking) = 0;

    /**
    * Set the flaming for this update.
    * <br>Description from the FOM: <i>Specifies whether or not the object is aflame</i>
    * <br>Description of the data type from the FOM: <i></i>
    *
    * @param flaming the new flaming
    */
    LIBAPI virtual void setFlaming(const bool& flaming) = 0;

    /**
    * Set the objectIdentifier for this update.
    * <br>Description from the FOM: <i>Identifies this EnvironmentObject instance (point, linear or areal)</i>
    * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
    *
    * @param objectIdentifier the new objectIdentifier
    */
    LIBAPI virtual void setObjectIdentifier(const DevStudio::EntityIdentifierStruct& objectIdentifier) = 0;

    /**
    * Set the referencedObjectIdentifier for this update.
    * <br>Description from the FOM: <i>Identifies the Synthetic Environment object instance to which this EnvironmentObject instance is associated</i>
    * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
    *
    * @param referencedObjectIdentifier the new referencedObjectIdentifier
    */
    LIBAPI virtual void setReferencedObjectIdentifier(const std::string& referencedObjectIdentifier) = 0;

    /**
    * Set the forceIdentifier for this update.
    * <br>Description from the FOM: <i>Identifies the force that created or modified this EnvironmentObject instance</i>
    * <br>Description of the data type from the FOM: <i>Force ID</i>
    *
    * @param forceIdentifier the new forceIdentifier
    */
    LIBAPI virtual void setForceIdentifier(const DevStudio::ForceIdentifierEnum::ForceIdentifierEnum& forceIdentifier) = 0;

    /**
    * Set the objectType for this update.
    * <br>Description from the FOM: <i>Identifies the type of this EnvironmentObject instance</i>
    * <br>Description of the data type from the FOM: <i>Record specifying the domain, the kind and the specific identification of the environment object</i>
    *
    * @param objectType the new objectType
    */
    LIBAPI virtual void setObjectType(const DevStudio::EnvironmentObjectTypeStruct& objectType) = 0;

    /**
    * Send all the attributes.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate()
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

    /**
    * Send all the attributes with a specified timestamp.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @param timestamp timestamp to send for this update
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate(HlaTimeStampPtr timestamp)
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

    /**
    * Send all the attributes with a specified timestamp.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @param logicalTime logical time to send for this update
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaInvalidLogicalTimeException if the logical time is not valid.
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate(HlaLogicalTimePtr logicalTime)
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaInvalidLogicalTimeException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

    /**
    * Send all the attributes with a specified timestamp.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @param timestamp timestamp to send for this update
    * @param logicalTime logical time to send for this update
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaInvalidLogicalTimeException if the logical time is not valid.
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate(HlaTimeStampPtr timestamp, HlaLogicalTimePtr logicalTime)
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaInvalidLogicalTimeException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;
    };
}
#endif
