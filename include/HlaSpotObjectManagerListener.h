/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLASPOTOBJECTMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLASPOTOBJECTMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaSpotObject.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaSpotObject instances.
    */
    class HlaSpotObjectManagerListener {

    public:

        LIBAPI virtual ~HlaSpotObjectManagerListener() {}

        /**
        * This method is called when a new HlaSpotObject instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param spotObject the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaSpotObjectDiscovered(HlaSpotObjectPtr spotObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaSpotObject instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param spotObject the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaSpotObjectInitialized(HlaSpotObjectPtr spotObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaSpotObjectManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param spotObject the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaSpotObjectInInterest(HlaSpotObjectPtr spotObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaSpotObjectManagerListener instance goes out of interest.
        *
        * @param spotObject the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaSpotObjectOutOfInterest(HlaSpotObjectPtr spotObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaSpotObject instance is deleted.
        *
        * @param spotObject the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaSpotObjectDeleted(HlaSpotObjectPtr spotObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaSpotObjectManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaSpotObjectManagerListener::Adapter : public HlaSpotObjectManagerListener {

    public:
        LIBAPI virtual void hlaSpotObjectDiscovered(HlaSpotObjectPtr spotObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaSpotObjectInitialized(HlaSpotObjectPtr spotObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaSpotObjectInInterest(HlaSpotObjectPtr spotObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaSpotObjectOutOfInterest(HlaSpotObjectPtr spotObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaSpotObjectDeleted(HlaSpotObjectPtr spotObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
