/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLARADIORECEIVERATTRIBUTES_H
#define DEVELOPER_STUDIO_HLARADIORECEIVERATTRIBUTES_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <string>
#include <vector>
#include <utility>
    
#include <boost/noncopyable.hpp>
    
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/ReceiverOperationalStatusEnum.h>
#include <DevStudio/datatypes/RelativePositionStruct.h>
#include <string>

#include <DevStudio/HlaException.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaEmbeddedSystemAttributes.h>
#include <DevStudio/HlaTimeStamped.h>

namespace DevStudio {

   /**
   * Interface used to represent all attributes for an object instance.
   */
   class HlaRadioReceiverAttributes : public HlaEmbeddedSystemAttributes {
   public:


     /**
      * An enumeration of the attributes of an HlaRadioReceiver
      *
      *<table summary="All attributes">
      * <tr><td><b>Enum constant</b></td><td><b>C++ name</b></td><td><b>FOM name</b></td></tr>
      * <tr><td>RADIO_INDEX</td><td>radioIndex</td><td><code>RadioIndex</code></td></tr>
      * <tr><td>RECEIVED_POWER</td><td>receivedPower</td><td><code>ReceivedPower</code></td></tr>
      * <tr><td>RECEIVED_TRANSMITTER_IDENTIFIER</td><td>receivedTransmitterIdentifier</td><td><code>ReceivedTransmitterIdentifier</code></td></tr>
      * <tr><td>RECEIVER_OPERATIONAL_STATUS</td><td>receiverOperationalStatus</td><td><code>ReceiverOperationalStatus</code></td></tr>
      * <tr><td>ENTITY_IDENTIFIER</td><td>entityIdentifier</td><td><code>EntityIdentifier</code></td></tr>
      * <tr><td>HOST_OBJECT_IDENTIFIER</td><td>hostObjectIdentifier</td><td><code>HostObjectIdentifier</code></td></tr>
      * <tr><td>RELATIVE_POSITION</td><td>relativePosition</td><td><code>RelativePosition</code></td></tr>
      *</table>
      */
      enum Attribute {
        /**
        * radioIndex (FOM name: <code>RadioIndex</code>).
        * <br>Description from the FOM: <i>A number that uniquely identifies this radio receiver from other receivers on the host object.</i>
        */
         RADIO_INDEX,

        /**
        * receivedPower (FOM name: <code>ReceivedPower</code>).
        * <br>Description from the FOM: <i>The radio frequency power received, after applying any propagation loss and antenna gain.</i>
        */
         RECEIVED_POWER,

        /**
        * receivedTransmitterIdentifier (FOM name: <code>ReceivedTransmitterIdentifier</code>).
        * <br>Description from the FOM: <i>The object instance ID of the transmitter that generated the received radio signal.</i>
        */
         RECEIVED_TRANSMITTER_IDENTIFIER,

        /**
        * receiverOperationalStatus (FOM name: <code>ReceiverOperationalStatus</code>).
        * <br>Description from the FOM: <i>The operational state of the radio receiver.</i>
        */
         RECEIVER_OPERATIONAL_STATUS,

        /**
        * entityIdentifier (FOM name: <code>EntityIdentifier</code>).
        * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
        */
         ENTITY_IDENTIFIER,

        /**
        * hostObjectIdentifier (FOM name: <code>HostObjectIdentifier</code>).
        * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
        */
         HOST_OBJECT_IDENTIFIER,

        /**
        * relativePosition (FOM name: <code>RelativePosition</code>).
        * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
        */
         RELATIVE_POSITION
      };

     /**
      * Gets the name of the attribute.
      *
      * @return The name of the attribute. An empty string will be returned if the attribute does not exist.
      */
      LIBAPI virtual std::wstring getName(Attribute attribute);

     /**
      * Finds the attribute specified in the parameter attributeName.
      * The found enumeration will be stored in the parameter attribute.
      *
      * @return true if the attribute was found, false otherwise.
      */
      LIBAPI virtual bool find(Attribute& attribute, std::wstring attributeName);

      LIBAPI virtual ~HlaRadioReceiverAttributes() {}
    
      /**
      * Returns true if the <code>radioIndex</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>A number that uniquely identifies this radio receiver from other receivers on the host object.</i>
      *
      * @return true if <code>radioIndex</code> is available.
      */
      LIBAPI virtual bool hasRadioIndex() = 0;

      /**
      * Gets the value of the <code>radioIndex</code> attribute.
      *
      * <br>Description from the FOM: <i>A number that uniquely identifies this radio receiver from other receivers on the host object.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the <code>radioIndex</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual unsigned short getRadioIndex()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>radioIndex</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>A number that uniquely identifies this radio receiver from other receivers on the host object.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>radioIndex</code> attribute.
      */
      LIBAPI virtual unsigned short getRadioIndex(unsigned short defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>radioIndex</code> attribute.
      * <br>Description from the FOM: <i>A number that uniquely identifies this radio receiver from other receivers on the host object.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the time stamped <code>radioIndex</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< unsigned short > getRadioIndexTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>receivedPower</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The radio frequency power received, after applying any propagation loss and antenna gain.</i>
      *
      * @return true if <code>receivedPower</code> is available.
      */
      LIBAPI virtual bool hasReceivedPower() = 0;

      /**
      * Gets the value of the <code>receivedPower</code> attribute.
      *
      * <br>Description from the FOM: <i>The radio frequency power received, after applying any propagation loss and antenna gain.</i>
      * <br>Description of the data type from the FOM: <i>Power ratio in decibels (dB) of a measured power referenced to 1 milliwatt (mW). [unit: decibel milliwatt (dBm), resolution: NA, accuracy: perfect]</i>
      *
      * @return the <code>receivedPower</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual float getReceivedPower()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>receivedPower</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The radio frequency power received, after applying any propagation loss and antenna gain.</i>
      * <br>Description of the data type from the FOM: <i>Power ratio in decibels (dB) of a measured power referenced to 1 milliwatt (mW). [unit: decibel milliwatt (dBm), resolution: NA, accuracy: perfect]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>receivedPower</code> attribute.
      */
      LIBAPI virtual float getReceivedPower(float defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>receivedPower</code> attribute.
      * <br>Description from the FOM: <i>The radio frequency power received, after applying any propagation loss and antenna gain.</i>
      * <br>Description of the data type from the FOM: <i>Power ratio in decibels (dB) of a measured power referenced to 1 milliwatt (mW). [unit: decibel milliwatt (dBm), resolution: NA, accuracy: perfect]</i>
      *
      * @return the time stamped <code>receivedPower</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< float > getReceivedPowerTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>receivedTransmitterIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The object instance ID of the transmitter that generated the received radio signal.</i>
      *
      * @return true if <code>receivedTransmitterIdentifier</code> is available.
      */
      LIBAPI virtual bool hasReceivedTransmitterIdentifier() = 0;

      /**
      * Gets the value of the <code>receivedTransmitterIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>The object instance ID of the transmitter that generated the received radio signal.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @return the <code>receivedTransmitterIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::string getReceivedTransmitterIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>receivedTransmitterIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The object instance ID of the transmitter that generated the received radio signal.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>receivedTransmitterIdentifier</code> attribute.
      */
      LIBAPI virtual std::string getReceivedTransmitterIdentifier(std::string defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>receivedTransmitterIdentifier</code> attribute.
      * <br>Description from the FOM: <i>The object instance ID of the transmitter that generated the received radio signal.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @return the time stamped <code>receivedTransmitterIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::string > getReceivedTransmitterIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>receiverOperationalStatus</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The operational state of the radio receiver.</i>
      *
      * @return true if <code>receiverOperationalStatus</code> is available.
      */
      LIBAPI virtual bool hasReceiverOperationalStatus() = 0;

      /**
      * Gets the value of the <code>receiverOperationalStatus</code> attribute.
      *
      * <br>Description from the FOM: <i>The operational state of the radio receiver.</i>
      * <br>Description of the data type from the FOM: <i>The operational state of a radio receiver.</i>
      *
      * @return the <code>receiverOperationalStatus</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::ReceiverOperationalStatusEnum::ReceiverOperationalStatusEnum getReceiverOperationalStatus()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>receiverOperationalStatus</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The operational state of the radio receiver.</i>
      * <br>Description of the data type from the FOM: <i>The operational state of a radio receiver.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>receiverOperationalStatus</code> attribute.
      */
      LIBAPI virtual DevStudio::ReceiverOperationalStatusEnum::ReceiverOperationalStatusEnum getReceiverOperationalStatus(DevStudio::ReceiverOperationalStatusEnum::ReceiverOperationalStatusEnum defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>receiverOperationalStatus</code> attribute.
      * <br>Description from the FOM: <i>The operational state of the radio receiver.</i>
      * <br>Description of the data type from the FOM: <i>The operational state of a radio receiver.</i>
      *
      * @return the time stamped <code>receiverOperationalStatus</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::ReceiverOperationalStatusEnum::ReceiverOperationalStatusEnum > getReceiverOperationalStatusTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>entityIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
      *
      * @return true if <code>entityIdentifier</code> is available.
      */
      LIBAPI virtual bool hasEntityIdentifier() = 0;

      /**
      * Gets the value of the <code>entityIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @return the <code>entityIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::EntityIdentifierStruct getEntityIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>entityIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>entityIdentifier</code> attribute.
      */
      LIBAPI virtual DevStudio::EntityIdentifierStruct getEntityIdentifier(DevStudio::EntityIdentifierStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>entityIdentifier</code> attribute.
      * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @return the time stamped <code>entityIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::EntityIdentifierStruct > getEntityIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>hostObjectIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
      *
      * @return true if <code>hostObjectIdentifier</code> is available.
      */
      LIBAPI virtual bool hasHostObjectIdentifier() = 0;

      /**
      * Gets the value of the <code>hostObjectIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @return the <code>hostObjectIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::string getHostObjectIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>hostObjectIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>hostObjectIdentifier</code> attribute.
      */
      LIBAPI virtual std::string getHostObjectIdentifier(std::string defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>hostObjectIdentifier</code> attribute.
      * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @return the time stamped <code>hostObjectIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::string > getHostObjectIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>relativePosition</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
      *
      * @return true if <code>relativePosition</code> is available.
      */
      LIBAPI virtual bool hasRelativePosition() = 0;

      /**
      * Gets the value of the <code>relativePosition</code> attribute.
      *
      * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
      * <br>Description of the data type from the FOM: <i>Relative position in right-handed Cartesian coordinates.</i>
      *
      * @return the <code>relativePosition</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::RelativePositionStruct getRelativePosition()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>relativePosition</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
      * <br>Description of the data type from the FOM: <i>Relative position in right-handed Cartesian coordinates.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>relativePosition</code> attribute.
      */
      LIBAPI virtual DevStudio::RelativePositionStruct getRelativePosition(DevStudio::RelativePositionStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>relativePosition</code> attribute.
      * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
      * <br>Description of the data type from the FOM: <i>Relative position in right-handed Cartesian coordinates.</i>
      *
      * @return the time stamped <code>relativePosition</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::RelativePositionStruct > getRelativePositionTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
   };
}
#endif
