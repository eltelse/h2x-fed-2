/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAENVIRONMENTOBJECTMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLAENVIRONMENTOBJECTMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaEnvironmentObject.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaEnvironmentObject instances.
    */
    class HlaEnvironmentObjectManagerListener {

    public:

        LIBAPI virtual ~HlaEnvironmentObjectManagerListener() {}

        /**
        * This method is called when a new HlaEnvironmentObject instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param environmentObject the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaEnvironmentObjectDiscovered(HlaEnvironmentObjectPtr environmentObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaEnvironmentObject instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param environmentObject the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaEnvironmentObjectInitialized(HlaEnvironmentObjectPtr environmentObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaEnvironmentObjectManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param environmentObject the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaEnvironmentObjectInInterest(HlaEnvironmentObjectPtr environmentObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaEnvironmentObjectManagerListener instance goes out of interest.
        *
        * @param environmentObject the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaEnvironmentObjectOutOfInterest(HlaEnvironmentObjectPtr environmentObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaEnvironmentObject instance is deleted.
        *
        * @param environmentObject the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaEnvironmentObjectDeleted(HlaEnvironmentObjectPtr environmentObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaEnvironmentObjectManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaEnvironmentObjectManagerListener::Adapter : public HlaEnvironmentObjectManagerListener {

    public:
        LIBAPI virtual void hlaEnvironmentObjectDiscovered(HlaEnvironmentObjectPtr environmentObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaEnvironmentObjectInitialized(HlaEnvironmentObjectPtr environmentObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaEnvironmentObjectInInterest(HlaEnvironmentObjectPtr environmentObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaEnvironmentObjectOutOfInterest(HlaEnvironmentObjectPtr environmentObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaEnvironmentObjectDeleted(HlaEnvironmentObjectPtr environmentObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
