/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLASENSORMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLASENSORMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaSensor.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaSensor instances.
    */
    class HlaSensorManagerListener {

    public:

        LIBAPI virtual ~HlaSensorManagerListener() {}

        /**
        * This method is called when a new HlaSensor instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param sensor the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaSensorDiscovered(HlaSensorPtr sensor, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaSensor instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param sensor the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaSensorInitialized(HlaSensorPtr sensor, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaSensorManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param sensor the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaSensorInInterest(HlaSensorPtr sensor, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaSensorManagerListener instance goes out of interest.
        *
        * @param sensor the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaSensorOutOfInterest(HlaSensorPtr sensor, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaSensor instance is deleted.
        *
        * @param sensor the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaSensorDeleted(HlaSensorPtr sensor, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaSensorManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaSensorManagerListener::Adapter : public HlaSensorManagerListener {

    public:
        LIBAPI virtual void hlaSensorDiscovered(HlaSensorPtr sensor, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaSensorInitialized(HlaSensorPtr sensor, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaSensorInInterest(HlaSensorPtr sensor, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaSensorOutOfInterest(HlaSensorPtr sensor, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaSensorDeleted(HlaSensorPtr sensor, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
