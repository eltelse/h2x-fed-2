/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAOTHERLINEAROBJECTATTRIBUTES_H
#define DEVELOPER_STUDIO_HLAOTHERLINEAROBJECTATTRIBUTES_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <string>
#include <vector>
#include <utility>
    
#include <boost/noncopyable.hpp>
    
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EnvironmentObjectTypeStruct.h>
#include <DevStudio/datatypes/ForceIdentifierEnum.h>
#include <string>

#include <DevStudio/HlaException.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaLinearObjectAttributes.h>
#include <DevStudio/HlaTimeStamped.h>

namespace DevStudio {

   /**
   * Interface used to represent all attributes for an object instance.
   */
   class HlaOtherLinearObjectAttributes : public HlaLinearObjectAttributes {
   public:


     /**
      * An enumeration of the attributes of an HlaOtherLinearObject
      *
      *<table summary="All attributes">
      * <tr><td><b>Enum constant</b></td><td><b>C++ name</b></td><td><b>FOM name</b></td></tr>
      * <tr><td>OBJECT_IDENTIFIER</td><td>objectIdentifier</td><td><code>ObjectIdentifier</code></td></tr>
      * <tr><td>REFERENCED_OBJECT_IDENTIFIER</td><td>referencedObjectIdentifier</td><td><code>ReferencedObjectIdentifier</code></td></tr>
      * <tr><td>FORCE_IDENTIFIER</td><td>forceIdentifier</td><td><code>ForceIdentifier</code></td></tr>
      * <tr><td>OBJECT_TYPE</td><td>objectType</td><td><code>ObjectType</code></td></tr>
      *</table>
      */
      enum Attribute {
        /**
        * objectIdentifier (FOM name: <code>ObjectIdentifier</code>).
        * <br>Description from the FOM: <i>Identifies this EnvironmentObject instance (point, linear or areal)</i>
        */
         OBJECT_IDENTIFIER,

        /**
        * referencedObjectIdentifier (FOM name: <code>ReferencedObjectIdentifier</code>).
        * <br>Description from the FOM: <i>Identifies the Synthetic Environment object instance to which this EnvironmentObject instance is associated</i>
        */
         REFERENCED_OBJECT_IDENTIFIER,

        /**
        * forceIdentifier (FOM name: <code>ForceIdentifier</code>).
        * <br>Description from the FOM: <i>Identifies the force that created or modified this EnvironmentObject instance</i>
        */
         FORCE_IDENTIFIER,

        /**
        * objectType (FOM name: <code>ObjectType</code>).
        * <br>Description from the FOM: <i>Identifies the type of this EnvironmentObject instance</i>
        */
         OBJECT_TYPE
      };

     /**
      * Gets the name of the attribute.
      *
      * @return The name of the attribute. An empty string will be returned if the attribute does not exist.
      */
      LIBAPI virtual std::wstring getName(Attribute attribute);

     /**
      * Finds the attribute specified in the parameter attributeName.
      * The found enumeration will be stored in the parameter attribute.
      *
      * @return true if the attribute was found, false otherwise.
      */
      LIBAPI virtual bool find(Attribute& attribute, std::wstring attributeName);

      LIBAPI virtual ~HlaOtherLinearObjectAttributes() {}
    
      /**
      * Returns true if the <code>objectIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Identifies this EnvironmentObject instance (point, linear or areal)</i>
      *
      * @return true if <code>objectIdentifier</code> is available.
      */
      LIBAPI virtual bool hasObjectIdentifier() = 0;

      /**
      * Gets the value of the <code>objectIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>Identifies this EnvironmentObject instance (point, linear or areal)</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @return the <code>objectIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::EntityIdentifierStruct getObjectIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>objectIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Identifies this EnvironmentObject instance (point, linear or areal)</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>objectIdentifier</code> attribute.
      */
      LIBAPI virtual DevStudio::EntityIdentifierStruct getObjectIdentifier(DevStudio::EntityIdentifierStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>objectIdentifier</code> attribute.
      * <br>Description from the FOM: <i>Identifies this EnvironmentObject instance (point, linear or areal)</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @return the time stamped <code>objectIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::EntityIdentifierStruct > getObjectIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>referencedObjectIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Identifies the Synthetic Environment object instance to which this EnvironmentObject instance is associated</i>
      *
      * @return true if <code>referencedObjectIdentifier</code> is available.
      */
      LIBAPI virtual bool hasReferencedObjectIdentifier() = 0;

      /**
      * Gets the value of the <code>referencedObjectIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>Identifies the Synthetic Environment object instance to which this EnvironmentObject instance is associated</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @return the <code>referencedObjectIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::string getReferencedObjectIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>referencedObjectIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Identifies the Synthetic Environment object instance to which this EnvironmentObject instance is associated</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>referencedObjectIdentifier</code> attribute.
      */
      LIBAPI virtual std::string getReferencedObjectIdentifier(std::string defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>referencedObjectIdentifier</code> attribute.
      * <br>Description from the FOM: <i>Identifies the Synthetic Environment object instance to which this EnvironmentObject instance is associated</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @return the time stamped <code>referencedObjectIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::string > getReferencedObjectIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>forceIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Identifies the force that created or modified this EnvironmentObject instance</i>
      *
      * @return true if <code>forceIdentifier</code> is available.
      */
      LIBAPI virtual bool hasForceIdentifier() = 0;

      /**
      * Gets the value of the <code>forceIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>Identifies the force that created or modified this EnvironmentObject instance</i>
      * <br>Description of the data type from the FOM: <i>Force ID</i>
      *
      * @return the <code>forceIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::ForceIdentifierEnum::ForceIdentifierEnum getForceIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>forceIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Identifies the force that created or modified this EnvironmentObject instance</i>
      * <br>Description of the data type from the FOM: <i>Force ID</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>forceIdentifier</code> attribute.
      */
      LIBAPI virtual DevStudio::ForceIdentifierEnum::ForceIdentifierEnum getForceIdentifier(DevStudio::ForceIdentifierEnum::ForceIdentifierEnum defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>forceIdentifier</code> attribute.
      * <br>Description from the FOM: <i>Identifies the force that created or modified this EnvironmentObject instance</i>
      * <br>Description of the data type from the FOM: <i>Force ID</i>
      *
      * @return the time stamped <code>forceIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::ForceIdentifierEnum::ForceIdentifierEnum > getForceIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>objectType</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Identifies the type of this EnvironmentObject instance</i>
      *
      * @return true if <code>objectType</code> is available.
      */
      LIBAPI virtual bool hasObjectType() = 0;

      /**
      * Gets the value of the <code>objectType</code> attribute.
      *
      * <br>Description from the FOM: <i>Identifies the type of this EnvironmentObject instance</i>
      * <br>Description of the data type from the FOM: <i>Record specifying the domain, the kind and the specific identification of the environment object</i>
      *
      * @return the <code>objectType</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::EnvironmentObjectTypeStruct getObjectType()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>objectType</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Identifies the type of this EnvironmentObject instance</i>
      * <br>Description of the data type from the FOM: <i>Record specifying the domain, the kind and the specific identification of the environment object</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>objectType</code> attribute.
      */
      LIBAPI virtual DevStudio::EnvironmentObjectTypeStruct getObjectType(DevStudio::EnvironmentObjectTypeStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>objectType</code> attribute.
      * <br>Description from the FOM: <i>Identifies the type of this EnvironmentObject instance</i>
      * <br>Description of the data type from the FOM: <i>Record specifying the domain, the kind and the specific identification of the environment object</i>
      *
      * @return the time stamped <code>objectType</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::EnvironmentObjectTypeStruct > getObjectTypeTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
   };
}
#endif
