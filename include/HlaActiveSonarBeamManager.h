/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAACTIVESONARBEAMMANAGER_H
#define DEVELOPER_STUDIO_HLAACTIVESONARBEAMMANAGER_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <DevStudio/datatypes/ActiveSonarScanPatternEnum.h>
#include <DevStudio/datatypes/EventIdentifierStruct.h>
#include <string>

#include <list>
#include <vector>

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaActiveSonarBeamManagerListener.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaException.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

   /**
   * Manager used to manage HlaActiveSonarBeams.
   */
   class HlaActiveSonarBeamManager : private boost::noncopyable {

   public:

      LIBAPI virtual ~HlaActiveSonarBeamManager() {}

      /**
      * Gets a list of all HlaActiveSonarBeams within interest, both local and remote.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of HlaActiveSonarBeams
      */
      LIBAPI virtual std::list<HlaActiveSonarBeamPtr> getHlaActiveSonarBeams() = 0;

      /**
      * Gets a list of all HlaActiveSonarBeams, both local and remote.
      * HlaActiveSonarBeams not within interest are also included.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of all HlaActiveSonarBeams
      */
      LIBAPI virtual std::list<HlaActiveSonarBeamPtr> getAllHlaActiveSonarBeams() = 0;

      /**
      * Gets a list of local HlaActiveSonarBeams within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of local HlaActiveSonarBeams
      */
      LIBAPI virtual std::list<HlaActiveSonarBeamPtr> getLocalHlaActiveSonarBeams() = 0;

      /**
      * Gets a list of remote HlaActiveSonarBeams within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of remote HlaActiveSonarBeams
      */
      LIBAPI virtual std::list<HlaActiveSonarBeamPtr> getRemoteHlaActiveSonarBeams() = 0;

      /**
      * Find a HlaActiveSonarBeam with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> of the HlaActiveSonarBeam to find
      *
      * @return the specified HlaActiveSonarBeam, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaActiveSonarBeamPtr getActiveSonarBeamByHlaInstanceName(const std::wstring& hlaInstanceName) = 0;

      /**
      * Find a HlaActiveSonarBeam with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param encodedHlaInstanceHandle the encoded <code>HLA instance handle</code> of the HlaActiveSonarBeam to find
      *
      * @return the specified HlaActiveSonarBeam, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaActiveSonarBeamPtr getActiveSonarBeamByHlaInstanceHandle(const std::vector<char>& encodedHlaInstanceHandle) = 0;

      /**
      * Creates a new HlaActiveSonarBeam, and sets all <i>Create</i> attributes.
      *
      * @return Returns a new local HlaActiveSonarBeam.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaActiveSonarBeamPtr createLocalHlaActiveSonarBeam(
      ) THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Creates a new HlaActiveSonarBeam with a specified <code>HLA instance name</code>, and sets all
      * <i>Create</i> attributes.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> to register for this instance
      *
      * @return Returns a new local HlaActiveSonarBeam.
      *
      * @throws HlaIllegalInstanceNameException if the name is illegal
      * @throws HlaInstanceNameInUseException if the name is already in use
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaActiveSonarBeamPtr createLocalHlaActiveSonarBeam(
         const std::wstring& hlaInstanceName
      ) THROW_SPEC (HlaIllegalInstanceNameException, HlaInstanceNameInUseException,
                    HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaActiveSonarBeam and removes it from the federation.
      *
      * @param activeSonarBeam The HlaActiveSonarBeam to delete
      *
      * @return the HlaActiveSonarBeam deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaActiveSonarBeamPtr deleteLocalHlaActiveSonarBeam(HlaActiveSonarBeamPtr activeSonarBeam)
          THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                      HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaActiveSonarBeam and removes it from the federation.
      *
      * @param activeSonarBeam The HlaActiveSonarBeam to delete
      * @param timestamp timestamp when the instance was removed.
      *
      * @return the HlaActiveSonarBeam deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaActiveSonarBeamPtr deleteLocalHlaActiveSonarBeam(HlaActiveSonarBeamPtr activeSonarBeam, HlaTimeStampPtr timestamp)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaActiveSonarBeam and removes it from the federation.
      *
      * @param activeSonarBeam The HlaActiveSonarBeam to delete
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaActiveSonarBeam deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaActiveSonarBeamPtr deleteLocalHlaActiveSonarBeam(HlaActiveSonarBeamPtr activeSonarBeam, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaActiveSonarBeam and removes it from the federation.
      *
      * @param activeSonarBeam The HlaActiveSonarBeam to delete
      * @param timestamp timestamp when the instance was removed.
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaActiveSonarBeam deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaActiveSonarBeamPtr deleteLocalHlaActiveSonarBeam(HlaActiveSonarBeamPtr activeSonarBeam, HlaTimeStampPtr timestamp, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Add a HlaActiveSonarBeam manager listener.
      *
      * @param managerListener listener to add
      */
      LIBAPI virtual void addHlaActiveSonarBeamManagerListener(HlaActiveSonarBeamManagerListenerPtr managerListener) = 0;

      /**
      * Removed a HlaActiveSonarBeam manager listener.
      *
      * @param managerListener listener to remove
      */
      LIBAPI virtual void removeHlaActiveSonarBeamManagerListener(HlaActiveSonarBeamManagerListenerPtr managerListener) = 0;

     /**
      * Add a listener that will automatically be added to all instances of HlaActiveSonarBeam (Both local and remote).
      * The listener is notified when any attribute of an instance of HlaActiveSonarBeam is updated.
      * The listener is also called when an interaction is sent to an instance of HlaActiveSonarBeam.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param listener The listener to add.
      */
      LIBAPI virtual void addHlaActiveSonarBeamDefaultInstanceListener(HlaActiveSonarBeamListenerPtr listener) = 0;

     /**
      * Remove a listener from the set of listeners that are automatically added to new instances of HlaActiveSonarBeam.
      * Note: The listener will not be removed from already existing instances of HlaActiveSonarBeam.
      * This method is idempotent.
      *
      * @param listener The listener to remove.
      */
      LIBAPI virtual void removeHlaActiveSonarBeamDefaultInstanceListener(HlaActiveSonarBeamListenerPtr listener) = 0;

     /**
      * Add a valueListener that will automatically be added to all instances of HlaActiveSonarBeam (Both local and remote).
      * The  Valuelistener is notified when any attribute of an instance of HlaActiveSonarBeam is updated.
      * Note that the attribute values that are notified are not guaranteed to
      * be the latest available values for that attribute.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to add.
      */
      LIBAPI virtual void addHlaActiveSonarBeamDefaultInstanceValueListener(HlaActiveSonarBeamValueListenerPtr valueListener) = 0;

     /**
      * Remove a valueListener from the set of listeners that are automatically added to new instances of HlaActiveSonarBeam.
      * Note: The valueListener will not be removed from already existing instances of HlaActiveSonarBeam.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to remove.
      */
      LIBAPI virtual void removeHlaActiveSonarBeamDefaultInstanceValueListener(HlaActiveSonarBeamValueListenerPtr valueListener) = 0;

     /**
      * Enables or disables the HlaActiveSonarBeam manager.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaActiveSonarBeam manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      *
      * @param enabled <code>true</code> to enable the HlaActiveSonarBeam manager, otherwise <code>false</code>
      */
      LIBAPI virtual void setEnabled(bool enabled) = 0;

     /**
      * Enables the HlaActiveSonarBeam manager if it is avalable in the FOM at runtime.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaActiveSonarBeam manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      */
      LIBAPI virtual void setEnabledIfAvailableInFom() = 0;

     /**
      * Check if the HlaActiveSonarBeam manager is actually enabled when connected.
      * An HlaActiveSonarBeam manager may be disabled with the <code>setEnabled(false)</code> function, or it may be configured
      * as <i>if available in the FOM</i> by the <code>setEnabledIfAvailableInFom()</code> function and
      * then not available in the FOM at runtime.
      * <p>
      * This method should only be used when connected, it will always return <code>false</code>
      * when not connected.
      *
      * @return <code>true</code> if connected and the HlaActiveSonarBeam manager is enabled.
      */
      LIBAPI virtual bool isEnabled() = 0;
   };
}
#endif
