/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAENVIRONMENTALENTITYMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLAENVIRONMENTALENTITYMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaEnvironmentalEntity.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaEnvironmentalEntity instances.
    */
    class HlaEnvironmentalEntityManagerListener {

    public:

        LIBAPI virtual ~HlaEnvironmentalEntityManagerListener() {}

        /**
        * This method is called when a new HlaEnvironmentalEntity instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param environmentalEntity the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaEnvironmentalEntityDiscovered(HlaEnvironmentalEntityPtr environmentalEntity, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaEnvironmentalEntity instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param environmentalEntity the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaEnvironmentalEntityInitialized(HlaEnvironmentalEntityPtr environmentalEntity, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaEnvironmentalEntityManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param environmentalEntity the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaEnvironmentalEntityInInterest(HlaEnvironmentalEntityPtr environmentalEntity, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaEnvironmentalEntityManagerListener instance goes out of interest.
        *
        * @param environmentalEntity the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaEnvironmentalEntityOutOfInterest(HlaEnvironmentalEntityPtr environmentalEntity, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaEnvironmentalEntity instance is deleted.
        *
        * @param environmentalEntity the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaEnvironmentalEntityDeleted(HlaEnvironmentalEntityPtr environmentalEntity, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaEnvironmentalEntityManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaEnvironmentalEntityManagerListener::Adapter : public HlaEnvironmentalEntityManagerListener {

    public:
        LIBAPI virtual void hlaEnvironmentalEntityDiscovered(HlaEnvironmentalEntityPtr environmentalEntity, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaEnvironmentalEntityInitialized(HlaEnvironmentalEntityPtr environmentalEntity, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaEnvironmentalEntityInInterest(HlaEnvironmentalEntityPtr environmentalEntity, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaEnvironmentalEntityOutOfInterest(HlaEnvironmentalEntityPtr environmentalEntity, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaEnvironmentalEntityDeleted(HlaEnvironmentalEntityPtr environmentalEntity, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
