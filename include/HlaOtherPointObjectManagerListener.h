/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAOTHERPOINTOBJECTMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLAOTHERPOINTOBJECTMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaOtherPointObject.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaOtherPointObject instances.
    */
    class HlaOtherPointObjectManagerListener {

    public:

        LIBAPI virtual ~HlaOtherPointObjectManagerListener() {}

        /**
        * This method is called when a new HlaOtherPointObject instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param otherPointObject the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaOtherPointObjectDiscovered(HlaOtherPointObjectPtr otherPointObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaOtherPointObject instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param otherPointObject the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaOtherPointObjectInitialized(HlaOtherPointObjectPtr otherPointObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaOtherPointObjectManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param otherPointObject the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaOtherPointObjectInInterest(HlaOtherPointObjectPtr otherPointObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaOtherPointObjectManagerListener instance goes out of interest.
        *
        * @param otherPointObject the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaOtherPointObjectOutOfInterest(HlaOtherPointObjectPtr otherPointObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaOtherPointObject instance is deleted.
        *
        * @param otherPointObject the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaOtherPointObjectDeleted(HlaOtherPointObjectPtr otherPointObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaOtherPointObjectManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaOtherPointObjectManagerListener::Adapter : public HlaOtherPointObjectManagerListener {

    public:
        LIBAPI virtual void hlaOtherPointObjectDiscovered(HlaOtherPointObjectPtr otherPointObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaOtherPointObjectInitialized(HlaOtherPointObjectPtr otherPointObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaOtherPointObjectInInterest(HlaOtherPointObjectPtr otherPointObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaOtherPointObjectOutOfInterest(HlaOtherPointObjectPtr otherPointObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaOtherPointObjectDeleted(HlaOtherPointObjectPtr otherPointObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
