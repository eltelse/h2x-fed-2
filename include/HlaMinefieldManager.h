/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAMINEFIELDMANAGER_H
#define DEVELOPER_STUDIO_HLAMINEFIELDMANAGER_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EntityTypeStruct.h>
#include <DevStudio/datatypes/EntityTypeStructLengthlessArray.h>
#include <DevStudio/datatypes/ForceIdentifierEnum.h>
#include <DevStudio/datatypes/MinefieldLaneEnum.h>
#include <DevStudio/datatypes/MinefieldProtocolEnum.h>
#include <DevStudio/datatypes/MinefieldStatusEnum.h>
#include <DevStudio/datatypes/MinefieldTypeEnum.h>
#include <DevStudio/datatypes/OrientationStruct.h>
#include <DevStudio/datatypes/PerimeterPointStruct.h>
#include <DevStudio/datatypes/PerimeterPointStructLengthlessArray.h>
#include <DevStudio/datatypes/WorldLocationStruct.h>
#include <vector>

#include <list>
#include <vector>

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaMinefieldManagerListener.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaException.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

   /**
   * Manager used to manage HlaMinefields.
   */
   class HlaMinefieldManager : private boost::noncopyable {

   public:

      LIBAPI virtual ~HlaMinefieldManager() {}

      /**
      * Gets a list of all HlaMinefields within interest, both local and remote.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of HlaMinefields
      */
      LIBAPI virtual std::list<HlaMinefieldPtr> getHlaMinefields() = 0;

      /**
      * Gets a list of all HlaMinefields, both local and remote.
      * HlaMinefields not within interest are also included.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of all HlaMinefields
      */
      LIBAPI virtual std::list<HlaMinefieldPtr> getAllHlaMinefields() = 0;

      /**
      * Gets a list of local HlaMinefields within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of local HlaMinefields
      */
      LIBAPI virtual std::list<HlaMinefieldPtr> getLocalHlaMinefields() = 0;

      /**
      * Gets a list of remote HlaMinefields within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of remote HlaMinefields
      */
      LIBAPI virtual std::list<HlaMinefieldPtr> getRemoteHlaMinefields() = 0;

      /**
      * Find a HlaMinefield with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> of the HlaMinefield to find
      *
      * @return the specified HlaMinefield, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaMinefieldPtr getMinefieldByHlaInstanceName(const std::wstring& hlaInstanceName) = 0;

      /**
      * Find a HlaMinefield with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param encodedHlaInstanceHandle the encoded <code>HLA instance handle</code> of the HlaMinefield to find
      *
      * @return the specified HlaMinefield, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaMinefieldPtr getMinefieldByHlaInstanceHandle(const std::vector<char>& encodedHlaInstanceHandle) = 0;

      /**
      * Creates a new HlaMinefield, and sets all <i>Create</i> attributes.
      *
      * @return Returns a new local HlaMinefield.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaMinefieldPtr createLocalHlaMinefield(
      ) THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Creates a new HlaMinefield with a specified <code>HLA instance name</code>, and sets all
      * <i>Create</i> attributes.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> to register for this instance
      *
      * @return Returns a new local HlaMinefield.
      *
      * @throws HlaIllegalInstanceNameException if the name is illegal
      * @throws HlaInstanceNameInUseException if the name is already in use
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaMinefieldPtr createLocalHlaMinefield(
         const std::wstring& hlaInstanceName
      ) THROW_SPEC (HlaIllegalInstanceNameException, HlaInstanceNameInUseException,
                    HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaMinefield and removes it from the federation.
      *
      * @param minefield The HlaMinefield to delete
      *
      * @return the HlaMinefield deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaMinefieldPtr deleteLocalHlaMinefield(HlaMinefieldPtr minefield)
          THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                      HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaMinefield and removes it from the federation.
      *
      * @param minefield The HlaMinefield to delete
      * @param timestamp timestamp when the instance was removed.
      *
      * @return the HlaMinefield deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaMinefieldPtr deleteLocalHlaMinefield(HlaMinefieldPtr minefield, HlaTimeStampPtr timestamp)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaMinefield and removes it from the federation.
      *
      * @param minefield The HlaMinefield to delete
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaMinefield deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaMinefieldPtr deleteLocalHlaMinefield(HlaMinefieldPtr minefield, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaMinefield and removes it from the federation.
      *
      * @param minefield The HlaMinefield to delete
      * @param timestamp timestamp when the instance was removed.
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaMinefield deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaMinefieldPtr deleteLocalHlaMinefield(HlaMinefieldPtr minefield, HlaTimeStampPtr timestamp, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Add a HlaMinefield manager listener.
      *
      * @param managerListener listener to add
      */
      LIBAPI virtual void addHlaMinefieldManagerListener(HlaMinefieldManagerListenerPtr managerListener) = 0;

      /**
      * Removed a HlaMinefield manager listener.
      *
      * @param managerListener listener to remove
      */
      LIBAPI virtual void removeHlaMinefieldManagerListener(HlaMinefieldManagerListenerPtr managerListener) = 0;

     /**
      * Add a listener that will automatically be added to all instances of HlaMinefield (Both local and remote).
      * The listener is notified when any attribute of an instance of HlaMinefield is updated.
      * The listener is also called when an interaction is sent to an instance of HlaMinefield.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param listener The listener to add.
      */
      LIBAPI virtual void addHlaMinefieldDefaultInstanceListener(HlaMinefieldListenerPtr listener) = 0;

     /**
      * Remove a listener from the set of listeners that are automatically added to new instances of HlaMinefield.
      * Note: The listener will not be removed from already existing instances of HlaMinefield.
      * This method is idempotent.
      *
      * @param listener The listener to remove.
      */
      LIBAPI virtual void removeHlaMinefieldDefaultInstanceListener(HlaMinefieldListenerPtr listener) = 0;

     /**
      * Add a valueListener that will automatically be added to all instances of HlaMinefield (Both local and remote).
      * The  Valuelistener is notified when any attribute of an instance of HlaMinefield is updated.
      * Note that the attribute values that are notified are not guaranteed to
      * be the latest available values for that attribute.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to add.
      */
      LIBAPI virtual void addHlaMinefieldDefaultInstanceValueListener(HlaMinefieldValueListenerPtr valueListener) = 0;

     /**
      * Remove a valueListener from the set of listeners that are automatically added to new instances of HlaMinefield.
      * Note: The valueListener will not be removed from already existing instances of HlaMinefield.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to remove.
      */
      LIBAPI virtual void removeHlaMinefieldDefaultInstanceValueListener(HlaMinefieldValueListenerPtr valueListener) = 0;

     /**
      * Enables or disables the HlaMinefield manager.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaMinefield manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      *
      * @param enabled <code>true</code> to enable the HlaMinefield manager, otherwise <code>false</code>
      */
      LIBAPI virtual void setEnabled(bool enabled) = 0;

     /**
      * Enables the HlaMinefield manager if it is avalable in the FOM at runtime.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaMinefield manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      */
      LIBAPI virtual void setEnabledIfAvailableInFom() = 0;

     /**
      * Check if the HlaMinefield manager is actually enabled when connected.
      * An HlaMinefield manager may be disabled with the <code>setEnabled(false)</code> function, or it may be configured
      * as <i>if available in the FOM</i> by the <code>setEnabledIfAvailableInFom()</code> function and
      * then not available in the FOM at runtime.
      * <p>
      * This method should only be used when connected, it will always return <code>false</code>
      * when not connected.
      *
      * @return <code>true</code> if connected and the HlaMinefield manager is enabled.
      */
      LIBAPI virtual bool isEnabled() = 0;
   };
}
#endif
