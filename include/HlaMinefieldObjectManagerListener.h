/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAMINEFIELDOBJECTMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLAMINEFIELDOBJECTMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaMinefieldObject.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaMinefieldObject instances.
    */
    class HlaMinefieldObjectManagerListener {

    public:

        LIBAPI virtual ~HlaMinefieldObjectManagerListener() {}

        /**
        * This method is called when a new HlaMinefieldObject instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param minefieldObject the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaMinefieldObjectDiscovered(HlaMinefieldObjectPtr minefieldObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaMinefieldObject instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param minefieldObject the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaMinefieldObjectInitialized(HlaMinefieldObjectPtr minefieldObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaMinefieldObjectManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param minefieldObject the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaMinefieldObjectInInterest(HlaMinefieldObjectPtr minefieldObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaMinefieldObjectManagerListener instance goes out of interest.
        *
        * @param minefieldObject the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaMinefieldObjectOutOfInterest(HlaMinefieldObjectPtr minefieldObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaMinefieldObject instance is deleted.
        *
        * @param minefieldObject the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaMinefieldObjectDeleted(HlaMinefieldObjectPtr minefieldObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaMinefieldObjectManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaMinefieldObjectManagerListener::Adapter : public HlaMinefieldObjectManagerListener {

    public:
        LIBAPI virtual void hlaMinefieldObjectDiscovered(HlaMinefieldObjectPtr minefieldObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaMinefieldObjectInitialized(HlaMinefieldObjectPtr minefieldObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaMinefieldObjectInInterest(HlaMinefieldObjectPtr minefieldObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaMinefieldObjectOutOfInterest(HlaMinefieldObjectPtr minefieldObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaMinefieldObjectDeleted(HlaMinefieldObjectPtr minefieldObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
