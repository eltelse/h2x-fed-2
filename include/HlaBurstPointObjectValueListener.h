/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLABURSTPOINTOBJECTVALUELISTENER_H
#define DEVELOPER_STUDIO_HLABURSTPOINTOBJECTVALUELISTENER_H

#include <memory>

#include <DevStudio/datatypes/ChemicalContentEnum.h>
#include <DevStudio/datatypes/DamageStatusEnum.h>
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EnvironmentObjectTypeStruct.h>
#include <DevStudio/datatypes/ForceIdentifierEnum.h>
#include <DevStudio/datatypes/OrientationStruct.h>
#include <DevStudio/datatypes/WorldLocationStruct.h>
#include <string>

#include <DevStudio/HlaLogicalTime.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaBurstPointObjectAttributes.h>    

namespace DevStudio {

   /**
   * Listener for updates of attributes, with the new updated values.  
   */
   class HlaBurstPointObjectValueListener {

   public:

      LIBAPI virtual ~HlaBurstPointObjectValueListener() {}
    
      /**
      * This method is called when the attribute <code>percentOpacity</code> is updated.
      * <br>Description from the FOM: <i>Specifies the opacity of the smoke</i>
      * <br>Description of the data type from the FOM: <i>Percentage [unit: percent (%), resolution: 1, accuracy: perfect]</i>
      *
      * @param burstPointObject The object which is updated.
      * @param percentOpacity The new value of the attribute in this update
      * @param validOldPercentOpacity True if oldPercentOpacity contains a valid value
      * @param oldPercentOpacity The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void percentOpacityUpdated(HlaBurstPointObjectPtr burstPointObject, unsigned int percentOpacity, bool validOldPercentOpacity, unsigned int oldPercentOpacity, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>cylinderSize</code> is updated.
      * <br>Description from the FOM: <i>Specifies the radius of the cylinder approximating an individual smoke burst ; for multiple bursts, the center bottom of each cylinder is calculated based on the model used to represent the multiple bursts</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param burstPointObject The object which is updated.
      * @param cylinderSize The new value of the attribute in this update
      * @param validOldCylinderSize True if oldCylinderSize contains a valid value
      * @param oldCylinderSize The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void cylinderSizeUpdated(HlaBurstPointObjectPtr burstPointObject, unsigned int cylinderSize, bool validOldCylinderSize, unsigned int oldCylinderSize, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>cylinderHeight</code> is updated.
      * <br>Description from the FOM: <i>Specifies the height of the cylinder approximating an individual smoke burst ; for multiple bursts, the center bottom of each cylinder is calculated based on the model used to represent the multiple bursts</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param burstPointObject The object which is updated.
      * @param cylinderHeight The new value of the attribute in this update
      * @param validOldCylinderHeight True if oldCylinderHeight contains a valid value
      * @param oldCylinderHeight The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void cylinderHeightUpdated(HlaBurstPointObjectPtr burstPointObject, unsigned int cylinderHeight, bool validOldCylinderHeight, unsigned int oldCylinderHeight, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>numberOfBursts</code> is updated.
      * <br>Description from the FOM: <i>Specifies the number of bursts in the instance of tactical smoke</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param burstPointObject The object which is updated.
      * @param numberOfBursts The new value of the attribute in this update
      * @param validOldNumberOfBursts True if oldNumberOfBursts contains a valid value
      * @param oldNumberOfBursts The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void numberOfBurstsUpdated(HlaBurstPointObjectPtr burstPointObject, unsigned int numberOfBursts, bool validOldNumberOfBursts, unsigned int oldNumberOfBursts, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>chemicalContent</code> is updated.
      * <br>Description from the FOM: <i>Specifies the chemical content of the smoke</i>
      * <br>Description of the data type from the FOM: <i>Smoke chemical content</i>
      *
      * @param burstPointObject The object which is updated.
      * @param chemicalContent The new value of the attribute in this update
      * @param validOldChemicalContent True if oldChemicalContent contains a valid value
      * @param oldChemicalContent The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void chemicalContentUpdated(HlaBurstPointObjectPtr burstPointObject, ChemicalContentEnum::ChemicalContentEnum chemicalContent, bool validOldChemicalContent, ChemicalContentEnum::ChemicalContentEnum oldChemicalContent, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>location</code> is updated.
      * <br>Description from the FOM: <i>Specifies the location of the object based on x, y and z coordinates</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @param burstPointObject The object which is updated.
      * @param location The new value of the attribute in this update
      * @param validOldLocation True if oldLocation contains a valid value
      * @param oldLocation The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void locationUpdated(HlaBurstPointObjectPtr burstPointObject, WorldLocationStruct location, bool validOldLocation, WorldLocationStruct oldLocation, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>orientation</code> is updated.
      * <br>Description from the FOM: <i>Specifies the angles of rotation around the coordinate axis between the object's attitude and the reference coordinate system axes ; these are calculated as the Tait-Bryan Euler angles, specifying the successive rotations needed to transform from the world coordinate system to the object coordinate system</i>
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @param burstPointObject The object which is updated.
      * @param orientation The new value of the attribute in this update
      * @param validOldOrientation True if oldOrientation contains a valid value
      * @param oldOrientation The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void orientationUpdated(HlaBurstPointObjectPtr burstPointObject, OrientationStruct orientation, bool validOldOrientation, OrientationStruct oldOrientation, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>percentComplete</code> is updated.
      * <br>Description from the FOM: <i>Specifies the percent completion of the object</i>
      * <br>Description of the data type from the FOM: <i>Percentage [unit: percent (%), resolution: 1, accuracy: perfect]</i>
      *
      * @param burstPointObject The object which is updated.
      * @param percentComplete The new value of the attribute in this update
      * @param validOldPercentComplete True if oldPercentComplete contains a valid value
      * @param oldPercentComplete The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void percentCompleteUpdated(HlaBurstPointObjectPtr burstPointObject, unsigned int percentComplete, bool validOldPercentComplete, unsigned int oldPercentComplete, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>damagedAppearance</code> is updated.
      * <br>Description from the FOM: <i>Specifies the damaged appearance of the object instance</i>
      * <br>Description of the data type from the FOM: <i>Damaged appearance</i>
      *
      * @param burstPointObject The object which is updated.
      * @param damagedAppearance The new value of the attribute in this update
      * @param validOldDamagedAppearance True if oldDamagedAppearance contains a valid value
      * @param oldDamagedAppearance The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void damagedAppearanceUpdated(HlaBurstPointObjectPtr burstPointObject, DamageStatusEnum::DamageStatusEnum damagedAppearance, bool validOldDamagedAppearance, DamageStatusEnum::DamageStatusEnum oldDamagedAppearance, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>objectPreDistributed</code> is updated.
      * <br>Description from the FOM: <i>Specifies whether or not the object was created before the start of the exercise</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param burstPointObject The object which is updated.
      * @param objectPreDistributed The new value of the attribute in this update
      * @param validOldObjectPreDistributed True if oldObjectPreDistributed contains a valid value
      * @param oldObjectPreDistributed The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void objectPreDistributedUpdated(HlaBurstPointObjectPtr burstPointObject, bool objectPreDistributed, bool validOldObjectPreDistributed, bool oldObjectPreDistributed, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>deactivated</code> is updated.
      * <br>Description from the FOM: <i>Specifies whether or not the object has been deactivated (it has ceased to exist in the synthetic environment)</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param burstPointObject The object which is updated.
      * @param deactivated The new value of the attribute in this update
      * @param validOldDeactivated True if oldDeactivated contains a valid value
      * @param oldDeactivated The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void deactivatedUpdated(HlaBurstPointObjectPtr burstPointObject, bool deactivated, bool validOldDeactivated, bool oldDeactivated, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>smoking</code> is updated.
      * <br>Description from the FOM: <i>Specifies whether or not the object is smoking (creating a smoke plume)</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param burstPointObject The object which is updated.
      * @param smoking The new value of the attribute in this update
      * @param validOldSmoking True if oldSmoking contains a valid value
      * @param oldSmoking The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void smokingUpdated(HlaBurstPointObjectPtr burstPointObject, bool smoking, bool validOldSmoking, bool oldSmoking, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>flaming</code> is updated.
      * <br>Description from the FOM: <i>Specifies whether or not the object is aflame</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param burstPointObject The object which is updated.
      * @param flaming The new value of the attribute in this update
      * @param validOldFlaming True if oldFlaming contains a valid value
      * @param oldFlaming The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void flamingUpdated(HlaBurstPointObjectPtr burstPointObject, bool flaming, bool validOldFlaming, bool oldFlaming, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>objectIdentifier</code> is updated.
      * <br>Description from the FOM: <i>Identifies this EnvironmentObject instance (point, linear or areal)</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @param burstPointObject The object which is updated.
      * @param objectIdentifier The new value of the attribute in this update
      * @param validOldObjectIdentifier True if oldObjectIdentifier contains a valid value
      * @param oldObjectIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void objectIdentifierUpdated(HlaBurstPointObjectPtr burstPointObject, EntityIdentifierStruct objectIdentifier, bool validOldObjectIdentifier, EntityIdentifierStruct oldObjectIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>referencedObjectIdentifier</code> is updated.
      * <br>Description from the FOM: <i>Identifies the Synthetic Environment object instance to which this EnvironmentObject instance is associated</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @param burstPointObject The object which is updated.
      * @param referencedObjectIdentifier The new value of the attribute in this update
      * @param validOldReferencedObjectIdentifier True if oldReferencedObjectIdentifier contains a valid value
      * @param oldReferencedObjectIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void referencedObjectIdentifierUpdated(HlaBurstPointObjectPtr burstPointObject, std::string referencedObjectIdentifier, bool validOldReferencedObjectIdentifier, std::string oldReferencedObjectIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>forceIdentifier</code> is updated.
      * <br>Description from the FOM: <i>Identifies the force that created or modified this EnvironmentObject instance</i>
      * <br>Description of the data type from the FOM: <i>Force ID</i>
      *
      * @param burstPointObject The object which is updated.
      * @param forceIdentifier The new value of the attribute in this update
      * @param validOldForceIdentifier True if oldForceIdentifier contains a valid value
      * @param oldForceIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void forceIdentifierUpdated(HlaBurstPointObjectPtr burstPointObject, ForceIdentifierEnum::ForceIdentifierEnum forceIdentifier, bool validOldForceIdentifier, ForceIdentifierEnum::ForceIdentifierEnum oldForceIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>objectType</code> is updated.
      * <br>Description from the FOM: <i>Identifies the type of this EnvironmentObject instance</i>
      * <br>Description of the data type from the FOM: <i>Record specifying the domain, the kind and the specific identification of the environment object</i>
      *
      * @param burstPointObject The object which is updated.
      * @param objectType The new value of the attribute in this update
      * @param validOldObjectType True if oldObjectType contains a valid value
      * @param oldObjectType The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void objectTypeUpdated(HlaBurstPointObjectPtr burstPointObject, EnvironmentObjectTypeStruct objectType, bool validOldObjectType, EnvironmentObjectTypeStruct oldObjectType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
      /**
      * This method is called when the producing federate of an attribute is changed.
      * Only available when using HLA Evolved.
      *
      * @param burstPointObject The object which is updated.
      * @param attribute The attribute that now has a new producing federate
      * @param oldProducingFederate The federate handle of the old producing federate
      * @param newProducingFederate The federate handle of the new producing federate
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void producingFederateUpdated(HlaBurstPointObjectPtr burstPointObject, HlaBurstPointObjectAttributes::Attribute attribute, HlaFederateIdPtr oldProducingFederate, HlaFederateIdPtr newProducingFederate, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;

      class Adapter;
   };

   /**
   * An adapter class that implements the HlaBurstPointObjectValueListener interface with empty methods.
   * It might be used as a base class for a listener.
   */
   class HlaBurstPointObjectValueListener::Adapter : public HlaBurstPointObjectValueListener {

   public:

      LIBAPI virtual void percentOpacityUpdated(HlaBurstPointObjectPtr burstPointObject, unsigned int percentOpacity, bool validOldPercentOpacity, unsigned int oldPercentOpacity, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void cylinderSizeUpdated(HlaBurstPointObjectPtr burstPointObject, unsigned int cylinderSize, bool validOldCylinderSize, unsigned int oldCylinderSize, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void cylinderHeightUpdated(HlaBurstPointObjectPtr burstPointObject, unsigned int cylinderHeight, bool validOldCylinderHeight, unsigned int oldCylinderHeight, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void numberOfBurstsUpdated(HlaBurstPointObjectPtr burstPointObject, unsigned int numberOfBursts, bool validOldNumberOfBursts, unsigned int oldNumberOfBursts, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void chemicalContentUpdated(HlaBurstPointObjectPtr burstPointObject, ChemicalContentEnum::ChemicalContentEnum chemicalContent, bool validOldChemicalContent, ChemicalContentEnum::ChemicalContentEnum oldChemicalContent, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void locationUpdated(HlaBurstPointObjectPtr burstPointObject, WorldLocationStruct location, bool validOldLocation, WorldLocationStruct oldLocation, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void orientationUpdated(HlaBurstPointObjectPtr burstPointObject, OrientationStruct orientation, bool validOldOrientation, OrientationStruct oldOrientation, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void percentCompleteUpdated(HlaBurstPointObjectPtr burstPointObject, unsigned int percentComplete, bool validOldPercentComplete, unsigned int oldPercentComplete, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void damagedAppearanceUpdated(HlaBurstPointObjectPtr burstPointObject, DamageStatusEnum::DamageStatusEnum damagedAppearance, bool validOldDamagedAppearance, DamageStatusEnum::DamageStatusEnum oldDamagedAppearance, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void objectPreDistributedUpdated(HlaBurstPointObjectPtr burstPointObject, bool objectPreDistributed, bool validOldObjectPreDistributed, bool oldObjectPreDistributed, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void deactivatedUpdated(HlaBurstPointObjectPtr burstPointObject, bool deactivated, bool validOldDeactivated, bool oldDeactivated, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void smokingUpdated(HlaBurstPointObjectPtr burstPointObject, bool smoking, bool validOldSmoking, bool oldSmoking, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void flamingUpdated(HlaBurstPointObjectPtr burstPointObject, bool flaming, bool validOldFlaming, bool oldFlaming, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void objectIdentifierUpdated(HlaBurstPointObjectPtr burstPointObject, EntityIdentifierStruct objectIdentifier, bool validOldObjectIdentifier, EntityIdentifierStruct oldObjectIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void referencedObjectIdentifierUpdated(HlaBurstPointObjectPtr burstPointObject, std::string referencedObjectIdentifier, bool validOldReferencedObjectIdentifier, std::string oldReferencedObjectIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void forceIdentifierUpdated(HlaBurstPointObjectPtr burstPointObject, ForceIdentifierEnum::ForceIdentifierEnum forceIdentifier, bool validOldForceIdentifier, ForceIdentifierEnum::ForceIdentifierEnum oldForceIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void objectTypeUpdated(HlaBurstPointObjectPtr burstPointObject, EnvironmentObjectTypeStruct objectType, bool validOldObjectType, EnvironmentObjectTypeStruct oldObjectType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
      LIBAPI virtual void producingFederateUpdated(HlaBurstPointObjectPtr burstPointObject, HlaBurstPointObjectAttributes::Attribute attribute, HlaFederateIdPtr oldProducingFederate, HlaFederateIdPtr newProducingFederate, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
   };

}
#endif
