/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAMINEFIELDDATAVALUELISTENER_H
#define DEVELOPER_STUDIO_HLAMINEFIELDDATAVALUELISTENER_H

#include <memory>

#include <DevStudio/datatypes/ClockTimeStruct.h>
#include <DevStudio/datatypes/ClockTimeStructLengthlessArray.h>
#include <DevStudio/datatypes/DepthMeterFloat32LengthlessArray.h>
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EntityTypeStruct.h>
#include <DevStudio/datatypes/MineDielectricDifferenceLengthlessArray.h>
#include <DevStudio/datatypes/MineFusingStruct.h>
#include <DevStudio/datatypes/MineFusingStructLengthlessArray.h>
#include <DevStudio/datatypes/MineIdentifierLengthlessArray.h>
#include <DevStudio/datatypes/MinefieldPaintSchemeEnum.h>
#include <DevStudio/datatypes/MinefieldPaintSchemeLengthlessArray.h>
#include <DevStudio/datatypes/MinefieldSensorTypeEnum.h>
#include <DevStudio/datatypes/MinefieldSensorTypeLengthlessArray.h>
#include <DevStudio/datatypes/OrientationStruct.h>
#include <DevStudio/datatypes/OrientationStructLengthlessArray.h>
#include <DevStudio/datatypes/RelativePositionStruct.h>
#include <DevStudio/datatypes/TemperatureDegreeCelsiusFloat32LengthlessArray.h>
#include <DevStudio/datatypes/UnsignedInteger8LengthlessArray.h>
#include <DevStudio/datatypes/WorldLocationStruct.h>
#include <DevStudio/datatypes/WorldLocationStructLengthlessArray.h>
#include <string>
#include <vector>

#include <DevStudio/HlaLogicalTime.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaMinefieldDataAttributes.h>    

namespace DevStudio {

   /**
   * Listener for updates of attributes, with the new updated values.  
   */
   class HlaMinefieldDataValueListener {

   public:

      LIBAPI virtual ~HlaMinefieldDataValueListener() {}
    
      /**
      * This method is called when the attribute <code>groundBurialDepthOffset</code> is updated.
      * <br>Description from the FOM: <i>Specifies the offset of the origin of the mine coordinate system with respect to the ground surface</i>
      * <br>Description of the data type from the FOM: <i>Specifies ground - snow - water burial depth offset for each mine in a collection of mines</i>
      *
      * @param minefieldData The object which is updated.
      * @param groundBurialDepthOffset The new value of the attribute in this update
      * @param validOldGroundBurialDepthOffset True if oldGroundBurialDepthOffset contains a valid value
      * @param oldGroundBurialDepthOffset The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void groundBurialDepthOffsetUpdated(HlaMinefieldDataPtr minefieldData, std::vector<float > groundBurialDepthOffset, bool validOldGroundBurialDepthOffset, std::vector<float > oldGroundBurialDepthOffset, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>fusing</code> is updated.
      * <br>Description from the FOM: <i>Specifies the primary and secondary fuse and anti-handling device for each mine in a collection of mines</i>
      * <br>Description of the data type from the FOM: <i>Specifies the type of primary fuse, the type of the secondary fuse and the anti-handling device status for a collection of mines</i>
      *
      * @param minefieldData The object which is updated.
      * @param fusing The new value of the attribute in this update
      * @param validOldFusing True if oldFusing contains a valid value
      * @param oldFusing The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void fusingUpdated(HlaMinefieldDataPtr minefieldData, std::vector<DevStudio::MineFusingStruct > fusing, bool validOldFusing, std::vector<DevStudio::MineFusingStruct > oldFusing, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>mineEmplacementTime</code> is updated.
      * <br>Description from the FOM: <i>Specifies the real-world (UTC) emplacement time of each mine in a collection of mines</i>
      * <br>Description of the data type from the FOM: <i>Dynamic array of ClockTimeStruct elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
      *
      * @param minefieldData The object which is updated.
      * @param mineEmplacementTime The new value of the attribute in this update
      * @param validOldMineEmplacementTime True if oldMineEmplacementTime contains a valid value
      * @param oldMineEmplacementTime The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void mineEmplacementTimeUpdated(HlaMinefieldDataPtr minefieldData, std::vector<DevStudio::ClockTimeStruct > mineEmplacementTime, bool validOldMineEmplacementTime, std::vector<DevStudio::ClockTimeStruct > oldMineEmplacementTime, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>mineEntityIdentifier</code> is updated.
      * <br>Description from the FOM: <i>Identifies the mine entity identifier; the MineEntityID in conjunction with the MinefieldID form the unique identifier for each mine</i>
      * <br>Description of the data type from the FOM: <i>Identifies the mine entity identifier for each mine in a collection of mines</i>
      *
      * @param minefieldData The object which is updated.
      * @param mineEntityIdentifier The new value of the attribute in this update
      * @param validOldMineEntityIdentifier True if oldMineEntityIdentifier contains a valid value
      * @param oldMineEntityIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void mineEntityIdentifierUpdated(HlaMinefieldDataPtr minefieldData, std::vector<unsigned short > mineEntityIdentifier, bool validOldMineEntityIdentifier, std::vector<unsigned short > oldMineEntityIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>minefieldIdentifier</code> is updated.
      * <br>Description from the FOM: <i>Identifies the minefield to which the mines belong</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @param minefieldData The object which is updated.
      * @param minefieldIdentifier The new value of the attribute in this update
      * @param validOldMinefieldIdentifier True if oldMinefieldIdentifier contains a valid value
      * @param oldMinefieldIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void minefieldIdentifierUpdated(HlaMinefieldDataPtr minefieldData, std::string minefieldIdentifier, bool validOldMinefieldIdentifier, std::string oldMinefieldIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>mineLocation</code> is updated.
      * <br>Description from the FOM: <i>Specifies the location of the mine relative to the minefield location for each mine in a collection of mines</i>
      * <br>Description of the data type from the FOM: <i>Dynamic array of WorldLocationStruct elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
      *
      * @param minefieldData The object which is updated.
      * @param mineLocation The new value of the attribute in this update
      * @param validOldMineLocation True if oldMineLocation contains a valid value
      * @param oldMineLocation The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void mineLocationUpdated(HlaMinefieldDataPtr minefieldData, std::vector<DevStudio::WorldLocationStruct > mineLocation, bool validOldMineLocation, std::vector<DevStudio::WorldLocationStruct > oldMineLocation, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>mineOrientation</code> is updated.
      * <br>Description from the FOM: <i>Specifies the orientation of the center axis direction of fire of the mine, relative to the minefield Coordinate System for each mine in a collection of mines</i>
      * <br>Description of the data type from the FOM: <i>Dynamic array of OrientationStruct elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
      *
      * @param minefieldData The object which is updated.
      * @param mineOrientation The new value of the attribute in this update
      * @param validOldMineOrientation True if oldMineOrientation contains a valid value
      * @param oldMineOrientation The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void mineOrientationUpdated(HlaMinefieldDataPtr minefieldData, std::vector<DevStudio::OrientationStruct > mineOrientation, bool validOldMineOrientation, std::vector<DevStudio::OrientationStruct > oldMineOrientation, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>mineType</code> is updated.
      * <br>Description from the FOM: <i>Specifies the type of mine for the collection of mines contained within the MinefieldData object</i>
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      *
      * @param minefieldData The object which is updated.
      * @param mineType The new value of the attribute in this update
      * @param validOldMineType True if oldMineType contains a valid value
      * @param oldMineType The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void mineTypeUpdated(HlaMinefieldDataPtr minefieldData, EntityTypeStruct mineType, bool validOldMineType, EntityTypeStruct oldMineType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>numberTripDetonationWires</code> is updated.
      * <br>Description from the FOM: <i>Specifies the number of trip detonation wires that exist for each mine in a collection of mines</i>
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of UnsignedInteger8 elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
      *
      * @param minefieldData The object which is updated.
      * @param numberTripDetonationWires The new value of the attribute in this update
      * @param validOldNumberTripDetonationWires True if oldNumberTripDetonationWires contains a valid value
      * @param oldNumberTripDetonationWires The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void numberTripDetonationWiresUpdated(HlaMinefieldDataPtr minefieldData, std::vector<char > numberTripDetonationWires, bool validOldNumberTripDetonationWires, std::vector<char > oldNumberTripDetonationWires, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>numberWireVertices</code> is updated.
      * <br>Description from the FOM: <i>Specifies the number of vertices for each trip wire of each mine in a collection of mines</i>
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of UnsignedInteger8 elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
      *
      * @param minefieldData The object which is updated.
      * @param numberWireVertices The new value of the attribute in this update
      * @param validOldNumberWireVertices True if oldNumberWireVertices contains a valid value
      * @param oldNumberWireVertices The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void numberWireVerticesUpdated(HlaMinefieldDataPtr minefieldData, std::vector<char > numberWireVertices, bool validOldNumberWireVertices, std::vector<char > oldNumberWireVertices, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>paintScheme</code> is updated.
      * <br>Description from the FOM: <i>Specifies the camouflage scheme/color of the mine</i>
      * <br>Description of the data type from the FOM: <i>Specifies the camouflage scheme/color for each mine in a collection of mines</i>
      *
      * @param minefieldData The object which is updated.
      * @param paintScheme The new value of the attribute in this update
      * @param validOldPaintScheme True if oldPaintScheme contains a valid value
      * @param oldPaintScheme The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void paintSchemeUpdated(HlaMinefieldDataPtr minefieldData, std::vector<DevStudio::MinefieldPaintSchemeEnum::MinefieldPaintSchemeEnum > paintScheme, bool validOldPaintScheme, std::vector<DevStudio::MinefieldPaintSchemeEnum::MinefieldPaintSchemeEnum > oldPaintScheme, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>reflectance</code> is updated.
      * <br>Description from the FOM: <i>Specifies the local dielectric difference between the mine and the surrounding soil</i>
      * <br>Description of the data type from the FOM: <i>Specifies local dielectric difference between the mine and the surrounding soil (reflectance) for each mine in a collection of mines</i>
      *
      * @param minefieldData The object which is updated.
      * @param reflectance The new value of the attribute in this update
      * @param validOldReflectance True if oldReflectance contains a valid value
      * @param oldReflectance The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void reflectanceUpdated(HlaMinefieldDataPtr minefieldData, std::vector<float > reflectance, bool validOldReflectance, std::vector<float > oldReflectance, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>scalarDetectionCoefficient</code> is updated.
      * <br>Description from the FOM: <i>Specifies the coefficient to be utilized to insure proper correlation between detectors located on different simulation platforms</i>
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of UnsignedInteger8 elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
      *
      * @param minefieldData The object which is updated.
      * @param scalarDetectionCoefficient The new value of the attribute in this update
      * @param validOldScalarDetectionCoefficient True if oldScalarDetectionCoefficient contains a valid value
      * @param oldScalarDetectionCoefficient The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void scalarDetectionCoefficientUpdated(HlaMinefieldDataPtr minefieldData, std::vector<char > scalarDetectionCoefficient, bool validOldScalarDetectionCoefficient, std::vector<char > oldScalarDetectionCoefficient, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>sensorTypes</code> is updated.
      * <br>Description from the FOM: <i>In QRP mode, specifies the requesting sensor types which were specified in the minefield query whereas in Heartbeat mode, specifies the sensor types that are being served by the minefield</i>
      * <br>Description of the data type from the FOM: <i>Specifies a collection of minefield sensor types</i>
      *
      * @param minefieldData The object which is updated.
      * @param sensorTypes The new value of the attribute in this update
      * @param validOldSensorTypes True if oldSensorTypes contains a valid value
      * @param oldSensorTypes The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void sensorTypesUpdated(HlaMinefieldDataPtr minefieldData, std::vector<DevStudio::MinefieldSensorTypeEnum::MinefieldSensorTypeEnum > sensorTypes, bool validOldSensorTypes, std::vector<DevStudio::MinefieldSensorTypeEnum::MinefieldSensorTypeEnum > oldSensorTypes, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>snowBurialDepthOffset</code> is updated.
      * <br>Description from the FOM: <i>Specifies the offset of the origin of the mine coordinate system with respect to the snow surface</i>
      * <br>Description of the data type from the FOM: <i>Specifies ground - snow - water burial depth offset for each mine in a collection of mines</i>
      *
      * @param minefieldData The object which is updated.
      * @param snowBurialDepthOffset The new value of the attribute in this update
      * @param validOldSnowBurialDepthOffset True if oldSnowBurialDepthOffset contains a valid value
      * @param oldSnowBurialDepthOffset The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void snowBurialDepthOffsetUpdated(HlaMinefieldDataPtr minefieldData, std::vector<float > snowBurialDepthOffset, bool validOldSnowBurialDepthOffset, std::vector<float > oldSnowBurialDepthOffset, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>thermalContrast</code> is updated.
      * <br>Description from the FOM: <i>Specifies the temperature difference between the mine and the surround soil in degrees Centigrade</i>
      * <br>Description of the data type from the FOM: <i>Specifies thermal contrast for each mine in a collection of mines</i>
      *
      * @param minefieldData The object which is updated.
      * @param thermalContrast The new value of the attribute in this update
      * @param validOldThermalContrast True if oldThermalContrast contains a valid value
      * @param oldThermalContrast The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void thermalContrastUpdated(HlaMinefieldDataPtr minefieldData, std::vector<float > thermalContrast, bool validOldThermalContrast, std::vector<float > oldThermalContrast, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>waterBurialDepthOffset</code> is updated.
      * <br>Description from the FOM: <i>Specifies the offset of the origin of the mine coordinate system with respect to the water surface</i>
      * <br>Description of the data type from the FOM: <i>Specifies ground - snow - water burial depth offset for each mine in a collection of mines</i>
      *
      * @param minefieldData The object which is updated.
      * @param waterBurialDepthOffset The new value of the attribute in this update
      * @param validOldWaterBurialDepthOffset True if oldWaterBurialDepthOffset contains a valid value
      * @param oldWaterBurialDepthOffset The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void waterBurialDepthOffsetUpdated(HlaMinefieldDataPtr minefieldData, std::vector<float > waterBurialDepthOffset, bool validOldWaterBurialDepthOffset, std::vector<float > oldWaterBurialDepthOffset, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>wireVertices</code> is updated.
      * <br>Description from the FOM: <i>Specifies the locations of vertices in a trip wire</i>
      * <br>Description of the data type from the FOM: <i>Dynamic array of WorldLocationStruct elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
      *
      * @param minefieldData The object which is updated.
      * @param wireVertices The new value of the attribute in this update
      * @param validOldWireVertices True if oldWireVertices contains a valid value
      * @param oldWireVertices The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void wireVerticesUpdated(HlaMinefieldDataPtr minefieldData, std::vector<DevStudio::WorldLocationStruct > wireVertices, bool validOldWireVertices, std::vector<DevStudio::WorldLocationStruct > oldWireVertices, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>entityIdentifier</code> is updated.
      * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @param minefieldData The object which is updated.
      * @param entityIdentifier The new value of the attribute in this update
      * @param validOldEntityIdentifier True if oldEntityIdentifier contains a valid value
      * @param oldEntityIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void entityIdentifierUpdated(HlaMinefieldDataPtr minefieldData, EntityIdentifierStruct entityIdentifier, bool validOldEntityIdentifier, EntityIdentifierStruct oldEntityIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>hostObjectIdentifier</code> is updated.
      * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @param minefieldData The object which is updated.
      * @param hostObjectIdentifier The new value of the attribute in this update
      * @param validOldHostObjectIdentifier True if oldHostObjectIdentifier contains a valid value
      * @param oldHostObjectIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void hostObjectIdentifierUpdated(HlaMinefieldDataPtr minefieldData, std::string hostObjectIdentifier, bool validOldHostObjectIdentifier, std::string oldHostObjectIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>relativePosition</code> is updated.
      * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
      * <br>Description of the data type from the FOM: <i>Relative position in right-handed Cartesian coordinates.</i>
      *
      * @param minefieldData The object which is updated.
      * @param relativePosition The new value of the attribute in this update
      * @param validOldRelativePosition True if oldRelativePosition contains a valid value
      * @param oldRelativePosition The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void relativePositionUpdated(HlaMinefieldDataPtr minefieldData, RelativePositionStruct relativePosition, bool validOldRelativePosition, RelativePositionStruct oldRelativePosition, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
      /**
      * This method is called when the producing federate of an attribute is changed.
      * Only available when using HLA Evolved.
      *
      * @param minefieldData The object which is updated.
      * @param attribute The attribute that now has a new producing federate
      * @param oldProducingFederate The federate handle of the old producing federate
      * @param newProducingFederate The federate handle of the new producing federate
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void producingFederateUpdated(HlaMinefieldDataPtr minefieldData, HlaMinefieldDataAttributes::Attribute attribute, HlaFederateIdPtr oldProducingFederate, HlaFederateIdPtr newProducingFederate, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;

      class Adapter;
   };

   /**
   * An adapter class that implements the HlaMinefieldDataValueListener interface with empty methods.
   * It might be used as a base class for a listener.
   */
   class HlaMinefieldDataValueListener::Adapter : public HlaMinefieldDataValueListener {

   public:

      LIBAPI virtual void groundBurialDepthOffsetUpdated(HlaMinefieldDataPtr minefieldData, std::vector<float > groundBurialDepthOffset, bool validOldGroundBurialDepthOffset, std::vector<float > oldGroundBurialDepthOffset, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void fusingUpdated(HlaMinefieldDataPtr minefieldData, std::vector<DevStudio::MineFusingStruct > fusing, bool validOldFusing, std::vector<DevStudio::MineFusingStruct > oldFusing, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void mineEmplacementTimeUpdated(HlaMinefieldDataPtr minefieldData, std::vector<DevStudio::ClockTimeStruct > mineEmplacementTime, bool validOldMineEmplacementTime, std::vector<DevStudio::ClockTimeStruct > oldMineEmplacementTime, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void mineEntityIdentifierUpdated(HlaMinefieldDataPtr minefieldData, std::vector<unsigned short > mineEntityIdentifier, bool validOldMineEntityIdentifier, std::vector<unsigned short > oldMineEntityIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void minefieldIdentifierUpdated(HlaMinefieldDataPtr minefieldData, std::string minefieldIdentifier, bool validOldMinefieldIdentifier, std::string oldMinefieldIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void mineLocationUpdated(HlaMinefieldDataPtr minefieldData, std::vector<DevStudio::WorldLocationStruct > mineLocation, bool validOldMineLocation, std::vector<DevStudio::WorldLocationStruct > oldMineLocation, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void mineOrientationUpdated(HlaMinefieldDataPtr minefieldData, std::vector<DevStudio::OrientationStruct > mineOrientation, bool validOldMineOrientation, std::vector<DevStudio::OrientationStruct > oldMineOrientation, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void mineTypeUpdated(HlaMinefieldDataPtr minefieldData, EntityTypeStruct mineType, bool validOldMineType, EntityTypeStruct oldMineType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void numberTripDetonationWiresUpdated(HlaMinefieldDataPtr minefieldData, std::vector<char > numberTripDetonationWires, bool validOldNumberTripDetonationWires, std::vector<char > oldNumberTripDetonationWires, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void numberWireVerticesUpdated(HlaMinefieldDataPtr minefieldData, std::vector<char > numberWireVertices, bool validOldNumberWireVertices, std::vector<char > oldNumberWireVertices, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void paintSchemeUpdated(HlaMinefieldDataPtr minefieldData, std::vector<DevStudio::MinefieldPaintSchemeEnum::MinefieldPaintSchemeEnum > paintScheme, bool validOldPaintScheme, std::vector<DevStudio::MinefieldPaintSchemeEnum::MinefieldPaintSchemeEnum > oldPaintScheme, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void reflectanceUpdated(HlaMinefieldDataPtr minefieldData, std::vector<float > reflectance, bool validOldReflectance, std::vector<float > oldReflectance, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void scalarDetectionCoefficientUpdated(HlaMinefieldDataPtr minefieldData, std::vector<char > scalarDetectionCoefficient, bool validOldScalarDetectionCoefficient, std::vector<char > oldScalarDetectionCoefficient, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void sensorTypesUpdated(HlaMinefieldDataPtr minefieldData, std::vector<DevStudio::MinefieldSensorTypeEnum::MinefieldSensorTypeEnum > sensorTypes, bool validOldSensorTypes, std::vector<DevStudio::MinefieldSensorTypeEnum::MinefieldSensorTypeEnum > oldSensorTypes, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void snowBurialDepthOffsetUpdated(HlaMinefieldDataPtr minefieldData, std::vector<float > snowBurialDepthOffset, bool validOldSnowBurialDepthOffset, std::vector<float > oldSnowBurialDepthOffset, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void thermalContrastUpdated(HlaMinefieldDataPtr minefieldData, std::vector<float > thermalContrast, bool validOldThermalContrast, std::vector<float > oldThermalContrast, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void waterBurialDepthOffsetUpdated(HlaMinefieldDataPtr minefieldData, std::vector<float > waterBurialDepthOffset, bool validOldWaterBurialDepthOffset, std::vector<float > oldWaterBurialDepthOffset, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void wireVerticesUpdated(HlaMinefieldDataPtr minefieldData, std::vector<DevStudio::WorldLocationStruct > wireVertices, bool validOldWireVertices, std::vector<DevStudio::WorldLocationStruct > oldWireVertices, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void entityIdentifierUpdated(HlaMinefieldDataPtr minefieldData, EntityIdentifierStruct entityIdentifier, bool validOldEntityIdentifier, EntityIdentifierStruct oldEntityIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void hostObjectIdentifierUpdated(HlaMinefieldDataPtr minefieldData, std::string hostObjectIdentifier, bool validOldHostObjectIdentifier, std::string oldHostObjectIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void relativePositionUpdated(HlaMinefieldDataPtr minefieldData, RelativePositionStruct relativePosition, bool validOldRelativePosition, RelativePositionStruct oldRelativePosition, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
      LIBAPI virtual void producingFederateUpdated(HlaMinefieldDataPtr minefieldData, HlaMinefieldDataAttributes::Attribute attribute, HlaFederateIdPtr oldProducingFederate, HlaFederateIdPtr newProducingFederate, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
   };

}
#endif
