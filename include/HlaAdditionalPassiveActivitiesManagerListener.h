/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAADDITIONALPASSIVEACTIVITIESMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLAADDITIONALPASSIVEACTIVITIESMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaAdditionalPassiveActivities.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaAdditionalPassiveActivities instances.
    */
    class HlaAdditionalPassiveActivitiesManagerListener {

    public:

        LIBAPI virtual ~HlaAdditionalPassiveActivitiesManagerListener() {}

        /**
        * This method is called when a new HlaAdditionalPassiveActivities instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param additionalPassiveActivities the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaAdditionalPassiveActivitiesDiscovered(HlaAdditionalPassiveActivitiesPtr additionalPassiveActivities, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaAdditionalPassiveActivities instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param additionalPassiveActivities the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaAdditionalPassiveActivitiesInitialized(HlaAdditionalPassiveActivitiesPtr additionalPassiveActivities, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaAdditionalPassiveActivitiesManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param additionalPassiveActivities the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaAdditionalPassiveActivitiesInInterest(HlaAdditionalPassiveActivitiesPtr additionalPassiveActivities, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaAdditionalPassiveActivitiesManagerListener instance goes out of interest.
        *
        * @param additionalPassiveActivities the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaAdditionalPassiveActivitiesOutOfInterest(HlaAdditionalPassiveActivitiesPtr additionalPassiveActivities, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaAdditionalPassiveActivities instance is deleted.
        *
        * @param additionalPassiveActivities the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaAdditionalPassiveActivitiesDeleted(HlaAdditionalPassiveActivitiesPtr additionalPassiveActivities, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaAdditionalPassiveActivitiesManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaAdditionalPassiveActivitiesManagerListener::Adapter : public HlaAdditionalPassiveActivitiesManagerListener {

    public:
        LIBAPI virtual void hlaAdditionalPassiveActivitiesDiscovered(HlaAdditionalPassiveActivitiesPtr additionalPassiveActivities, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaAdditionalPassiveActivitiesInitialized(HlaAdditionalPassiveActivitiesPtr additionalPassiveActivities, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaAdditionalPassiveActivitiesInInterest(HlaAdditionalPassiveActivitiesPtr additionalPassiveActivities, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaAdditionalPassiveActivitiesOutOfInterest(HlaAdditionalPassiveActivitiesPtr additionalPassiveActivities, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaAdditionalPassiveActivitiesDeleted(HlaAdditionalPassiveActivitiesPtr additionalPassiveActivities, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
