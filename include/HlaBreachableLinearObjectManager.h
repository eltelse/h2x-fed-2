/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLABREACHABLELINEAROBJECTMANAGER_H
#define DEVELOPER_STUDIO_HLABREACHABLELINEAROBJECTMANAGER_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <DevStudio/datatypes/BreachableSegmentStruct.h>
#include <DevStudio/datatypes/BreachableSegmentStructLengthlessArray.h>
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EnvironmentObjectTypeStruct.h>
#include <DevStudio/datatypes/ForceIdentifierEnum.h>
#include <string>
#include <vector>

#include <list>
#include <vector>

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaBreachableLinearObjectManagerListener.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaException.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

   /**
   * Manager used to manage HlaBreachableLinearObjects.
   */
   class HlaBreachableLinearObjectManager : private boost::noncopyable {

   public:

      LIBAPI virtual ~HlaBreachableLinearObjectManager() {}

      /**
      * Gets a list of all HlaBreachableLinearObjects within interest, both local and remote.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of HlaBreachableLinearObjects
      */
      LIBAPI virtual std::list<HlaBreachableLinearObjectPtr> getHlaBreachableLinearObjects() = 0;

      /**
      * Gets a list of all HlaBreachableLinearObjects, both local and remote.
      * HlaBreachableLinearObjects not within interest are also included.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of all HlaBreachableLinearObjects
      */
      LIBAPI virtual std::list<HlaBreachableLinearObjectPtr> getAllHlaBreachableLinearObjects() = 0;

      /**
      * Gets a list of local HlaBreachableLinearObjects within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of local HlaBreachableLinearObjects
      */
      LIBAPI virtual std::list<HlaBreachableLinearObjectPtr> getLocalHlaBreachableLinearObjects() = 0;

      /**
      * Gets a list of remote HlaBreachableLinearObjects within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of remote HlaBreachableLinearObjects
      */
      LIBAPI virtual std::list<HlaBreachableLinearObjectPtr> getRemoteHlaBreachableLinearObjects() = 0;

      /**
      * Find a HlaBreachableLinearObject with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> of the HlaBreachableLinearObject to find
      *
      * @return the specified HlaBreachableLinearObject, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaBreachableLinearObjectPtr getBreachableLinearObjectByHlaInstanceName(const std::wstring& hlaInstanceName) = 0;

      /**
      * Find a HlaBreachableLinearObject with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param encodedHlaInstanceHandle the encoded <code>HLA instance handle</code> of the HlaBreachableLinearObject to find
      *
      * @return the specified HlaBreachableLinearObject, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaBreachableLinearObjectPtr getBreachableLinearObjectByHlaInstanceHandle(const std::vector<char>& encodedHlaInstanceHandle) = 0;

      /**
      * Creates a new HlaBreachableLinearObject, and sets all <i>Create</i> attributes.
      *
      * @return Returns a new local HlaBreachableLinearObject.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaBreachableLinearObjectPtr createLocalHlaBreachableLinearObject(
      ) THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Creates a new HlaBreachableLinearObject with a specified <code>HLA instance name</code>, and sets all
      * <i>Create</i> attributes.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> to register for this instance
      *
      * @return Returns a new local HlaBreachableLinearObject.
      *
      * @throws HlaIllegalInstanceNameException if the name is illegal
      * @throws HlaInstanceNameInUseException if the name is already in use
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaBreachableLinearObjectPtr createLocalHlaBreachableLinearObject(
         const std::wstring& hlaInstanceName
      ) THROW_SPEC (HlaIllegalInstanceNameException, HlaInstanceNameInUseException,
                    HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaBreachableLinearObject and removes it from the federation.
      *
      * @param breachableLinearObject The HlaBreachableLinearObject to delete
      *
      * @return the HlaBreachableLinearObject deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaBreachableLinearObjectPtr deleteLocalHlaBreachableLinearObject(HlaBreachableLinearObjectPtr breachableLinearObject)
          THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                      HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaBreachableLinearObject and removes it from the federation.
      *
      * @param breachableLinearObject The HlaBreachableLinearObject to delete
      * @param timestamp timestamp when the instance was removed.
      *
      * @return the HlaBreachableLinearObject deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaBreachableLinearObjectPtr deleteLocalHlaBreachableLinearObject(HlaBreachableLinearObjectPtr breachableLinearObject, HlaTimeStampPtr timestamp)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaBreachableLinearObject and removes it from the federation.
      *
      * @param breachableLinearObject The HlaBreachableLinearObject to delete
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaBreachableLinearObject deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaBreachableLinearObjectPtr deleteLocalHlaBreachableLinearObject(HlaBreachableLinearObjectPtr breachableLinearObject, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaBreachableLinearObject and removes it from the federation.
      *
      * @param breachableLinearObject The HlaBreachableLinearObject to delete
      * @param timestamp timestamp when the instance was removed.
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaBreachableLinearObject deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaBreachableLinearObjectPtr deleteLocalHlaBreachableLinearObject(HlaBreachableLinearObjectPtr breachableLinearObject, HlaTimeStampPtr timestamp, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Add a HlaBreachableLinearObject manager listener.
      *
      * @param managerListener listener to add
      */
      LIBAPI virtual void addHlaBreachableLinearObjectManagerListener(HlaBreachableLinearObjectManagerListenerPtr managerListener) = 0;

      /**
      * Removed a HlaBreachableLinearObject manager listener.
      *
      * @param managerListener listener to remove
      */
      LIBAPI virtual void removeHlaBreachableLinearObjectManagerListener(HlaBreachableLinearObjectManagerListenerPtr managerListener) = 0;

     /**
      * Add a listener that will automatically be added to all instances of HlaBreachableLinearObject (Both local and remote).
      * The listener is notified when any attribute of an instance of HlaBreachableLinearObject is updated.
      * The listener is also called when an interaction is sent to an instance of HlaBreachableLinearObject.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param listener The listener to add.
      */
      LIBAPI virtual void addHlaBreachableLinearObjectDefaultInstanceListener(HlaBreachableLinearObjectListenerPtr listener) = 0;

     /**
      * Remove a listener from the set of listeners that are automatically added to new instances of HlaBreachableLinearObject.
      * Note: The listener will not be removed from already existing instances of HlaBreachableLinearObject.
      * This method is idempotent.
      *
      * @param listener The listener to remove.
      */
      LIBAPI virtual void removeHlaBreachableLinearObjectDefaultInstanceListener(HlaBreachableLinearObjectListenerPtr listener) = 0;

     /**
      * Add a valueListener that will automatically be added to all instances of HlaBreachableLinearObject (Both local and remote).
      * The  Valuelistener is notified when any attribute of an instance of HlaBreachableLinearObject is updated.
      * Note that the attribute values that are notified are not guaranteed to
      * be the latest available values for that attribute.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to add.
      */
      LIBAPI virtual void addHlaBreachableLinearObjectDefaultInstanceValueListener(HlaBreachableLinearObjectValueListenerPtr valueListener) = 0;

     /**
      * Remove a valueListener from the set of listeners that are automatically added to new instances of HlaBreachableLinearObject.
      * Note: The valueListener will not be removed from already existing instances of HlaBreachableLinearObject.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to remove.
      */
      LIBAPI virtual void removeHlaBreachableLinearObjectDefaultInstanceValueListener(HlaBreachableLinearObjectValueListenerPtr valueListener) = 0;

     /**
      * Enables or disables the HlaBreachableLinearObject manager.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaBreachableLinearObject manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      *
      * @param enabled <code>true</code> to enable the HlaBreachableLinearObject manager, otherwise <code>false</code>
      */
      LIBAPI virtual void setEnabled(bool enabled) = 0;

     /**
      * Enables the HlaBreachableLinearObject manager if it is avalable in the FOM at runtime.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaBreachableLinearObject manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      */
      LIBAPI virtual void setEnabledIfAvailableInFom() = 0;

     /**
      * Check if the HlaBreachableLinearObject manager is actually enabled when connected.
      * An HlaBreachableLinearObject manager may be disabled with the <code>setEnabled(false)</code> function, or it may be configured
      * as <i>if available in the FOM</i> by the <code>setEnabledIfAvailableInFom()</code> function and
      * then not available in the FOM at runtime.
      * <p>
      * This method should only be used when connected, it will always return <code>false</code>
      * when not connected.
      *
      * @return <code>true</code> if connected and the HlaBreachableLinearObject manager is enabled.
      */
      LIBAPI virtual bool isEnabled() = 0;
   };
}
#endif
