/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLARADARBEAMMANAGER_H
#define DEVELOPER_STUDIO_HLARADARBEAMMANAGER_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <DevStudio/datatypes/BeamFunctionCodeEnum.h>
#include <DevStudio/datatypes/EventIdentifierStruct.h>
#include <DevStudio/datatypes/RTIobjectIdArray.h>
#include <string>
#include <vector>

#include <list>
#include <vector>

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaRadarBeamManagerListener.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaException.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

   /**
   * Manager used to manage HlaRadarBeams.
   */
   class HlaRadarBeamManager : private boost::noncopyable {

   public:

      LIBAPI virtual ~HlaRadarBeamManager() {}

      /**
      * Gets a list of all HlaRadarBeams within interest, both local and remote.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of HlaRadarBeams
      */
      LIBAPI virtual std::list<HlaRadarBeamPtr> getHlaRadarBeams() = 0;

      /**
      * Gets a list of all HlaRadarBeams, both local and remote.
      * HlaRadarBeams not within interest are also included.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of all HlaRadarBeams
      */
      LIBAPI virtual std::list<HlaRadarBeamPtr> getAllHlaRadarBeams() = 0;

      /**
      * Gets a list of local HlaRadarBeams within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of local HlaRadarBeams
      */
      LIBAPI virtual std::list<HlaRadarBeamPtr> getLocalHlaRadarBeams() = 0;

      /**
      * Gets a list of remote HlaRadarBeams within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of remote HlaRadarBeams
      */
      LIBAPI virtual std::list<HlaRadarBeamPtr> getRemoteHlaRadarBeams() = 0;

      /**
      * Find a HlaRadarBeam with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> of the HlaRadarBeam to find
      *
      * @return the specified HlaRadarBeam, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaRadarBeamPtr getRadarBeamByHlaInstanceName(const std::wstring& hlaInstanceName) = 0;

      /**
      * Find a HlaRadarBeam with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param encodedHlaInstanceHandle the encoded <code>HLA instance handle</code> of the HlaRadarBeam to find
      *
      * @return the specified HlaRadarBeam, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaRadarBeamPtr getRadarBeamByHlaInstanceHandle(const std::vector<char>& encodedHlaInstanceHandle) = 0;

      /**
      * Creates a new HlaRadarBeam, and sets all <i>Create</i> attributes.
      *
      * @return Returns a new local HlaRadarBeam.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaRadarBeamPtr createLocalHlaRadarBeam(
      ) THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Creates a new HlaRadarBeam with a specified <code>HLA instance name</code>, and sets all
      * <i>Create</i> attributes.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> to register for this instance
      *
      * @return Returns a new local HlaRadarBeam.
      *
      * @throws HlaIllegalInstanceNameException if the name is illegal
      * @throws HlaInstanceNameInUseException if the name is already in use
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaRadarBeamPtr createLocalHlaRadarBeam(
         const std::wstring& hlaInstanceName
      ) THROW_SPEC (HlaIllegalInstanceNameException, HlaInstanceNameInUseException,
                    HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaRadarBeam and removes it from the federation.
      *
      * @param radarBeam The HlaRadarBeam to delete
      *
      * @return the HlaRadarBeam deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaRadarBeamPtr deleteLocalHlaRadarBeam(HlaRadarBeamPtr radarBeam)
          THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                      HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaRadarBeam and removes it from the federation.
      *
      * @param radarBeam The HlaRadarBeam to delete
      * @param timestamp timestamp when the instance was removed.
      *
      * @return the HlaRadarBeam deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaRadarBeamPtr deleteLocalHlaRadarBeam(HlaRadarBeamPtr radarBeam, HlaTimeStampPtr timestamp)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaRadarBeam and removes it from the federation.
      *
      * @param radarBeam The HlaRadarBeam to delete
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaRadarBeam deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaRadarBeamPtr deleteLocalHlaRadarBeam(HlaRadarBeamPtr radarBeam, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaRadarBeam and removes it from the federation.
      *
      * @param radarBeam The HlaRadarBeam to delete
      * @param timestamp timestamp when the instance was removed.
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaRadarBeam deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaRadarBeamPtr deleteLocalHlaRadarBeam(HlaRadarBeamPtr radarBeam, HlaTimeStampPtr timestamp, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Add a HlaRadarBeam manager listener.
      *
      * @param managerListener listener to add
      */
      LIBAPI virtual void addHlaRadarBeamManagerListener(HlaRadarBeamManagerListenerPtr managerListener) = 0;

      /**
      * Removed a HlaRadarBeam manager listener.
      *
      * @param managerListener listener to remove
      */
      LIBAPI virtual void removeHlaRadarBeamManagerListener(HlaRadarBeamManagerListenerPtr managerListener) = 0;

     /**
      * Add a listener that will automatically be added to all instances of HlaRadarBeam (Both local and remote).
      * The listener is notified when any attribute of an instance of HlaRadarBeam is updated.
      * The listener is also called when an interaction is sent to an instance of HlaRadarBeam.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param listener The listener to add.
      */
      LIBAPI virtual void addHlaRadarBeamDefaultInstanceListener(HlaRadarBeamListenerPtr listener) = 0;

     /**
      * Remove a listener from the set of listeners that are automatically added to new instances of HlaRadarBeam.
      * Note: The listener will not be removed from already existing instances of HlaRadarBeam.
      * This method is idempotent.
      *
      * @param listener The listener to remove.
      */
      LIBAPI virtual void removeHlaRadarBeamDefaultInstanceListener(HlaRadarBeamListenerPtr listener) = 0;

     /**
      * Add a valueListener that will automatically be added to all instances of HlaRadarBeam (Both local and remote).
      * The  Valuelistener is notified when any attribute of an instance of HlaRadarBeam is updated.
      * Note that the attribute values that are notified are not guaranteed to
      * be the latest available values for that attribute.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to add.
      */
      LIBAPI virtual void addHlaRadarBeamDefaultInstanceValueListener(HlaRadarBeamValueListenerPtr valueListener) = 0;

     /**
      * Remove a valueListener from the set of listeners that are automatically added to new instances of HlaRadarBeam.
      * Note: The valueListener will not be removed from already existing instances of HlaRadarBeam.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to remove.
      */
      LIBAPI virtual void removeHlaRadarBeamDefaultInstanceValueListener(HlaRadarBeamValueListenerPtr valueListener) = 0;

     /**
      * Enables or disables the HlaRadarBeam manager.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaRadarBeam manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      *
      * @param enabled <code>true</code> to enable the HlaRadarBeam manager, otherwise <code>false</code>
      */
      LIBAPI virtual void setEnabled(bool enabled) = 0;

     /**
      * Enables the HlaRadarBeam manager if it is avalable in the FOM at runtime.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaRadarBeam manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      */
      LIBAPI virtual void setEnabledIfAvailableInFom() = 0;

     /**
      * Check if the HlaRadarBeam manager is actually enabled when connected.
      * An HlaRadarBeam manager may be disabled with the <code>setEnabled(false)</code> function, or it may be configured
      * as <i>if available in the FOM</i> by the <code>setEnabledIfAvailableInFom()</code> function and
      * then not available in the FOM at runtime.
      * <p>
      * This method should only be used when connected, it will always return <code>false</code>
      * when not connected.
      *
      * @return <code>true</code> if connected and the HlaRadarBeam manager is enabled.
      */
      LIBAPI virtual bool isEnabled() = 0;
   };
}
#endif
