/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLASPOTOBJECTVALUELISTENER_H
#define DEVELOPER_STUDIO_HLASPOTOBJECTVALUELISTENER_H

#include <memory>

#include <DevStudio/datatypes/AcquisitionTypeEnum.h>
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EntityTypeStruct.h>
#include <DevStudio/datatypes/FortificationsEnum.h>
#include <DevStudio/datatypes/GeographicShapeStruct.h>
#include <DevStudio/datatypes/HostilityTypeEnum.h>
#include <DevStudio/datatypes/IsPartOfStruct.h>
#include <DevStudio/datatypes/PositionStruct.h>
#include <DevStudio/datatypes/SpatialVariantStruct.h>
#include <DevStudio/datatypes/SpotCategoryEnum.h>
#include <string>

#include <DevStudio/HlaLogicalTime.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaSpotObjectAttributes.h>    

namespace DevStudio {

   /**
   * Listener for updates of attributes, with the new updated values.  
   */
   class HlaSpotObjectValueListener {

   public:

      LIBAPI virtual ~HlaSpotObjectValueListener() {}
    
      /**
      * This method is called when the attribute <code>quantity</code> is updated.
      * <br>Description from the FOM: <i>Quantity of spotted targets.</i>
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      *
      * @param spotObject The object which is updated.
      * @param quantity The new value of the attribute in this update
      * @param validOldQuantity True if oldQuantity contains a valid value
      * @param oldQuantity The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void quantityUpdated(HlaSpotObjectPtr spotObject, short quantity, bool validOldQuantity, short oldQuantity, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>reportingEntity</code> is updated.
      * <br>Description from the FOM: <i>This field is holding a unique identifier for the reporting entity.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @param spotObject The object which is updated.
      * @param reportingEntity The new value of the attribute in this update
      * @param validOldReportingEntity True if oldReportingEntity contains a valid value
      * @param oldReportingEntity The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void reportingEntityUpdated(HlaSpotObjectPtr spotObject, std::string reportingEntity, bool validOldReportingEntity, std::string oldReportingEntity, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>c2ReportingEntity</code> is updated.
      * <br>Description from the FOM: <i>This field is holding a unique c2 identifier for the reporting entity.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @param spotObject The object which is updated.
      * @param c2ReportingEntity The new value of the attribute in this update
      * @param validOldC2ReportingEntity True if oldC2ReportingEntity contains a valid value
      * @param oldC2ReportingEntity The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void c2ReportingEntityUpdated(HlaSpotObjectPtr spotObject, std::string c2ReportingEntity, bool validOldC2ReportingEntity, std::string oldC2ReportingEntity, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>detectionMeans</code> is updated.
      * <br>Description from the FOM: <i>Which kind of detection device spotted the target.</i>
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      *
      * @param spotObject The object which is updated.
      * @param detectionMeans The new value of the attribute in this update
      * @param validOldDetectionMeans True if oldDetectionMeans contains a valid value
      * @param oldDetectionMeans The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void detectionMeansUpdated(HlaSpotObjectPtr spotObject, EntityTypeStruct detectionMeans, bool validOldDetectionMeans, EntityTypeStruct oldDetectionMeans, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>hostilityType</code> is updated.
      * <br>Description from the FOM: <i>Hostility type of spotted target.</i>
      * <br>Description of the data type from the FOM: <i>-NULL-</i>
      *
      * @param spotObject The object which is updated.
      * @param hostilityType The new value of the attribute in this update
      * @param validOldHostilityType True if oldHostilityType contains a valid value
      * @param oldHostilityType The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void hostilityTypeUpdated(HlaSpotObjectPtr spotObject, HostilityTypeEnum::HostilityTypeEnum hostilityType, bool validOldHostilityType, HostilityTypeEnum::HostilityTypeEnum oldHostilityType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>description</code> is updated.
      * <br>Description from the FOM: <i>The description of spotted event.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param spotObject The object which is updated.
      * @param description The new value of the attribute in this update
      * @param validOldDescription True if oldDescription contains a valid value
      * @param oldDescription The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void descriptionUpdated(HlaSpotObjectPtr spotObject, std::wstring description, bool validOldDescription, std::wstring oldDescription, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>lastUpdateTime</code> is updated.
      * <br>Description from the FOM: <i>The last time the spot object was updated.</i>
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      *
      * @param spotObject The object which is updated.
      * @param lastUpdateTime The new value of the attribute in this update
      * @param validOldLastUpdateTime True if oldLastUpdateTime contains a valid value
      * @param oldLastUpdateTime The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void lastUpdateTimeUpdated(HlaSpotObjectPtr spotObject, short lastUpdateTime, bool validOldLastUpdateTime, short oldLastUpdateTime, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>geographicShape</code> is updated.
      * <br>Description from the FOM: <i>Representation of 2D Force geographic shape.</i>
      * <br>Description of the data type from the FOM: <i>-NULL-</i>
      *
      * @param spotObject The object which is updated.
      * @param geographicShape The new value of the attribute in this update
      * @param validOldGeographicShape True if oldGeographicShape contains a valid value
      * @param oldGeographicShape The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void geographicShapeUpdated(HlaSpotObjectPtr spotObject, GeographicShapeStruct geographicShape, bool validOldGeographicShape, GeographicShapeStruct oldGeographicShape, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>spottedEntityIdentifier</code> is updated.
      * <br>Description from the FOM: <i>Entity identifier of spotted entity.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @param spotObject The object which is updated.
      * @param spottedEntityIdentifier The new value of the attribute in this update
      * @param validOldSpottedEntityIdentifier True if oldSpottedEntityIdentifier contains a valid value
      * @param oldSpottedEntityIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void spottedEntityIdentifierUpdated(HlaSpotObjectPtr spotObject, std::string spottedEntityIdentifier, bool validOldSpottedEntityIdentifier, std::string oldSpottedEntityIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>position</code> is updated.
      * <br>Description from the FOM: <i>Position of the spot object.</i>
      * <br>Description of the data type from the FOM: <i>-NULL-</i>
      *
      * @param spotObject The object which is updated.
      * @param position The new value of the attribute in this update
      * @param validOldPosition True if oldPosition contains a valid value
      * @param oldPosition The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void positionUpdated(HlaSpotObjectPtr spotObject, PositionStruct position, bool validOldPosition, PositionStruct oldPosition, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>widthAccuracy</code> is updated.
      * <br>Description from the FOM: <i>Width accuracy of the spot object. Will be filled using meters between 0-999.</i>
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      *
      * @param spotObject The object which is updated.
      * @param widthAccuracy The new value of the attribute in this update
      * @param validOldWidthAccuracy True if oldWidthAccuracy contains a valid value
      * @param oldWidthAccuracy The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void widthAccuracyUpdated(HlaSpotObjectPtr spotObject, float widthAccuracy, bool validOldWidthAccuracy, float oldWidthAccuracy, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>lengthAccuracy</code> is updated.
      * <br>Description from the FOM: <i>Length accuracy of the spot object. Will be filled using meters between 0-999.</i>
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      *
      * @param spotObject The object which is updated.
      * @param lengthAccuracy The new value of the attribute in this update
      * @param validOldLengthAccuracy True if oldLengthAccuracy contains a valid value
      * @param oldLengthAccuracy The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void lengthAccuracyUpdated(HlaSpotObjectPtr spotObject, float lengthAccuracy, bool validOldLengthAccuracy, float oldLengthAccuracy, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>altitudeAccuracy</code> is updated.
      * <br>Description from the FOM: <i>Altitude accuracy of the spot object. Will be filled using meters between 0-100.</i>
      * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
      *
      * @param spotObject The object which is updated.
      * @param altitudeAccuracy The new value of the attribute in this update
      * @param validOldAltitudeAccuracy True if oldAltitudeAccuracy contains a valid value
      * @param oldAltitudeAccuracy The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void altitudeAccuracyUpdated(HlaSpotObjectPtr spotObject, float altitudeAccuracy, bool validOldAltitudeAccuracy, float oldAltitudeAccuracy, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>spotType</code> is updated.
      * <br>Description from the FOM: <i>Spot object Type. Represented as an integer which its value is directing to an external enumeration.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^31, 2^31 - 1]</i>
      *
      * @param spotObject The object which is updated.
      * @param spotType The new value of the attribute in this update
      * @param validOldSpotType True if oldSpotType contains a valid value
      * @param oldSpotType The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void spotTypeUpdated(HlaSpotObjectPtr spotObject, int spotType, bool validOldSpotType, int oldSpotType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>weaponType</code> is updated.
      * <br>Description from the FOM: <i>Defines main weapon type</i>
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      *
      * @param spotObject The object which is updated.
      * @param weaponType The new value of the attribute in this update
      * @param validOldWeaponType True if oldWeaponType contains a valid value
      * @param oldWeaponType The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void weaponTypeUpdated(HlaSpotObjectPtr spotObject, EntityTypeStruct weaponType, bool validOldWeaponType, EntityTypeStruct oldWeaponType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>spotStatus</code> is updated.
      * <br>Description from the FOM: <i>Spot object status (e.g: Alive, Destroyed and etc.). Represented as an integer which its value is directing to an external enumeration.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^31, 2^31 - 1]</i>
      *
      * @param spotObject The object which is updated.
      * @param spotStatus The new value of the attribute in this update
      * @param validOldSpotStatus True if oldSpotStatus contains a valid value
      * @param oldSpotStatus The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void spotStatusUpdated(HlaSpotObjectPtr spotObject, int spotStatus, bool validOldSpotStatus, int oldSpotStatus, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>hostile</code> is updated.
      * <br>Description from the FOM: <i>Hostile (yes/no)</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param spotObject The object which is updated.
      * @param hostile The new value of the attribute in this update
      * @param validOldHostile True if oldHostile contains a valid value
      * @param oldHostile The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void hostileUpdated(HlaSpotObjectPtr spotObject, bool hostile, bool validOldHostile, bool oldHostile, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>infrastructure</code> is updated.
      * <br>Description from the FOM: <i>Spot Infrastructure. Represented as an integer which its value is directing to an external enumeration.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^31, 2^31 - 1]</i>
      *
      * @param spotObject The object which is updated.
      * @param infrastructure The new value of the attribute in this update
      * @param validOldInfrastructure True if oldInfrastructure contains a valid value
      * @param oldInfrastructure The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void infrastructureUpdated(HlaSpotObjectPtr spotObject, int infrastructure, bool validOldInfrastructure, int oldInfrastructure, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>fortifications</code> is updated.
      * <br>Description from the FOM: <i>Defines fortification type .</i>
      * <br>Description of the data type from the FOM: <i>-NULL-</i>
      *
      * @param spotObject The object which is updated.
      * @param fortifications The new value of the attribute in this update
      * @param validOldFortifications True if oldFortifications contains a valid value
      * @param oldFortifications The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void fortificationsUpdated(HlaSpotObjectPtr spotObject, FortificationsEnum::FortificationsEnum fortifications, bool validOldFortifications, FortificationsEnum::FortificationsEnum oldFortifications, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>floors</code> is updated.
      * <br>Description from the FOM: <i>Floor Number. (Max 8 characters).</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param spotObject The object which is updated.
      * @param floors The new value of the attribute in this update
      * @param validOldFloors True if oldFloors contains a valid value
      * @param oldFloors The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void floorsUpdated(HlaSpotObjectPtr spotObject, std::wstring floors, bool validOldFloors, std::wstring oldFloors, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>totalFloors</code> is updated.
      * <br>Description from the FOM: <i>Number of total floors. (Max 8 characters).</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param spotObject The object which is updated.
      * @param totalFloors The new value of the attribute in this update
      * @param validOldTotalFloors True if oldTotalFloors contains a valid value
      * @param oldTotalFloors The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void totalFloorsUpdated(HlaSpotObjectPtr spotObject, std::wstring totalFloors, bool validOldTotalFloors, std::wstring oldTotalFloors, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>acquisitionType</code> is updated.
      * <br>Description from the FOM: <i>Spot object acquisition Type.</i>
      * <br>Description of the data type from the FOM: <i>-NULL-</i>
      *
      * @param spotObject The object which is updated.
      * @param acquisitionType The new value of the attribute in this update
      * @param validOldAcquisitionType True if oldAcquisitionType contains a valid value
      * @param oldAcquisitionType The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void acquisitionTypeUpdated(HlaSpotObjectPtr spotObject, AcquisitionTypeEnum::AcquisitionTypeEnum acquisitionType, bool validOldAcquisitionType, AcquisitionTypeEnum::AcquisitionTypeEnum oldAcquisitionType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>reportingEntityName</code> is updated.
      * <br>Description from the FOM: <i>The name of the reporting unit/entity. (Max 17 characters).</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param spotObject The object which is updated.
      * @param reportingEntityName The new value of the attribute in this update
      * @param validOldReportingEntityName True if oldReportingEntityName contains a valid value
      * @param oldReportingEntityName The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void reportingEntityNameUpdated(HlaSpotObjectPtr spotObject, std::wstring reportingEntityName, bool validOldReportingEntityName, std::wstring oldReportingEntityName, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>armyOrg</code> is updated.
      * <br>Description from the FOM: <i>Defines of which army organization the spotted entity belongs. Represented as an integer which its value is directing to an external enumeration.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^31, 2^31 - 1]</i>
      *
      * @param spotObject The object which is updated.
      * @param armyOrg The new value of the attribute in this update
      * @param validOldArmyOrg True if oldArmyOrg contains a valid value
      * @param oldArmyOrg The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void armyOrgUpdated(HlaSpotObjectPtr spotObject, int armyOrg, bool validOldArmyOrg, int oldArmyOrg, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>spotCategory</code> is updated.
      * <br>Description from the FOM: <i>Spot object category.</i>
      * <br>Description of the data type from the FOM: <i>-NULL-</i>
      *
      * @param spotObject The object which is updated.
      * @param spotCategory The new value of the attribute in this update
      * @param validOldSpotCategory True if oldSpotCategory contains a valid value
      * @param oldSpotCategory The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void spotCategoryUpdated(HlaSpotObjectPtr spotObject, SpotCategoryEnum::SpotCategoryEnum spotCategory, bool validOldSpotCategory, SpotCategoryEnum::SpotCategoryEnum oldSpotCategory, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>casualtiesNumber</code> is updated.
      * <br>Description from the FOM: <i>Number of casualties in the spotted entity.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^31, 2^31 - 1]</i>
      *
      * @param spotObject The object which is updated.
      * @param casualtiesNumber The new value of the attribute in this update
      * @param validOldCasualtiesNumber True if oldCasualtiesNumber contains a valid value
      * @param oldCasualtiesNumber The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void casualtiesNumberUpdated(HlaSpotObjectPtr spotObject, int casualtiesNumber, bool validOldCasualtiesNumber, int oldCasualtiesNumber, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>spottedEntityActivityStatus</code> is updated.
      * <br>Description from the FOM: <i>Spotted entity activity status. Represented as an integer which its value is directing to an external enumeration.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^31, 2^31 - 1]</i>
      *
      * @param spotObject The object which is updated.
      * @param spottedEntityActivityStatus The new value of the attribute in this update
      * @param validOldSpottedEntityActivityStatus True if oldSpottedEntityActivityStatus contains a valid value
      * @param oldSpottedEntityActivityStatus The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void spottedEntityActivityStatusUpdated(HlaSpotObjectPtr spotObject, int spottedEntityActivityStatus, bool validOldSpottedEntityActivityStatus, int oldSpottedEntityActivityStatus, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>entityType</code> is updated.
      * <br>Description from the FOM: <i>The category of the entity.</i>
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      *
      * @param spotObject The object which is updated.
      * @param entityType The new value of the attribute in this update
      * @param validOldEntityType True if oldEntityType contains a valid value
      * @param oldEntityType The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void entityTypeUpdated(HlaSpotObjectPtr spotObject, EntityTypeStruct entityType, bool validOldEntityType, EntityTypeStruct oldEntityType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>entityIdentifier</code> is updated.
      * <br>Description from the FOM: <i>The unique identifier for the entity instance.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @param spotObject The object which is updated.
      * @param entityIdentifier The new value of the attribute in this update
      * @param validOldEntityIdentifier True if oldEntityIdentifier contains a valid value
      * @param oldEntityIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void entityIdentifierUpdated(HlaSpotObjectPtr spotObject, EntityIdentifierStruct entityIdentifier, bool validOldEntityIdentifier, EntityIdentifierStruct oldEntityIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>isPartOf</code> is updated.
      * <br>Description from the FOM: <i>Defines if the entity if a constituent part of another entity (denoted the host entity). If the entity is a constituent part of another entity then the HostEntityIdentifier shall be set to the EntityIdentifier of the host entity and the HostRTIObjectIdentifier shall be set to the RTI object instance ID of the host entity. If the entity is not a constituent part of another entity then the HostEntityIdentifier shall be set to 0.0.0 and the HostRTIObjectIdentifier shall be set to the empty string.</i>
      * <br>Description of the data type from the FOM: <i>Defines the spatial relationship between two objects.</i>
      *
      * @param spotObject The object which is updated.
      * @param isPartOf The new value of the attribute in this update
      * @param validOldIsPartOf True if oldIsPartOf contains a valid value
      * @param oldIsPartOf The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void isPartOfUpdated(HlaSpotObjectPtr spotObject, IsPartOfStruct isPartOf, bool validOldIsPartOf, IsPartOfStruct oldIsPartOf, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>spatial</code> is updated.
      * <br>Description from the FOM: <i>Spatial state stored in one variant record attribute.</i>
      * <br>Description of the data type from the FOM: <i>Variant Record for a single spatial attribute.</i>
      *
      * @param spotObject The object which is updated.
      * @param spatial The new value of the attribute in this update
      * @param validOldSpatial True if oldSpatial contains a valid value
      * @param oldSpatial The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void spatialUpdated(HlaSpotObjectPtr spotObject, SpatialVariantStruct spatial, bool validOldSpatial, SpatialVariantStruct oldSpatial, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>relativeSpatial</code> is updated.
      * <br>Description from the FOM: <i>Relative spatial state stored in one variant record attribute.</i>
      * <br>Description of the data type from the FOM: <i>Variant Record for a single spatial attribute.</i>
      *
      * @param spotObject The object which is updated.
      * @param relativeSpatial The new value of the attribute in this update
      * @param validOldRelativeSpatial True if oldRelativeSpatial contains a valid value
      * @param oldRelativeSpatial The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void relativeSpatialUpdated(HlaSpotObjectPtr spotObject, SpatialVariantStruct relativeSpatial, bool validOldRelativeSpatial, SpatialVariantStruct oldRelativeSpatial, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
      /**
      * This method is called when the producing federate of an attribute is changed.
      * Only available when using HLA Evolved.
      *
      * @param spotObject The object which is updated.
      * @param attribute The attribute that now has a new producing federate
      * @param oldProducingFederate The federate handle of the old producing federate
      * @param newProducingFederate The federate handle of the new producing federate
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void producingFederateUpdated(HlaSpotObjectPtr spotObject, HlaSpotObjectAttributes::Attribute attribute, HlaFederateIdPtr oldProducingFederate, HlaFederateIdPtr newProducingFederate, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;

      class Adapter;
   };

   /**
   * An adapter class that implements the HlaSpotObjectValueListener interface with empty methods.
   * It might be used as a base class for a listener.
   */
   class HlaSpotObjectValueListener::Adapter : public HlaSpotObjectValueListener {

   public:

      LIBAPI virtual void quantityUpdated(HlaSpotObjectPtr spotObject, short quantity, bool validOldQuantity, short oldQuantity, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void reportingEntityUpdated(HlaSpotObjectPtr spotObject, std::string reportingEntity, bool validOldReportingEntity, std::string oldReportingEntity, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void c2ReportingEntityUpdated(HlaSpotObjectPtr spotObject, std::string c2ReportingEntity, bool validOldC2ReportingEntity, std::string oldC2ReportingEntity, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void detectionMeansUpdated(HlaSpotObjectPtr spotObject, EntityTypeStruct detectionMeans, bool validOldDetectionMeans, EntityTypeStruct oldDetectionMeans, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void hostilityTypeUpdated(HlaSpotObjectPtr spotObject, HostilityTypeEnum::HostilityTypeEnum hostilityType, bool validOldHostilityType, HostilityTypeEnum::HostilityTypeEnum oldHostilityType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void descriptionUpdated(HlaSpotObjectPtr spotObject, std::wstring description, bool validOldDescription, std::wstring oldDescription, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void lastUpdateTimeUpdated(HlaSpotObjectPtr spotObject, short lastUpdateTime, bool validOldLastUpdateTime, short oldLastUpdateTime, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void geographicShapeUpdated(HlaSpotObjectPtr spotObject, GeographicShapeStruct geographicShape, bool validOldGeographicShape, GeographicShapeStruct oldGeographicShape, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void spottedEntityIdentifierUpdated(HlaSpotObjectPtr spotObject, std::string spottedEntityIdentifier, bool validOldSpottedEntityIdentifier, std::string oldSpottedEntityIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void positionUpdated(HlaSpotObjectPtr spotObject, PositionStruct position, bool validOldPosition, PositionStruct oldPosition, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void widthAccuracyUpdated(HlaSpotObjectPtr spotObject, float widthAccuracy, bool validOldWidthAccuracy, float oldWidthAccuracy, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void lengthAccuracyUpdated(HlaSpotObjectPtr spotObject, float lengthAccuracy, bool validOldLengthAccuracy, float oldLengthAccuracy, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void altitudeAccuracyUpdated(HlaSpotObjectPtr spotObject, float altitudeAccuracy, bool validOldAltitudeAccuracy, float oldAltitudeAccuracy, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void spotTypeUpdated(HlaSpotObjectPtr spotObject, int spotType, bool validOldSpotType, int oldSpotType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void weaponTypeUpdated(HlaSpotObjectPtr spotObject, EntityTypeStruct weaponType, bool validOldWeaponType, EntityTypeStruct oldWeaponType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void spotStatusUpdated(HlaSpotObjectPtr spotObject, int spotStatus, bool validOldSpotStatus, int oldSpotStatus, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void hostileUpdated(HlaSpotObjectPtr spotObject, bool hostile, bool validOldHostile, bool oldHostile, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void infrastructureUpdated(HlaSpotObjectPtr spotObject, int infrastructure, bool validOldInfrastructure, int oldInfrastructure, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void fortificationsUpdated(HlaSpotObjectPtr spotObject, FortificationsEnum::FortificationsEnum fortifications, bool validOldFortifications, FortificationsEnum::FortificationsEnum oldFortifications, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void floorsUpdated(HlaSpotObjectPtr spotObject, std::wstring floors, bool validOldFloors, std::wstring oldFloors, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void totalFloorsUpdated(HlaSpotObjectPtr spotObject, std::wstring totalFloors, bool validOldTotalFloors, std::wstring oldTotalFloors, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void acquisitionTypeUpdated(HlaSpotObjectPtr spotObject, AcquisitionTypeEnum::AcquisitionTypeEnum acquisitionType, bool validOldAcquisitionType, AcquisitionTypeEnum::AcquisitionTypeEnum oldAcquisitionType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void reportingEntityNameUpdated(HlaSpotObjectPtr spotObject, std::wstring reportingEntityName, bool validOldReportingEntityName, std::wstring oldReportingEntityName, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void armyOrgUpdated(HlaSpotObjectPtr spotObject, int armyOrg, bool validOldArmyOrg, int oldArmyOrg, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void spotCategoryUpdated(HlaSpotObjectPtr spotObject, SpotCategoryEnum::SpotCategoryEnum spotCategory, bool validOldSpotCategory, SpotCategoryEnum::SpotCategoryEnum oldSpotCategory, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void casualtiesNumberUpdated(HlaSpotObjectPtr spotObject, int casualtiesNumber, bool validOldCasualtiesNumber, int oldCasualtiesNumber, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void spottedEntityActivityStatusUpdated(HlaSpotObjectPtr spotObject, int spottedEntityActivityStatus, bool validOldSpottedEntityActivityStatus, int oldSpottedEntityActivityStatus, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void entityTypeUpdated(HlaSpotObjectPtr spotObject, EntityTypeStruct entityType, bool validOldEntityType, EntityTypeStruct oldEntityType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void entityIdentifierUpdated(HlaSpotObjectPtr spotObject, EntityIdentifierStruct entityIdentifier, bool validOldEntityIdentifier, EntityIdentifierStruct oldEntityIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void isPartOfUpdated(HlaSpotObjectPtr spotObject, IsPartOfStruct isPartOf, bool validOldIsPartOf, IsPartOfStruct oldIsPartOf, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void spatialUpdated(HlaSpotObjectPtr spotObject, SpatialVariantStruct spatial, bool validOldSpatial, SpatialVariantStruct oldSpatial, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void relativeSpatialUpdated(HlaSpotObjectPtr spotObject, SpatialVariantStruct relativeSpatial, bool validOldRelativeSpatial, SpatialVariantStruct oldRelativeSpatial, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
      LIBAPI virtual void producingFederateUpdated(HlaSpotObjectPtr spotObject, HlaSpotObjectAttributes::Attribute attribute, HlaFederateIdPtr oldProducingFederate, HlaFederateIdPtr newProducingFederate, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
   };

}
#endif
