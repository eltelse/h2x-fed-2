/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLASTRUCTUREOBJECTMANAGER_H
#define DEVELOPER_STUDIO_HLASTRUCTUREOBJECTMANAGER_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <DevStudio/datatypes/DamageStatusEnum.h>
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EnvironmentObjectTypeStruct.h>
#include <DevStudio/datatypes/ForceIdentifierEnum.h>
#include <DevStudio/datatypes/OrientationStruct.h>
#include <DevStudio/datatypes/WorldLocationStruct.h>
#include <string>

#include <list>
#include <vector>

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaStructureObjectManagerListener.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaException.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

   /**
   * Manager used to manage HlaStructureObjects.
   */
   class HlaStructureObjectManager : private boost::noncopyable {

   public:

      LIBAPI virtual ~HlaStructureObjectManager() {}

      /**
      * Gets a list of all HlaStructureObjects within interest, both local and remote.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of HlaStructureObjects
      */
      LIBAPI virtual std::list<HlaStructureObjectPtr> getHlaStructureObjects() = 0;

      /**
      * Gets a list of all HlaStructureObjects, both local and remote.
      * HlaStructureObjects not within interest are also included.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of all HlaStructureObjects
      */
      LIBAPI virtual std::list<HlaStructureObjectPtr> getAllHlaStructureObjects() = 0;

      /**
      * Gets a list of local HlaStructureObjects within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of local HlaStructureObjects
      */
      LIBAPI virtual std::list<HlaStructureObjectPtr> getLocalHlaStructureObjects() = 0;

      /**
      * Gets a list of remote HlaStructureObjects within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of remote HlaStructureObjects
      */
      LIBAPI virtual std::list<HlaStructureObjectPtr> getRemoteHlaStructureObjects() = 0;

      /**
      * Find a HlaStructureObject with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> of the HlaStructureObject to find
      *
      * @return the specified HlaStructureObject, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaStructureObjectPtr getStructureObjectByHlaInstanceName(const std::wstring& hlaInstanceName) = 0;

      /**
      * Find a HlaStructureObject with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param encodedHlaInstanceHandle the encoded <code>HLA instance handle</code> of the HlaStructureObject to find
      *
      * @return the specified HlaStructureObject, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaStructureObjectPtr getStructureObjectByHlaInstanceHandle(const std::vector<char>& encodedHlaInstanceHandle) = 0;

      /**
      * Creates a new HlaStructureObject, and sets all <i>Create</i> attributes.
      *
      * @return Returns a new local HlaStructureObject.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaStructureObjectPtr createLocalHlaStructureObject(
      ) THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Creates a new HlaStructureObject with a specified <code>HLA instance name</code>, and sets all
      * <i>Create</i> attributes.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> to register for this instance
      *
      * @return Returns a new local HlaStructureObject.
      *
      * @throws HlaIllegalInstanceNameException if the name is illegal
      * @throws HlaInstanceNameInUseException if the name is already in use
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaStructureObjectPtr createLocalHlaStructureObject(
         const std::wstring& hlaInstanceName
      ) THROW_SPEC (HlaIllegalInstanceNameException, HlaInstanceNameInUseException,
                    HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaStructureObject and removes it from the federation.
      *
      * @param structureObject The HlaStructureObject to delete
      *
      * @return the HlaStructureObject deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaStructureObjectPtr deleteLocalHlaStructureObject(HlaStructureObjectPtr structureObject)
          THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                      HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaStructureObject and removes it from the federation.
      *
      * @param structureObject The HlaStructureObject to delete
      * @param timestamp timestamp when the instance was removed.
      *
      * @return the HlaStructureObject deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaStructureObjectPtr deleteLocalHlaStructureObject(HlaStructureObjectPtr structureObject, HlaTimeStampPtr timestamp)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaStructureObject and removes it from the federation.
      *
      * @param structureObject The HlaStructureObject to delete
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaStructureObject deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaStructureObjectPtr deleteLocalHlaStructureObject(HlaStructureObjectPtr structureObject, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaStructureObject and removes it from the federation.
      *
      * @param structureObject The HlaStructureObject to delete
      * @param timestamp timestamp when the instance was removed.
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaStructureObject deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaStructureObjectPtr deleteLocalHlaStructureObject(HlaStructureObjectPtr structureObject, HlaTimeStampPtr timestamp, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Add a HlaStructureObject manager listener.
      *
      * @param managerListener listener to add
      */
      LIBAPI virtual void addHlaStructureObjectManagerListener(HlaStructureObjectManagerListenerPtr managerListener) = 0;

      /**
      * Removed a HlaStructureObject manager listener.
      *
      * @param managerListener listener to remove
      */
      LIBAPI virtual void removeHlaStructureObjectManagerListener(HlaStructureObjectManagerListenerPtr managerListener) = 0;

     /**
      * Add a listener that will automatically be added to all instances of HlaStructureObject (Both local and remote).
      * The listener is notified when any attribute of an instance of HlaStructureObject is updated.
      * The listener is also called when an interaction is sent to an instance of HlaStructureObject.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param listener The listener to add.
      */
      LIBAPI virtual void addHlaStructureObjectDefaultInstanceListener(HlaStructureObjectListenerPtr listener) = 0;

     /**
      * Remove a listener from the set of listeners that are automatically added to new instances of HlaStructureObject.
      * Note: The listener will not be removed from already existing instances of HlaStructureObject.
      * This method is idempotent.
      *
      * @param listener The listener to remove.
      */
      LIBAPI virtual void removeHlaStructureObjectDefaultInstanceListener(HlaStructureObjectListenerPtr listener) = 0;

     /**
      * Add a valueListener that will automatically be added to all instances of HlaStructureObject (Both local and remote).
      * The  Valuelistener is notified when any attribute of an instance of HlaStructureObject is updated.
      * Note that the attribute values that are notified are not guaranteed to
      * be the latest available values for that attribute.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to add.
      */
      LIBAPI virtual void addHlaStructureObjectDefaultInstanceValueListener(HlaStructureObjectValueListenerPtr valueListener) = 0;

     /**
      * Remove a valueListener from the set of listeners that are automatically added to new instances of HlaStructureObject.
      * Note: The valueListener will not be removed from already existing instances of HlaStructureObject.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to remove.
      */
      LIBAPI virtual void removeHlaStructureObjectDefaultInstanceValueListener(HlaStructureObjectValueListenerPtr valueListener) = 0;

     /**
      * Enables or disables the HlaStructureObject manager.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaStructureObject manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      *
      * @param enabled <code>true</code> to enable the HlaStructureObject manager, otherwise <code>false</code>
      */
      LIBAPI virtual void setEnabled(bool enabled) = 0;

     /**
      * Enables the HlaStructureObject manager if it is avalable in the FOM at runtime.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaStructureObject manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      */
      LIBAPI virtual void setEnabledIfAvailableInFom() = 0;

     /**
      * Check if the HlaStructureObject manager is actually enabled when connected.
      * An HlaStructureObject manager may be disabled with the <code>setEnabled(false)</code> function, or it may be configured
      * as <i>if available in the FOM</i> by the <code>setEnabledIfAvailableInFom()</code> function and
      * then not available in the FOM at runtime.
      * <p>
      * This method should only be used when connected, it will always return <code>false</code>
      * when not connected.
      *
      * @return <code>true</code> if connected and the HlaStructureObject manager is enabled.
      */
      LIBAPI virtual bool isEnabled() = 0;
   };
}
#endif
