/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAVISINTSPOTOBJECTMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLAVISINTSPOTOBJECTMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaVisintSpotObject.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaVisintSpotObject instances.
    */
    class HlaVisintSpotObjectManagerListener {

    public:

        LIBAPI virtual ~HlaVisintSpotObjectManagerListener() {}

        /**
        * This method is called when a new HlaVisintSpotObject instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param visintSpotObject the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaVisintSpotObjectDiscovered(HlaVisintSpotObjectPtr visintSpotObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaVisintSpotObject instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param visintSpotObject the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaVisintSpotObjectInitialized(HlaVisintSpotObjectPtr visintSpotObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaVisintSpotObjectManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param visintSpotObject the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaVisintSpotObjectInInterest(HlaVisintSpotObjectPtr visintSpotObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaVisintSpotObjectManagerListener instance goes out of interest.
        *
        * @param visintSpotObject the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaVisintSpotObjectOutOfInterest(HlaVisintSpotObjectPtr visintSpotObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaVisintSpotObject instance is deleted.
        *
        * @param visintSpotObject the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaVisintSpotObjectDeleted(HlaVisintSpotObjectPtr visintSpotObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaVisintSpotObjectManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaVisintSpotObjectManagerListener::Adapter : public HlaVisintSpotObjectManagerListener {

    public:
        LIBAPI virtual void hlaVisintSpotObjectDiscovered(HlaVisintSpotObjectPtr visintSpotObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaVisintSpotObjectInitialized(HlaVisintSpotObjectPtr visintSpotObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaVisintSpotObjectInInterest(HlaVisintSpotObjectPtr visintSpotObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaVisintSpotObjectOutOfInterest(HlaVisintSpotObjectPtr visintSpotObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaVisintSpotObjectDeleted(HlaVisintSpotObjectPtr visintSpotObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
