/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLARRBMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLARRBMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaRRB.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaRRB instances.
    */
    class HlaRRBManagerListener {

    public:

        LIBAPI virtual ~HlaRRBManagerListener() {}

        /**
        * This method is called when a new HlaRRB instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param rRB the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaRRBDiscovered(HlaRRBPtr rRB, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaRRB instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param rRB the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaRRBInitialized(HlaRRBPtr rRB, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaRRBManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param rRB the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaRRBInInterest(HlaRRBPtr rRB, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaRRBManagerListener instance goes out of interest.
        *
        * @param rRB the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaRRBOutOfInterest(HlaRRBPtr rRB, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaRRB instance is deleted.
        *
        * @param rRB the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaRRBDeleted(HlaRRBPtr rRB, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaRRBManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaRRBManagerListener::Adapter : public HlaRRBManagerListener {

    public:
        LIBAPI virtual void hlaRRBDiscovered(HlaRRBPtr rRB, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaRRBInitialized(HlaRRBPtr rRB, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaRRBInInterest(HlaRRBPtr rRB, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaRRBOutOfInterest(HlaRRBPtr rRB, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaRRBDeleted(HlaRRBPtr rRB, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
