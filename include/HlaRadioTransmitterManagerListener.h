/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLARADIOTRANSMITTERMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLARADIOTRANSMITTERMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaRadioTransmitter.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaRadioTransmitter instances.
    */
    class HlaRadioTransmitterManagerListener {

    public:

        LIBAPI virtual ~HlaRadioTransmitterManagerListener() {}

        /**
        * This method is called when a new HlaRadioTransmitter instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param radioTransmitter the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaRadioTransmitterDiscovered(HlaRadioTransmitterPtr radioTransmitter, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaRadioTransmitter instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param radioTransmitter the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaRadioTransmitterInitialized(HlaRadioTransmitterPtr radioTransmitter, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaRadioTransmitterManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param radioTransmitter the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaRadioTransmitterInInterest(HlaRadioTransmitterPtr radioTransmitter, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaRadioTransmitterManagerListener instance goes out of interest.
        *
        * @param radioTransmitter the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaRadioTransmitterOutOfInterest(HlaRadioTransmitterPtr radioTransmitter, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaRadioTransmitter instance is deleted.
        *
        * @param radioTransmitter the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaRadioTransmitterDeleted(HlaRadioTransmitterPtr radioTransmitter, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaRadioTransmitterManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaRadioTransmitterManagerListener::Adapter : public HlaRadioTransmitterManagerListener {

    public:
        LIBAPI virtual void hlaRadioTransmitterDiscovered(HlaRadioTransmitterPtr radioTransmitter, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaRadioTransmitterInitialized(HlaRadioTransmitterPtr radioTransmitter, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaRadioTransmitterInInterest(HlaRadioTransmitterPtr radioTransmitter, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaRadioTransmitterOutOfInterest(HlaRadioTransmitterPtr radioTransmitter, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaRadioTransmitterDeleted(HlaRadioTransmitterPtr radioTransmitter, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
