/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLARADIORECEIVERMANAGER_H
#define DEVELOPER_STUDIO_HLARADIORECEIVERMANAGER_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/ReceiverOperationalStatusEnum.h>
#include <DevStudio/datatypes/RelativePositionStruct.h>
#include <string>

#include <list>
#include <vector>

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaRadioReceiverManagerListener.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaException.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

   /**
   * Manager used to manage HlaRadioReceivers.
   */
   class HlaRadioReceiverManager : private boost::noncopyable {

   public:

      LIBAPI virtual ~HlaRadioReceiverManager() {}

      /**
      * Gets a list of all HlaRadioReceivers within interest, both local and remote.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of HlaRadioReceivers
      */
      LIBAPI virtual std::list<HlaRadioReceiverPtr> getHlaRadioReceivers() = 0;

      /**
      * Gets a list of all HlaRadioReceivers, both local and remote.
      * HlaRadioReceivers not within interest are also included.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of all HlaRadioReceivers
      */
      LIBAPI virtual std::list<HlaRadioReceiverPtr> getAllHlaRadioReceivers() = 0;

      /**
      * Gets a list of local HlaRadioReceivers within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of local HlaRadioReceivers
      */
      LIBAPI virtual std::list<HlaRadioReceiverPtr> getLocalHlaRadioReceivers() = 0;

      /**
      * Gets a list of remote HlaRadioReceivers within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of remote HlaRadioReceivers
      */
      LIBAPI virtual std::list<HlaRadioReceiverPtr> getRemoteHlaRadioReceivers() = 0;

      /**
      * Find a HlaRadioReceiver with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> of the HlaRadioReceiver to find
      *
      * @return the specified HlaRadioReceiver, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaRadioReceiverPtr getRadioReceiverByHlaInstanceName(const std::wstring& hlaInstanceName) = 0;

      /**
      * Find a HlaRadioReceiver with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param encodedHlaInstanceHandle the encoded <code>HLA instance handle</code> of the HlaRadioReceiver to find
      *
      * @return the specified HlaRadioReceiver, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaRadioReceiverPtr getRadioReceiverByHlaInstanceHandle(const std::vector<char>& encodedHlaInstanceHandle) = 0;

      /**
      * Creates a new HlaRadioReceiver, and sets all <i>Create</i> attributes.
      *
      * @return Returns a new local HlaRadioReceiver.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaRadioReceiverPtr createLocalHlaRadioReceiver(
      ) THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Creates a new HlaRadioReceiver with a specified <code>HLA instance name</code>, and sets all
      * <i>Create</i> attributes.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> to register for this instance
      *
      * @return Returns a new local HlaRadioReceiver.
      *
      * @throws HlaIllegalInstanceNameException if the name is illegal
      * @throws HlaInstanceNameInUseException if the name is already in use
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaRadioReceiverPtr createLocalHlaRadioReceiver(
         const std::wstring& hlaInstanceName
      ) THROW_SPEC (HlaIllegalInstanceNameException, HlaInstanceNameInUseException,
                    HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaRadioReceiver and removes it from the federation.
      *
      * @param radioReceiver The HlaRadioReceiver to delete
      *
      * @return the HlaRadioReceiver deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaRadioReceiverPtr deleteLocalHlaRadioReceiver(HlaRadioReceiverPtr radioReceiver)
          THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                      HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaRadioReceiver and removes it from the federation.
      *
      * @param radioReceiver The HlaRadioReceiver to delete
      * @param timestamp timestamp when the instance was removed.
      *
      * @return the HlaRadioReceiver deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaRadioReceiverPtr deleteLocalHlaRadioReceiver(HlaRadioReceiverPtr radioReceiver, HlaTimeStampPtr timestamp)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaRadioReceiver and removes it from the federation.
      *
      * @param radioReceiver The HlaRadioReceiver to delete
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaRadioReceiver deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaRadioReceiverPtr deleteLocalHlaRadioReceiver(HlaRadioReceiverPtr radioReceiver, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaRadioReceiver and removes it from the federation.
      *
      * @param radioReceiver The HlaRadioReceiver to delete
      * @param timestamp timestamp when the instance was removed.
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaRadioReceiver deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaRadioReceiverPtr deleteLocalHlaRadioReceiver(HlaRadioReceiverPtr radioReceiver, HlaTimeStampPtr timestamp, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Add a HlaRadioReceiver manager listener.
      *
      * @param managerListener listener to add
      */
      LIBAPI virtual void addHlaRadioReceiverManagerListener(HlaRadioReceiverManagerListenerPtr managerListener) = 0;

      /**
      * Removed a HlaRadioReceiver manager listener.
      *
      * @param managerListener listener to remove
      */
      LIBAPI virtual void removeHlaRadioReceiverManagerListener(HlaRadioReceiverManagerListenerPtr managerListener) = 0;

     /**
      * Add a listener that will automatically be added to all instances of HlaRadioReceiver (Both local and remote).
      * The listener is notified when any attribute of an instance of HlaRadioReceiver is updated.
      * The listener is also called when an interaction is sent to an instance of HlaRadioReceiver.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param listener The listener to add.
      */
      LIBAPI virtual void addHlaRadioReceiverDefaultInstanceListener(HlaRadioReceiverListenerPtr listener) = 0;

     /**
      * Remove a listener from the set of listeners that are automatically added to new instances of HlaRadioReceiver.
      * Note: The listener will not be removed from already existing instances of HlaRadioReceiver.
      * This method is idempotent.
      *
      * @param listener The listener to remove.
      */
      LIBAPI virtual void removeHlaRadioReceiverDefaultInstanceListener(HlaRadioReceiverListenerPtr listener) = 0;

     /**
      * Add a valueListener that will automatically be added to all instances of HlaRadioReceiver (Both local and remote).
      * The  Valuelistener is notified when any attribute of an instance of HlaRadioReceiver is updated.
      * Note that the attribute values that are notified are not guaranteed to
      * be the latest available values for that attribute.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to add.
      */
      LIBAPI virtual void addHlaRadioReceiverDefaultInstanceValueListener(HlaRadioReceiverValueListenerPtr valueListener) = 0;

     /**
      * Remove a valueListener from the set of listeners that are automatically added to new instances of HlaRadioReceiver.
      * Note: The valueListener will not be removed from already existing instances of HlaRadioReceiver.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to remove.
      */
      LIBAPI virtual void removeHlaRadioReceiverDefaultInstanceValueListener(HlaRadioReceiverValueListenerPtr valueListener) = 0;

     /**
      * Enables or disables the HlaRadioReceiver manager.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaRadioReceiver manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      *
      * @param enabled <code>true</code> to enable the HlaRadioReceiver manager, otherwise <code>false</code>
      */
      LIBAPI virtual void setEnabled(bool enabled) = 0;

     /**
      * Enables the HlaRadioReceiver manager if it is avalable in the FOM at runtime.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaRadioReceiver manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      */
      LIBAPI virtual void setEnabledIfAvailableInFom() = 0;

     /**
      * Check if the HlaRadioReceiver manager is actually enabled when connected.
      * An HlaRadioReceiver manager may be disabled with the <code>setEnabled(false)</code> function, or it may be configured
      * as <i>if available in the FOM</i> by the <code>setEnabledIfAvailableInFom()</code> function and
      * then not available in the FOM at runtime.
      * <p>
      * This method should only be used when connected, it will always return <code>false</code>
      * when not connected.
      *
      * @return <code>true</code> if connected and the HlaRadioReceiver manager is enabled.
      */
      LIBAPI virtual bool isEnabled() = 0;
   };
}
#endif
