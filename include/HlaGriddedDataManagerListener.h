/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAGRIDDEDDATAMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLAGRIDDEDDATAMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaGriddedData.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaGriddedData instances.
    */
    class HlaGriddedDataManagerListener {

    public:

        LIBAPI virtual ~HlaGriddedDataManagerListener() {}

        /**
        * This method is called when a new HlaGriddedData instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param griddedData the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaGriddedDataDiscovered(HlaGriddedDataPtr griddedData, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaGriddedData instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param griddedData the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaGriddedDataInitialized(HlaGriddedDataPtr griddedData, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaGriddedDataManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param griddedData the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaGriddedDataInInterest(HlaGriddedDataPtr griddedData, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaGriddedDataManagerListener instance goes out of interest.
        *
        * @param griddedData the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaGriddedDataOutOfInterest(HlaGriddedDataPtr griddedData, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaGriddedData instance is deleted.
        *
        * @param griddedData the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaGriddedDataDeleted(HlaGriddedDataPtr griddedData, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaGriddedDataManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaGriddedDataManagerListener::Adapter : public HlaGriddedDataManagerListener {

    public:
        LIBAPI virtual void hlaGriddedDataDiscovered(HlaGriddedDataPtr griddedData, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaGriddedDataInitialized(HlaGriddedDataPtr griddedData, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaGriddedDataInInterest(HlaGriddedDataPtr griddedData, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaGriddedDataOutOfInterest(HlaGriddedDataPtr griddedData, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaGriddedDataDeleted(HlaGriddedDataPtr griddedData, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
