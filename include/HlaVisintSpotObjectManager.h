/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAVISINTSPOTOBJECTMANAGER_H
#define DEVELOPER_STUDIO_HLAVISINTSPOTOBJECTMANAGER_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <DevStudio/datatypes/AcquisitionTypeEnum.h>
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EntityTypeStruct.h>
#include <DevStudio/datatypes/FortificationsEnum.h>
#include <DevStudio/datatypes/GeographicShapeStruct.h>
#include <DevStudio/datatypes/HostilityTypeEnum.h>
#include <DevStudio/datatypes/IsPartOfStruct.h>
#include <DevStudio/datatypes/PositionStruct.h>
#include <DevStudio/datatypes/SpatialVariantStruct.h>
#include <DevStudio/datatypes/SpotCategoryEnum.h>
#include <string>

#include <list>
#include <vector>

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaVisintSpotObjectManagerListener.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaException.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

   /**
   * Manager used to manage HlaVisintSpotObjects.
   */
   class HlaVisintSpotObjectManager : private boost::noncopyable {

   public:

      LIBAPI virtual ~HlaVisintSpotObjectManager() {}

      /**
      * Gets a list of all HlaVisintSpotObjects within interest, both local and remote.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of HlaVisintSpotObjects
      */
      LIBAPI virtual std::list<HlaVisintSpotObjectPtr> getHlaVisintSpotObjects() = 0;

      /**
      * Gets a list of all HlaVisintSpotObjects, both local and remote.
      * HlaVisintSpotObjects not within interest are also included.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of all HlaVisintSpotObjects
      */
      LIBAPI virtual std::list<HlaVisintSpotObjectPtr> getAllHlaVisintSpotObjects() = 0;

      /**
      * Gets a list of local HlaVisintSpotObjects within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of local HlaVisintSpotObjects
      */
      LIBAPI virtual std::list<HlaVisintSpotObjectPtr> getLocalHlaVisintSpotObjects() = 0;

      /**
      * Gets a list of remote HlaVisintSpotObjects within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of remote HlaVisintSpotObjects
      */
      LIBAPI virtual std::list<HlaVisintSpotObjectPtr> getRemoteHlaVisintSpotObjects() = 0;

      /**
      * Find a HlaVisintSpotObject with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> of the HlaVisintSpotObject to find
      *
      * @return the specified HlaVisintSpotObject, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaVisintSpotObjectPtr getVisintSpotObjectByHlaInstanceName(const std::wstring& hlaInstanceName) = 0;

      /**
      * Find a HlaVisintSpotObject with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param encodedHlaInstanceHandle the encoded <code>HLA instance handle</code> of the HlaVisintSpotObject to find
      *
      * @return the specified HlaVisintSpotObject, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaVisintSpotObjectPtr getVisintSpotObjectByHlaInstanceHandle(const std::vector<char>& encodedHlaInstanceHandle) = 0;

      /**
      * Creates a new HlaVisintSpotObject, and sets all <i>Create</i> attributes.
      *
      * @return Returns a new local HlaVisintSpotObject.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaVisintSpotObjectPtr createLocalHlaVisintSpotObject(
      ) THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Creates a new HlaVisintSpotObject with a specified <code>HLA instance name</code>, and sets all
      * <i>Create</i> attributes.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> to register for this instance
      *
      * @return Returns a new local HlaVisintSpotObject.
      *
      * @throws HlaIllegalInstanceNameException if the name is illegal
      * @throws HlaInstanceNameInUseException if the name is already in use
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaVisintSpotObjectPtr createLocalHlaVisintSpotObject(
         const std::wstring& hlaInstanceName
      ) THROW_SPEC (HlaIllegalInstanceNameException, HlaInstanceNameInUseException,
                    HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaVisintSpotObject and removes it from the federation.
      *
      * @param visintSpotObject The HlaVisintSpotObject to delete
      *
      * @return the HlaVisintSpotObject deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaVisintSpotObjectPtr deleteLocalHlaVisintSpotObject(HlaVisintSpotObjectPtr visintSpotObject)
          THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                      HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaVisintSpotObject and removes it from the federation.
      *
      * @param visintSpotObject The HlaVisintSpotObject to delete
      * @param timestamp timestamp when the instance was removed.
      *
      * @return the HlaVisintSpotObject deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaVisintSpotObjectPtr deleteLocalHlaVisintSpotObject(HlaVisintSpotObjectPtr visintSpotObject, HlaTimeStampPtr timestamp)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaVisintSpotObject and removes it from the federation.
      *
      * @param visintSpotObject The HlaVisintSpotObject to delete
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaVisintSpotObject deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaVisintSpotObjectPtr deleteLocalHlaVisintSpotObject(HlaVisintSpotObjectPtr visintSpotObject, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaVisintSpotObject and removes it from the federation.
      *
      * @param visintSpotObject The HlaVisintSpotObject to delete
      * @param timestamp timestamp when the instance was removed.
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaVisintSpotObject deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaVisintSpotObjectPtr deleteLocalHlaVisintSpotObject(HlaVisintSpotObjectPtr visintSpotObject, HlaTimeStampPtr timestamp, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Add a HlaVisintSpotObject manager listener.
      *
      * @param managerListener listener to add
      */
      LIBAPI virtual void addHlaVisintSpotObjectManagerListener(HlaVisintSpotObjectManagerListenerPtr managerListener) = 0;

      /**
      * Removed a HlaVisintSpotObject manager listener.
      *
      * @param managerListener listener to remove
      */
      LIBAPI virtual void removeHlaVisintSpotObjectManagerListener(HlaVisintSpotObjectManagerListenerPtr managerListener) = 0;

     /**
      * Add a listener that will automatically be added to all instances of HlaVisintSpotObject (Both local and remote).
      * The listener is notified when any attribute of an instance of HlaVisintSpotObject is updated.
      * The listener is also called when an interaction is sent to an instance of HlaVisintSpotObject.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param listener The listener to add.
      */
      LIBAPI virtual void addHlaVisintSpotObjectDefaultInstanceListener(HlaVisintSpotObjectListenerPtr listener) = 0;

     /**
      * Remove a listener from the set of listeners that are automatically added to new instances of HlaVisintSpotObject.
      * Note: The listener will not be removed from already existing instances of HlaVisintSpotObject.
      * This method is idempotent.
      *
      * @param listener The listener to remove.
      */
      LIBAPI virtual void removeHlaVisintSpotObjectDefaultInstanceListener(HlaVisintSpotObjectListenerPtr listener) = 0;

     /**
      * Add a valueListener that will automatically be added to all instances of HlaVisintSpotObject (Both local and remote).
      * The  Valuelistener is notified when any attribute of an instance of HlaVisintSpotObject is updated.
      * Note that the attribute values that are notified are not guaranteed to
      * be the latest available values for that attribute.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to add.
      */
      LIBAPI virtual void addHlaVisintSpotObjectDefaultInstanceValueListener(HlaVisintSpotObjectValueListenerPtr valueListener) = 0;

     /**
      * Remove a valueListener from the set of listeners that are automatically added to new instances of HlaVisintSpotObject.
      * Note: The valueListener will not be removed from already existing instances of HlaVisintSpotObject.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to remove.
      */
      LIBAPI virtual void removeHlaVisintSpotObjectDefaultInstanceValueListener(HlaVisintSpotObjectValueListenerPtr valueListener) = 0;

     /**
      * Enables or disables the HlaVisintSpotObject manager.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaVisintSpotObject manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      *
      * @param enabled <code>true</code> to enable the HlaVisintSpotObject manager, otherwise <code>false</code>
      */
      LIBAPI virtual void setEnabled(bool enabled) = 0;

     /**
      * Enables the HlaVisintSpotObject manager if it is avalable in the FOM at runtime.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaVisintSpotObject manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      */
      LIBAPI virtual void setEnabledIfAvailableInFom() = 0;

     /**
      * Check if the HlaVisintSpotObject manager is actually enabled when connected.
      * An HlaVisintSpotObject manager may be disabled with the <code>setEnabled(false)</code> function, or it may be configured
      * as <i>if available in the FOM</i> by the <code>setEnabledIfAvailableInFom()</code> function and
      * then not available in the FOM at runtime.
      * <p>
      * This method should only be used when connected, it will always return <code>false</code>
      * when not connected.
      *
      * @return <code>true</code> if connected and the HlaVisintSpotObject manager is enabled.
      */
      LIBAPI virtual bool isEnabled() = 0;
   };
}
#endif
