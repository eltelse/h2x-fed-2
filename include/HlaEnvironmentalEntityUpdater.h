/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAENVIRONMENTALENTITYUPDATER_H
#define DEVELOPER_STUDIO_HLAENVIRONMENTALENTITYUPDATER_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <boost/noncopyable.hpp>

#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EntityTypeStruct.h>
#include <DevStudio/datatypes/IsPartOfStruct.h>
#include <DevStudio/datatypes/OpacityCodeEnum.h>
#include <DevStudio/datatypes/SpatialVariantStruct.h>

#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaException.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaBaseEntityUpdater.h>

namespace DevStudio {

    /**
    * Updater used to update attribute values.
    */
    class HlaEnvironmentalEntityUpdater : public HlaBaseEntityUpdater {

    public:

    LIBAPI virtual ~HlaEnvironmentalEntityUpdater() {}

    /**
    * Set the opacityCode for this update.
    * <br>Description from the FOM: <i>Specifies the density of an environmental entity</i>
    * <br>Description of the data type from the FOM: <i>Density of environmentals</i>
    *
    * @param opacityCode the new opacityCode
    */
    LIBAPI virtual void setOpacityCode(const DevStudio::OpacityCodeEnum::OpacityCodeEnum& opacityCode) = 0;

    /**
    * Set the entityType for this update.
    * <br>Description from the FOM: <i>The category of the entity.</i>
    * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
    *
    * @param entityType the new entityType
    */
    LIBAPI virtual void setEntityType(const DevStudio::EntityTypeStruct& entityType) = 0;

    /**
    * Set the entityIdentifier for this update.
    * <br>Description from the FOM: <i>The unique identifier for the entity instance.</i>
    * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
    *
    * @param entityIdentifier the new entityIdentifier
    */
    LIBAPI virtual void setEntityIdentifier(const DevStudio::EntityIdentifierStruct& entityIdentifier) = 0;

    /**
    * Set the isPartOf for this update.
    * <br>Description from the FOM: <i>Defines if the entity if a constituent part of another entity (denoted the host entity). If the entity is a constituent part of another entity then the HostEntityIdentifier shall be set to the EntityIdentifier of the host entity and the HostRTIObjectIdentifier shall be set to the RTI object instance ID of the host entity. If the entity is not a constituent part of another entity then the HostEntityIdentifier shall be set to 0.0.0 and the HostRTIObjectIdentifier shall be set to the empty string.</i>
    * <br>Description of the data type from the FOM: <i>Defines the spatial relationship between two objects.</i>
    *
    * @param isPartOf the new isPartOf
    */
    LIBAPI virtual void setIsPartOf(const DevStudio::IsPartOfStruct& isPartOf) = 0;

    /**
    * Set the spatial for this update.
    * <br>Description from the FOM: <i>Spatial state stored in one variant record attribute.</i>
    * <br>Description of the data type from the FOM: <i>Variant Record for a single spatial attribute.</i>
    *
    * @param spatial the new spatial
    */
    LIBAPI virtual void setSpatial(const DevStudio::SpatialVariantStruct& spatial) = 0;

    /**
    * Set the relativeSpatial for this update.
    * <br>Description from the FOM: <i>Relative spatial state stored in one variant record attribute.</i>
    * <br>Description of the data type from the FOM: <i>Variant Record for a single spatial attribute.</i>
    *
    * @param relativeSpatial the new relativeSpatial
    */
    LIBAPI virtual void setRelativeSpatial(const DevStudio::SpatialVariantStruct& relativeSpatial) = 0;

    /**
    * Send all the attributes.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate()
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

    /**
    * Send all the attributes with a specified timestamp.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @param timestamp timestamp to send for this update
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate(HlaTimeStampPtr timestamp)
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

    /**
    * Send all the attributes with a specified timestamp.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @param logicalTime logical time to send for this update
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaInvalidLogicalTimeException if the logical time is not valid.
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate(HlaLogicalTimePtr logicalTime)
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaInvalidLogicalTimeException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

    /**
    * Send all the attributes with a specified timestamp.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @param timestamp timestamp to send for this update
    * @param logicalTime logical time to send for this update
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaInvalidLogicalTimeException if the logical time is not valid.
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate(HlaTimeStampPtr timestamp, HlaLogicalTimePtr logicalTime)
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaInvalidLogicalTimeException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;
    };
}
#endif
