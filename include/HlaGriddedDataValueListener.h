/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAGRIDDEDDATAVALUELISTENER_H
#define DEVELOPER_STUDIO_HLAGRIDDEDDATAVALUELISTENER_H

#include <memory>

#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EnvironmentDataCoordinateSystemEnum.h>
#include <DevStudio/datatypes/EnvironmentGridTypeEnum.h>
#include <DevStudio/datatypes/EnvironmentTypeStruct.h>
#include <DevStudio/datatypes/GridAxisStruct.h>
#include <DevStudio/datatypes/GridAxisStructLengthlessArray.h>
#include <DevStudio/datatypes/GridDataStruct.h>
#include <DevStudio/datatypes/GridDataStructLengthlessArray.h>
#include <DevStudio/datatypes/OrientationStruct.h>
#include <vector>

#include <DevStudio/HlaLogicalTime.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaGriddedDataAttributes.h>    

namespace DevStudio {

   /**
   * Listener for updates of attributes, with the new updated values.  
   */
   class HlaGriddedDataValueListener {

   public:

      LIBAPI virtual ~HlaGriddedDataValueListener() {}
    
      /**
      * This method is called when the attribute <code>gridIdentifier</code> is updated.
      * <br>Description from the FOM: <i>Identifies the environmental simulation application</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @param griddedData The object which is updated.
      * @param gridIdentifier The new value of the attribute in this update
      * @param validOldGridIdentifier True if oldGridIdentifier contains a valid value
      * @param oldGridIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void gridIdentifierUpdated(HlaGriddedDataPtr griddedData, EntityIdentifierStruct gridIdentifier, bool validOldGridIdentifier, EntityIdentifierStruct oldGridIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>coordinateSystem</code> is updated.
      * <br>Description from the FOM: <i>Specifies the coordinate system used to locate the data grid</i>
      * <br>Description of the data type from the FOM: <i>Environment data coordinate system</i>
      *
      * @param griddedData The object which is updated.
      * @param coordinateSystem The new value of the attribute in this update
      * @param validOldCoordinateSystem True if oldCoordinateSystem contains a valid value
      * @param oldCoordinateSystem The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void coordinateSystemUpdated(HlaGriddedDataPtr griddedData, EnvironmentDataCoordinateSystemEnum::EnvironmentDataCoordinateSystemEnum coordinateSystem, bool validOldCoordinateSystem, EnvironmentDataCoordinateSystemEnum::EnvironmentDataCoordinateSystemEnum oldCoordinateSystem, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>numberOfGridAxes</code> is updated.
      * <br>Description from the FOM: <i>Specifies the number of grid axes used to define the data grid (e.g. three grid axes for an x, y, z coordinate system)</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param griddedData The object which is updated.
      * @param numberOfGridAxes The new value of the attribute in this update
      * @param validOldNumberOfGridAxes True if oldNumberOfGridAxes contains a valid value
      * @param oldNumberOfGridAxes The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void numberOfGridAxesUpdated(HlaGriddedDataPtr griddedData, char numberOfGridAxes, bool validOldNumberOfGridAxes, char oldNumberOfGridAxes, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>constantGrid</code> is updated.
      * <br>Description from the FOM: <i>Specifies whether the grid axes remain constant for the life of the data grid</i>
      * <br>Description of the data type from the FOM: <i>Environment data grid type</i>
      *
      * @param griddedData The object which is updated.
      * @param constantGrid The new value of the attribute in this update
      * @param validOldConstantGrid True if oldConstantGrid contains a valid value
      * @param oldConstantGrid The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void constantGridUpdated(HlaGriddedDataPtr griddedData, EnvironmentGridTypeEnum::EnvironmentGridTypeEnum constantGrid, bool validOldConstantGrid, EnvironmentGridTypeEnum::EnvironmentGridTypeEnum oldConstantGrid, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>environmentType</code> is updated.
      * <br>Description from the FOM: <i>Identifies the type of environmental entity being described</i>
      * <br>Description of the data type from the FOM: <i>Record specifying the kind of environment, the domain and any extra information necessary for describing the environmental entity</i>
      *
      * @param griddedData The object which is updated.
      * @param environmentType The new value of the attribute in this update
      * @param validOldEnvironmentType True if oldEnvironmentType contains a valid value
      * @param oldEnvironmentType The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void environmentTypeUpdated(HlaGriddedDataPtr griddedData, EnvironmentTypeStruct environmentType, bool validOldEnvironmentType, EnvironmentTypeStruct oldEnvironmentType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>orientation</code> is updated.
      * <br>Description from the FOM: <i>Specifies the orientation of the data grid, with Euler angles</i>
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @param griddedData The object which is updated.
      * @param orientation The new value of the attribute in this update
      * @param validOldOrientation True if oldOrientation contains a valid value
      * @param oldOrientation The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void orientationUpdated(HlaGriddedDataPtr griddedData, OrientationStruct orientation, bool validOldOrientation, OrientationStruct oldOrientation, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>sampleTime</code> is updated.
      * <br>Description from the FOM: <i>Specifies the valid time of the environmental data sample</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^64-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param griddedData The object which is updated.
      * @param sampleTime The new value of the attribute in this update
      * @param validOldSampleTime True if oldSampleTime contains a valid value
      * @param oldSampleTime The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void sampleTimeUpdated(HlaGriddedDataPtr griddedData, unsigned long long sampleTime, bool validOldSampleTime, unsigned long long oldSampleTime, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>totalValues</code> is updated.
      * <br>Description from the FOM: <i>Specifies the number of data values that make up this grid</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param griddedData The object which is updated.
      * @param totalValues The new value of the attribute in this update
      * @param validOldTotalValues True if oldTotalValues contains a valid value
      * @param oldTotalValues The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void totalValuesUpdated(HlaGriddedDataPtr griddedData, unsigned int totalValues, bool validOldTotalValues, unsigned int oldTotalValues, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>vectorDimension</code> is updated.
      * <br>Description from the FOM: <i>Specifies the number of data values at each grid point ; VectorDimension shall be one for scalar data, and shall be greater than one when multiple enumerated environmental data values are sent for each grid point (e.g. u, v, w wind components have VectorDimension = 3)</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param griddedData The object which is updated.
      * @param vectorDimension The new value of the attribute in this update
      * @param validOldVectorDimension True if oldVectorDimension contains a valid value
      * @param oldVectorDimension The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void vectorDimensionUpdated(HlaGriddedDataPtr griddedData, char vectorDimension, bool validOldVectorDimension, char oldVectorDimension, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>gridAxisInfo</code> is updated.
      * <br>Description from the FOM: <i>Specifies information on grid axes</i>
      * <br>Description of the data type from the FOM: <i>Specifies detailed information for a collection of grid axes</i>
      *
      * @param griddedData The object which is updated.
      * @param gridAxisInfo The new value of the attribute in this update
      * @param validOldGridAxisInfo True if oldGridAxisInfo contains a valid value
      * @param oldGridAxisInfo The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void gridAxisInfoUpdated(HlaGriddedDataPtr griddedData, std::vector<DevStudio::GridAxisStruct > gridAxisInfo, bool validOldGridAxisInfo, std::vector<DevStudio::GridAxisStruct > oldGridAxisInfo, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>gridDataInfo</code> is updated.
      * <br>Description from the FOM: <i>Specifies information on grid data representations</i>
      * <br>Description of the data type from the FOM: <i>Specifies detailed information for a collection of grid data representations</i>
      *
      * @param griddedData The object which is updated.
      * @param gridDataInfo The new value of the attribute in this update
      * @param validOldGridDataInfo True if oldGridDataInfo contains a valid value
      * @param oldGridDataInfo The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void gridDataInfoUpdated(HlaGriddedDataPtr griddedData, std::vector<DevStudio::GridDataStruct > gridDataInfo, bool validOldGridDataInfo, std::vector<DevStudio::GridDataStruct > oldGridDataInfo, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
      /**
      * This method is called when the producing federate of an attribute is changed.
      * Only available when using HLA Evolved.
      *
      * @param griddedData The object which is updated.
      * @param attribute The attribute that now has a new producing federate
      * @param oldProducingFederate The federate handle of the old producing federate
      * @param newProducingFederate The federate handle of the new producing federate
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void producingFederateUpdated(HlaGriddedDataPtr griddedData, HlaGriddedDataAttributes::Attribute attribute, HlaFederateIdPtr oldProducingFederate, HlaFederateIdPtr newProducingFederate, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;

      class Adapter;
   };

   /**
   * An adapter class that implements the HlaGriddedDataValueListener interface with empty methods.
   * It might be used as a base class for a listener.
   */
   class HlaGriddedDataValueListener::Adapter : public HlaGriddedDataValueListener {

   public:

      LIBAPI virtual void gridIdentifierUpdated(HlaGriddedDataPtr griddedData, EntityIdentifierStruct gridIdentifier, bool validOldGridIdentifier, EntityIdentifierStruct oldGridIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void coordinateSystemUpdated(HlaGriddedDataPtr griddedData, EnvironmentDataCoordinateSystemEnum::EnvironmentDataCoordinateSystemEnum coordinateSystem, bool validOldCoordinateSystem, EnvironmentDataCoordinateSystemEnum::EnvironmentDataCoordinateSystemEnum oldCoordinateSystem, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void numberOfGridAxesUpdated(HlaGriddedDataPtr griddedData, char numberOfGridAxes, bool validOldNumberOfGridAxes, char oldNumberOfGridAxes, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void constantGridUpdated(HlaGriddedDataPtr griddedData, EnvironmentGridTypeEnum::EnvironmentGridTypeEnum constantGrid, bool validOldConstantGrid, EnvironmentGridTypeEnum::EnvironmentGridTypeEnum oldConstantGrid, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void environmentTypeUpdated(HlaGriddedDataPtr griddedData, EnvironmentTypeStruct environmentType, bool validOldEnvironmentType, EnvironmentTypeStruct oldEnvironmentType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void orientationUpdated(HlaGriddedDataPtr griddedData, OrientationStruct orientation, bool validOldOrientation, OrientationStruct oldOrientation, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void sampleTimeUpdated(HlaGriddedDataPtr griddedData, unsigned long long sampleTime, bool validOldSampleTime, unsigned long long oldSampleTime, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void totalValuesUpdated(HlaGriddedDataPtr griddedData, unsigned int totalValues, bool validOldTotalValues, unsigned int oldTotalValues, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void vectorDimensionUpdated(HlaGriddedDataPtr griddedData, char vectorDimension, bool validOldVectorDimension, char oldVectorDimension, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void gridAxisInfoUpdated(HlaGriddedDataPtr griddedData, std::vector<DevStudio::GridAxisStruct > gridAxisInfo, bool validOldGridAxisInfo, std::vector<DevStudio::GridAxisStruct > oldGridAxisInfo, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void gridDataInfoUpdated(HlaGriddedDataPtr griddedData, std::vector<DevStudio::GridDataStruct > gridDataInfo, bool validOldGridDataInfo, std::vector<DevStudio::GridDataStruct > oldGridDataInfo, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
      LIBAPI virtual void producingFederateUpdated(HlaGriddedDataPtr griddedData, HlaGriddedDataAttributes::Attribute attribute, HlaFederateIdPtr oldProducingFederate, HlaFederateIdPtr newProducingFederate, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
   };

}
#endif
