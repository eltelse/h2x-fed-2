/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAMINEFIELDDATAATTRIBUTES_H
#define DEVELOPER_STUDIO_HLAMINEFIELDDATAATTRIBUTES_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <string>
#include <vector>
#include <utility>
    
#include <boost/noncopyable.hpp>
    
#include <DevStudio/datatypes/ClockTimeStruct.h>
#include <DevStudio/datatypes/ClockTimeStructLengthlessArray.h>
#include <DevStudio/datatypes/DepthMeterFloat32LengthlessArray.h>
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EntityTypeStruct.h>
#include <DevStudio/datatypes/MineDielectricDifferenceLengthlessArray.h>
#include <DevStudio/datatypes/MineFusingStruct.h>
#include <DevStudio/datatypes/MineFusingStructLengthlessArray.h>
#include <DevStudio/datatypes/MineIdentifierLengthlessArray.h>
#include <DevStudio/datatypes/MinefieldPaintSchemeEnum.h>
#include <DevStudio/datatypes/MinefieldPaintSchemeLengthlessArray.h>
#include <DevStudio/datatypes/MinefieldSensorTypeEnum.h>
#include <DevStudio/datatypes/MinefieldSensorTypeLengthlessArray.h>
#include <DevStudio/datatypes/OrientationStruct.h>
#include <DevStudio/datatypes/OrientationStructLengthlessArray.h>
#include <DevStudio/datatypes/RelativePositionStruct.h>
#include <DevStudio/datatypes/TemperatureDegreeCelsiusFloat32LengthlessArray.h>
#include <DevStudio/datatypes/UnsignedInteger8LengthlessArray.h>
#include <DevStudio/datatypes/WorldLocationStruct.h>
#include <DevStudio/datatypes/WorldLocationStructLengthlessArray.h>
#include <string>
#include <vector>

#include <DevStudio/HlaException.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaEmbeddedSystemAttributes.h>
#include <DevStudio/HlaTimeStamped.h>

namespace DevStudio {

   /**
   * Interface used to represent all attributes for an object instance.
   */
   class HlaMinefieldDataAttributes : public HlaEmbeddedSystemAttributes {
   public:


     /**
      * An enumeration of the attributes of an HlaMinefieldData
      *
      *<table summary="All attributes">
      * <tr><td><b>Enum constant</b></td><td><b>C++ name</b></td><td><b>FOM name</b></td></tr>
      * <tr><td>GROUND_BURIAL_DEPTH_OFFSET</td><td>groundBurialDepthOffset</td><td><code>GroundBurialDepthOffset</code></td></tr>
      * <tr><td>FUSING</td><td>fusing</td><td><code>Fusing</code></td></tr>
      * <tr><td>MINE_EMPLACEMENT_TIME</td><td>mineEmplacementTime</td><td><code>MineEmplacementTime</code></td></tr>
      * <tr><td>MINE_ENTITY_IDENTIFIER</td><td>mineEntityIdentifier</td><td><code>MineEntityIdentifier</code></td></tr>
      * <tr><td>MINEFIELD_IDENTIFIER</td><td>minefieldIdentifier</td><td><code>MinefieldIdentifier</code></td></tr>
      * <tr><td>MINE_LOCATION</td><td>mineLocation</td><td><code>MineLocation</code></td></tr>
      * <tr><td>MINE_ORIENTATION</td><td>mineOrientation</td><td><code>MineOrientation</code></td></tr>
      * <tr><td>MINE_TYPE</td><td>mineType</td><td><code>MineType</code></td></tr>
      * <tr><td>NUMBER_TRIP_DETONATION_WIRES</td><td>numberTripDetonationWires</td><td><code>NumberTripDetonationWires</code></td></tr>
      * <tr><td>NUMBER_WIRE_VERTICES</td><td>numberWireVertices</td><td><code>NumberWireVertices</code></td></tr>
      * <tr><td>PAINT_SCHEME</td><td>paintScheme</td><td><code>PaintScheme</code></td></tr>
      * <tr><td>REFLECTANCE</td><td>reflectance</td><td><code>Reflectance</code></td></tr>
      * <tr><td>SCALAR_DETECTION_COEFFICIENT</td><td>scalarDetectionCoefficient</td><td><code>ScalarDetectionCoefficient</code></td></tr>
      * <tr><td>SENSOR_TYPES</td><td>sensorTypes</td><td><code>SensorTypes</code></td></tr>
      * <tr><td>SNOW_BURIAL_DEPTH_OFFSET</td><td>snowBurialDepthOffset</td><td><code>SnowBurialDepthOffset</code></td></tr>
      * <tr><td>THERMAL_CONTRAST</td><td>thermalContrast</td><td><code>ThermalContrast</code></td></tr>
      * <tr><td>WATER_BURIAL_DEPTH_OFFSET</td><td>waterBurialDepthOffset</td><td><code>WaterBurialDepthOffset</code></td></tr>
      * <tr><td>WIRE_VERTICES</td><td>wireVertices</td><td><code>WireVertices</code></td></tr>
      * <tr><td>ENTITY_IDENTIFIER</td><td>entityIdentifier</td><td><code>EntityIdentifier</code></td></tr>
      * <tr><td>HOST_OBJECT_IDENTIFIER</td><td>hostObjectIdentifier</td><td><code>HostObjectIdentifier</code></td></tr>
      * <tr><td>RELATIVE_POSITION</td><td>relativePosition</td><td><code>RelativePosition</code></td></tr>
      *</table>
      */
      enum Attribute {
        /**
        * groundBurialDepthOffset (FOM name: <code>GroundBurialDepthOffset</code>).
        * <br>Description from the FOM: <i>Specifies the offset of the origin of the mine coordinate system with respect to the ground surface</i>
        */
         GROUND_BURIAL_DEPTH_OFFSET,

        /**
        * fusing (FOM name: <code>Fusing</code>).
        * <br>Description from the FOM: <i>Specifies the primary and secondary fuse and anti-handling device for each mine in a collection of mines</i>
        */
         FUSING,

        /**
        * mineEmplacementTime (FOM name: <code>MineEmplacementTime</code>).
        * <br>Description from the FOM: <i>Specifies the real-world (UTC) emplacement time of each mine in a collection of mines</i>
        */
         MINE_EMPLACEMENT_TIME,

        /**
        * mineEntityIdentifier (FOM name: <code>MineEntityIdentifier</code>).
        * <br>Description from the FOM: <i>Identifies the mine entity identifier; the MineEntityID in conjunction with the MinefieldID form the unique identifier for each mine</i>
        */
         MINE_ENTITY_IDENTIFIER,

        /**
        * minefieldIdentifier (FOM name: <code>MinefieldIdentifier</code>).
        * <br>Description from the FOM: <i>Identifies the minefield to which the mines belong</i>
        */
         MINEFIELD_IDENTIFIER,

        /**
        * mineLocation (FOM name: <code>MineLocation</code>).
        * <br>Description from the FOM: <i>Specifies the location of the mine relative to the minefield location for each mine in a collection of mines</i>
        */
         MINE_LOCATION,

        /**
        * mineOrientation (FOM name: <code>MineOrientation</code>).
        * <br>Description from the FOM: <i>Specifies the orientation of the center axis direction of fire of the mine, relative to the minefield Coordinate System for each mine in a collection of mines</i>
        */
         MINE_ORIENTATION,

        /**
        * mineType (FOM name: <code>MineType</code>).
        * <br>Description from the FOM: <i>Specifies the type of mine for the collection of mines contained within the MinefieldData object</i>
        */
         MINE_TYPE,

        /**
        * numberTripDetonationWires (FOM name: <code>NumberTripDetonationWires</code>).
        * <br>Description from the FOM: <i>Specifies the number of trip detonation wires that exist for each mine in a collection of mines</i>
        */
         NUMBER_TRIP_DETONATION_WIRES,

        /**
        * numberWireVertices (FOM name: <code>NumberWireVertices</code>).
        * <br>Description from the FOM: <i>Specifies the number of vertices for each trip wire of each mine in a collection of mines</i>
        */
         NUMBER_WIRE_VERTICES,

        /**
        * paintScheme (FOM name: <code>PaintScheme</code>).
        * <br>Description from the FOM: <i>Specifies the camouflage scheme/color of the mine</i>
        */
         PAINT_SCHEME,

        /**
        * reflectance (FOM name: <code>Reflectance</code>).
        * <br>Description from the FOM: <i>Specifies the local dielectric difference between the mine and the surrounding soil</i>
        */
         REFLECTANCE,

        /**
        * scalarDetectionCoefficient (FOM name: <code>ScalarDetectionCoefficient</code>).
        * <br>Description from the FOM: <i>Specifies the coefficient to be utilized to insure proper correlation between detectors located on different simulation platforms</i>
        */
         SCALAR_DETECTION_COEFFICIENT,

        /**
        * sensorTypes (FOM name: <code>SensorTypes</code>).
        * <br>Description from the FOM: <i>In QRP mode, specifies the requesting sensor types which were specified in the minefield query whereas in Heartbeat mode, specifies the sensor types that are being served by the minefield</i>
        */
         SENSOR_TYPES,

        /**
        * snowBurialDepthOffset (FOM name: <code>SnowBurialDepthOffset</code>).
        * <br>Description from the FOM: <i>Specifies the offset of the origin of the mine coordinate system with respect to the snow surface</i>
        */
         SNOW_BURIAL_DEPTH_OFFSET,

        /**
        * thermalContrast (FOM name: <code>ThermalContrast</code>).
        * <br>Description from the FOM: <i>Specifies the temperature difference between the mine and the surround soil in degrees Centigrade</i>
        */
         THERMAL_CONTRAST,

        /**
        * waterBurialDepthOffset (FOM name: <code>WaterBurialDepthOffset</code>).
        * <br>Description from the FOM: <i>Specifies the offset of the origin of the mine coordinate system with respect to the water surface</i>
        */
         WATER_BURIAL_DEPTH_OFFSET,

        /**
        * wireVertices (FOM name: <code>WireVertices</code>).
        * <br>Description from the FOM: <i>Specifies the locations of vertices in a trip wire</i>
        */
         WIRE_VERTICES,

        /**
        * entityIdentifier (FOM name: <code>EntityIdentifier</code>).
        * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
        */
         ENTITY_IDENTIFIER,

        /**
        * hostObjectIdentifier (FOM name: <code>HostObjectIdentifier</code>).
        * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
        */
         HOST_OBJECT_IDENTIFIER,

        /**
        * relativePosition (FOM name: <code>RelativePosition</code>).
        * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
        */
         RELATIVE_POSITION
      };

     /**
      * Gets the name of the attribute.
      *
      * @return The name of the attribute. An empty string will be returned if the attribute does not exist.
      */
      LIBAPI virtual std::wstring getName(Attribute attribute);

     /**
      * Finds the attribute specified in the parameter attributeName.
      * The found enumeration will be stored in the parameter attribute.
      *
      * @return true if the attribute was found, false otherwise.
      */
      LIBAPI virtual bool find(Attribute& attribute, std::wstring attributeName);

      LIBAPI virtual ~HlaMinefieldDataAttributes() {}
    
      /**
      * Returns true if the <code>groundBurialDepthOffset</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies the offset of the origin of the mine coordinate system with respect to the ground surface</i>
      *
      * @return true if <code>groundBurialDepthOffset</code> is available.
      */
      LIBAPI virtual bool hasGroundBurialDepthOffset() = 0;

      /**
      * Gets the value of the <code>groundBurialDepthOffset</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies the offset of the origin of the mine coordinate system with respect to the ground surface</i>
      * <br>Description of the data type from the FOM: <i>Specifies ground - snow - water burial depth offset for each mine in a collection of mines</i>
      *
      * @return the <code>groundBurialDepthOffset</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::vector<float > getGroundBurialDepthOffset()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>groundBurialDepthOffset</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies the offset of the origin of the mine coordinate system with respect to the ground surface</i>
      * <br>Description of the data type from the FOM: <i>Specifies ground - snow - water burial depth offset for each mine in a collection of mines</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>groundBurialDepthOffset</code> attribute.
      */
      LIBAPI virtual std::vector<float > getGroundBurialDepthOffset(std::vector<float > defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>groundBurialDepthOffset</code> attribute.
      * <br>Description from the FOM: <i>Specifies the offset of the origin of the mine coordinate system with respect to the ground surface</i>
      * <br>Description of the data type from the FOM: <i>Specifies ground - snow - water burial depth offset for each mine in a collection of mines</i>
      *
      * @return the time stamped <code>groundBurialDepthOffset</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::vector<float > > getGroundBurialDepthOffsetTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>fusing</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies the primary and secondary fuse and anti-handling device for each mine in a collection of mines</i>
      *
      * @return true if <code>fusing</code> is available.
      */
      LIBAPI virtual bool hasFusing() = 0;

      /**
      * Gets the value of the <code>fusing</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies the primary and secondary fuse and anti-handling device for each mine in a collection of mines</i>
      * <br>Description of the data type from the FOM: <i>Specifies the type of primary fuse, the type of the secondary fuse and the anti-handling device status for a collection of mines</i>
      *
      * @return the <code>fusing</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::vector<DevStudio::MineFusingStruct > getFusing()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>fusing</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies the primary and secondary fuse and anti-handling device for each mine in a collection of mines</i>
      * <br>Description of the data type from the FOM: <i>Specifies the type of primary fuse, the type of the secondary fuse and the anti-handling device status for a collection of mines</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>fusing</code> attribute.
      */
      LIBAPI virtual std::vector<DevStudio::MineFusingStruct > getFusing(std::vector<DevStudio::MineFusingStruct > defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>fusing</code> attribute.
      * <br>Description from the FOM: <i>Specifies the primary and secondary fuse and anti-handling device for each mine in a collection of mines</i>
      * <br>Description of the data type from the FOM: <i>Specifies the type of primary fuse, the type of the secondary fuse and the anti-handling device status for a collection of mines</i>
      *
      * @return the time stamped <code>fusing</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::vector<DevStudio::MineFusingStruct > > getFusingTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>mineEmplacementTime</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies the real-world (UTC) emplacement time of each mine in a collection of mines</i>
      *
      * @return true if <code>mineEmplacementTime</code> is available.
      */
      LIBAPI virtual bool hasMineEmplacementTime() = 0;

      /**
      * Gets the value of the <code>mineEmplacementTime</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies the real-world (UTC) emplacement time of each mine in a collection of mines</i>
      * <br>Description of the data type from the FOM: <i>Dynamic array of ClockTimeStruct elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
      *
      * @return the <code>mineEmplacementTime</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::vector<DevStudio::ClockTimeStruct > getMineEmplacementTime()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>mineEmplacementTime</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies the real-world (UTC) emplacement time of each mine in a collection of mines</i>
      * <br>Description of the data type from the FOM: <i>Dynamic array of ClockTimeStruct elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>mineEmplacementTime</code> attribute.
      */
      LIBAPI virtual std::vector<DevStudio::ClockTimeStruct > getMineEmplacementTime(std::vector<DevStudio::ClockTimeStruct > defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>mineEmplacementTime</code> attribute.
      * <br>Description from the FOM: <i>Specifies the real-world (UTC) emplacement time of each mine in a collection of mines</i>
      * <br>Description of the data type from the FOM: <i>Dynamic array of ClockTimeStruct elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
      *
      * @return the time stamped <code>mineEmplacementTime</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::vector<DevStudio::ClockTimeStruct > > getMineEmplacementTimeTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>mineEntityIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Identifies the mine entity identifier; the MineEntityID in conjunction with the MinefieldID form the unique identifier for each mine</i>
      *
      * @return true if <code>mineEntityIdentifier</code> is available.
      */
      LIBAPI virtual bool hasMineEntityIdentifier() = 0;

      /**
      * Gets the value of the <code>mineEntityIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>Identifies the mine entity identifier; the MineEntityID in conjunction with the MinefieldID form the unique identifier for each mine</i>
      * <br>Description of the data type from the FOM: <i>Identifies the mine entity identifier for each mine in a collection of mines</i>
      *
      * @return the <code>mineEntityIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::vector<unsigned short > getMineEntityIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>mineEntityIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Identifies the mine entity identifier; the MineEntityID in conjunction with the MinefieldID form the unique identifier for each mine</i>
      * <br>Description of the data type from the FOM: <i>Identifies the mine entity identifier for each mine in a collection of mines</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>mineEntityIdentifier</code> attribute.
      */
      LIBAPI virtual std::vector<unsigned short > getMineEntityIdentifier(std::vector<unsigned short > defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>mineEntityIdentifier</code> attribute.
      * <br>Description from the FOM: <i>Identifies the mine entity identifier; the MineEntityID in conjunction with the MinefieldID form the unique identifier for each mine</i>
      * <br>Description of the data type from the FOM: <i>Identifies the mine entity identifier for each mine in a collection of mines</i>
      *
      * @return the time stamped <code>mineEntityIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::vector<unsigned short > > getMineEntityIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>minefieldIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Identifies the minefield to which the mines belong</i>
      *
      * @return true if <code>minefieldIdentifier</code> is available.
      */
      LIBAPI virtual bool hasMinefieldIdentifier() = 0;

      /**
      * Gets the value of the <code>minefieldIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>Identifies the minefield to which the mines belong</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @return the <code>minefieldIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::string getMinefieldIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>minefieldIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Identifies the minefield to which the mines belong</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>minefieldIdentifier</code> attribute.
      */
      LIBAPI virtual std::string getMinefieldIdentifier(std::string defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>minefieldIdentifier</code> attribute.
      * <br>Description from the FOM: <i>Identifies the minefield to which the mines belong</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @return the time stamped <code>minefieldIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::string > getMinefieldIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>mineLocation</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies the location of the mine relative to the minefield location for each mine in a collection of mines</i>
      *
      * @return true if <code>mineLocation</code> is available.
      */
      LIBAPI virtual bool hasMineLocation() = 0;

      /**
      * Gets the value of the <code>mineLocation</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies the location of the mine relative to the minefield location for each mine in a collection of mines</i>
      * <br>Description of the data type from the FOM: <i>Dynamic array of WorldLocationStruct elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
      *
      * @return the <code>mineLocation</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::vector<DevStudio::WorldLocationStruct > getMineLocation()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>mineLocation</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies the location of the mine relative to the minefield location for each mine in a collection of mines</i>
      * <br>Description of the data type from the FOM: <i>Dynamic array of WorldLocationStruct elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>mineLocation</code> attribute.
      */
      LIBAPI virtual std::vector<DevStudio::WorldLocationStruct > getMineLocation(std::vector<DevStudio::WorldLocationStruct > defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>mineLocation</code> attribute.
      * <br>Description from the FOM: <i>Specifies the location of the mine relative to the minefield location for each mine in a collection of mines</i>
      * <br>Description of the data type from the FOM: <i>Dynamic array of WorldLocationStruct elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
      *
      * @return the time stamped <code>mineLocation</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::vector<DevStudio::WorldLocationStruct > > getMineLocationTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>mineOrientation</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies the orientation of the center axis direction of fire of the mine, relative to the minefield Coordinate System for each mine in a collection of mines</i>
      *
      * @return true if <code>mineOrientation</code> is available.
      */
      LIBAPI virtual bool hasMineOrientation() = 0;

      /**
      * Gets the value of the <code>mineOrientation</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies the orientation of the center axis direction of fire of the mine, relative to the minefield Coordinate System for each mine in a collection of mines</i>
      * <br>Description of the data type from the FOM: <i>Dynamic array of OrientationStruct elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
      *
      * @return the <code>mineOrientation</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::vector<DevStudio::OrientationStruct > getMineOrientation()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>mineOrientation</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies the orientation of the center axis direction of fire of the mine, relative to the minefield Coordinate System for each mine in a collection of mines</i>
      * <br>Description of the data type from the FOM: <i>Dynamic array of OrientationStruct elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>mineOrientation</code> attribute.
      */
      LIBAPI virtual std::vector<DevStudio::OrientationStruct > getMineOrientation(std::vector<DevStudio::OrientationStruct > defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>mineOrientation</code> attribute.
      * <br>Description from the FOM: <i>Specifies the orientation of the center axis direction of fire of the mine, relative to the minefield Coordinate System for each mine in a collection of mines</i>
      * <br>Description of the data type from the FOM: <i>Dynamic array of OrientationStruct elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
      *
      * @return the time stamped <code>mineOrientation</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::vector<DevStudio::OrientationStruct > > getMineOrientationTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>mineType</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies the type of mine for the collection of mines contained within the MinefieldData object</i>
      *
      * @return true if <code>mineType</code> is available.
      */
      LIBAPI virtual bool hasMineType() = 0;

      /**
      * Gets the value of the <code>mineType</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies the type of mine for the collection of mines contained within the MinefieldData object</i>
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      *
      * @return the <code>mineType</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::EntityTypeStruct getMineType()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>mineType</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies the type of mine for the collection of mines contained within the MinefieldData object</i>
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>mineType</code> attribute.
      */
      LIBAPI virtual DevStudio::EntityTypeStruct getMineType(DevStudio::EntityTypeStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>mineType</code> attribute.
      * <br>Description from the FOM: <i>Specifies the type of mine for the collection of mines contained within the MinefieldData object</i>
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      *
      * @return the time stamped <code>mineType</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::EntityTypeStruct > getMineTypeTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>numberTripDetonationWires</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies the number of trip detonation wires that exist for each mine in a collection of mines</i>
      *
      * @return true if <code>numberTripDetonationWires</code> is available.
      */
      LIBAPI virtual bool hasNumberTripDetonationWires() = 0;

      /**
      * Gets the value of the <code>numberTripDetonationWires</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies the number of trip detonation wires that exist for each mine in a collection of mines</i>
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of UnsignedInteger8 elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
      *
      * @return the <code>numberTripDetonationWires</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::vector<char > getNumberTripDetonationWires()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>numberTripDetonationWires</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies the number of trip detonation wires that exist for each mine in a collection of mines</i>
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of UnsignedInteger8 elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>numberTripDetonationWires</code> attribute.
      */
      LIBAPI virtual std::vector<char > getNumberTripDetonationWires(std::vector<char > defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>numberTripDetonationWires</code> attribute.
      * <br>Description from the FOM: <i>Specifies the number of trip detonation wires that exist for each mine in a collection of mines</i>
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of UnsignedInteger8 elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
      *
      * @return the time stamped <code>numberTripDetonationWires</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::vector<char > > getNumberTripDetonationWiresTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>numberWireVertices</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies the number of vertices for each trip wire of each mine in a collection of mines</i>
      *
      * @return true if <code>numberWireVertices</code> is available.
      */
      LIBAPI virtual bool hasNumberWireVertices() = 0;

      /**
      * Gets the value of the <code>numberWireVertices</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies the number of vertices for each trip wire of each mine in a collection of mines</i>
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of UnsignedInteger8 elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
      *
      * @return the <code>numberWireVertices</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::vector<char > getNumberWireVertices()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>numberWireVertices</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies the number of vertices for each trip wire of each mine in a collection of mines</i>
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of UnsignedInteger8 elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>numberWireVertices</code> attribute.
      */
      LIBAPI virtual std::vector<char > getNumberWireVertices(std::vector<char > defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>numberWireVertices</code> attribute.
      * <br>Description from the FOM: <i>Specifies the number of vertices for each trip wire of each mine in a collection of mines</i>
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of UnsignedInteger8 elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
      *
      * @return the time stamped <code>numberWireVertices</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::vector<char > > getNumberWireVerticesTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>paintScheme</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies the camouflage scheme/color of the mine</i>
      *
      * @return true if <code>paintScheme</code> is available.
      */
      LIBAPI virtual bool hasPaintScheme() = 0;

      /**
      * Gets the value of the <code>paintScheme</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies the camouflage scheme/color of the mine</i>
      * <br>Description of the data type from the FOM: <i>Specifies the camouflage scheme/color for each mine in a collection of mines</i>
      *
      * @return the <code>paintScheme</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::vector<DevStudio::MinefieldPaintSchemeEnum::MinefieldPaintSchemeEnum > getPaintScheme()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>paintScheme</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies the camouflage scheme/color of the mine</i>
      * <br>Description of the data type from the FOM: <i>Specifies the camouflage scheme/color for each mine in a collection of mines</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>paintScheme</code> attribute.
      */
      LIBAPI virtual std::vector<DevStudio::MinefieldPaintSchemeEnum::MinefieldPaintSchemeEnum > getPaintScheme(std::vector<DevStudio::MinefieldPaintSchemeEnum::MinefieldPaintSchemeEnum > defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>paintScheme</code> attribute.
      * <br>Description from the FOM: <i>Specifies the camouflage scheme/color of the mine</i>
      * <br>Description of the data type from the FOM: <i>Specifies the camouflage scheme/color for each mine in a collection of mines</i>
      *
      * @return the time stamped <code>paintScheme</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::vector<DevStudio::MinefieldPaintSchemeEnum::MinefieldPaintSchemeEnum > > getPaintSchemeTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>reflectance</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies the local dielectric difference between the mine and the surrounding soil</i>
      *
      * @return true if <code>reflectance</code> is available.
      */
      LIBAPI virtual bool hasReflectance() = 0;

      /**
      * Gets the value of the <code>reflectance</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies the local dielectric difference between the mine and the surrounding soil</i>
      * <br>Description of the data type from the FOM: <i>Specifies local dielectric difference between the mine and the surrounding soil (reflectance) for each mine in a collection of mines</i>
      *
      * @return the <code>reflectance</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::vector<float > getReflectance()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>reflectance</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies the local dielectric difference between the mine and the surrounding soil</i>
      * <br>Description of the data type from the FOM: <i>Specifies local dielectric difference between the mine and the surrounding soil (reflectance) for each mine in a collection of mines</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>reflectance</code> attribute.
      */
      LIBAPI virtual std::vector<float > getReflectance(std::vector<float > defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>reflectance</code> attribute.
      * <br>Description from the FOM: <i>Specifies the local dielectric difference between the mine and the surrounding soil</i>
      * <br>Description of the data type from the FOM: <i>Specifies local dielectric difference between the mine and the surrounding soil (reflectance) for each mine in a collection of mines</i>
      *
      * @return the time stamped <code>reflectance</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::vector<float > > getReflectanceTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>scalarDetectionCoefficient</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies the coefficient to be utilized to insure proper correlation between detectors located on different simulation platforms</i>
      *
      * @return true if <code>scalarDetectionCoefficient</code> is available.
      */
      LIBAPI virtual bool hasScalarDetectionCoefficient() = 0;

      /**
      * Gets the value of the <code>scalarDetectionCoefficient</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies the coefficient to be utilized to insure proper correlation between detectors located on different simulation platforms</i>
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of UnsignedInteger8 elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
      *
      * @return the <code>scalarDetectionCoefficient</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::vector<char > getScalarDetectionCoefficient()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>scalarDetectionCoefficient</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies the coefficient to be utilized to insure proper correlation between detectors located on different simulation platforms</i>
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of UnsignedInteger8 elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>scalarDetectionCoefficient</code> attribute.
      */
      LIBAPI virtual std::vector<char > getScalarDetectionCoefficient(std::vector<char > defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>scalarDetectionCoefficient</code> attribute.
      * <br>Description from the FOM: <i>Specifies the coefficient to be utilized to insure proper correlation between detectors located on different simulation platforms</i>
      * <br>Description of the data type from the FOM: <i>Generic dynamic array of UnsignedInteger8 elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
      *
      * @return the time stamped <code>scalarDetectionCoefficient</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::vector<char > > getScalarDetectionCoefficientTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>sensorTypes</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>In QRP mode, specifies the requesting sensor types which were specified in the minefield query whereas in Heartbeat mode, specifies the sensor types that are being served by the minefield</i>
      *
      * @return true if <code>sensorTypes</code> is available.
      */
      LIBAPI virtual bool hasSensorTypes() = 0;

      /**
      * Gets the value of the <code>sensorTypes</code> attribute.
      *
      * <br>Description from the FOM: <i>In QRP mode, specifies the requesting sensor types which were specified in the minefield query whereas in Heartbeat mode, specifies the sensor types that are being served by the minefield</i>
      * <br>Description of the data type from the FOM: <i>Specifies a collection of minefield sensor types</i>
      *
      * @return the <code>sensorTypes</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::vector<DevStudio::MinefieldSensorTypeEnum::MinefieldSensorTypeEnum > getSensorTypes()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>sensorTypes</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>In QRP mode, specifies the requesting sensor types which were specified in the minefield query whereas in Heartbeat mode, specifies the sensor types that are being served by the minefield</i>
      * <br>Description of the data type from the FOM: <i>Specifies a collection of minefield sensor types</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>sensorTypes</code> attribute.
      */
      LIBAPI virtual std::vector<DevStudio::MinefieldSensorTypeEnum::MinefieldSensorTypeEnum > getSensorTypes(std::vector<DevStudio::MinefieldSensorTypeEnum::MinefieldSensorTypeEnum > defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>sensorTypes</code> attribute.
      * <br>Description from the FOM: <i>In QRP mode, specifies the requesting sensor types which were specified in the minefield query whereas in Heartbeat mode, specifies the sensor types that are being served by the minefield</i>
      * <br>Description of the data type from the FOM: <i>Specifies a collection of minefield sensor types</i>
      *
      * @return the time stamped <code>sensorTypes</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::vector<DevStudio::MinefieldSensorTypeEnum::MinefieldSensorTypeEnum > > getSensorTypesTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>snowBurialDepthOffset</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies the offset of the origin of the mine coordinate system with respect to the snow surface</i>
      *
      * @return true if <code>snowBurialDepthOffset</code> is available.
      */
      LIBAPI virtual bool hasSnowBurialDepthOffset() = 0;

      /**
      * Gets the value of the <code>snowBurialDepthOffset</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies the offset of the origin of the mine coordinate system with respect to the snow surface</i>
      * <br>Description of the data type from the FOM: <i>Specifies ground - snow - water burial depth offset for each mine in a collection of mines</i>
      *
      * @return the <code>snowBurialDepthOffset</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::vector<float > getSnowBurialDepthOffset()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>snowBurialDepthOffset</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies the offset of the origin of the mine coordinate system with respect to the snow surface</i>
      * <br>Description of the data type from the FOM: <i>Specifies ground - snow - water burial depth offset for each mine in a collection of mines</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>snowBurialDepthOffset</code> attribute.
      */
      LIBAPI virtual std::vector<float > getSnowBurialDepthOffset(std::vector<float > defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>snowBurialDepthOffset</code> attribute.
      * <br>Description from the FOM: <i>Specifies the offset of the origin of the mine coordinate system with respect to the snow surface</i>
      * <br>Description of the data type from the FOM: <i>Specifies ground - snow - water burial depth offset for each mine in a collection of mines</i>
      *
      * @return the time stamped <code>snowBurialDepthOffset</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::vector<float > > getSnowBurialDepthOffsetTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>thermalContrast</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies the temperature difference between the mine and the surround soil in degrees Centigrade</i>
      *
      * @return true if <code>thermalContrast</code> is available.
      */
      LIBAPI virtual bool hasThermalContrast() = 0;

      /**
      * Gets the value of the <code>thermalContrast</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies the temperature difference between the mine and the surround soil in degrees Centigrade</i>
      * <br>Description of the data type from the FOM: <i>Specifies thermal contrast for each mine in a collection of mines</i>
      *
      * @return the <code>thermalContrast</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::vector<float > getThermalContrast()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>thermalContrast</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies the temperature difference between the mine and the surround soil in degrees Centigrade</i>
      * <br>Description of the data type from the FOM: <i>Specifies thermal contrast for each mine in a collection of mines</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>thermalContrast</code> attribute.
      */
      LIBAPI virtual std::vector<float > getThermalContrast(std::vector<float > defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>thermalContrast</code> attribute.
      * <br>Description from the FOM: <i>Specifies the temperature difference between the mine and the surround soil in degrees Centigrade</i>
      * <br>Description of the data type from the FOM: <i>Specifies thermal contrast for each mine in a collection of mines</i>
      *
      * @return the time stamped <code>thermalContrast</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::vector<float > > getThermalContrastTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>waterBurialDepthOffset</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies the offset of the origin of the mine coordinate system with respect to the water surface</i>
      *
      * @return true if <code>waterBurialDepthOffset</code> is available.
      */
      LIBAPI virtual bool hasWaterBurialDepthOffset() = 0;

      /**
      * Gets the value of the <code>waterBurialDepthOffset</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies the offset of the origin of the mine coordinate system with respect to the water surface</i>
      * <br>Description of the data type from the FOM: <i>Specifies ground - snow - water burial depth offset for each mine in a collection of mines</i>
      *
      * @return the <code>waterBurialDepthOffset</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::vector<float > getWaterBurialDepthOffset()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>waterBurialDepthOffset</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies the offset of the origin of the mine coordinate system with respect to the water surface</i>
      * <br>Description of the data type from the FOM: <i>Specifies ground - snow - water burial depth offset for each mine in a collection of mines</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>waterBurialDepthOffset</code> attribute.
      */
      LIBAPI virtual std::vector<float > getWaterBurialDepthOffset(std::vector<float > defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>waterBurialDepthOffset</code> attribute.
      * <br>Description from the FOM: <i>Specifies the offset of the origin of the mine coordinate system with respect to the water surface</i>
      * <br>Description of the data type from the FOM: <i>Specifies ground - snow - water burial depth offset for each mine in a collection of mines</i>
      *
      * @return the time stamped <code>waterBurialDepthOffset</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::vector<float > > getWaterBurialDepthOffsetTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>wireVertices</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies the locations of vertices in a trip wire</i>
      *
      * @return true if <code>wireVertices</code> is available.
      */
      LIBAPI virtual bool hasWireVertices() = 0;

      /**
      * Gets the value of the <code>wireVertices</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies the locations of vertices in a trip wire</i>
      * <br>Description of the data type from the FOM: <i>Dynamic array of WorldLocationStruct elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
      *
      * @return the <code>wireVertices</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::vector<DevStudio::WorldLocationStruct > getWireVertices()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>wireVertices</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies the locations of vertices in a trip wire</i>
      * <br>Description of the data type from the FOM: <i>Dynamic array of WorldLocationStruct elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>wireVertices</code> attribute.
      */
      LIBAPI virtual std::vector<DevStudio::WorldLocationStruct > getWireVertices(std::vector<DevStudio::WorldLocationStruct > defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>wireVertices</code> attribute.
      * <br>Description from the FOM: <i>Specifies the locations of vertices in a trip wire</i>
      * <br>Description of the data type from the FOM: <i>Dynamic array of WorldLocationStruct elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
      *
      * @return the time stamped <code>wireVertices</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::vector<DevStudio::WorldLocationStruct > > getWireVerticesTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>entityIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
      *
      * @return true if <code>entityIdentifier</code> is available.
      */
      LIBAPI virtual bool hasEntityIdentifier() = 0;

      /**
      * Gets the value of the <code>entityIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @return the <code>entityIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::EntityIdentifierStruct getEntityIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>entityIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>entityIdentifier</code> attribute.
      */
      LIBAPI virtual DevStudio::EntityIdentifierStruct getEntityIdentifier(DevStudio::EntityIdentifierStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>entityIdentifier</code> attribute.
      * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @return the time stamped <code>entityIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::EntityIdentifierStruct > getEntityIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>hostObjectIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
      *
      * @return true if <code>hostObjectIdentifier</code> is available.
      */
      LIBAPI virtual bool hasHostObjectIdentifier() = 0;

      /**
      * Gets the value of the <code>hostObjectIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @return the <code>hostObjectIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::string getHostObjectIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>hostObjectIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>hostObjectIdentifier</code> attribute.
      */
      LIBAPI virtual std::string getHostObjectIdentifier(std::string defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>hostObjectIdentifier</code> attribute.
      * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @return the time stamped <code>hostObjectIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::string > getHostObjectIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>relativePosition</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
      *
      * @return true if <code>relativePosition</code> is available.
      */
      LIBAPI virtual bool hasRelativePosition() = 0;

      /**
      * Gets the value of the <code>relativePosition</code> attribute.
      *
      * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
      * <br>Description of the data type from the FOM: <i>Relative position in right-handed Cartesian coordinates.</i>
      *
      * @return the <code>relativePosition</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::RelativePositionStruct getRelativePosition()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>relativePosition</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
      * <br>Description of the data type from the FOM: <i>Relative position in right-handed Cartesian coordinates.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>relativePosition</code> attribute.
      */
      LIBAPI virtual DevStudio::RelativePositionStruct getRelativePosition(DevStudio::RelativePositionStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>relativePosition</code> attribute.
      * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
      * <br>Description of the data type from the FOM: <i>Relative position in right-handed Cartesian coordinates.</i>
      *
      * @return the time stamped <code>relativePosition</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::RelativePositionStruct > getRelativePositionTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
   };
}
#endif
