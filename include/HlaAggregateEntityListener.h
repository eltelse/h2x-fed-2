/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAAGGREGATEENTITYLISTENER_H
#define DEVELOPER_STUDIO_HLAAGGREGATEENTITYLISTENER_H

#include <set>


#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaAggregateEntityAttributes.h>

namespace DevStudio {
   /**
   * Listener for updates of attributes.  
   */
   class HlaAggregateEntityListener {
   public:

      LIBAPI virtual ~HlaAggregateEntityListener() {}

      /**
      * This method is called when a HLA <code>reflectAttributeValueUpdate</code> is received for an remote object,
      * or a set of attributes is updated on a local object.
      *
      * @param aggregateEntity The aggregateEntity which this update corresponds to.
      * @param attributes The set of attributes that are updated.
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time the update was initiated.
      */
      LIBAPI virtual void attributesUpdated(HlaAggregateEntityPtr aggregateEntity, std::set<HlaAggregateEntityAttributes::Attribute> &attributes, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

      class Adapter;
   };

  /**
  * An adapter class that implements the HlaAggregateEntityListener interface with empty methods.
  * It might be used as a base class for a listener.
  */
  class HlaAggregateEntityListener::Adapter : public HlaAggregateEntityListener {

  public:

     LIBAPI virtual void attributesUpdated(HlaAggregateEntityPtr aggregateEntity, std::set<HlaAggregateEntityAttributes::Attribute> &attributes, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
    };

}
#endif
