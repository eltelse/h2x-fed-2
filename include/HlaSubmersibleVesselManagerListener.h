/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLASUBMERSIBLEVESSELMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLASUBMERSIBLEVESSELMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaSubmersibleVessel.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaSubmersibleVessel instances.
    */
    class HlaSubmersibleVesselManagerListener {

    public:

        LIBAPI virtual ~HlaSubmersibleVesselManagerListener() {}

        /**
        * This method is called when a new HlaSubmersibleVessel instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param submersibleVessel the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaSubmersibleVesselDiscovered(HlaSubmersibleVesselPtr submersibleVessel, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaSubmersibleVessel instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param submersibleVessel the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaSubmersibleVesselInitialized(HlaSubmersibleVesselPtr submersibleVessel, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaSubmersibleVesselManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param submersibleVessel the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaSubmersibleVesselInInterest(HlaSubmersibleVesselPtr submersibleVessel, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaSubmersibleVesselManagerListener instance goes out of interest.
        *
        * @param submersibleVessel the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaSubmersibleVesselOutOfInterest(HlaSubmersibleVesselPtr submersibleVessel, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaSubmersibleVessel instance is deleted.
        *
        * @param submersibleVessel the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaSubmersibleVesselDeleted(HlaSubmersibleVesselPtr submersibleVessel, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaSubmersibleVesselManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaSubmersibleVesselManagerListener::Adapter : public HlaSubmersibleVesselManagerListener {

    public:
        LIBAPI virtual void hlaSubmersibleVesselDiscovered(HlaSubmersibleVesselPtr submersibleVessel, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaSubmersibleVesselInitialized(HlaSubmersibleVesselPtr submersibleVessel, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaSubmersibleVesselInInterest(HlaSubmersibleVesselPtr submersibleVessel, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaSubmersibleVesselOutOfInterest(HlaSubmersibleVesselPtr submersibleVessel, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaSubmersibleVesselDeleted(HlaSubmersibleVesselPtr submersibleVessel, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
