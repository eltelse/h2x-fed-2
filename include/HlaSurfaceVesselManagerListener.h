/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLASURFACEVESSELMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLASURFACEVESSELMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaSurfaceVessel.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaSurfaceVessel instances.
    */
    class HlaSurfaceVesselManagerListener {

    public:

        LIBAPI virtual ~HlaSurfaceVesselManagerListener() {}

        /**
        * This method is called when a new HlaSurfaceVessel instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param surfaceVessel the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaSurfaceVesselDiscovered(HlaSurfaceVesselPtr surfaceVessel, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaSurfaceVessel instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param surfaceVessel the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaSurfaceVesselInitialized(HlaSurfaceVesselPtr surfaceVessel, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaSurfaceVesselManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param surfaceVessel the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaSurfaceVesselInInterest(HlaSurfaceVesselPtr surfaceVessel, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaSurfaceVesselManagerListener instance goes out of interest.
        *
        * @param surfaceVessel the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaSurfaceVesselOutOfInterest(HlaSurfaceVesselPtr surfaceVessel, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaSurfaceVessel instance is deleted.
        *
        * @param surfaceVessel the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaSurfaceVesselDeleted(HlaSurfaceVesselPtr surfaceVessel, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaSurfaceVesselManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaSurfaceVesselManagerListener::Adapter : public HlaSurfaceVesselManagerListener {

    public:
        LIBAPI virtual void hlaSurfaceVesselDiscovered(HlaSurfaceVesselPtr surfaceVessel, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaSurfaceVesselInitialized(HlaSurfaceVesselPtr surfaceVessel, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaSurfaceVesselInInterest(HlaSurfaceVesselPtr surfaceVessel, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaSurfaceVesselOutOfInterest(HlaSurfaceVesselPtr surfaceVessel, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaSurfaceVesselDeleted(HlaSurfaceVesselPtr surfaceVessel, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
