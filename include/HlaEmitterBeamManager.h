/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAEMITTERBEAMMANAGER_H
#define DEVELOPER_STUDIO_HLAEMITTERBEAMMANAGER_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <DevStudio/datatypes/BeamFunctionCodeEnum.h>
#include <DevStudio/datatypes/EventIdentifierStruct.h>
#include <string>

#include <list>
#include <vector>

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaEmitterBeamManagerListener.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaException.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

   /**
   * Manager used to manage HlaEmitterBeams.
   */
   class HlaEmitterBeamManager : private boost::noncopyable {

   public:

      LIBAPI virtual ~HlaEmitterBeamManager() {}

      /**
      * Gets a list of all HlaEmitterBeams within interest, both local and remote.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of HlaEmitterBeams
      */
      LIBAPI virtual std::list<HlaEmitterBeamPtr> getHlaEmitterBeams() = 0;

      /**
      * Gets a list of all HlaEmitterBeams, both local and remote.
      * HlaEmitterBeams not within interest are also included.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of all HlaEmitterBeams
      */
      LIBAPI virtual std::list<HlaEmitterBeamPtr> getAllHlaEmitterBeams() = 0;

      /**
      * Gets a list of local HlaEmitterBeams within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of local HlaEmitterBeams
      */
      LIBAPI virtual std::list<HlaEmitterBeamPtr> getLocalHlaEmitterBeams() = 0;

      /**
      * Gets a list of remote HlaEmitterBeams within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of remote HlaEmitterBeams
      */
      LIBAPI virtual std::list<HlaEmitterBeamPtr> getRemoteHlaEmitterBeams() = 0;

      /**
      * Find a HlaEmitterBeam with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> of the HlaEmitterBeam to find
      *
      * @return the specified HlaEmitterBeam, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaEmitterBeamPtr getEmitterBeamByHlaInstanceName(const std::wstring& hlaInstanceName) = 0;

      /**
      * Find a HlaEmitterBeam with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param encodedHlaInstanceHandle the encoded <code>HLA instance handle</code> of the HlaEmitterBeam to find
      *
      * @return the specified HlaEmitterBeam, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaEmitterBeamPtr getEmitterBeamByHlaInstanceHandle(const std::vector<char>& encodedHlaInstanceHandle) = 0;

      /**
      * Creates a new HlaEmitterBeam, and sets all <i>Create</i> attributes.
      *
      * @return Returns a new local HlaEmitterBeam.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaEmitterBeamPtr createLocalHlaEmitterBeam(
      ) THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Creates a new HlaEmitterBeam with a specified <code>HLA instance name</code>, and sets all
      * <i>Create</i> attributes.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> to register for this instance
      *
      * @return Returns a new local HlaEmitterBeam.
      *
      * @throws HlaIllegalInstanceNameException if the name is illegal
      * @throws HlaInstanceNameInUseException if the name is already in use
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaEmitterBeamPtr createLocalHlaEmitterBeam(
         const std::wstring& hlaInstanceName
      ) THROW_SPEC (HlaIllegalInstanceNameException, HlaInstanceNameInUseException,
                    HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaEmitterBeam and removes it from the federation.
      *
      * @param emitterBeam The HlaEmitterBeam to delete
      *
      * @return the HlaEmitterBeam deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaEmitterBeamPtr deleteLocalHlaEmitterBeam(HlaEmitterBeamPtr emitterBeam)
          THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                      HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaEmitterBeam and removes it from the federation.
      *
      * @param emitterBeam The HlaEmitterBeam to delete
      * @param timestamp timestamp when the instance was removed.
      *
      * @return the HlaEmitterBeam deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaEmitterBeamPtr deleteLocalHlaEmitterBeam(HlaEmitterBeamPtr emitterBeam, HlaTimeStampPtr timestamp)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaEmitterBeam and removes it from the federation.
      *
      * @param emitterBeam The HlaEmitterBeam to delete
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaEmitterBeam deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaEmitterBeamPtr deleteLocalHlaEmitterBeam(HlaEmitterBeamPtr emitterBeam, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaEmitterBeam and removes it from the federation.
      *
      * @param emitterBeam The HlaEmitterBeam to delete
      * @param timestamp timestamp when the instance was removed.
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaEmitterBeam deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaEmitterBeamPtr deleteLocalHlaEmitterBeam(HlaEmitterBeamPtr emitterBeam, HlaTimeStampPtr timestamp, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Add a HlaEmitterBeam manager listener.
      *
      * @param managerListener listener to add
      */
      LIBAPI virtual void addHlaEmitterBeamManagerListener(HlaEmitterBeamManagerListenerPtr managerListener) = 0;

      /**
      * Removed a HlaEmitterBeam manager listener.
      *
      * @param managerListener listener to remove
      */
      LIBAPI virtual void removeHlaEmitterBeamManagerListener(HlaEmitterBeamManagerListenerPtr managerListener) = 0;

     /**
      * Add a listener that will automatically be added to all instances of HlaEmitterBeam (Both local and remote).
      * The listener is notified when any attribute of an instance of HlaEmitterBeam is updated.
      * The listener is also called when an interaction is sent to an instance of HlaEmitterBeam.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param listener The listener to add.
      */
      LIBAPI virtual void addHlaEmitterBeamDefaultInstanceListener(HlaEmitterBeamListenerPtr listener) = 0;

     /**
      * Remove a listener from the set of listeners that are automatically added to new instances of HlaEmitterBeam.
      * Note: The listener will not be removed from already existing instances of HlaEmitterBeam.
      * This method is idempotent.
      *
      * @param listener The listener to remove.
      */
      LIBAPI virtual void removeHlaEmitterBeamDefaultInstanceListener(HlaEmitterBeamListenerPtr listener) = 0;

     /**
      * Add a valueListener that will automatically be added to all instances of HlaEmitterBeam (Both local and remote).
      * The  Valuelistener is notified when any attribute of an instance of HlaEmitterBeam is updated.
      * Note that the attribute values that are notified are not guaranteed to
      * be the latest available values for that attribute.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to add.
      */
      LIBAPI virtual void addHlaEmitterBeamDefaultInstanceValueListener(HlaEmitterBeamValueListenerPtr valueListener) = 0;

     /**
      * Remove a valueListener from the set of listeners that are automatically added to new instances of HlaEmitterBeam.
      * Note: The valueListener will not be removed from already existing instances of HlaEmitterBeam.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to remove.
      */
      LIBAPI virtual void removeHlaEmitterBeamDefaultInstanceValueListener(HlaEmitterBeamValueListenerPtr valueListener) = 0;

     /**
      * Enables or disables the HlaEmitterBeam manager.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaEmitterBeam manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      *
      * @param enabled <code>true</code> to enable the HlaEmitterBeam manager, otherwise <code>false</code>
      */
      LIBAPI virtual void setEnabled(bool enabled) = 0;

     /**
      * Enables the HlaEmitterBeam manager if it is avalable in the FOM at runtime.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaEmitterBeam manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      */
      LIBAPI virtual void setEnabledIfAvailableInFom() = 0;

     /**
      * Check if the HlaEmitterBeam manager is actually enabled when connected.
      * An HlaEmitterBeam manager may be disabled with the <code>setEnabled(false)</code> function, or it may be configured
      * as <i>if available in the FOM</i> by the <code>setEnabledIfAvailableInFom()</code> function and
      * then not available in the FOM at runtime.
      * <p>
      * This method should only be used when connected, it will always return <code>false</code>
      * when not connected.
      *
      * @return <code>true</code> if connected and the HlaEmitterBeam manager is enabled.
      */
      LIBAPI virtual bool isEnabled() = 0;
   };
}
#endif
