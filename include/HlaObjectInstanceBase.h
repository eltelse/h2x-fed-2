/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAOBJECTINSTANCEBASE_H
#define DEVELOPER_STUDIO_HLAOBJECTINSTANCEBASE_H

#include <string>
#include <memory>
#include <vector>

#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaFederateId.h>


namespace DevStudio {

    /**
    * Base interface used by all object instances.
    */
    class HlaObjectInstanceBase {

    public:

        /**
        * An enumeration of all object class types.
        */
        enum ObjectClassType {
            /** HLAobjectRoot, instance of <code>HlaHLAobjectRoot</code> (FOM name <code>HLAobjectRoot</code>) */
            HLAOBJECT_ROOT,
            /** BaseEntity, instance of <code>HlaBaseEntity</code> (FOM name <code>HLAobjectRoot.BaseEntity</code>) */
            BASE_ENTITY,
            /** PhysicalEntity, instance of <code>HlaPhysicalEntity</code> (FOM name <code>HLAobjectRoot.BaseEntity.PhysicalEntity</code>) */
            PHYSICAL_ENTITY,
            /** Platform, instance of <code>HlaPlatform</code> (FOM name <code>HLAobjectRoot.BaseEntity.PhysicalEntity.Platform</code>) */
            PLATFORM,
            /** Aircraft, instance of <code>HlaAircraft</code> (FOM name <code>HLAobjectRoot.BaseEntity.PhysicalEntity.Platform.Aircraft</code>) */
            AIRCRAFT,
            /** AmphibiousVehicle, instance of <code>HlaAmphibiousVehicle</code> (FOM name <code>HLAobjectRoot.BaseEntity.PhysicalEntity.Platform.AmphibiousVehicle</code>) */
            AMPHIBIOUS_VEHICLE,
            /** GroundVehicle, instance of <code>HlaGroundVehicle</code> (FOM name <code>HLAobjectRoot.BaseEntity.PhysicalEntity.Platform.GroundVehicle</code>) */
            GROUND_VEHICLE,
            /** MultiDomainPlatform, instance of <code>HlaMultiDomainPlatform</code> (FOM name <code>HLAobjectRoot.BaseEntity.PhysicalEntity.Platform.MultiDomainPlatform</code>) */
            MULTI_DOMAIN_PLATFORM,
            /** Spacecraft, instance of <code>HlaSpacecraft</code> (FOM name <code>HLAobjectRoot.BaseEntity.PhysicalEntity.Platform.Spacecraft</code>) */
            SPACECRAFT,
            /** SubmersibleVessel, instance of <code>HlaSubmersibleVessel</code> (FOM name <code>HLAobjectRoot.BaseEntity.PhysicalEntity.Platform.SubmersibleVessel</code>) */
            SUBMERSIBLE_VESSEL,
            /** SurfaceVessel, instance of <code>HlaSurfaceVessel</code> (FOM name <code>HLAobjectRoot.BaseEntity.PhysicalEntity.Platform.SurfaceVessel</code>) */
            SURFACE_VESSEL,
            /** Lifeform, instance of <code>HlaLifeform</code> (FOM name <code>HLAobjectRoot.BaseEntity.PhysicalEntity.Lifeform</code>) */
            LIFEFORM,
            /** Human, instance of <code>HlaHuman</code> (FOM name <code>HLAobjectRoot.BaseEntity.PhysicalEntity.Lifeform.Human</code>) */
            HUMAN,
            /** NonHuman, instance of <code>HlaNonHuman</code> (FOM name <code>HLAobjectRoot.BaseEntity.PhysicalEntity.Lifeform.NonHuman</code>) */
            NON_HUMAN,
            /** CulturalFeature, instance of <code>HlaCulturalFeature</code> (FOM name <code>HLAobjectRoot.BaseEntity.PhysicalEntity.CulturalFeature</code>) */
            CULTURAL_FEATURE,
            /** Munition, instance of <code>HlaMunition</code> (FOM name <code>HLAobjectRoot.BaseEntity.PhysicalEntity.Munition</code>) */
            MUNITION,
            /** Expendables, instance of <code>HlaExpendables</code> (FOM name <code>HLAobjectRoot.BaseEntity.PhysicalEntity.Expendables</code>) */
            EXPENDABLES,
            /** Radio, instance of <code>HlaRadio</code> (FOM name <code>HLAobjectRoot.BaseEntity.PhysicalEntity.Radio</code>) */
            RADIO,
            /** Sensor, instance of <code>HlaSensor</code> (FOM name <code>HLAobjectRoot.BaseEntity.PhysicalEntity.Sensor</code>) */
            SENSOR,
            /** Supplies, instance of <code>HlaSupplies</code> (FOM name <code>HLAobjectRoot.BaseEntity.PhysicalEntity.Supplies</code>) */
            SUPPLIES,
            /** AggregateEntity, instance of <code>HlaAggregateEntity</code> (FOM name <code>HLAobjectRoot.BaseEntity.AggregateEntity</code>) */
            AGGREGATE_ENTITY,
            /** ExtendedAggregateEntity, instance of <code>HlaExtendedAggregateEntity</code> (FOM name <code>HLAobjectRoot.BaseEntity.AggregateEntity.ExtendedAggregateEntity</code>) */
            EXTENDED_AGGREGATE_ENTITY,
            /** EnvironmentalEntity, instance of <code>HlaEnvironmentalEntity</code> (FOM name <code>HLAobjectRoot.BaseEntity.EnvironmentalEntity</code>) */
            ENVIRONMENTAL_ENTITY,
            /** SpotObject, instance of <code>HlaSpotObject</code> (FOM name <code>HLAobjectRoot.BaseEntity.SpotObject</code>) */
            SPOT_OBJECT,
            /** ThreatSpotObject, instance of <code>HlaThreatSpotObject</code> (FOM name <code>HLAobjectRoot.BaseEntity.SpotObject.ThreatSpotObject</code>) */
            THREAT_SPOT_OBJECT,
            /** VisintSpotObject, instance of <code>HlaVisintSpotObject</code> (FOM name <code>HLAobjectRoot.BaseEntity.SpotObject.VisintSpotObject</code>) */
            VISINT_SPOT_OBJECT,
            /** Minefield, instance of <code>HlaMinefield</code> (FOM name <code>HLAobjectRoot.Minefield</code>) */
            MINEFIELD,
            /** EmbeddedSystem, instance of <code>HlaEmbeddedSystem</code> (FOM name <code>HLAobjectRoot.EmbeddedSystem</code>) */
            EMBEDDED_SYSTEM,
            /** MinefieldData, instance of <code>HlaMinefieldData</code> (FOM name <code>HLAobjectRoot.EmbeddedSystem.MinefieldData</code>) */
            MINEFIELD_DATA,
            /** RadioTransmitter, instance of <code>HlaRadioTransmitter</code> (FOM name <code>HLAobjectRoot.EmbeddedSystem.RadioTransmitter</code>) */
            RADIO_TRANSMITTER,
            /** RadioReceiver, instance of <code>HlaRadioReceiver</code> (FOM name <code>HLAobjectRoot.EmbeddedSystem.RadioReceiver</code>) */
            RADIO_RECEIVER,
            /** Designator, instance of <code>HlaDesignator</code> (FOM name <code>HLAobjectRoot.EmbeddedSystem.Designator</code>) */
            DESIGNATOR,
            /** EmitterSystem, instance of <code>HlaEmitterSystem</code> (FOM name <code>HLAobjectRoot.EmbeddedSystem.EmitterSystem</code>) */
            EMITTER_SYSTEM,
            /** IFF, instance of <code>HlaIFF</code> (FOM name <code>HLAobjectRoot.EmbeddedSystem.IFF</code>) */
            IFF,
            /** NatoIFF, instance of <code>HlaNatoIFF</code> (FOM name <code>HLAobjectRoot.EmbeddedSystem.IFF.NatoIFF</code>) */
            NATO_IFF,
            /** NatoIFFInterrogator, instance of <code>HlaNatoIFFInterrogator</code> (FOM name <code>HLAobjectRoot.EmbeddedSystem.IFF.NatoIFF.NatoIFFInterrogator</code>) */
            NATO_IFFINTERROGATOR,
            /** NatoIFFTransponder, instance of <code>HlaNatoIFFTransponder</code> (FOM name <code>HLAobjectRoot.EmbeddedSystem.IFF.NatoIFF.NatoIFFTransponder</code>) */
            NATO_IFFTRANSPONDER,
            /** SovietIFF, instance of <code>HlaSovietIFF</code> (FOM name <code>HLAobjectRoot.EmbeddedSystem.IFF.SovietIFF</code>) */
            SOVIET_IFF,
            /** SovietIFFInterrogator, instance of <code>HlaSovietIFFInterrogator</code> (FOM name <code>HLAobjectRoot.EmbeddedSystem.IFF.SovietIFF.SovietIFFInterrogator</code>) */
            SOVIET_IFFINTERROGATOR,
            /** SovietIFFTransponder, instance of <code>HlaSovietIFFTransponder</code> (FOM name <code>HLAobjectRoot.EmbeddedSystem.IFF.SovietIFF.SovietIFFTransponder</code>) */
            SOVIET_IFFTRANSPONDER,
            /** RRB, instance of <code>HlaRRB</code> (FOM name <code>HLAobjectRoot.EmbeddedSystem.IFF.RRB</code>) */
            RRB,
            /** UnderwaterAcousticsEmission, instance of <code>HlaUnderwaterAcousticsEmission</code> (FOM name <code>HLAobjectRoot.EmbeddedSystem.UnderwaterAcousticsEmission</code>) */
            UNDERWATER_ACOUSTICS_EMISSION,
            /** ActiveSonar, instance of <code>HlaActiveSonar</code> (FOM name <code>HLAobjectRoot.EmbeddedSystem.UnderwaterAcousticsEmission.ActiveSonar</code>) */
            ACTIVE_SONAR,
            /** PropulsionNoise, instance of <code>HlaPropulsionNoise</code> (FOM name <code>HLAobjectRoot.EmbeddedSystem.UnderwaterAcousticsEmission.PropulsionNoise</code>) */
            PROPULSION_NOISE,
            /** AdditionalPassiveActivities, instance of <code>HlaAdditionalPassiveActivities</code> (FOM name <code>HLAobjectRoot.EmbeddedSystem.UnderwaterAcousticsEmission.AdditionalPassiveActivities</code>) */
            ADDITIONAL_PASSIVE_ACTIVITIES,
            /** EnvironmentObject, instance of <code>HlaEnvironmentObject</code> (FOM name <code>HLAobjectRoot.EnvironmentObject</code>) */
            ENVIRONMENT_OBJECT,
            /** ArealObject, instance of <code>HlaArealObject</code> (FOM name <code>HLAobjectRoot.EnvironmentObject.ArealObject</code>) */
            AREAL_OBJECT,
            /** OtherArealObject, instance of <code>HlaOtherArealObject</code> (FOM name <code>HLAobjectRoot.EnvironmentObject.ArealObject.OtherArealObject</code>) */
            OTHER_AREAL_OBJECT,
            /** MinefieldObject, instance of <code>HlaMinefieldObject</code> (FOM name <code>HLAobjectRoot.EnvironmentObject.ArealObject.MinefieldObject</code>) */
            MINEFIELD_OBJECT,
            /** LinearObject, instance of <code>HlaLinearObject</code> (FOM name <code>HLAobjectRoot.EnvironmentObject.LinearObject</code>) */
            LINEAR_OBJECT,
            /** BreachableLinearObject, instance of <code>HlaBreachableLinearObject</code> (FOM name <code>HLAobjectRoot.EnvironmentObject.LinearObject.BreachableLinearObject</code>) */
            BREACHABLE_LINEAR_OBJECT,
            /** BreachObject, instance of <code>HlaBreachObject</code> (FOM name <code>HLAobjectRoot.EnvironmentObject.LinearObject.BreachObject</code>) */
            BREACH_OBJECT,
            /** ExhaustSmokeObject, instance of <code>HlaExhaustSmokeObject</code> (FOM name <code>HLAobjectRoot.EnvironmentObject.LinearObject.ExhaustSmokeObject</code>) */
            EXHAUST_SMOKE_OBJECT,
            /** OtherLinearObject, instance of <code>HlaOtherLinearObject</code> (FOM name <code>HLAobjectRoot.EnvironmentObject.LinearObject.OtherLinearObject</code>) */
            OTHER_LINEAR_OBJECT,
            /** MinefieldLaneMarkerObject, instance of <code>HlaMinefieldLaneMarkerObject</code> (FOM name <code>HLAobjectRoot.EnvironmentObject.LinearObject.MinefieldLaneMarkerObject</code>) */
            MINEFIELD_LANE_MARKER_OBJECT,
            /** PointObject, instance of <code>HlaPointObject</code> (FOM name <code>HLAobjectRoot.EnvironmentObject.PointObject</code>) */
            POINT_OBJECT,
            /** BreachablePointObject, instance of <code>HlaBreachablePointObject</code> (FOM name <code>HLAobjectRoot.EnvironmentObject.PointObject.BreachablePointObject</code>) */
            BREACHABLE_POINT_OBJECT,
            /** BurstPointObject, instance of <code>HlaBurstPointObject</code> (FOM name <code>HLAobjectRoot.EnvironmentObject.PointObject.BurstPointObject</code>) */
            BURST_POINT_OBJECT,
            /** CraterObject, instance of <code>HlaCraterObject</code> (FOM name <code>HLAobjectRoot.EnvironmentObject.PointObject.CraterObject</code>) */
            CRATER_OBJECT,
            /** OtherPointObject, instance of <code>HlaOtherPointObject</code> (FOM name <code>HLAobjectRoot.EnvironmentObject.PointObject.OtherPointObject</code>) */
            OTHER_POINT_OBJECT,
            /** RibbonBridgeObject, instance of <code>HlaRibbonBridgeObject</code> (FOM name <code>HLAobjectRoot.EnvironmentObject.PointObject.RibbonBridgeObject</code>) */
            RIBBON_BRIDGE_OBJECT,
            /** StructureObject, instance of <code>HlaStructureObject</code> (FOM name <code>HLAobjectRoot.EnvironmentObject.PointObject.StructureObject</code>) */
            STRUCTURE_OBJECT,
            /** EnvironmentProcess, instance of <code>HlaEnvironmentProcess</code> (FOM name <code>HLAobjectRoot.EnvironmentProcess</code>) */
            ENVIRONMENT_PROCESS,
            /** GriddedData, instance of <code>HlaGriddedData</code> (FOM name <code>HLAobjectRoot.GriddedData</code>) */
            GRIDDED_DATA,
            /** EmitterBeam, instance of <code>HlaEmitterBeam</code> (FOM name <code>HLAobjectRoot.EmitterBeam</code>) */
            EMITTER_BEAM,
            /** RadarBeam, instance of <code>HlaRadarBeam</code> (FOM name <code>HLAobjectRoot.EmitterBeam.RadarBeam</code>) */
            RADAR_BEAM,
            /** JammerBeam, instance of <code>HlaJammerBeam</code> (FOM name <code>HLAobjectRoot.EmitterBeam.JammerBeam</code>) */
            JAMMER_BEAM,
            /** ActiveSonarBeam, instance of <code>HlaActiveSonarBeam</code> (FOM name <code>HLAobjectRoot.ActiveSonarBeam</code>) */
            ACTIVE_SONAR_BEAM
        };

        /**
        * Tells if this object instance is local or remote.
        * An local object is an object that you have originally created.
        *
        * @return true if local.
        */
        LIBAPI virtual bool isLocal() const = 0;

        /**
        * Tells if this object instance is initialized.
        *
        * @return true if initialized.
        */
        LIBAPI virtual bool isInitialized() = 0;

        /**
        * Tells if this object instance is within interest.
        * A removed instance is never within interest.
        *
        * @return true if within interest.
        */
        LIBAPI virtual bool isWithinInterest() const = 0;

        /**
        * Tells if this object instance has been removed.
        *
        * @return true if removed.
        */
        LIBAPI virtual bool isRemoved() const = 0;

        /**
        * Get the <code>HLA instance name</code>.
        *
        * @return the <code>HLA instance name</code>.
        */
        LIBAPI virtual std::wstring getHlaInstanceName() const = 0;

        /**
        * Get the encoded <code>HLA instance handle</code>.
        *
        * @return the encoded <code>HLA instance handle</code>.
        */
        LIBAPI virtual std::vector<char> getEncodedHlaObjectInstanceHandle() const = 0;

        /**
        * Get the object class type for this object instance.
        *
        * @return the object class type for this object instance
        */
        LIBAPI virtual ObjectClassType getClassType() const = 0;

        /**
        * Get the HlaFederateId for the federate that created this instance
        *
        * @return The federate id for the federate that created this instance
        */
        LIBAPI virtual HlaFederateIdPtr getProducingFederate() const = 0;

        LIBAPI virtual ~HlaObjectInstanceBase() {}
    };
}
#endif
