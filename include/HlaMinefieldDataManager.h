/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAMINEFIELDDATAMANAGER_H
#define DEVELOPER_STUDIO_HLAMINEFIELDDATAMANAGER_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <DevStudio/datatypes/ClockTimeStruct.h>
#include <DevStudio/datatypes/ClockTimeStructLengthlessArray.h>
#include <DevStudio/datatypes/DepthMeterFloat32LengthlessArray.h>
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EntityTypeStruct.h>
#include <DevStudio/datatypes/MineDielectricDifferenceLengthlessArray.h>
#include <DevStudio/datatypes/MineFusingStruct.h>
#include <DevStudio/datatypes/MineFusingStructLengthlessArray.h>
#include <DevStudio/datatypes/MineIdentifierLengthlessArray.h>
#include <DevStudio/datatypes/MinefieldPaintSchemeEnum.h>
#include <DevStudio/datatypes/MinefieldPaintSchemeLengthlessArray.h>
#include <DevStudio/datatypes/MinefieldSensorTypeEnum.h>
#include <DevStudio/datatypes/MinefieldSensorTypeLengthlessArray.h>
#include <DevStudio/datatypes/OrientationStruct.h>
#include <DevStudio/datatypes/OrientationStructLengthlessArray.h>
#include <DevStudio/datatypes/RelativePositionStruct.h>
#include <DevStudio/datatypes/TemperatureDegreeCelsiusFloat32LengthlessArray.h>
#include <DevStudio/datatypes/UnsignedInteger8LengthlessArray.h>
#include <DevStudio/datatypes/WorldLocationStruct.h>
#include <DevStudio/datatypes/WorldLocationStructLengthlessArray.h>
#include <string>
#include <vector>

#include <list>
#include <vector>

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaMinefieldDataManagerListener.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaException.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

   /**
   * Manager used to manage HlaMinefieldDatas.
   */
   class HlaMinefieldDataManager : private boost::noncopyable {

   public:

      LIBAPI virtual ~HlaMinefieldDataManager() {}

      /**
      * Gets a list of all HlaMinefieldDatas within interest, both local and remote.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of HlaMinefieldDatas
      */
      LIBAPI virtual std::list<HlaMinefieldDataPtr> getHlaMinefieldDatas() = 0;

      /**
      * Gets a list of all HlaMinefieldDatas, both local and remote.
      * HlaMinefieldDatas not within interest are also included.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of all HlaMinefieldDatas
      */
      LIBAPI virtual std::list<HlaMinefieldDataPtr> getAllHlaMinefieldDatas() = 0;

      /**
      * Gets a list of local HlaMinefieldDatas within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of local HlaMinefieldDatas
      */
      LIBAPI virtual std::list<HlaMinefieldDataPtr> getLocalHlaMinefieldDatas() = 0;

      /**
      * Gets a list of remote HlaMinefieldDatas within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of remote HlaMinefieldDatas
      */
      LIBAPI virtual std::list<HlaMinefieldDataPtr> getRemoteHlaMinefieldDatas() = 0;

      /**
      * Find a HlaMinefieldData with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> of the HlaMinefieldData to find
      *
      * @return the specified HlaMinefieldData, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaMinefieldDataPtr getMinefieldDataByHlaInstanceName(const std::wstring& hlaInstanceName) = 0;

      /**
      * Find a HlaMinefieldData with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param encodedHlaInstanceHandle the encoded <code>HLA instance handle</code> of the HlaMinefieldData to find
      *
      * @return the specified HlaMinefieldData, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaMinefieldDataPtr getMinefieldDataByHlaInstanceHandle(const std::vector<char>& encodedHlaInstanceHandle) = 0;

      /**
      * Creates a new HlaMinefieldData, and sets all <i>Create</i> attributes.
      *
      * @return Returns a new local HlaMinefieldData.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaMinefieldDataPtr createLocalHlaMinefieldData(
      ) THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Creates a new HlaMinefieldData with a specified <code>HLA instance name</code>, and sets all
      * <i>Create</i> attributes.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> to register for this instance
      *
      * @return Returns a new local HlaMinefieldData.
      *
      * @throws HlaIllegalInstanceNameException if the name is illegal
      * @throws HlaInstanceNameInUseException if the name is already in use
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaMinefieldDataPtr createLocalHlaMinefieldData(
         const std::wstring& hlaInstanceName
      ) THROW_SPEC (HlaIllegalInstanceNameException, HlaInstanceNameInUseException,
                    HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaMinefieldData and removes it from the federation.
      *
      * @param minefieldData The HlaMinefieldData to delete
      *
      * @return the HlaMinefieldData deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaMinefieldDataPtr deleteLocalHlaMinefieldData(HlaMinefieldDataPtr minefieldData)
          THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                      HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaMinefieldData and removes it from the federation.
      *
      * @param minefieldData The HlaMinefieldData to delete
      * @param timestamp timestamp when the instance was removed.
      *
      * @return the HlaMinefieldData deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaMinefieldDataPtr deleteLocalHlaMinefieldData(HlaMinefieldDataPtr minefieldData, HlaTimeStampPtr timestamp)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaMinefieldData and removes it from the federation.
      *
      * @param minefieldData The HlaMinefieldData to delete
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaMinefieldData deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaMinefieldDataPtr deleteLocalHlaMinefieldData(HlaMinefieldDataPtr minefieldData, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaMinefieldData and removes it from the federation.
      *
      * @param minefieldData The HlaMinefieldData to delete
      * @param timestamp timestamp when the instance was removed.
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaMinefieldData deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaMinefieldDataPtr deleteLocalHlaMinefieldData(HlaMinefieldDataPtr minefieldData, HlaTimeStampPtr timestamp, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Add a HlaMinefieldData manager listener.
      *
      * @param managerListener listener to add
      */
      LIBAPI virtual void addHlaMinefieldDataManagerListener(HlaMinefieldDataManagerListenerPtr managerListener) = 0;

      /**
      * Removed a HlaMinefieldData manager listener.
      *
      * @param managerListener listener to remove
      */
      LIBAPI virtual void removeHlaMinefieldDataManagerListener(HlaMinefieldDataManagerListenerPtr managerListener) = 0;

     /**
      * Add a listener that will automatically be added to all instances of HlaMinefieldData (Both local and remote).
      * The listener is notified when any attribute of an instance of HlaMinefieldData is updated.
      * The listener is also called when an interaction is sent to an instance of HlaMinefieldData.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param listener The listener to add.
      */
      LIBAPI virtual void addHlaMinefieldDataDefaultInstanceListener(HlaMinefieldDataListenerPtr listener) = 0;

     /**
      * Remove a listener from the set of listeners that are automatically added to new instances of HlaMinefieldData.
      * Note: The listener will not be removed from already existing instances of HlaMinefieldData.
      * This method is idempotent.
      *
      * @param listener The listener to remove.
      */
      LIBAPI virtual void removeHlaMinefieldDataDefaultInstanceListener(HlaMinefieldDataListenerPtr listener) = 0;

     /**
      * Add a valueListener that will automatically be added to all instances of HlaMinefieldData (Both local and remote).
      * The  Valuelistener is notified when any attribute of an instance of HlaMinefieldData is updated.
      * Note that the attribute values that are notified are not guaranteed to
      * be the latest available values for that attribute.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to add.
      */
      LIBAPI virtual void addHlaMinefieldDataDefaultInstanceValueListener(HlaMinefieldDataValueListenerPtr valueListener) = 0;

     /**
      * Remove a valueListener from the set of listeners that are automatically added to new instances of HlaMinefieldData.
      * Note: The valueListener will not be removed from already existing instances of HlaMinefieldData.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to remove.
      */
      LIBAPI virtual void removeHlaMinefieldDataDefaultInstanceValueListener(HlaMinefieldDataValueListenerPtr valueListener) = 0;

     /**
      * Enables or disables the HlaMinefieldData manager.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaMinefieldData manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      *
      * @param enabled <code>true</code> to enable the HlaMinefieldData manager, otherwise <code>false</code>
      */
      LIBAPI virtual void setEnabled(bool enabled) = 0;

     /**
      * Enables the HlaMinefieldData manager if it is avalable in the FOM at runtime.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaMinefieldData manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      */
      LIBAPI virtual void setEnabledIfAvailableInFom() = 0;

     /**
      * Check if the HlaMinefieldData manager is actually enabled when connected.
      * An HlaMinefieldData manager may be disabled with the <code>setEnabled(false)</code> function, or it may be configured
      * as <i>if available in the FOM</i> by the <code>setEnabledIfAvailableInFom()</code> function and
      * then not available in the FOM at runtime.
      * <p>
      * This method should only be used when connected, it will always return <code>false</code>
      * when not connected.
      *
      * @return <code>true</code> if connected and the HlaMinefieldData manager is enabled.
      */
      LIBAPI virtual bool isEnabled() = 0;
   };
}
#endif
