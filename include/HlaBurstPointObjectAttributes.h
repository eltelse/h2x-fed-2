/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLABURSTPOINTOBJECTATTRIBUTES_H
#define DEVELOPER_STUDIO_HLABURSTPOINTOBJECTATTRIBUTES_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <string>
#include <vector>
#include <utility>
    
#include <boost/noncopyable.hpp>
    
#include <DevStudio/datatypes/ChemicalContentEnum.h>
#include <DevStudio/datatypes/DamageStatusEnum.h>
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EnvironmentObjectTypeStruct.h>
#include <DevStudio/datatypes/ForceIdentifierEnum.h>
#include <DevStudio/datatypes/OrientationStruct.h>
#include <DevStudio/datatypes/WorldLocationStruct.h>
#include <string>

#include <DevStudio/HlaException.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaPointObjectAttributes.h>
#include <DevStudio/HlaTimeStamped.h>

namespace DevStudio {

   /**
   * Interface used to represent all attributes for an object instance.
   */
   class HlaBurstPointObjectAttributes : public HlaPointObjectAttributes {
   public:


     /**
      * An enumeration of the attributes of an HlaBurstPointObject
      *
      *<table summary="All attributes">
      * <tr><td><b>Enum constant</b></td><td><b>C++ name</b></td><td><b>FOM name</b></td></tr>
      * <tr><td>PERCENT_OPACITY</td><td>percentOpacity</td><td><code>PercentOpacity</code></td></tr>
      * <tr><td>CYLINDER_SIZE</td><td>cylinderSize</td><td><code>CylinderSize</code></td></tr>
      * <tr><td>CYLINDER_HEIGHT</td><td>cylinderHeight</td><td><code>CylinderHeight</code></td></tr>
      * <tr><td>NUMBER_OF_BURSTS</td><td>numberOfBursts</td><td><code>NumberOfBursts</code></td></tr>
      * <tr><td>CHEMICAL_CONTENT</td><td>chemicalContent</td><td><code>ChemicalContent</code></td></tr>
      * <tr><td>LOCATION</td><td>location</td><td><code>Location</code></td></tr>
      * <tr><td>ORIENTATION</td><td>orientation</td><td><code>Orientation</code></td></tr>
      * <tr><td>PERCENT_COMPLETE</td><td>percentComplete</td><td><code>PercentComplete</code></td></tr>
      * <tr><td>DAMAGED_APPEARANCE</td><td>damagedAppearance</td><td><code>DamagedAppearance</code></td></tr>
      * <tr><td>OBJECT_PRE_DISTRIBUTED</td><td>objectPreDistributed</td><td><code>ObjectPreDistributed</code></td></tr>
      * <tr><td>DEACTIVATED</td><td>deactivated</td><td><code>Deactivated</code></td></tr>
      * <tr><td>SMOKING</td><td>smoking</td><td><code>Smoking</code></td></tr>
      * <tr><td>FLAMING</td><td>flaming</td><td><code>Flaming</code></td></tr>
      * <tr><td>OBJECT_IDENTIFIER</td><td>objectIdentifier</td><td><code>ObjectIdentifier</code></td></tr>
      * <tr><td>REFERENCED_OBJECT_IDENTIFIER</td><td>referencedObjectIdentifier</td><td><code>ReferencedObjectIdentifier</code></td></tr>
      * <tr><td>FORCE_IDENTIFIER</td><td>forceIdentifier</td><td><code>ForceIdentifier</code></td></tr>
      * <tr><td>OBJECT_TYPE</td><td>objectType</td><td><code>ObjectType</code></td></tr>
      *</table>
      */
      enum Attribute {
        /**
        * percentOpacity (FOM name: <code>PercentOpacity</code>).
        * <br>Description from the FOM: <i>Specifies the opacity of the smoke</i>
        */
         PERCENT_OPACITY,

        /**
        * cylinderSize (FOM name: <code>CylinderSize</code>).
        * <br>Description from the FOM: <i>Specifies the radius of the cylinder approximating an individual smoke burst ; for multiple bursts, the center bottom of each cylinder is calculated based on the model used to represent the multiple bursts</i>
        */
         CYLINDER_SIZE,

        /**
        * cylinderHeight (FOM name: <code>CylinderHeight</code>).
        * <br>Description from the FOM: <i>Specifies the height of the cylinder approximating an individual smoke burst ; for multiple bursts, the center bottom of each cylinder is calculated based on the model used to represent the multiple bursts</i>
        */
         CYLINDER_HEIGHT,

        /**
        * numberOfBursts (FOM name: <code>NumberOfBursts</code>).
        * <br>Description from the FOM: <i>Specifies the number of bursts in the instance of tactical smoke</i>
        */
         NUMBER_OF_BURSTS,

        /**
        * chemicalContent (FOM name: <code>ChemicalContent</code>).
        * <br>Description from the FOM: <i>Specifies the chemical content of the smoke</i>
        */
         CHEMICAL_CONTENT,

        /**
        * location (FOM name: <code>Location</code>).
        * <br>Description from the FOM: <i>Specifies the location of the object based on x, y and z coordinates</i>
        */
         LOCATION,

        /**
        * orientation (FOM name: <code>Orientation</code>).
        * <br>Description from the FOM: <i>Specifies the angles of rotation around the coordinate axis between the object's attitude and the reference coordinate system axes ; these are calculated as the Tait-Bryan Euler angles, specifying the successive rotations needed to transform from the world coordinate system to the object coordinate system</i>
        */
         ORIENTATION,

        /**
        * percentComplete (FOM name: <code>PercentComplete</code>).
        * <br>Description from the FOM: <i>Specifies the percent completion of the object</i>
        */
         PERCENT_COMPLETE,

        /**
        * damagedAppearance (FOM name: <code>DamagedAppearance</code>).
        * <br>Description from the FOM: <i>Specifies the damaged appearance of the object instance</i>
        */
         DAMAGED_APPEARANCE,

        /**
        * objectPreDistributed (FOM name: <code>ObjectPreDistributed</code>).
        * <br>Description from the FOM: <i>Specifies whether or not the object was created before the start of the exercise</i>
        */
         OBJECT_PRE_DISTRIBUTED,

        /**
        * deactivated (FOM name: <code>Deactivated</code>).
        * <br>Description from the FOM: <i>Specifies whether or not the object has been deactivated (it has ceased to exist in the synthetic environment)</i>
        */
         DEACTIVATED,

        /**
        * smoking (FOM name: <code>Smoking</code>).
        * <br>Description from the FOM: <i>Specifies whether or not the object is smoking (creating a smoke plume)</i>
        */
         SMOKING,

        /**
        * flaming (FOM name: <code>Flaming</code>).
        * <br>Description from the FOM: <i>Specifies whether or not the object is aflame</i>
        */
         FLAMING,

        /**
        * objectIdentifier (FOM name: <code>ObjectIdentifier</code>).
        * <br>Description from the FOM: <i>Identifies this EnvironmentObject instance (point, linear or areal)</i>
        */
         OBJECT_IDENTIFIER,

        /**
        * referencedObjectIdentifier (FOM name: <code>ReferencedObjectIdentifier</code>).
        * <br>Description from the FOM: <i>Identifies the Synthetic Environment object instance to which this EnvironmentObject instance is associated</i>
        */
         REFERENCED_OBJECT_IDENTIFIER,

        /**
        * forceIdentifier (FOM name: <code>ForceIdentifier</code>).
        * <br>Description from the FOM: <i>Identifies the force that created or modified this EnvironmentObject instance</i>
        */
         FORCE_IDENTIFIER,

        /**
        * objectType (FOM name: <code>ObjectType</code>).
        * <br>Description from the FOM: <i>Identifies the type of this EnvironmentObject instance</i>
        */
         OBJECT_TYPE
      };

     /**
      * Gets the name of the attribute.
      *
      * @return The name of the attribute. An empty string will be returned if the attribute does not exist.
      */
      LIBAPI virtual std::wstring getName(Attribute attribute);

     /**
      * Finds the attribute specified in the parameter attributeName.
      * The found enumeration will be stored in the parameter attribute.
      *
      * @return true if the attribute was found, false otherwise.
      */
      LIBAPI virtual bool find(Attribute& attribute, std::wstring attributeName);

      LIBAPI virtual ~HlaBurstPointObjectAttributes() {}
    
      /**
      * Returns true if the <code>percentOpacity</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies the opacity of the smoke</i>
      *
      * @return true if <code>percentOpacity</code> is available.
      */
      LIBAPI virtual bool hasPercentOpacity() = 0;

      /**
      * Gets the value of the <code>percentOpacity</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies the opacity of the smoke</i>
      * <br>Description of the data type from the FOM: <i>Percentage [unit: percent (%), resolution: 1, accuracy: perfect]</i>
      *
      * @return the <code>percentOpacity</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual unsigned int getPercentOpacity()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>percentOpacity</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies the opacity of the smoke</i>
      * <br>Description of the data type from the FOM: <i>Percentage [unit: percent (%), resolution: 1, accuracy: perfect]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>percentOpacity</code> attribute.
      */
      LIBAPI virtual unsigned int getPercentOpacity(unsigned int defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>percentOpacity</code> attribute.
      * <br>Description from the FOM: <i>Specifies the opacity of the smoke</i>
      * <br>Description of the data type from the FOM: <i>Percentage [unit: percent (%), resolution: 1, accuracy: perfect]</i>
      *
      * @return the time stamped <code>percentOpacity</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< unsigned int > getPercentOpacityTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>cylinderSize</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies the radius of the cylinder approximating an individual smoke burst ; for multiple bursts, the center bottom of each cylinder is calculated based on the model used to represent the multiple bursts</i>
      *
      * @return true if <code>cylinderSize</code> is available.
      */
      LIBAPI virtual bool hasCylinderSize() = 0;

      /**
      * Gets the value of the <code>cylinderSize</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies the radius of the cylinder approximating an individual smoke burst ; for multiple bursts, the center bottom of each cylinder is calculated based on the model used to represent the multiple bursts</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the <code>cylinderSize</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual unsigned int getCylinderSize()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>cylinderSize</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies the radius of the cylinder approximating an individual smoke burst ; for multiple bursts, the center bottom of each cylinder is calculated based on the model used to represent the multiple bursts</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>cylinderSize</code> attribute.
      */
      LIBAPI virtual unsigned int getCylinderSize(unsigned int defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>cylinderSize</code> attribute.
      * <br>Description from the FOM: <i>Specifies the radius of the cylinder approximating an individual smoke burst ; for multiple bursts, the center bottom of each cylinder is calculated based on the model used to represent the multiple bursts</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the time stamped <code>cylinderSize</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< unsigned int > getCylinderSizeTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>cylinderHeight</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies the height of the cylinder approximating an individual smoke burst ; for multiple bursts, the center bottom of each cylinder is calculated based on the model used to represent the multiple bursts</i>
      *
      * @return true if <code>cylinderHeight</code> is available.
      */
      LIBAPI virtual bool hasCylinderHeight() = 0;

      /**
      * Gets the value of the <code>cylinderHeight</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies the height of the cylinder approximating an individual smoke burst ; for multiple bursts, the center bottom of each cylinder is calculated based on the model used to represent the multiple bursts</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the <code>cylinderHeight</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual unsigned int getCylinderHeight()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>cylinderHeight</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies the height of the cylinder approximating an individual smoke burst ; for multiple bursts, the center bottom of each cylinder is calculated based on the model used to represent the multiple bursts</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>cylinderHeight</code> attribute.
      */
      LIBAPI virtual unsigned int getCylinderHeight(unsigned int defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>cylinderHeight</code> attribute.
      * <br>Description from the FOM: <i>Specifies the height of the cylinder approximating an individual smoke burst ; for multiple bursts, the center bottom of each cylinder is calculated based on the model used to represent the multiple bursts</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the time stamped <code>cylinderHeight</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< unsigned int > getCylinderHeightTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>numberOfBursts</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies the number of bursts in the instance of tactical smoke</i>
      *
      * @return true if <code>numberOfBursts</code> is available.
      */
      LIBAPI virtual bool hasNumberOfBursts() = 0;

      /**
      * Gets the value of the <code>numberOfBursts</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies the number of bursts in the instance of tactical smoke</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the <code>numberOfBursts</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual unsigned int getNumberOfBursts()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>numberOfBursts</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies the number of bursts in the instance of tactical smoke</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>numberOfBursts</code> attribute.
      */
      LIBAPI virtual unsigned int getNumberOfBursts(unsigned int defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>numberOfBursts</code> attribute.
      * <br>Description from the FOM: <i>Specifies the number of bursts in the instance of tactical smoke</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the time stamped <code>numberOfBursts</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< unsigned int > getNumberOfBurstsTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>chemicalContent</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies the chemical content of the smoke</i>
      *
      * @return true if <code>chemicalContent</code> is available.
      */
      LIBAPI virtual bool hasChemicalContent() = 0;

      /**
      * Gets the value of the <code>chemicalContent</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies the chemical content of the smoke</i>
      * <br>Description of the data type from the FOM: <i>Smoke chemical content</i>
      *
      * @return the <code>chemicalContent</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::ChemicalContentEnum::ChemicalContentEnum getChemicalContent()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>chemicalContent</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies the chemical content of the smoke</i>
      * <br>Description of the data type from the FOM: <i>Smoke chemical content</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>chemicalContent</code> attribute.
      */
      LIBAPI virtual DevStudio::ChemicalContentEnum::ChemicalContentEnum getChemicalContent(DevStudio::ChemicalContentEnum::ChemicalContentEnum defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>chemicalContent</code> attribute.
      * <br>Description from the FOM: <i>Specifies the chemical content of the smoke</i>
      * <br>Description of the data type from the FOM: <i>Smoke chemical content</i>
      *
      * @return the time stamped <code>chemicalContent</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::ChemicalContentEnum::ChemicalContentEnum > getChemicalContentTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>location</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies the location of the object based on x, y and z coordinates</i>
      *
      * @return true if <code>location</code> is available.
      */
      LIBAPI virtual bool hasLocation() = 0;

      /**
      * Gets the value of the <code>location</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies the location of the object based on x, y and z coordinates</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @return the <code>location</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::WorldLocationStruct getLocation()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>location</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies the location of the object based on x, y and z coordinates</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>location</code> attribute.
      */
      LIBAPI virtual DevStudio::WorldLocationStruct getLocation(DevStudio::WorldLocationStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>location</code> attribute.
      * <br>Description from the FOM: <i>Specifies the location of the object based on x, y and z coordinates</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @return the time stamped <code>location</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::WorldLocationStruct > getLocationTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>orientation</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies the angles of rotation around the coordinate axis between the object's attitude and the reference coordinate system axes ; these are calculated as the Tait-Bryan Euler angles, specifying the successive rotations needed to transform from the world coordinate system to the object coordinate system</i>
      *
      * @return true if <code>orientation</code> is available.
      */
      LIBAPI virtual bool hasOrientation() = 0;

      /**
      * Gets the value of the <code>orientation</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies the angles of rotation around the coordinate axis between the object's attitude and the reference coordinate system axes ; these are calculated as the Tait-Bryan Euler angles, specifying the successive rotations needed to transform from the world coordinate system to the object coordinate system</i>
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @return the <code>orientation</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::OrientationStruct getOrientation()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>orientation</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies the angles of rotation around the coordinate axis between the object's attitude and the reference coordinate system axes ; these are calculated as the Tait-Bryan Euler angles, specifying the successive rotations needed to transform from the world coordinate system to the object coordinate system</i>
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>orientation</code> attribute.
      */
      LIBAPI virtual DevStudio::OrientationStruct getOrientation(DevStudio::OrientationStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>orientation</code> attribute.
      * <br>Description from the FOM: <i>Specifies the angles of rotation around the coordinate axis between the object's attitude and the reference coordinate system axes ; these are calculated as the Tait-Bryan Euler angles, specifying the successive rotations needed to transform from the world coordinate system to the object coordinate system</i>
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @return the time stamped <code>orientation</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::OrientationStruct > getOrientationTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>percentComplete</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies the percent completion of the object</i>
      *
      * @return true if <code>percentComplete</code> is available.
      */
      LIBAPI virtual bool hasPercentComplete() = 0;

      /**
      * Gets the value of the <code>percentComplete</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies the percent completion of the object</i>
      * <br>Description of the data type from the FOM: <i>Percentage [unit: percent (%), resolution: 1, accuracy: perfect]</i>
      *
      * @return the <code>percentComplete</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual unsigned int getPercentComplete()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>percentComplete</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies the percent completion of the object</i>
      * <br>Description of the data type from the FOM: <i>Percentage [unit: percent (%), resolution: 1, accuracy: perfect]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>percentComplete</code> attribute.
      */
      LIBAPI virtual unsigned int getPercentComplete(unsigned int defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>percentComplete</code> attribute.
      * <br>Description from the FOM: <i>Specifies the percent completion of the object</i>
      * <br>Description of the data type from the FOM: <i>Percentage [unit: percent (%), resolution: 1, accuracy: perfect]</i>
      *
      * @return the time stamped <code>percentComplete</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< unsigned int > getPercentCompleteTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>damagedAppearance</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies the damaged appearance of the object instance</i>
      *
      * @return true if <code>damagedAppearance</code> is available.
      */
      LIBAPI virtual bool hasDamagedAppearance() = 0;

      /**
      * Gets the value of the <code>damagedAppearance</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies the damaged appearance of the object instance</i>
      * <br>Description of the data type from the FOM: <i>Damaged appearance</i>
      *
      * @return the <code>damagedAppearance</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::DamageStatusEnum::DamageStatusEnum getDamagedAppearance()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>damagedAppearance</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies the damaged appearance of the object instance</i>
      * <br>Description of the data type from the FOM: <i>Damaged appearance</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>damagedAppearance</code> attribute.
      */
      LIBAPI virtual DevStudio::DamageStatusEnum::DamageStatusEnum getDamagedAppearance(DevStudio::DamageStatusEnum::DamageStatusEnum defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>damagedAppearance</code> attribute.
      * <br>Description from the FOM: <i>Specifies the damaged appearance of the object instance</i>
      * <br>Description of the data type from the FOM: <i>Damaged appearance</i>
      *
      * @return the time stamped <code>damagedAppearance</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::DamageStatusEnum::DamageStatusEnum > getDamagedAppearanceTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>objectPreDistributed</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies whether or not the object was created before the start of the exercise</i>
      *
      * @return true if <code>objectPreDistributed</code> is available.
      */
      LIBAPI virtual bool hasObjectPreDistributed() = 0;

      /**
      * Gets the value of the <code>objectPreDistributed</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies whether or not the object was created before the start of the exercise</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>objectPreDistributed</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual bool getObjectPreDistributed()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>objectPreDistributed</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies whether or not the object was created before the start of the exercise</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>objectPreDistributed</code> attribute.
      */
      LIBAPI virtual bool getObjectPreDistributed(bool defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>objectPreDistributed</code> attribute.
      * <br>Description from the FOM: <i>Specifies whether or not the object was created before the start of the exercise</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>objectPreDistributed</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< bool > getObjectPreDistributedTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>deactivated</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies whether or not the object has been deactivated (it has ceased to exist in the synthetic environment)</i>
      *
      * @return true if <code>deactivated</code> is available.
      */
      LIBAPI virtual bool hasDeactivated() = 0;

      /**
      * Gets the value of the <code>deactivated</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies whether or not the object has been deactivated (it has ceased to exist in the synthetic environment)</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>deactivated</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual bool getDeactivated()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>deactivated</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies whether or not the object has been deactivated (it has ceased to exist in the synthetic environment)</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>deactivated</code> attribute.
      */
      LIBAPI virtual bool getDeactivated(bool defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>deactivated</code> attribute.
      * <br>Description from the FOM: <i>Specifies whether or not the object has been deactivated (it has ceased to exist in the synthetic environment)</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>deactivated</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< bool > getDeactivatedTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>smoking</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies whether or not the object is smoking (creating a smoke plume)</i>
      *
      * @return true if <code>smoking</code> is available.
      */
      LIBAPI virtual bool hasSmoking() = 0;

      /**
      * Gets the value of the <code>smoking</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies whether or not the object is smoking (creating a smoke plume)</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>smoking</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual bool getSmoking()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>smoking</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies whether or not the object is smoking (creating a smoke plume)</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>smoking</code> attribute.
      */
      LIBAPI virtual bool getSmoking(bool defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>smoking</code> attribute.
      * <br>Description from the FOM: <i>Specifies whether or not the object is smoking (creating a smoke plume)</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>smoking</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< bool > getSmokingTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>flaming</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies whether or not the object is aflame</i>
      *
      * @return true if <code>flaming</code> is available.
      */
      LIBAPI virtual bool hasFlaming() = 0;

      /**
      * Gets the value of the <code>flaming</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies whether or not the object is aflame</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>flaming</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual bool getFlaming()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>flaming</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies whether or not the object is aflame</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>flaming</code> attribute.
      */
      LIBAPI virtual bool getFlaming(bool defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>flaming</code> attribute.
      * <br>Description from the FOM: <i>Specifies whether or not the object is aflame</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>flaming</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< bool > getFlamingTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>objectIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Identifies this EnvironmentObject instance (point, linear or areal)</i>
      *
      * @return true if <code>objectIdentifier</code> is available.
      */
      LIBAPI virtual bool hasObjectIdentifier() = 0;

      /**
      * Gets the value of the <code>objectIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>Identifies this EnvironmentObject instance (point, linear or areal)</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @return the <code>objectIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::EntityIdentifierStruct getObjectIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>objectIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Identifies this EnvironmentObject instance (point, linear or areal)</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>objectIdentifier</code> attribute.
      */
      LIBAPI virtual DevStudio::EntityIdentifierStruct getObjectIdentifier(DevStudio::EntityIdentifierStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>objectIdentifier</code> attribute.
      * <br>Description from the FOM: <i>Identifies this EnvironmentObject instance (point, linear or areal)</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @return the time stamped <code>objectIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::EntityIdentifierStruct > getObjectIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>referencedObjectIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Identifies the Synthetic Environment object instance to which this EnvironmentObject instance is associated</i>
      *
      * @return true if <code>referencedObjectIdentifier</code> is available.
      */
      LIBAPI virtual bool hasReferencedObjectIdentifier() = 0;

      /**
      * Gets the value of the <code>referencedObjectIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>Identifies the Synthetic Environment object instance to which this EnvironmentObject instance is associated</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @return the <code>referencedObjectIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::string getReferencedObjectIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>referencedObjectIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Identifies the Synthetic Environment object instance to which this EnvironmentObject instance is associated</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>referencedObjectIdentifier</code> attribute.
      */
      LIBAPI virtual std::string getReferencedObjectIdentifier(std::string defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>referencedObjectIdentifier</code> attribute.
      * <br>Description from the FOM: <i>Identifies the Synthetic Environment object instance to which this EnvironmentObject instance is associated</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @return the time stamped <code>referencedObjectIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::string > getReferencedObjectIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>forceIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Identifies the force that created or modified this EnvironmentObject instance</i>
      *
      * @return true if <code>forceIdentifier</code> is available.
      */
      LIBAPI virtual bool hasForceIdentifier() = 0;

      /**
      * Gets the value of the <code>forceIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>Identifies the force that created or modified this EnvironmentObject instance</i>
      * <br>Description of the data type from the FOM: <i>Force ID</i>
      *
      * @return the <code>forceIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::ForceIdentifierEnum::ForceIdentifierEnum getForceIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>forceIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Identifies the force that created or modified this EnvironmentObject instance</i>
      * <br>Description of the data type from the FOM: <i>Force ID</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>forceIdentifier</code> attribute.
      */
      LIBAPI virtual DevStudio::ForceIdentifierEnum::ForceIdentifierEnum getForceIdentifier(DevStudio::ForceIdentifierEnum::ForceIdentifierEnum defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>forceIdentifier</code> attribute.
      * <br>Description from the FOM: <i>Identifies the force that created or modified this EnvironmentObject instance</i>
      * <br>Description of the data type from the FOM: <i>Force ID</i>
      *
      * @return the time stamped <code>forceIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::ForceIdentifierEnum::ForceIdentifierEnum > getForceIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>objectType</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Identifies the type of this EnvironmentObject instance</i>
      *
      * @return true if <code>objectType</code> is available.
      */
      LIBAPI virtual bool hasObjectType() = 0;

      /**
      * Gets the value of the <code>objectType</code> attribute.
      *
      * <br>Description from the FOM: <i>Identifies the type of this EnvironmentObject instance</i>
      * <br>Description of the data type from the FOM: <i>Record specifying the domain, the kind and the specific identification of the environment object</i>
      *
      * @return the <code>objectType</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::EnvironmentObjectTypeStruct getObjectType()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>objectType</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Identifies the type of this EnvironmentObject instance</i>
      * <br>Description of the data type from the FOM: <i>Record specifying the domain, the kind and the specific identification of the environment object</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>objectType</code> attribute.
      */
      LIBAPI virtual DevStudio::EnvironmentObjectTypeStruct getObjectType(DevStudio::EnvironmentObjectTypeStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>objectType</code> attribute.
      * <br>Description from the FOM: <i>Identifies the type of this EnvironmentObject instance</i>
      * <br>Description of the data type from the FOM: <i>Record specifying the domain, the kind and the specific identification of the environment object</i>
      *
      * @return the time stamped <code>objectType</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::EnvironmentObjectTypeStruct > getObjectTypeTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
   };
}
#endif
