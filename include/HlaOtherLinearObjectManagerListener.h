/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAOTHERLINEAROBJECTMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLAOTHERLINEAROBJECTMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaOtherLinearObject.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaOtherLinearObject instances.
    */
    class HlaOtherLinearObjectManagerListener {

    public:

        LIBAPI virtual ~HlaOtherLinearObjectManagerListener() {}

        /**
        * This method is called when a new HlaOtherLinearObject instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param otherLinearObject the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaOtherLinearObjectDiscovered(HlaOtherLinearObjectPtr otherLinearObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaOtherLinearObject instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param otherLinearObject the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaOtherLinearObjectInitialized(HlaOtherLinearObjectPtr otherLinearObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaOtherLinearObjectManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param otherLinearObject the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaOtherLinearObjectInInterest(HlaOtherLinearObjectPtr otherLinearObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaOtherLinearObjectManagerListener instance goes out of interest.
        *
        * @param otherLinearObject the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaOtherLinearObjectOutOfInterest(HlaOtherLinearObjectPtr otherLinearObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaOtherLinearObject instance is deleted.
        *
        * @param otherLinearObject the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaOtherLinearObjectDeleted(HlaOtherLinearObjectPtr otherLinearObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaOtherLinearObjectManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaOtherLinearObjectManagerListener::Adapter : public HlaOtherLinearObjectManagerListener {

    public:
        LIBAPI virtual void hlaOtherLinearObjectDiscovered(HlaOtherLinearObjectPtr otherLinearObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaOtherLinearObjectInitialized(HlaOtherLinearObjectPtr otherLinearObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaOtherLinearObjectInInterest(HlaOtherLinearObjectPtr otherLinearObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaOtherLinearObjectOutOfInterest(HlaOtherLinearObjectPtr otherLinearObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaOtherLinearObjectDeleted(HlaOtherLinearObjectPtr otherLinearObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
