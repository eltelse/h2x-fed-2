/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLACRATEROBJECTMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLACRATEROBJECTMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaCraterObject.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaCraterObject instances.
    */
    class HlaCraterObjectManagerListener {

    public:

        LIBAPI virtual ~HlaCraterObjectManagerListener() {}

        /**
        * This method is called when a new HlaCraterObject instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param craterObject the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaCraterObjectDiscovered(HlaCraterObjectPtr craterObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaCraterObject instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param craterObject the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaCraterObjectInitialized(HlaCraterObjectPtr craterObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaCraterObjectManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param craterObject the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaCraterObjectInInterest(HlaCraterObjectPtr craterObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaCraterObjectManagerListener instance goes out of interest.
        *
        * @param craterObject the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaCraterObjectOutOfInterest(HlaCraterObjectPtr craterObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaCraterObject instance is deleted.
        *
        * @param craterObject the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaCraterObjectDeleted(HlaCraterObjectPtr craterObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaCraterObjectManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaCraterObjectManagerListener::Adapter : public HlaCraterObjectManagerListener {

    public:
        LIBAPI virtual void hlaCraterObjectDiscovered(HlaCraterObjectPtr craterObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaCraterObjectInitialized(HlaCraterObjectPtr craterObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaCraterObjectInInterest(HlaCraterObjectPtr craterObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaCraterObjectOutOfInterest(HlaCraterObjectPtr craterObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaCraterObjectDeleted(HlaCraterObjectPtr craterObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
