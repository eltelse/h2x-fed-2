/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLACULTURALFEATUREMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLACULTURALFEATUREMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaCulturalFeature.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaCulturalFeature instances.
    */
    class HlaCulturalFeatureManagerListener {

    public:

        LIBAPI virtual ~HlaCulturalFeatureManagerListener() {}

        /**
        * This method is called when a new HlaCulturalFeature instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param culturalFeature the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaCulturalFeatureDiscovered(HlaCulturalFeaturePtr culturalFeature, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaCulturalFeature instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param culturalFeature the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaCulturalFeatureInitialized(HlaCulturalFeaturePtr culturalFeature, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaCulturalFeatureManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param culturalFeature the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaCulturalFeatureInInterest(HlaCulturalFeaturePtr culturalFeature, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaCulturalFeatureManagerListener instance goes out of interest.
        *
        * @param culturalFeature the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaCulturalFeatureOutOfInterest(HlaCulturalFeaturePtr culturalFeature, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaCulturalFeature instance is deleted.
        *
        * @param culturalFeature the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaCulturalFeatureDeleted(HlaCulturalFeaturePtr culturalFeature, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaCulturalFeatureManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaCulturalFeatureManagerListener::Adapter : public HlaCulturalFeatureManagerListener {

    public:
        LIBAPI virtual void hlaCulturalFeatureDiscovered(HlaCulturalFeaturePtr culturalFeature, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaCulturalFeatureInitialized(HlaCulturalFeaturePtr culturalFeature, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaCulturalFeatureInInterest(HlaCulturalFeaturePtr culturalFeature, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaCulturalFeatureOutOfInterest(HlaCulturalFeaturePtr culturalFeature, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaCulturalFeatureDeleted(HlaCulturalFeaturePtr culturalFeature, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
