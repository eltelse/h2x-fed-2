/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAMINEFIELDATTRIBUTES_H
#define DEVELOPER_STUDIO_HLAMINEFIELDATTRIBUTES_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <string>
#include <vector>
#include <utility>
    
#include <boost/noncopyable.hpp>
    
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EntityTypeStruct.h>
#include <DevStudio/datatypes/EntityTypeStructLengthlessArray.h>
#include <DevStudio/datatypes/ForceIdentifierEnum.h>
#include <DevStudio/datatypes/MinefieldLaneEnum.h>
#include <DevStudio/datatypes/MinefieldProtocolEnum.h>
#include <DevStudio/datatypes/MinefieldStatusEnum.h>
#include <DevStudio/datatypes/MinefieldTypeEnum.h>
#include <DevStudio/datatypes/OrientationStruct.h>
#include <DevStudio/datatypes/PerimeterPointStruct.h>
#include <DevStudio/datatypes/PerimeterPointStructLengthlessArray.h>
#include <DevStudio/datatypes/WorldLocationStruct.h>
#include <vector>

#include <DevStudio/HlaException.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaHLAobjectRootAttributes.h>
#include <DevStudio/HlaTimeStamped.h>

namespace DevStudio {

   /**
   * Interface used to represent all attributes for an object instance.
   */
   class HlaMinefieldAttributes : public HlaHLAobjectRootAttributes {
   public:


     /**
      * An enumeration of the attributes of an HlaMinefield
      *
      *<table summary="All attributes">
      * <tr><td><b>Enum constant</b></td><td><b>C++ name</b></td><td><b>FOM name</b></td></tr>
      * <tr><td>ACTIVE_STATUS</td><td>activeStatus</td><td><code>ActiveStatus</code></td></tr>
      * <tr><td>FORCE_IDENTIFIER</td><td>forceIdentifier</td><td><code>ForceIdentifier</code></td></tr>
      * <tr><td>LANE</td><td>lane</td><td><code>Lane</code></td></tr>
      * <tr><td>MINEFIELD_APPEARANCE_TYPE</td><td>minefieldAppearanceType</td><td><code>MinefieldAppearanceType</code></td></tr>
      * <tr><td>MINEFIELD_IDENTIFIER</td><td>minefieldIdentifier</td><td><code>MinefieldIdentifier</code></td></tr>
      * <tr><td>MINEFIELD_LOCATION</td><td>minefieldLocation</td><td><code>MinefieldLocation</code></td></tr>
      * <tr><td>MINEFIELD_ORIENTATION</td><td>minefieldOrientation</td><td><code>MinefieldOrientation</code></td></tr>
      * <tr><td>MINEFIELD_TYPE</td><td>minefieldType</td><td><code>MinefieldType</code></td></tr>
      * <tr><td>MINE_TYPES</td><td>mineTypes</td><td><code>MineTypes</code></td></tr>
      * <tr><td>PERIMETER_POINT_COORDINATES</td><td>perimeterPointCoordinates</td><td><code>PerimeterPointCoordinates</code></td></tr>
      * <tr><td>PROTOCOL_MODE</td><td>protocolMode</td><td><code>ProtocolMode</code></td></tr>
      * <tr><td>STATE</td><td>state</td><td><code>State</code></td></tr>
      *</table>
      */
      enum Attribute {
        /**
        * activeStatus (FOM name: <code>ActiveStatus</code>).
        * <br>Description from the FOM: <i>Specifies the active status of the minefield</i>
        */
         ACTIVE_STATUS,

        /**
        * forceIdentifier (FOM name: <code>ForceIdentifier</code>).
        * <br>Description from the FOM: <i>Identifies the force to which the minefield belongs</i>
        */
         FORCE_IDENTIFIER,

        /**
        * lane (FOM name: <code>Lane</code>).
        * <br>Description from the FOM: <i>Specifies whether or not the minefield has an active lane</i>
        */
         LANE,

        /**
        * minefieldAppearanceType (FOM name: <code>MinefieldAppearanceType</code>).
        * <br>Description from the FOM: <i>Specifies the appearance information needed for displaying the symbology of the minefield</i>
        */
         MINEFIELD_APPEARANCE_TYPE,

        /**
        * minefieldIdentifier (FOM name: <code>MinefieldIdentifier</code>).
        * <br>Description from the FOM: <i>Uniquely identifies this minefield instance in association with the federate's site and application</i>
        */
         MINEFIELD_IDENTIFIER,

        /**
        * minefieldLocation (FOM name: <code>MinefieldLocation</code>).
        * <br>Description from the FOM: <i>Specifies the location of the center of the minefield</i>
        */
         MINEFIELD_LOCATION,

        /**
        * minefieldOrientation (FOM name: <code>MinefieldOrientation</code>).
        * <br>Description from the FOM: <i>Specifies the orientation of the minefield, with Euler angles</i>
        */
         MINEFIELD_ORIENTATION,

        /**
        * minefieldType (FOM name: <code>MinefieldType</code>).
        * <br>Description from the FOM: <i>Specifies the minefield type</i>
        */
         MINEFIELD_TYPE,

        /**
        * mineTypes (FOM name: <code>MineTypes</code>).
        * <br>Description from the FOM: <i>Specifies the type of each mine contained within the minefield</i>
        */
         MINE_TYPES,

        /**
        * perimeterPointCoordinates (FOM name: <code>PerimeterPointCoordinates</code>).
        * <br>Description from the FOM: <i>Specifies the location of each perimeter point, relative to the minefield location</i>
        */
         PERIMETER_POINT_COORDINATES,

        /**
        * protocolMode (FOM name: <code>ProtocolMode</code>).
        * <br>Description from the FOM: <i>Specifies the mode (Heartbeat or Query Response Protocol) being used to communicate data about the minefield</i>
        */
         PROTOCOL_MODE,

        /**
        * state (FOM name: <code>State</code>).
        * <br>Description from the FOM: <i>Specifies whether or not the minefield has been deactivated</i>
        */
         STATE
      };

     /**
      * Gets the name of the attribute.
      *
      * @return The name of the attribute. An empty string will be returned if the attribute does not exist.
      */
      LIBAPI virtual std::wstring getName(Attribute attribute);

     /**
      * Finds the attribute specified in the parameter attributeName.
      * The found enumeration will be stored in the parameter attribute.
      *
      * @return true if the attribute was found, false otherwise.
      */
      LIBAPI virtual bool find(Attribute& attribute, std::wstring attributeName);

      LIBAPI virtual ~HlaMinefieldAttributes() {}
    
      /**
      * Returns true if the <code>activeStatus</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies the active status of the minefield</i>
      *
      * @return true if <code>activeStatus</code> is available.
      */
      LIBAPI virtual bool hasActiveStatus() = 0;

      /**
      * Gets the value of the <code>activeStatus</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies the active status of the minefield</i>
      * <br>Description of the data type from the FOM: <i>Minefield status</i>
      *
      * @return the <code>activeStatus</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::MinefieldStatusEnum::MinefieldStatusEnum getActiveStatus()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>activeStatus</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies the active status of the minefield</i>
      * <br>Description of the data type from the FOM: <i>Minefield status</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>activeStatus</code> attribute.
      */
      LIBAPI virtual DevStudio::MinefieldStatusEnum::MinefieldStatusEnum getActiveStatus(DevStudio::MinefieldStatusEnum::MinefieldStatusEnum defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>activeStatus</code> attribute.
      * <br>Description from the FOM: <i>Specifies the active status of the minefield</i>
      * <br>Description of the data type from the FOM: <i>Minefield status</i>
      *
      * @return the time stamped <code>activeStatus</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::MinefieldStatusEnum::MinefieldStatusEnum > getActiveStatusTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>forceIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Identifies the force to which the minefield belongs</i>
      *
      * @return true if <code>forceIdentifier</code> is available.
      */
      LIBAPI virtual bool hasForceIdentifier() = 0;

      /**
      * Gets the value of the <code>forceIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>Identifies the force to which the minefield belongs</i>
      * <br>Description of the data type from the FOM: <i>Force ID</i>
      *
      * @return the <code>forceIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::ForceIdentifierEnum::ForceIdentifierEnum getForceIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>forceIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Identifies the force to which the minefield belongs</i>
      * <br>Description of the data type from the FOM: <i>Force ID</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>forceIdentifier</code> attribute.
      */
      LIBAPI virtual DevStudio::ForceIdentifierEnum::ForceIdentifierEnum getForceIdentifier(DevStudio::ForceIdentifierEnum::ForceIdentifierEnum defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>forceIdentifier</code> attribute.
      * <br>Description from the FOM: <i>Identifies the force to which the minefield belongs</i>
      * <br>Description of the data type from the FOM: <i>Force ID</i>
      *
      * @return the time stamped <code>forceIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::ForceIdentifierEnum::ForceIdentifierEnum > getForceIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>lane</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies whether or not the minefield has an active lane</i>
      *
      * @return true if <code>lane</code> is available.
      */
      LIBAPI virtual bool hasLane() = 0;

      /**
      * Gets the value of the <code>lane</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies whether or not the minefield has an active lane</i>
      * <br>Description of the data type from the FOM: <i>Minefield lane status</i>
      *
      * @return the <code>lane</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::MinefieldLaneEnum::MinefieldLaneEnum getLane()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>lane</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies whether or not the minefield has an active lane</i>
      * <br>Description of the data type from the FOM: <i>Minefield lane status</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>lane</code> attribute.
      */
      LIBAPI virtual DevStudio::MinefieldLaneEnum::MinefieldLaneEnum getLane(DevStudio::MinefieldLaneEnum::MinefieldLaneEnum defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>lane</code> attribute.
      * <br>Description from the FOM: <i>Specifies whether or not the minefield has an active lane</i>
      * <br>Description of the data type from the FOM: <i>Minefield lane status</i>
      *
      * @return the time stamped <code>lane</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::MinefieldLaneEnum::MinefieldLaneEnum > getLaneTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>minefieldAppearanceType</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies the appearance information needed for displaying the symbology of the minefield</i>
      *
      * @return true if <code>minefieldAppearanceType</code> is available.
      */
      LIBAPI virtual bool hasMinefieldAppearanceType() = 0;

      /**
      * Gets the value of the <code>minefieldAppearanceType</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies the appearance information needed for displaying the symbology of the minefield</i>
      * <br>Description of the data type from the FOM: <i>Minefield type</i>
      *
      * @return the <code>minefieldAppearanceType</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::MinefieldTypeEnum::MinefieldTypeEnum getMinefieldAppearanceType()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>minefieldAppearanceType</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies the appearance information needed for displaying the symbology of the minefield</i>
      * <br>Description of the data type from the FOM: <i>Minefield type</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>minefieldAppearanceType</code> attribute.
      */
      LIBAPI virtual DevStudio::MinefieldTypeEnum::MinefieldTypeEnum getMinefieldAppearanceType(DevStudio::MinefieldTypeEnum::MinefieldTypeEnum defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>minefieldAppearanceType</code> attribute.
      * <br>Description from the FOM: <i>Specifies the appearance information needed for displaying the symbology of the minefield</i>
      * <br>Description of the data type from the FOM: <i>Minefield type</i>
      *
      * @return the time stamped <code>minefieldAppearanceType</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::MinefieldTypeEnum::MinefieldTypeEnum > getMinefieldAppearanceTypeTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>minefieldIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Uniquely identifies this minefield instance in association with the federate's site and application</i>
      *
      * @return true if <code>minefieldIdentifier</code> is available.
      */
      LIBAPI virtual bool hasMinefieldIdentifier() = 0;

      /**
      * Gets the value of the <code>minefieldIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>Uniquely identifies this minefield instance in association with the federate's site and application</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @return the <code>minefieldIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::EntityIdentifierStruct getMinefieldIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>minefieldIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Uniquely identifies this minefield instance in association with the federate's site and application</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>minefieldIdentifier</code> attribute.
      */
      LIBAPI virtual DevStudio::EntityIdentifierStruct getMinefieldIdentifier(DevStudio::EntityIdentifierStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>minefieldIdentifier</code> attribute.
      * <br>Description from the FOM: <i>Uniquely identifies this minefield instance in association with the federate's site and application</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @return the time stamped <code>minefieldIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::EntityIdentifierStruct > getMinefieldIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>minefieldLocation</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies the location of the center of the minefield</i>
      *
      * @return true if <code>minefieldLocation</code> is available.
      */
      LIBAPI virtual bool hasMinefieldLocation() = 0;

      /**
      * Gets the value of the <code>minefieldLocation</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies the location of the center of the minefield</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @return the <code>minefieldLocation</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::WorldLocationStruct getMinefieldLocation()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>minefieldLocation</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies the location of the center of the minefield</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>minefieldLocation</code> attribute.
      */
      LIBAPI virtual DevStudio::WorldLocationStruct getMinefieldLocation(DevStudio::WorldLocationStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>minefieldLocation</code> attribute.
      * <br>Description from the FOM: <i>Specifies the location of the center of the minefield</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @return the time stamped <code>minefieldLocation</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::WorldLocationStruct > getMinefieldLocationTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>minefieldOrientation</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies the orientation of the minefield, with Euler angles</i>
      *
      * @return true if <code>minefieldOrientation</code> is available.
      */
      LIBAPI virtual bool hasMinefieldOrientation() = 0;

      /**
      * Gets the value of the <code>minefieldOrientation</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies the orientation of the minefield, with Euler angles</i>
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @return the <code>minefieldOrientation</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::OrientationStruct getMinefieldOrientation()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>minefieldOrientation</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies the orientation of the minefield, with Euler angles</i>
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>minefieldOrientation</code> attribute.
      */
      LIBAPI virtual DevStudio::OrientationStruct getMinefieldOrientation(DevStudio::OrientationStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>minefieldOrientation</code> attribute.
      * <br>Description from the FOM: <i>Specifies the orientation of the minefield, with Euler angles</i>
      * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @return the time stamped <code>minefieldOrientation</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::OrientationStruct > getMinefieldOrientationTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>minefieldType</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies the minefield type</i>
      *
      * @return true if <code>minefieldType</code> is available.
      */
      LIBAPI virtual bool hasMinefieldType() = 0;

      /**
      * Gets the value of the <code>minefieldType</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies the minefield type</i>
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      *
      * @return the <code>minefieldType</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::EntityTypeStruct getMinefieldType()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>minefieldType</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies the minefield type</i>
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>minefieldType</code> attribute.
      */
      LIBAPI virtual DevStudio::EntityTypeStruct getMinefieldType(DevStudio::EntityTypeStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>minefieldType</code> attribute.
      * <br>Description from the FOM: <i>Specifies the minefield type</i>
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      *
      * @return the time stamped <code>minefieldType</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::EntityTypeStruct > getMinefieldTypeTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>mineTypes</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies the type of each mine contained within the minefield</i>
      *
      * @return true if <code>mineTypes</code> is available.
      */
      LIBAPI virtual bool hasMineTypes() = 0;

      /**
      * Gets the value of the <code>mineTypes</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies the type of each mine contained within the minefield</i>
      * <br>Description of the data type from the FOM: <i>Dynamic array of EntityTypeStruct elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
      *
      * @return the <code>mineTypes</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::vector<DevStudio::EntityTypeStruct > getMineTypes()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>mineTypes</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies the type of each mine contained within the minefield</i>
      * <br>Description of the data type from the FOM: <i>Dynamic array of EntityTypeStruct elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>mineTypes</code> attribute.
      */
      LIBAPI virtual std::vector<DevStudio::EntityTypeStruct > getMineTypes(std::vector<DevStudio::EntityTypeStruct > defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>mineTypes</code> attribute.
      * <br>Description from the FOM: <i>Specifies the type of each mine contained within the minefield</i>
      * <br>Description of the data type from the FOM: <i>Dynamic array of EntityTypeStruct elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
      *
      * @return the time stamped <code>mineTypes</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::vector<DevStudio::EntityTypeStruct > > getMineTypesTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>perimeterPointCoordinates</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies the location of each perimeter point, relative to the minefield location</i>
      *
      * @return true if <code>perimeterPointCoordinates</code> is available.
      */
      LIBAPI virtual bool hasPerimeterPointCoordinates() = 0;

      /**
      * Gets the value of the <code>perimeterPointCoordinates</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies the location of each perimeter point, relative to the minefield location</i>
      * <br>Description of the data type from the FOM: <i>Specifies the location of perimeter points (collection)</i>
      *
      * @return the <code>perimeterPointCoordinates</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::vector<DevStudio::PerimeterPointStruct > getPerimeterPointCoordinates()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>perimeterPointCoordinates</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies the location of each perimeter point, relative to the minefield location</i>
      * <br>Description of the data type from the FOM: <i>Specifies the location of perimeter points (collection)</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>perimeterPointCoordinates</code> attribute.
      */
      LIBAPI virtual std::vector<DevStudio::PerimeterPointStruct > getPerimeterPointCoordinates(std::vector<DevStudio::PerimeterPointStruct > defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>perimeterPointCoordinates</code> attribute.
      * <br>Description from the FOM: <i>Specifies the location of each perimeter point, relative to the minefield location</i>
      * <br>Description of the data type from the FOM: <i>Specifies the location of perimeter points (collection)</i>
      *
      * @return the time stamped <code>perimeterPointCoordinates</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::vector<DevStudio::PerimeterPointStruct > > getPerimeterPointCoordinatesTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>protocolMode</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies the mode (Heartbeat or Query Response Protocol) being used to communicate data about the minefield</i>
      *
      * @return true if <code>protocolMode</code> is available.
      */
      LIBAPI virtual bool hasProtocolMode() = 0;

      /**
      * Gets the value of the <code>protocolMode</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies the mode (Heartbeat or Query Response Protocol) being used to communicate data about the minefield</i>
      * <br>Description of the data type from the FOM: <i>Minefield communication protocol mode</i>
      *
      * @return the <code>protocolMode</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::MinefieldProtocolEnum::MinefieldProtocolEnum getProtocolMode()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>protocolMode</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies the mode (Heartbeat or Query Response Protocol) being used to communicate data about the minefield</i>
      * <br>Description of the data type from the FOM: <i>Minefield communication protocol mode</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>protocolMode</code> attribute.
      */
      LIBAPI virtual DevStudio::MinefieldProtocolEnum::MinefieldProtocolEnum getProtocolMode(DevStudio::MinefieldProtocolEnum::MinefieldProtocolEnum defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>protocolMode</code> attribute.
      * <br>Description from the FOM: <i>Specifies the mode (Heartbeat or Query Response Protocol) being used to communicate data about the minefield</i>
      * <br>Description of the data type from the FOM: <i>Minefield communication protocol mode</i>
      *
      * @return the time stamped <code>protocolMode</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::MinefieldProtocolEnum::MinefieldProtocolEnum > getProtocolModeTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>state</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies whether or not the minefield has been deactivated</i>
      *
      * @return true if <code>state</code> is available.
      */
      LIBAPI virtual bool hasState() = 0;

      /**
      * Gets the value of the <code>state</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies whether or not the minefield has been deactivated</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>state</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual bool getState()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>state</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies whether or not the minefield has been deactivated</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>state</code> attribute.
      */
      LIBAPI virtual bool getState(bool defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>state</code> attribute.
      * <br>Description from the FOM: <i>Specifies whether or not the minefield has been deactivated</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>state</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< bool > getStateTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
   };
}
#endif
