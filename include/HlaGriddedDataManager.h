/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAGRIDDEDDATAMANAGER_H
#define DEVELOPER_STUDIO_HLAGRIDDEDDATAMANAGER_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EnvironmentDataCoordinateSystemEnum.h>
#include <DevStudio/datatypes/EnvironmentGridTypeEnum.h>
#include <DevStudio/datatypes/EnvironmentTypeStruct.h>
#include <DevStudio/datatypes/GridAxisStruct.h>
#include <DevStudio/datatypes/GridAxisStructLengthlessArray.h>
#include <DevStudio/datatypes/GridDataStruct.h>
#include <DevStudio/datatypes/GridDataStructLengthlessArray.h>
#include <DevStudio/datatypes/OrientationStruct.h>
#include <vector>

#include <list>
#include <vector>

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaGriddedDataManagerListener.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaException.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

   /**
   * Manager used to manage HlaGriddedDatas.
   */
   class HlaGriddedDataManager : private boost::noncopyable {

   public:

      LIBAPI virtual ~HlaGriddedDataManager() {}

      /**
      * Gets a list of all HlaGriddedDatas within interest, both local and remote.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of HlaGriddedDatas
      */
      LIBAPI virtual std::list<HlaGriddedDataPtr> getHlaGriddedDatas() = 0;

      /**
      * Gets a list of all HlaGriddedDatas, both local and remote.
      * HlaGriddedDatas not within interest are also included.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of all HlaGriddedDatas
      */
      LIBAPI virtual std::list<HlaGriddedDataPtr> getAllHlaGriddedDatas() = 0;

      /**
      * Gets a list of local HlaGriddedDatas within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of local HlaGriddedDatas
      */
      LIBAPI virtual std::list<HlaGriddedDataPtr> getLocalHlaGriddedDatas() = 0;

      /**
      * Gets a list of remote HlaGriddedDatas within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of remote HlaGriddedDatas
      */
      LIBAPI virtual std::list<HlaGriddedDataPtr> getRemoteHlaGriddedDatas() = 0;

      /**
      * Find a HlaGriddedData with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> of the HlaGriddedData to find
      *
      * @return the specified HlaGriddedData, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaGriddedDataPtr getGriddedDataByHlaInstanceName(const std::wstring& hlaInstanceName) = 0;

      /**
      * Find a HlaGriddedData with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param encodedHlaInstanceHandle the encoded <code>HLA instance handle</code> of the HlaGriddedData to find
      *
      * @return the specified HlaGriddedData, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaGriddedDataPtr getGriddedDataByHlaInstanceHandle(const std::vector<char>& encodedHlaInstanceHandle) = 0;

      /**
      * Creates a new HlaGriddedData, and sets all <i>Create</i> attributes.
      *
      * @return Returns a new local HlaGriddedData.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaGriddedDataPtr createLocalHlaGriddedData(
      ) THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Creates a new HlaGriddedData with a specified <code>HLA instance name</code>, and sets all
      * <i>Create</i> attributes.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> to register for this instance
      *
      * @return Returns a new local HlaGriddedData.
      *
      * @throws HlaIllegalInstanceNameException if the name is illegal
      * @throws HlaInstanceNameInUseException if the name is already in use
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaGriddedDataPtr createLocalHlaGriddedData(
         const std::wstring& hlaInstanceName
      ) THROW_SPEC (HlaIllegalInstanceNameException, HlaInstanceNameInUseException,
                    HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaGriddedData and removes it from the federation.
      *
      * @param griddedData The HlaGriddedData to delete
      *
      * @return the HlaGriddedData deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaGriddedDataPtr deleteLocalHlaGriddedData(HlaGriddedDataPtr griddedData)
          THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                      HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaGriddedData and removes it from the federation.
      *
      * @param griddedData The HlaGriddedData to delete
      * @param timestamp timestamp when the instance was removed.
      *
      * @return the HlaGriddedData deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaGriddedDataPtr deleteLocalHlaGriddedData(HlaGriddedDataPtr griddedData, HlaTimeStampPtr timestamp)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaGriddedData and removes it from the federation.
      *
      * @param griddedData The HlaGriddedData to delete
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaGriddedData deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaGriddedDataPtr deleteLocalHlaGriddedData(HlaGriddedDataPtr griddedData, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaGriddedData and removes it from the federation.
      *
      * @param griddedData The HlaGriddedData to delete
      * @param timestamp timestamp when the instance was removed.
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaGriddedData deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaGriddedDataPtr deleteLocalHlaGriddedData(HlaGriddedDataPtr griddedData, HlaTimeStampPtr timestamp, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Add a HlaGriddedData manager listener.
      *
      * @param managerListener listener to add
      */
      LIBAPI virtual void addHlaGriddedDataManagerListener(HlaGriddedDataManagerListenerPtr managerListener) = 0;

      /**
      * Removed a HlaGriddedData manager listener.
      *
      * @param managerListener listener to remove
      */
      LIBAPI virtual void removeHlaGriddedDataManagerListener(HlaGriddedDataManagerListenerPtr managerListener) = 0;

     /**
      * Add a listener that will automatically be added to all instances of HlaGriddedData (Both local and remote).
      * The listener is notified when any attribute of an instance of HlaGriddedData is updated.
      * The listener is also called when an interaction is sent to an instance of HlaGriddedData.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param listener The listener to add.
      */
      LIBAPI virtual void addHlaGriddedDataDefaultInstanceListener(HlaGriddedDataListenerPtr listener) = 0;

     /**
      * Remove a listener from the set of listeners that are automatically added to new instances of HlaGriddedData.
      * Note: The listener will not be removed from already existing instances of HlaGriddedData.
      * This method is idempotent.
      *
      * @param listener The listener to remove.
      */
      LIBAPI virtual void removeHlaGriddedDataDefaultInstanceListener(HlaGriddedDataListenerPtr listener) = 0;

     /**
      * Add a valueListener that will automatically be added to all instances of HlaGriddedData (Both local and remote).
      * The  Valuelistener is notified when any attribute of an instance of HlaGriddedData is updated.
      * Note that the attribute values that are notified are not guaranteed to
      * be the latest available values for that attribute.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to add.
      */
      LIBAPI virtual void addHlaGriddedDataDefaultInstanceValueListener(HlaGriddedDataValueListenerPtr valueListener) = 0;

     /**
      * Remove a valueListener from the set of listeners that are automatically added to new instances of HlaGriddedData.
      * Note: The valueListener will not be removed from already existing instances of HlaGriddedData.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to remove.
      */
      LIBAPI virtual void removeHlaGriddedDataDefaultInstanceValueListener(HlaGriddedDataValueListenerPtr valueListener) = 0;

     /**
      * Enables or disables the HlaGriddedData manager.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaGriddedData manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      *
      * @param enabled <code>true</code> to enable the HlaGriddedData manager, otherwise <code>false</code>
      */
      LIBAPI virtual void setEnabled(bool enabled) = 0;

     /**
      * Enables the HlaGriddedData manager if it is avalable in the FOM at runtime.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaGriddedData manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      */
      LIBAPI virtual void setEnabledIfAvailableInFom() = 0;

     /**
      * Check if the HlaGriddedData manager is actually enabled when connected.
      * An HlaGriddedData manager may be disabled with the <code>setEnabled(false)</code> function, or it may be configured
      * as <i>if available in the FOM</i> by the <code>setEnabledIfAvailableInFom()</code> function and
      * then not available in the FOM at runtime.
      * <p>
      * This method should only be used when connected, it will always return <code>false</code>
      * when not connected.
      *
      * @return <code>true</code> if connected and the HlaGriddedData manager is enabled.
      */
      LIBAPI virtual bool isEnabled() = 0;
   };
}
#endif
