/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAOTHERAREALOBJECTMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLAOTHERAREALOBJECTMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaOtherArealObject.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaOtherArealObject instances.
    */
    class HlaOtherArealObjectManagerListener {

    public:

        LIBAPI virtual ~HlaOtherArealObjectManagerListener() {}

        /**
        * This method is called when a new HlaOtherArealObject instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param otherArealObject the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaOtherArealObjectDiscovered(HlaOtherArealObjectPtr otherArealObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaOtherArealObject instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param otherArealObject the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaOtherArealObjectInitialized(HlaOtherArealObjectPtr otherArealObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaOtherArealObjectManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param otherArealObject the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaOtherArealObjectInInterest(HlaOtherArealObjectPtr otherArealObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaOtherArealObjectManagerListener instance goes out of interest.
        *
        * @param otherArealObject the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaOtherArealObjectOutOfInterest(HlaOtherArealObjectPtr otherArealObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaOtherArealObject instance is deleted.
        *
        * @param otherArealObject the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaOtherArealObjectDeleted(HlaOtherArealObjectPtr otherArealObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaOtherArealObjectManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaOtherArealObjectManagerListener::Adapter : public HlaOtherArealObjectManagerListener {

    public:
        LIBAPI virtual void hlaOtherArealObjectDiscovered(HlaOtherArealObjectPtr otherArealObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaOtherArealObjectInitialized(HlaOtherArealObjectPtr otherArealObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaOtherArealObjectInInterest(HlaOtherArealObjectPtr otherArealObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaOtherArealObjectOutOfInterest(HlaOtherArealObjectPtr otherArealObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaOtherArealObjectDeleted(HlaOtherArealObjectPtr otherArealObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
