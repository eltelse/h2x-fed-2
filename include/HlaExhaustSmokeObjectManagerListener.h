/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAEXHAUSTSMOKEOBJECTMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLAEXHAUSTSMOKEOBJECTMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaExhaustSmokeObject.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaExhaustSmokeObject instances.
    */
    class HlaExhaustSmokeObjectManagerListener {

    public:

        LIBAPI virtual ~HlaExhaustSmokeObjectManagerListener() {}

        /**
        * This method is called when a new HlaExhaustSmokeObject instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param exhaustSmokeObject the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaExhaustSmokeObjectDiscovered(HlaExhaustSmokeObjectPtr exhaustSmokeObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaExhaustSmokeObject instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param exhaustSmokeObject the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaExhaustSmokeObjectInitialized(HlaExhaustSmokeObjectPtr exhaustSmokeObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaExhaustSmokeObjectManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param exhaustSmokeObject the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaExhaustSmokeObjectInInterest(HlaExhaustSmokeObjectPtr exhaustSmokeObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaExhaustSmokeObjectManagerListener instance goes out of interest.
        *
        * @param exhaustSmokeObject the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaExhaustSmokeObjectOutOfInterest(HlaExhaustSmokeObjectPtr exhaustSmokeObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaExhaustSmokeObject instance is deleted.
        *
        * @param exhaustSmokeObject the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaExhaustSmokeObjectDeleted(HlaExhaustSmokeObjectPtr exhaustSmokeObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaExhaustSmokeObjectManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaExhaustSmokeObjectManagerListener::Adapter : public HlaExhaustSmokeObjectManagerListener {

    public:
        LIBAPI virtual void hlaExhaustSmokeObjectDiscovered(HlaExhaustSmokeObjectPtr exhaustSmokeObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaExhaustSmokeObjectInitialized(HlaExhaustSmokeObjectPtr exhaustSmokeObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaExhaustSmokeObjectInInterest(HlaExhaustSmokeObjectPtr exhaustSmokeObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaExhaustSmokeObjectOutOfInterest(HlaExhaustSmokeObjectPtr exhaustSmokeObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaExhaustSmokeObjectDeleted(HlaExhaustSmokeObjectPtr exhaustSmokeObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
