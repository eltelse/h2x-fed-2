/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLABREACHOBJECTMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLABREACHOBJECTMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaBreachObject.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaBreachObject instances.
    */
    class HlaBreachObjectManagerListener {

    public:

        LIBAPI virtual ~HlaBreachObjectManagerListener() {}

        /**
        * This method is called when a new HlaBreachObject instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param breachObject the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaBreachObjectDiscovered(HlaBreachObjectPtr breachObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaBreachObject instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param breachObject the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaBreachObjectInitialized(HlaBreachObjectPtr breachObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaBreachObjectManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param breachObject the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaBreachObjectInInterest(HlaBreachObjectPtr breachObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaBreachObjectManagerListener instance goes out of interest.
        *
        * @param breachObject the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaBreachObjectOutOfInterest(HlaBreachObjectPtr breachObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaBreachObject instance is deleted.
        *
        * @param breachObject the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaBreachObjectDeleted(HlaBreachObjectPtr breachObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaBreachObjectManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaBreachObjectManagerListener::Adapter : public HlaBreachObjectManagerListener {

    public:
        LIBAPI virtual void hlaBreachObjectDiscovered(HlaBreachObjectPtr breachObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaBreachObjectInitialized(HlaBreachObjectPtr breachObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaBreachObjectInInterest(HlaBreachObjectPtr breachObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaBreachObjectOutOfInterest(HlaBreachObjectPtr breachObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaBreachObjectDeleted(HlaBreachObjectPtr breachObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
