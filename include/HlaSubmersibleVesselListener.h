/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLASUBMERSIBLEVESSELLISTENER_H
#define DEVELOPER_STUDIO_HLASUBMERSIBLEVESSELLISTENER_H

#include <set>


#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaSubmersibleVesselAttributes.h>

namespace DevStudio {
   /**
   * Listener for updates of attributes.  
   */
   class HlaSubmersibleVesselListener {
   public:

      LIBAPI virtual ~HlaSubmersibleVesselListener() {}

      /**
      * This method is called when a HLA <code>reflectAttributeValueUpdate</code> is received for an remote object,
      * or a set of attributes is updated on a local object.
      *
      * @param submersibleVessel The submersibleVessel which this update corresponds to.
      * @param attributes The set of attributes that are updated.
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time the update was initiated.
      */
      LIBAPI virtual void attributesUpdated(HlaSubmersibleVesselPtr submersibleVessel, std::set<HlaSubmersibleVesselAttributes::Attribute> &attributes, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

      class Adapter;
   };

  /**
  * An adapter class that implements the HlaSubmersibleVesselListener interface with empty methods.
  * It might be used as a base class for a listener.
  */
  class HlaSubmersibleVesselListener::Adapter : public HlaSubmersibleVesselListener {

  public:

     LIBAPI virtual void attributesUpdated(HlaSubmersibleVesselPtr submersibleVessel, std::set<HlaSubmersibleVesselAttributes::Attribute> &attributes, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
    };

}
#endif
