/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAMINEFIELDUPDATER_H
#define DEVELOPER_STUDIO_HLAMINEFIELDUPDATER_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <boost/noncopyable.hpp>

#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EntityTypeStruct.h>
#include <DevStudio/datatypes/EntityTypeStructLengthlessArray.h>
#include <DevStudio/datatypes/ForceIdentifierEnum.h>
#include <DevStudio/datatypes/MinefieldLaneEnum.h>
#include <DevStudio/datatypes/MinefieldProtocolEnum.h>
#include <DevStudio/datatypes/MinefieldStatusEnum.h>
#include <DevStudio/datatypes/MinefieldTypeEnum.h>
#include <DevStudio/datatypes/OrientationStruct.h>
#include <DevStudio/datatypes/PerimeterPointStruct.h>
#include <DevStudio/datatypes/PerimeterPointStructLengthlessArray.h>
#include <DevStudio/datatypes/WorldLocationStruct.h>
#include <vector>

#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaException.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaHLAobjectRootUpdater.h>

namespace DevStudio {

    /**
    * Updater used to update attribute values.
    */
    class HlaMinefieldUpdater : public HlaHLAobjectRootUpdater {

    public:

    LIBAPI virtual ~HlaMinefieldUpdater() {}

    /**
    * Set the activeStatus for this update.
    * <br>Description from the FOM: <i>Specifies the active status of the minefield</i>
    * <br>Description of the data type from the FOM: <i>Minefield status</i>
    *
    * @param activeStatus the new activeStatus
    */
    LIBAPI virtual void setActiveStatus(const DevStudio::MinefieldStatusEnum::MinefieldStatusEnum& activeStatus) = 0;

    /**
    * Set the forceIdentifier for this update.
    * <br>Description from the FOM: <i>Identifies the force to which the minefield belongs</i>
    * <br>Description of the data type from the FOM: <i>Force ID</i>
    *
    * @param forceIdentifier the new forceIdentifier
    */
    LIBAPI virtual void setForceIdentifier(const DevStudio::ForceIdentifierEnum::ForceIdentifierEnum& forceIdentifier) = 0;

    /**
    * Set the lane for this update.
    * <br>Description from the FOM: <i>Specifies whether or not the minefield has an active lane</i>
    * <br>Description of the data type from the FOM: <i>Minefield lane status</i>
    *
    * @param lane the new lane
    */
    LIBAPI virtual void setLane(const DevStudio::MinefieldLaneEnum::MinefieldLaneEnum& lane) = 0;

    /**
    * Set the minefieldAppearanceType for this update.
    * <br>Description from the FOM: <i>Specifies the appearance information needed for displaying the symbology of the minefield</i>
    * <br>Description of the data type from the FOM: <i>Minefield type</i>
    *
    * @param minefieldAppearanceType the new minefieldAppearanceType
    */
    LIBAPI virtual void setMinefieldAppearanceType(const DevStudio::MinefieldTypeEnum::MinefieldTypeEnum& minefieldAppearanceType) = 0;

    /**
    * Set the minefieldIdentifier for this update.
    * <br>Description from the FOM: <i>Uniquely identifies this minefield instance in association with the federate's site and application</i>
    * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
    *
    * @param minefieldIdentifier the new minefieldIdentifier
    */
    LIBAPI virtual void setMinefieldIdentifier(const DevStudio::EntityIdentifierStruct& minefieldIdentifier) = 0;

    /**
    * Set the minefieldLocation for this update.
    * <br>Description from the FOM: <i>Specifies the location of the center of the minefield</i>
    * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
    *
    * @param minefieldLocation the new minefieldLocation
    */
    LIBAPI virtual void setMinefieldLocation(const DevStudio::WorldLocationStruct& minefieldLocation) = 0;

    /**
    * Set the minefieldOrientation for this update.
    * <br>Description from the FOM: <i>Specifies the orientation of the minefield, with Euler angles</i>
    * <br>Description of the data type from the FOM: <i>The orientation of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
    *
    * @param minefieldOrientation the new minefieldOrientation
    */
    LIBAPI virtual void setMinefieldOrientation(const DevStudio::OrientationStruct& minefieldOrientation) = 0;

    /**
    * Set the minefieldType for this update.
    * <br>Description from the FOM: <i>Specifies the minefield type</i>
    * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
    *
    * @param minefieldType the new minefieldType
    */
    LIBAPI virtual void setMinefieldType(const DevStudio::EntityTypeStruct& minefieldType) = 0;

    /**
    * Set the mineTypes for this update.
    * <br>Description from the FOM: <i>Specifies the type of each mine contained within the minefield</i>
    * <br>Description of the data type from the FOM: <i>Dynamic array of EntityTypeStruct elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
    *
    * @param mineTypes the new mineTypes
    */
    LIBAPI virtual void setMineTypes(const std::vector<DevStudio::EntityTypeStruct >& mineTypes) = 0;

    /**
    * Set the perimeterPointCoordinates for this update.
    * <br>Description from the FOM: <i>Specifies the location of each perimeter point, relative to the minefield location</i>
    * <br>Description of the data type from the FOM: <i>Specifies the location of perimeter points (collection)</i>
    *
    * @param perimeterPointCoordinates the new perimeterPointCoordinates
    */
    LIBAPI virtual void setPerimeterPointCoordinates(const std::vector<DevStudio::PerimeterPointStruct >& perimeterPointCoordinates) = 0;

    /**
    * Set the protocolMode for this update.
    * <br>Description from the FOM: <i>Specifies the mode (Heartbeat or Query Response Protocol) being used to communicate data about the minefield</i>
    * <br>Description of the data type from the FOM: <i>Minefield communication protocol mode</i>
    *
    * @param protocolMode the new protocolMode
    */
    LIBAPI virtual void setProtocolMode(const DevStudio::MinefieldProtocolEnum::MinefieldProtocolEnum& protocolMode) = 0;

    /**
    * Set the state for this update.
    * <br>Description from the FOM: <i>Specifies whether or not the minefield has been deactivated</i>
    * <br>Description of the data type from the FOM: <i></i>
    *
    * @param state the new state
    */
    LIBAPI virtual void setState(const bool& state) = 0;

    /**
    * Send all the attributes.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate()
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

    /**
    * Send all the attributes with a specified timestamp.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @param timestamp timestamp to send for this update
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate(HlaTimeStampPtr timestamp)
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

    /**
    * Send all the attributes with a specified timestamp.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @param logicalTime logical time to send for this update
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaInvalidLogicalTimeException if the logical time is not valid.
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate(HlaLogicalTimePtr logicalTime)
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaInvalidLogicalTimeException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

    /**
    * Send all the attributes with a specified timestamp.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @param timestamp timestamp to send for this update
    * @param logicalTime logical time to send for this update
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaInvalidLogicalTimeException if the logical time is not valid.
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate(HlaTimeStampPtr timestamp, HlaLogicalTimePtr logicalTime)
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaInvalidLogicalTimeException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;
    };
}
#endif
