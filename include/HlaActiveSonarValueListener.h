/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAACTIVESONARVALUELISTENER_H
#define DEVELOPER_STUDIO_HLAACTIVESONARVALUELISTENER_H

#include <memory>

#include <DevStudio/datatypes/ActiveSonarEnum.h>
#include <DevStudio/datatypes/ActiveSonarFunctionCodeEnum.h>
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EventIdentifierStruct.h>
#include <DevStudio/datatypes/RelativePositionStruct.h>
#include <string>

#include <DevStudio/HlaLogicalTime.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaActiveSonarAttributes.h>    

namespace DevStudio {

   /**
   * Listener for updates of attributes, with the new updated values.  
   */
   class HlaActiveSonarValueListener {

   public:

      LIBAPI virtual ~HlaActiveSonarValueListener() {}
    
      /**
      * This method is called when the attribute <code>acousticName</code> is updated.
      * <br>Description from the FOM: <i>Defines the type of sonar being represented.</i>
      * <br>Description of the data type from the FOM: <i>Acoustic system name</i>
      *
      * @param activeSonar The object which is updated.
      * @param acousticName The new value of the attribute in this update
      * @param validOldAcousticName True if oldAcousticName contains a valid value
      * @param oldAcousticName The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void acousticNameUpdated(HlaActiveSonarPtr activeSonar, ActiveSonarEnum::ActiveSonarEnum acousticName, bool validOldAcousticName, ActiveSonarEnum::ActiveSonarEnum oldAcousticName, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>functionCode</code> is updated.
      * <br>Description from the FOM: <i>Declares the current primary use of the sonar. It may be used by simulatons to infer intent.</i>
      * <br>Description of the data type from the FOM: <i>The current function being performed by the sonar</i>
      *
      * @param activeSonar The object which is updated.
      * @param functionCode The new value of the attribute in this update
      * @param validOldFunctionCode True if oldFunctionCode contains a valid value
      * @param oldFunctionCode The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void functionCodeUpdated(HlaActiveSonarPtr activeSonar, ActiveSonarFunctionCodeEnum::ActiveSonarFunctionCodeEnum functionCode, bool validOldFunctionCode, ActiveSonarFunctionCodeEnum::ActiveSonarFunctionCodeEnum oldFunctionCode, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>acousticsIdentifier</code> is updated.
      * <br>Description from the FOM: <i>Unique identifier for the sonar on an entity, Starts with 1</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param activeSonar The object which is updated.
      * @param acousticsIdentifier The new value of the attribute in this update
      * @param validOldAcousticsIdentifier True if oldAcousticsIdentifier contains a valid value
      * @param oldAcousticsIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void acousticsIdentifierUpdated(HlaActiveSonarPtr activeSonar, char acousticsIdentifier, bool validOldAcousticsIdentifier, char oldAcousticsIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>eventIdentifier</code> is updated.
      * <br>Description from the FOM: <i>The generating federate uses the Event Identifier to associate related events. The event number begins at one at the beginning of the exercise and is incremented by one for each event.</i>
      * <br>Description of the data type from the FOM: <i>Identification of an event. Based on the Event Identifier record as specified in IEEE 1278.1-1995 section 5.2.18.</i>
      *
      * @param activeSonar The object which is updated.
      * @param eventIdentifier The new value of the attribute in this update
      * @param validOldEventIdentifier True if oldEventIdentifier contains a valid value
      * @param oldEventIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void eventIdentifierUpdated(HlaActiveSonarPtr activeSonar, EventIdentifierStruct eventIdentifier, bool validOldEventIdentifier, EventIdentifierStruct oldEventIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>entityIdentifier</code> is updated.
      * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @param activeSonar The object which is updated.
      * @param entityIdentifier The new value of the attribute in this update
      * @param validOldEntityIdentifier True if oldEntityIdentifier contains a valid value
      * @param oldEntityIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void entityIdentifierUpdated(HlaActiveSonarPtr activeSonar, EntityIdentifierStruct entityIdentifier, bool validOldEntityIdentifier, EntityIdentifierStruct oldEntityIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>hostObjectIdentifier</code> is updated.
      * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @param activeSonar The object which is updated.
      * @param hostObjectIdentifier The new value of the attribute in this update
      * @param validOldHostObjectIdentifier True if oldHostObjectIdentifier contains a valid value
      * @param oldHostObjectIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void hostObjectIdentifierUpdated(HlaActiveSonarPtr activeSonar, std::string hostObjectIdentifier, bool validOldHostObjectIdentifier, std::string oldHostObjectIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>relativePosition</code> is updated.
      * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
      * <br>Description of the data type from the FOM: <i>Relative position in right-handed Cartesian coordinates.</i>
      *
      * @param activeSonar The object which is updated.
      * @param relativePosition The new value of the attribute in this update
      * @param validOldRelativePosition True if oldRelativePosition contains a valid value
      * @param oldRelativePosition The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void relativePositionUpdated(HlaActiveSonarPtr activeSonar, RelativePositionStruct relativePosition, bool validOldRelativePosition, RelativePositionStruct oldRelativePosition, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
      /**
      * This method is called when the producing federate of an attribute is changed.
      * Only available when using HLA Evolved.
      *
      * @param activeSonar The object which is updated.
      * @param attribute The attribute that now has a new producing federate
      * @param oldProducingFederate The federate handle of the old producing federate
      * @param newProducingFederate The federate handle of the new producing federate
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void producingFederateUpdated(HlaActiveSonarPtr activeSonar, HlaActiveSonarAttributes::Attribute attribute, HlaFederateIdPtr oldProducingFederate, HlaFederateIdPtr newProducingFederate, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;

      class Adapter;
   };

   /**
   * An adapter class that implements the HlaActiveSonarValueListener interface with empty methods.
   * It might be used as a base class for a listener.
   */
   class HlaActiveSonarValueListener::Adapter : public HlaActiveSonarValueListener {

   public:

      LIBAPI virtual void acousticNameUpdated(HlaActiveSonarPtr activeSonar, ActiveSonarEnum::ActiveSonarEnum acousticName, bool validOldAcousticName, ActiveSonarEnum::ActiveSonarEnum oldAcousticName, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void functionCodeUpdated(HlaActiveSonarPtr activeSonar, ActiveSonarFunctionCodeEnum::ActiveSonarFunctionCodeEnum functionCode, bool validOldFunctionCode, ActiveSonarFunctionCodeEnum::ActiveSonarFunctionCodeEnum oldFunctionCode, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void acousticsIdentifierUpdated(HlaActiveSonarPtr activeSonar, char acousticsIdentifier, bool validOldAcousticsIdentifier, char oldAcousticsIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void eventIdentifierUpdated(HlaActiveSonarPtr activeSonar, EventIdentifierStruct eventIdentifier, bool validOldEventIdentifier, EventIdentifierStruct oldEventIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void entityIdentifierUpdated(HlaActiveSonarPtr activeSonar, EntityIdentifierStruct entityIdentifier, bool validOldEntityIdentifier, EntityIdentifierStruct oldEntityIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void hostObjectIdentifierUpdated(HlaActiveSonarPtr activeSonar, std::string hostObjectIdentifier, bool validOldHostObjectIdentifier, std::string oldHostObjectIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void relativePositionUpdated(HlaActiveSonarPtr activeSonar, RelativePositionStruct relativePosition, bool validOldRelativePosition, RelativePositionStruct oldRelativePosition, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
      LIBAPI virtual void producingFederateUpdated(HlaActiveSonarPtr activeSonar, HlaActiveSonarAttributes::Attribute attribute, HlaFederateIdPtr oldProducingFederate, HlaFederateIdPtr newProducingFederate, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
   };

}
#endif
