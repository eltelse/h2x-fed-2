/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLABREACHOBJECTVALUELISTENER_H
#define DEVELOPER_STUDIO_HLABREACHOBJECTVALUELISTENER_H

#include <memory>

#include <DevStudio/datatypes/BreachStruct.h>
#include <DevStudio/datatypes/BreachStructLengthlessArray.h>
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EnvironmentObjectTypeStruct.h>
#include <DevStudio/datatypes/ForceIdentifierEnum.h>
#include <string>
#include <vector>

#include <DevStudio/HlaLogicalTime.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaBreachObjectAttributes.h>    

namespace DevStudio {

   /**
   * Listener for updates of attributes, with the new updated values.  
   */
   class HlaBreachObjectValueListener {

   public:

      LIBAPI virtual ~HlaBreachObjectValueListener() {}
    
      /**
      * This method is called when the attribute <code>segmentRecords</code> is updated.
      * <br>Description from the FOM: <i>Specifies a breach linear object</i>
      * <br>Description of the data type from the FOM: <i>Specifies a breach linear object (a collection of segments)</i>
      *
      * @param breachObject The object which is updated.
      * @param segmentRecords The new value of the attribute in this update
      * @param validOldSegmentRecords True if oldSegmentRecords contains a valid value
      * @param oldSegmentRecords The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void segmentRecordsUpdated(HlaBreachObjectPtr breachObject, std::vector<DevStudio::BreachStruct > segmentRecords, bool validOldSegmentRecords, std::vector<DevStudio::BreachStruct > oldSegmentRecords, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>objectIdentifier</code> is updated.
      * <br>Description from the FOM: <i>Identifies this EnvironmentObject instance (point, linear or areal)</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @param breachObject The object which is updated.
      * @param objectIdentifier The new value of the attribute in this update
      * @param validOldObjectIdentifier True if oldObjectIdentifier contains a valid value
      * @param oldObjectIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void objectIdentifierUpdated(HlaBreachObjectPtr breachObject, EntityIdentifierStruct objectIdentifier, bool validOldObjectIdentifier, EntityIdentifierStruct oldObjectIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>referencedObjectIdentifier</code> is updated.
      * <br>Description from the FOM: <i>Identifies the Synthetic Environment object instance to which this EnvironmentObject instance is associated</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @param breachObject The object which is updated.
      * @param referencedObjectIdentifier The new value of the attribute in this update
      * @param validOldReferencedObjectIdentifier True if oldReferencedObjectIdentifier contains a valid value
      * @param oldReferencedObjectIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void referencedObjectIdentifierUpdated(HlaBreachObjectPtr breachObject, std::string referencedObjectIdentifier, bool validOldReferencedObjectIdentifier, std::string oldReferencedObjectIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>forceIdentifier</code> is updated.
      * <br>Description from the FOM: <i>Identifies the force that created or modified this EnvironmentObject instance</i>
      * <br>Description of the data type from the FOM: <i>Force ID</i>
      *
      * @param breachObject The object which is updated.
      * @param forceIdentifier The new value of the attribute in this update
      * @param validOldForceIdentifier True if oldForceIdentifier contains a valid value
      * @param oldForceIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void forceIdentifierUpdated(HlaBreachObjectPtr breachObject, ForceIdentifierEnum::ForceIdentifierEnum forceIdentifier, bool validOldForceIdentifier, ForceIdentifierEnum::ForceIdentifierEnum oldForceIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>objectType</code> is updated.
      * <br>Description from the FOM: <i>Identifies the type of this EnvironmentObject instance</i>
      * <br>Description of the data type from the FOM: <i>Record specifying the domain, the kind and the specific identification of the environment object</i>
      *
      * @param breachObject The object which is updated.
      * @param objectType The new value of the attribute in this update
      * @param validOldObjectType True if oldObjectType contains a valid value
      * @param oldObjectType The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void objectTypeUpdated(HlaBreachObjectPtr breachObject, EnvironmentObjectTypeStruct objectType, bool validOldObjectType, EnvironmentObjectTypeStruct oldObjectType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
      /**
      * This method is called when the producing federate of an attribute is changed.
      * Only available when using HLA Evolved.
      *
      * @param breachObject The object which is updated.
      * @param attribute The attribute that now has a new producing federate
      * @param oldProducingFederate The federate handle of the old producing federate
      * @param newProducingFederate The federate handle of the new producing federate
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void producingFederateUpdated(HlaBreachObjectPtr breachObject, HlaBreachObjectAttributes::Attribute attribute, HlaFederateIdPtr oldProducingFederate, HlaFederateIdPtr newProducingFederate, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;

      class Adapter;
   };

   /**
   * An adapter class that implements the HlaBreachObjectValueListener interface with empty methods.
   * It might be used as a base class for a listener.
   */
   class HlaBreachObjectValueListener::Adapter : public HlaBreachObjectValueListener {

   public:

      LIBAPI virtual void segmentRecordsUpdated(HlaBreachObjectPtr breachObject, std::vector<DevStudio::BreachStruct > segmentRecords, bool validOldSegmentRecords, std::vector<DevStudio::BreachStruct > oldSegmentRecords, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void objectIdentifierUpdated(HlaBreachObjectPtr breachObject, EntityIdentifierStruct objectIdentifier, bool validOldObjectIdentifier, EntityIdentifierStruct oldObjectIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void referencedObjectIdentifierUpdated(HlaBreachObjectPtr breachObject, std::string referencedObjectIdentifier, bool validOldReferencedObjectIdentifier, std::string oldReferencedObjectIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void forceIdentifierUpdated(HlaBreachObjectPtr breachObject, ForceIdentifierEnum::ForceIdentifierEnum forceIdentifier, bool validOldForceIdentifier, ForceIdentifierEnum::ForceIdentifierEnum oldForceIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void objectTypeUpdated(HlaBreachObjectPtr breachObject, EnvironmentObjectTypeStruct objectType, bool validOldObjectType, EnvironmentObjectTypeStruct oldObjectType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
      LIBAPI virtual void producingFederateUpdated(HlaBreachObjectPtr breachObject, HlaBreachObjectAttributes::Attribute attribute, HlaFederateIdPtr oldProducingFederate, HlaFederateIdPtr newProducingFederate, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
   };

}
#endif
