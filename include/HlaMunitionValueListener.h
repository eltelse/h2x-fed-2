/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAMUNITIONVALUELISTENER_H
#define DEVELOPER_STUDIO_HLAMUNITIONVALUELISTENER_H

#include <memory>

#include <DevStudio/datatypes/ArticulatedParameterStruct.h>
#include <DevStudio/datatypes/ArticulatedParameterStructLengthlessArray.h>
#include <DevStudio/datatypes/CamouflageEnum.h>
#include <DevStudio/datatypes/DamageStatusEnum.h>
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EntityTypeStruct.h>
#include <DevStudio/datatypes/ForceIdentifierEnum.h>
#include <DevStudio/datatypes/IsPartOfStruct.h>
#include <DevStudio/datatypes/MarkingStruct.h>
#include <DevStudio/datatypes/PropulsionSystemDataStruct.h>
#include <DevStudio/datatypes/PropulsionSystemDataStructLengthlessArray.h>
#include <DevStudio/datatypes/SpatialVariantStruct.h>
#include <DevStudio/datatypes/TrailingEffectsCodeEnum.h>
#include <DevStudio/datatypes/VectoringNozzleSystemDataStruct.h>
#include <DevStudio/datatypes/VectoringNozzleSystemDataStructLengthlessArray.h>
#include <vector>

#include <DevStudio/HlaLogicalTime.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaMunitionAttributes.h>    

namespace DevStudio {

   /**
   * Listener for updates of attributes, with the new updated values.  
   */
   class HlaMunitionValueListener {

   public:

      LIBAPI virtual ~HlaMunitionValueListener() {}
    
      /**
      * This method is called when the attribute <code>launcherFlashPresent</code> is updated.
      * <br>Description from the FOM: <i>Whether the flash of the munition being launched is present or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param munition The object which is updated.
      * @param launcherFlashPresent The new value of the attribute in this update
      * @param validOldLauncherFlashPresent True if oldLauncherFlashPresent contains a valid value
      * @param oldLauncherFlashPresent The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void launcherFlashPresentUpdated(HlaMunitionPtr munition, bool launcherFlashPresent, bool validOldLauncherFlashPresent, bool oldLauncherFlashPresent, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>acousticSignatureIndex</code> is updated.
      * <br>Description from the FOM: <i>Index used to obtain the acoustics (sound through air) signature state of the entity.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^15, 2^15-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param munition The object which is updated.
      * @param acousticSignatureIndex The new value of the attribute in this update
      * @param validOldAcousticSignatureIndex True if oldAcousticSignatureIndex contains a valid value
      * @param oldAcousticSignatureIndex The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void acousticSignatureIndexUpdated(HlaMunitionPtr munition, short acousticSignatureIndex, bool validOldAcousticSignatureIndex, short oldAcousticSignatureIndex, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>alternateEntityType</code> is updated.
      * <br>Description from the FOM: <i>The category of entity to be used when viewed by entities on the 'opposite' side.</i>
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      *
      * @param munition The object which is updated.
      * @param alternateEntityType The new value of the attribute in this update
      * @param validOldAlternateEntityType True if oldAlternateEntityType contains a valid value
      * @param oldAlternateEntityType The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void alternateEntityTypeUpdated(HlaMunitionPtr munition, EntityTypeStruct alternateEntityType, bool validOldAlternateEntityType, EntityTypeStruct oldAlternateEntityType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>articulatedParametersArray</code> is updated.
      * <br>Description from the FOM: <i>Identification of the visible parts, and their states, of the entity which are capable of independent motion.</i>
      * <br>Description of the data type from the FOM: <i>Dynamic array of ArticulatedParameterStruct elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
      *
      * @param munition The object which is updated.
      * @param articulatedParametersArray The new value of the attribute in this update
      * @param validOldArticulatedParametersArray True if oldArticulatedParametersArray contains a valid value
      * @param oldArticulatedParametersArray The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void articulatedParametersArrayUpdated(HlaMunitionPtr munition, std::vector<DevStudio::ArticulatedParameterStruct > articulatedParametersArray, bool validOldArticulatedParametersArray, std::vector<DevStudio::ArticulatedParameterStruct > oldArticulatedParametersArray, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>camouflageType</code> is updated.
      * <br>Description from the FOM: <i>The type of camouflage in use (if any).</i>
      * <br>Description of the data type from the FOM: <i>Camouflage type</i>
      *
      * @param munition The object which is updated.
      * @param camouflageType The new value of the attribute in this update
      * @param validOldCamouflageType True if oldCamouflageType contains a valid value
      * @param oldCamouflageType The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void camouflageTypeUpdated(HlaMunitionPtr munition, CamouflageEnum::CamouflageEnum camouflageType, bool validOldCamouflageType, CamouflageEnum::CamouflageEnum oldCamouflageType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>damageState</code> is updated.
      * <br>Description from the FOM: <i>The state of damage of the entity.</i>
      * <br>Description of the data type from the FOM: <i>Damaged appearance</i>
      *
      * @param munition The object which is updated.
      * @param damageState The new value of the attribute in this update
      * @param validOldDamageState True if oldDamageState contains a valid value
      * @param oldDamageState The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void damageStateUpdated(HlaMunitionPtr munition, DamageStatusEnum::DamageStatusEnum damageState, bool validOldDamageState, DamageStatusEnum::DamageStatusEnum oldDamageState, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>engineSmokeOn</code> is updated.
      * <br>Description from the FOM: <i>Whether the entity's engine is generating smoke or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param munition The object which is updated.
      * @param engineSmokeOn The new value of the attribute in this update
      * @param validOldEngineSmokeOn True if oldEngineSmokeOn contains a valid value
      * @param oldEngineSmokeOn The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void engineSmokeOnUpdated(HlaMunitionPtr munition, bool engineSmokeOn, bool validOldEngineSmokeOn, bool oldEngineSmokeOn, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>firePowerDisabled</code> is updated.
      * <br>Description from the FOM: <i>Whether the entity's main weapon system has been disabled or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param munition The object which is updated.
      * @param firePowerDisabled The new value of the attribute in this update
      * @param validOldFirePowerDisabled True if oldFirePowerDisabled contains a valid value
      * @param oldFirePowerDisabled The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void firePowerDisabledUpdated(HlaMunitionPtr munition, bool firePowerDisabled, bool validOldFirePowerDisabled, bool oldFirePowerDisabled, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>flamesPresent</code> is updated.
      * <br>Description from the FOM: <i>Whether the entity is on fire (with visible flames) or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param munition The object which is updated.
      * @param flamesPresent The new value of the attribute in this update
      * @param validOldFlamesPresent True if oldFlamesPresent contains a valid value
      * @param oldFlamesPresent The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void flamesPresentUpdated(HlaMunitionPtr munition, bool flamesPresent, bool validOldFlamesPresent, bool oldFlamesPresent, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>forceIdentifier</code> is updated.
      * <br>Description from the FOM: <i>The identification of the force that the entity belongs to.</i>
      * <br>Description of the data type from the FOM: <i>Force ID</i>
      *
      * @param munition The object which is updated.
      * @param forceIdentifier The new value of the attribute in this update
      * @param validOldForceIdentifier True if oldForceIdentifier contains a valid value
      * @param oldForceIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void forceIdentifierUpdated(HlaMunitionPtr munition, ForceIdentifierEnum::ForceIdentifierEnum forceIdentifier, bool validOldForceIdentifier, ForceIdentifierEnum::ForceIdentifierEnum oldForceIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>hasAmmunitionSupplyCap</code> is updated.
      * <br>Description from the FOM: <i>Whether the entity has the capability to supply other entities with ammunition.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param munition The object which is updated.
      * @param hasAmmunitionSupplyCap The new value of the attribute in this update
      * @param validOldHasAmmunitionSupplyCap True if oldHasAmmunitionSupplyCap contains a valid value
      * @param oldHasAmmunitionSupplyCap The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void hasAmmunitionSupplyCapUpdated(HlaMunitionPtr munition, bool hasAmmunitionSupplyCap, bool validOldHasAmmunitionSupplyCap, bool oldHasAmmunitionSupplyCap, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>hasFuelSupplyCap</code> is updated.
      * <br>Description from the FOM: <i>Whether the entity has the capability to supply other entities with fuel or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param munition The object which is updated.
      * @param hasFuelSupplyCap The new value of the attribute in this update
      * @param validOldHasFuelSupplyCap True if oldHasFuelSupplyCap contains a valid value
      * @param oldHasFuelSupplyCap The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void hasFuelSupplyCapUpdated(HlaMunitionPtr munition, bool hasFuelSupplyCap, bool validOldHasFuelSupplyCap, bool oldHasFuelSupplyCap, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>hasRecoveryCap</code> is updated.
      * <br>Description from the FOM: <i>Whether the entity has the capability to recover other entities or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param munition The object which is updated.
      * @param hasRecoveryCap The new value of the attribute in this update
      * @param validOldHasRecoveryCap True if oldHasRecoveryCap contains a valid value
      * @param oldHasRecoveryCap The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void hasRecoveryCapUpdated(HlaMunitionPtr munition, bool hasRecoveryCap, bool validOldHasRecoveryCap, bool oldHasRecoveryCap, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>hasRepairCap</code> is updated.
      * <br>Description from the FOM: <i>Whether the entity has the capability to repair other entities or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param munition The object which is updated.
      * @param hasRepairCap The new value of the attribute in this update
      * @param validOldHasRepairCap True if oldHasRepairCap contains a valid value
      * @param oldHasRepairCap The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void hasRepairCapUpdated(HlaMunitionPtr munition, bool hasRepairCap, bool validOldHasRepairCap, bool oldHasRepairCap, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>immobilized</code> is updated.
      * <br>Description from the FOM: <i>Whether the entity is immobilized or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param munition The object which is updated.
      * @param immobilized The new value of the attribute in this update
      * @param validOldImmobilized True if oldImmobilized contains a valid value
      * @param oldImmobilized The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void immobilizedUpdated(HlaMunitionPtr munition, bool immobilized, bool validOldImmobilized, bool oldImmobilized, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>infraredSignatureIndex</code> is updated.
      * <br>Description from the FOM: <i>Index used to obtain the infra-red signature state of the entity.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^15, 2^15-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param munition The object which is updated.
      * @param infraredSignatureIndex The new value of the attribute in this update
      * @param validOldInfraredSignatureIndex True if oldInfraredSignatureIndex contains a valid value
      * @param oldInfraredSignatureIndex The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void infraredSignatureIndexUpdated(HlaMunitionPtr munition, short infraredSignatureIndex, bool validOldInfraredSignatureIndex, short oldInfraredSignatureIndex, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>isConcealed</code> is updated.
      * <br>Description from the FOM: <i>Whether the entity is concealed or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param munition The object which is updated.
      * @param isConcealed The new value of the attribute in this update
      * @param validOldIsConcealed True if oldIsConcealed contains a valid value
      * @param oldIsConcealed The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void isConcealedUpdated(HlaMunitionPtr munition, bool isConcealed, bool validOldIsConcealed, bool oldIsConcealed, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>liveEntityMeasuredSpeed</code> is updated.
      * <br>Description from the FOM: <i>The entity's own measurement of speed (e.g. air speed for aircraft).</i>
      * <br>Description of the data type from the FOM: <i>Velocity/Speed measured in decimeter per second. [unit: decimeter per second (dm/s), resolution: 1, accuracy: perfect]</i>
      *
      * @param munition The object which is updated.
      * @param liveEntityMeasuredSpeed The new value of the attribute in this update
      * @param validOldLiveEntityMeasuredSpeed True if oldLiveEntityMeasuredSpeed contains a valid value
      * @param oldLiveEntityMeasuredSpeed The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void liveEntityMeasuredSpeedUpdated(HlaMunitionPtr munition, unsigned short liveEntityMeasuredSpeed, bool validOldLiveEntityMeasuredSpeed, unsigned short oldLiveEntityMeasuredSpeed, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>marking</code> is updated.
      * <br>Description from the FOM: <i>A unique marking or combination of characters used to distinguish the entity from other entities.</i>
      * <br>Description of the data type from the FOM: <i>Character set used in the marking and the string of characters to be interpreted for display.</i>
      *
      * @param munition The object which is updated.
      * @param marking The new value of the attribute in this update
      * @param validOldMarking True if oldMarking contains a valid value
      * @param oldMarking The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void markingUpdated(HlaMunitionPtr munition, MarkingStruct marking, bool validOldMarking, MarkingStruct oldMarking, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>powerPlantOn</code> is updated.
      * <br>Description from the FOM: <i>Whether the entity's power plant is on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param munition The object which is updated.
      * @param powerPlantOn The new value of the attribute in this update
      * @param validOldPowerPlantOn True if oldPowerPlantOn contains a valid value
      * @param oldPowerPlantOn The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void powerPlantOnUpdated(HlaMunitionPtr munition, bool powerPlantOn, bool validOldPowerPlantOn, bool oldPowerPlantOn, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>propulsionSystemsData</code> is updated.
      * <br>Description from the FOM: <i>The basic operating data of the propulsion systems aboard the entity.</i>
      * <br>Description of the data type from the FOM: <i>A set of Propulsion System Data descriptions.</i>
      *
      * @param munition The object which is updated.
      * @param propulsionSystemsData The new value of the attribute in this update
      * @param validOldPropulsionSystemsData True if oldPropulsionSystemsData contains a valid value
      * @param oldPropulsionSystemsData The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void propulsionSystemsDataUpdated(HlaMunitionPtr munition, std::vector<DevStudio::PropulsionSystemDataStruct > propulsionSystemsData, bool validOldPropulsionSystemsData, std::vector<DevStudio::PropulsionSystemDataStruct > oldPropulsionSystemsData, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>radarCrossSectionSignatureIndex</code> is updated.
      * <br>Description from the FOM: <i>Index used to obtain the radar cross section signature state of the entity.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^15, 2^15-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param munition The object which is updated.
      * @param radarCrossSectionSignatureIndex The new value of the attribute in this update
      * @param validOldRadarCrossSectionSignatureIndex True if oldRadarCrossSectionSignatureIndex contains a valid value
      * @param oldRadarCrossSectionSignatureIndex The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void radarCrossSectionSignatureIndexUpdated(HlaMunitionPtr munition, short radarCrossSectionSignatureIndex, bool validOldRadarCrossSectionSignatureIndex, short oldRadarCrossSectionSignatureIndex, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>smokePlumePresent</code> is updated.
      * <br>Description from the FOM: <i>Whether the entity is generating smoke or not (intentional or unintentional).</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param munition The object which is updated.
      * @param smokePlumePresent The new value of the attribute in this update
      * @param validOldSmokePlumePresent True if oldSmokePlumePresent contains a valid value
      * @param oldSmokePlumePresent The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void smokePlumePresentUpdated(HlaMunitionPtr munition, bool smokePlumePresent, bool validOldSmokePlumePresent, bool oldSmokePlumePresent, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>tentDeployed</code> is updated.
      * <br>Description from the FOM: <i>Whether the entity has deployed tent or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param munition The object which is updated.
      * @param tentDeployed The new value of the attribute in this update
      * @param validOldTentDeployed True if oldTentDeployed contains a valid value
      * @param oldTentDeployed The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void tentDeployedUpdated(HlaMunitionPtr munition, bool tentDeployed, bool validOldTentDeployed, bool oldTentDeployed, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>trailingEffectsCode</code> is updated.
      * <br>Description from the FOM: <i>The type and size of any trail that the entity is making.</i>
      * <br>Description of the data type from the FOM: <i>Size of trailing effect</i>
      *
      * @param munition The object which is updated.
      * @param trailingEffectsCode The new value of the attribute in this update
      * @param validOldTrailingEffectsCode True if oldTrailingEffectsCode contains a valid value
      * @param oldTrailingEffectsCode The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void trailingEffectsCodeUpdated(HlaMunitionPtr munition, TrailingEffectsCodeEnum::TrailingEffectsCodeEnum trailingEffectsCode, bool validOldTrailingEffectsCode, TrailingEffectsCodeEnum::TrailingEffectsCodeEnum oldTrailingEffectsCode, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>vectoringNozzleSystemData</code> is updated.
      * <br>Description from the FOM: <i>The basic operational data for the vectoring nozzle systems aboard the entity.</i>
      * <br>Description of the data type from the FOM: <i>A set of Vectoring Nozzle System Data description.</i>
      *
      * @param munition The object which is updated.
      * @param vectoringNozzleSystemData The new value of the attribute in this update
      * @param validOldVectoringNozzleSystemData True if oldVectoringNozzleSystemData contains a valid value
      * @param oldVectoringNozzleSystemData The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void vectoringNozzleSystemDataUpdated(HlaMunitionPtr munition, std::vector<DevStudio::VectoringNozzleSystemDataStruct > vectoringNozzleSystemData, bool validOldVectoringNozzleSystemData, std::vector<DevStudio::VectoringNozzleSystemDataStruct > oldVectoringNozzleSystemData, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>entityType</code> is updated.
      * <br>Description from the FOM: <i>The category of the entity.</i>
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      *
      * @param munition The object which is updated.
      * @param entityType The new value of the attribute in this update
      * @param validOldEntityType True if oldEntityType contains a valid value
      * @param oldEntityType The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void entityTypeUpdated(HlaMunitionPtr munition, EntityTypeStruct entityType, bool validOldEntityType, EntityTypeStruct oldEntityType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>entityIdentifier</code> is updated.
      * <br>Description from the FOM: <i>The unique identifier for the entity instance.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @param munition The object which is updated.
      * @param entityIdentifier The new value of the attribute in this update
      * @param validOldEntityIdentifier True if oldEntityIdentifier contains a valid value
      * @param oldEntityIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void entityIdentifierUpdated(HlaMunitionPtr munition, EntityIdentifierStruct entityIdentifier, bool validOldEntityIdentifier, EntityIdentifierStruct oldEntityIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>isPartOf</code> is updated.
      * <br>Description from the FOM: <i>Defines if the entity if a constituent part of another entity (denoted the host entity). If the entity is a constituent part of another entity then the HostEntityIdentifier shall be set to the EntityIdentifier of the host entity and the HostRTIObjectIdentifier shall be set to the RTI object instance ID of the host entity. If the entity is not a constituent part of another entity then the HostEntityIdentifier shall be set to 0.0.0 and the HostRTIObjectIdentifier shall be set to the empty string.</i>
      * <br>Description of the data type from the FOM: <i>Defines the spatial relationship between two objects.</i>
      *
      * @param munition The object which is updated.
      * @param isPartOf The new value of the attribute in this update
      * @param validOldIsPartOf True if oldIsPartOf contains a valid value
      * @param oldIsPartOf The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void isPartOfUpdated(HlaMunitionPtr munition, IsPartOfStruct isPartOf, bool validOldIsPartOf, IsPartOfStruct oldIsPartOf, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>spatial</code> is updated.
      * <br>Description from the FOM: <i>Spatial state stored in one variant record attribute.</i>
      * <br>Description of the data type from the FOM: <i>Variant Record for a single spatial attribute.</i>
      *
      * @param munition The object which is updated.
      * @param spatial The new value of the attribute in this update
      * @param validOldSpatial True if oldSpatial contains a valid value
      * @param oldSpatial The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void spatialUpdated(HlaMunitionPtr munition, SpatialVariantStruct spatial, bool validOldSpatial, SpatialVariantStruct oldSpatial, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>relativeSpatial</code> is updated.
      * <br>Description from the FOM: <i>Relative spatial state stored in one variant record attribute.</i>
      * <br>Description of the data type from the FOM: <i>Variant Record for a single spatial attribute.</i>
      *
      * @param munition The object which is updated.
      * @param relativeSpatial The new value of the attribute in this update
      * @param validOldRelativeSpatial True if oldRelativeSpatial contains a valid value
      * @param oldRelativeSpatial The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void relativeSpatialUpdated(HlaMunitionPtr munition, SpatialVariantStruct relativeSpatial, bool validOldRelativeSpatial, SpatialVariantStruct oldRelativeSpatial, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
      /**
      * This method is called when the producing federate of an attribute is changed.
      * Only available when using HLA Evolved.
      *
      * @param munition The object which is updated.
      * @param attribute The attribute that now has a new producing federate
      * @param oldProducingFederate The federate handle of the old producing federate
      * @param newProducingFederate The federate handle of the new producing federate
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void producingFederateUpdated(HlaMunitionPtr munition, HlaMunitionAttributes::Attribute attribute, HlaFederateIdPtr oldProducingFederate, HlaFederateIdPtr newProducingFederate, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;

      class Adapter;
   };

   /**
   * An adapter class that implements the HlaMunitionValueListener interface with empty methods.
   * It might be used as a base class for a listener.
   */
   class HlaMunitionValueListener::Adapter : public HlaMunitionValueListener {

   public:

      LIBAPI virtual void launcherFlashPresentUpdated(HlaMunitionPtr munition, bool launcherFlashPresent, bool validOldLauncherFlashPresent, bool oldLauncherFlashPresent, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void acousticSignatureIndexUpdated(HlaMunitionPtr munition, short acousticSignatureIndex, bool validOldAcousticSignatureIndex, short oldAcousticSignatureIndex, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void alternateEntityTypeUpdated(HlaMunitionPtr munition, EntityTypeStruct alternateEntityType, bool validOldAlternateEntityType, EntityTypeStruct oldAlternateEntityType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void articulatedParametersArrayUpdated(HlaMunitionPtr munition, std::vector<DevStudio::ArticulatedParameterStruct > articulatedParametersArray, bool validOldArticulatedParametersArray, std::vector<DevStudio::ArticulatedParameterStruct > oldArticulatedParametersArray, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void camouflageTypeUpdated(HlaMunitionPtr munition, CamouflageEnum::CamouflageEnum camouflageType, bool validOldCamouflageType, CamouflageEnum::CamouflageEnum oldCamouflageType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void damageStateUpdated(HlaMunitionPtr munition, DamageStatusEnum::DamageStatusEnum damageState, bool validOldDamageState, DamageStatusEnum::DamageStatusEnum oldDamageState, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void engineSmokeOnUpdated(HlaMunitionPtr munition, bool engineSmokeOn, bool validOldEngineSmokeOn, bool oldEngineSmokeOn, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void firePowerDisabledUpdated(HlaMunitionPtr munition, bool firePowerDisabled, bool validOldFirePowerDisabled, bool oldFirePowerDisabled, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void flamesPresentUpdated(HlaMunitionPtr munition, bool flamesPresent, bool validOldFlamesPresent, bool oldFlamesPresent, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void forceIdentifierUpdated(HlaMunitionPtr munition, ForceIdentifierEnum::ForceIdentifierEnum forceIdentifier, bool validOldForceIdentifier, ForceIdentifierEnum::ForceIdentifierEnum oldForceIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void hasAmmunitionSupplyCapUpdated(HlaMunitionPtr munition, bool hasAmmunitionSupplyCap, bool validOldHasAmmunitionSupplyCap, bool oldHasAmmunitionSupplyCap, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void hasFuelSupplyCapUpdated(HlaMunitionPtr munition, bool hasFuelSupplyCap, bool validOldHasFuelSupplyCap, bool oldHasFuelSupplyCap, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void hasRecoveryCapUpdated(HlaMunitionPtr munition, bool hasRecoveryCap, bool validOldHasRecoveryCap, bool oldHasRecoveryCap, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void hasRepairCapUpdated(HlaMunitionPtr munition, bool hasRepairCap, bool validOldHasRepairCap, bool oldHasRepairCap, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void immobilizedUpdated(HlaMunitionPtr munition, bool immobilized, bool validOldImmobilized, bool oldImmobilized, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void infraredSignatureIndexUpdated(HlaMunitionPtr munition, short infraredSignatureIndex, bool validOldInfraredSignatureIndex, short oldInfraredSignatureIndex, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void isConcealedUpdated(HlaMunitionPtr munition, bool isConcealed, bool validOldIsConcealed, bool oldIsConcealed, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void liveEntityMeasuredSpeedUpdated(HlaMunitionPtr munition, unsigned short liveEntityMeasuredSpeed, bool validOldLiveEntityMeasuredSpeed, unsigned short oldLiveEntityMeasuredSpeed, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void markingUpdated(HlaMunitionPtr munition, MarkingStruct marking, bool validOldMarking, MarkingStruct oldMarking, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void powerPlantOnUpdated(HlaMunitionPtr munition, bool powerPlantOn, bool validOldPowerPlantOn, bool oldPowerPlantOn, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void propulsionSystemsDataUpdated(HlaMunitionPtr munition, std::vector<DevStudio::PropulsionSystemDataStruct > propulsionSystemsData, bool validOldPropulsionSystemsData, std::vector<DevStudio::PropulsionSystemDataStruct > oldPropulsionSystemsData, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void radarCrossSectionSignatureIndexUpdated(HlaMunitionPtr munition, short radarCrossSectionSignatureIndex, bool validOldRadarCrossSectionSignatureIndex, short oldRadarCrossSectionSignatureIndex, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void smokePlumePresentUpdated(HlaMunitionPtr munition, bool smokePlumePresent, bool validOldSmokePlumePresent, bool oldSmokePlumePresent, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void tentDeployedUpdated(HlaMunitionPtr munition, bool tentDeployed, bool validOldTentDeployed, bool oldTentDeployed, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void trailingEffectsCodeUpdated(HlaMunitionPtr munition, TrailingEffectsCodeEnum::TrailingEffectsCodeEnum trailingEffectsCode, bool validOldTrailingEffectsCode, TrailingEffectsCodeEnum::TrailingEffectsCodeEnum oldTrailingEffectsCode, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void vectoringNozzleSystemDataUpdated(HlaMunitionPtr munition, std::vector<DevStudio::VectoringNozzleSystemDataStruct > vectoringNozzleSystemData, bool validOldVectoringNozzleSystemData, std::vector<DevStudio::VectoringNozzleSystemDataStruct > oldVectoringNozzleSystemData, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void entityTypeUpdated(HlaMunitionPtr munition, EntityTypeStruct entityType, bool validOldEntityType, EntityTypeStruct oldEntityType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void entityIdentifierUpdated(HlaMunitionPtr munition, EntityIdentifierStruct entityIdentifier, bool validOldEntityIdentifier, EntityIdentifierStruct oldEntityIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void isPartOfUpdated(HlaMunitionPtr munition, IsPartOfStruct isPartOf, bool validOldIsPartOf, IsPartOfStruct oldIsPartOf, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void spatialUpdated(HlaMunitionPtr munition, SpatialVariantStruct spatial, bool validOldSpatial, SpatialVariantStruct oldSpatial, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void relativeSpatialUpdated(HlaMunitionPtr munition, SpatialVariantStruct relativeSpatial, bool validOldRelativeSpatial, SpatialVariantStruct oldRelativeSpatial, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
      LIBAPI virtual void producingFederateUpdated(HlaMunitionPtr munition, HlaMunitionAttributes::Attribute attribute, HlaFederateIdPtr oldProducingFederate, HlaFederateIdPtr newProducingFederate, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
   };

}
#endif
