/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAACTIVESONARBEAMATTRIBUTES_H
#define DEVELOPER_STUDIO_HLAACTIVESONARBEAMATTRIBUTES_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <string>
#include <vector>
#include <utility>
    
#include <boost/noncopyable.hpp>
    
#include <DevStudio/datatypes/ActiveSonarScanPatternEnum.h>
#include <DevStudio/datatypes/EventIdentifierStruct.h>
#include <string>

#include <DevStudio/HlaException.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaHLAobjectRootAttributes.h>
#include <DevStudio/HlaTimeStamped.h>

namespace DevStudio {

   /**
   * Interface used to represent all attributes for an object instance.
   */
   class HlaActiveSonarBeamAttributes : public HlaHLAobjectRootAttributes {
   public:


     /**
      * An enumeration of the attributes of an HlaActiveSonarBeam
      *
      *<table summary="All attributes">
      * <tr><td><b>Enum constant</b></td><td><b>C++ name</b></td><td><b>FOM name</b></td></tr>
      * <tr><td>ACTIVE_EMISSION_PARAMETER_INDEX</td><td>activeEmissionParameterIndex</td><td><code>ActiveEmissionParameterIndex</code></td></tr>
      * <tr><td>ACTIVE_SONAR_IDENTIFIER</td><td>activeSonarIdentifier</td><td><code>ActiveSonarIdentifier</code></td></tr>
      * <tr><td>AZIMUTH_BEAMWIDTH</td><td>azimuthBeamwidth</td><td><code>AzimuthBeamwidth</code></td></tr>
      * <tr><td>AZIMUTH_CENTER</td><td>azimuthCenter</td><td><code>AzimuthCenter</code></td></tr>
      * <tr><td>BEAM_IDENTIFIER</td><td>beamIdentifier</td><td><code>BeamIdentifier</code></td></tr>
      * <tr><td>ELEVATION_BEAMWIDTH</td><td>elevationBeamwidth</td><td><code>ElevationBeamwidth</code></td></tr>
      * <tr><td>ELEVATION_CENTER</td><td>elevationCenter</td><td><code>ElevationCenter</code></td></tr>
      * <tr><td>EVENT_IDENTIFIER</td><td>eventIdentifier</td><td><code>EventIdentifier</code></td></tr>
      * <tr><td>SCAN_PATTERN</td><td>scanPattern</td><td><code>ScanPattern</code></td></tr>
      *</table>
      */
      enum Attribute {
        /**
        * activeEmissionParameterIndex (FOM name: <code>ActiveEmissionParameterIndex</code>).
        * <br>Description from the FOM: <i>An index into the database of active (intentional) underwater acoustics emissions.</i>
        */
         ACTIVE_EMISSION_PARAMETER_INDEX,

        /**
        * activeSonarIdentifier (FOM name: <code>ActiveSonarIdentifier</code>).
        * <br>Description from the FOM: <i>The RTI ID of the ActiveSonar emitting this beam</i>
        */
         ACTIVE_SONAR_IDENTIFIER,

        /**
        * azimuthBeamwidth (FOM name: <code>AzimuthBeamwidth</code>).
        * <br>Description from the FOM: <i>The horizontal width of the main beam (as opposed to any side lobes) measured from beam center to the 3 dB down point. Omni directional beams shall have a beam width of 0 radians.</i>
        */
         AZIMUTH_BEAMWIDTH,

        /**
        * azimuthCenter (FOM name: <code>AzimuthCenter</code>).
        * <br>Description from the FOM: <i>The center azimuthal bearing of the main beam (as opposed to side lobes) in relation to the own ship heading, clockwise positive. Omnidirection beams shall have an azimuthal center of 0 radians.</i>
        */
         AZIMUTH_CENTER,

        /**
        * beamIdentifier (FOM name: <code>BeamIdentifier</code>).
        * <br>Description from the FOM: <i>The identification of the active sonar beam, which must be unique on the active sonar system.</i>
        */
         BEAM_IDENTIFIER,

        /**
        * elevationBeamwidth (FOM name: <code>ElevationBeamwidth</code>).
        * <br>Description from the FOM: <i>Vertical angle from beam center to 3db down point. O is omnidirectional</i>
        */
         ELEVATION_BEAMWIDTH,

        /**
        * elevationCenter (FOM name: <code>ElevationCenter</code>).
        * <br>Description from the FOM: <i>The angle of axis of the beam center to the horizontal plane. Positive upward.</i>
        */
         ELEVATION_CENTER,

        /**
        * eventIdentifier (FOM name: <code>EventIdentifier</code>).
        * <br>Description from the FOM: <i>Used to associate changes to the beam with its ActiveSonar.</i>
        */
         EVENT_IDENTIFIER,

        /**
        * scanPattern (FOM name: <code>ScanPattern</code>).
        * <br>Description from the FOM: <i>The pattern that describes the angular movement of the sonar beam during its sweep.</i>
        */
         SCAN_PATTERN
      };

     /**
      * Gets the name of the attribute.
      *
      * @return The name of the attribute. An empty string will be returned if the attribute does not exist.
      */
      LIBAPI virtual std::wstring getName(Attribute attribute);

     /**
      * Finds the attribute specified in the parameter attributeName.
      * The found enumeration will be stored in the parameter attribute.
      *
      * @return true if the attribute was found, false otherwise.
      */
      LIBAPI virtual bool find(Attribute& attribute, std::wstring attributeName);

      LIBAPI virtual ~HlaActiveSonarBeamAttributes() {}
    
      /**
      * Returns true if the <code>activeEmissionParameterIndex</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>An index into the database of active (intentional) underwater acoustics emissions.</i>
      *
      * @return true if <code>activeEmissionParameterIndex</code> is available.
      */
      LIBAPI virtual bool hasActiveEmissionParameterIndex() = 0;

      /**
      * Gets the value of the <code>activeEmissionParameterIndex</code> attribute.
      *
      * <br>Description from the FOM: <i>An index into the database of active (intentional) underwater acoustics emissions.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^15, 2^15-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the <code>activeEmissionParameterIndex</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual short getActiveEmissionParameterIndex()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>activeEmissionParameterIndex</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>An index into the database of active (intentional) underwater acoustics emissions.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^15, 2^15-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>activeEmissionParameterIndex</code> attribute.
      */
      LIBAPI virtual short getActiveEmissionParameterIndex(short defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>activeEmissionParameterIndex</code> attribute.
      * <br>Description from the FOM: <i>An index into the database of active (intentional) underwater acoustics emissions.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^15, 2^15-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the time stamped <code>activeEmissionParameterIndex</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< short > getActiveEmissionParameterIndexTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>activeSonarIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The RTI ID of the ActiveSonar emitting this beam</i>
      *
      * @return true if <code>activeSonarIdentifier</code> is available.
      */
      LIBAPI virtual bool hasActiveSonarIdentifier() = 0;

      /**
      * Gets the value of the <code>activeSonarIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>The RTI ID of the ActiveSonar emitting this beam</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @return the <code>activeSonarIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::string getActiveSonarIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>activeSonarIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The RTI ID of the ActiveSonar emitting this beam</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>activeSonarIdentifier</code> attribute.
      */
      LIBAPI virtual std::string getActiveSonarIdentifier(std::string defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>activeSonarIdentifier</code> attribute.
      * <br>Description from the FOM: <i>The RTI ID of the ActiveSonar emitting this beam</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @return the time stamped <code>activeSonarIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::string > getActiveSonarIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>azimuthBeamwidth</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The horizontal width of the main beam (as opposed to any side lobes) measured from beam center to the 3 dB down point. Omni directional beams shall have a beam width of 0 radians.</i>
      *
      * @return true if <code>azimuthBeamwidth</code> is available.
      */
      LIBAPI virtual bool hasAzimuthBeamwidth() = 0;

      /**
      * Gets the value of the <code>azimuthBeamwidth</code> attribute.
      *
      * <br>Description from the FOM: <i>The horizontal width of the main beam (as opposed to any side lobes) measured from beam center to the 3 dB down point. Omni directional beams shall have a beam width of 0 radians.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @return the <code>azimuthBeamwidth</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual float getAzimuthBeamwidth()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>azimuthBeamwidth</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The horizontal width of the main beam (as opposed to any side lobes) measured from beam center to the 3 dB down point. Omni directional beams shall have a beam width of 0 radians.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>azimuthBeamwidth</code> attribute.
      */
      LIBAPI virtual float getAzimuthBeamwidth(float defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>azimuthBeamwidth</code> attribute.
      * <br>Description from the FOM: <i>The horizontal width of the main beam (as opposed to any side lobes) measured from beam center to the 3 dB down point. Omni directional beams shall have a beam width of 0 radians.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @return the time stamped <code>azimuthBeamwidth</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< float > getAzimuthBeamwidthTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>azimuthCenter</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The center azimuthal bearing of the main beam (as opposed to side lobes) in relation to the own ship heading, clockwise positive. Omnidirection beams shall have an azimuthal center of 0 radians.</i>
      *
      * @return true if <code>azimuthCenter</code> is available.
      */
      LIBAPI virtual bool hasAzimuthCenter() = 0;

      /**
      * Gets the value of the <code>azimuthCenter</code> attribute.
      *
      * <br>Description from the FOM: <i>The center azimuthal bearing of the main beam (as opposed to side lobes) in relation to the own ship heading, clockwise positive. Omnidirection beams shall have an azimuthal center of 0 radians.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @return the <code>azimuthCenter</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual float getAzimuthCenter()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>azimuthCenter</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The center azimuthal bearing of the main beam (as opposed to side lobes) in relation to the own ship heading, clockwise positive. Omnidirection beams shall have an azimuthal center of 0 radians.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>azimuthCenter</code> attribute.
      */
      LIBAPI virtual float getAzimuthCenter(float defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>azimuthCenter</code> attribute.
      * <br>Description from the FOM: <i>The center azimuthal bearing of the main beam (as opposed to side lobes) in relation to the own ship heading, clockwise positive. Omnidirection beams shall have an azimuthal center of 0 radians.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @return the time stamped <code>azimuthCenter</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< float > getAzimuthCenterTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>beamIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The identification of the active sonar beam, which must be unique on the active sonar system.</i>
      *
      * @return true if <code>beamIdentifier</code> is available.
      */
      LIBAPI virtual bool hasBeamIdentifier() = 0;

      /**
      * Gets the value of the <code>beamIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>The identification of the active sonar beam, which must be unique on the active sonar system.</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the <code>beamIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual char getBeamIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>beamIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The identification of the active sonar beam, which must be unique on the active sonar system.</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>beamIdentifier</code> attribute.
      */
      LIBAPI virtual char getBeamIdentifier(char defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>beamIdentifier</code> attribute.
      * <br>Description from the FOM: <i>The identification of the active sonar beam, which must be unique on the active sonar system.</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the time stamped <code>beamIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< char > getBeamIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>elevationBeamwidth</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Vertical angle from beam center to 3db down point. O is omnidirectional</i>
      *
      * @return true if <code>elevationBeamwidth</code> is available.
      */
      LIBAPI virtual bool hasElevationBeamwidth() = 0;

      /**
      * Gets the value of the <code>elevationBeamwidth</code> attribute.
      *
      * <br>Description from the FOM: <i>Vertical angle from beam center to 3db down point. O is omnidirectional</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @return the <code>elevationBeamwidth</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual float getElevationBeamwidth()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>elevationBeamwidth</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Vertical angle from beam center to 3db down point. O is omnidirectional</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>elevationBeamwidth</code> attribute.
      */
      LIBAPI virtual float getElevationBeamwidth(float defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>elevationBeamwidth</code> attribute.
      * <br>Description from the FOM: <i>Vertical angle from beam center to 3db down point. O is omnidirectional</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @return the time stamped <code>elevationBeamwidth</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< float > getElevationBeamwidthTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>elevationCenter</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The angle of axis of the beam center to the horizontal plane. Positive upward.</i>
      *
      * @return true if <code>elevationCenter</code> is available.
      */
      LIBAPI virtual bool hasElevationCenter() = 0;

      /**
      * Gets the value of the <code>elevationCenter</code> attribute.
      *
      * <br>Description from the FOM: <i>The angle of axis of the beam center to the horizontal plane. Positive upward.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @return the <code>elevationCenter</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual float getElevationCenter()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>elevationCenter</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The angle of axis of the beam center to the horizontal plane. Positive upward.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>elevationCenter</code> attribute.
      */
      LIBAPI virtual float getElevationCenter(float defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>elevationCenter</code> attribute.
      * <br>Description from the FOM: <i>The angle of axis of the beam center to the horizontal plane. Positive upward.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @return the time stamped <code>elevationCenter</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< float > getElevationCenterTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>eventIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Used to associate changes to the beam with its ActiveSonar.</i>
      *
      * @return true if <code>eventIdentifier</code> is available.
      */
      LIBAPI virtual bool hasEventIdentifier() = 0;

      /**
      * Gets the value of the <code>eventIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>Used to associate changes to the beam with its ActiveSonar.</i>
      * <br>Description of the data type from the FOM: <i>Identification of an event. Based on the Event Identifier record as specified in IEEE 1278.1-1995 section 5.2.18.</i>
      *
      * @return the <code>eventIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::EventIdentifierStruct getEventIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>eventIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Used to associate changes to the beam with its ActiveSonar.</i>
      * <br>Description of the data type from the FOM: <i>Identification of an event. Based on the Event Identifier record as specified in IEEE 1278.1-1995 section 5.2.18.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>eventIdentifier</code> attribute.
      */
      LIBAPI virtual DevStudio::EventIdentifierStruct getEventIdentifier(DevStudio::EventIdentifierStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>eventIdentifier</code> attribute.
      * <br>Description from the FOM: <i>Used to associate changes to the beam with its ActiveSonar.</i>
      * <br>Description of the data type from the FOM: <i>Identification of an event. Based on the Event Identifier record as specified in IEEE 1278.1-1995 section 5.2.18.</i>
      *
      * @return the time stamped <code>eventIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::EventIdentifierStruct > getEventIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>scanPattern</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The pattern that describes the angular movement of the sonar beam during its sweep.</i>
      *
      * @return true if <code>scanPattern</code> is available.
      */
      LIBAPI virtual bool hasScanPattern() = 0;

      /**
      * Gets the value of the <code>scanPattern</code> attribute.
      *
      * <br>Description from the FOM: <i>The pattern that describes the angular movement of the sonar beam during its sweep.</i>
      * <br>Description of the data type from the FOM: <i>Acoustic scan pattern</i>
      *
      * @return the <code>scanPattern</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::ActiveSonarScanPatternEnum::ActiveSonarScanPatternEnum getScanPattern()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>scanPattern</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The pattern that describes the angular movement of the sonar beam during its sweep.</i>
      * <br>Description of the data type from the FOM: <i>Acoustic scan pattern</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>scanPattern</code> attribute.
      */
      LIBAPI virtual DevStudio::ActiveSonarScanPatternEnum::ActiveSonarScanPatternEnum getScanPattern(DevStudio::ActiveSonarScanPatternEnum::ActiveSonarScanPatternEnum defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>scanPattern</code> attribute.
      * <br>Description from the FOM: <i>The pattern that describes the angular movement of the sonar beam during its sweep.</i>
      * <br>Description of the data type from the FOM: <i>Acoustic scan pattern</i>
      *
      * @return the time stamped <code>scanPattern</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::ActiveSonarScanPatternEnum::ActiveSonarScanPatternEnum > getScanPatternTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
   };
}
#endif
