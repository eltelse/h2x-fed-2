/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLATHREATSPOTOBJECTLISTENER_H
#define DEVELOPER_STUDIO_HLATHREATSPOTOBJECTLISTENER_H

#include <set>


#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaThreatSpotObjectAttributes.h>

namespace DevStudio {
   /**
   * Listener for updates of attributes.  
   */
   class HlaThreatSpotObjectListener {
   public:

      LIBAPI virtual ~HlaThreatSpotObjectListener() {}

      /**
      * This method is called when a HLA <code>reflectAttributeValueUpdate</code> is received for an remote object,
      * or a set of attributes is updated on a local object.
      *
      * @param threatSpotObject The threatSpotObject which this update corresponds to.
      * @param attributes The set of attributes that are updated.
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time the update was initiated.
      */
      LIBAPI virtual void attributesUpdated(HlaThreatSpotObjectPtr threatSpotObject, std::set<HlaThreatSpotObjectAttributes::Attribute> &attributes, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

      class Adapter;
   };

  /**
  * An adapter class that implements the HlaThreatSpotObjectListener interface with empty methods.
  * It might be used as a base class for a listener.
  */
  class HlaThreatSpotObjectListener::Adapter : public HlaThreatSpotObjectListener {

  public:

     LIBAPI virtual void attributesUpdated(HlaThreatSpotObjectPtr threatSpotObject, std::set<HlaThreatSpotObjectAttributes::Attribute> &attributes, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
    };

}
#endif
