/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAMINEFIELDOBJECTMANAGER_H
#define DEVELOPER_STUDIO_HLAMINEFIELDOBJECTMANAGER_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <DevStudio/datatypes/BreachedStatusEnum.h>
#include <DevStudio/datatypes/DamageStatusEnum.h>
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EnvironmentObjectTypeStruct.h>
#include <DevStudio/datatypes/ForceIdentifierEnum.h>
#include <DevStudio/datatypes/WorldLocationStruct.h>
#include <DevStudio/datatypes/WorldLocationStructLengthlessArray.h>
#include <string>
#include <vector>

#include <list>
#include <vector>

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaMinefieldObjectManagerListener.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaException.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

   /**
   * Manager used to manage HlaMinefieldObjects.
   */
   class HlaMinefieldObjectManager : private boost::noncopyable {

   public:

      LIBAPI virtual ~HlaMinefieldObjectManager() {}

      /**
      * Gets a list of all HlaMinefieldObjects within interest, both local and remote.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of HlaMinefieldObjects
      */
      LIBAPI virtual std::list<HlaMinefieldObjectPtr> getHlaMinefieldObjects() = 0;

      /**
      * Gets a list of all HlaMinefieldObjects, both local and remote.
      * HlaMinefieldObjects not within interest are also included.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of all HlaMinefieldObjects
      */
      LIBAPI virtual std::list<HlaMinefieldObjectPtr> getAllHlaMinefieldObjects() = 0;

      /**
      * Gets a list of local HlaMinefieldObjects within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of local HlaMinefieldObjects
      */
      LIBAPI virtual std::list<HlaMinefieldObjectPtr> getLocalHlaMinefieldObjects() = 0;

      /**
      * Gets a list of remote HlaMinefieldObjects within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of remote HlaMinefieldObjects
      */
      LIBAPI virtual std::list<HlaMinefieldObjectPtr> getRemoteHlaMinefieldObjects() = 0;

      /**
      * Find a HlaMinefieldObject with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> of the HlaMinefieldObject to find
      *
      * @return the specified HlaMinefieldObject, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaMinefieldObjectPtr getMinefieldObjectByHlaInstanceName(const std::wstring& hlaInstanceName) = 0;

      /**
      * Find a HlaMinefieldObject with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param encodedHlaInstanceHandle the encoded <code>HLA instance handle</code> of the HlaMinefieldObject to find
      *
      * @return the specified HlaMinefieldObject, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaMinefieldObjectPtr getMinefieldObjectByHlaInstanceHandle(const std::vector<char>& encodedHlaInstanceHandle) = 0;

      /**
      * Creates a new HlaMinefieldObject, and sets all <i>Create</i> attributes.
      *
      * @return Returns a new local HlaMinefieldObject.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaMinefieldObjectPtr createLocalHlaMinefieldObject(
      ) THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Creates a new HlaMinefieldObject with a specified <code>HLA instance name</code>, and sets all
      * <i>Create</i> attributes.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> to register for this instance
      *
      * @return Returns a new local HlaMinefieldObject.
      *
      * @throws HlaIllegalInstanceNameException if the name is illegal
      * @throws HlaInstanceNameInUseException if the name is already in use
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaMinefieldObjectPtr createLocalHlaMinefieldObject(
         const std::wstring& hlaInstanceName
      ) THROW_SPEC (HlaIllegalInstanceNameException, HlaInstanceNameInUseException,
                    HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaMinefieldObject and removes it from the federation.
      *
      * @param minefieldObject The HlaMinefieldObject to delete
      *
      * @return the HlaMinefieldObject deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaMinefieldObjectPtr deleteLocalHlaMinefieldObject(HlaMinefieldObjectPtr minefieldObject)
          THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                      HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaMinefieldObject and removes it from the federation.
      *
      * @param minefieldObject The HlaMinefieldObject to delete
      * @param timestamp timestamp when the instance was removed.
      *
      * @return the HlaMinefieldObject deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaMinefieldObjectPtr deleteLocalHlaMinefieldObject(HlaMinefieldObjectPtr minefieldObject, HlaTimeStampPtr timestamp)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaMinefieldObject and removes it from the federation.
      *
      * @param minefieldObject The HlaMinefieldObject to delete
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaMinefieldObject deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaMinefieldObjectPtr deleteLocalHlaMinefieldObject(HlaMinefieldObjectPtr minefieldObject, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaMinefieldObject and removes it from the federation.
      *
      * @param minefieldObject The HlaMinefieldObject to delete
      * @param timestamp timestamp when the instance was removed.
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaMinefieldObject deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaMinefieldObjectPtr deleteLocalHlaMinefieldObject(HlaMinefieldObjectPtr minefieldObject, HlaTimeStampPtr timestamp, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Add a HlaMinefieldObject manager listener.
      *
      * @param managerListener listener to add
      */
      LIBAPI virtual void addHlaMinefieldObjectManagerListener(HlaMinefieldObjectManagerListenerPtr managerListener) = 0;

      /**
      * Removed a HlaMinefieldObject manager listener.
      *
      * @param managerListener listener to remove
      */
      LIBAPI virtual void removeHlaMinefieldObjectManagerListener(HlaMinefieldObjectManagerListenerPtr managerListener) = 0;

     /**
      * Add a listener that will automatically be added to all instances of HlaMinefieldObject (Both local and remote).
      * The listener is notified when any attribute of an instance of HlaMinefieldObject is updated.
      * The listener is also called when an interaction is sent to an instance of HlaMinefieldObject.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param listener The listener to add.
      */
      LIBAPI virtual void addHlaMinefieldObjectDefaultInstanceListener(HlaMinefieldObjectListenerPtr listener) = 0;

     /**
      * Remove a listener from the set of listeners that are automatically added to new instances of HlaMinefieldObject.
      * Note: The listener will not be removed from already existing instances of HlaMinefieldObject.
      * This method is idempotent.
      *
      * @param listener The listener to remove.
      */
      LIBAPI virtual void removeHlaMinefieldObjectDefaultInstanceListener(HlaMinefieldObjectListenerPtr listener) = 0;

     /**
      * Add a valueListener that will automatically be added to all instances of HlaMinefieldObject (Both local and remote).
      * The  Valuelistener is notified when any attribute of an instance of HlaMinefieldObject is updated.
      * Note that the attribute values that are notified are not guaranteed to
      * be the latest available values for that attribute.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to add.
      */
      LIBAPI virtual void addHlaMinefieldObjectDefaultInstanceValueListener(HlaMinefieldObjectValueListenerPtr valueListener) = 0;

     /**
      * Remove a valueListener from the set of listeners that are automatically added to new instances of HlaMinefieldObject.
      * Note: The valueListener will not be removed from already existing instances of HlaMinefieldObject.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to remove.
      */
      LIBAPI virtual void removeHlaMinefieldObjectDefaultInstanceValueListener(HlaMinefieldObjectValueListenerPtr valueListener) = 0;

     /**
      * Enables or disables the HlaMinefieldObject manager.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaMinefieldObject manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      *
      * @param enabled <code>true</code> to enable the HlaMinefieldObject manager, otherwise <code>false</code>
      */
      LIBAPI virtual void setEnabled(bool enabled) = 0;

     /**
      * Enables the HlaMinefieldObject manager if it is avalable in the FOM at runtime.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaMinefieldObject manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      */
      LIBAPI virtual void setEnabledIfAvailableInFom() = 0;

     /**
      * Check if the HlaMinefieldObject manager is actually enabled when connected.
      * An HlaMinefieldObject manager may be disabled with the <code>setEnabled(false)</code> function, or it may be configured
      * as <i>if available in the FOM</i> by the <code>setEnabledIfAvailableInFom()</code> function and
      * then not available in the FOM at runtime.
      * <p>
      * This method should only be used when connected, it will always return <code>false</code>
      * when not connected.
      *
      * @return <code>true</code> if connected and the HlaMinefieldObject manager is enabled.
      */
      LIBAPI virtual bool isEnabled() = 0;
   };
}
#endif
