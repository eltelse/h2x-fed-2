/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLASUBMERSIBLEVESSELUPDATER_H
#define DEVELOPER_STUDIO_HLASUBMERSIBLEVESSELUPDATER_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <boost/noncopyable.hpp>

#include <DevStudio/datatypes/ArticulatedParameterStruct.h>
#include <DevStudio/datatypes/ArticulatedParameterStructLengthlessArray.h>
#include <DevStudio/datatypes/CamouflageEnum.h>
#include <DevStudio/datatypes/DamageStatusEnum.h>
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EntityTypeStruct.h>
#include <DevStudio/datatypes/ForceIdentifierEnum.h>
#include <DevStudio/datatypes/HatchStateEnum.h>
#include <DevStudio/datatypes/IsPartOfStruct.h>
#include <DevStudio/datatypes/MarkingStruct.h>
#include <DevStudio/datatypes/PropulsionSystemDataStruct.h>
#include <DevStudio/datatypes/PropulsionSystemDataStructLengthlessArray.h>
#include <DevStudio/datatypes/SpatialVariantStruct.h>
#include <DevStudio/datatypes/TrailingEffectsCodeEnum.h>
#include <DevStudio/datatypes/VectoringNozzleSystemDataStruct.h>
#include <DevStudio/datatypes/VectoringNozzleSystemDataStructLengthlessArray.h>
#include <vector>

#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaException.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaPlatformUpdater.h>

namespace DevStudio {

    /**
    * Updater used to update attribute values.
    */
    class HlaSubmersibleVesselUpdater : public HlaPlatformUpdater {

    public:

    LIBAPI virtual ~HlaSubmersibleVesselUpdater() {}

    /**
    * Set the afterburnerOn for this update.
    * <br>Description from the FOM: <i>Whether the entity's afterburner is on or not.</i>
    * <br>Description of the data type from the FOM: <i></i>
    *
    * @param afterburnerOn the new afterburnerOn
    */
    LIBAPI virtual void setAfterburnerOn(const bool& afterburnerOn) = 0;

    /**
    * Set the antiCollisionLightsOn for this update.
    * <br>Description from the FOM: <i>Whether the entity's anti-collision lights are on or not.</i>
    * <br>Description of the data type from the FOM: <i></i>
    *
    * @param antiCollisionLightsOn the new antiCollisionLightsOn
    */
    LIBAPI virtual void setAntiCollisionLightsOn(const bool& antiCollisionLightsOn) = 0;

    /**
    * Set the blackOutBrakeLightsOn for this update.
    * <br>Description from the FOM: <i>Whether the entity's black out brake lights are on or not.</i>
    * <br>Description of the data type from the FOM: <i></i>
    *
    * @param blackOutBrakeLightsOn the new blackOutBrakeLightsOn
    */
    LIBAPI virtual void setBlackOutBrakeLightsOn(const bool& blackOutBrakeLightsOn) = 0;

    /**
    * Set the blackOutLightsOn for this update.
    * <br>Description from the FOM: <i>Whether the entity's black out lights are on or not.</i>
    * <br>Description of the data type from the FOM: <i></i>
    *
    * @param blackOutLightsOn the new blackOutLightsOn
    */
    LIBAPI virtual void setBlackOutLightsOn(const bool& blackOutLightsOn) = 0;

    /**
    * Set the brakeLightsOn for this update.
    * <br>Description from the FOM: <i>Whether the entity's brake lights are on or not.</i>
    * <br>Description of the data type from the FOM: <i></i>
    *
    * @param brakeLightsOn the new brakeLightsOn
    */
    LIBAPI virtual void setBrakeLightsOn(const bool& brakeLightsOn) = 0;

    /**
    * Set the formationLightsOn for this update.
    * <br>Description from the FOM: <i>Whether the entity's formation lights are on or not.</i>
    * <br>Description of the data type from the FOM: <i></i>
    *
    * @param formationLightsOn the new formationLightsOn
    */
    LIBAPI virtual void setFormationLightsOn(const bool& formationLightsOn) = 0;

    /**
    * Set the hatchState for this update.
    * <br>Description from the FOM: <i>The state of the entity's (main) hatch.</i>
    * <br>Description of the data type from the FOM: <i>Hatch state</i>
    *
    * @param hatchState the new hatchState
    */
    LIBAPI virtual void setHatchState(const DevStudio::HatchStateEnum::HatchStateEnum& hatchState) = 0;

    /**
    * Set the headLightsOn for this update.
    * <br>Description from the FOM: <i>Whether the entity's headlights are on or not.</i>
    * <br>Description of the data type from the FOM: <i></i>
    *
    * @param headLightsOn the new headLightsOn
    */
    LIBAPI virtual void setHeadLightsOn(const bool& headLightsOn) = 0;

    /**
    * Set the interiorLightsOn for this update.
    * <br>Description from the FOM: <i>Whether the entity's internal lights are on or not.</i>
    * <br>Description of the data type from the FOM: <i></i>
    *
    * @param interiorLightsOn the new interiorLightsOn
    */
    LIBAPI virtual void setInteriorLightsOn(const bool& interiorLightsOn) = 0;

    /**
    * Set the landingLightsOn for this update.
    * <br>Description from the FOM: <i>Whether the entity's landing lights are on or not.</i>
    * <br>Description of the data type from the FOM: <i></i>
    *
    * @param landingLightsOn the new landingLightsOn
    */
    LIBAPI virtual void setLandingLightsOn(const bool& landingLightsOn) = 0;

    /**
    * Set the launcherRaised for this update.
    * <br>Description from the FOM: <i>Whether the entity's weapon launcher is in the raised position.</i>
    * <br>Description of the data type from the FOM: <i></i>
    *
    * @param launcherRaised the new launcherRaised
    */
    LIBAPI virtual void setLauncherRaised(const bool& launcherRaised) = 0;

    /**
    * Set the navigationLightsOn for this update.
    * <br>Description from the FOM: <i>Whether the entity's navigation lights are on or not.</i>
    * <br>Description of the data type from the FOM: <i></i>
    *
    * @param navigationLightsOn the new navigationLightsOn
    */
    LIBAPI virtual void setNavigationLightsOn(const bool& navigationLightsOn) = 0;

    /**
    * Set the rampDeployed for this update.
    * <br>Description from the FOM: <i>Whether the entity has deployed a ramp or not.</i>
    * <br>Description of the data type from the FOM: <i></i>
    *
    * @param rampDeployed the new rampDeployed
    */
    LIBAPI virtual void setRampDeployed(const bool& rampDeployed) = 0;

    /**
    * Set the runningLightsOn for this update.
    * <br>Description from the FOM: <i>Whether the entity's running lights are on or not.</i>
    * <br>Description of the data type from the FOM: <i></i>
    *
    * @param runningLightsOn the new runningLightsOn
    */
    LIBAPI virtual void setRunningLightsOn(const bool& runningLightsOn) = 0;

    /**
    * Set the spotLightsOn for this update.
    * <br>Description from the FOM: <i>Whether the entity's spotlights are on or not.</i>
    * <br>Description of the data type from the FOM: <i></i>
    *
    * @param spotLightsOn the new spotLightsOn
    */
    LIBAPI virtual void setSpotLightsOn(const bool& spotLightsOn) = 0;

    /**
    * Set the tailLightsOn for this update.
    * <br>Description from the FOM: <i>Whether the entity's tail lights are on or not.</i>
    * <br>Description of the data type from the FOM: <i></i>
    *
    * @param tailLightsOn the new tailLightsOn
    */
    LIBAPI virtual void setTailLightsOn(const bool& tailLightsOn) = 0;

    /**
    * Set the acousticSignatureIndex for this update.
    * <br>Description from the FOM: <i>Index used to obtain the acoustics (sound through air) signature state of the entity.</i>
    * <br>Description of the data type from the FOM: <i>Integer in the range [-2^15, 2^15-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
    *
    * @param acousticSignatureIndex the new acousticSignatureIndex
    */
    LIBAPI virtual void setAcousticSignatureIndex(const short& acousticSignatureIndex) = 0;

    /**
    * Set the alternateEntityType for this update.
    * <br>Description from the FOM: <i>The category of entity to be used when viewed by entities on the 'opposite' side.</i>
    * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
    *
    * @param alternateEntityType the new alternateEntityType
    */
    LIBAPI virtual void setAlternateEntityType(const DevStudio::EntityTypeStruct& alternateEntityType) = 0;

    /**
    * Set the articulatedParametersArray for this update.
    * <br>Description from the FOM: <i>Identification of the visible parts, and their states, of the entity which are capable of independent motion.</i>
    * <br>Description of the data type from the FOM: <i>Dynamic array of ArticulatedParameterStruct elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
    *
    * @param articulatedParametersArray the new articulatedParametersArray
    */
    LIBAPI virtual void setArticulatedParametersArray(const std::vector<DevStudio::ArticulatedParameterStruct >& articulatedParametersArray) = 0;

    /**
    * Set the camouflageType for this update.
    * <br>Description from the FOM: <i>The type of camouflage in use (if any).</i>
    * <br>Description of the data type from the FOM: <i>Camouflage type</i>
    *
    * @param camouflageType the new camouflageType
    */
    LIBAPI virtual void setCamouflageType(const DevStudio::CamouflageEnum::CamouflageEnum& camouflageType) = 0;

    /**
    * Set the damageState for this update.
    * <br>Description from the FOM: <i>The state of damage of the entity.</i>
    * <br>Description of the data type from the FOM: <i>Damaged appearance</i>
    *
    * @param damageState the new damageState
    */
    LIBAPI virtual void setDamageState(const DevStudio::DamageStatusEnum::DamageStatusEnum& damageState) = 0;

    /**
    * Set the engineSmokeOn for this update.
    * <br>Description from the FOM: <i>Whether the entity's engine is generating smoke or not.</i>
    * <br>Description of the data type from the FOM: <i></i>
    *
    * @param engineSmokeOn the new engineSmokeOn
    */
    LIBAPI virtual void setEngineSmokeOn(const bool& engineSmokeOn) = 0;

    /**
    * Set the firePowerDisabled for this update.
    * <br>Description from the FOM: <i>Whether the entity's main weapon system has been disabled or not.</i>
    * <br>Description of the data type from the FOM: <i></i>
    *
    * @param firePowerDisabled the new firePowerDisabled
    */
    LIBAPI virtual void setFirePowerDisabled(const bool& firePowerDisabled) = 0;

    /**
    * Set the flamesPresent for this update.
    * <br>Description from the FOM: <i>Whether the entity is on fire (with visible flames) or not.</i>
    * <br>Description of the data type from the FOM: <i></i>
    *
    * @param flamesPresent the new flamesPresent
    */
    LIBAPI virtual void setFlamesPresent(const bool& flamesPresent) = 0;

    /**
    * Set the forceIdentifier for this update.
    * <br>Description from the FOM: <i>The identification of the force that the entity belongs to.</i>
    * <br>Description of the data type from the FOM: <i>Force ID</i>
    *
    * @param forceIdentifier the new forceIdentifier
    */
    LIBAPI virtual void setForceIdentifier(const DevStudio::ForceIdentifierEnum::ForceIdentifierEnum& forceIdentifier) = 0;

    /**
    * Set the hasAmmunitionSupplyCap for this update.
    * <br>Description from the FOM: <i>Whether the entity has the capability to supply other entities with ammunition.</i>
    * <br>Description of the data type from the FOM: <i></i>
    *
    * @param hasAmmunitionSupplyCap the new hasAmmunitionSupplyCap
    */
    LIBAPI virtual void setHasAmmunitionSupplyCap(const bool& hasAmmunitionSupplyCap) = 0;

    /**
    * Set the hasFuelSupplyCap for this update.
    * <br>Description from the FOM: <i>Whether the entity has the capability to supply other entities with fuel or not.</i>
    * <br>Description of the data type from the FOM: <i></i>
    *
    * @param hasFuelSupplyCap the new hasFuelSupplyCap
    */
    LIBAPI virtual void setHasFuelSupplyCap(const bool& hasFuelSupplyCap) = 0;

    /**
    * Set the hasRecoveryCap for this update.
    * <br>Description from the FOM: <i>Whether the entity has the capability to recover other entities or not.</i>
    * <br>Description of the data type from the FOM: <i></i>
    *
    * @param hasRecoveryCap the new hasRecoveryCap
    */
    LIBAPI virtual void setHasRecoveryCap(const bool& hasRecoveryCap) = 0;

    /**
    * Set the hasRepairCap for this update.
    * <br>Description from the FOM: <i>Whether the entity has the capability to repair other entities or not.</i>
    * <br>Description of the data type from the FOM: <i></i>
    *
    * @param hasRepairCap the new hasRepairCap
    */
    LIBAPI virtual void setHasRepairCap(const bool& hasRepairCap) = 0;

    /**
    * Set the immobilized for this update.
    * <br>Description from the FOM: <i>Whether the entity is immobilized or not.</i>
    * <br>Description of the data type from the FOM: <i></i>
    *
    * @param immobilized the new immobilized
    */
    LIBAPI virtual void setImmobilized(const bool& immobilized) = 0;

    /**
    * Set the infraredSignatureIndex for this update.
    * <br>Description from the FOM: <i>Index used to obtain the infra-red signature state of the entity.</i>
    * <br>Description of the data type from the FOM: <i>Integer in the range [-2^15, 2^15-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
    *
    * @param infraredSignatureIndex the new infraredSignatureIndex
    */
    LIBAPI virtual void setInfraredSignatureIndex(const short& infraredSignatureIndex) = 0;

    /**
    * Set the isConcealed for this update.
    * <br>Description from the FOM: <i>Whether the entity is concealed or not.</i>
    * <br>Description of the data type from the FOM: <i></i>
    *
    * @param isConcealed the new isConcealed
    */
    LIBAPI virtual void setIsConcealed(const bool& isConcealed) = 0;

    /**
    * Set the liveEntityMeasuredSpeed for this update.
    * <br>Description from the FOM: <i>The entity's own measurement of speed (e.g. air speed for aircraft).</i>
    * <br>Description of the data type from the FOM: <i>Velocity/Speed measured in decimeter per second. [unit: decimeter per second (dm/s), resolution: 1, accuracy: perfect]</i>
    *
    * @param liveEntityMeasuredSpeed the new liveEntityMeasuredSpeed
    */
    LIBAPI virtual void setLiveEntityMeasuredSpeed(const unsigned short& liveEntityMeasuredSpeed) = 0;

    /**
    * Set the marking for this update.
    * <br>Description from the FOM: <i>A unique marking or combination of characters used to distinguish the entity from other entities.</i>
    * <br>Description of the data type from the FOM: <i>Character set used in the marking and the string of characters to be interpreted for display.</i>
    *
    * @param marking the new marking
    */
    LIBAPI virtual void setMarking(const DevStudio::MarkingStruct& marking) = 0;

    /**
    * Set the powerPlantOn for this update.
    * <br>Description from the FOM: <i>Whether the entity's power plant is on or not.</i>
    * <br>Description of the data type from the FOM: <i></i>
    *
    * @param powerPlantOn the new powerPlantOn
    */
    LIBAPI virtual void setPowerPlantOn(const bool& powerPlantOn) = 0;

    /**
    * Set the propulsionSystemsData for this update.
    * <br>Description from the FOM: <i>The basic operating data of the propulsion systems aboard the entity.</i>
    * <br>Description of the data type from the FOM: <i>A set of Propulsion System Data descriptions.</i>
    *
    * @param propulsionSystemsData the new propulsionSystemsData
    */
    LIBAPI virtual void setPropulsionSystemsData(const std::vector<DevStudio::PropulsionSystemDataStruct >& propulsionSystemsData) = 0;

    /**
    * Set the radarCrossSectionSignatureIndex for this update.
    * <br>Description from the FOM: <i>Index used to obtain the radar cross section signature state of the entity.</i>
    * <br>Description of the data type from the FOM: <i>Integer in the range [-2^15, 2^15-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
    *
    * @param radarCrossSectionSignatureIndex the new radarCrossSectionSignatureIndex
    */
    LIBAPI virtual void setRadarCrossSectionSignatureIndex(const short& radarCrossSectionSignatureIndex) = 0;

    /**
    * Set the smokePlumePresent for this update.
    * <br>Description from the FOM: <i>Whether the entity is generating smoke or not (intentional or unintentional).</i>
    * <br>Description of the data type from the FOM: <i></i>
    *
    * @param smokePlumePresent the new smokePlumePresent
    */
    LIBAPI virtual void setSmokePlumePresent(const bool& smokePlumePresent) = 0;

    /**
    * Set the tentDeployed for this update.
    * <br>Description from the FOM: <i>Whether the entity has deployed tent or not.</i>
    * <br>Description of the data type from the FOM: <i></i>
    *
    * @param tentDeployed the new tentDeployed
    */
    LIBAPI virtual void setTentDeployed(const bool& tentDeployed) = 0;

    /**
    * Set the trailingEffectsCode for this update.
    * <br>Description from the FOM: <i>The type and size of any trail that the entity is making.</i>
    * <br>Description of the data type from the FOM: <i>Size of trailing effect</i>
    *
    * @param trailingEffectsCode the new trailingEffectsCode
    */
    LIBAPI virtual void setTrailingEffectsCode(const DevStudio::TrailingEffectsCodeEnum::TrailingEffectsCodeEnum& trailingEffectsCode) = 0;

    /**
    * Set the vectoringNozzleSystemData for this update.
    * <br>Description from the FOM: <i>The basic operational data for the vectoring nozzle systems aboard the entity.</i>
    * <br>Description of the data type from the FOM: <i>A set of Vectoring Nozzle System Data description.</i>
    *
    * @param vectoringNozzleSystemData the new vectoringNozzleSystemData
    */
    LIBAPI virtual void setVectoringNozzleSystemData(const std::vector<DevStudio::VectoringNozzleSystemDataStruct >& vectoringNozzleSystemData) = 0;

    /**
    * Set the entityType for this update.
    * <br>Description from the FOM: <i>The category of the entity.</i>
    * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
    *
    * @param entityType the new entityType
    */
    LIBAPI virtual void setEntityType(const DevStudio::EntityTypeStruct& entityType) = 0;

    /**
    * Set the entityIdentifier for this update.
    * <br>Description from the FOM: <i>The unique identifier for the entity instance.</i>
    * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
    *
    * @param entityIdentifier the new entityIdentifier
    */
    LIBAPI virtual void setEntityIdentifier(const DevStudio::EntityIdentifierStruct& entityIdentifier) = 0;

    /**
    * Set the isPartOf for this update.
    * <br>Description from the FOM: <i>Defines if the entity if a constituent part of another entity (denoted the host entity). If the entity is a constituent part of another entity then the HostEntityIdentifier shall be set to the EntityIdentifier of the host entity and the HostRTIObjectIdentifier shall be set to the RTI object instance ID of the host entity. If the entity is not a constituent part of another entity then the HostEntityIdentifier shall be set to 0.0.0 and the HostRTIObjectIdentifier shall be set to the empty string.</i>
    * <br>Description of the data type from the FOM: <i>Defines the spatial relationship between two objects.</i>
    *
    * @param isPartOf the new isPartOf
    */
    LIBAPI virtual void setIsPartOf(const DevStudio::IsPartOfStruct& isPartOf) = 0;

    /**
    * Set the spatial for this update.
    * <br>Description from the FOM: <i>Spatial state stored in one variant record attribute.</i>
    * <br>Description of the data type from the FOM: <i>Variant Record for a single spatial attribute.</i>
    *
    * @param spatial the new spatial
    */
    LIBAPI virtual void setSpatial(const DevStudio::SpatialVariantStruct& spatial) = 0;

    /**
    * Set the relativeSpatial for this update.
    * <br>Description from the FOM: <i>Relative spatial state stored in one variant record attribute.</i>
    * <br>Description of the data type from the FOM: <i>Variant Record for a single spatial attribute.</i>
    *
    * @param relativeSpatial the new relativeSpatial
    */
    LIBAPI virtual void setRelativeSpatial(const DevStudio::SpatialVariantStruct& relativeSpatial) = 0;

    /**
    * Send all the attributes.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate()
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

    /**
    * Send all the attributes with a specified timestamp.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @param timestamp timestamp to send for this update
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate(HlaTimeStampPtr timestamp)
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

    /**
    * Send all the attributes with a specified timestamp.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @param logicalTime logical time to send for this update
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaInvalidLogicalTimeException if the logical time is not valid.
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate(HlaLogicalTimePtr logicalTime)
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaInvalidLogicalTimeException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

    /**
    * Send all the attributes with a specified timestamp.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @param timestamp timestamp to send for this update
    * @param logicalTime logical time to send for this update
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaInvalidLogicalTimeException if the logical time is not valid.
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate(HlaTimeStampPtr timestamp, HlaLogicalTimePtr logicalTime)
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaInvalidLogicalTimeException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;
    };
}
#endif
