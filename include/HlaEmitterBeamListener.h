/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAEMITTERBEAMLISTENER_H
#define DEVELOPER_STUDIO_HLAEMITTERBEAMLISTENER_H

#include <set>


#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaEmitterBeamAttributes.h>

namespace DevStudio {
   /**
   * Listener for updates of attributes.  
   */
   class HlaEmitterBeamListener {
   public:

      LIBAPI virtual ~HlaEmitterBeamListener() {}

      /**
      * This method is called when a HLA <code>reflectAttributeValueUpdate</code> is received for an remote object,
      * or a set of attributes is updated on a local object.
      *
      * @param emitterBeam The emitterBeam which this update corresponds to.
      * @param attributes The set of attributes that are updated.
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time the update was initiated.
      */
      LIBAPI virtual void attributesUpdated(HlaEmitterBeamPtr emitterBeam, std::set<HlaEmitterBeamAttributes::Attribute> &attributes, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

      class Adapter;
   };

  /**
  * An adapter class that implements the HlaEmitterBeamListener interface with empty methods.
  * It might be used as a base class for a listener.
  */
  class HlaEmitterBeamListener::Adapter : public HlaEmitterBeamListener {

  public:

     LIBAPI virtual void attributesUpdated(HlaEmitterBeamPtr emitterBeam, std::set<HlaEmitterBeamAttributes::Attribute> &attributes, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
    };

}
#endif
