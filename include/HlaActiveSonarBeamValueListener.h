/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAACTIVESONARBEAMVALUELISTENER_H
#define DEVELOPER_STUDIO_HLAACTIVESONARBEAMVALUELISTENER_H

#include <memory>

#include <DevStudio/datatypes/ActiveSonarScanPatternEnum.h>
#include <DevStudio/datatypes/EventIdentifierStruct.h>
#include <string>

#include <DevStudio/HlaLogicalTime.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaActiveSonarBeamAttributes.h>    

namespace DevStudio {

   /**
   * Listener for updates of attributes, with the new updated values.  
   */
   class HlaActiveSonarBeamValueListener {

   public:

      LIBAPI virtual ~HlaActiveSonarBeamValueListener() {}
    
      /**
      * This method is called when the attribute <code>activeEmissionParameterIndex</code> is updated.
      * <br>Description from the FOM: <i>An index into the database of active (intentional) underwater acoustics emissions.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^15, 2^15-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param activeSonarBeam The object which is updated.
      * @param activeEmissionParameterIndex The new value of the attribute in this update
      * @param validOldActiveEmissionParameterIndex True if oldActiveEmissionParameterIndex contains a valid value
      * @param oldActiveEmissionParameterIndex The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void activeEmissionParameterIndexUpdated(HlaActiveSonarBeamPtr activeSonarBeam, short activeEmissionParameterIndex, bool validOldActiveEmissionParameterIndex, short oldActiveEmissionParameterIndex, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>activeSonarIdentifier</code> is updated.
      * <br>Description from the FOM: <i>The RTI ID of the ActiveSonar emitting this beam</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @param activeSonarBeam The object which is updated.
      * @param activeSonarIdentifier The new value of the attribute in this update
      * @param validOldActiveSonarIdentifier True if oldActiveSonarIdentifier contains a valid value
      * @param oldActiveSonarIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void activeSonarIdentifierUpdated(HlaActiveSonarBeamPtr activeSonarBeam, std::string activeSonarIdentifier, bool validOldActiveSonarIdentifier, std::string oldActiveSonarIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>azimuthBeamwidth</code> is updated.
      * <br>Description from the FOM: <i>The horizontal width of the main beam (as opposed to any side lobes) measured from beam center to the 3 dB down point. Omni directional beams shall have a beam width of 0 radians.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @param activeSonarBeam The object which is updated.
      * @param azimuthBeamwidth The new value of the attribute in this update
      * @param validOldAzimuthBeamwidth True if oldAzimuthBeamwidth contains a valid value
      * @param oldAzimuthBeamwidth The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void azimuthBeamwidthUpdated(HlaActiveSonarBeamPtr activeSonarBeam, float azimuthBeamwidth, bool validOldAzimuthBeamwidth, float oldAzimuthBeamwidth, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>azimuthCenter</code> is updated.
      * <br>Description from the FOM: <i>The center azimuthal bearing of the main beam (as opposed to side lobes) in relation to the own ship heading, clockwise positive. Omnidirection beams shall have an azimuthal center of 0 radians.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @param activeSonarBeam The object which is updated.
      * @param azimuthCenter The new value of the attribute in this update
      * @param validOldAzimuthCenter True if oldAzimuthCenter contains a valid value
      * @param oldAzimuthCenter The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void azimuthCenterUpdated(HlaActiveSonarBeamPtr activeSonarBeam, float azimuthCenter, bool validOldAzimuthCenter, float oldAzimuthCenter, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>beamIdentifier</code> is updated.
      * <br>Description from the FOM: <i>The identification of the active sonar beam, which must be unique on the active sonar system.</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param activeSonarBeam The object which is updated.
      * @param beamIdentifier The new value of the attribute in this update
      * @param validOldBeamIdentifier True if oldBeamIdentifier contains a valid value
      * @param oldBeamIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void beamIdentifierUpdated(HlaActiveSonarBeamPtr activeSonarBeam, char beamIdentifier, bool validOldBeamIdentifier, char oldBeamIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>elevationBeamwidth</code> is updated.
      * <br>Description from the FOM: <i>Vertical angle from beam center to 3db down point. O is omnidirectional</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @param activeSonarBeam The object which is updated.
      * @param elevationBeamwidth The new value of the attribute in this update
      * @param validOldElevationBeamwidth True if oldElevationBeamwidth contains a valid value
      * @param oldElevationBeamwidth The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void elevationBeamwidthUpdated(HlaActiveSonarBeamPtr activeSonarBeam, float elevationBeamwidth, bool validOldElevationBeamwidth, float oldElevationBeamwidth, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>elevationCenter</code> is updated.
      * <br>Description from the FOM: <i>The angle of axis of the beam center to the horizontal plane. Positive upward.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @param activeSonarBeam The object which is updated.
      * @param elevationCenter The new value of the attribute in this update
      * @param validOldElevationCenter True if oldElevationCenter contains a valid value
      * @param oldElevationCenter The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void elevationCenterUpdated(HlaActiveSonarBeamPtr activeSonarBeam, float elevationCenter, bool validOldElevationCenter, float oldElevationCenter, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>eventIdentifier</code> is updated.
      * <br>Description from the FOM: <i>Used to associate changes to the beam with its ActiveSonar.</i>
      * <br>Description of the data type from the FOM: <i>Identification of an event. Based on the Event Identifier record as specified in IEEE 1278.1-1995 section 5.2.18.</i>
      *
      * @param activeSonarBeam The object which is updated.
      * @param eventIdentifier The new value of the attribute in this update
      * @param validOldEventIdentifier True if oldEventIdentifier contains a valid value
      * @param oldEventIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void eventIdentifierUpdated(HlaActiveSonarBeamPtr activeSonarBeam, EventIdentifierStruct eventIdentifier, bool validOldEventIdentifier, EventIdentifierStruct oldEventIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>scanPattern</code> is updated.
      * <br>Description from the FOM: <i>The pattern that describes the angular movement of the sonar beam during its sweep.</i>
      * <br>Description of the data type from the FOM: <i>Acoustic scan pattern</i>
      *
      * @param activeSonarBeam The object which is updated.
      * @param scanPattern The new value of the attribute in this update
      * @param validOldScanPattern True if oldScanPattern contains a valid value
      * @param oldScanPattern The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void scanPatternUpdated(HlaActiveSonarBeamPtr activeSonarBeam, ActiveSonarScanPatternEnum::ActiveSonarScanPatternEnum scanPattern, bool validOldScanPattern, ActiveSonarScanPatternEnum::ActiveSonarScanPatternEnum oldScanPattern, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
      /**
      * This method is called when the producing federate of an attribute is changed.
      * Only available when using HLA Evolved.
      *
      * @param activeSonarBeam The object which is updated.
      * @param attribute The attribute that now has a new producing federate
      * @param oldProducingFederate The federate handle of the old producing federate
      * @param newProducingFederate The federate handle of the new producing federate
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void producingFederateUpdated(HlaActiveSonarBeamPtr activeSonarBeam, HlaActiveSonarBeamAttributes::Attribute attribute, HlaFederateIdPtr oldProducingFederate, HlaFederateIdPtr newProducingFederate, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;

      class Adapter;
   };

   /**
   * An adapter class that implements the HlaActiveSonarBeamValueListener interface with empty methods.
   * It might be used as a base class for a listener.
   */
   class HlaActiveSonarBeamValueListener::Adapter : public HlaActiveSonarBeamValueListener {

   public:

      LIBAPI virtual void activeEmissionParameterIndexUpdated(HlaActiveSonarBeamPtr activeSonarBeam, short activeEmissionParameterIndex, bool validOldActiveEmissionParameterIndex, short oldActiveEmissionParameterIndex, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void activeSonarIdentifierUpdated(HlaActiveSonarBeamPtr activeSonarBeam, std::string activeSonarIdentifier, bool validOldActiveSonarIdentifier, std::string oldActiveSonarIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void azimuthBeamwidthUpdated(HlaActiveSonarBeamPtr activeSonarBeam, float azimuthBeamwidth, bool validOldAzimuthBeamwidth, float oldAzimuthBeamwidth, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void azimuthCenterUpdated(HlaActiveSonarBeamPtr activeSonarBeam, float azimuthCenter, bool validOldAzimuthCenter, float oldAzimuthCenter, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void beamIdentifierUpdated(HlaActiveSonarBeamPtr activeSonarBeam, char beamIdentifier, bool validOldBeamIdentifier, char oldBeamIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void elevationBeamwidthUpdated(HlaActiveSonarBeamPtr activeSonarBeam, float elevationBeamwidth, bool validOldElevationBeamwidth, float oldElevationBeamwidth, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void elevationCenterUpdated(HlaActiveSonarBeamPtr activeSonarBeam, float elevationCenter, bool validOldElevationCenter, float oldElevationCenter, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void eventIdentifierUpdated(HlaActiveSonarBeamPtr activeSonarBeam, EventIdentifierStruct eventIdentifier, bool validOldEventIdentifier, EventIdentifierStruct oldEventIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void scanPatternUpdated(HlaActiveSonarBeamPtr activeSonarBeam, ActiveSonarScanPatternEnum::ActiveSonarScanPatternEnum scanPattern, bool validOldScanPattern, ActiveSonarScanPatternEnum::ActiveSonarScanPatternEnum oldScanPattern, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
      LIBAPI virtual void producingFederateUpdated(HlaActiveSonarBeamPtr activeSonarBeam, HlaActiveSonarBeamAttributes::Attribute attribute, HlaFederateIdPtr oldProducingFederate, HlaFederateIdPtr newProducingFederate, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
   };

}
#endif
