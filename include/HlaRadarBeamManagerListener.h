/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLARADARBEAMMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLARADARBEAMMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaRadarBeam.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaRadarBeam instances.
    */
    class HlaRadarBeamManagerListener {

    public:

        LIBAPI virtual ~HlaRadarBeamManagerListener() {}

        /**
        * This method is called when a new HlaRadarBeam instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param radarBeam the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaRadarBeamDiscovered(HlaRadarBeamPtr radarBeam, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaRadarBeam instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param radarBeam the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaRadarBeamInitialized(HlaRadarBeamPtr radarBeam, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaRadarBeamManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param radarBeam the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaRadarBeamInInterest(HlaRadarBeamPtr radarBeam, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaRadarBeamManagerListener instance goes out of interest.
        *
        * @param radarBeam the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaRadarBeamOutOfInterest(HlaRadarBeamPtr radarBeam, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaRadarBeam instance is deleted.
        *
        * @param radarBeam the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaRadarBeamDeleted(HlaRadarBeamPtr radarBeam, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaRadarBeamManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaRadarBeamManagerListener::Adapter : public HlaRadarBeamManagerListener {

    public:
        LIBAPI virtual void hlaRadarBeamDiscovered(HlaRadarBeamPtr radarBeam, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaRadarBeamInitialized(HlaRadarBeamPtr radarBeam, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaRadarBeamInInterest(HlaRadarBeamPtr radarBeam, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaRadarBeamOutOfInterest(HlaRadarBeamPtr radarBeam, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaRadarBeamDeleted(HlaRadarBeamPtr radarBeam, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
