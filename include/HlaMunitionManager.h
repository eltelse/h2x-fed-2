/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAMUNITIONMANAGER_H
#define DEVELOPER_STUDIO_HLAMUNITIONMANAGER_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <DevStudio/datatypes/ArticulatedParameterStruct.h>
#include <DevStudio/datatypes/ArticulatedParameterStructLengthlessArray.h>
#include <DevStudio/datatypes/CamouflageEnum.h>
#include <DevStudio/datatypes/DamageStatusEnum.h>
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EntityTypeStruct.h>
#include <DevStudio/datatypes/ForceIdentifierEnum.h>
#include <DevStudio/datatypes/IsPartOfStruct.h>
#include <DevStudio/datatypes/MarkingStruct.h>
#include <DevStudio/datatypes/PropulsionSystemDataStruct.h>
#include <DevStudio/datatypes/PropulsionSystemDataStructLengthlessArray.h>
#include <DevStudio/datatypes/SpatialVariantStruct.h>
#include <DevStudio/datatypes/TrailingEffectsCodeEnum.h>
#include <DevStudio/datatypes/VectoringNozzleSystemDataStruct.h>
#include <DevStudio/datatypes/VectoringNozzleSystemDataStructLengthlessArray.h>
#include <vector>

#include <list>
#include <vector>

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaMunitionManagerListener.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaException.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

   /**
   * Manager used to manage HlaMunitions.
   */
   class HlaMunitionManager : private boost::noncopyable {

   public:

      LIBAPI virtual ~HlaMunitionManager() {}

      /**
      * Gets a list of all HlaMunitions within interest, both local and remote.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of HlaMunitions
      */
      LIBAPI virtual std::list<HlaMunitionPtr> getHlaMunitions() = 0;

      /**
      * Gets a list of all HlaMunitions, both local and remote.
      * HlaMunitions not within interest are also included.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of all HlaMunitions
      */
      LIBAPI virtual std::list<HlaMunitionPtr> getAllHlaMunitions() = 0;

      /**
      * Gets a list of local HlaMunitions within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of local HlaMunitions
      */
      LIBAPI virtual std::list<HlaMunitionPtr> getLocalHlaMunitions() = 0;

      /**
      * Gets a list of remote HlaMunitions within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of remote HlaMunitions
      */
      LIBAPI virtual std::list<HlaMunitionPtr> getRemoteHlaMunitions() = 0;

      /**
      * Find a HlaMunition with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> of the HlaMunition to find
      *
      * @return the specified HlaMunition, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaMunitionPtr getMunitionByHlaInstanceName(const std::wstring& hlaInstanceName) = 0;

      /**
      * Find a HlaMunition with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param encodedHlaInstanceHandle the encoded <code>HLA instance handle</code> of the HlaMunition to find
      *
      * @return the specified HlaMunition, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaMunitionPtr getMunitionByHlaInstanceHandle(const std::vector<char>& encodedHlaInstanceHandle) = 0;

      /**
      * Creates a new HlaMunition, and sets all <i>Create</i> attributes.
      *
      * @return Returns a new local HlaMunition.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaMunitionPtr createLocalHlaMunition(
      ) THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Creates a new HlaMunition with a specified <code>HLA instance name</code>, and sets all
      * <i>Create</i> attributes.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> to register for this instance
      *
      * @return Returns a new local HlaMunition.
      *
      * @throws HlaIllegalInstanceNameException if the name is illegal
      * @throws HlaInstanceNameInUseException if the name is already in use
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaMunitionPtr createLocalHlaMunition(
         const std::wstring& hlaInstanceName
      ) THROW_SPEC (HlaIllegalInstanceNameException, HlaInstanceNameInUseException,
                    HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaMunition and removes it from the federation.
      *
      * @param munition The HlaMunition to delete
      *
      * @return the HlaMunition deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaMunitionPtr deleteLocalHlaMunition(HlaMunitionPtr munition)
          THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                      HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaMunition and removes it from the federation.
      *
      * @param munition The HlaMunition to delete
      * @param timestamp timestamp when the instance was removed.
      *
      * @return the HlaMunition deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaMunitionPtr deleteLocalHlaMunition(HlaMunitionPtr munition, HlaTimeStampPtr timestamp)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaMunition and removes it from the federation.
      *
      * @param munition The HlaMunition to delete
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaMunition deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaMunitionPtr deleteLocalHlaMunition(HlaMunitionPtr munition, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaMunition and removes it from the federation.
      *
      * @param munition The HlaMunition to delete
      * @param timestamp timestamp when the instance was removed.
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaMunition deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaMunitionPtr deleteLocalHlaMunition(HlaMunitionPtr munition, HlaTimeStampPtr timestamp, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Add a HlaMunition manager listener.
      *
      * @param managerListener listener to add
      */
      LIBAPI virtual void addHlaMunitionManagerListener(HlaMunitionManagerListenerPtr managerListener) = 0;

      /**
      * Removed a HlaMunition manager listener.
      *
      * @param managerListener listener to remove
      */
      LIBAPI virtual void removeHlaMunitionManagerListener(HlaMunitionManagerListenerPtr managerListener) = 0;

     /**
      * Add a listener that will automatically be added to all instances of HlaMunition (Both local and remote).
      * The listener is notified when any attribute of an instance of HlaMunition is updated.
      * The listener is also called when an interaction is sent to an instance of HlaMunition.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param listener The listener to add.
      */
      LIBAPI virtual void addHlaMunitionDefaultInstanceListener(HlaMunitionListenerPtr listener) = 0;

     /**
      * Remove a listener from the set of listeners that are automatically added to new instances of HlaMunition.
      * Note: The listener will not be removed from already existing instances of HlaMunition.
      * This method is idempotent.
      *
      * @param listener The listener to remove.
      */
      LIBAPI virtual void removeHlaMunitionDefaultInstanceListener(HlaMunitionListenerPtr listener) = 0;

     /**
      * Add a valueListener that will automatically be added to all instances of HlaMunition (Both local and remote).
      * The  Valuelistener is notified when any attribute of an instance of HlaMunition is updated.
      * Note that the attribute values that are notified are not guaranteed to
      * be the latest available values for that attribute.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to add.
      */
      LIBAPI virtual void addHlaMunitionDefaultInstanceValueListener(HlaMunitionValueListenerPtr valueListener) = 0;

     /**
      * Remove a valueListener from the set of listeners that are automatically added to new instances of HlaMunition.
      * Note: The valueListener will not be removed from already existing instances of HlaMunition.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to remove.
      */
      LIBAPI virtual void removeHlaMunitionDefaultInstanceValueListener(HlaMunitionValueListenerPtr valueListener) = 0;

     /**
      * Enables or disables the HlaMunition manager.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaMunition manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      *
      * @param enabled <code>true</code> to enable the HlaMunition manager, otherwise <code>false</code>
      */
      LIBAPI virtual void setEnabled(bool enabled) = 0;

     /**
      * Enables the HlaMunition manager if it is avalable in the FOM at runtime.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaMunition manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      */
      LIBAPI virtual void setEnabledIfAvailableInFom() = 0;

     /**
      * Check if the HlaMunition manager is actually enabled when connected.
      * An HlaMunition manager may be disabled with the <code>setEnabled(false)</code> function, or it may be configured
      * as <i>if available in the FOM</i> by the <code>setEnabledIfAvailableInFom()</code> function and
      * then not available in the FOM at runtime.
      * <p>
      * This method should only be used when connected, it will always return <code>false</code>
      * when not connected.
      *
      * @return <code>true</code> if connected and the HlaMunition manager is enabled.
      */
      LIBAPI virtual bool isEnabled() = 0;
   };
}
#endif
