/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAUNDERWATERACOUSTICSEMISSIONMANAGER_H
#define DEVELOPER_STUDIO_HLAUNDERWATERACOUSTICSEMISSIONMANAGER_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EventIdentifierStruct.h>
#include <DevStudio/datatypes/RelativePositionStruct.h>
#include <string>

#include <list>
#include <vector>

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaUnderwaterAcousticsEmissionManagerListener.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaException.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

   /**
   * Manager used to manage HlaUnderwaterAcousticsEmissions.
   */
   class HlaUnderwaterAcousticsEmissionManager : private boost::noncopyable {

   public:

      LIBAPI virtual ~HlaUnderwaterAcousticsEmissionManager() {}

      /**
      * Gets a list of all HlaUnderwaterAcousticsEmissions within interest, both local and remote.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of HlaUnderwaterAcousticsEmissions
      */
      LIBAPI virtual std::list<HlaUnderwaterAcousticsEmissionPtr> getHlaUnderwaterAcousticsEmissions() = 0;

      /**
      * Gets a list of all HlaUnderwaterAcousticsEmissions, both local and remote.
      * HlaUnderwaterAcousticsEmissions not within interest are also included.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of all HlaUnderwaterAcousticsEmissions
      */
      LIBAPI virtual std::list<HlaUnderwaterAcousticsEmissionPtr> getAllHlaUnderwaterAcousticsEmissions() = 0;

      /**
      * Gets a list of local HlaUnderwaterAcousticsEmissions within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of local HlaUnderwaterAcousticsEmissions
      */
      LIBAPI virtual std::list<HlaUnderwaterAcousticsEmissionPtr> getLocalHlaUnderwaterAcousticsEmissions() = 0;

      /**
      * Gets a list of remote HlaUnderwaterAcousticsEmissions within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of remote HlaUnderwaterAcousticsEmissions
      */
      LIBAPI virtual std::list<HlaUnderwaterAcousticsEmissionPtr> getRemoteHlaUnderwaterAcousticsEmissions() = 0;

      /**
      * Find a HlaUnderwaterAcousticsEmission with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> of the HlaUnderwaterAcousticsEmission to find
      *
      * @return the specified HlaUnderwaterAcousticsEmission, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaUnderwaterAcousticsEmissionPtr getUnderwaterAcousticsEmissionByHlaInstanceName(const std::wstring& hlaInstanceName) = 0;

      /**
      * Find a HlaUnderwaterAcousticsEmission with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param encodedHlaInstanceHandle the encoded <code>HLA instance handle</code> of the HlaUnderwaterAcousticsEmission to find
      *
      * @return the specified HlaUnderwaterAcousticsEmission, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaUnderwaterAcousticsEmissionPtr getUnderwaterAcousticsEmissionByHlaInstanceHandle(const std::vector<char>& encodedHlaInstanceHandle) = 0;

      /**
      * Creates a new HlaUnderwaterAcousticsEmission, and sets all <i>Create</i> attributes.
      *
      * @return Returns a new local HlaUnderwaterAcousticsEmission.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaUnderwaterAcousticsEmissionPtr createLocalHlaUnderwaterAcousticsEmission(
      ) THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Creates a new HlaUnderwaterAcousticsEmission with a specified <code>HLA instance name</code>, and sets all
      * <i>Create</i> attributes.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> to register for this instance
      *
      * @return Returns a new local HlaUnderwaterAcousticsEmission.
      *
      * @throws HlaIllegalInstanceNameException if the name is illegal
      * @throws HlaInstanceNameInUseException if the name is already in use
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaUnderwaterAcousticsEmissionPtr createLocalHlaUnderwaterAcousticsEmission(
         const std::wstring& hlaInstanceName
      ) THROW_SPEC (HlaIllegalInstanceNameException, HlaInstanceNameInUseException,
                    HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaUnderwaterAcousticsEmission and removes it from the federation.
      *
      * @param underwaterAcousticsEmission The HlaUnderwaterAcousticsEmission to delete
      *
      * @return the HlaUnderwaterAcousticsEmission deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaUnderwaterAcousticsEmissionPtr deleteLocalHlaUnderwaterAcousticsEmission(HlaUnderwaterAcousticsEmissionPtr underwaterAcousticsEmission)
          THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                      HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaUnderwaterAcousticsEmission and removes it from the federation.
      *
      * @param underwaterAcousticsEmission The HlaUnderwaterAcousticsEmission to delete
      * @param timestamp timestamp when the instance was removed.
      *
      * @return the HlaUnderwaterAcousticsEmission deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaUnderwaterAcousticsEmissionPtr deleteLocalHlaUnderwaterAcousticsEmission(HlaUnderwaterAcousticsEmissionPtr underwaterAcousticsEmission, HlaTimeStampPtr timestamp)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaUnderwaterAcousticsEmission and removes it from the federation.
      *
      * @param underwaterAcousticsEmission The HlaUnderwaterAcousticsEmission to delete
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaUnderwaterAcousticsEmission deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaUnderwaterAcousticsEmissionPtr deleteLocalHlaUnderwaterAcousticsEmission(HlaUnderwaterAcousticsEmissionPtr underwaterAcousticsEmission, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaUnderwaterAcousticsEmission and removes it from the federation.
      *
      * @param underwaterAcousticsEmission The HlaUnderwaterAcousticsEmission to delete
      * @param timestamp timestamp when the instance was removed.
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaUnderwaterAcousticsEmission deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaUnderwaterAcousticsEmissionPtr deleteLocalHlaUnderwaterAcousticsEmission(HlaUnderwaterAcousticsEmissionPtr underwaterAcousticsEmission, HlaTimeStampPtr timestamp, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Add a HlaUnderwaterAcousticsEmission manager listener.
      *
      * @param managerListener listener to add
      */
      LIBAPI virtual void addHlaUnderwaterAcousticsEmissionManagerListener(HlaUnderwaterAcousticsEmissionManagerListenerPtr managerListener) = 0;

      /**
      * Removed a HlaUnderwaterAcousticsEmission manager listener.
      *
      * @param managerListener listener to remove
      */
      LIBAPI virtual void removeHlaUnderwaterAcousticsEmissionManagerListener(HlaUnderwaterAcousticsEmissionManagerListenerPtr managerListener) = 0;

     /**
      * Add a listener that will automatically be added to all instances of HlaUnderwaterAcousticsEmission (Both local and remote).
      * The listener is notified when any attribute of an instance of HlaUnderwaterAcousticsEmission is updated.
      * The listener is also called when an interaction is sent to an instance of HlaUnderwaterAcousticsEmission.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param listener The listener to add.
      */
      LIBAPI virtual void addHlaUnderwaterAcousticsEmissionDefaultInstanceListener(HlaUnderwaterAcousticsEmissionListenerPtr listener) = 0;

     /**
      * Remove a listener from the set of listeners that are automatically added to new instances of HlaUnderwaterAcousticsEmission.
      * Note: The listener will not be removed from already existing instances of HlaUnderwaterAcousticsEmission.
      * This method is idempotent.
      *
      * @param listener The listener to remove.
      */
      LIBAPI virtual void removeHlaUnderwaterAcousticsEmissionDefaultInstanceListener(HlaUnderwaterAcousticsEmissionListenerPtr listener) = 0;

     /**
      * Add a valueListener that will automatically be added to all instances of HlaUnderwaterAcousticsEmission (Both local and remote).
      * The  Valuelistener is notified when any attribute of an instance of HlaUnderwaterAcousticsEmission is updated.
      * Note that the attribute values that are notified are not guaranteed to
      * be the latest available values for that attribute.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to add.
      */
      LIBAPI virtual void addHlaUnderwaterAcousticsEmissionDefaultInstanceValueListener(HlaUnderwaterAcousticsEmissionValueListenerPtr valueListener) = 0;

     /**
      * Remove a valueListener from the set of listeners that are automatically added to new instances of HlaUnderwaterAcousticsEmission.
      * Note: The valueListener will not be removed from already existing instances of HlaUnderwaterAcousticsEmission.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to remove.
      */
      LIBAPI virtual void removeHlaUnderwaterAcousticsEmissionDefaultInstanceValueListener(HlaUnderwaterAcousticsEmissionValueListenerPtr valueListener) = 0;

     /**
      * Enables or disables the HlaUnderwaterAcousticsEmission manager.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaUnderwaterAcousticsEmission manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      *
      * @param enabled <code>true</code> to enable the HlaUnderwaterAcousticsEmission manager, otherwise <code>false</code>
      */
      LIBAPI virtual void setEnabled(bool enabled) = 0;

     /**
      * Enables the HlaUnderwaterAcousticsEmission manager if it is avalable in the FOM at runtime.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaUnderwaterAcousticsEmission manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      */
      LIBAPI virtual void setEnabledIfAvailableInFom() = 0;

     /**
      * Check if the HlaUnderwaterAcousticsEmission manager is actually enabled when connected.
      * An HlaUnderwaterAcousticsEmission manager may be disabled with the <code>setEnabled(false)</code> function, or it may be configured
      * as <i>if available in the FOM</i> by the <code>setEnabledIfAvailableInFom()</code> function and
      * then not available in the FOM at runtime.
      * <p>
      * This method should only be used when connected, it will always return <code>false</code>
      * when not connected.
      *
      * @return <code>true</code> if connected and the HlaUnderwaterAcousticsEmission manager is enabled.
      */
      LIBAPI virtual bool isEnabled() = 0;
   };
}
#endif
