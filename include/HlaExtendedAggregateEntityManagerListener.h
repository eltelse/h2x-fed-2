/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAEXTENDEDAGGREGATEENTITYMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLAEXTENDEDAGGREGATEENTITYMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaExtendedAggregateEntity.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaExtendedAggregateEntity instances.
    */
    class HlaExtendedAggregateEntityManagerListener {

    public:

        LIBAPI virtual ~HlaExtendedAggregateEntityManagerListener() {}

        /**
        * This method is called when a new HlaExtendedAggregateEntity instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param extendedAggregateEntity the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaExtendedAggregateEntityDiscovered(HlaExtendedAggregateEntityPtr extendedAggregateEntity, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaExtendedAggregateEntity instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param extendedAggregateEntity the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaExtendedAggregateEntityInitialized(HlaExtendedAggregateEntityPtr extendedAggregateEntity, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaExtendedAggregateEntityManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param extendedAggregateEntity the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaExtendedAggregateEntityInInterest(HlaExtendedAggregateEntityPtr extendedAggregateEntity, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaExtendedAggregateEntityManagerListener instance goes out of interest.
        *
        * @param extendedAggregateEntity the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaExtendedAggregateEntityOutOfInterest(HlaExtendedAggregateEntityPtr extendedAggregateEntity, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaExtendedAggregateEntity instance is deleted.
        *
        * @param extendedAggregateEntity the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaExtendedAggregateEntityDeleted(HlaExtendedAggregateEntityPtr extendedAggregateEntity, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaExtendedAggregateEntityManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaExtendedAggregateEntityManagerListener::Adapter : public HlaExtendedAggregateEntityManagerListener {

    public:
        LIBAPI virtual void hlaExtendedAggregateEntityDiscovered(HlaExtendedAggregateEntityPtr extendedAggregateEntity, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaExtendedAggregateEntityInitialized(HlaExtendedAggregateEntityPtr extendedAggregateEntity, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaExtendedAggregateEntityInInterest(HlaExtendedAggregateEntityPtr extendedAggregateEntity, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaExtendedAggregateEntityOutOfInterest(HlaExtendedAggregateEntityPtr extendedAggregateEntity, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaExtendedAggregateEntityDeleted(HlaExtendedAggregateEntityPtr extendedAggregateEntity, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
