/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAAGGREGATEENTITYVALUELISTENER_H
#define DEVELOPER_STUDIO_HLAAGGREGATEENTITYVALUELISTENER_H

#include <memory>

#include <DevStudio/datatypes/AggregateMarkingStruct.h>
#include <DevStudio/datatypes/AggregateStateEnum.h>
#include <DevStudio/datatypes/DimensionStruct.h>
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EntityTypeStruct.h>
#include <DevStudio/datatypes/ForceIdentifierEnum.h>
#include <DevStudio/datatypes/FormationEnum.h>
#include <DevStudio/datatypes/IsPartOfStruct.h>
#include <DevStudio/datatypes/RTIobjectIdArray.h>
#include <DevStudio/datatypes/SilentAggregateStruct.h>
#include <DevStudio/datatypes/SilentAggregateStructLengthlessArray.h>
#include <DevStudio/datatypes/SilentEntityStruct.h>
#include <DevStudio/datatypes/SilentEntityStructLengthlessArray.h>
#include <DevStudio/datatypes/SpatialVariantStruct.h>
#include <DevStudio/datatypes/VariableDatumStruct.h>
#include <DevStudio/datatypes/VariableDatumStructLengthlessArray.h>
#include <string>
#include <vector>

#include <DevStudio/HlaLogicalTime.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaAggregateEntityAttributes.h>    

namespace DevStudio {

   /**
   * Listener for updates of attributes, with the new updated values.  
   */
   class HlaAggregateEntityValueListener {

   public:

      LIBAPI virtual ~HlaAggregateEntityValueListener() {}
    
      /**
      * This method is called when the attribute <code>aggregateMarking</code> is updated.
      * <br>Description from the FOM: <i>A unique marking or combination of characters used to distinguish the aggregate from other aggregates.</i>
      * <br>Description of the data type from the FOM: <i>Unique marking associated with an aggregate.</i>
      *
      * @param aggregateEntity The object which is updated.
      * @param aggregateMarking The new value of the attribute in this update
      * @param validOldAggregateMarking True if oldAggregateMarking contains a valid value
      * @param oldAggregateMarking The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void aggregateMarkingUpdated(HlaAggregateEntityPtr aggregateEntity, AggregateMarkingStruct aggregateMarking, bool validOldAggregateMarking, AggregateMarkingStruct oldAggregateMarking, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>aggregateState</code> is updated.
      * <br>Description from the FOM: <i>An indicator of the extent of association of objects form an operating group.</i>
      * <br>Description of the data type from the FOM: <i>Aggregate state</i>
      *
      * @param aggregateEntity The object which is updated.
      * @param aggregateState The new value of the attribute in this update
      * @param validOldAggregateState True if oldAggregateState contains a valid value
      * @param oldAggregateState The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void aggregateStateUpdated(HlaAggregateEntityPtr aggregateEntity, AggregateStateEnum::AggregateStateEnum aggregateState, bool validOldAggregateState, AggregateStateEnum::AggregateStateEnum oldAggregateState, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>dimensions</code> is updated.
      * <br>Description from the FOM: <i>The size of the area covered by the units in the aggregate.</i>
      * <br>Description of the data type from the FOM: <i>Bounding box in X,Y,Z axis.</i>
      *
      * @param aggregateEntity The object which is updated.
      * @param dimensions The new value of the attribute in this update
      * @param validOldDimensions True if oldDimensions contains a valid value
      * @param oldDimensions The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void dimensionsUpdated(HlaAggregateEntityPtr aggregateEntity, DimensionStruct dimensions, bool validOldDimensions, DimensionStruct oldDimensions, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>entityIdentifiers</code> is updated.
      * <br>Description from the FOM: <i>The identification of entities that are contained within the aggregate.</i>
      * <br>Description of the data type from the FOM: <i>Set of ID's of registered object instances.</i>
      *
      * @param aggregateEntity The object which is updated.
      * @param entityIdentifiers The new value of the attribute in this update
      * @param validOldEntityIdentifiers True if oldEntityIdentifiers contains a valid value
      * @param oldEntityIdentifiers The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void entityIdentifiersUpdated(HlaAggregateEntityPtr aggregateEntity, std::vector<std::string > entityIdentifiers, bool validOldEntityIdentifiers, std::vector<std::string > oldEntityIdentifiers, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>forceIdentifier</code> is updated.
      * <br>Description from the FOM: <i>The identification of the force that the aggregate belongs to.</i>
      * <br>Description of the data type from the FOM: <i>Force ID</i>
      *
      * @param aggregateEntity The object which is updated.
      * @param forceIdentifier The new value of the attribute in this update
      * @param validOldForceIdentifier True if oldForceIdentifier contains a valid value
      * @param oldForceIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void forceIdentifierUpdated(HlaAggregateEntityPtr aggregateEntity, ForceIdentifierEnum::ForceIdentifierEnum forceIdentifier, bool validOldForceIdentifier, ForceIdentifierEnum::ForceIdentifierEnum oldForceIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>formation</code> is updated.
      * <br>Description from the FOM: <i>The category of positional arrangement of the entities within the aggregate.</i>
      * <br>Description of the data type from the FOM: <i>Formation</i>
      *
      * @param aggregateEntity The object which is updated.
      * @param formation The new value of the attribute in this update
      * @param validOldFormation True if oldFormation contains a valid value
      * @param oldFormation The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void formationUpdated(HlaAggregateEntityPtr aggregateEntity, FormationEnum::FormationEnum formation, bool validOldFormation, FormationEnum::FormationEnum oldFormation, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>numberOfSilentEntities</code> is updated.
      * <br>Description from the FOM: <i>The number of elements in the SilentEntities list.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^15, 2^15-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param aggregateEntity The object which is updated.
      * @param numberOfSilentEntities The new value of the attribute in this update
      * @param validOldNumberOfSilentEntities True if oldNumberOfSilentEntities contains a valid value
      * @param oldNumberOfSilentEntities The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void numberOfSilentEntitiesUpdated(HlaAggregateEntityPtr aggregateEntity, short numberOfSilentEntities, bool validOldNumberOfSilentEntities, short oldNumberOfSilentEntities, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>numberOfVariableDatums</code> is updated.
      * <br>Description from the FOM: <i>The number of records in the VariableDatums structure.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param aggregateEntity The object which is updated.
      * @param numberOfVariableDatums The new value of the attribute in this update
      * @param validOldNumberOfVariableDatums True if oldNumberOfVariableDatums contains a valid value
      * @param oldNumberOfVariableDatums The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void numberOfVariableDatumsUpdated(HlaAggregateEntityPtr aggregateEntity, unsigned int numberOfVariableDatums, bool validOldNumberOfVariableDatums, unsigned int oldNumberOfVariableDatums, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>silentAggregates</code> is updated.
      * <br>Description from the FOM: <i>The numbers and types, of silent aggregates contained in the aggregate. Silent aggregates are sub-aggregates that are in the aggregate, but that are not separately represented in the virtual world.</i>
      * <br>Description of the data type from the FOM: <i>Set of silent aggregates (aggregates not registered in the federation).</i>
      *
      * @param aggregateEntity The object which is updated.
      * @param silentAggregates The new value of the attribute in this update
      * @param validOldSilentAggregates True if oldSilentAggregates contains a valid value
      * @param oldSilentAggregates The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void silentAggregatesUpdated(HlaAggregateEntityPtr aggregateEntity, std::vector<DevStudio::SilentAggregateStruct > silentAggregates, bool validOldSilentAggregates, std::vector<DevStudio::SilentAggregateStruct > oldSilentAggregates, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>silentEntities</code> is updated.
      * <br>Description from the FOM: <i>The numbers and types, of silent entities in the aggregate. Silent entities are entities that are in the aggregate, but that are not separately represented in the virtual world.</i>
      * <br>Description of the data type from the FOM: <i>A set of silent entities (entities not registered in the federation).</i>
      *
      * @param aggregateEntity The object which is updated.
      * @param silentEntities The new value of the attribute in this update
      * @param validOldSilentEntities True if oldSilentEntities contains a valid value
      * @param oldSilentEntities The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void silentEntitiesUpdated(HlaAggregateEntityPtr aggregateEntity, std::vector<DevStudio::SilentEntityStruct > silentEntities, bool validOldSilentEntities, std::vector<DevStudio::SilentEntityStruct > oldSilentEntities, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>subAggregateIdentifiers</code> is updated.
      * <br>Description from the FOM: <i>The identifications of aggregates represented in the virtual world that are contained in the aggregate.</i>
      * <br>Description of the data type from the FOM: <i>Set of ID's of registered object instances.</i>
      *
      * @param aggregateEntity The object which is updated.
      * @param subAggregateIdentifiers The new value of the attribute in this update
      * @param validOldSubAggregateIdentifiers True if oldSubAggregateIdentifiers contains a valid value
      * @param oldSubAggregateIdentifiers The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void subAggregateIdentifiersUpdated(HlaAggregateEntityPtr aggregateEntity, std::vector<std::string > subAggregateIdentifiers, bool validOldSubAggregateIdentifiers, std::vector<std::string > oldSubAggregateIdentifiers, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>variableDatums</code> is updated.
      * <br>Description from the FOM: <i>Extra data that describes the aggregate.</i>
      * <br>Description of the data type from the FOM: <i>Set of additional data associated with an aggregate.</i>
      *
      * @param aggregateEntity The object which is updated.
      * @param variableDatums The new value of the attribute in this update
      * @param validOldVariableDatums True if oldVariableDatums contains a valid value
      * @param oldVariableDatums The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void variableDatumsUpdated(HlaAggregateEntityPtr aggregateEntity, std::vector<DevStudio::VariableDatumStruct > variableDatums, bool validOldVariableDatums, std::vector<DevStudio::VariableDatumStruct > oldVariableDatums, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>entityType</code> is updated.
      * <br>Description from the FOM: <i>The category of the entity.</i>
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      *
      * @param aggregateEntity The object which is updated.
      * @param entityType The new value of the attribute in this update
      * @param validOldEntityType True if oldEntityType contains a valid value
      * @param oldEntityType The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void entityTypeUpdated(HlaAggregateEntityPtr aggregateEntity, EntityTypeStruct entityType, bool validOldEntityType, EntityTypeStruct oldEntityType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>entityIdentifier</code> is updated.
      * <br>Description from the FOM: <i>The unique identifier for the entity instance.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @param aggregateEntity The object which is updated.
      * @param entityIdentifier The new value of the attribute in this update
      * @param validOldEntityIdentifier True if oldEntityIdentifier contains a valid value
      * @param oldEntityIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void entityIdentifierUpdated(HlaAggregateEntityPtr aggregateEntity, EntityIdentifierStruct entityIdentifier, bool validOldEntityIdentifier, EntityIdentifierStruct oldEntityIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>isPartOf</code> is updated.
      * <br>Description from the FOM: <i>Defines if the entity if a constituent part of another entity (denoted the host entity). If the entity is a constituent part of another entity then the HostEntityIdentifier shall be set to the EntityIdentifier of the host entity and the HostRTIObjectIdentifier shall be set to the RTI object instance ID of the host entity. If the entity is not a constituent part of another entity then the HostEntityIdentifier shall be set to 0.0.0 and the HostRTIObjectIdentifier shall be set to the empty string.</i>
      * <br>Description of the data type from the FOM: <i>Defines the spatial relationship between two objects.</i>
      *
      * @param aggregateEntity The object which is updated.
      * @param isPartOf The new value of the attribute in this update
      * @param validOldIsPartOf True if oldIsPartOf contains a valid value
      * @param oldIsPartOf The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void isPartOfUpdated(HlaAggregateEntityPtr aggregateEntity, IsPartOfStruct isPartOf, bool validOldIsPartOf, IsPartOfStruct oldIsPartOf, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>spatial</code> is updated.
      * <br>Description from the FOM: <i>Spatial state stored in one variant record attribute.</i>
      * <br>Description of the data type from the FOM: <i>Variant Record for a single spatial attribute.</i>
      *
      * @param aggregateEntity The object which is updated.
      * @param spatial The new value of the attribute in this update
      * @param validOldSpatial True if oldSpatial contains a valid value
      * @param oldSpatial The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void spatialUpdated(HlaAggregateEntityPtr aggregateEntity, SpatialVariantStruct spatial, bool validOldSpatial, SpatialVariantStruct oldSpatial, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>relativeSpatial</code> is updated.
      * <br>Description from the FOM: <i>Relative spatial state stored in one variant record attribute.</i>
      * <br>Description of the data type from the FOM: <i>Variant Record for a single spatial attribute.</i>
      *
      * @param aggregateEntity The object which is updated.
      * @param relativeSpatial The new value of the attribute in this update
      * @param validOldRelativeSpatial True if oldRelativeSpatial contains a valid value
      * @param oldRelativeSpatial The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void relativeSpatialUpdated(HlaAggregateEntityPtr aggregateEntity, SpatialVariantStruct relativeSpatial, bool validOldRelativeSpatial, SpatialVariantStruct oldRelativeSpatial, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
      /**
      * This method is called when the producing federate of an attribute is changed.
      * Only available when using HLA Evolved.
      *
      * @param aggregateEntity The object which is updated.
      * @param attribute The attribute that now has a new producing federate
      * @param oldProducingFederate The federate handle of the old producing federate
      * @param newProducingFederate The federate handle of the new producing federate
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void producingFederateUpdated(HlaAggregateEntityPtr aggregateEntity, HlaAggregateEntityAttributes::Attribute attribute, HlaFederateIdPtr oldProducingFederate, HlaFederateIdPtr newProducingFederate, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;

      class Adapter;
   };

   /**
   * An adapter class that implements the HlaAggregateEntityValueListener interface with empty methods.
   * It might be used as a base class for a listener.
   */
   class HlaAggregateEntityValueListener::Adapter : public HlaAggregateEntityValueListener {

   public:

      LIBAPI virtual void aggregateMarkingUpdated(HlaAggregateEntityPtr aggregateEntity, AggregateMarkingStruct aggregateMarking, bool validOldAggregateMarking, AggregateMarkingStruct oldAggregateMarking, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void aggregateStateUpdated(HlaAggregateEntityPtr aggregateEntity, AggregateStateEnum::AggregateStateEnum aggregateState, bool validOldAggregateState, AggregateStateEnum::AggregateStateEnum oldAggregateState, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void dimensionsUpdated(HlaAggregateEntityPtr aggregateEntity, DimensionStruct dimensions, bool validOldDimensions, DimensionStruct oldDimensions, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void entityIdentifiersUpdated(HlaAggregateEntityPtr aggregateEntity, std::vector<std::string > entityIdentifiers, bool validOldEntityIdentifiers, std::vector<std::string > oldEntityIdentifiers, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void forceIdentifierUpdated(HlaAggregateEntityPtr aggregateEntity, ForceIdentifierEnum::ForceIdentifierEnum forceIdentifier, bool validOldForceIdentifier, ForceIdentifierEnum::ForceIdentifierEnum oldForceIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void formationUpdated(HlaAggregateEntityPtr aggregateEntity, FormationEnum::FormationEnum formation, bool validOldFormation, FormationEnum::FormationEnum oldFormation, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void numberOfSilentEntitiesUpdated(HlaAggregateEntityPtr aggregateEntity, short numberOfSilentEntities, bool validOldNumberOfSilentEntities, short oldNumberOfSilentEntities, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void numberOfVariableDatumsUpdated(HlaAggregateEntityPtr aggregateEntity, unsigned int numberOfVariableDatums, bool validOldNumberOfVariableDatums, unsigned int oldNumberOfVariableDatums, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void silentAggregatesUpdated(HlaAggregateEntityPtr aggregateEntity, std::vector<DevStudio::SilentAggregateStruct > silentAggregates, bool validOldSilentAggregates, std::vector<DevStudio::SilentAggregateStruct > oldSilentAggregates, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void silentEntitiesUpdated(HlaAggregateEntityPtr aggregateEntity, std::vector<DevStudio::SilentEntityStruct > silentEntities, bool validOldSilentEntities, std::vector<DevStudio::SilentEntityStruct > oldSilentEntities, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void subAggregateIdentifiersUpdated(HlaAggregateEntityPtr aggregateEntity, std::vector<std::string > subAggregateIdentifiers, bool validOldSubAggregateIdentifiers, std::vector<std::string > oldSubAggregateIdentifiers, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void variableDatumsUpdated(HlaAggregateEntityPtr aggregateEntity, std::vector<DevStudio::VariableDatumStruct > variableDatums, bool validOldVariableDatums, std::vector<DevStudio::VariableDatumStruct > oldVariableDatums, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void entityTypeUpdated(HlaAggregateEntityPtr aggregateEntity, EntityTypeStruct entityType, bool validOldEntityType, EntityTypeStruct oldEntityType, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void entityIdentifierUpdated(HlaAggregateEntityPtr aggregateEntity, EntityIdentifierStruct entityIdentifier, bool validOldEntityIdentifier, EntityIdentifierStruct oldEntityIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void isPartOfUpdated(HlaAggregateEntityPtr aggregateEntity, IsPartOfStruct isPartOf, bool validOldIsPartOf, IsPartOfStruct oldIsPartOf, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void spatialUpdated(HlaAggregateEntityPtr aggregateEntity, SpatialVariantStruct spatial, bool validOldSpatial, SpatialVariantStruct oldSpatial, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void relativeSpatialUpdated(HlaAggregateEntityPtr aggregateEntity, SpatialVariantStruct relativeSpatial, bool validOldRelativeSpatial, SpatialVariantStruct oldRelativeSpatial, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
      LIBAPI virtual void producingFederateUpdated(HlaAggregateEntityPtr aggregateEntity, HlaAggregateEntityAttributes::Attribute attribute, HlaFederateIdPtr oldProducingFederate, HlaFederateIdPtr newProducingFederate, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
   };

}
#endif
