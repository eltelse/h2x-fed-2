/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAACTIVESONARBEAMUPDATER_H
#define DEVELOPER_STUDIO_HLAACTIVESONARBEAMUPDATER_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <boost/noncopyable.hpp>

#include <DevStudio/datatypes/ActiveSonarScanPatternEnum.h>
#include <DevStudio/datatypes/EventIdentifierStruct.h>
#include <string>

#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaException.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaHLAobjectRootUpdater.h>

namespace DevStudio {

    /**
    * Updater used to update attribute values.
    */
    class HlaActiveSonarBeamUpdater : public HlaHLAobjectRootUpdater {

    public:

    LIBAPI virtual ~HlaActiveSonarBeamUpdater() {}

    /**
    * Set the activeEmissionParameterIndex for this update.
    * <br>Description from the FOM: <i>An index into the database of active (intentional) underwater acoustics emissions.</i>
    * <br>Description of the data type from the FOM: <i>Integer in the range [-2^15, 2^15-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
    *
    * @param activeEmissionParameterIndex the new activeEmissionParameterIndex
    */
    LIBAPI virtual void setActiveEmissionParameterIndex(const short& activeEmissionParameterIndex) = 0;

    /**
    * Set the activeSonarIdentifier for this update.
    * <br>Description from the FOM: <i>The RTI ID of the ActiveSonar emitting this beam</i>
    * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
    *
    * @param activeSonarIdentifier the new activeSonarIdentifier
    */
    LIBAPI virtual void setActiveSonarIdentifier(const std::string& activeSonarIdentifier) = 0;

    /**
    * Set the azimuthBeamwidth for this update.
    * <br>Description from the FOM: <i>The horizontal width of the main beam (as opposed to any side lobes) measured from beam center to the 3 dB down point. Omni directional beams shall have a beam width of 0 radians.</i>
    * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
    *
    * @param azimuthBeamwidth the new azimuthBeamwidth
    */
    LIBAPI virtual void setAzimuthBeamwidth(const float& azimuthBeamwidth) = 0;

    /**
    * Set the azimuthCenter for this update.
    * <br>Description from the FOM: <i>The center azimuthal bearing of the main beam (as opposed to side lobes) in relation to the own ship heading, clockwise positive. Omnidirection beams shall have an azimuthal center of 0 radians.</i>
    * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
    *
    * @param azimuthCenter the new azimuthCenter
    */
    LIBAPI virtual void setAzimuthCenter(const float& azimuthCenter) = 0;

    /**
    * Set the beamIdentifier for this update.
    * <br>Description from the FOM: <i>The identification of the active sonar beam, which must be unique on the active sonar system.</i>
    * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
    *
    * @param beamIdentifier the new beamIdentifier
    */
    LIBAPI virtual void setBeamIdentifier(const char& beamIdentifier) = 0;

    /**
    * Set the elevationBeamwidth for this update.
    * <br>Description from the FOM: <i>Vertical angle from beam center to 3db down point. O is omnidirectional</i>
    * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
    *
    * @param elevationBeamwidth the new elevationBeamwidth
    */
    LIBAPI virtual void setElevationBeamwidth(const float& elevationBeamwidth) = 0;

    /**
    * Set the elevationCenter for this update.
    * <br>Description from the FOM: <i>The angle of axis of the beam center to the horizontal plane. Positive upward.</i>
    * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
    *
    * @param elevationCenter the new elevationCenter
    */
    LIBAPI virtual void setElevationCenter(const float& elevationCenter) = 0;

    /**
    * Set the eventIdentifier for this update.
    * <br>Description from the FOM: <i>Used to associate changes to the beam with its ActiveSonar.</i>
    * <br>Description of the data type from the FOM: <i>Identification of an event. Based on the Event Identifier record as specified in IEEE 1278.1-1995 section 5.2.18.</i>
    *
    * @param eventIdentifier the new eventIdentifier
    */
    LIBAPI virtual void setEventIdentifier(const DevStudio::EventIdentifierStruct& eventIdentifier) = 0;

    /**
    * Set the scanPattern for this update.
    * <br>Description from the FOM: <i>The pattern that describes the angular movement of the sonar beam during its sweep.</i>
    * <br>Description of the data type from the FOM: <i>Acoustic scan pattern</i>
    *
    * @param scanPattern the new scanPattern
    */
    LIBAPI virtual void setScanPattern(const DevStudio::ActiveSonarScanPatternEnum::ActiveSonarScanPatternEnum& scanPattern) = 0;

    /**
    * Send all the attributes.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate()
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

    /**
    * Send all the attributes with a specified timestamp.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @param timestamp timestamp to send for this update
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate(HlaTimeStampPtr timestamp)
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

    /**
    * Send all the attributes with a specified timestamp.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @param logicalTime logical time to send for this update
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaInvalidLogicalTimeException if the logical time is not valid.
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate(HlaLogicalTimePtr logicalTime)
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaInvalidLogicalTimeException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

    /**
    * Send all the attributes with a specified timestamp.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @param timestamp timestamp to send for this update
    * @param logicalTime logical time to send for this update
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaInvalidLogicalTimeException if the logical time is not valid.
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate(HlaTimeStampPtr timestamp, HlaLogicalTimePtr logicalTime)
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaInvalidLogicalTimeException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;
    };
}
#endif
