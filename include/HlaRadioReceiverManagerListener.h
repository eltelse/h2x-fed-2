/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLARADIORECEIVERMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLARADIORECEIVERMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaRadioReceiver.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaRadioReceiver instances.
    */
    class HlaRadioReceiverManagerListener {

    public:

        LIBAPI virtual ~HlaRadioReceiverManagerListener() {}

        /**
        * This method is called when a new HlaRadioReceiver instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param radioReceiver the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaRadioReceiverDiscovered(HlaRadioReceiverPtr radioReceiver, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaRadioReceiver instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param radioReceiver the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaRadioReceiverInitialized(HlaRadioReceiverPtr radioReceiver, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaRadioReceiverManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param radioReceiver the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaRadioReceiverInInterest(HlaRadioReceiverPtr radioReceiver, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaRadioReceiverManagerListener instance goes out of interest.
        *
        * @param radioReceiver the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaRadioReceiverOutOfInterest(HlaRadioReceiverPtr radioReceiver, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaRadioReceiver instance is deleted.
        *
        * @param radioReceiver the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaRadioReceiverDeleted(HlaRadioReceiverPtr radioReceiver, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaRadioReceiverManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaRadioReceiverManagerListener::Adapter : public HlaRadioReceiverManagerListener {

    public:
        LIBAPI virtual void hlaRadioReceiverDiscovered(HlaRadioReceiverPtr radioReceiver, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaRadioReceiverInitialized(HlaRadioReceiverPtr radioReceiver, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaRadioReceiverInInterest(HlaRadioReceiverPtr radioReceiver, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaRadioReceiverOutOfInterest(HlaRadioReceiverPtr radioReceiver, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaRadioReceiverDeleted(HlaRadioReceiverPtr radioReceiver, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
