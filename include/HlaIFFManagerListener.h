/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAIFFMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLAIFFMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaIFF.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaIFF instances.
    */
    class HlaIFFManagerListener {

    public:

        LIBAPI virtual ~HlaIFFManagerListener() {}

        /**
        * This method is called when a new HlaIFF instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param iFF the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaIFFDiscovered(HlaIFFPtr iFF, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaIFF instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param iFF the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaIFFInitialized(HlaIFFPtr iFF, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaIFFManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param iFF the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaIFFInInterest(HlaIFFPtr iFF, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaIFFManagerListener instance goes out of interest.
        *
        * @param iFF the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaIFFOutOfInterest(HlaIFFPtr iFF, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaIFF instance is deleted.
        *
        * @param iFF the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaIFFDeleted(HlaIFFPtr iFF, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaIFFManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaIFFManagerListener::Adapter : public HlaIFFManagerListener {

    public:
        LIBAPI virtual void hlaIFFDiscovered(HlaIFFPtr iFF, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaIFFInitialized(HlaIFFPtr iFF, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaIFFInInterest(HlaIFFPtr iFF, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaIFFOutOfInterest(HlaIFFPtr iFF, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaIFFDeleted(HlaIFFPtr iFF, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
