/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAPROPULSIONNOISEMANAGER_H
#define DEVELOPER_STUDIO_HLAPROPULSIONNOISEMANAGER_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EventIdentifierStruct.h>
#include <DevStudio/datatypes/PropulsionPlantEnum.h>
#include <DevStudio/datatypes/RelativePositionStruct.h>
#include <DevStudio/datatypes/ShaftDataStruct.h>
#include <DevStudio/datatypes/ShaftDataStructLengthlessArray1Plus.h>
#include <string>
#include <vector>

#include <list>
#include <vector>

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaPropulsionNoiseManagerListener.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaException.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

   /**
   * Manager used to manage HlaPropulsionNoises.
   */
   class HlaPropulsionNoiseManager : private boost::noncopyable {

   public:

      LIBAPI virtual ~HlaPropulsionNoiseManager() {}

      /**
      * Gets a list of all HlaPropulsionNoises within interest, both local and remote.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of HlaPropulsionNoises
      */
      LIBAPI virtual std::list<HlaPropulsionNoisePtr> getHlaPropulsionNoises() = 0;

      /**
      * Gets a list of all HlaPropulsionNoises, both local and remote.
      * HlaPropulsionNoises not within interest are also included.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of all HlaPropulsionNoises
      */
      LIBAPI virtual std::list<HlaPropulsionNoisePtr> getAllHlaPropulsionNoises() = 0;

      /**
      * Gets a list of local HlaPropulsionNoises within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of local HlaPropulsionNoises
      */
      LIBAPI virtual std::list<HlaPropulsionNoisePtr> getLocalHlaPropulsionNoises() = 0;

      /**
      * Gets a list of remote HlaPropulsionNoises within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of remote HlaPropulsionNoises
      */
      LIBAPI virtual std::list<HlaPropulsionNoisePtr> getRemoteHlaPropulsionNoises() = 0;

      /**
      * Find a HlaPropulsionNoise with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> of the HlaPropulsionNoise to find
      *
      * @return the specified HlaPropulsionNoise, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaPropulsionNoisePtr getPropulsionNoiseByHlaInstanceName(const std::wstring& hlaInstanceName) = 0;

      /**
      * Find a HlaPropulsionNoise with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param encodedHlaInstanceHandle the encoded <code>HLA instance handle</code> of the HlaPropulsionNoise to find
      *
      * @return the specified HlaPropulsionNoise, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaPropulsionNoisePtr getPropulsionNoiseByHlaInstanceHandle(const std::vector<char>& encodedHlaInstanceHandle) = 0;

      /**
      * Creates a new HlaPropulsionNoise, and sets all <i>Create</i> attributes.
      *
      * @return Returns a new local HlaPropulsionNoise.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaPropulsionNoisePtr createLocalHlaPropulsionNoise(
      ) THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Creates a new HlaPropulsionNoise with a specified <code>HLA instance name</code>, and sets all
      * <i>Create</i> attributes.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> to register for this instance
      *
      * @return Returns a new local HlaPropulsionNoise.
      *
      * @throws HlaIllegalInstanceNameException if the name is illegal
      * @throws HlaInstanceNameInUseException if the name is already in use
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaPropulsionNoisePtr createLocalHlaPropulsionNoise(
         const std::wstring& hlaInstanceName
      ) THROW_SPEC (HlaIllegalInstanceNameException, HlaInstanceNameInUseException,
                    HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaPropulsionNoise and removes it from the federation.
      *
      * @param propulsionNoise The HlaPropulsionNoise to delete
      *
      * @return the HlaPropulsionNoise deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaPropulsionNoisePtr deleteLocalHlaPropulsionNoise(HlaPropulsionNoisePtr propulsionNoise)
          THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                      HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaPropulsionNoise and removes it from the federation.
      *
      * @param propulsionNoise The HlaPropulsionNoise to delete
      * @param timestamp timestamp when the instance was removed.
      *
      * @return the HlaPropulsionNoise deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaPropulsionNoisePtr deleteLocalHlaPropulsionNoise(HlaPropulsionNoisePtr propulsionNoise, HlaTimeStampPtr timestamp)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaPropulsionNoise and removes it from the federation.
      *
      * @param propulsionNoise The HlaPropulsionNoise to delete
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaPropulsionNoise deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaPropulsionNoisePtr deleteLocalHlaPropulsionNoise(HlaPropulsionNoisePtr propulsionNoise, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaPropulsionNoise and removes it from the federation.
      *
      * @param propulsionNoise The HlaPropulsionNoise to delete
      * @param timestamp timestamp when the instance was removed.
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaPropulsionNoise deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaPropulsionNoisePtr deleteLocalHlaPropulsionNoise(HlaPropulsionNoisePtr propulsionNoise, HlaTimeStampPtr timestamp, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Add a HlaPropulsionNoise manager listener.
      *
      * @param managerListener listener to add
      */
      LIBAPI virtual void addHlaPropulsionNoiseManagerListener(HlaPropulsionNoiseManagerListenerPtr managerListener) = 0;

      /**
      * Removed a HlaPropulsionNoise manager listener.
      *
      * @param managerListener listener to remove
      */
      LIBAPI virtual void removeHlaPropulsionNoiseManagerListener(HlaPropulsionNoiseManagerListenerPtr managerListener) = 0;

     /**
      * Add a listener that will automatically be added to all instances of HlaPropulsionNoise (Both local and remote).
      * The listener is notified when any attribute of an instance of HlaPropulsionNoise is updated.
      * The listener is also called when an interaction is sent to an instance of HlaPropulsionNoise.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param listener The listener to add.
      */
      LIBAPI virtual void addHlaPropulsionNoiseDefaultInstanceListener(HlaPropulsionNoiseListenerPtr listener) = 0;

     /**
      * Remove a listener from the set of listeners that are automatically added to new instances of HlaPropulsionNoise.
      * Note: The listener will not be removed from already existing instances of HlaPropulsionNoise.
      * This method is idempotent.
      *
      * @param listener The listener to remove.
      */
      LIBAPI virtual void removeHlaPropulsionNoiseDefaultInstanceListener(HlaPropulsionNoiseListenerPtr listener) = 0;

     /**
      * Add a valueListener that will automatically be added to all instances of HlaPropulsionNoise (Both local and remote).
      * The  Valuelistener is notified when any attribute of an instance of HlaPropulsionNoise is updated.
      * Note that the attribute values that are notified are not guaranteed to
      * be the latest available values for that attribute.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to add.
      */
      LIBAPI virtual void addHlaPropulsionNoiseDefaultInstanceValueListener(HlaPropulsionNoiseValueListenerPtr valueListener) = 0;

     /**
      * Remove a valueListener from the set of listeners that are automatically added to new instances of HlaPropulsionNoise.
      * Note: The valueListener will not be removed from already existing instances of HlaPropulsionNoise.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to remove.
      */
      LIBAPI virtual void removeHlaPropulsionNoiseDefaultInstanceValueListener(HlaPropulsionNoiseValueListenerPtr valueListener) = 0;

     /**
      * Enables or disables the HlaPropulsionNoise manager.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaPropulsionNoise manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      *
      * @param enabled <code>true</code> to enable the HlaPropulsionNoise manager, otherwise <code>false</code>
      */
      LIBAPI virtual void setEnabled(bool enabled) = 0;

     /**
      * Enables the HlaPropulsionNoise manager if it is avalable in the FOM at runtime.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaPropulsionNoise manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      */
      LIBAPI virtual void setEnabledIfAvailableInFom() = 0;

     /**
      * Check if the HlaPropulsionNoise manager is actually enabled when connected.
      * An HlaPropulsionNoise manager may be disabled with the <code>setEnabled(false)</code> function, or it may be configured
      * as <i>if available in the FOM</i> by the <code>setEnabledIfAvailableInFom()</code> function and
      * then not available in the FOM at runtime.
      * <p>
      * This method should only be used when connected, it will always return <code>false</code>
      * when not connected.
      *
      * @return <code>true</code> if connected and the HlaPropulsionNoise manager is enabled.
      */
      LIBAPI virtual bool isEnabled() = 0;
   };
}
#endif
