/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLABURSTPOINTOBJECTMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLABURSTPOINTOBJECTMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaBurstPointObject.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaBurstPointObject instances.
    */
    class HlaBurstPointObjectManagerListener {

    public:

        LIBAPI virtual ~HlaBurstPointObjectManagerListener() {}

        /**
        * This method is called when a new HlaBurstPointObject instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param burstPointObject the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaBurstPointObjectDiscovered(HlaBurstPointObjectPtr burstPointObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaBurstPointObject instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param burstPointObject the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaBurstPointObjectInitialized(HlaBurstPointObjectPtr burstPointObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaBurstPointObjectManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param burstPointObject the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaBurstPointObjectInInterest(HlaBurstPointObjectPtr burstPointObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaBurstPointObjectManagerListener instance goes out of interest.
        *
        * @param burstPointObject the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaBurstPointObjectOutOfInterest(HlaBurstPointObjectPtr burstPointObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaBurstPointObject instance is deleted.
        *
        * @param burstPointObject the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaBurstPointObjectDeleted(HlaBurstPointObjectPtr burstPointObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaBurstPointObjectManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaBurstPointObjectManagerListener::Adapter : public HlaBurstPointObjectManagerListener {

    public:
        LIBAPI virtual void hlaBurstPointObjectDiscovered(HlaBurstPointObjectPtr burstPointObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaBurstPointObjectInitialized(HlaBurstPointObjectPtr burstPointObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaBurstPointObjectInInterest(HlaBurstPointObjectPtr burstPointObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaBurstPointObjectOutOfInterest(HlaBurstPointObjectPtr burstPointObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaBurstPointObjectDeleted(HlaBurstPointObjectPtr burstPointObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
