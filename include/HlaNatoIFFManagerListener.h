/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLANATOIFFMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLANATOIFFMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaNatoIFF.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaNatoIFF instances.
    */
    class HlaNatoIFFManagerListener {

    public:

        LIBAPI virtual ~HlaNatoIFFManagerListener() {}

        /**
        * This method is called when a new HlaNatoIFF instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param natoIFF the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaNatoIFFDiscovered(HlaNatoIFFPtr natoIFF, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaNatoIFF instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param natoIFF the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaNatoIFFInitialized(HlaNatoIFFPtr natoIFF, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaNatoIFFManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param natoIFF the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaNatoIFFInInterest(HlaNatoIFFPtr natoIFF, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaNatoIFFManagerListener instance goes out of interest.
        *
        * @param natoIFF the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaNatoIFFOutOfInterest(HlaNatoIFFPtr natoIFF, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaNatoIFF instance is deleted.
        *
        * @param natoIFF the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaNatoIFFDeleted(HlaNatoIFFPtr natoIFF, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaNatoIFFManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaNatoIFFManagerListener::Adapter : public HlaNatoIFFManagerListener {

    public:
        LIBAPI virtual void hlaNatoIFFDiscovered(HlaNatoIFFPtr natoIFF, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaNatoIFFInitialized(HlaNatoIFFPtr natoIFF, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaNatoIFFInInterest(HlaNatoIFFPtr natoIFF, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaNatoIFFOutOfInterest(HlaNatoIFFPtr natoIFF, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaNatoIFFDeleted(HlaNatoIFFPtr natoIFF, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
