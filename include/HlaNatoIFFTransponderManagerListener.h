/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLANATOIFFTRANSPONDERMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLANATOIFFTRANSPONDERMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaNatoIFFTransponder.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaNatoIFFTransponder instances.
    */
    class HlaNatoIFFTransponderManagerListener {

    public:

        LIBAPI virtual ~HlaNatoIFFTransponderManagerListener() {}

        /**
        * This method is called when a new HlaNatoIFFTransponder instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param natoIFFTransponder the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaNatoIFFTransponderDiscovered(HlaNatoIFFTransponderPtr natoIFFTransponder, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaNatoIFFTransponder instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param natoIFFTransponder the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaNatoIFFTransponderInitialized(HlaNatoIFFTransponderPtr natoIFFTransponder, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaNatoIFFTransponderManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param natoIFFTransponder the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaNatoIFFTransponderInInterest(HlaNatoIFFTransponderPtr natoIFFTransponder, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaNatoIFFTransponderManagerListener instance goes out of interest.
        *
        * @param natoIFFTransponder the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaNatoIFFTransponderOutOfInterest(HlaNatoIFFTransponderPtr natoIFFTransponder, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaNatoIFFTransponder instance is deleted.
        *
        * @param natoIFFTransponder the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaNatoIFFTransponderDeleted(HlaNatoIFFTransponderPtr natoIFFTransponder, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaNatoIFFTransponderManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaNatoIFFTransponderManagerListener::Adapter : public HlaNatoIFFTransponderManagerListener {

    public:
        LIBAPI virtual void hlaNatoIFFTransponderDiscovered(HlaNatoIFFTransponderPtr natoIFFTransponder, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaNatoIFFTransponderInitialized(HlaNatoIFFTransponderPtr natoIFFTransponder, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaNatoIFFTransponderInInterest(HlaNatoIFFTransponderPtr natoIFFTransponder, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaNatoIFFTransponderOutOfInterest(HlaNatoIFFTransponderPtr natoIFFTransponder, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaNatoIFFTransponderDeleted(HlaNatoIFFTransponderPtr natoIFFTransponder, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
