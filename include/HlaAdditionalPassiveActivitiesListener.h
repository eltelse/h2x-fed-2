/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAADDITIONALPASSIVEACTIVITIESLISTENER_H
#define DEVELOPER_STUDIO_HLAADDITIONALPASSIVEACTIVITIESLISTENER_H

#include <set>


#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaAdditionalPassiveActivitiesAttributes.h>

namespace DevStudio {
   /**
   * Listener for updates of attributes.  
   */
   class HlaAdditionalPassiveActivitiesListener {
   public:

      LIBAPI virtual ~HlaAdditionalPassiveActivitiesListener() {}

      /**
      * This method is called when a HLA <code>reflectAttributeValueUpdate</code> is received for an remote object,
      * or a set of attributes is updated on a local object.
      *
      * @param additionalPassiveActivities The additionalPassiveActivities which this update corresponds to.
      * @param attributes The set of attributes that are updated.
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time the update was initiated.
      */
      LIBAPI virtual void attributesUpdated(HlaAdditionalPassiveActivitiesPtr additionalPassiveActivities, std::set<HlaAdditionalPassiveActivitiesAttributes::Attribute> &attributes, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

      class Adapter;
   };

  /**
  * An adapter class that implements the HlaAdditionalPassiveActivitiesListener interface with empty methods.
  * It might be used as a base class for a listener.
  */
  class HlaAdditionalPassiveActivitiesListener::Adapter : public HlaAdditionalPassiveActivitiesListener {

  public:

     LIBAPI virtual void attributesUpdated(HlaAdditionalPassiveActivitiesPtr additionalPassiveActivities, std::set<HlaAdditionalPassiveActivitiesAttributes::Attribute> &attributes, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
    };

}
#endif
