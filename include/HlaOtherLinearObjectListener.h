/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAOTHERLINEAROBJECTLISTENER_H
#define DEVELOPER_STUDIO_HLAOTHERLINEAROBJECTLISTENER_H

#include <set>


#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaOtherLinearObjectAttributes.h>

namespace DevStudio {
   /**
   * Listener for updates of attributes.  
   */
   class HlaOtherLinearObjectListener {
   public:

      LIBAPI virtual ~HlaOtherLinearObjectListener() {}

      /**
      * This method is called when a HLA <code>reflectAttributeValueUpdate</code> is received for an remote object,
      * or a set of attributes is updated on a local object.
      *
      * @param otherLinearObject The otherLinearObject which this update corresponds to.
      * @param attributes The set of attributes that are updated.
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time the update was initiated.
      */
      LIBAPI virtual void attributesUpdated(HlaOtherLinearObjectPtr otherLinearObject, std::set<HlaOtherLinearObjectAttributes::Attribute> &attributes, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

      class Adapter;
   };

  /**
  * An adapter class that implements the HlaOtherLinearObjectListener interface with empty methods.
  * It might be used as a base class for a listener.
  */
  class HlaOtherLinearObjectListener::Adapter : public HlaOtherLinearObjectListener {

  public:

     LIBAPI virtual void attributesUpdated(HlaOtherLinearObjectPtr otherLinearObject, std::set<HlaOtherLinearObjectAttributes::Attribute> &attributes, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
    };

}
#endif
