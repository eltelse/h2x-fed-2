/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAPROPULSIONNOISEVALUELISTENER_H
#define DEVELOPER_STUDIO_HLAPROPULSIONNOISEVALUELISTENER_H

#include <memory>

#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EventIdentifierStruct.h>
#include <DevStudio/datatypes/PropulsionPlantEnum.h>
#include <DevStudio/datatypes/RelativePositionStruct.h>
#include <DevStudio/datatypes/ShaftDataStruct.h>
#include <DevStudio/datatypes/ShaftDataStructLengthlessArray1Plus.h>
#include <string>
#include <vector>

#include <DevStudio/HlaLogicalTime.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaPropulsionNoiseAttributes.h>    

namespace DevStudio {

   /**
   * Listener for updates of attributes, with the new updated values.  
   */
   class HlaPropulsionNoiseValueListener {

   public:

      LIBAPI virtual ~HlaPropulsionNoiseValueListener() {}
    
      /**
      * This method is called when the attribute <code>hullMaskerOn</code> is updated.
      * <br>Description from the FOM: <i>Whether the hull masker silencing system is on.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param propulsionNoise The object which is updated.
      * @param hullMaskerOn The new value of the attribute in this update
      * @param validOldHullMaskerOn True if oldHullMaskerOn contains a valid value
      * @param oldHullMaskerOn The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void hullMaskerOnUpdated(HlaPropulsionNoisePtr propulsionNoise, bool hullMaskerOn, bool validOldHullMaskerOn, bool oldHullMaskerOn, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>passiveParameterIndex</code> is updated.
      * <br>Description from the FOM: <i>Database index to look up the entities acoustic signature</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param propulsionNoise The object which is updated.
      * @param passiveParameterIndex The new value of the attribute in this update
      * @param validOldPassiveParameterIndex True if oldPassiveParameterIndex contains a valid value
      * @param oldPassiveParameterIndex The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void passiveParameterIndexUpdated(HlaPropulsionNoisePtr propulsionNoise, unsigned short passiveParameterIndex, bool validOldPassiveParameterIndex, unsigned short oldPassiveParameterIndex, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>propulsionPlantConfiguration</code> is updated.
      * <br>Description from the FOM: <i>The operating mode of the propulsion plant</i>
      * <br>Description of the data type from the FOM: <i>Propulsion plant configuration</i>
      *
      * @param propulsionNoise The object which is updated.
      * @param propulsionPlantConfiguration The new value of the attribute in this update
      * @param validOldPropulsionPlantConfiguration True if oldPropulsionPlantConfiguration contains a valid value
      * @param oldPropulsionPlantConfiguration The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void propulsionPlantConfigurationUpdated(HlaPropulsionNoisePtr propulsionNoise, PropulsionPlantEnum::PropulsionPlantEnum propulsionPlantConfiguration, bool validOldPropulsionPlantConfiguration, PropulsionPlantEnum::PropulsionPlantEnum oldPropulsionPlantConfiguration, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>shaftRateData</code> is updated.
      * <br>Description from the FOM: <i>Information about each of the propulsion shafts associated with the entity. Shafts are ordered from port to starboard, when looking from the stern to the bow.</i>
      * <br>Description of the data type from the FOM: <i>Array of propulsion shaft states, one per shaft</i>
      *
      * @param propulsionNoise The object which is updated.
      * @param shaftRateData The new value of the attribute in this update
      * @param validOldShaftRateData True if oldShaftRateData contains a valid value
      * @param oldShaftRateData The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void shaftRateDataUpdated(HlaPropulsionNoisePtr propulsionNoise, std::vector</* not empty */ DevStudio::ShaftDataStruct > shaftRateData, bool validOldShaftRateData, std::vector</* not empty */ DevStudio::ShaftDataStruct > oldShaftRateData, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>eventIdentifier</code> is updated.
      * <br>Description from the FOM: <i>The generating federate uses the Event Identifier to associate related events. The event number begins at one at the beginning of the exercise and is incremented by one for each event.</i>
      * <br>Description of the data type from the FOM: <i>Identification of an event. Based on the Event Identifier record as specified in IEEE 1278.1-1995 section 5.2.18.</i>
      *
      * @param propulsionNoise The object which is updated.
      * @param eventIdentifier The new value of the attribute in this update
      * @param validOldEventIdentifier True if oldEventIdentifier contains a valid value
      * @param oldEventIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void eventIdentifierUpdated(HlaPropulsionNoisePtr propulsionNoise, EventIdentifierStruct eventIdentifier, bool validOldEventIdentifier, EventIdentifierStruct oldEventIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>entityIdentifier</code> is updated.
      * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @param propulsionNoise The object which is updated.
      * @param entityIdentifier The new value of the attribute in this update
      * @param validOldEntityIdentifier True if oldEntityIdentifier contains a valid value
      * @param oldEntityIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void entityIdentifierUpdated(HlaPropulsionNoisePtr propulsionNoise, EntityIdentifierStruct entityIdentifier, bool validOldEntityIdentifier, EntityIdentifierStruct oldEntityIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>hostObjectIdentifier</code> is updated.
      * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @param propulsionNoise The object which is updated.
      * @param hostObjectIdentifier The new value of the attribute in this update
      * @param validOldHostObjectIdentifier True if oldHostObjectIdentifier contains a valid value
      * @param oldHostObjectIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void hostObjectIdentifierUpdated(HlaPropulsionNoisePtr propulsionNoise, std::string hostObjectIdentifier, bool validOldHostObjectIdentifier, std::string oldHostObjectIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>relativePosition</code> is updated.
      * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
      * <br>Description of the data type from the FOM: <i>Relative position in right-handed Cartesian coordinates.</i>
      *
      * @param propulsionNoise The object which is updated.
      * @param relativePosition The new value of the attribute in this update
      * @param validOldRelativePosition True if oldRelativePosition contains a valid value
      * @param oldRelativePosition The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void relativePositionUpdated(HlaPropulsionNoisePtr propulsionNoise, RelativePositionStruct relativePosition, bool validOldRelativePosition, RelativePositionStruct oldRelativePosition, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
      /**
      * This method is called when the producing federate of an attribute is changed.
      * Only available when using HLA Evolved.
      *
      * @param propulsionNoise The object which is updated.
      * @param attribute The attribute that now has a new producing federate
      * @param oldProducingFederate The federate handle of the old producing federate
      * @param newProducingFederate The federate handle of the new producing federate
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void producingFederateUpdated(HlaPropulsionNoisePtr propulsionNoise, HlaPropulsionNoiseAttributes::Attribute attribute, HlaFederateIdPtr oldProducingFederate, HlaFederateIdPtr newProducingFederate, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;

      class Adapter;
   };

   /**
   * An adapter class that implements the HlaPropulsionNoiseValueListener interface with empty methods.
   * It might be used as a base class for a listener.
   */
   class HlaPropulsionNoiseValueListener::Adapter : public HlaPropulsionNoiseValueListener {

   public:

      LIBAPI virtual void hullMaskerOnUpdated(HlaPropulsionNoisePtr propulsionNoise, bool hullMaskerOn, bool validOldHullMaskerOn, bool oldHullMaskerOn, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void passiveParameterIndexUpdated(HlaPropulsionNoisePtr propulsionNoise, unsigned short passiveParameterIndex, bool validOldPassiveParameterIndex, unsigned short oldPassiveParameterIndex, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void propulsionPlantConfigurationUpdated(HlaPropulsionNoisePtr propulsionNoise, PropulsionPlantEnum::PropulsionPlantEnum propulsionPlantConfiguration, bool validOldPropulsionPlantConfiguration, PropulsionPlantEnum::PropulsionPlantEnum oldPropulsionPlantConfiguration, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void shaftRateDataUpdated(HlaPropulsionNoisePtr propulsionNoise, std::vector</* not empty */ DevStudio::ShaftDataStruct > shaftRateData, bool validOldShaftRateData, std::vector</* not empty */ DevStudio::ShaftDataStruct > oldShaftRateData, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void eventIdentifierUpdated(HlaPropulsionNoisePtr propulsionNoise, EventIdentifierStruct eventIdentifier, bool validOldEventIdentifier, EventIdentifierStruct oldEventIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void entityIdentifierUpdated(HlaPropulsionNoisePtr propulsionNoise, EntityIdentifierStruct entityIdentifier, bool validOldEntityIdentifier, EntityIdentifierStruct oldEntityIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void hostObjectIdentifierUpdated(HlaPropulsionNoisePtr propulsionNoise, std::string hostObjectIdentifier, bool validOldHostObjectIdentifier, std::string oldHostObjectIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void relativePositionUpdated(HlaPropulsionNoisePtr propulsionNoise, RelativePositionStruct relativePosition, bool validOldRelativePosition, RelativePositionStruct oldRelativePosition, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
      LIBAPI virtual void producingFederateUpdated(HlaPropulsionNoisePtr propulsionNoise, HlaPropulsionNoiseAttributes::Attribute attribute, HlaFederateIdPtr oldProducingFederate, HlaFederateIdPtr newProducingFederate, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
   };

}
#endif
