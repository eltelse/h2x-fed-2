/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAEMBEDDEDSYSTEMMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLAEMBEDDEDSYSTEMMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaEmbeddedSystem.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaEmbeddedSystem instances.
    */
    class HlaEmbeddedSystemManagerListener {

    public:

        LIBAPI virtual ~HlaEmbeddedSystemManagerListener() {}

        /**
        * This method is called when a new HlaEmbeddedSystem instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param embeddedSystem the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaEmbeddedSystemDiscovered(HlaEmbeddedSystemPtr embeddedSystem, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaEmbeddedSystem instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param embeddedSystem the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaEmbeddedSystemInitialized(HlaEmbeddedSystemPtr embeddedSystem, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaEmbeddedSystemManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param embeddedSystem the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaEmbeddedSystemInInterest(HlaEmbeddedSystemPtr embeddedSystem, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaEmbeddedSystemManagerListener instance goes out of interest.
        *
        * @param embeddedSystem the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaEmbeddedSystemOutOfInterest(HlaEmbeddedSystemPtr embeddedSystem, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaEmbeddedSystem instance is deleted.
        *
        * @param embeddedSystem the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaEmbeddedSystemDeleted(HlaEmbeddedSystemPtr embeddedSystem, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaEmbeddedSystemManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaEmbeddedSystemManagerListener::Adapter : public HlaEmbeddedSystemManagerListener {

    public:
        LIBAPI virtual void hlaEmbeddedSystemDiscovered(HlaEmbeddedSystemPtr embeddedSystem, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaEmbeddedSystemInitialized(HlaEmbeddedSystemPtr embeddedSystem, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaEmbeddedSystemInInterest(HlaEmbeddedSystemPtr embeddedSystem, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaEmbeddedSystemOutOfInterest(HlaEmbeddedSystemPtr embeddedSystem, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaEmbeddedSystemDeleted(HlaEmbeddedSystemPtr embeddedSystem, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
