/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLASPOTOBJECTUPDATER_H
#define DEVELOPER_STUDIO_HLASPOTOBJECTUPDATER_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <boost/noncopyable.hpp>

#include <DevStudio/datatypes/AcquisitionTypeEnum.h>
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EntityTypeStruct.h>
#include <DevStudio/datatypes/FortificationsEnum.h>
#include <DevStudio/datatypes/GeographicShapeStruct.h>
#include <DevStudio/datatypes/HostilityTypeEnum.h>
#include <DevStudio/datatypes/IsPartOfStruct.h>
#include <DevStudio/datatypes/PositionStruct.h>
#include <DevStudio/datatypes/SpatialVariantStruct.h>
#include <DevStudio/datatypes/SpotCategoryEnum.h>
#include <string>

#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaException.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaBaseEntityUpdater.h>

namespace DevStudio {

    /**
    * Updater used to update attribute values.
    */
    class HlaSpotObjectUpdater : public HlaBaseEntityUpdater {

    public:

    LIBAPI virtual ~HlaSpotObjectUpdater() {}

    /**
    * Set the quantity for this update.
    * <br>Description from the FOM: <i>Quantity of spotted targets.</i>
    * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
    *
    * @param quantity the new quantity
    */
    LIBAPI virtual void setQuantity(const short& quantity) = 0;

    /**
    * Set the reportingEntity for this update.
    * <br>Description from the FOM: <i>This field is holding a unique identifier for the reporting entity.</i>
    * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
    *
    * @param reportingEntity the new reportingEntity
    */
    LIBAPI virtual void setReportingEntity(const std::string& reportingEntity) = 0;

    /**
    * Set the c2ReportingEntity for this update.
    * <br>Description from the FOM: <i>This field is holding a unique c2 identifier for the reporting entity.</i>
    * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
    *
    * @param c2ReportingEntity the new c2ReportingEntity
    */
    LIBAPI virtual void setC2ReportingEntity(const std::string& c2ReportingEntity) = 0;

    /**
    * Set the detectionMeans for this update.
    * <br>Description from the FOM: <i>Which kind of detection device spotted the target.</i>
    * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
    *
    * @param detectionMeans the new detectionMeans
    */
    LIBAPI virtual void setDetectionMeans(const DevStudio::EntityTypeStruct& detectionMeans) = 0;

    /**
    * Set the hostilityType for this update.
    * <br>Description from the FOM: <i>Hostility type of spotted target.</i>
    * <br>Description of the data type from the FOM: <i>-NULL-</i>
    *
    * @param hostilityType the new hostilityType
    */
    LIBAPI virtual void setHostilityType(const DevStudio::HostilityTypeEnum::HostilityTypeEnum& hostilityType) = 0;

    /**
    * Set the description for this update.
    * <br>Description from the FOM: <i>The description of spotted event.</i>
    * <br>Description of the data type from the FOM: <i></i>
    *
    * @param description the new description
    */
    LIBAPI virtual void setDescription(const std::wstring& description) = 0;

    /**
    * Set the lastUpdateTime for this update.
    * <br>Description from the FOM: <i>The last time the spot object was updated.</i>
    * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
    *
    * @param lastUpdateTime the new lastUpdateTime
    */
    LIBAPI virtual void setLastUpdateTime(const short& lastUpdateTime) = 0;

    /**
    * Set the geographicShape for this update.
    * <br>Description from the FOM: <i>Representation of 2D Force geographic shape.</i>
    * <br>Description of the data type from the FOM: <i>-NULL-</i>
    *
    * @param geographicShape the new geographicShape
    */
    LIBAPI virtual void setGeographicShape(const DevStudio::GeographicShapeStruct& geographicShape) = 0;

    /**
    * Set the spottedEntityIdentifier for this update.
    * <br>Description from the FOM: <i>Entity identifier of spotted entity.</i>
    * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
    *
    * @param spottedEntityIdentifier the new spottedEntityIdentifier
    */
    LIBAPI virtual void setSpottedEntityIdentifier(const std::string& spottedEntityIdentifier) = 0;

    /**
    * Set the position for this update.
    * <br>Description from the FOM: <i>Position of the spot object.</i>
    * <br>Description of the data type from the FOM: <i>-NULL-</i>
    *
    * @param position the new position
    */
    LIBAPI virtual void setPosition(const DevStudio::PositionStruct& position) = 0;

    /**
    * Set the widthAccuracy for this update.
    * <br>Description from the FOM: <i>Width accuracy of the spot object. Will be filled using meters between 0-999.</i>
    * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
    *
    * @param widthAccuracy the new widthAccuracy
    */
    LIBAPI virtual void setWidthAccuracy(const float& widthAccuracy) = 0;

    /**
    * Set the lengthAccuracy for this update.
    * <br>Description from the FOM: <i>Length accuracy of the spot object. Will be filled using meters between 0-999.</i>
    * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
    *
    * @param lengthAccuracy the new lengthAccuracy
    */
    LIBAPI virtual void setLengthAccuracy(const float& lengthAccuracy) = 0;

    /**
    * Set the altitudeAccuracy for this update.
    * <br>Description from the FOM: <i>Altitude accuracy of the spot object. Will be filled using meters between 0-100.</i>
    * <br>Description of the data type from the FOM: <i> [unit: NA]</i>
    *
    * @param altitudeAccuracy the new altitudeAccuracy
    */
    LIBAPI virtual void setAltitudeAccuracy(const float& altitudeAccuracy) = 0;

    /**
    * Set the spotType for this update.
    * <br>Description from the FOM: <i>Spot object Type. Represented as an integer which its value is directing to an external enumeration.</i>
    * <br>Description of the data type from the FOM: <i>Integer in the range [-2^31, 2^31 - 1]</i>
    *
    * @param spotType the new spotType
    */
    LIBAPI virtual void setSpotType(const int& spotType) = 0;

    /**
    * Set the weaponType for this update.
    * <br>Description from the FOM: <i>Defines main weapon type</i>
    * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
    *
    * @param weaponType the new weaponType
    */
    LIBAPI virtual void setWeaponType(const DevStudio::EntityTypeStruct& weaponType) = 0;

    /**
    * Set the spotStatus for this update.
    * <br>Description from the FOM: <i>Spot object status (e.g: Alive, Destroyed and etc.). Represented as an integer which its value is directing to an external enumeration.</i>
    * <br>Description of the data type from the FOM: <i>Integer in the range [-2^31, 2^31 - 1]</i>
    *
    * @param spotStatus the new spotStatus
    */
    LIBAPI virtual void setSpotStatus(const int& spotStatus) = 0;

    /**
    * Set the hostile for this update.
    * <br>Description from the FOM: <i>Hostile (yes/no)</i>
    * <br>Description of the data type from the FOM: <i></i>
    *
    * @param hostile the new hostile
    */
    LIBAPI virtual void setHostile(const bool& hostile) = 0;

    /**
    * Set the infrastructure for this update.
    * <br>Description from the FOM: <i>Spot Infrastructure. Represented as an integer which its value is directing to an external enumeration.</i>
    * <br>Description of the data type from the FOM: <i>Integer in the range [-2^31, 2^31 - 1]</i>
    *
    * @param infrastructure the new infrastructure
    */
    LIBAPI virtual void setInfrastructure(const int& infrastructure) = 0;

    /**
    * Set the fortifications for this update.
    * <br>Description from the FOM: <i>Defines fortification type .</i>
    * <br>Description of the data type from the FOM: <i>-NULL-</i>
    *
    * @param fortifications the new fortifications
    */
    LIBAPI virtual void setFortifications(const DevStudio::FortificationsEnum::FortificationsEnum& fortifications) = 0;

    /**
    * Set the floors for this update.
    * <br>Description from the FOM: <i>Floor Number. (Max 8 characters).</i>
    * <br>Description of the data type from the FOM: <i></i>
    *
    * @param floors the new floors
    */
    LIBAPI virtual void setFloors(const std::wstring& floors) = 0;

    /**
    * Set the totalFloors for this update.
    * <br>Description from the FOM: <i>Number of total floors. (Max 8 characters).</i>
    * <br>Description of the data type from the FOM: <i></i>
    *
    * @param totalFloors the new totalFloors
    */
    LIBAPI virtual void setTotalFloors(const std::wstring& totalFloors) = 0;

    /**
    * Set the acquisitionType for this update.
    * <br>Description from the FOM: <i>Spot object acquisition Type.</i>
    * <br>Description of the data type from the FOM: <i>-NULL-</i>
    *
    * @param acquisitionType the new acquisitionType
    */
    LIBAPI virtual void setAcquisitionType(const DevStudio::AcquisitionTypeEnum::AcquisitionTypeEnum& acquisitionType) = 0;

    /**
    * Set the reportingEntityName for this update.
    * <br>Description from the FOM: <i>The name of the reporting unit/entity. (Max 17 characters).</i>
    * <br>Description of the data type from the FOM: <i></i>
    *
    * @param reportingEntityName the new reportingEntityName
    */
    LIBAPI virtual void setReportingEntityName(const std::wstring& reportingEntityName) = 0;

    /**
    * Set the armyOrg for this update.
    * <br>Description from the FOM: <i>Defines of which army organization the spotted entity belongs. Represented as an integer which its value is directing to an external enumeration.</i>
    * <br>Description of the data type from the FOM: <i>Integer in the range [-2^31, 2^31 - 1]</i>
    *
    * @param armyOrg the new armyOrg
    */
    LIBAPI virtual void setArmyOrg(const int& armyOrg) = 0;

    /**
    * Set the spotCategory for this update.
    * <br>Description from the FOM: <i>Spot object category.</i>
    * <br>Description of the data type from the FOM: <i>-NULL-</i>
    *
    * @param spotCategory the new spotCategory
    */
    LIBAPI virtual void setSpotCategory(const DevStudio::SpotCategoryEnum::SpotCategoryEnum& spotCategory) = 0;

    /**
    * Set the casualtiesNumber for this update.
    * <br>Description from the FOM: <i>Number of casualties in the spotted entity.</i>
    * <br>Description of the data type from the FOM: <i>Integer in the range [-2^31, 2^31 - 1]</i>
    *
    * @param casualtiesNumber the new casualtiesNumber
    */
    LIBAPI virtual void setCasualtiesNumber(const int& casualtiesNumber) = 0;

    /**
    * Set the spottedEntityActivityStatus for this update.
    * <br>Description from the FOM: <i>Spotted entity activity status. Represented as an integer which its value is directing to an external enumeration.</i>
    * <br>Description of the data type from the FOM: <i>Integer in the range [-2^31, 2^31 - 1]</i>
    *
    * @param spottedEntityActivityStatus the new spottedEntityActivityStatus
    */
    LIBAPI virtual void setSpottedEntityActivityStatus(const int& spottedEntityActivityStatus) = 0;

    /**
    * Set the entityType for this update.
    * <br>Description from the FOM: <i>The category of the entity.</i>
    * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
    *
    * @param entityType the new entityType
    */
    LIBAPI virtual void setEntityType(const DevStudio::EntityTypeStruct& entityType) = 0;

    /**
    * Set the entityIdentifier for this update.
    * <br>Description from the FOM: <i>The unique identifier for the entity instance.</i>
    * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
    *
    * @param entityIdentifier the new entityIdentifier
    */
    LIBAPI virtual void setEntityIdentifier(const DevStudio::EntityIdentifierStruct& entityIdentifier) = 0;

    /**
    * Set the isPartOf for this update.
    * <br>Description from the FOM: <i>Defines if the entity if a constituent part of another entity (denoted the host entity). If the entity is a constituent part of another entity then the HostEntityIdentifier shall be set to the EntityIdentifier of the host entity and the HostRTIObjectIdentifier shall be set to the RTI object instance ID of the host entity. If the entity is not a constituent part of another entity then the HostEntityIdentifier shall be set to 0.0.0 and the HostRTIObjectIdentifier shall be set to the empty string.</i>
    * <br>Description of the data type from the FOM: <i>Defines the spatial relationship between two objects.</i>
    *
    * @param isPartOf the new isPartOf
    */
    LIBAPI virtual void setIsPartOf(const DevStudio::IsPartOfStruct& isPartOf) = 0;

    /**
    * Set the spatial for this update.
    * <br>Description from the FOM: <i>Spatial state stored in one variant record attribute.</i>
    * <br>Description of the data type from the FOM: <i>Variant Record for a single spatial attribute.</i>
    *
    * @param spatial the new spatial
    */
    LIBAPI virtual void setSpatial(const DevStudio::SpatialVariantStruct& spatial) = 0;

    /**
    * Set the relativeSpatial for this update.
    * <br>Description from the FOM: <i>Relative spatial state stored in one variant record attribute.</i>
    * <br>Description of the data type from the FOM: <i>Variant Record for a single spatial attribute.</i>
    *
    * @param relativeSpatial the new relativeSpatial
    */
    LIBAPI virtual void setRelativeSpatial(const DevStudio::SpatialVariantStruct& relativeSpatial) = 0;

    /**
    * Send all the attributes.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate()
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

    /**
    * Send all the attributes with a specified timestamp.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @param timestamp timestamp to send for this update
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate(HlaTimeStampPtr timestamp)
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

    /**
    * Send all the attributes with a specified timestamp.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @param logicalTime logical time to send for this update
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaInvalidLogicalTimeException if the logical time is not valid.
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate(HlaLogicalTimePtr logicalTime)
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaInvalidLogicalTimeException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

    /**
    * Send all the attributes with a specified timestamp.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @param timestamp timestamp to send for this update
    * @param logicalTime logical time to send for this update
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaInvalidLogicalTimeException if the logical time is not valid.
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate(HlaTimeStampPtr timestamp, HlaLogicalTimePtr logicalTime)
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaInvalidLogicalTimeException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;
    };
}
#endif
