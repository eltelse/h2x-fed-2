/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLATHREATSPOTOBJECTMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLATHREATSPOTOBJECTMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaThreatSpotObject.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaThreatSpotObject instances.
    */
    class HlaThreatSpotObjectManagerListener {

    public:

        LIBAPI virtual ~HlaThreatSpotObjectManagerListener() {}

        /**
        * This method is called when a new HlaThreatSpotObject instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param threatSpotObject the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaThreatSpotObjectDiscovered(HlaThreatSpotObjectPtr threatSpotObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaThreatSpotObject instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param threatSpotObject the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaThreatSpotObjectInitialized(HlaThreatSpotObjectPtr threatSpotObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaThreatSpotObjectManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param threatSpotObject the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaThreatSpotObjectInInterest(HlaThreatSpotObjectPtr threatSpotObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaThreatSpotObjectManagerListener instance goes out of interest.
        *
        * @param threatSpotObject the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaThreatSpotObjectOutOfInterest(HlaThreatSpotObjectPtr threatSpotObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaThreatSpotObject instance is deleted.
        *
        * @param threatSpotObject the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaThreatSpotObjectDeleted(HlaThreatSpotObjectPtr threatSpotObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaThreatSpotObjectManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaThreatSpotObjectManagerListener::Adapter : public HlaThreatSpotObjectManagerListener {

    public:
        LIBAPI virtual void hlaThreatSpotObjectDiscovered(HlaThreatSpotObjectPtr threatSpotObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaThreatSpotObjectInitialized(HlaThreatSpotObjectPtr threatSpotObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaThreatSpotObjectInInterest(HlaThreatSpotObjectPtr threatSpotObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaThreatSpotObjectOutOfInterest(HlaThreatSpotObjectPtr threatSpotObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaThreatSpotObjectDeleted(HlaThreatSpotObjectPtr threatSpotObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
