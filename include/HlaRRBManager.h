/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLARRBMANAGER_H
#define DEVELOPER_STUDIO_HLARRBMANAGER_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EventIdentifierStruct.h>
#include <DevStudio/datatypes/FundamentalParameterDataStruct.h>
#include <DevStudio/datatypes/FundamentalParameterDataStructLengthlessArray.h>
#include <DevStudio/datatypes/IffOperationalParameter1Enum.h>
#include <DevStudio/datatypes/IffOperationalParameter2Enum.h>
#include <DevStudio/datatypes/IffSystemModeEnum.h>
#include <DevStudio/datatypes/IffSystemNameEnum.h>
#include <DevStudio/datatypes/IffSystemTypeEnum.h>
#include <DevStudio/datatypes/RelativePositionStruct.h>
#include <string>
#include <vector>

#include <list>
#include <vector>

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaRRBManagerListener.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaException.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

   /**
   * Manager used to manage HlaRRBs.
   */
   class HlaRRBManager : private boost::noncopyable {

   public:

      LIBAPI virtual ~HlaRRBManager() {}

      /**
      * Gets a list of all HlaRRBs within interest, both local and remote.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of HlaRRBs
      */
      LIBAPI virtual std::list<HlaRRBPtr> getHlaRRBs() = 0;

      /**
      * Gets a list of all HlaRRBs, both local and remote.
      * HlaRRBs not within interest are also included.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of all HlaRRBs
      */
      LIBAPI virtual std::list<HlaRRBPtr> getAllHlaRRBs() = 0;

      /**
      * Gets a list of local HlaRRBs within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of local HlaRRBs
      */
      LIBAPI virtual std::list<HlaRRBPtr> getLocalHlaRRBs() = 0;

      /**
      * Gets a list of remote HlaRRBs within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of remote HlaRRBs
      */
      LIBAPI virtual std::list<HlaRRBPtr> getRemoteHlaRRBs() = 0;

      /**
      * Find a HlaRRB with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> of the HlaRRB to find
      *
      * @return the specified HlaRRB, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaRRBPtr getRRBByHlaInstanceName(const std::wstring& hlaInstanceName) = 0;

      /**
      * Find a HlaRRB with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param encodedHlaInstanceHandle the encoded <code>HLA instance handle</code> of the HlaRRB to find
      *
      * @return the specified HlaRRB, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaRRBPtr getRRBByHlaInstanceHandle(const std::vector<char>& encodedHlaInstanceHandle) = 0;

      /**
      * Creates a new HlaRRB, and sets all <i>Create</i> attributes.
      *
      * @return Returns a new local HlaRRB.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaRRBPtr createLocalHlaRRB(
      ) THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Creates a new HlaRRB with a specified <code>HLA instance name</code>, and sets all
      * <i>Create</i> attributes.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> to register for this instance
      *
      * @return Returns a new local HlaRRB.
      *
      * @throws HlaIllegalInstanceNameException if the name is illegal
      * @throws HlaInstanceNameInUseException if the name is already in use
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaRRBPtr createLocalHlaRRB(
         const std::wstring& hlaInstanceName
      ) THROW_SPEC (HlaIllegalInstanceNameException, HlaInstanceNameInUseException,
                    HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaRRB and removes it from the federation.
      *
      * @param rRB The HlaRRB to delete
      *
      * @return the HlaRRB deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaRRBPtr deleteLocalHlaRRB(HlaRRBPtr rRB)
          THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                      HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaRRB and removes it from the federation.
      *
      * @param rRB The HlaRRB to delete
      * @param timestamp timestamp when the instance was removed.
      *
      * @return the HlaRRB deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaRRBPtr deleteLocalHlaRRB(HlaRRBPtr rRB, HlaTimeStampPtr timestamp)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaRRB and removes it from the federation.
      *
      * @param rRB The HlaRRB to delete
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaRRB deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaRRBPtr deleteLocalHlaRRB(HlaRRBPtr rRB, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaRRB and removes it from the federation.
      *
      * @param rRB The HlaRRB to delete
      * @param timestamp timestamp when the instance was removed.
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaRRB deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaRRBPtr deleteLocalHlaRRB(HlaRRBPtr rRB, HlaTimeStampPtr timestamp, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Add a HlaRRB manager listener.
      *
      * @param managerListener listener to add
      */
      LIBAPI virtual void addHlaRRBManagerListener(HlaRRBManagerListenerPtr managerListener) = 0;

      /**
      * Removed a HlaRRB manager listener.
      *
      * @param managerListener listener to remove
      */
      LIBAPI virtual void removeHlaRRBManagerListener(HlaRRBManagerListenerPtr managerListener) = 0;

     /**
      * Add a listener that will automatically be added to all instances of HlaRRB (Both local and remote).
      * The listener is notified when any attribute of an instance of HlaRRB is updated.
      * The listener is also called when an interaction is sent to an instance of HlaRRB.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param listener The listener to add.
      */
      LIBAPI virtual void addHlaRRBDefaultInstanceListener(HlaRRBListenerPtr listener) = 0;

     /**
      * Remove a listener from the set of listeners that are automatically added to new instances of HlaRRB.
      * Note: The listener will not be removed from already existing instances of HlaRRB.
      * This method is idempotent.
      *
      * @param listener The listener to remove.
      */
      LIBAPI virtual void removeHlaRRBDefaultInstanceListener(HlaRRBListenerPtr listener) = 0;

     /**
      * Add a valueListener that will automatically be added to all instances of HlaRRB (Both local and remote).
      * The  Valuelistener is notified when any attribute of an instance of HlaRRB is updated.
      * Note that the attribute values that are notified are not guaranteed to
      * be the latest available values for that attribute.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to add.
      */
      LIBAPI virtual void addHlaRRBDefaultInstanceValueListener(HlaRRBValueListenerPtr valueListener) = 0;

     /**
      * Remove a valueListener from the set of listeners that are automatically added to new instances of HlaRRB.
      * Note: The valueListener will not be removed from already existing instances of HlaRRB.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to remove.
      */
      LIBAPI virtual void removeHlaRRBDefaultInstanceValueListener(HlaRRBValueListenerPtr valueListener) = 0;

     /**
      * Enables or disables the HlaRRB manager.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaRRB manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      *
      * @param enabled <code>true</code> to enable the HlaRRB manager, otherwise <code>false</code>
      */
      LIBAPI virtual void setEnabled(bool enabled) = 0;

     /**
      * Enables the HlaRRB manager if it is avalable in the FOM at runtime.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaRRB manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      */
      LIBAPI virtual void setEnabledIfAvailableInFom() = 0;

     /**
      * Check if the HlaRRB manager is actually enabled when connected.
      * An HlaRRB manager may be disabled with the <code>setEnabled(false)</code> function, or it may be configured
      * as <i>if available in the FOM</i> by the <code>setEnabledIfAvailableInFom()</code> function and
      * then not available in the FOM at runtime.
      * <p>
      * This method should only be used when connected, it will always return <code>false</code>
      * when not connected.
      *
      * @return <code>true</code> if connected and the HlaRRB manager is enabled.
      */
      LIBAPI virtual bool isEnabled() = 0;
   };
}
#endif
