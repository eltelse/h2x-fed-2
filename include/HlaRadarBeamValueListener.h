/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLARADARBEAMVALUELISTENER_H
#define DEVELOPER_STUDIO_HLARADARBEAMVALUELISTENER_H

#include <memory>

#include <DevStudio/datatypes/BeamFunctionCodeEnum.h>
#include <DevStudio/datatypes/EventIdentifierStruct.h>
#include <DevStudio/datatypes/RTIobjectIdArray.h>
#include <string>
#include <vector>

#include <DevStudio/HlaLogicalTime.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaRadarBeamAttributes.h>    

namespace DevStudio {

   /**
   * Listener for updates of attributes, with the new updated values.  
   */
   class HlaRadarBeamValueListener {

   public:

      LIBAPI virtual ~HlaRadarBeamValueListener() {}
    
      /**
      * This method is called when the attribute <code>highDensityTrack</code> is updated.
      * <br>Description from the FOM: <i>When TRUE the receiving simulation can assume that all targets that are in the scan pattern of the radar beam are being tracked</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param radarBeam The object which is updated.
      * @param highDensityTrack The new value of the attribute in this update
      * @param validOldHighDensityTrack True if oldHighDensityTrack contains a valid value
      * @param oldHighDensityTrack The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void highDensityTrackUpdated(HlaRadarBeamPtr radarBeam, bool highDensityTrack, bool validOldHighDensityTrack, bool oldHighDensityTrack, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>trackObjectIdentifiers</code> is updated.
      * <br>Description from the FOM: <i>Identification of the entities being tracked.</i>
      * <br>Description of the data type from the FOM: <i>Set of ID's of registered object instances.</i>
      *
      * @param radarBeam The object which is updated.
      * @param trackObjectIdentifiers The new value of the attribute in this update
      * @param validOldTrackObjectIdentifiers True if oldTrackObjectIdentifiers contains a valid value
      * @param oldTrackObjectIdentifiers The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void trackObjectIdentifiersUpdated(HlaRadarBeamPtr radarBeam, std::vector<std::string > trackObjectIdentifiers, bool validOldTrackObjectIdentifiers, std::vector<std::string > oldTrackObjectIdentifiers, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>beamAzimuthCenter</code> is updated.
      * <br>Description from the FOM: <i>The azimuth center of the emitter beam's scan volume relative to the emitter system.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @param radarBeam The object which is updated.
      * @param beamAzimuthCenter The new value of the attribute in this update
      * @param validOldBeamAzimuthCenter True if oldBeamAzimuthCenter contains a valid value
      * @param oldBeamAzimuthCenter The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void beamAzimuthCenterUpdated(HlaRadarBeamPtr radarBeam, float beamAzimuthCenter, bool validOldBeamAzimuthCenter, float oldBeamAzimuthCenter, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>beamAzimuthSweep</code> is updated.
      * <br>Description from the FOM: <i>The azimuth half-angle of the emitter beam's scan volume relative to the emitter system.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @param radarBeam The object which is updated.
      * @param beamAzimuthSweep The new value of the attribute in this update
      * @param validOldBeamAzimuthSweep True if oldBeamAzimuthSweep contains a valid value
      * @param oldBeamAzimuthSweep The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void beamAzimuthSweepUpdated(HlaRadarBeamPtr radarBeam, float beamAzimuthSweep, bool validOldBeamAzimuthSweep, float oldBeamAzimuthSweep, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>beamElevationCenter</code> is updated.
      * <br>Description from the FOM: <i>The elevation center of the emitter beam's scan volume relative to the emitter system.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @param radarBeam The object which is updated.
      * @param beamElevationCenter The new value of the attribute in this update
      * @param validOldBeamElevationCenter True if oldBeamElevationCenter contains a valid value
      * @param oldBeamElevationCenter The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void beamElevationCenterUpdated(HlaRadarBeamPtr radarBeam, float beamElevationCenter, bool validOldBeamElevationCenter, float oldBeamElevationCenter, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>beamElevationSweep</code> is updated.
      * <br>Description from the FOM: <i>The elevation half-angle of the emitter beam's scan volume relative to the emitter system.</i>
      * <br>Description of the data type from the FOM: <i>Angle, based on SI derived unit radian, unit symbol rad. [unit: radian (rad), resolution: NA, accuracy: NA]</i>
      *
      * @param radarBeam The object which is updated.
      * @param beamElevationSweep The new value of the attribute in this update
      * @param validOldBeamElevationSweep True if oldBeamElevationSweep contains a valid value
      * @param oldBeamElevationSweep The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void beamElevationSweepUpdated(HlaRadarBeamPtr radarBeam, float beamElevationSweep, bool validOldBeamElevationSweep, float oldBeamElevationSweep, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>beamFunctionCode</code> is updated.
      * <br>Description from the FOM: <i>The function of the emitter beam.</i>
      * <br>Description of the data type from the FOM: <i>Beam function</i>
      *
      * @param radarBeam The object which is updated.
      * @param beamFunctionCode The new value of the attribute in this update
      * @param validOldBeamFunctionCode True if oldBeamFunctionCode contains a valid value
      * @param oldBeamFunctionCode The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void beamFunctionCodeUpdated(HlaRadarBeamPtr radarBeam, BeamFunctionCodeEnum::BeamFunctionCodeEnum beamFunctionCode, bool validOldBeamFunctionCode, BeamFunctionCodeEnum::BeamFunctionCodeEnum oldBeamFunctionCode, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>beamIdentifier</code> is updated.
      * <br>Description from the FOM: <i>The identification of the emitter beam (must be unique on the emitter system).</i>
      * <br>Description of the data type from the FOM: <i>Uninterpreted 8-bit value. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param radarBeam The object which is updated.
      * @param beamIdentifier The new value of the attribute in this update
      * @param validOldBeamIdentifier True if oldBeamIdentifier contains a valid value
      * @param oldBeamIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void beamIdentifierUpdated(HlaRadarBeamPtr radarBeam, char beamIdentifier, bool validOldBeamIdentifier, char oldBeamIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>beamParameterIndex</code> is updated.
      * <br>Description from the FOM: <i>The index, into the federation specific emissions database, of the current operating mode of the emitter beam.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param radarBeam The object which is updated.
      * @param beamParameterIndex The new value of the attribute in this update
      * @param validOldBeamParameterIndex True if oldBeamParameterIndex contains a valid value
      * @param oldBeamParameterIndex The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void beamParameterIndexUpdated(HlaRadarBeamPtr radarBeam, unsigned short beamParameterIndex, bool validOldBeamParameterIndex, unsigned short oldBeamParameterIndex, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>effectiveRadiatedPower</code> is updated.
      * <br>Description from the FOM: <i>The effective radiated power of the emitter beam.</i>
      * <br>Description of the data type from the FOM: <i>Power ratio in decibels (dB) of a measured power referenced to 1 milliwatt (mW). [unit: decibel milliwatt (dBm), resolution: NA, accuracy: perfect]</i>
      *
      * @param radarBeam The object which is updated.
      * @param effectiveRadiatedPower The new value of the attribute in this update
      * @param validOldEffectiveRadiatedPower True if oldEffectiveRadiatedPower contains a valid value
      * @param oldEffectiveRadiatedPower The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void effectiveRadiatedPowerUpdated(HlaRadarBeamPtr radarBeam, float effectiveRadiatedPower, bool validOldEffectiveRadiatedPower, float oldEffectiveRadiatedPower, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>emissionFrequency</code> is updated.
      * <br>Description from the FOM: <i>The center frequency of the emitter beam.</i>
      * <br>Description of the data type from the FOM: <i>Frequency, based on SI derived unit hertz, unit symbol Hz. [unit: hertz (Hz), resolution: NA, accuracy: NA]</i>
      *
      * @param radarBeam The object which is updated.
      * @param emissionFrequency The new value of the attribute in this update
      * @param validOldEmissionFrequency True if oldEmissionFrequency contains a valid value
      * @param oldEmissionFrequency The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void emissionFrequencyUpdated(HlaRadarBeamPtr radarBeam, float emissionFrequency, bool validOldEmissionFrequency, float oldEmissionFrequency, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>emitterSystemIdentifier</code> is updated.
      * <br>Description from the FOM: <i>The identification of the emitter system that is generating this emitter beam.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @param radarBeam The object which is updated.
      * @param emitterSystemIdentifier The new value of the attribute in this update
      * @param validOldEmitterSystemIdentifier True if oldEmitterSystemIdentifier contains a valid value
      * @param oldEmitterSystemIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void emitterSystemIdentifierUpdated(HlaRadarBeamPtr radarBeam, std::string emitterSystemIdentifier, bool validOldEmitterSystemIdentifier, std::string oldEmitterSystemIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>eventIdentifier</code> is updated.
      * <br>Description from the FOM: <i>The EventIdentifier is used by the generating federate to associate related events. The event number shall start at one at the beginning of the exercise, and be incremented by one for each event.</i>
      * <br>Description of the data type from the FOM: <i>Identification of an event. Based on the Event Identifier record as specified in IEEE 1278.1-1995 section 5.2.18.</i>
      *
      * @param radarBeam The object which is updated.
      * @param eventIdentifier The new value of the attribute in this update
      * @param validOldEventIdentifier True if oldEventIdentifier contains a valid value
      * @param oldEventIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void eventIdentifierUpdated(HlaRadarBeamPtr radarBeam, EventIdentifierStruct eventIdentifier, bool validOldEventIdentifier, EventIdentifierStruct oldEventIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>frequencyRange</code> is updated.
      * <br>Description from the FOM: <i>The bandwidth of the frequencies covered by the emitter beam.</i>
      * <br>Description of the data type from the FOM: <i>Frequency, based on SI derived unit hertz, unit symbol Hz. [unit: hertz (Hz), resolution: NA, accuracy: NA]</i>
      *
      * @param radarBeam The object which is updated.
      * @param frequencyRange The new value of the attribute in this update
      * @param validOldFrequencyRange True if oldFrequencyRange contains a valid value
      * @param oldFrequencyRange The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void frequencyRangeUpdated(HlaRadarBeamPtr radarBeam, float frequencyRange, bool validOldFrequencyRange, float oldFrequencyRange, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>pulseRepetitionFrequency</code> is updated.
      * <br>Description from the FOM: <i>The Pulse Repetition Frequency of the emitter beam.</i>
      * <br>Description of the data type from the FOM: <i>Frequency, based on SI derived unit hertz, unit symbol Hz. [unit: hertz (Hz), resolution: NA, accuracy: NA]</i>
      *
      * @param radarBeam The object which is updated.
      * @param pulseRepetitionFrequency The new value of the attribute in this update
      * @param validOldPulseRepetitionFrequency True if oldPulseRepetitionFrequency contains a valid value
      * @param oldPulseRepetitionFrequency The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void pulseRepetitionFrequencyUpdated(HlaRadarBeamPtr radarBeam, float pulseRepetitionFrequency, bool validOldPulseRepetitionFrequency, float oldPulseRepetitionFrequency, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>pulseWidth</code> is updated.
      * <br>Description from the FOM: <i>The pulse width of the emitter beam.</i>
      * <br>Description of the data type from the FOM: <i>Time, based on SI base unit second, expressed in microsecond, unit symbol μs. [unit: microsecond, resolution: NA, accuracy: NA]</i>
      *
      * @param radarBeam The object which is updated.
      * @param pulseWidth The new value of the attribute in this update
      * @param validOldPulseWidth True if oldPulseWidth contains a valid value
      * @param oldPulseWidth The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void pulseWidthUpdated(HlaRadarBeamPtr radarBeam, float pulseWidth, bool validOldPulseWidth, float oldPulseWidth, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>sweepSynch</code> is updated.
      * <br>Description from the FOM: <i>The percentage of time a scan is through its pattern from its origin.</i>
      * <br>Description of the data type from the FOM: <i>Percentage [unit: percent (%), resolution: NA, accuracy: NA]</i>
      *
      * @param radarBeam The object which is updated.
      * @param sweepSynch The new value of the attribute in this update
      * @param validOldSweepSynch True if oldSweepSynch contains a valid value
      * @param oldSweepSynch The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void sweepSynchUpdated(HlaRadarBeamPtr radarBeam, float sweepSynch, bool validOldSweepSynch, float oldSweepSynch, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
      /**
      * This method is called when the producing federate of an attribute is changed.
      * Only available when using HLA Evolved.
      *
      * @param radarBeam The object which is updated.
      * @param attribute The attribute that now has a new producing federate
      * @param oldProducingFederate The federate handle of the old producing federate
      * @param newProducingFederate The federate handle of the new producing federate
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void producingFederateUpdated(HlaRadarBeamPtr radarBeam, HlaRadarBeamAttributes::Attribute attribute, HlaFederateIdPtr oldProducingFederate, HlaFederateIdPtr newProducingFederate, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;

      class Adapter;
   };

   /**
   * An adapter class that implements the HlaRadarBeamValueListener interface with empty methods.
   * It might be used as a base class for a listener.
   */
   class HlaRadarBeamValueListener::Adapter : public HlaRadarBeamValueListener {

   public:

      LIBAPI virtual void highDensityTrackUpdated(HlaRadarBeamPtr radarBeam, bool highDensityTrack, bool validOldHighDensityTrack, bool oldHighDensityTrack, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void trackObjectIdentifiersUpdated(HlaRadarBeamPtr radarBeam, std::vector<std::string > trackObjectIdentifiers, bool validOldTrackObjectIdentifiers, std::vector<std::string > oldTrackObjectIdentifiers, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void beamAzimuthCenterUpdated(HlaRadarBeamPtr radarBeam, float beamAzimuthCenter, bool validOldBeamAzimuthCenter, float oldBeamAzimuthCenter, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void beamAzimuthSweepUpdated(HlaRadarBeamPtr radarBeam, float beamAzimuthSweep, bool validOldBeamAzimuthSweep, float oldBeamAzimuthSweep, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void beamElevationCenterUpdated(HlaRadarBeamPtr radarBeam, float beamElevationCenter, bool validOldBeamElevationCenter, float oldBeamElevationCenter, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void beamElevationSweepUpdated(HlaRadarBeamPtr radarBeam, float beamElevationSweep, bool validOldBeamElevationSweep, float oldBeamElevationSweep, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void beamFunctionCodeUpdated(HlaRadarBeamPtr radarBeam, BeamFunctionCodeEnum::BeamFunctionCodeEnum beamFunctionCode, bool validOldBeamFunctionCode, BeamFunctionCodeEnum::BeamFunctionCodeEnum oldBeamFunctionCode, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void beamIdentifierUpdated(HlaRadarBeamPtr radarBeam, char beamIdentifier, bool validOldBeamIdentifier, char oldBeamIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void beamParameterIndexUpdated(HlaRadarBeamPtr radarBeam, unsigned short beamParameterIndex, bool validOldBeamParameterIndex, unsigned short oldBeamParameterIndex, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void effectiveRadiatedPowerUpdated(HlaRadarBeamPtr radarBeam, float effectiveRadiatedPower, bool validOldEffectiveRadiatedPower, float oldEffectiveRadiatedPower, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void emissionFrequencyUpdated(HlaRadarBeamPtr radarBeam, float emissionFrequency, bool validOldEmissionFrequency, float oldEmissionFrequency, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void emitterSystemIdentifierUpdated(HlaRadarBeamPtr radarBeam, std::string emitterSystemIdentifier, bool validOldEmitterSystemIdentifier, std::string oldEmitterSystemIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void eventIdentifierUpdated(HlaRadarBeamPtr radarBeam, EventIdentifierStruct eventIdentifier, bool validOldEventIdentifier, EventIdentifierStruct oldEventIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void frequencyRangeUpdated(HlaRadarBeamPtr radarBeam, float frequencyRange, bool validOldFrequencyRange, float oldFrequencyRange, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void pulseRepetitionFrequencyUpdated(HlaRadarBeamPtr radarBeam, float pulseRepetitionFrequency, bool validOldPulseRepetitionFrequency, float oldPulseRepetitionFrequency, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void pulseWidthUpdated(HlaRadarBeamPtr radarBeam, float pulseWidth, bool validOldPulseWidth, float oldPulseWidth, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void sweepSynchUpdated(HlaRadarBeamPtr radarBeam, float sweepSynch, bool validOldSweepSynch, float oldSweepSynch, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
      LIBAPI virtual void producingFederateUpdated(HlaRadarBeamPtr radarBeam, HlaRadarBeamAttributes::Attribute attribute, HlaFederateIdPtr oldProducingFederate, HlaFederateIdPtr newProducingFederate, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
   };

}
#endif
