/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLATHREATSPOTOBJECTMANAGER_H
#define DEVELOPER_STUDIO_HLATHREATSPOTOBJECTMANAGER_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <DevStudio/datatypes/AcquisitionTypeEnum.h>
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EntityTypeStruct.h>
#include <DevStudio/datatypes/FortificationsEnum.h>
#include <DevStudio/datatypes/GeographicShapeStruct.h>
#include <DevStudio/datatypes/HostilityTypeEnum.h>
#include <DevStudio/datatypes/IsPartOfStruct.h>
#include <DevStudio/datatypes/PositionStruct.h>
#include <DevStudio/datatypes/SpatialVariantStruct.h>
#include <DevStudio/datatypes/SpotCategoryEnum.h>
#include <string>

#include <list>
#include <vector>

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaThreatSpotObjectManagerListener.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaException.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

   /**
   * Manager used to manage HlaThreatSpotObjects.
   */
   class HlaThreatSpotObjectManager : private boost::noncopyable {

   public:

      LIBAPI virtual ~HlaThreatSpotObjectManager() {}

      /**
      * Gets a list of all HlaThreatSpotObjects within interest, both local and remote.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of HlaThreatSpotObjects
      */
      LIBAPI virtual std::list<HlaThreatSpotObjectPtr> getHlaThreatSpotObjects() = 0;

      /**
      * Gets a list of all HlaThreatSpotObjects, both local and remote.
      * HlaThreatSpotObjects not within interest are also included.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of all HlaThreatSpotObjects
      */
      LIBAPI virtual std::list<HlaThreatSpotObjectPtr> getAllHlaThreatSpotObjects() = 0;

      /**
      * Gets a list of local HlaThreatSpotObjects within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of local HlaThreatSpotObjects
      */
      LIBAPI virtual std::list<HlaThreatSpotObjectPtr> getLocalHlaThreatSpotObjects() = 0;

      /**
      * Gets a list of remote HlaThreatSpotObjects within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of remote HlaThreatSpotObjects
      */
      LIBAPI virtual std::list<HlaThreatSpotObjectPtr> getRemoteHlaThreatSpotObjects() = 0;

      /**
      * Find a HlaThreatSpotObject with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> of the HlaThreatSpotObject to find
      *
      * @return the specified HlaThreatSpotObject, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaThreatSpotObjectPtr getThreatSpotObjectByHlaInstanceName(const std::wstring& hlaInstanceName) = 0;

      /**
      * Find a HlaThreatSpotObject with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param encodedHlaInstanceHandle the encoded <code>HLA instance handle</code> of the HlaThreatSpotObject to find
      *
      * @return the specified HlaThreatSpotObject, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaThreatSpotObjectPtr getThreatSpotObjectByHlaInstanceHandle(const std::vector<char>& encodedHlaInstanceHandle) = 0;

      /**
      * Creates a new HlaThreatSpotObject, and sets all <i>Create</i> attributes.
      *
      * @return Returns a new local HlaThreatSpotObject.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaThreatSpotObjectPtr createLocalHlaThreatSpotObject(
      ) THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Creates a new HlaThreatSpotObject with a specified <code>HLA instance name</code>, and sets all
      * <i>Create</i> attributes.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> to register for this instance
      *
      * @return Returns a new local HlaThreatSpotObject.
      *
      * @throws HlaIllegalInstanceNameException if the name is illegal
      * @throws HlaInstanceNameInUseException if the name is already in use
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaThreatSpotObjectPtr createLocalHlaThreatSpotObject(
         const std::wstring& hlaInstanceName
      ) THROW_SPEC (HlaIllegalInstanceNameException, HlaInstanceNameInUseException,
                    HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaThreatSpotObject and removes it from the federation.
      *
      * @param threatSpotObject The HlaThreatSpotObject to delete
      *
      * @return the HlaThreatSpotObject deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaThreatSpotObjectPtr deleteLocalHlaThreatSpotObject(HlaThreatSpotObjectPtr threatSpotObject)
          THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                      HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaThreatSpotObject and removes it from the federation.
      *
      * @param threatSpotObject The HlaThreatSpotObject to delete
      * @param timestamp timestamp when the instance was removed.
      *
      * @return the HlaThreatSpotObject deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaThreatSpotObjectPtr deleteLocalHlaThreatSpotObject(HlaThreatSpotObjectPtr threatSpotObject, HlaTimeStampPtr timestamp)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaThreatSpotObject and removes it from the federation.
      *
      * @param threatSpotObject The HlaThreatSpotObject to delete
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaThreatSpotObject deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaThreatSpotObjectPtr deleteLocalHlaThreatSpotObject(HlaThreatSpotObjectPtr threatSpotObject, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaThreatSpotObject and removes it from the federation.
      *
      * @param threatSpotObject The HlaThreatSpotObject to delete
      * @param timestamp timestamp when the instance was removed.
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaThreatSpotObject deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaThreatSpotObjectPtr deleteLocalHlaThreatSpotObject(HlaThreatSpotObjectPtr threatSpotObject, HlaTimeStampPtr timestamp, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Add a HlaThreatSpotObject manager listener.
      *
      * @param managerListener listener to add
      */
      LIBAPI virtual void addHlaThreatSpotObjectManagerListener(HlaThreatSpotObjectManagerListenerPtr managerListener) = 0;

      /**
      * Removed a HlaThreatSpotObject manager listener.
      *
      * @param managerListener listener to remove
      */
      LIBAPI virtual void removeHlaThreatSpotObjectManagerListener(HlaThreatSpotObjectManagerListenerPtr managerListener) = 0;

     /**
      * Add a listener that will automatically be added to all instances of HlaThreatSpotObject (Both local and remote).
      * The listener is notified when any attribute of an instance of HlaThreatSpotObject is updated.
      * The listener is also called when an interaction is sent to an instance of HlaThreatSpotObject.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param listener The listener to add.
      */
      LIBAPI virtual void addHlaThreatSpotObjectDefaultInstanceListener(HlaThreatSpotObjectListenerPtr listener) = 0;

     /**
      * Remove a listener from the set of listeners that are automatically added to new instances of HlaThreatSpotObject.
      * Note: The listener will not be removed from already existing instances of HlaThreatSpotObject.
      * This method is idempotent.
      *
      * @param listener The listener to remove.
      */
      LIBAPI virtual void removeHlaThreatSpotObjectDefaultInstanceListener(HlaThreatSpotObjectListenerPtr listener) = 0;

     /**
      * Add a valueListener that will automatically be added to all instances of HlaThreatSpotObject (Both local and remote).
      * The  Valuelistener is notified when any attribute of an instance of HlaThreatSpotObject is updated.
      * Note that the attribute values that are notified are not guaranteed to
      * be the latest available values for that attribute.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to add.
      */
      LIBAPI virtual void addHlaThreatSpotObjectDefaultInstanceValueListener(HlaThreatSpotObjectValueListenerPtr valueListener) = 0;

     /**
      * Remove a valueListener from the set of listeners that are automatically added to new instances of HlaThreatSpotObject.
      * Note: The valueListener will not be removed from already existing instances of HlaThreatSpotObject.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to remove.
      */
      LIBAPI virtual void removeHlaThreatSpotObjectDefaultInstanceValueListener(HlaThreatSpotObjectValueListenerPtr valueListener) = 0;

     /**
      * Enables or disables the HlaThreatSpotObject manager.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaThreatSpotObject manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      *
      * @param enabled <code>true</code> to enable the HlaThreatSpotObject manager, otherwise <code>false</code>
      */
      LIBAPI virtual void setEnabled(bool enabled) = 0;

     /**
      * Enables the HlaThreatSpotObject manager if it is avalable in the FOM at runtime.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaThreatSpotObject manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      */
      LIBAPI virtual void setEnabledIfAvailableInFom() = 0;

     /**
      * Check if the HlaThreatSpotObject manager is actually enabled when connected.
      * An HlaThreatSpotObject manager may be disabled with the <code>setEnabled(false)</code> function, or it may be configured
      * as <i>if available in the FOM</i> by the <code>setEnabledIfAvailableInFom()</code> function and
      * then not available in the FOM at runtime.
      * <p>
      * This method should only be used when connected, it will always return <code>false</code>
      * when not connected.
      *
      * @return <code>true</code> if connected and the HlaThreatSpotObject manager is enabled.
      */
      LIBAPI virtual bool isEnabled() = 0;
   };
}
#endif
