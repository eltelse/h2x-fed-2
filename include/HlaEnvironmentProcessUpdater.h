/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAENVIRONMENTPROCESSUPDATER_H
#define DEVELOPER_STUDIO_HLAENVIRONMENTPROCESSUPDATER_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <boost/noncopyable.hpp>

#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EnvironmentModelTypeEnum.h>
#include <DevStudio/datatypes/EnvironmentRecStruct.h>
#include <DevStudio/datatypes/EnvironmentRecStructArray.h>
#include <DevStudio/datatypes/EnvironmentTypeStruct.h>
#include <vector>

#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaException.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaHLAobjectRootUpdater.h>

namespace DevStudio {

    /**
    * Updater used to update attribute values.
    */
    class HlaEnvironmentProcessUpdater : public HlaHLAobjectRootUpdater {

    public:

    LIBAPI virtual ~HlaEnvironmentProcessUpdater() {}

    /**
    * Set the processIdentifier for this update.
    * <br>Description from the FOM: <i>Identifies which process issued the environmental process update</i>
    * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
    *
    * @param processIdentifier the new processIdentifier
    */
    LIBAPI virtual void setProcessIdentifier(const DevStudio::EntityIdentifierStruct& processIdentifier) = 0;

    /**
    * Set the type for this update.
    * <br>Description from the FOM: <i>Identifies the environmental process type</i>
    * <br>Description of the data type from the FOM: <i>Record specifying the kind of environment, the domain and any extra information necessary for describing the environmental entity</i>
    *
    * @param type the new type
    */
    LIBAPI virtual void setType(const DevStudio::EnvironmentTypeStruct& type) = 0;

    /**
    * Set the modelType for this update.
    * <br>Description from the FOM: <i>Identifies the model used for generating this environmental process update ; defined in 1278.1a as being exercise specific</i>
    * <br>Description of the data type from the FOM: <i>Environment process model type</i>
    *
    * @param modelType the new modelType
    */
    LIBAPI virtual void setModelType(const DevStudio::EnvironmentModelTypeEnum::EnvironmentModelTypeEnum& modelType) = 0;

    /**
    * Set the environmentProcessActive for this update.
    * <br>Description from the FOM: <i>Specifies the status of the environmental process (active or inactive)</i>
    * <br>Description of the data type from the FOM: <i></i>
    *
    * @param environmentProcessActive the new environmentProcessActive
    */
    LIBAPI virtual void setEnvironmentProcessActive(const bool& environmentProcessActive) = 0;

    /**
    * Set the sequenceNumber for this update.
    * <br>Description from the FOM: <i>Optional, used to support update sequencing ; if not used, set to EP_NO_SEQUENCE ; when used, set to zero for each exercise and incremented by one for each update sent</i>
    * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
    *
    * @param sequenceNumber the new sequenceNumber
    */
    LIBAPI virtual void setSequenceNumber(const unsigned short& sequenceNumber) = 0;

    /**
    * Set the environmentRecData for this update.
    * <br>Description from the FOM: <i>Specifies environment records</i>
    * <br>Description of the data type from the FOM: <i>Specifies environment records as a collection of geometry and state records</i>
    *
    * @param environmentRecData the new environmentRecData
    */
    LIBAPI virtual void setEnvironmentRecData(const std::vector<DevStudio::EnvironmentRecStruct >& environmentRecData) = 0;

    /**
    * Send all the attributes.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate()
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

    /**
    * Send all the attributes with a specified timestamp.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @param timestamp timestamp to send for this update
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate(HlaTimeStampPtr timestamp)
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

    /**
    * Send all the attributes with a specified timestamp.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @param logicalTime logical time to send for this update
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaInvalidLogicalTimeException if the logical time is not valid.
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate(HlaLogicalTimePtr logicalTime)
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaInvalidLogicalTimeException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

    /**
    * Send all the attributes with a specified timestamp.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @param timestamp timestamp to send for this update
    * @param logicalTime logical time to send for this update
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaInvalidLogicalTimeException if the logical time is not valid.
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate(HlaTimeStampPtr timestamp, HlaLogicalTimePtr logicalTime)
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaInvalidLogicalTimeException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;
    };
}
#endif
