/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAMINEFIELDLANEMARKEROBJECTMANAGER_H
#define DEVELOPER_STUDIO_HLAMINEFIELDLANEMARKEROBJECTMANAGER_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EnvironmentObjectTypeStruct.h>
#include <DevStudio/datatypes/ForceIdentifierEnum.h>
#include <DevStudio/datatypes/MinefieldLaneMarkerStruct.h>
#include <DevStudio/datatypes/MinefieldLaneMarkerStructLengthlessArray.h>
#include <string>
#include <vector>

#include <list>
#include <vector>

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaMinefieldLaneMarkerObjectManagerListener.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaException.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

   /**
   * Manager used to manage HlaMinefieldLaneMarkerObjects.
   */
   class HlaMinefieldLaneMarkerObjectManager : private boost::noncopyable {

   public:

      LIBAPI virtual ~HlaMinefieldLaneMarkerObjectManager() {}

      /**
      * Gets a list of all HlaMinefieldLaneMarkerObjects within interest, both local and remote.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of HlaMinefieldLaneMarkerObjects
      */
      LIBAPI virtual std::list<HlaMinefieldLaneMarkerObjectPtr> getHlaMinefieldLaneMarkerObjects() = 0;

      /**
      * Gets a list of all HlaMinefieldLaneMarkerObjects, both local and remote.
      * HlaMinefieldLaneMarkerObjects not within interest are also included.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of all HlaMinefieldLaneMarkerObjects
      */
      LIBAPI virtual std::list<HlaMinefieldLaneMarkerObjectPtr> getAllHlaMinefieldLaneMarkerObjects() = 0;

      /**
      * Gets a list of local HlaMinefieldLaneMarkerObjects within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of local HlaMinefieldLaneMarkerObjects
      */
      LIBAPI virtual std::list<HlaMinefieldLaneMarkerObjectPtr> getLocalHlaMinefieldLaneMarkerObjects() = 0;

      /**
      * Gets a list of remote HlaMinefieldLaneMarkerObjects within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of remote HlaMinefieldLaneMarkerObjects
      */
      LIBAPI virtual std::list<HlaMinefieldLaneMarkerObjectPtr> getRemoteHlaMinefieldLaneMarkerObjects() = 0;

      /**
      * Find a HlaMinefieldLaneMarkerObject with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> of the HlaMinefieldLaneMarkerObject to find
      *
      * @return the specified HlaMinefieldLaneMarkerObject, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaMinefieldLaneMarkerObjectPtr getMinefieldLaneMarkerObjectByHlaInstanceName(const std::wstring& hlaInstanceName) = 0;

      /**
      * Find a HlaMinefieldLaneMarkerObject with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param encodedHlaInstanceHandle the encoded <code>HLA instance handle</code> of the HlaMinefieldLaneMarkerObject to find
      *
      * @return the specified HlaMinefieldLaneMarkerObject, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaMinefieldLaneMarkerObjectPtr getMinefieldLaneMarkerObjectByHlaInstanceHandle(const std::vector<char>& encodedHlaInstanceHandle) = 0;

      /**
      * Creates a new HlaMinefieldLaneMarkerObject, and sets all <i>Create</i> attributes.
      *
      * @return Returns a new local HlaMinefieldLaneMarkerObject.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaMinefieldLaneMarkerObjectPtr createLocalHlaMinefieldLaneMarkerObject(
      ) THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Creates a new HlaMinefieldLaneMarkerObject with a specified <code>HLA instance name</code>, and sets all
      * <i>Create</i> attributes.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> to register for this instance
      *
      * @return Returns a new local HlaMinefieldLaneMarkerObject.
      *
      * @throws HlaIllegalInstanceNameException if the name is illegal
      * @throws HlaInstanceNameInUseException if the name is already in use
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaMinefieldLaneMarkerObjectPtr createLocalHlaMinefieldLaneMarkerObject(
         const std::wstring& hlaInstanceName
      ) THROW_SPEC (HlaIllegalInstanceNameException, HlaInstanceNameInUseException,
                    HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaMinefieldLaneMarkerObject and removes it from the federation.
      *
      * @param minefieldLaneMarkerObject The HlaMinefieldLaneMarkerObject to delete
      *
      * @return the HlaMinefieldLaneMarkerObject deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaMinefieldLaneMarkerObjectPtr deleteLocalHlaMinefieldLaneMarkerObject(HlaMinefieldLaneMarkerObjectPtr minefieldLaneMarkerObject)
          THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                      HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaMinefieldLaneMarkerObject and removes it from the federation.
      *
      * @param minefieldLaneMarkerObject The HlaMinefieldLaneMarkerObject to delete
      * @param timestamp timestamp when the instance was removed.
      *
      * @return the HlaMinefieldLaneMarkerObject deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaMinefieldLaneMarkerObjectPtr deleteLocalHlaMinefieldLaneMarkerObject(HlaMinefieldLaneMarkerObjectPtr minefieldLaneMarkerObject, HlaTimeStampPtr timestamp)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaMinefieldLaneMarkerObject and removes it from the federation.
      *
      * @param minefieldLaneMarkerObject The HlaMinefieldLaneMarkerObject to delete
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaMinefieldLaneMarkerObject deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaMinefieldLaneMarkerObjectPtr deleteLocalHlaMinefieldLaneMarkerObject(HlaMinefieldLaneMarkerObjectPtr minefieldLaneMarkerObject, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaMinefieldLaneMarkerObject and removes it from the federation.
      *
      * @param minefieldLaneMarkerObject The HlaMinefieldLaneMarkerObject to delete
      * @param timestamp timestamp when the instance was removed.
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaMinefieldLaneMarkerObject deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaMinefieldLaneMarkerObjectPtr deleteLocalHlaMinefieldLaneMarkerObject(HlaMinefieldLaneMarkerObjectPtr minefieldLaneMarkerObject, HlaTimeStampPtr timestamp, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Add a HlaMinefieldLaneMarkerObject manager listener.
      *
      * @param managerListener listener to add
      */
      LIBAPI virtual void addHlaMinefieldLaneMarkerObjectManagerListener(HlaMinefieldLaneMarkerObjectManagerListenerPtr managerListener) = 0;

      /**
      * Removed a HlaMinefieldLaneMarkerObject manager listener.
      *
      * @param managerListener listener to remove
      */
      LIBAPI virtual void removeHlaMinefieldLaneMarkerObjectManagerListener(HlaMinefieldLaneMarkerObjectManagerListenerPtr managerListener) = 0;

     /**
      * Add a listener that will automatically be added to all instances of HlaMinefieldLaneMarkerObject (Both local and remote).
      * The listener is notified when any attribute of an instance of HlaMinefieldLaneMarkerObject is updated.
      * The listener is also called when an interaction is sent to an instance of HlaMinefieldLaneMarkerObject.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param listener The listener to add.
      */
      LIBAPI virtual void addHlaMinefieldLaneMarkerObjectDefaultInstanceListener(HlaMinefieldLaneMarkerObjectListenerPtr listener) = 0;

     /**
      * Remove a listener from the set of listeners that are automatically added to new instances of HlaMinefieldLaneMarkerObject.
      * Note: The listener will not be removed from already existing instances of HlaMinefieldLaneMarkerObject.
      * This method is idempotent.
      *
      * @param listener The listener to remove.
      */
      LIBAPI virtual void removeHlaMinefieldLaneMarkerObjectDefaultInstanceListener(HlaMinefieldLaneMarkerObjectListenerPtr listener) = 0;

     /**
      * Add a valueListener that will automatically be added to all instances of HlaMinefieldLaneMarkerObject (Both local and remote).
      * The  Valuelistener is notified when any attribute of an instance of HlaMinefieldLaneMarkerObject is updated.
      * Note that the attribute values that are notified are not guaranteed to
      * be the latest available values for that attribute.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to add.
      */
      LIBAPI virtual void addHlaMinefieldLaneMarkerObjectDefaultInstanceValueListener(HlaMinefieldLaneMarkerObjectValueListenerPtr valueListener) = 0;

     /**
      * Remove a valueListener from the set of listeners that are automatically added to new instances of HlaMinefieldLaneMarkerObject.
      * Note: The valueListener will not be removed from already existing instances of HlaMinefieldLaneMarkerObject.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to remove.
      */
      LIBAPI virtual void removeHlaMinefieldLaneMarkerObjectDefaultInstanceValueListener(HlaMinefieldLaneMarkerObjectValueListenerPtr valueListener) = 0;

     /**
      * Enables or disables the HlaMinefieldLaneMarkerObject manager.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaMinefieldLaneMarkerObject manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      *
      * @param enabled <code>true</code> to enable the HlaMinefieldLaneMarkerObject manager, otherwise <code>false</code>
      */
      LIBAPI virtual void setEnabled(bool enabled) = 0;

     /**
      * Enables the HlaMinefieldLaneMarkerObject manager if it is avalable in the FOM at runtime.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaMinefieldLaneMarkerObject manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      */
      LIBAPI virtual void setEnabledIfAvailableInFom() = 0;

     /**
      * Check if the HlaMinefieldLaneMarkerObject manager is actually enabled when connected.
      * An HlaMinefieldLaneMarkerObject manager may be disabled with the <code>setEnabled(false)</code> function, or it may be configured
      * as <i>if available in the FOM</i> by the <code>setEnabledIfAvailableInFom()</code> function and
      * then not available in the FOM at runtime.
      * <p>
      * This method should only be used when connected, it will always return <code>false</code>
      * when not connected.
      *
      * @return <code>true</code> if connected and the HlaMinefieldLaneMarkerObject manager is enabled.
      */
      LIBAPI virtual bool isEnabled() = 0;
   };
}
#endif
