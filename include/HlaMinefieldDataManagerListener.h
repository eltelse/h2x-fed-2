/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAMINEFIELDDATAMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLAMINEFIELDDATAMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaMinefieldData.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaMinefieldData instances.
    */
    class HlaMinefieldDataManagerListener {

    public:

        LIBAPI virtual ~HlaMinefieldDataManagerListener() {}

        /**
        * This method is called when a new HlaMinefieldData instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param minefieldData the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaMinefieldDataDiscovered(HlaMinefieldDataPtr minefieldData, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaMinefieldData instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param minefieldData the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaMinefieldDataInitialized(HlaMinefieldDataPtr minefieldData, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaMinefieldDataManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param minefieldData the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaMinefieldDataInInterest(HlaMinefieldDataPtr minefieldData, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaMinefieldDataManagerListener instance goes out of interest.
        *
        * @param minefieldData the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaMinefieldDataOutOfInterest(HlaMinefieldDataPtr minefieldData, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaMinefieldData instance is deleted.
        *
        * @param minefieldData the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaMinefieldDataDeleted(HlaMinefieldDataPtr minefieldData, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaMinefieldDataManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaMinefieldDataManagerListener::Adapter : public HlaMinefieldDataManagerListener {

    public:
        LIBAPI virtual void hlaMinefieldDataDiscovered(HlaMinefieldDataPtr minefieldData, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaMinefieldDataInitialized(HlaMinefieldDataPtr minefieldData, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaMinefieldDataInInterest(HlaMinefieldDataPtr minefieldData, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaMinefieldDataOutOfInterest(HlaMinefieldDataPtr minefieldData, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaMinefieldDataDeleted(HlaMinefieldDataPtr minefieldData, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
