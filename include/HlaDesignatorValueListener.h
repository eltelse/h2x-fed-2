/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLADESIGNATORVALUELISTENER_H
#define DEVELOPER_STUDIO_HLADESIGNATORVALUELISTENER_H

#include <memory>

#include <DevStudio/datatypes/AccelerationVectorStruct.h>
#include <DevStudio/datatypes/DeadReckoningAlgorithmEnum.h>
#include <DevStudio/datatypes/DesignatorCodeEnum.h>
#include <DevStudio/datatypes/DesignatorCodeNameEnum.h>
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/RelativePositionStruct.h>
#include <DevStudio/datatypes/WorldLocationStruct.h>
#include <string>

#include <DevStudio/HlaLogicalTime.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaDesignatorAttributes.h>    

namespace DevStudio {

   /**
   * Listener for updates of attributes, with the new updated values.  
   */
   class HlaDesignatorValueListener {

   public:

      LIBAPI virtual ~HlaDesignatorValueListener() {}
    
      /**
      * This method is called when the attribute <code>codeName</code> is updated.
      * <br>Description from the FOM: <i>The code name of the designator system.</i>
      * <br>Description of the data type from the FOM: <i>Designator code name</i>
      *
      * @param designator The object which is updated.
      * @param codeName The new value of the attribute in this update
      * @param validOldCodeName True if oldCodeName contains a valid value
      * @param oldCodeName The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void codeNameUpdated(HlaDesignatorPtr designator, DesignatorCodeNameEnum::DesignatorCodeNameEnum codeName, bool validOldCodeName, DesignatorCodeNameEnum::DesignatorCodeNameEnum oldCodeName, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>designatedObjectIdentifier</code> is updated.
      * <br>Description from the FOM: <i>The object instance ID of the entity that is currently being designated (if any).</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @param designator The object which is updated.
      * @param designatedObjectIdentifier The new value of the attribute in this update
      * @param validOldDesignatedObjectIdentifier True if oldDesignatedObjectIdentifier contains a valid value
      * @param oldDesignatedObjectIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void designatedObjectIdentifierUpdated(HlaDesignatorPtr designator, std::string designatedObjectIdentifier, bool validOldDesignatedObjectIdentifier, std::string oldDesignatedObjectIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>designatorCode</code> is updated.
      * <br>Description from the FOM: <i>The designator code being used by the designating entity.</i>
      * <br>Description of the data type from the FOM: <i>Designator code</i>
      *
      * @param designator The object which is updated.
      * @param designatorCode The new value of the attribute in this update
      * @param validOldDesignatorCode True if oldDesignatorCode contains a valid value
      * @param oldDesignatorCode The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void designatorCodeUpdated(HlaDesignatorPtr designator, DesignatorCodeEnum::DesignatorCodeEnum designatorCode, bool validOldDesignatorCode, DesignatorCodeEnum::DesignatorCodeEnum oldDesignatorCode, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>designatorEmissionWavelength</code> is updated.
      * <br>Description from the FOM: <i>The wavelength of the designator system.</i>
      * <br>Description of the data type from the FOM: <i>Wavelength expressed in micrometer. [unit: micron, resolution: NA, accuracy: perfect]</i>
      *
      * @param designator The object which is updated.
      * @param designatorEmissionWavelength The new value of the attribute in this update
      * @param validOldDesignatorEmissionWavelength True if oldDesignatorEmissionWavelength contains a valid value
      * @param oldDesignatorEmissionWavelength The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void designatorEmissionWavelengthUpdated(HlaDesignatorPtr designator, float designatorEmissionWavelength, bool validOldDesignatorEmissionWavelength, float oldDesignatorEmissionWavelength, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>designatorOutputPower</code> is updated.
      * <br>Description from the FOM: <i>The output power of the designator system.</i>
      * <br>Description of the data type from the FOM: <i>The unit of power is the watt (W), which is equal to one joule per second. [unit: watt (W), resolution: NA, accuracy: perfect]</i>
      *
      * @param designator The object which is updated.
      * @param designatorOutputPower The new value of the attribute in this update
      * @param validOldDesignatorOutputPower True if oldDesignatorOutputPower contains a valid value
      * @param oldDesignatorOutputPower The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void designatorOutputPowerUpdated(HlaDesignatorPtr designator, float designatorOutputPower, bool validOldDesignatorOutputPower, float oldDesignatorOutputPower, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>designatorSpotLocation</code> is updated.
      * <br>Description from the FOM: <i>The location, in the world coordinate system, of the designator spot.</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @param designator The object which is updated.
      * @param designatorSpotLocation The new value of the attribute in this update
      * @param validOldDesignatorSpotLocation True if oldDesignatorSpotLocation contains a valid value
      * @param oldDesignatorSpotLocation The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void designatorSpotLocationUpdated(HlaDesignatorPtr designator, WorldLocationStruct designatorSpotLocation, bool validOldDesignatorSpotLocation, WorldLocationStruct oldDesignatorSpotLocation, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>deadReckoningAlgorithm</code> is updated.
      * <br>Description from the FOM: <i>Dead reckoning algorithm used by the issuing object.</i>
      * <br>Description of the data type from the FOM: <i>Dead-reckoning algorithm</i>
      *
      * @param designator The object which is updated.
      * @param deadReckoningAlgorithm The new value of the attribute in this update
      * @param validOldDeadReckoningAlgorithm True if oldDeadReckoningAlgorithm contains a valid value
      * @param oldDeadReckoningAlgorithm The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void deadReckoningAlgorithmUpdated(HlaDesignatorPtr designator, DeadReckoningAlgorithmEnum::DeadReckoningAlgorithmEnum deadReckoningAlgorithm, bool validOldDeadReckoningAlgorithm, DeadReckoningAlgorithmEnum::DeadReckoningAlgorithmEnum oldDeadReckoningAlgorithm, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>relativeSpotLocation</code> is updated.
      * <br>Description from the FOM: <i>The location of the designator spot, relative to the object being designated (if any).</i>
      * <br>Description of the data type from the FOM: <i>Relative position in right-handed Cartesian coordinates.</i>
      *
      * @param designator The object which is updated.
      * @param relativeSpotLocation The new value of the attribute in this update
      * @param validOldRelativeSpotLocation True if oldRelativeSpotLocation contains a valid value
      * @param oldRelativeSpotLocation The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void relativeSpotLocationUpdated(HlaDesignatorPtr designator, RelativePositionStruct relativeSpotLocation, bool validOldRelativeSpotLocation, RelativePositionStruct oldRelativeSpotLocation, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>spotLinearAccelerationVector</code> is updated.
      * <br>Description from the FOM: <i>The rate of change in linear velocity of the designator spot over time.</i>
      * <br>Description of the data type from the FOM: <i>The magnitude of the change in linear velocity over time.</i>
      *
      * @param designator The object which is updated.
      * @param spotLinearAccelerationVector The new value of the attribute in this update
      * @param validOldSpotLinearAccelerationVector True if oldSpotLinearAccelerationVector contains a valid value
      * @param oldSpotLinearAccelerationVector The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void spotLinearAccelerationVectorUpdated(HlaDesignatorPtr designator, AccelerationVectorStruct spotLinearAccelerationVector, bool validOldSpotLinearAccelerationVector, AccelerationVectorStruct oldSpotLinearAccelerationVector, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>entityIdentifier</code> is updated.
      * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @param designator The object which is updated.
      * @param entityIdentifier The new value of the attribute in this update
      * @param validOldEntityIdentifier True if oldEntityIdentifier contains a valid value
      * @param oldEntityIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void entityIdentifierUpdated(HlaDesignatorPtr designator, EntityIdentifierStruct entityIdentifier, bool validOldEntityIdentifier, EntityIdentifierStruct oldEntityIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>hostObjectIdentifier</code> is updated.
      * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @param designator The object which is updated.
      * @param hostObjectIdentifier The new value of the attribute in this update
      * @param validOldHostObjectIdentifier True if oldHostObjectIdentifier contains a valid value
      * @param oldHostObjectIdentifier The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void hostObjectIdentifierUpdated(HlaDesignatorPtr designator, std::string hostObjectIdentifier, bool validOldHostObjectIdentifier, std::string oldHostObjectIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
    
      /**
      * This method is called when the attribute <code>relativePosition</code> is updated.
      * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
      * <br>Description of the data type from the FOM: <i>Relative position in right-handed Cartesian coordinates.</i>
      *
      * @param designator The object which is updated.
      * @param relativePosition The new value of the attribute in this update
      * @param validOldRelativePosition True if oldRelativePosition contains a valid value
      * @param oldRelativePosition The old value
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void relativePositionUpdated(HlaDesignatorPtr designator, RelativePositionStruct relativePosition, bool validOldRelativePosition, RelativePositionStruct oldRelativePosition, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;
      /**
      * This method is called when the producing federate of an attribute is changed.
      * Only available when using HLA Evolved.
      *
      * @param designator The object which is updated.
      * @param attribute The attribute that now has a new producing federate
      * @param oldProducingFederate The federate handle of the old producing federate
      * @param newProducingFederate The federate handle of the new producing federate
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time when the update was initiated.
      */
      LIBAPI virtual void producingFederateUpdated(HlaDesignatorPtr designator, HlaDesignatorAttributes::Attribute attribute, HlaFederateIdPtr oldProducingFederate, HlaFederateIdPtr newProducingFederate, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime)=0;

      class Adapter;
   };

   /**
   * An adapter class that implements the HlaDesignatorValueListener interface with empty methods.
   * It might be used as a base class for a listener.
   */
   class HlaDesignatorValueListener::Adapter : public HlaDesignatorValueListener {

   public:

      LIBAPI virtual void codeNameUpdated(HlaDesignatorPtr designator, DesignatorCodeNameEnum::DesignatorCodeNameEnum codeName, bool validOldCodeName, DesignatorCodeNameEnum::DesignatorCodeNameEnum oldCodeName, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void designatedObjectIdentifierUpdated(HlaDesignatorPtr designator, std::string designatedObjectIdentifier, bool validOldDesignatedObjectIdentifier, std::string oldDesignatedObjectIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void designatorCodeUpdated(HlaDesignatorPtr designator, DesignatorCodeEnum::DesignatorCodeEnum designatorCode, bool validOldDesignatorCode, DesignatorCodeEnum::DesignatorCodeEnum oldDesignatorCode, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void designatorEmissionWavelengthUpdated(HlaDesignatorPtr designator, float designatorEmissionWavelength, bool validOldDesignatorEmissionWavelength, float oldDesignatorEmissionWavelength, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void designatorOutputPowerUpdated(HlaDesignatorPtr designator, float designatorOutputPower, bool validOldDesignatorOutputPower, float oldDesignatorOutputPower, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void designatorSpotLocationUpdated(HlaDesignatorPtr designator, WorldLocationStruct designatorSpotLocation, bool validOldDesignatorSpotLocation, WorldLocationStruct oldDesignatorSpotLocation, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void deadReckoningAlgorithmUpdated(HlaDesignatorPtr designator, DeadReckoningAlgorithmEnum::DeadReckoningAlgorithmEnum deadReckoningAlgorithm, bool validOldDeadReckoningAlgorithm, DeadReckoningAlgorithmEnum::DeadReckoningAlgorithmEnum oldDeadReckoningAlgorithm, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void relativeSpotLocationUpdated(HlaDesignatorPtr designator, RelativePositionStruct relativeSpotLocation, bool validOldRelativeSpotLocation, RelativePositionStruct oldRelativeSpotLocation, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void spotLinearAccelerationVectorUpdated(HlaDesignatorPtr designator, AccelerationVectorStruct spotLinearAccelerationVector, bool validOldSpotLinearAccelerationVector, AccelerationVectorStruct oldSpotLinearAccelerationVector, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void entityIdentifierUpdated(HlaDesignatorPtr designator, EntityIdentifierStruct entityIdentifier, bool validOldEntityIdentifier, EntityIdentifierStruct oldEntityIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void hostObjectIdentifierUpdated(HlaDesignatorPtr designator, std::string hostObjectIdentifier, bool validOldHostObjectIdentifier, std::string oldHostObjectIdentifier, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

      LIBAPI virtual void relativePositionUpdated(HlaDesignatorPtr designator, RelativePositionStruct relativePosition, bool validOldRelativePosition, RelativePositionStruct oldRelativePosition, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
      LIBAPI virtual void producingFederateUpdated(HlaDesignatorPtr designator, HlaDesignatorAttributes::Attribute attribute, HlaFederateIdPtr oldProducingFederate, HlaFederateIdPtr newProducingFederate, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
   };

}
#endif
