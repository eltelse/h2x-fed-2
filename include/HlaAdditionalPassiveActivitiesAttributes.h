/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAADDITIONALPASSIVEACTIVITIESATTRIBUTES_H
#define DEVELOPER_STUDIO_HLAADDITIONALPASSIVEACTIVITIESATTRIBUTES_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <string>
#include <vector>
#include <utility>
    
#include <boost/noncopyable.hpp>
    
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EventIdentifierStruct.h>
#include <DevStudio/datatypes/RelativePositionStruct.h>
#include <string>

#include <DevStudio/HlaException.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaUnderwaterAcousticsEmissionAttributes.h>
#include <DevStudio/HlaTimeStamped.h>

namespace DevStudio {

   /**
   * Interface used to represent all attributes for an object instance.
   */
   class HlaAdditionalPassiveActivitiesAttributes : public HlaUnderwaterAcousticsEmissionAttributes {
   public:


     /**
      * An enumeration of the attributes of an HlaAdditionalPassiveActivities
      *
      *<table summary="All attributes">
      * <tr><td><b>Enum constant</b></td><td><b>C++ name</b></td><td><b>FOM name</b></td></tr>
      * <tr><td>ACTIVITY_CODE</td><td>activityCode</td><td><code>ActivityCode</code></td></tr>
      * <tr><td>ACTIVITY_PARAMETER</td><td>activityParameter</td><td><code>ActivityParameter</code></td></tr>
      * <tr><td>IS_SILENT</td><td>isSilent</td><td><code>IsSilent</code></td></tr>
      * <tr><td>EVENT_IDENTIFIER</td><td>eventIdentifier</td><td><code>EventIdentifier</code></td></tr>
      * <tr><td>ENTITY_IDENTIFIER</td><td>entityIdentifier</td><td><code>EntityIdentifier</code></td></tr>
      * <tr><td>HOST_OBJECT_IDENTIFIER</td><td>hostObjectIdentifier</td><td><code>HostObjectIdentifier</code></td></tr>
      * <tr><td>RELATIVE_POSITION</td><td>relativePosition</td><td><code>RelativePosition</code></td></tr>
      *</table>
      */
      enum Attribute {
        /**
        * activityCode (FOM name: <code>ActivityCode</code>).
        * <br>Description from the FOM: <i>Database index used to indicate the passive signature of the entity</i>
        */
         ACTIVITY_CODE,

        /**
        * activityParameter (FOM name: <code>ActivityParameter</code>).
        * <br>Description from the FOM: <i>The current state of this activity.</i>
        */
         ACTIVITY_PARAMETER,

        /**
        * isSilent (FOM name: <code>IsSilent</code>).
        * <br>Description from the FOM: <i>Used to silence an additional passive activity without destroying this object.</i>
        */
         IS_SILENT,

        /**
        * eventIdentifier (FOM name: <code>EventIdentifier</code>).
        * <br>Description from the FOM: <i>The generating federate uses the Event Identifier to associate related events. The event number begins at one at the beginning of the exercise and is incremented by one for each event.</i>
        */
         EVENT_IDENTIFIER,

        /**
        * entityIdentifier (FOM name: <code>EntityIdentifier</code>).
        * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
        */
         ENTITY_IDENTIFIER,

        /**
        * hostObjectIdentifier (FOM name: <code>HostObjectIdentifier</code>).
        * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
        */
         HOST_OBJECT_IDENTIFIER,

        /**
        * relativePosition (FOM name: <code>RelativePosition</code>).
        * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
        */
         RELATIVE_POSITION
      };

     /**
      * Gets the name of the attribute.
      *
      * @return The name of the attribute. An empty string will be returned if the attribute does not exist.
      */
      LIBAPI virtual std::wstring getName(Attribute attribute);

     /**
      * Finds the attribute specified in the parameter attributeName.
      * The found enumeration will be stored in the parameter attribute.
      *
      * @return true if the attribute was found, false otherwise.
      */
      LIBAPI virtual bool find(Attribute& attribute, std::wstring attributeName);

      LIBAPI virtual ~HlaAdditionalPassiveActivitiesAttributes() {}
    
      /**
      * Returns true if the <code>activityCode</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Database index used to indicate the passive signature of the entity</i>
      *
      * @return true if <code>activityCode</code> is available.
      */
      LIBAPI virtual bool hasActivityCode() = 0;

      /**
      * Gets the value of the <code>activityCode</code> attribute.
      *
      * <br>Description from the FOM: <i>Database index used to indicate the passive signature of the entity</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the <code>activityCode</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual unsigned short getActivityCode()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>activityCode</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Database index used to indicate the passive signature of the entity</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>activityCode</code> attribute.
      */
      LIBAPI virtual unsigned short getActivityCode(unsigned short defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>activityCode</code> attribute.
      * <br>Description from the FOM: <i>Database index used to indicate the passive signature of the entity</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the time stamped <code>activityCode</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< unsigned short > getActivityCodeTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>activityParameter</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The current state of this activity.</i>
      *
      * @return true if <code>activityParameter</code> is available.
      */
      LIBAPI virtual bool hasActivityParameter() = 0;

      /**
      * Gets the value of the <code>activityParameter</code> attribute.
      *
      * <br>Description from the FOM: <i>The current state of this activity.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^15, 2^15-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the <code>activityParameter</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual short getActivityParameter()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>activityParameter</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The current state of this activity.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^15, 2^15-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>activityParameter</code> attribute.
      */
      LIBAPI virtual short getActivityParameter(short defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>activityParameter</code> attribute.
      * <br>Description from the FOM: <i>The current state of this activity.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^15, 2^15-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the time stamped <code>activityParameter</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< short > getActivityParameterTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>isSilent</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Used to silence an additional passive activity without destroying this object.</i>
      *
      * @return true if <code>isSilent</code> is available.
      */
      LIBAPI virtual bool hasIsSilent() = 0;

      /**
      * Gets the value of the <code>isSilent</code> attribute.
      *
      * <br>Description from the FOM: <i>Used to silence an additional passive activity without destroying this object.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>isSilent</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual bool getIsSilent()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>isSilent</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Used to silence an additional passive activity without destroying this object.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>isSilent</code> attribute.
      */
      LIBAPI virtual bool getIsSilent(bool defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>isSilent</code> attribute.
      * <br>Description from the FOM: <i>Used to silence an additional passive activity without destroying this object.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>isSilent</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< bool > getIsSilentTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>eventIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The generating federate uses the Event Identifier to associate related events. The event number begins at one at the beginning of the exercise and is incremented by one for each event.</i>
      *
      * @return true if <code>eventIdentifier</code> is available.
      */
      LIBAPI virtual bool hasEventIdentifier() = 0;

      /**
      * Gets the value of the <code>eventIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>The generating federate uses the Event Identifier to associate related events. The event number begins at one at the beginning of the exercise and is incremented by one for each event.</i>
      * <br>Description of the data type from the FOM: <i>Identification of an event. Based on the Event Identifier record as specified in IEEE 1278.1-1995 section 5.2.18.</i>
      *
      * @return the <code>eventIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::EventIdentifierStruct getEventIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>eventIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The generating federate uses the Event Identifier to associate related events. The event number begins at one at the beginning of the exercise and is incremented by one for each event.</i>
      * <br>Description of the data type from the FOM: <i>Identification of an event. Based on the Event Identifier record as specified in IEEE 1278.1-1995 section 5.2.18.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>eventIdentifier</code> attribute.
      */
      LIBAPI virtual DevStudio::EventIdentifierStruct getEventIdentifier(DevStudio::EventIdentifierStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>eventIdentifier</code> attribute.
      * <br>Description from the FOM: <i>The generating federate uses the Event Identifier to associate related events. The event number begins at one at the beginning of the exercise and is incremented by one for each event.</i>
      * <br>Description of the data type from the FOM: <i>Identification of an event. Based on the Event Identifier record as specified in IEEE 1278.1-1995 section 5.2.18.</i>
      *
      * @return the time stamped <code>eventIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::EventIdentifierStruct > getEventIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>entityIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
      *
      * @return true if <code>entityIdentifier</code> is available.
      */
      LIBAPI virtual bool hasEntityIdentifier() = 0;

      /**
      * Gets the value of the <code>entityIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @return the <code>entityIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::EntityIdentifierStruct getEntityIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>entityIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>entityIdentifier</code> attribute.
      */
      LIBAPI virtual DevStudio::EntityIdentifierStruct getEntityIdentifier(DevStudio::EntityIdentifierStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>entityIdentifier</code> attribute.
      * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @return the time stamped <code>entityIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::EntityIdentifierStruct > getEntityIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>hostObjectIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
      *
      * @return true if <code>hostObjectIdentifier</code> is available.
      */
      LIBAPI virtual bool hasHostObjectIdentifier() = 0;

      /**
      * Gets the value of the <code>hostObjectIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @return the <code>hostObjectIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::string getHostObjectIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>hostObjectIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>hostObjectIdentifier</code> attribute.
      */
      LIBAPI virtual std::string getHostObjectIdentifier(std::string defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>hostObjectIdentifier</code> attribute.
      * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @return the time stamped <code>hostObjectIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::string > getHostObjectIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>relativePosition</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
      *
      * @return true if <code>relativePosition</code> is available.
      */
      LIBAPI virtual bool hasRelativePosition() = 0;

      /**
      * Gets the value of the <code>relativePosition</code> attribute.
      *
      * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
      * <br>Description of the data type from the FOM: <i>Relative position in right-handed Cartesian coordinates.</i>
      *
      * @return the <code>relativePosition</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::RelativePositionStruct getRelativePosition()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>relativePosition</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
      * <br>Description of the data type from the FOM: <i>Relative position in right-handed Cartesian coordinates.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>relativePosition</code> attribute.
      */
      LIBAPI virtual DevStudio::RelativePositionStruct getRelativePosition(DevStudio::RelativePositionStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>relativePosition</code> attribute.
      * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
      * <br>Description of the data type from the FOM: <i>Relative position in right-handed Cartesian coordinates.</i>
      *
      * @return the time stamped <code>relativePosition</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::RelativePositionStruct > getRelativePositionTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
   };
}
#endif
