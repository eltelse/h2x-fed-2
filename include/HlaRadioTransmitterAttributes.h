/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLARADIOTRANSMITTERATTRIBUTES_H
#define DEVELOPER_STUDIO_HLARADIOTRANSMITTERATTRIBUTES_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <string>
#include <vector>
#include <utility>
    
#include <boost/noncopyable.hpp>
    
#include <DevStudio/datatypes/AntennaPatternVariantStruct.h>
#include <DevStudio/datatypes/AntennaPatternVariantStructLengthlessArray.h>
#include <DevStudio/datatypes/CryptographicModeEnum.h>
#include <DevStudio/datatypes/CryptographicSystemTypeEnum.h>
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/RFModulationSystemTypeEnum.h>
#include <DevStudio/datatypes/RFModulationTypeVariantStruct.h>
#include <DevStudio/datatypes/RadioInputSourceEnum.h>
#include <DevStudio/datatypes/RadioTypeStruct.h>
#include <DevStudio/datatypes/RelativePositionStruct.h>
#include <DevStudio/datatypes/SpreadSpectrumVariantStruct.h>
#include <DevStudio/datatypes/TransmitterOperationalStatusEnum.h>
#include <DevStudio/datatypes/WorldLocationStruct.h>
#include <string>
#include <vector>

#include <DevStudio/HlaException.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaEmbeddedSystemAttributes.h>
#include <DevStudio/HlaTimeStamped.h>

namespace DevStudio {

   /**
   * Interface used to represent all attributes for an object instance.
   */
   class HlaRadioTransmitterAttributes : public HlaEmbeddedSystemAttributes {
   public:


     /**
      * An enumeration of the attributes of an HlaRadioTransmitter
      *
      *<table summary="All attributes">
      * <tr><td><b>Enum constant</b></td><td><b>C++ name</b></td><td><b>FOM name</b></td></tr>
      * <tr><td>ANTENNA_PATTERN_DATA</td><td>antennaPatternData</td><td><code>AntennaPatternData</code></td></tr>
      * <tr><td>CRYPTOGRAPHIC_MODE</td><td>cryptographicMode</td><td><code>CryptographicMode</code></td></tr>
      * <tr><td>CRYPTO_SYSTEM</td><td>cryptoSystem</td><td><code>CryptoSystem</code></td></tr>
      * <tr><td>ENCRYPTION_KEY_IDENTIFIER</td><td>encryptionKeyIdentifier</td><td><code>EncryptionKeyIdentifier</code></td></tr>
      * <tr><td>FREQUENCY</td><td>frequency</td><td><code>Frequency</code></td></tr>
      * <tr><td>FREQUENCY_BANDWIDTH</td><td>frequencyBandwidth</td><td><code>FrequencyBandwidth</code></td></tr>
      * <tr><td>RADIO_INDEX</td><td>radioIndex</td><td><code>RadioIndex</code></td></tr>
      * <tr><td>RADIO_INPUT_SOURCE</td><td>radioInputSource</td><td><code>RadioInputSource</code></td></tr>
      * <tr><td>RADIO_SYSTEM_TYPE</td><td>radioSystemType</td><td><code>RadioSystemType</code></td></tr>
      * <tr><td>R_FMODULATION_SYSTEM_TYPE</td><td>rFModulationSystemType</td><td><code>RFModulationSystemType</code></td></tr>
      * <tr><td>R_FMODULATION_TYPE</td><td>rFModulationType</td><td><code>RFModulationType</code></td></tr>
      * <tr><td>SPREAD_SPECTRUM</td><td>spreadSpectrum</td><td><code>SpreadSpectrum</code></td></tr>
      * <tr><td>STREAM_TAG</td><td>streamTag</td><td><code>StreamTag</code></td></tr>
      * <tr><td>TIME_HOP_IN_USE</td><td>timeHopInUse</td><td><code>TimeHopInUse</code></td></tr>
      * <tr><td>TRANSMITTED_POWER</td><td>transmittedPower</td><td><code>TransmittedPower</code></td></tr>
      * <tr><td>TRANSMITTER_OPERATIONAL_STATUS</td><td>transmitterOperationalStatus</td><td><code>TransmitterOperationalStatus</code></td></tr>
      * <tr><td>WORLD_LOCATION</td><td>worldLocation</td><td><code>WorldLocation</code></td></tr>
      * <tr><td>ENTITY_IDENTIFIER</td><td>entityIdentifier</td><td><code>EntityIdentifier</code></td></tr>
      * <tr><td>HOST_OBJECT_IDENTIFIER</td><td>hostObjectIdentifier</td><td><code>HostObjectIdentifier</code></td></tr>
      * <tr><td>RELATIVE_POSITION</td><td>relativePosition</td><td><code>RelativePosition</code></td></tr>
      *</table>
      */
      enum Attribute {
        /**
        * antennaPatternData (FOM name: <code>AntennaPatternData</code>).
        * <br>Description from the FOM: <i>The radiation pattern of the radio's antenna.</i>
        */
         ANTENNA_PATTERN_DATA,

        /**
        * cryptographicMode (FOM name: <code>CryptographicMode</code>).
        * <br>Description from the FOM: <i>The mode of the cryptographic system.</i>
        */
         CRYPTOGRAPHIC_MODE,

        /**
        * cryptoSystem (FOM name: <code>CryptoSystem</code>).
        * <br>Description from the FOM: <i>The type of cryptographic equipment in use.</i>
        */
         CRYPTO_SYSTEM,

        /**
        * encryptionKeyIdentifier (FOM name: <code>EncryptionKeyIdentifier</code>).
        * <br>Description from the FOM: <i>The identification of the key used to encrypt the radio signals being transmitted. The transmitter and receiver should be considered to be using the same key if these numbers match.</i>
        */
         ENCRYPTION_KEY_IDENTIFIER,

        /**
        * frequency (FOM name: <code>Frequency</code>).
        * <br>Description from the FOM: <i>The center frequency of the radio transmissions.</i>
        */
         FREQUENCY,

        /**
        * frequencyBandwidth (FOM name: <code>FrequencyBandwidth</code>).
        * <br>Description from the FOM: <i>The bandpass of the radio transmissions.</i>
        */
         FREQUENCY_BANDWIDTH,

        /**
        * radioIndex (FOM name: <code>RadioIndex</code>).
        * <br>Description from the FOM: <i>A number that uniquely identifies this radio from others on the host.</i>
        */
         RADIO_INDEX,

        /**
        * radioInputSource (FOM name: <code>RadioInputSource</code>).
        * <br>Description from the FOM: <i>Specifies which position or data port provided the input for the transmission.</i>
        */
         RADIO_INPUT_SOURCE,

        /**
        * radioSystemType (FOM name: <code>RadioSystemType</code>).
        * <br>Description from the FOM: <i>The type of radio transmitter.</i>
        */
         RADIO_SYSTEM_TYPE,

        /**
        * rFModulationSystemType (FOM name: <code>RFModulationSystemType</code>).
        * <br>Description from the FOM: <i>The radio system type associated with this transmitter.</i>
        */
         R_FMODULATION_SYSTEM_TYPE,

        /**
        * rFModulationType (FOM name: <code>RFModulationType</code>).
        * <br>Description from the FOM: <i>Classification of the modulation type.</i>
        */
         R_FMODULATION_TYPE,

        /**
        * spreadSpectrum (FOM name: <code>SpreadSpectrum</code>).
        * <br>Description from the FOM: <i>Describes the spread spectrum characteristics of the transmission, such as frequency hopping or other spread spectrum transmission modes.</i>
        */
         SPREAD_SPECTRUM,

        /**
        * streamTag (FOM name: <code>StreamTag</code>).
        * <br>Description from the FOM: <i>A globally unique identifier for the associated audio stream</i>
        */
         STREAM_TAG,

        /**
        * timeHopInUse (FOM name: <code>TimeHopInUse</code>).
        * <br>Description from the FOM: <i>Whether the radio is using time hopping or not.</i>
        */
         TIME_HOP_IN_USE,

        /**
        * transmittedPower (FOM name: <code>TransmittedPower</code>).
        * <br>Description from the FOM: <i>The average transmitted power.</i>
        */
         TRANSMITTED_POWER,

        /**
        * transmitterOperationalStatus (FOM name: <code>TransmitterOperationalStatus</code>).
        * <br>Description from the FOM: <i>The current operational state of the radio transmitter.</i>
        */
         TRANSMITTER_OPERATIONAL_STATUS,

        /**
        * worldLocation (FOM name: <code>WorldLocation</code>).
        * <br>Description from the FOM: <i>The location of the antenna in the world coordinate system.</i>
        */
         WORLD_LOCATION,

        /**
        * entityIdentifier (FOM name: <code>EntityIdentifier</code>).
        * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
        */
         ENTITY_IDENTIFIER,

        /**
        * hostObjectIdentifier (FOM name: <code>HostObjectIdentifier</code>).
        * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
        */
         HOST_OBJECT_IDENTIFIER,

        /**
        * relativePosition (FOM name: <code>RelativePosition</code>).
        * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
        */
         RELATIVE_POSITION
      };

     /**
      * Gets the name of the attribute.
      *
      * @return The name of the attribute. An empty string will be returned if the attribute does not exist.
      */
      LIBAPI virtual std::wstring getName(Attribute attribute);

     /**
      * Finds the attribute specified in the parameter attributeName.
      * The found enumeration will be stored in the parameter attribute.
      *
      * @return true if the attribute was found, false otherwise.
      */
      LIBAPI virtual bool find(Attribute& attribute, std::wstring attributeName);

      LIBAPI virtual ~HlaRadioTransmitterAttributes() {}
    
      /**
      * Returns true if the <code>antennaPatternData</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The radiation pattern of the radio's antenna.</i>
      *
      * @return true if <code>antennaPatternData</code> is available.
      */
      LIBAPI virtual bool hasAntennaPatternData() = 0;

      /**
      * Gets the value of the <code>antennaPatternData</code> attribute.
      *
      * <br>Description from the FOM: <i>The radiation pattern of the radio's antenna.</i>
      * <br>Description of the data type from the FOM: <i>Represents an antenna's radiation pattern, its orientation in space, and the polarization of the radiation.</i>
      *
      * @return the <code>antennaPatternData</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::vector<DevStudio::AntennaPatternVariantStruct > getAntennaPatternData()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>antennaPatternData</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The radiation pattern of the radio's antenna.</i>
      * <br>Description of the data type from the FOM: <i>Represents an antenna's radiation pattern, its orientation in space, and the polarization of the radiation.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>antennaPatternData</code> attribute.
      */
      LIBAPI virtual std::vector<DevStudio::AntennaPatternVariantStruct > getAntennaPatternData(std::vector<DevStudio::AntennaPatternVariantStruct > defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>antennaPatternData</code> attribute.
      * <br>Description from the FOM: <i>The radiation pattern of the radio's antenna.</i>
      * <br>Description of the data type from the FOM: <i>Represents an antenna's radiation pattern, its orientation in space, and the polarization of the radiation.</i>
      *
      * @return the time stamped <code>antennaPatternData</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::vector<DevStudio::AntennaPatternVariantStruct > > getAntennaPatternDataTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>cryptographicMode</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The mode of the cryptographic system.</i>
      *
      * @return true if <code>cryptographicMode</code> is available.
      */
      LIBAPI virtual bool hasCryptographicMode() = 0;

      /**
      * Gets the value of the <code>cryptographicMode</code> attribute.
      *
      * <br>Description from the FOM: <i>The mode of the cryptographic system.</i>
      * <br>Description of the data type from the FOM: <i>Represents the encryption mode of a cryptographic system.</i>
      *
      * @return the <code>cryptographicMode</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::CryptographicModeEnum::CryptographicModeEnum getCryptographicMode()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>cryptographicMode</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The mode of the cryptographic system.</i>
      * <br>Description of the data type from the FOM: <i>Represents the encryption mode of a cryptographic system.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>cryptographicMode</code> attribute.
      */
      LIBAPI virtual DevStudio::CryptographicModeEnum::CryptographicModeEnum getCryptographicMode(DevStudio::CryptographicModeEnum::CryptographicModeEnum defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>cryptographicMode</code> attribute.
      * <br>Description from the FOM: <i>The mode of the cryptographic system.</i>
      * <br>Description of the data type from the FOM: <i>Represents the encryption mode of a cryptographic system.</i>
      *
      * @return the time stamped <code>cryptographicMode</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::CryptographicModeEnum::CryptographicModeEnum > getCryptographicModeTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>cryptoSystem</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The type of cryptographic equipment in use.</i>
      *
      * @return true if <code>cryptoSystem</code> is available.
      */
      LIBAPI virtual bool hasCryptoSystem() = 0;

      /**
      * Gets the value of the <code>cryptoSystem</code> attribute.
      *
      * <br>Description from the FOM: <i>The type of cryptographic equipment in use.</i>
      * <br>Description of the data type from the FOM: <i>Identifies the type of cryptographic equipment</i>
      *
      * @return the <code>cryptoSystem</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::CryptographicSystemTypeEnum::CryptographicSystemTypeEnum getCryptoSystem()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>cryptoSystem</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The type of cryptographic equipment in use.</i>
      * <br>Description of the data type from the FOM: <i>Identifies the type of cryptographic equipment</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>cryptoSystem</code> attribute.
      */
      LIBAPI virtual DevStudio::CryptographicSystemTypeEnum::CryptographicSystemTypeEnum getCryptoSystem(DevStudio::CryptographicSystemTypeEnum::CryptographicSystemTypeEnum defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>cryptoSystem</code> attribute.
      * <br>Description from the FOM: <i>The type of cryptographic equipment in use.</i>
      * <br>Description of the data type from the FOM: <i>Identifies the type of cryptographic equipment</i>
      *
      * @return the time stamped <code>cryptoSystem</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::CryptographicSystemTypeEnum::CryptographicSystemTypeEnum > getCryptoSystemTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>encryptionKeyIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The identification of the key used to encrypt the radio signals being transmitted. The transmitter and receiver should be considered to be using the same key if these numbers match.</i>
      *
      * @return true if <code>encryptionKeyIdentifier</code> is available.
      */
      LIBAPI virtual bool hasEncryptionKeyIdentifier() = 0;

      /**
      * Gets the value of the <code>encryptionKeyIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>The identification of the key used to encrypt the radio signals being transmitted. The transmitter and receiver should be considered to be using the same key if these numbers match.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the <code>encryptionKeyIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual unsigned short getEncryptionKeyIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>encryptionKeyIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The identification of the key used to encrypt the radio signals being transmitted. The transmitter and receiver should be considered to be using the same key if these numbers match.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>encryptionKeyIdentifier</code> attribute.
      */
      LIBAPI virtual unsigned short getEncryptionKeyIdentifier(unsigned short defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>encryptionKeyIdentifier</code> attribute.
      * <br>Description from the FOM: <i>The identification of the key used to encrypt the radio signals being transmitted. The transmitter and receiver should be considered to be using the same key if these numbers match.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the time stamped <code>encryptionKeyIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< unsigned short > getEncryptionKeyIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>frequency</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The center frequency of the radio transmissions.</i>
      *
      * @return true if <code>frequency</code> is available.
      */
      LIBAPI virtual bool hasFrequency() = 0;

      /**
      * Gets the value of the <code>frequency</code> attribute.
      *
      * <br>Description from the FOM: <i>The center frequency of the radio transmissions.</i>
      * <br>Description of the data type from the FOM: <i>Frequency of a radio transmission, in hertz. [unit: hertz (Hz), resolution: NA, accuracy: NA]</i>
      *
      * @return the <code>frequency</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual unsigned long long getFrequency()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>frequency</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The center frequency of the radio transmissions.</i>
      * <br>Description of the data type from the FOM: <i>Frequency of a radio transmission, in hertz. [unit: hertz (Hz), resolution: NA, accuracy: NA]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>frequency</code> attribute.
      */
      LIBAPI virtual unsigned long long getFrequency(unsigned long long defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>frequency</code> attribute.
      * <br>Description from the FOM: <i>The center frequency of the radio transmissions.</i>
      * <br>Description of the data type from the FOM: <i>Frequency of a radio transmission, in hertz. [unit: hertz (Hz), resolution: NA, accuracy: NA]</i>
      *
      * @return the time stamped <code>frequency</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< unsigned long long > getFrequencyTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>frequencyBandwidth</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The bandpass of the radio transmissions.</i>
      *
      * @return true if <code>frequencyBandwidth</code> is available.
      */
      LIBAPI virtual bool hasFrequencyBandwidth() = 0;

      /**
      * Gets the value of the <code>frequencyBandwidth</code> attribute.
      *
      * <br>Description from the FOM: <i>The bandpass of the radio transmissions.</i>
      * <br>Description of the data type from the FOM: <i>Frequency, based on SI derived unit hertz, unit symbol Hz. [unit: hertz (Hz), resolution: NA, accuracy: NA]</i>
      *
      * @return the <code>frequencyBandwidth</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual float getFrequencyBandwidth()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>frequencyBandwidth</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The bandpass of the radio transmissions.</i>
      * <br>Description of the data type from the FOM: <i>Frequency, based on SI derived unit hertz, unit symbol Hz. [unit: hertz (Hz), resolution: NA, accuracy: NA]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>frequencyBandwidth</code> attribute.
      */
      LIBAPI virtual float getFrequencyBandwidth(float defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>frequencyBandwidth</code> attribute.
      * <br>Description from the FOM: <i>The bandpass of the radio transmissions.</i>
      * <br>Description of the data type from the FOM: <i>Frequency, based on SI derived unit hertz, unit symbol Hz. [unit: hertz (Hz), resolution: NA, accuracy: NA]</i>
      *
      * @return the time stamped <code>frequencyBandwidth</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< float > getFrequencyBandwidthTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>radioIndex</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>A number that uniquely identifies this radio from others on the host.</i>
      *
      * @return true if <code>radioIndex</code> is available.
      */
      LIBAPI virtual bool hasRadioIndex() = 0;

      /**
      * Gets the value of the <code>radioIndex</code> attribute.
      *
      * <br>Description from the FOM: <i>A number that uniquely identifies this radio from others on the host.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the <code>radioIndex</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual unsigned short getRadioIndex()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>radioIndex</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>A number that uniquely identifies this radio from others on the host.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>radioIndex</code> attribute.
      */
      LIBAPI virtual unsigned short getRadioIndex(unsigned short defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>radioIndex</code> attribute.
      * <br>Description from the FOM: <i>A number that uniquely identifies this radio from others on the host.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^16-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the time stamped <code>radioIndex</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< unsigned short > getRadioIndexTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>radioInputSource</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Specifies which position or data port provided the input for the transmission.</i>
      *
      * @return true if <code>radioInputSource</code> is available.
      */
      LIBAPI virtual bool hasRadioInputSource() = 0;

      /**
      * Gets the value of the <code>radioInputSource</code> attribute.
      *
      * <br>Description from the FOM: <i>Specifies which position or data port provided the input for the transmission.</i>
      * <br>Description of the data type from the FOM: <i>Radio input source</i>
      *
      * @return the <code>radioInputSource</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::RadioInputSourceEnum::RadioInputSourceEnum getRadioInputSource()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>radioInputSource</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Specifies which position or data port provided the input for the transmission.</i>
      * <br>Description of the data type from the FOM: <i>Radio input source</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>radioInputSource</code> attribute.
      */
      LIBAPI virtual DevStudio::RadioInputSourceEnum::RadioInputSourceEnum getRadioInputSource(DevStudio::RadioInputSourceEnum::RadioInputSourceEnum defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>radioInputSource</code> attribute.
      * <br>Description from the FOM: <i>Specifies which position or data port provided the input for the transmission.</i>
      * <br>Description of the data type from the FOM: <i>Radio input source</i>
      *
      * @return the time stamped <code>radioInputSource</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::RadioInputSourceEnum::RadioInputSourceEnum > getRadioInputSourceTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>radioSystemType</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The type of radio transmitter.</i>
      *
      * @return true if <code>radioSystemType</code> is available.
      */
      LIBAPI virtual bool hasRadioSystemType() = 0;

      /**
      * Gets the value of the <code>radioSystemType</code> attribute.
      *
      * <br>Description from the FOM: <i>The type of radio transmitter.</i>
      * <br>Description of the data type from the FOM: <i>Specifies the type of a radio.</i>
      *
      * @return the <code>radioSystemType</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::RadioTypeStruct getRadioSystemType()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>radioSystemType</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The type of radio transmitter.</i>
      * <br>Description of the data type from the FOM: <i>Specifies the type of a radio.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>radioSystemType</code> attribute.
      */
      LIBAPI virtual DevStudio::RadioTypeStruct getRadioSystemType(DevStudio::RadioTypeStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>radioSystemType</code> attribute.
      * <br>Description from the FOM: <i>The type of radio transmitter.</i>
      * <br>Description of the data type from the FOM: <i>Specifies the type of a radio.</i>
      *
      * @return the time stamped <code>radioSystemType</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::RadioTypeStruct > getRadioSystemTypeTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>rFModulationSystemType</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The radio system type associated with this transmitter.</i>
      *
      * @return true if <code>rFModulationSystemType</code> is available.
      */
      LIBAPI virtual bool hasRFModulationSystemType() = 0;

      /**
      * Gets the value of the <code>rFModulationSystemType</code> attribute.
      *
      * <br>Description from the FOM: <i>The radio system type associated with this transmitter.</i>
      * <br>Description of the data type from the FOM: <i>The radio system type associated with this transmitter.</i>
      *
      * @return the <code>rFModulationSystemType</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::RFModulationSystemTypeEnum::RFModulationSystemTypeEnum getRFModulationSystemType()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>rFModulationSystemType</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The radio system type associated with this transmitter.</i>
      * <br>Description of the data type from the FOM: <i>The radio system type associated with this transmitter.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>rFModulationSystemType</code> attribute.
      */
      LIBAPI virtual DevStudio::RFModulationSystemTypeEnum::RFModulationSystemTypeEnum getRFModulationSystemType(DevStudio::RFModulationSystemTypeEnum::RFModulationSystemTypeEnum defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>rFModulationSystemType</code> attribute.
      * <br>Description from the FOM: <i>The radio system type associated with this transmitter.</i>
      * <br>Description of the data type from the FOM: <i>The radio system type associated with this transmitter.</i>
      *
      * @return the time stamped <code>rFModulationSystemType</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::RFModulationSystemTypeEnum::RFModulationSystemTypeEnum > getRFModulationSystemTypeTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>rFModulationType</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Classification of the modulation type.</i>
      *
      * @return true if <code>rFModulationType</code> is available.
      */
      LIBAPI virtual bool hasRFModulationType() = 0;

      /**
      * Gets the value of the <code>rFModulationType</code> attribute.
      *
      * <br>Description from the FOM: <i>Classification of the modulation type.</i>
      * <br>Description of the data type from the FOM: <i>Specifies the major modulation type as well as certain detailed information specific to the type.</i>
      *
      * @return the <code>rFModulationType</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::RFModulationTypeVariantStruct getRFModulationType()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>rFModulationType</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Classification of the modulation type.</i>
      * <br>Description of the data type from the FOM: <i>Specifies the major modulation type as well as certain detailed information specific to the type.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>rFModulationType</code> attribute.
      */
      LIBAPI virtual DevStudio::RFModulationTypeVariantStruct getRFModulationType(DevStudio::RFModulationTypeVariantStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>rFModulationType</code> attribute.
      * <br>Description from the FOM: <i>Classification of the modulation type.</i>
      * <br>Description of the data type from the FOM: <i>Specifies the major modulation type as well as certain detailed information specific to the type.</i>
      *
      * @return the time stamped <code>rFModulationType</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::RFModulationTypeVariantStruct > getRFModulationTypeTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>spreadSpectrum</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Describes the spread spectrum characteristics of the transmission, such as frequency hopping or other spread spectrum transmission modes.</i>
      *
      * @return true if <code>spreadSpectrum</code> is available.
      */
      LIBAPI virtual bool hasSpreadSpectrum() = 0;

      /**
      * Gets the value of the <code>spreadSpectrum</code> attribute.
      *
      * <br>Description from the FOM: <i>Describes the spread spectrum characteristics of the transmission, such as frequency hopping or other spread spectrum transmission modes.</i>
      * <br>Description of the data type from the FOM: <i>Identifies the actual spread spectrum technique in use.</i>
      *
      * @return the <code>spreadSpectrum</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::SpreadSpectrumVariantStruct getSpreadSpectrum()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>spreadSpectrum</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Describes the spread spectrum characteristics of the transmission, such as frequency hopping or other spread spectrum transmission modes.</i>
      * <br>Description of the data type from the FOM: <i>Identifies the actual spread spectrum technique in use.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>spreadSpectrum</code> attribute.
      */
      LIBAPI virtual DevStudio::SpreadSpectrumVariantStruct getSpreadSpectrum(DevStudio::SpreadSpectrumVariantStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>spreadSpectrum</code> attribute.
      * <br>Description from the FOM: <i>Describes the spread spectrum characteristics of the transmission, such as frequency hopping or other spread spectrum transmission modes.</i>
      * <br>Description of the data type from the FOM: <i>Identifies the actual spread spectrum technique in use.</i>
      *
      * @return the time stamped <code>spreadSpectrum</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::SpreadSpectrumVariantStruct > getSpreadSpectrumTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>streamTag</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>A globally unique identifier for the associated audio stream</i>
      *
      * @return true if <code>streamTag</code> is available.
      */
      LIBAPI virtual bool hasStreamTag() = 0;

      /**
      * Gets the value of the <code>streamTag</code> attribute.
      *
      * <br>Description from the FOM: <i>A globally unique identifier for the associated audio stream</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^64-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the <code>streamTag</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual unsigned long long getStreamTag()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>streamTag</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>A globally unique identifier for the associated audio stream</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^64-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>streamTag</code> attribute.
      */
      LIBAPI virtual unsigned long long getStreamTag(unsigned long long defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>streamTag</code> attribute.
      * <br>Description from the FOM: <i>A globally unique identifier for the associated audio stream</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^64-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the time stamped <code>streamTag</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< unsigned long long > getStreamTagTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>timeHopInUse</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Whether the radio is using time hopping or not.</i>
      *
      * @return true if <code>timeHopInUse</code> is available.
      */
      LIBAPI virtual bool hasTimeHopInUse() = 0;

      /**
      * Gets the value of the <code>timeHopInUse</code> attribute.
      *
      * <br>Description from the FOM: <i>Whether the radio is using time hopping or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>timeHopInUse</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual bool getTimeHopInUse()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>timeHopInUse</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Whether the radio is using time hopping or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>timeHopInUse</code> attribute.
      */
      LIBAPI virtual bool getTimeHopInUse(bool defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>timeHopInUse</code> attribute.
      * <br>Description from the FOM: <i>Whether the radio is using time hopping or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>timeHopInUse</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< bool > getTimeHopInUseTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>transmittedPower</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The average transmitted power.</i>
      *
      * @return true if <code>transmittedPower</code> is available.
      */
      LIBAPI virtual bool hasTransmittedPower() = 0;

      /**
      * Gets the value of the <code>transmittedPower</code> attribute.
      *
      * <br>Description from the FOM: <i>The average transmitted power.</i>
      * <br>Description of the data type from the FOM: <i>Power ratio in decibels (dB) of a measured power referenced to 1 milliwatt (mW). [unit: decibel milliwatt (dBm), resolution: NA, accuracy: perfect]</i>
      *
      * @return the <code>transmittedPower</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual float getTransmittedPower()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>transmittedPower</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The average transmitted power.</i>
      * <br>Description of the data type from the FOM: <i>Power ratio in decibels (dB) of a measured power referenced to 1 milliwatt (mW). [unit: decibel milliwatt (dBm), resolution: NA, accuracy: perfect]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>transmittedPower</code> attribute.
      */
      LIBAPI virtual float getTransmittedPower(float defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>transmittedPower</code> attribute.
      * <br>Description from the FOM: <i>The average transmitted power.</i>
      * <br>Description of the data type from the FOM: <i>Power ratio in decibels (dB) of a measured power referenced to 1 milliwatt (mW). [unit: decibel milliwatt (dBm), resolution: NA, accuracy: perfect]</i>
      *
      * @return the time stamped <code>transmittedPower</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< float > getTransmittedPowerTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>transmitterOperationalStatus</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The current operational state of the radio transmitter.</i>
      *
      * @return true if <code>transmitterOperationalStatus</code> is available.
      */
      LIBAPI virtual bool hasTransmitterOperationalStatus() = 0;

      /**
      * Gets the value of the <code>transmitterOperationalStatus</code> attribute.
      *
      * <br>Description from the FOM: <i>The current operational state of the radio transmitter.</i>
      * <br>Description of the data type from the FOM: <i>The current operational state of a radio transmitter.</i>
      *
      * @return the <code>transmitterOperationalStatus</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::TransmitterOperationalStatusEnum::TransmitterOperationalStatusEnum getTransmitterOperationalStatus()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>transmitterOperationalStatus</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The current operational state of the radio transmitter.</i>
      * <br>Description of the data type from the FOM: <i>The current operational state of a radio transmitter.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>transmitterOperationalStatus</code> attribute.
      */
      LIBAPI virtual DevStudio::TransmitterOperationalStatusEnum::TransmitterOperationalStatusEnum getTransmitterOperationalStatus(DevStudio::TransmitterOperationalStatusEnum::TransmitterOperationalStatusEnum defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>transmitterOperationalStatus</code> attribute.
      * <br>Description from the FOM: <i>The current operational state of the radio transmitter.</i>
      * <br>Description of the data type from the FOM: <i>The current operational state of a radio transmitter.</i>
      *
      * @return the time stamped <code>transmitterOperationalStatus</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::TransmitterOperationalStatusEnum::TransmitterOperationalStatusEnum > getTransmitterOperationalStatusTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>worldLocation</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The location of the antenna in the world coordinate system.</i>
      *
      * @return true if <code>worldLocation</code> is available.
      */
      LIBAPI virtual bool hasWorldLocation() = 0;

      /**
      * Gets the value of the <code>worldLocation</code> attribute.
      *
      * <br>Description from the FOM: <i>The location of the antenna in the world coordinate system.</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @return the <code>worldLocation</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::WorldLocationStruct getWorldLocation()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>worldLocation</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The location of the antenna in the world coordinate system.</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>worldLocation</code> attribute.
      */
      LIBAPI virtual DevStudio::WorldLocationStruct getWorldLocation(DevStudio::WorldLocationStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>worldLocation</code> attribute.
      * <br>Description from the FOM: <i>The location of the antenna in the world coordinate system.</i>
      * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
      *
      * @return the time stamped <code>worldLocation</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::WorldLocationStruct > getWorldLocationTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>entityIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
      *
      * @return true if <code>entityIdentifier</code> is available.
      */
      LIBAPI virtual bool hasEntityIdentifier() = 0;

      /**
      * Gets the value of the <code>entityIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @return the <code>entityIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::EntityIdentifierStruct getEntityIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>entityIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>entityIdentifier</code> attribute.
      */
      LIBAPI virtual DevStudio::EntityIdentifierStruct getEntityIdentifier(DevStudio::EntityIdentifierStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>entityIdentifier</code> attribute.
      * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @return the time stamped <code>entityIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::EntityIdentifierStruct > getEntityIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>hostObjectIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
      *
      * @return true if <code>hostObjectIdentifier</code> is available.
      */
      LIBAPI virtual bool hasHostObjectIdentifier() = 0;

      /**
      * Gets the value of the <code>hostObjectIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @return the <code>hostObjectIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::string getHostObjectIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>hostObjectIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>hostObjectIdentifier</code> attribute.
      */
      LIBAPI virtual std::string getHostObjectIdentifier(std::string defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>hostObjectIdentifier</code> attribute.
      * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
      * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
      *
      * @return the time stamped <code>hostObjectIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::string > getHostObjectIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>relativePosition</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
      *
      * @return true if <code>relativePosition</code> is available.
      */
      LIBAPI virtual bool hasRelativePosition() = 0;

      /**
      * Gets the value of the <code>relativePosition</code> attribute.
      *
      * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
      * <br>Description of the data type from the FOM: <i>Relative position in right-handed Cartesian coordinates.</i>
      *
      * @return the <code>relativePosition</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::RelativePositionStruct getRelativePosition()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>relativePosition</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
      * <br>Description of the data type from the FOM: <i>Relative position in right-handed Cartesian coordinates.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>relativePosition</code> attribute.
      */
      LIBAPI virtual DevStudio::RelativePositionStruct getRelativePosition(DevStudio::RelativePositionStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>relativePosition</code> attribute.
      * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
      * <br>Description of the data type from the FOM: <i>Relative position in right-handed Cartesian coordinates.</i>
      *
      * @return the time stamped <code>relativePosition</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::RelativePositionStruct > getRelativePositionTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
   };
}
#endif
