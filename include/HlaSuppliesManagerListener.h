/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLASUPPLIESMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLASUPPLIESMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaSupplies.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaSupplies instances.
    */
    class HlaSuppliesManagerListener {

    public:

        LIBAPI virtual ~HlaSuppliesManagerListener() {}

        /**
        * This method is called when a new HlaSupplies instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param supplies the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaSuppliesDiscovered(HlaSuppliesPtr supplies, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaSupplies instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param supplies the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaSuppliesInitialized(HlaSuppliesPtr supplies, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaSuppliesManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param supplies the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaSuppliesInInterest(HlaSuppliesPtr supplies, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaSuppliesManagerListener instance goes out of interest.
        *
        * @param supplies the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaSuppliesOutOfInterest(HlaSuppliesPtr supplies, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaSupplies instance is deleted.
        *
        * @param supplies the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaSuppliesDeleted(HlaSuppliesPtr supplies, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaSuppliesManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaSuppliesManagerListener::Adapter : public HlaSuppliesManagerListener {

    public:
        LIBAPI virtual void hlaSuppliesDiscovered(HlaSuppliesPtr supplies, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaSuppliesInitialized(HlaSuppliesPtr supplies, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaSuppliesInInterest(HlaSuppliesPtr supplies, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaSuppliesOutOfInterest(HlaSuppliesPtr supplies, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaSuppliesDeleted(HlaSuppliesPtr supplies, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
