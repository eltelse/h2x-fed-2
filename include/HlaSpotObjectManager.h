/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLASPOTOBJECTMANAGER_H
#define DEVELOPER_STUDIO_HLASPOTOBJECTMANAGER_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <DevStudio/datatypes/AcquisitionTypeEnum.h>
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EntityTypeStruct.h>
#include <DevStudio/datatypes/FortificationsEnum.h>
#include <DevStudio/datatypes/GeographicShapeStruct.h>
#include <DevStudio/datatypes/HostilityTypeEnum.h>
#include <DevStudio/datatypes/IsPartOfStruct.h>
#include <DevStudio/datatypes/PositionStruct.h>
#include <DevStudio/datatypes/SpatialVariantStruct.h>
#include <DevStudio/datatypes/SpotCategoryEnum.h>
#include <string>

#include <list>
#include <vector>

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaSpotObjectManagerListener.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaException.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

   /**
   * Manager used to manage HlaSpotObjects.
   */
   class HlaSpotObjectManager : private boost::noncopyable {

   public:

      LIBAPI virtual ~HlaSpotObjectManager() {}

      /**
      * Gets a list of all HlaSpotObjects within interest, both local and remote.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of HlaSpotObjects
      */
      LIBAPI virtual std::list<HlaSpotObjectPtr> getHlaSpotObjects() = 0;

      /**
      * Gets a list of all HlaSpotObjects, both local and remote.
      * HlaSpotObjects not within interest are also included.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of all HlaSpotObjects
      */
      LIBAPI virtual std::list<HlaSpotObjectPtr> getAllHlaSpotObjects() = 0;

      /**
      * Gets a list of local HlaSpotObjects within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of local HlaSpotObjects
      */
      LIBAPI virtual std::list<HlaSpotObjectPtr> getLocalHlaSpotObjects() = 0;

      /**
      * Gets a list of remote HlaSpotObjects within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of remote HlaSpotObjects
      */
      LIBAPI virtual std::list<HlaSpotObjectPtr> getRemoteHlaSpotObjects() = 0;

      /**
      * Find a HlaSpotObject with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> of the HlaSpotObject to find
      *
      * @return the specified HlaSpotObject, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaSpotObjectPtr getSpotObjectByHlaInstanceName(const std::wstring& hlaInstanceName) = 0;

      /**
      * Find a HlaSpotObject with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param encodedHlaInstanceHandle the encoded <code>HLA instance handle</code> of the HlaSpotObject to find
      *
      * @return the specified HlaSpotObject, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaSpotObjectPtr getSpotObjectByHlaInstanceHandle(const std::vector<char>& encodedHlaInstanceHandle) = 0;

      /**
      * Creates a new HlaSpotObject, and sets all <i>Create</i> attributes.
      *
      * @return Returns a new local HlaSpotObject.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaSpotObjectPtr createLocalHlaSpotObject(
      ) THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Creates a new HlaSpotObject with a specified <code>HLA instance name</code>, and sets all
      * <i>Create</i> attributes.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> to register for this instance
      *
      * @return Returns a new local HlaSpotObject.
      *
      * @throws HlaIllegalInstanceNameException if the name is illegal
      * @throws HlaInstanceNameInUseException if the name is already in use
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaSpotObjectPtr createLocalHlaSpotObject(
         const std::wstring& hlaInstanceName
      ) THROW_SPEC (HlaIllegalInstanceNameException, HlaInstanceNameInUseException,
                    HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaSpotObject and removes it from the federation.
      *
      * @param spotObject The HlaSpotObject to delete
      *
      * @return the HlaSpotObject deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaSpotObjectPtr deleteLocalHlaSpotObject(HlaSpotObjectPtr spotObject)
          THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                      HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaSpotObject and removes it from the federation.
      *
      * @param spotObject The HlaSpotObject to delete
      * @param timestamp timestamp when the instance was removed.
      *
      * @return the HlaSpotObject deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaSpotObjectPtr deleteLocalHlaSpotObject(HlaSpotObjectPtr spotObject, HlaTimeStampPtr timestamp)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaSpotObject and removes it from the federation.
      *
      * @param spotObject The HlaSpotObject to delete
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaSpotObject deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaSpotObjectPtr deleteLocalHlaSpotObject(HlaSpotObjectPtr spotObject, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaSpotObject and removes it from the federation.
      *
      * @param spotObject The HlaSpotObject to delete
      * @param timestamp timestamp when the instance was removed.
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaSpotObject deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaSpotObjectPtr deleteLocalHlaSpotObject(HlaSpotObjectPtr spotObject, HlaTimeStampPtr timestamp, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Add a HlaSpotObject manager listener.
      *
      * @param managerListener listener to add
      */
      LIBAPI virtual void addHlaSpotObjectManagerListener(HlaSpotObjectManagerListenerPtr managerListener) = 0;

      /**
      * Removed a HlaSpotObject manager listener.
      *
      * @param managerListener listener to remove
      */
      LIBAPI virtual void removeHlaSpotObjectManagerListener(HlaSpotObjectManagerListenerPtr managerListener) = 0;

     /**
      * Add a listener that will automatically be added to all instances of HlaSpotObject (Both local and remote).
      * The listener is notified when any attribute of an instance of HlaSpotObject is updated.
      * The listener is also called when an interaction is sent to an instance of HlaSpotObject.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param listener The listener to add.
      */
      LIBAPI virtual void addHlaSpotObjectDefaultInstanceListener(HlaSpotObjectListenerPtr listener) = 0;

     /**
      * Remove a listener from the set of listeners that are automatically added to new instances of HlaSpotObject.
      * Note: The listener will not be removed from already existing instances of HlaSpotObject.
      * This method is idempotent.
      *
      * @param listener The listener to remove.
      */
      LIBAPI virtual void removeHlaSpotObjectDefaultInstanceListener(HlaSpotObjectListenerPtr listener) = 0;

     /**
      * Add a valueListener that will automatically be added to all instances of HlaSpotObject (Both local and remote).
      * The  Valuelistener is notified when any attribute of an instance of HlaSpotObject is updated.
      * Note that the attribute values that are notified are not guaranteed to
      * be the latest available values for that attribute.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to add.
      */
      LIBAPI virtual void addHlaSpotObjectDefaultInstanceValueListener(HlaSpotObjectValueListenerPtr valueListener) = 0;

     /**
      * Remove a valueListener from the set of listeners that are automatically added to new instances of HlaSpotObject.
      * Note: The valueListener will not be removed from already existing instances of HlaSpotObject.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to remove.
      */
      LIBAPI virtual void removeHlaSpotObjectDefaultInstanceValueListener(HlaSpotObjectValueListenerPtr valueListener) = 0;

     /**
      * Enables or disables the HlaSpotObject manager.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaSpotObject manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      *
      * @param enabled <code>true</code> to enable the HlaSpotObject manager, otherwise <code>false</code>
      */
      LIBAPI virtual void setEnabled(bool enabled) = 0;

     /**
      * Enables the HlaSpotObject manager if it is avalable in the FOM at runtime.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaSpotObject manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      */
      LIBAPI virtual void setEnabledIfAvailableInFom() = 0;

     /**
      * Check if the HlaSpotObject manager is actually enabled when connected.
      * An HlaSpotObject manager may be disabled with the <code>setEnabled(false)</code> function, or it may be configured
      * as <i>if available in the FOM</i> by the <code>setEnabledIfAvailableInFom()</code> function and
      * then not available in the FOM at runtime.
      * <p>
      * This method should only be used when connected, it will always return <code>false</code>
      * when not connected.
      *
      * @return <code>true</code> if connected and the HlaSpotObject manager is enabled.
      */
      LIBAPI virtual bool isEnabled() = 0;
   };
}
#endif
