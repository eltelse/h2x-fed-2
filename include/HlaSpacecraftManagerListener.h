/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLASPACECRAFTMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLASPACECRAFTMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaSpacecraft.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaSpacecraft instances.
    */
    class HlaSpacecraftManagerListener {

    public:

        LIBAPI virtual ~HlaSpacecraftManagerListener() {}

        /**
        * This method is called when a new HlaSpacecraft instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param spacecraft the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaSpacecraftDiscovered(HlaSpacecraftPtr spacecraft, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaSpacecraft instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param spacecraft the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaSpacecraftInitialized(HlaSpacecraftPtr spacecraft, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaSpacecraftManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param spacecraft the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaSpacecraftInInterest(HlaSpacecraftPtr spacecraft, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaSpacecraftManagerListener instance goes out of interest.
        *
        * @param spacecraft the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaSpacecraftOutOfInterest(HlaSpacecraftPtr spacecraft, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaSpacecraft instance is deleted.
        *
        * @param spacecraft the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaSpacecraftDeleted(HlaSpacecraftPtr spacecraft, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaSpacecraftManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaSpacecraftManagerListener::Adapter : public HlaSpacecraftManagerListener {

    public:
        LIBAPI virtual void hlaSpacecraftDiscovered(HlaSpacecraftPtr spacecraft, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaSpacecraftInitialized(HlaSpacecraftPtr spacecraft, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaSpacecraftInInterest(HlaSpacecraftPtr spacecraft, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaSpacecraftOutOfInterest(HlaSpacecraftPtr spacecraft, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaSpacecraftDeleted(HlaSpacecraftPtr spacecraft, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
