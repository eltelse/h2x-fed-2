/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLARADIOTRANSMITTERLISTENER_H
#define DEVELOPER_STUDIO_HLARADIOTRANSMITTERLISTENER_H

#include <set>


#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaRadioTransmitterAttributes.h>

namespace DevStudio {
   /**
   * Listener for updates of attributes.  
   */
   class HlaRadioTransmitterListener {
   public:

      LIBAPI virtual ~HlaRadioTransmitterListener() {}

      /**
      * This method is called when a HLA <code>reflectAttributeValueUpdate</code> is received for an remote object,
      * or a set of attributes is updated on a local object.
      *
      * @param radioTransmitter The radioTransmitter which this update corresponds to.
      * @param attributes The set of attributes that are updated.
      * @param timeStamp The time when the update was initiated.
      * @param logicalTime The logical time the update was initiated.
      */
      LIBAPI virtual void attributesUpdated(HlaRadioTransmitterPtr radioTransmitter, std::set<HlaRadioTransmitterAttributes::Attribute> &attributes, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

      class Adapter;
   };

  /**
  * An adapter class that implements the HlaRadioTransmitterListener interface with empty methods.
  * It might be used as a base class for a listener.
  */
  class HlaRadioTransmitterListener::Adapter : public HlaRadioTransmitterListener {

  public:

     LIBAPI virtual void attributesUpdated(HlaRadioTransmitterPtr radioTransmitter, std::set<HlaRadioTransmitterAttributes::Attribute> &attributes, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
    };

}
#endif
