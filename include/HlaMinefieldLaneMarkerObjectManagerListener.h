/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAMINEFIELDLANEMARKEROBJECTMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLAMINEFIELDLANEMARKEROBJECTMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaMinefieldLaneMarkerObject.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaMinefieldLaneMarkerObject instances.
    */
    class HlaMinefieldLaneMarkerObjectManagerListener {

    public:

        LIBAPI virtual ~HlaMinefieldLaneMarkerObjectManagerListener() {}

        /**
        * This method is called when a new HlaMinefieldLaneMarkerObject instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param minefieldLaneMarkerObject the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaMinefieldLaneMarkerObjectDiscovered(HlaMinefieldLaneMarkerObjectPtr minefieldLaneMarkerObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaMinefieldLaneMarkerObject instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param minefieldLaneMarkerObject the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaMinefieldLaneMarkerObjectInitialized(HlaMinefieldLaneMarkerObjectPtr minefieldLaneMarkerObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaMinefieldLaneMarkerObjectManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param minefieldLaneMarkerObject the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaMinefieldLaneMarkerObjectInInterest(HlaMinefieldLaneMarkerObjectPtr minefieldLaneMarkerObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaMinefieldLaneMarkerObjectManagerListener instance goes out of interest.
        *
        * @param minefieldLaneMarkerObject the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaMinefieldLaneMarkerObjectOutOfInterest(HlaMinefieldLaneMarkerObjectPtr minefieldLaneMarkerObject, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaMinefieldLaneMarkerObject instance is deleted.
        *
        * @param minefieldLaneMarkerObject the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaMinefieldLaneMarkerObjectDeleted(HlaMinefieldLaneMarkerObjectPtr minefieldLaneMarkerObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaMinefieldLaneMarkerObjectManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaMinefieldLaneMarkerObjectManagerListener::Adapter : public HlaMinefieldLaneMarkerObjectManagerListener {

    public:
        LIBAPI virtual void hlaMinefieldLaneMarkerObjectDiscovered(HlaMinefieldLaneMarkerObjectPtr minefieldLaneMarkerObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaMinefieldLaneMarkerObjectInitialized(HlaMinefieldLaneMarkerObjectPtr minefieldLaneMarkerObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaMinefieldLaneMarkerObjectInInterest(HlaMinefieldLaneMarkerObjectPtr minefieldLaneMarkerObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaMinefieldLaneMarkerObjectOutOfInterest(HlaMinefieldLaneMarkerObjectPtr minefieldLaneMarkerObject, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaMinefieldLaneMarkerObjectDeleted(HlaMinefieldLaneMarkerObjectPtr minefieldLaneMarkerObject, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
