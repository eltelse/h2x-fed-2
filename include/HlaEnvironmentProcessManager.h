/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAENVIRONMENTPROCESSMANAGER_H
#define DEVELOPER_STUDIO_HLAENVIRONMENTPROCESSMANAGER_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EnvironmentModelTypeEnum.h>
#include <DevStudio/datatypes/EnvironmentRecStruct.h>
#include <DevStudio/datatypes/EnvironmentRecStructArray.h>
#include <DevStudio/datatypes/EnvironmentTypeStruct.h>
#include <vector>

#include <list>
#include <vector>

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaEnvironmentProcessManagerListener.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaException.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

   /**
   * Manager used to manage HlaEnvironmentProcess.
   */
   class HlaEnvironmentProcessManager : private boost::noncopyable {

   public:

      LIBAPI virtual ~HlaEnvironmentProcessManager() {}

      /**
      * Gets a list of all HlaEnvironmentProcess within interest, both local and remote.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of HlaEnvironmentProcess
      */
      LIBAPI virtual std::list<HlaEnvironmentProcessPtr> getHlaEnvironmentProcess() = 0;

      /**
      * Gets a list of all HlaEnvironmentProcess, both local and remote.
      * HlaEnvironmentProcess not within interest are also included.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of all HlaEnvironmentProcess
      */
      LIBAPI virtual std::list<HlaEnvironmentProcessPtr> getAllHlaEnvironmentProcess() = 0;

      /**
      * Gets a list of local HlaEnvironmentProcess within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of local HlaEnvironmentProcess
      */
      LIBAPI virtual std::list<HlaEnvironmentProcessPtr> getLocalHlaEnvironmentProcess() = 0;

      /**
      * Gets a list of remote HlaEnvironmentProcesss within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of remote HlaEnvironmentProcesss
      */
      LIBAPI virtual std::list<HlaEnvironmentProcessPtr> getRemoteHlaEnvironmentProcess() = 0;

      /**
      * Find a HlaEnvironmentProcess with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> of the HlaEnvironmentProcess to find
      *
      * @return the specified HlaEnvironmentProcess, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaEnvironmentProcessPtr getEnvironmentProcessByHlaInstanceName(const std::wstring& hlaInstanceName) = 0;

      /**
      * Find a HlaEnvironmentProcess with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param encodedHlaInstanceHandle the encoded <code>HLA instance handle</code> of the HlaEnvironmentProcess to find
      *
      * @return the specified HlaEnvironmentProcess, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaEnvironmentProcessPtr getEnvironmentProcessByHlaInstanceHandle(const std::vector<char>& encodedHlaInstanceHandle) = 0;

      /**
      * Creates a new HlaEnvironmentProcess, and sets all <i>Create</i> attributes.
      *
      * @return Returns a new local HlaEnvironmentProcess.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaEnvironmentProcessPtr createLocalHlaEnvironmentProcess(
      ) THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Creates a new HlaEnvironmentProcess with a specified <code>HLA instance name</code>, and sets all
      * <i>Create</i> attributes.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> to register for this instance
      *
      * @return Returns a new local HlaEnvironmentProcess.
      *
      * @throws HlaIllegalInstanceNameException if the name is illegal
      * @throws HlaInstanceNameInUseException if the name is already in use
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaEnvironmentProcessPtr createLocalHlaEnvironmentProcess(
         const std::wstring& hlaInstanceName
      ) THROW_SPEC (HlaIllegalInstanceNameException, HlaInstanceNameInUseException,
                    HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaEnvironmentProcess and removes it from the federation.
      *
      * @param environmentProcess The HlaEnvironmentProcess to delete
      *
      * @return the HlaEnvironmentProcess deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaEnvironmentProcessPtr deleteLocalHlaEnvironmentProcess(HlaEnvironmentProcessPtr environmentProcess)
          THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                      HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaEnvironmentProcess and removes it from the federation.
      *
      * @param environmentProcess The HlaEnvironmentProcess to delete
      * @param timestamp timestamp when the instance was removed.
      *
      * @return the HlaEnvironmentProcess deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaEnvironmentProcessPtr deleteLocalHlaEnvironmentProcess(HlaEnvironmentProcessPtr environmentProcess, HlaTimeStampPtr timestamp)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaEnvironmentProcess and removes it from the federation.
      *
      * @param environmentProcess The HlaEnvironmentProcess to delete
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaEnvironmentProcess deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaEnvironmentProcessPtr deleteLocalHlaEnvironmentProcess(HlaEnvironmentProcessPtr environmentProcess, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaEnvironmentProcess and removes it from the federation.
      *
      * @param environmentProcess The HlaEnvironmentProcess to delete
      * @param timestamp timestamp when the instance was removed.
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaEnvironmentProcess deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaEnvironmentProcessPtr deleteLocalHlaEnvironmentProcess(HlaEnvironmentProcessPtr environmentProcess, HlaTimeStampPtr timestamp, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Add a HlaEnvironmentProcess manager listener.
      *
      * @param managerListener listener to add
      */
      LIBAPI virtual void addHlaEnvironmentProcessManagerListener(HlaEnvironmentProcessManagerListenerPtr managerListener) = 0;

      /**
      * Removed a HlaEnvironmentProcess manager listener.
      *
      * @param managerListener listener to remove
      */
      LIBAPI virtual void removeHlaEnvironmentProcessManagerListener(HlaEnvironmentProcessManagerListenerPtr managerListener) = 0;

     /**
      * Add a listener that will automatically be added to all instances of HlaEnvironmentProcess (Both local and remote).
      * The listener is notified when any attribute of an instance of HlaEnvironmentProcess is updated.
      * The listener is also called when an interaction is sent to an instance of HlaEnvironmentProcess.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param listener The listener to add.
      */
      LIBAPI virtual void addHlaEnvironmentProcessDefaultInstanceListener(HlaEnvironmentProcessListenerPtr listener) = 0;

     /**
      * Remove a listener from the set of listeners that are automatically added to new instances of HlaEnvironmentProcess.
      * Note: The listener will not be removed from already existing instances of HlaEnvironmentProcess.
      * This method is idempotent.
      *
      * @param listener The listener to remove.
      */
      LIBAPI virtual void removeHlaEnvironmentProcessDefaultInstanceListener(HlaEnvironmentProcessListenerPtr listener) = 0;

     /**
      * Add a valueListener that will automatically be added to all instances of HlaEnvironmentProcess (Both local and remote).
      * The  Valuelistener is notified when any attribute of an instance of HlaEnvironmentProcess is updated.
      * Note that the attribute values that are notified are not guaranteed to
      * be the latest available values for that attribute.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to add.
      */
      LIBAPI virtual void addHlaEnvironmentProcessDefaultInstanceValueListener(HlaEnvironmentProcessValueListenerPtr valueListener) = 0;

     /**
      * Remove a valueListener from the set of listeners that are automatically added to new instances of HlaEnvironmentProcess.
      * Note: The valueListener will not be removed from already existing instances of HlaEnvironmentProcess.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to remove.
      */
      LIBAPI virtual void removeHlaEnvironmentProcessDefaultInstanceValueListener(HlaEnvironmentProcessValueListenerPtr valueListener) = 0;

     /**
      * Enables or disables the HlaEnvironmentProcess manager.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaEnvironmentProcess manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      *
      * @param enabled <code>true</code> to enable the HlaEnvironmentProcess manager, otherwise <code>false</code>
      */
      LIBAPI virtual void setEnabled(bool enabled) = 0;

     /**
      * Enables the HlaEnvironmentProcess manager if it is avalable in the FOM at runtime.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaEnvironmentProcess manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      */
      LIBAPI virtual void setEnabledIfAvailableInFom() = 0;

     /**
      * Check if the HlaEnvironmentProcess manager is actually enabled when connected.
      * An HlaEnvironmentProcess manager may be disabled with the <code>setEnabled(false)</code> function, or it may be configured
      * as <i>if available in the FOM</i> by the <code>setEnabledIfAvailableInFom()</code> function and
      * then not available in the FOM at runtime.
      * <p>
      * This method should only be used when connected, it will always return <code>false</code>
      * when not connected.
      *
      * @return <code>true</code> if connected and the HlaEnvironmentProcess manager is enabled.
      */
      LIBAPI virtual bool isEnabled() = 0;
   };
}
#endif
