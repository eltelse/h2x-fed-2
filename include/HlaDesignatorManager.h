/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLADESIGNATORMANAGER_H
#define DEVELOPER_STUDIO_HLADESIGNATORMANAGER_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <DevStudio/datatypes/AccelerationVectorStruct.h>
#include <DevStudio/datatypes/DeadReckoningAlgorithmEnum.h>
#include <DevStudio/datatypes/DesignatorCodeEnum.h>
#include <DevStudio/datatypes/DesignatorCodeNameEnum.h>
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/RelativePositionStruct.h>
#include <DevStudio/datatypes/WorldLocationStruct.h>
#include <string>

#include <list>
#include <vector>

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaDesignatorManagerListener.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaException.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

   /**
   * Manager used to manage HlaDesignators.
   */
   class HlaDesignatorManager : private boost::noncopyable {

   public:

      LIBAPI virtual ~HlaDesignatorManager() {}

      /**
      * Gets a list of all HlaDesignators within interest, both local and remote.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of HlaDesignators
      */
      LIBAPI virtual std::list<HlaDesignatorPtr> getHlaDesignators() = 0;

      /**
      * Gets a list of all HlaDesignators, both local and remote.
      * HlaDesignators not within interest are also included.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of all HlaDesignators
      */
      LIBAPI virtual std::list<HlaDesignatorPtr> getAllHlaDesignators() = 0;

      /**
      * Gets a list of local HlaDesignators within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of local HlaDesignators
      */
      LIBAPI virtual std::list<HlaDesignatorPtr> getLocalHlaDesignators() = 0;

      /**
      * Gets a list of remote HlaDesignators within interest.
      * The list is a copy that is allowed to be altered.
      *
      * @return a list of remote HlaDesignators
      */
      LIBAPI virtual std::list<HlaDesignatorPtr> getRemoteHlaDesignators() = 0;

      /**
      * Find a HlaDesignator with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> of the HlaDesignator to find
      *
      * @return the specified HlaDesignator, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaDesignatorPtr getDesignatorByHlaInstanceName(const std::wstring& hlaInstanceName) = 0;

      /**
      * Find a HlaDesignator with the given <code>HLA instance name</code>.
      * If none is found, <code>null</code> is returned.
      *
      * @param encodedHlaInstanceHandle the encoded <code>HLA instance handle</code> of the HlaDesignator to find
      *
      * @return the specified HlaDesignator, or <code>null</code> if not found.
      */
      LIBAPI virtual HlaDesignatorPtr getDesignatorByHlaInstanceHandle(const std::vector<char>& encodedHlaInstanceHandle) = 0;

      /**
      * Creates a new HlaDesignator, and sets all <i>Create</i> attributes.
      *
      * @return Returns a new local HlaDesignator.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaDesignatorPtr createLocalHlaDesignator(
      ) THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Creates a new HlaDesignator with a specified <code>HLA instance name</code>, and sets all
      * <i>Create</i> attributes.
      *
      * @param hlaInstanceName the <code>HLA instance name</code> to register for this instance
      *
      * @return Returns a new local HlaDesignator.
      *
      * @throws HlaIllegalInstanceNameException if the name is illegal
      * @throws HlaInstanceNameInUseException if the name is already in use
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaDesignatorPtr createLocalHlaDesignator(
         const std::wstring& hlaInstanceName
      ) THROW_SPEC (HlaIllegalInstanceNameException, HlaInstanceNameInUseException,
                    HlaNotConnectedException, HlaInternalException, HlaRtiException,
                    HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaDesignator and removes it from the federation.
      *
      * @param designator The HlaDesignator to delete
      *
      * @return the HlaDesignator deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaDesignatorPtr deleteLocalHlaDesignator(HlaDesignatorPtr designator)
          THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                      HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaDesignator and removes it from the federation.
      *
      * @param designator The HlaDesignator to delete
      * @param timestamp timestamp when the instance was removed.
      *
      * @return the HlaDesignator deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaDesignatorPtr deleteLocalHlaDesignator(HlaDesignatorPtr designator, HlaTimeStampPtr timestamp)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaDesignator and removes it from the federation.
      *
      * @param designator The HlaDesignator to delete
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaDesignator deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaDesignatorPtr deleteLocalHlaDesignator(HlaDesignatorPtr designator, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Deletes a local HlaDesignator and removes it from the federation.
      *
      * @param designator The HlaDesignator to delete
      * @param timestamp timestamp when the instance was removed.
      * @param logicalTime The logical time for the delete
      *
      * @return the HlaDesignator deleted, null if it didn't exist.
      *
      * @throws HlaNotConnectedException if the federate is not connected
      * @throws HlaInternalException on internal errors
      * @throws HlaRtiException on exception from the RTI
      * @throws HlaInvalidLogicalTimeException on exception from the RTI
      * @throws HlaSaveInProgressException if a federation save is in progress
      * @throws HlaRestoreInProgressException if a federation restore is in progress
      */
      LIBAPI virtual HlaDesignatorPtr deleteLocalHlaDesignator(HlaDesignatorPtr designator, HlaTimeStampPtr timestamp, HlaLogicalTimePtr logicalTime)
      THROW_SPEC (HlaNotConnectedException, HlaInternalException, HlaRtiException, HlaInvalidLogicalTimeException,
                  HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

      /**
      * Add a HlaDesignator manager listener.
      *
      * @param managerListener listener to add
      */
      LIBAPI virtual void addHlaDesignatorManagerListener(HlaDesignatorManagerListenerPtr managerListener) = 0;

      /**
      * Removed a HlaDesignator manager listener.
      *
      * @param managerListener listener to remove
      */
      LIBAPI virtual void removeHlaDesignatorManagerListener(HlaDesignatorManagerListenerPtr managerListener) = 0;

     /**
      * Add a listener that will automatically be added to all instances of HlaDesignator (Both local and remote).
      * The listener is notified when any attribute of an instance of HlaDesignator is updated.
      * The listener is also called when an interaction is sent to an instance of HlaDesignator.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param listener The listener to add.
      */
      LIBAPI virtual void addHlaDesignatorDefaultInstanceListener(HlaDesignatorListenerPtr listener) = 0;

     /**
      * Remove a listener from the set of listeners that are automatically added to new instances of HlaDesignator.
      * Note: The listener will not be removed from already existing instances of HlaDesignator.
      * This method is idempotent.
      *
      * @param listener The listener to remove.
      */
      LIBAPI virtual void removeHlaDesignatorDefaultInstanceListener(HlaDesignatorListenerPtr listener) = 0;

     /**
      * Add a valueListener that will automatically be added to all instances of HlaDesignator (Both local and remote).
      * The  Valuelistener is notified when any attribute of an instance of HlaDesignator is updated.
      * Note that the attribute values that are notified are not guaranteed to
      * be the latest available values for that attribute.
      * All listeners will automatically be removed when an instance is removed.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to add.
      */
      LIBAPI virtual void addHlaDesignatorDefaultInstanceValueListener(HlaDesignatorValueListenerPtr valueListener) = 0;

     /**
      * Remove a valueListener from the set of listeners that are automatically added to new instances of HlaDesignator.
      * Note: The valueListener will not be removed from already existing instances of HlaDesignator.
      * This method is idempotent.
      *
      * @param valueListener The valueListener to remove.
      */
      LIBAPI virtual void removeHlaDesignatorDefaultInstanceValueListener(HlaDesignatorValueListenerPtr valueListener) = 0;

     /**
      * Enables or disables the HlaDesignator manager.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaDesignator manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      *
      * @param enabled <code>true</code> to enable the HlaDesignator manager, otherwise <code>false</code>
      */
      LIBAPI virtual void setEnabled(bool enabled) = 0;

     /**
      * Enables the HlaDesignator manager if it is avalable in the FOM at runtime.
      * The manager is enabled by default. The state can only be changed when not connected.
      * When the HlaDesignator manager is disabled it will act as disconnected,
      * ie throws HlaNotConnectedException.
      */
      LIBAPI virtual void setEnabledIfAvailableInFom() = 0;

     /**
      * Check if the HlaDesignator manager is actually enabled when connected.
      * An HlaDesignator manager may be disabled with the <code>setEnabled(false)</code> function, or it may be configured
      * as <i>if available in the FOM</i> by the <code>setEnabledIfAvailableInFom()</code> function and
      * then not available in the FOM at runtime.
      * <p>
      * This method should only be used when connected, it will always return <code>false</code>
      * when not connected.
      *
      * @return <code>true</code> if connected and the HlaDesignator manager is enabled.
      */
      LIBAPI virtual bool isEnabled() = 0;
   };
}
#endif
