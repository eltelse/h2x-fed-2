/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLASETTINGS_H
#define DEVELOPER_STUDIO_HLASETTINGS_H

#include <string>
#include <map>
#include <vector>
#include <exception>
#include <iostream>

#include <boost/noncopyable.hpp>

#include <RtiDriver/Detective.h>

#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Settings for a HlaWorld.
    * <p>
    * It is recommended to use the regular configuration mechanism of the application to control the HLA configuration.
    * This should be done by overriding this class, and combine that with the usual configuration mechanism.
    * <p>
    * It is possible to override the settings for a HlaWorld with a <code>FederateConfig.txt</code>
    * file in the current directory.
    * <p>
    * A template for FederateConfig.txt:

     <pre><code>
    #
    # HlaWorld template settings file for a Developer Studio generated federate
    #

    # CRC host
    crcHost = 192.168.1.42

    # CRC port
    crcPort = 9000

    # Local Settings Designator for HLA 1516-2010 Evolved
    localSettingsDesignator = MyLSD

    # Federate name
    federateName = Federate

    # Federation name
    federationName = Federation

    # HLA 1516-2000 FOM URL
    classicFomURL = fom.xml

    # Main HLA 1516-2010 Evolved FOM Module URL
    evolvedFomURL = fomModule.xml

    # Additional HLA 1516-2010 Evolved FOM Module URLs
    additionalFomURLs = additionalModule.xml ; additionalModule2.xml

    # HLA 1.3 FED URL
    fedURL = fom.fed

    # Try to create the federation before join
    createFederation = true

    # Try to destroy the federation after resign
    destroyFederation = false

    # RTI Profile to use (empty, A, B, C or D), default when empty is to use the
    # environment variable RTI_DRIVER_PROFILE
    rtiDriverProfile = B

    # Enable HLA Time Management with time constrained
    timeConstrained = true

    # Enable HLA Time Management with time regulating
    timeRegulating = true

    # Step size
    stepSize = 5

    # HLA Time Management lookahead
    lookahead = 5

    # Time stamp factory to use (none, system_clock, rpr_relative or rpr_absolute)
    timeStampFactory = system_clock
    </code></pre>

    */
    class HlaSettings : private boost::noncopyable {
    public:
        /** Enumeration for the different HLA standards */
        enum HlaVersion {
            /** <code>HLA_13</code> (with ordinal 0) */
            HLA_13,
            /** <code>HLA_1516</code> (with ordinal 1) */
            HLA_1516,
            /** <code>HLA_EVOLVED</code> (with ordinal 2) */
            HLA_EVOLVED,
            /** <code>HLA_VERSION_NONE</code> used by DIS profile (with ordinal 3) */
            HLA_VERSION_NONE
        };

        /** Enumeration for different HLA call types */
        enum CallType {
            /** Create Federation Execution */
            CREATE, 
            /** Join Federation Execution */
            JOIN
        };

        /**
        * Creates a HlaSettings with the default settings and then reads the settings file and updates the
        * settings that are specified in the file.
        */
        LIBAPI HlaSettings();

        /**
        * Creates a HlaSettings with the default settings and then reads the settings file and updates the
        * settings that are specified in the file, finally overrides CRC host and CRC port with the
        * values specified here.
        *
        * @param crcHost CRC host
        * @param crcPort CRC port
        */
        LIBAPI HlaSettings(const std::wstring& crcHost, int crcPort);

        LIBAPI virtual ~HlaSettings() {}

        /**
        * Get the CRC host.
        *
        * @return the CRC host
        */
        LIBAPI virtual std::wstring getCrcHost() const {
            return _crcHost;
        }

        /**
        * Get the CRC port.
        *
        * @return the CRC port
        */
        LIBAPI virtual int getCrcPort() const {
            return _crcPort;
        }

        /**
        * Get the Local Settings Designator (LSD) for HLA Evolved.
        * The LSD can be used instead of CRC Host and Port to specify CRC to connect to.
        *
        * Note that the LSD is only used if connecting to HLA Evolved and to use the default LSD (an empty string),
        * the returned CRC host also has to be an empty string.
        *
        * @return the Local Settings Designator (LSD)
        */
        LIBAPI virtual std::wstring getLocalSettingsDesignator() const {
            return _localSettingsDesignator;
        }

        /**
        * Get the Federate name.
        *
        * @return the Federate name
        */
        LIBAPI virtual std::wstring getFederateName() const {
            return _federateName;
        }

        /**
        * Get the Federate type.
        *
        * @return the Federate type
        */
        LIBAPI virtual std::wstring getFederateType() const {
            return _federateType;
        }

        /**
        * Get the Federation name.
        *
        * @return the Federation name
        */
        LIBAPI virtual std::wstring getFederationName() const {
            return _federationName;
        }

        /**
        * Get the FOM URL.
        *
        * @param callType   Type of HLA call
        * @param hlaVersion HLA version of the RTI in use
        *
        * @return the FOM URL
        */
        LIBAPI virtual std::wstring getFomUrl(CallType callType, HlaVersion hlaVersion) const {
            if (hlaVersion == HLA_13) {
                return _fedUrl;
            } else if (hlaVersion == HLA_1516) {
                return _classicFomUrl;
            } else {
                return _evolvedFomUrl;
            }
        }

        /**
         * Get additional FOM Module URLs.
         *
         * @param callType   Type of HLA call
         * @param hlaVersion HLA version of the RTI in use
         *
         * @return the FOM URLs
         */
        LIBAPI virtual std::vector<std::wstring> getAdditionalFomUrls(CallType callType, HlaVersion hlaVersion) const {
            return _additionalFomUrls;
        }

        /**
        * Get if the Federation should be created before join.
        *
        * @return <code>true</code> if the Federation should be created before join
        */
        LIBAPI virtual bool getCreateFederation() const {
            return _createFederation;
        }

        /**
        * Get if the Federation should be destroyed after resign.
        *
        * @return <code>true</code> if the Federation should be destroyed after resign
        */
        LIBAPI virtual bool getDestroyFederation() const {
            return _destroyFederation;
        }

        /**
        * Get the RTI Driver Profile name.
        *
        * @return The RTI Driver Profile name.
        */
        LIBAPI virtual std::wstring getRtiDriverProfile() const {
            return _rtiDriverProfile;
        }

        /**
        * Get the RTI Driver Detectives.
        *
        * <b>The ability to attach detectives must be used with caution. Misbehaving detectives can
        * cause the entire HlaWorld to misbehave or even crash.</b>
        *
        * Note that the detectives will be installed in reverse order:
        * <pre>
        *    Federate -> Detective(count = n) -> ... -> Detective(count = 2) -> Detective(count = 1) -> RTI
        * </pre>
        *
        * @param count call count, starts at 1 and will be incremented for each call.
        *
        * @return The RTI Driver Detective, use <code>null</code> to indicate <i>no more detectives</i>
        */
        LIBAPI virtual std::auto_ptr<RtiDriver::Detective> getRtiDriverDetective(int count) {
            return std::auto_ptr<RtiDriver::Detective>((RtiDriver::Detective*)NULL);
        }

        /**
        * Check if a settings file has been defined that should override the user supplied settings.
        *
        * @return <code>True</code> if a settings file has been defined, <code>false</code> othervise.
        */
        LIBAPI virtual bool hasSettingsFile() const {
            return LOADED_DEFAULT_SETTINGS;
        }

        /**
        * Get the time stamp factory name.
        *
        * @return The time stamp factory name.
        */
        LIBAPI virtual std::wstring getTimeStampFactoryName() const {
            return _timeStampFactory;
        }

        /**
        * Help function for HlaTuning.
        *
        * @return the setting corresponding to the specific name <code>setting</code>. Returns the empty string if the
        * setting is not found.
        */
        LIBAPI virtual std::string getSetting(std::string setting) const;

        /**
        * Get if this federate should be time constrained using <code>HLA Time Management</code>.
        *
        * @return <code>true</code> if this federate should be time constrained
        */
        LIBAPI virtual bool getTimeConstrained() const {
            return _timeConstrained;
        }

        /**
        * Get if this federate should be time regulating using <code>HLA Time Management</code>.
        *
        * @return <code>true</code> if this federate should be time regulating
        */
        LIBAPI virtual bool getTimeRegulating() const {
            return _timeRegulating;
        }

        /**
        * Get the step size for this federate.
        *
        * @return the step size for this federate
        */
        LIBAPI virtual long long getStepSize(HlaLogicalTimePtr nextLogicalTime) {
            return _stepSize;
        }

        /**
        * Set the step size for this federate.
        */
        LIBAPI virtual void setStepSize(long long stepSize) {
            _stepSize = stepSize;
        }

        /**
        * Get the <code>HLA Time Management</code> lookahead for this federate.
        *
        * @return the lookahead for this federate
        */
        LIBAPI virtual long long getLookahead(HlaLogicalTimePtr nextLogicalTime) {
            return _lookahead;
        }

        /**
        * Set the <code>HLA Time Management</code> lookahead for this federate.
        */
        LIBAPI virtual void setLookahead(long long lookahead) {
            _lookahead = lookahead;
        }

    protected:
        const char* const _settingsFileUrl; /**< The Url to the settings file */
        std::wstring _crcHost; /**< CRC Host, i.e. localhost */
        int _crcPort; /**< CRC Port, i.e. 8989 */
        std::wstring _localSettingsDesignator; /**< Local Settings Designator (LSD) for HLA 1516-2010 Evolved */
        std::wstring _federateName; /**< Name of the federate */
        std::wstring _federateType; /**< The type of federate */
        std::wstring _federationName; /**< Name of the federation */
        std::wstring _classicFomUrl; /**< The Url to the HLA 1516-2000 FOM */
        std::wstring _evolvedFomUrl; /**< The Url to the Base HLA 1516-2010 Evolved FOM module */
        std::vector<std::wstring> _additionalFomUrls; /**< The Urls to the additional HLA 1516-2010 Evolved FOM Modules */
        std::wstring _fedUrl; /**< The Url to the HLA 1.3 FED file */
        bool _createFederation; /**< True if the Federation should be created before join. */
        bool _destroyFederation; /**< True if the Federation should be destroyed after resign. */
        std::wstring _rtiDriverProfile; /**< The RTI Driver Profile. */
        std::wstring _timeStampFactory; /**< The time stamp factory name. */
        long long _stepSize;  /**< The step size for time management */
        long long _lookahead; /**< The lookahead for time management */
        bool _timeRegulating; /**< If the federate is time regulating */
        bool _timeConstrained; /**< If the federate is time constrained */



    private:
        std::map<std::string, std::string> _variables;
        const bool LOADED_DEFAULT_SETTINGS; /**< Loaded default settings */

        void addVariableFromString(std::string& line);
        bool fileSettingsVariableExists(const std::string variableName);
        void loadSettings();
        bool loadDefaultSettings();
    };
}
#endif
