/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLASUBMERSIBLEVESSELATTRIBUTES_H
#define DEVELOPER_STUDIO_HLASUBMERSIBLEVESSELATTRIBUTES_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <string>
#include <vector>
#include <utility>
    
#include <boost/noncopyable.hpp>
    
#include <DevStudio/datatypes/ArticulatedParameterStruct.h>
#include <DevStudio/datatypes/ArticulatedParameterStructLengthlessArray.h>
#include <DevStudio/datatypes/CamouflageEnum.h>
#include <DevStudio/datatypes/DamageStatusEnum.h>
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EntityTypeStruct.h>
#include <DevStudio/datatypes/ForceIdentifierEnum.h>
#include <DevStudio/datatypes/HatchStateEnum.h>
#include <DevStudio/datatypes/IsPartOfStruct.h>
#include <DevStudio/datatypes/MarkingStruct.h>
#include <DevStudio/datatypes/PropulsionSystemDataStruct.h>
#include <DevStudio/datatypes/PropulsionSystemDataStructLengthlessArray.h>
#include <DevStudio/datatypes/SpatialVariantStruct.h>
#include <DevStudio/datatypes/TrailingEffectsCodeEnum.h>
#include <DevStudio/datatypes/VectoringNozzleSystemDataStruct.h>
#include <DevStudio/datatypes/VectoringNozzleSystemDataStructLengthlessArray.h>
#include <vector>

#include <DevStudio/HlaException.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaPlatformAttributes.h>
#include <DevStudio/HlaTimeStamped.h>

namespace DevStudio {

   /**
   * Interface used to represent all attributes for an object instance.
   */
   class HlaSubmersibleVesselAttributes : public HlaPlatformAttributes {
   public:


     /**
      * An enumeration of the attributes of an HlaSubmersibleVessel
      *
      *<table summary="All attributes">
      * <tr><td><b>Enum constant</b></td><td><b>C++ name</b></td><td><b>FOM name</b></td></tr>
      * <tr><td>AFTERBURNER_ON</td><td>afterburnerOn</td><td><code>AfterburnerOn</code></td></tr>
      * <tr><td>ANTI_COLLISION_LIGHTS_ON</td><td>antiCollisionLightsOn</td><td><code>AntiCollisionLightsOn</code></td></tr>
      * <tr><td>BLACK_OUT_BRAKE_LIGHTS_ON</td><td>blackOutBrakeLightsOn</td><td><code>BlackOutBrakeLightsOn</code></td></tr>
      * <tr><td>BLACK_OUT_LIGHTS_ON</td><td>blackOutLightsOn</td><td><code>BlackOutLightsOn</code></td></tr>
      * <tr><td>BRAKE_LIGHTS_ON</td><td>brakeLightsOn</td><td><code>BrakeLightsOn</code></td></tr>
      * <tr><td>FORMATION_LIGHTS_ON</td><td>formationLightsOn</td><td><code>FormationLightsOn</code></td></tr>
      * <tr><td>HATCH_STATE</td><td>hatchState</td><td><code>HatchState</code></td></tr>
      * <tr><td>HEAD_LIGHTS_ON</td><td>headLightsOn</td><td><code>HeadLightsOn</code></td></tr>
      * <tr><td>INTERIOR_LIGHTS_ON</td><td>interiorLightsOn</td><td><code>InteriorLightsOn</code></td></tr>
      * <tr><td>LANDING_LIGHTS_ON</td><td>landingLightsOn</td><td><code>LandingLightsOn</code></td></tr>
      * <tr><td>LAUNCHER_RAISED</td><td>launcherRaised</td><td><code>LauncherRaised</code></td></tr>
      * <tr><td>NAVIGATION_LIGHTS_ON</td><td>navigationLightsOn</td><td><code>NavigationLightsOn</code></td></tr>
      * <tr><td>RAMP_DEPLOYED</td><td>rampDeployed</td><td><code>RampDeployed</code></td></tr>
      * <tr><td>RUNNING_LIGHTS_ON</td><td>runningLightsOn</td><td><code>RunningLightsOn</code></td></tr>
      * <tr><td>SPOT_LIGHTS_ON</td><td>spotLightsOn</td><td><code>SpotLightsOn</code></td></tr>
      * <tr><td>TAIL_LIGHTS_ON</td><td>tailLightsOn</td><td><code>TailLightsOn</code></td></tr>
      * <tr><td>ACOUSTIC_SIGNATURE_INDEX</td><td>acousticSignatureIndex</td><td><code>AcousticSignatureIndex</code></td></tr>
      * <tr><td>ALTERNATE_ENTITY_TYPE</td><td>alternateEntityType</td><td><code>AlternateEntityType</code></td></tr>
      * <tr><td>ARTICULATED_PARAMETERS_ARRAY</td><td>articulatedParametersArray</td><td><code>ArticulatedParametersArray</code></td></tr>
      * <tr><td>CAMOUFLAGE_TYPE</td><td>camouflageType</td><td><code>CamouflageType</code></td></tr>
      * <tr><td>DAMAGE_STATE</td><td>damageState</td><td><code>DamageState</code></td></tr>
      * <tr><td>ENGINE_SMOKE_ON</td><td>engineSmokeOn</td><td><code>EngineSmokeOn</code></td></tr>
      * <tr><td>FIRE_POWER_DISABLED</td><td>firePowerDisabled</td><td><code>FirePowerDisabled</code></td></tr>
      * <tr><td>FLAMES_PRESENT</td><td>flamesPresent</td><td><code>FlamesPresent</code></td></tr>
      * <tr><td>FORCE_IDENTIFIER</td><td>forceIdentifier</td><td><code>ForceIdentifier</code></td></tr>
      * <tr><td>HAS_AMMUNITION_SUPPLY_CAP</td><td>hasAmmunitionSupplyCap</td><td><code>HasAmmunitionSupplyCap</code></td></tr>
      * <tr><td>HAS_FUEL_SUPPLY_CAP</td><td>hasFuelSupplyCap</td><td><code>HasFuelSupplyCap</code></td></tr>
      * <tr><td>HAS_RECOVERY_CAP</td><td>hasRecoveryCap</td><td><code>HasRecoveryCap</code></td></tr>
      * <tr><td>HAS_REPAIR_CAP</td><td>hasRepairCap</td><td><code>HasRepairCap</code></td></tr>
      * <tr><td>IMMOBILIZED</td><td>immobilized</td><td><code>Immobilized</code></td></tr>
      * <tr><td>INFRARED_SIGNATURE_INDEX</td><td>infraredSignatureIndex</td><td><code>InfraredSignatureIndex</code></td></tr>
      * <tr><td>IS_CONCEALED</td><td>isConcealed</td><td><code>IsConcealed</code></td></tr>
      * <tr><td>LIVE_ENTITY_MEASURED_SPEED</td><td>liveEntityMeasuredSpeed</td><td><code>LiveEntityMeasuredSpeed</code></td></tr>
      * <tr><td>MARKING</td><td>marking</td><td><code>Marking</code></td></tr>
      * <tr><td>POWER_PLANT_ON</td><td>powerPlantOn</td><td><code>PowerPlantOn</code></td></tr>
      * <tr><td>PROPULSION_SYSTEMS_DATA</td><td>propulsionSystemsData</td><td><code>PropulsionSystemsData</code></td></tr>
      * <tr><td>RADAR_CROSS_SECTION_SIGNATURE_INDEX</td><td>radarCrossSectionSignatureIndex</td><td><code>RadarCrossSectionSignatureIndex</code></td></tr>
      * <tr><td>SMOKE_PLUME_PRESENT</td><td>smokePlumePresent</td><td><code>SmokePlumePresent</code></td></tr>
      * <tr><td>TENT_DEPLOYED</td><td>tentDeployed</td><td><code>TentDeployed</code></td></tr>
      * <tr><td>TRAILING_EFFECTS_CODE</td><td>trailingEffectsCode</td><td><code>TrailingEffectsCode</code></td></tr>
      * <tr><td>VECTORING_NOZZLE_SYSTEM_DATA</td><td>vectoringNozzleSystemData</td><td><code>VectoringNozzleSystemData</code></td></tr>
      * <tr><td>ENTITY_TYPE</td><td>entityType</td><td><code>EntityType</code></td></tr>
      * <tr><td>ENTITY_IDENTIFIER</td><td>entityIdentifier</td><td><code>EntityIdentifier</code></td></tr>
      * <tr><td>IS_PART_OF</td><td>isPartOf</td><td><code>IsPartOf</code></td></tr>
      * <tr><td>SPATIAL</td><td>spatial</td><td><code>Spatial</code></td></tr>
      * <tr><td>RELATIVE_SPATIAL</td><td>relativeSpatial</td><td><code>RelativeSpatial</code></td></tr>
      *</table>
      */
      enum Attribute {
        /**
        * afterburnerOn (FOM name: <code>AfterburnerOn</code>).
        * <br>Description from the FOM: <i>Whether the entity's afterburner is on or not.</i>
        */
         AFTERBURNER_ON,

        /**
        * antiCollisionLightsOn (FOM name: <code>AntiCollisionLightsOn</code>).
        * <br>Description from the FOM: <i>Whether the entity's anti-collision lights are on or not.</i>
        */
         ANTI_COLLISION_LIGHTS_ON,

        /**
        * blackOutBrakeLightsOn (FOM name: <code>BlackOutBrakeLightsOn</code>).
        * <br>Description from the FOM: <i>Whether the entity's black out brake lights are on or not.</i>
        */
         BLACK_OUT_BRAKE_LIGHTS_ON,

        /**
        * blackOutLightsOn (FOM name: <code>BlackOutLightsOn</code>).
        * <br>Description from the FOM: <i>Whether the entity's black out lights are on or not.</i>
        */
         BLACK_OUT_LIGHTS_ON,

        /**
        * brakeLightsOn (FOM name: <code>BrakeLightsOn</code>).
        * <br>Description from the FOM: <i>Whether the entity's brake lights are on or not.</i>
        */
         BRAKE_LIGHTS_ON,

        /**
        * formationLightsOn (FOM name: <code>FormationLightsOn</code>).
        * <br>Description from the FOM: <i>Whether the entity's formation lights are on or not.</i>
        */
         FORMATION_LIGHTS_ON,

        /**
        * hatchState (FOM name: <code>HatchState</code>).
        * <br>Description from the FOM: <i>The state of the entity's (main) hatch.</i>
        */
         HATCH_STATE,

        /**
        * headLightsOn (FOM name: <code>HeadLightsOn</code>).
        * <br>Description from the FOM: <i>Whether the entity's headlights are on or not.</i>
        */
         HEAD_LIGHTS_ON,

        /**
        * interiorLightsOn (FOM name: <code>InteriorLightsOn</code>).
        * <br>Description from the FOM: <i>Whether the entity's internal lights are on or not.</i>
        */
         INTERIOR_LIGHTS_ON,

        /**
        * landingLightsOn (FOM name: <code>LandingLightsOn</code>).
        * <br>Description from the FOM: <i>Whether the entity's landing lights are on or not.</i>
        */
         LANDING_LIGHTS_ON,

        /**
        * launcherRaised (FOM name: <code>LauncherRaised</code>).
        * <br>Description from the FOM: <i>Whether the entity's weapon launcher is in the raised position.</i>
        */
         LAUNCHER_RAISED,

        /**
        * navigationLightsOn (FOM name: <code>NavigationLightsOn</code>).
        * <br>Description from the FOM: <i>Whether the entity's navigation lights are on or not.</i>
        */
         NAVIGATION_LIGHTS_ON,

        /**
        * rampDeployed (FOM name: <code>RampDeployed</code>).
        * <br>Description from the FOM: <i>Whether the entity has deployed a ramp or not.</i>
        */
         RAMP_DEPLOYED,

        /**
        * runningLightsOn (FOM name: <code>RunningLightsOn</code>).
        * <br>Description from the FOM: <i>Whether the entity's running lights are on or not.</i>
        */
         RUNNING_LIGHTS_ON,

        /**
        * spotLightsOn (FOM name: <code>SpotLightsOn</code>).
        * <br>Description from the FOM: <i>Whether the entity's spotlights are on or not.</i>
        */
         SPOT_LIGHTS_ON,

        /**
        * tailLightsOn (FOM name: <code>TailLightsOn</code>).
        * <br>Description from the FOM: <i>Whether the entity's tail lights are on or not.</i>
        */
         TAIL_LIGHTS_ON,

        /**
        * acousticSignatureIndex (FOM name: <code>AcousticSignatureIndex</code>).
        * <br>Description from the FOM: <i>Index used to obtain the acoustics (sound through air) signature state of the entity.</i>
        */
         ACOUSTIC_SIGNATURE_INDEX,

        /**
        * alternateEntityType (FOM name: <code>AlternateEntityType</code>).
        * <br>Description from the FOM: <i>The category of entity to be used when viewed by entities on the 'opposite' side.</i>
        */
         ALTERNATE_ENTITY_TYPE,

        /**
        * articulatedParametersArray (FOM name: <code>ArticulatedParametersArray</code>).
        * <br>Description from the FOM: <i>Identification of the visible parts, and their states, of the entity which are capable of independent motion.</i>
        */
         ARTICULATED_PARAMETERS_ARRAY,

        /**
        * camouflageType (FOM name: <code>CamouflageType</code>).
        * <br>Description from the FOM: <i>The type of camouflage in use (if any).</i>
        */
         CAMOUFLAGE_TYPE,

        /**
        * damageState (FOM name: <code>DamageState</code>).
        * <br>Description from the FOM: <i>The state of damage of the entity.</i>
        */
         DAMAGE_STATE,

        /**
        * engineSmokeOn (FOM name: <code>EngineSmokeOn</code>).
        * <br>Description from the FOM: <i>Whether the entity's engine is generating smoke or not.</i>
        */
         ENGINE_SMOKE_ON,

        /**
        * firePowerDisabled (FOM name: <code>FirePowerDisabled</code>).
        * <br>Description from the FOM: <i>Whether the entity's main weapon system has been disabled or not.</i>
        */
         FIRE_POWER_DISABLED,

        /**
        * flamesPresent (FOM name: <code>FlamesPresent</code>).
        * <br>Description from the FOM: <i>Whether the entity is on fire (with visible flames) or not.</i>
        */
         FLAMES_PRESENT,

        /**
        * forceIdentifier (FOM name: <code>ForceIdentifier</code>).
        * <br>Description from the FOM: <i>The identification of the force that the entity belongs to.</i>
        */
         FORCE_IDENTIFIER,

        /**
        * hasAmmunitionSupplyCap (FOM name: <code>HasAmmunitionSupplyCap</code>).
        * <br>Description from the FOM: <i>Whether the entity has the capability to supply other entities with ammunition.</i>
        */
         HAS_AMMUNITION_SUPPLY_CAP,

        /**
        * hasFuelSupplyCap (FOM name: <code>HasFuelSupplyCap</code>).
        * <br>Description from the FOM: <i>Whether the entity has the capability to supply other entities with fuel or not.</i>
        */
         HAS_FUEL_SUPPLY_CAP,

        /**
        * hasRecoveryCap (FOM name: <code>HasRecoveryCap</code>).
        * <br>Description from the FOM: <i>Whether the entity has the capability to recover other entities or not.</i>
        */
         HAS_RECOVERY_CAP,

        /**
        * hasRepairCap (FOM name: <code>HasRepairCap</code>).
        * <br>Description from the FOM: <i>Whether the entity has the capability to repair other entities or not.</i>
        */
         HAS_REPAIR_CAP,

        /**
        * immobilized (FOM name: <code>Immobilized</code>).
        * <br>Description from the FOM: <i>Whether the entity is immobilized or not.</i>
        */
         IMMOBILIZED,

        /**
        * infraredSignatureIndex (FOM name: <code>InfraredSignatureIndex</code>).
        * <br>Description from the FOM: <i>Index used to obtain the infra-red signature state of the entity.</i>
        */
         INFRARED_SIGNATURE_INDEX,

        /**
        * isConcealed (FOM name: <code>IsConcealed</code>).
        * <br>Description from the FOM: <i>Whether the entity is concealed or not.</i>
        */
         IS_CONCEALED,

        /**
        * liveEntityMeasuredSpeed (FOM name: <code>LiveEntityMeasuredSpeed</code>).
        * <br>Description from the FOM: <i>The entity's own measurement of speed (e.g. air speed for aircraft).</i>
        */
         LIVE_ENTITY_MEASURED_SPEED,

        /**
        * marking (FOM name: <code>Marking</code>).
        * <br>Description from the FOM: <i>A unique marking or combination of characters used to distinguish the entity from other entities.</i>
        */
         MARKING,

        /**
        * powerPlantOn (FOM name: <code>PowerPlantOn</code>).
        * <br>Description from the FOM: <i>Whether the entity's power plant is on or not.</i>
        */
         POWER_PLANT_ON,

        /**
        * propulsionSystemsData (FOM name: <code>PropulsionSystemsData</code>).
        * <br>Description from the FOM: <i>The basic operating data of the propulsion systems aboard the entity.</i>
        */
         PROPULSION_SYSTEMS_DATA,

        /**
        * radarCrossSectionSignatureIndex (FOM name: <code>RadarCrossSectionSignatureIndex</code>).
        * <br>Description from the FOM: <i>Index used to obtain the radar cross section signature state of the entity.</i>
        */
         RADAR_CROSS_SECTION_SIGNATURE_INDEX,

        /**
        * smokePlumePresent (FOM name: <code>SmokePlumePresent</code>).
        * <br>Description from the FOM: <i>Whether the entity is generating smoke or not (intentional or unintentional).</i>
        */
         SMOKE_PLUME_PRESENT,

        /**
        * tentDeployed (FOM name: <code>TentDeployed</code>).
        * <br>Description from the FOM: <i>Whether the entity has deployed tent or not.</i>
        */
         TENT_DEPLOYED,

        /**
        * trailingEffectsCode (FOM name: <code>TrailingEffectsCode</code>).
        * <br>Description from the FOM: <i>The type and size of any trail that the entity is making.</i>
        */
         TRAILING_EFFECTS_CODE,

        /**
        * vectoringNozzleSystemData (FOM name: <code>VectoringNozzleSystemData</code>).
        * <br>Description from the FOM: <i>The basic operational data for the vectoring nozzle systems aboard the entity.</i>
        */
         VECTORING_NOZZLE_SYSTEM_DATA,

        /**
        * entityType (FOM name: <code>EntityType</code>).
        * <br>Description from the FOM: <i>The category of the entity.</i>
        */
         ENTITY_TYPE,

        /**
        * entityIdentifier (FOM name: <code>EntityIdentifier</code>).
        * <br>Description from the FOM: <i>The unique identifier for the entity instance.</i>
        */
         ENTITY_IDENTIFIER,

        /**
        * isPartOf (FOM name: <code>IsPartOf</code>).
        * <br>Description from the FOM: <i>Defines if the entity if a constituent part of another entity (denoted the host entity). If the entity is a constituent part of another entity then the HostEntityIdentifier shall be set to the EntityIdentifier of the host entity and the HostRTIObjectIdentifier shall be set to the RTI object instance ID of the host entity. If the entity is not a constituent part of another entity then the HostEntityIdentifier shall be set to 0.0.0 and the HostRTIObjectIdentifier shall be set to the empty string.</i>
        */
         IS_PART_OF,

        /**
        * spatial (FOM name: <code>Spatial</code>).
        * <br>Description from the FOM: <i>Spatial state stored in one variant record attribute.</i>
        */
         SPATIAL,

        /**
        * relativeSpatial (FOM name: <code>RelativeSpatial</code>).
        * <br>Description from the FOM: <i>Relative spatial state stored in one variant record attribute.</i>
        */
         RELATIVE_SPATIAL
      };

     /**
      * Gets the name of the attribute.
      *
      * @return The name of the attribute. An empty string will be returned if the attribute does not exist.
      */
      LIBAPI virtual std::wstring getName(Attribute attribute);

     /**
      * Finds the attribute specified in the parameter attributeName.
      * The found enumeration will be stored in the parameter attribute.
      *
      * @return true if the attribute was found, false otherwise.
      */
      LIBAPI virtual bool find(Attribute& attribute, std::wstring attributeName);

      LIBAPI virtual ~HlaSubmersibleVesselAttributes() {}
    
      /**
      * Returns true if the <code>afterburnerOn</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Whether the entity's afterburner is on or not.</i>
      *
      * @return true if <code>afterburnerOn</code> is available.
      */
      LIBAPI virtual bool hasAfterburnerOn() = 0;

      /**
      * Gets the value of the <code>afterburnerOn</code> attribute.
      *
      * <br>Description from the FOM: <i>Whether the entity's afterburner is on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>afterburnerOn</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual bool getAfterburnerOn()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>afterburnerOn</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Whether the entity's afterburner is on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>afterburnerOn</code> attribute.
      */
      LIBAPI virtual bool getAfterburnerOn(bool defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>afterburnerOn</code> attribute.
      * <br>Description from the FOM: <i>Whether the entity's afterburner is on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>afterburnerOn</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< bool > getAfterburnerOnTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>antiCollisionLightsOn</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Whether the entity's anti-collision lights are on or not.</i>
      *
      * @return true if <code>antiCollisionLightsOn</code> is available.
      */
      LIBAPI virtual bool hasAntiCollisionLightsOn() = 0;

      /**
      * Gets the value of the <code>antiCollisionLightsOn</code> attribute.
      *
      * <br>Description from the FOM: <i>Whether the entity's anti-collision lights are on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>antiCollisionLightsOn</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual bool getAntiCollisionLightsOn()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>antiCollisionLightsOn</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Whether the entity's anti-collision lights are on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>antiCollisionLightsOn</code> attribute.
      */
      LIBAPI virtual bool getAntiCollisionLightsOn(bool defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>antiCollisionLightsOn</code> attribute.
      * <br>Description from the FOM: <i>Whether the entity's anti-collision lights are on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>antiCollisionLightsOn</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< bool > getAntiCollisionLightsOnTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>blackOutBrakeLightsOn</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Whether the entity's black out brake lights are on or not.</i>
      *
      * @return true if <code>blackOutBrakeLightsOn</code> is available.
      */
      LIBAPI virtual bool hasBlackOutBrakeLightsOn() = 0;

      /**
      * Gets the value of the <code>blackOutBrakeLightsOn</code> attribute.
      *
      * <br>Description from the FOM: <i>Whether the entity's black out brake lights are on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>blackOutBrakeLightsOn</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual bool getBlackOutBrakeLightsOn()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>blackOutBrakeLightsOn</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Whether the entity's black out brake lights are on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>blackOutBrakeLightsOn</code> attribute.
      */
      LIBAPI virtual bool getBlackOutBrakeLightsOn(bool defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>blackOutBrakeLightsOn</code> attribute.
      * <br>Description from the FOM: <i>Whether the entity's black out brake lights are on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>blackOutBrakeLightsOn</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< bool > getBlackOutBrakeLightsOnTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>blackOutLightsOn</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Whether the entity's black out lights are on or not.</i>
      *
      * @return true if <code>blackOutLightsOn</code> is available.
      */
      LIBAPI virtual bool hasBlackOutLightsOn() = 0;

      /**
      * Gets the value of the <code>blackOutLightsOn</code> attribute.
      *
      * <br>Description from the FOM: <i>Whether the entity's black out lights are on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>blackOutLightsOn</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual bool getBlackOutLightsOn()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>blackOutLightsOn</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Whether the entity's black out lights are on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>blackOutLightsOn</code> attribute.
      */
      LIBAPI virtual bool getBlackOutLightsOn(bool defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>blackOutLightsOn</code> attribute.
      * <br>Description from the FOM: <i>Whether the entity's black out lights are on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>blackOutLightsOn</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< bool > getBlackOutLightsOnTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>brakeLightsOn</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Whether the entity's brake lights are on or not.</i>
      *
      * @return true if <code>brakeLightsOn</code> is available.
      */
      LIBAPI virtual bool hasBrakeLightsOn() = 0;

      /**
      * Gets the value of the <code>brakeLightsOn</code> attribute.
      *
      * <br>Description from the FOM: <i>Whether the entity's brake lights are on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>brakeLightsOn</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual bool getBrakeLightsOn()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>brakeLightsOn</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Whether the entity's brake lights are on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>brakeLightsOn</code> attribute.
      */
      LIBAPI virtual bool getBrakeLightsOn(bool defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>brakeLightsOn</code> attribute.
      * <br>Description from the FOM: <i>Whether the entity's brake lights are on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>brakeLightsOn</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< bool > getBrakeLightsOnTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>formationLightsOn</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Whether the entity's formation lights are on or not.</i>
      *
      * @return true if <code>formationLightsOn</code> is available.
      */
      LIBAPI virtual bool hasFormationLightsOn() = 0;

      /**
      * Gets the value of the <code>formationLightsOn</code> attribute.
      *
      * <br>Description from the FOM: <i>Whether the entity's formation lights are on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>formationLightsOn</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual bool getFormationLightsOn()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>formationLightsOn</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Whether the entity's formation lights are on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>formationLightsOn</code> attribute.
      */
      LIBAPI virtual bool getFormationLightsOn(bool defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>formationLightsOn</code> attribute.
      * <br>Description from the FOM: <i>Whether the entity's formation lights are on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>formationLightsOn</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< bool > getFormationLightsOnTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>hatchState</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The state of the entity's (main) hatch.</i>
      *
      * @return true if <code>hatchState</code> is available.
      */
      LIBAPI virtual bool hasHatchState() = 0;

      /**
      * Gets the value of the <code>hatchState</code> attribute.
      *
      * <br>Description from the FOM: <i>The state of the entity's (main) hatch.</i>
      * <br>Description of the data type from the FOM: <i>Hatch state</i>
      *
      * @return the <code>hatchState</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HatchStateEnum::HatchStateEnum getHatchState()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>hatchState</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The state of the entity's (main) hatch.</i>
      * <br>Description of the data type from the FOM: <i>Hatch state</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>hatchState</code> attribute.
      */
      LIBAPI virtual DevStudio::HatchStateEnum::HatchStateEnum getHatchState(DevStudio::HatchStateEnum::HatchStateEnum defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>hatchState</code> attribute.
      * <br>Description from the FOM: <i>The state of the entity's (main) hatch.</i>
      * <br>Description of the data type from the FOM: <i>Hatch state</i>
      *
      * @return the time stamped <code>hatchState</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::HatchStateEnum::HatchStateEnum > getHatchStateTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>headLightsOn</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Whether the entity's headlights are on or not.</i>
      *
      * @return true if <code>headLightsOn</code> is available.
      */
      LIBAPI virtual bool hasHeadLightsOn() = 0;

      /**
      * Gets the value of the <code>headLightsOn</code> attribute.
      *
      * <br>Description from the FOM: <i>Whether the entity's headlights are on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>headLightsOn</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual bool getHeadLightsOn()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>headLightsOn</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Whether the entity's headlights are on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>headLightsOn</code> attribute.
      */
      LIBAPI virtual bool getHeadLightsOn(bool defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>headLightsOn</code> attribute.
      * <br>Description from the FOM: <i>Whether the entity's headlights are on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>headLightsOn</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< bool > getHeadLightsOnTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>interiorLightsOn</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Whether the entity's internal lights are on or not.</i>
      *
      * @return true if <code>interiorLightsOn</code> is available.
      */
      LIBAPI virtual bool hasInteriorLightsOn() = 0;

      /**
      * Gets the value of the <code>interiorLightsOn</code> attribute.
      *
      * <br>Description from the FOM: <i>Whether the entity's internal lights are on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>interiorLightsOn</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual bool getInteriorLightsOn()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>interiorLightsOn</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Whether the entity's internal lights are on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>interiorLightsOn</code> attribute.
      */
      LIBAPI virtual bool getInteriorLightsOn(bool defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>interiorLightsOn</code> attribute.
      * <br>Description from the FOM: <i>Whether the entity's internal lights are on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>interiorLightsOn</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< bool > getInteriorLightsOnTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>landingLightsOn</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Whether the entity's landing lights are on or not.</i>
      *
      * @return true if <code>landingLightsOn</code> is available.
      */
      LIBAPI virtual bool hasLandingLightsOn() = 0;

      /**
      * Gets the value of the <code>landingLightsOn</code> attribute.
      *
      * <br>Description from the FOM: <i>Whether the entity's landing lights are on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>landingLightsOn</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual bool getLandingLightsOn()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>landingLightsOn</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Whether the entity's landing lights are on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>landingLightsOn</code> attribute.
      */
      LIBAPI virtual bool getLandingLightsOn(bool defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>landingLightsOn</code> attribute.
      * <br>Description from the FOM: <i>Whether the entity's landing lights are on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>landingLightsOn</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< bool > getLandingLightsOnTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>launcherRaised</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Whether the entity's weapon launcher is in the raised position.</i>
      *
      * @return true if <code>launcherRaised</code> is available.
      */
      LIBAPI virtual bool hasLauncherRaised() = 0;

      /**
      * Gets the value of the <code>launcherRaised</code> attribute.
      *
      * <br>Description from the FOM: <i>Whether the entity's weapon launcher is in the raised position.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>launcherRaised</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual bool getLauncherRaised()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>launcherRaised</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Whether the entity's weapon launcher is in the raised position.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>launcherRaised</code> attribute.
      */
      LIBAPI virtual bool getLauncherRaised(bool defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>launcherRaised</code> attribute.
      * <br>Description from the FOM: <i>Whether the entity's weapon launcher is in the raised position.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>launcherRaised</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< bool > getLauncherRaisedTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>navigationLightsOn</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Whether the entity's navigation lights are on or not.</i>
      *
      * @return true if <code>navigationLightsOn</code> is available.
      */
      LIBAPI virtual bool hasNavigationLightsOn() = 0;

      /**
      * Gets the value of the <code>navigationLightsOn</code> attribute.
      *
      * <br>Description from the FOM: <i>Whether the entity's navigation lights are on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>navigationLightsOn</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual bool getNavigationLightsOn()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>navigationLightsOn</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Whether the entity's navigation lights are on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>navigationLightsOn</code> attribute.
      */
      LIBAPI virtual bool getNavigationLightsOn(bool defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>navigationLightsOn</code> attribute.
      * <br>Description from the FOM: <i>Whether the entity's navigation lights are on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>navigationLightsOn</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< bool > getNavigationLightsOnTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>rampDeployed</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Whether the entity has deployed a ramp or not.</i>
      *
      * @return true if <code>rampDeployed</code> is available.
      */
      LIBAPI virtual bool hasRampDeployed() = 0;

      /**
      * Gets the value of the <code>rampDeployed</code> attribute.
      *
      * <br>Description from the FOM: <i>Whether the entity has deployed a ramp or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>rampDeployed</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual bool getRampDeployed()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>rampDeployed</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Whether the entity has deployed a ramp or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>rampDeployed</code> attribute.
      */
      LIBAPI virtual bool getRampDeployed(bool defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>rampDeployed</code> attribute.
      * <br>Description from the FOM: <i>Whether the entity has deployed a ramp or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>rampDeployed</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< bool > getRampDeployedTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>runningLightsOn</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Whether the entity's running lights are on or not.</i>
      *
      * @return true if <code>runningLightsOn</code> is available.
      */
      LIBAPI virtual bool hasRunningLightsOn() = 0;

      /**
      * Gets the value of the <code>runningLightsOn</code> attribute.
      *
      * <br>Description from the FOM: <i>Whether the entity's running lights are on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>runningLightsOn</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual bool getRunningLightsOn()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>runningLightsOn</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Whether the entity's running lights are on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>runningLightsOn</code> attribute.
      */
      LIBAPI virtual bool getRunningLightsOn(bool defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>runningLightsOn</code> attribute.
      * <br>Description from the FOM: <i>Whether the entity's running lights are on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>runningLightsOn</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< bool > getRunningLightsOnTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>spotLightsOn</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Whether the entity's spotlights are on or not.</i>
      *
      * @return true if <code>spotLightsOn</code> is available.
      */
      LIBAPI virtual bool hasSpotLightsOn() = 0;

      /**
      * Gets the value of the <code>spotLightsOn</code> attribute.
      *
      * <br>Description from the FOM: <i>Whether the entity's spotlights are on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>spotLightsOn</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual bool getSpotLightsOn()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>spotLightsOn</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Whether the entity's spotlights are on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>spotLightsOn</code> attribute.
      */
      LIBAPI virtual bool getSpotLightsOn(bool defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>spotLightsOn</code> attribute.
      * <br>Description from the FOM: <i>Whether the entity's spotlights are on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>spotLightsOn</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< bool > getSpotLightsOnTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>tailLightsOn</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Whether the entity's tail lights are on or not.</i>
      *
      * @return true if <code>tailLightsOn</code> is available.
      */
      LIBAPI virtual bool hasTailLightsOn() = 0;

      /**
      * Gets the value of the <code>tailLightsOn</code> attribute.
      *
      * <br>Description from the FOM: <i>Whether the entity's tail lights are on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>tailLightsOn</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual bool getTailLightsOn()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>tailLightsOn</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Whether the entity's tail lights are on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>tailLightsOn</code> attribute.
      */
      LIBAPI virtual bool getTailLightsOn(bool defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>tailLightsOn</code> attribute.
      * <br>Description from the FOM: <i>Whether the entity's tail lights are on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>tailLightsOn</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< bool > getTailLightsOnTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>acousticSignatureIndex</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Index used to obtain the acoustics (sound through air) signature state of the entity.</i>
      *
      * @return true if <code>acousticSignatureIndex</code> is available.
      */
      LIBAPI virtual bool hasAcousticSignatureIndex() = 0;

      /**
      * Gets the value of the <code>acousticSignatureIndex</code> attribute.
      *
      * <br>Description from the FOM: <i>Index used to obtain the acoustics (sound through air) signature state of the entity.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^15, 2^15-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the <code>acousticSignatureIndex</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual short getAcousticSignatureIndex()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>acousticSignatureIndex</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Index used to obtain the acoustics (sound through air) signature state of the entity.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^15, 2^15-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>acousticSignatureIndex</code> attribute.
      */
      LIBAPI virtual short getAcousticSignatureIndex(short defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>acousticSignatureIndex</code> attribute.
      * <br>Description from the FOM: <i>Index used to obtain the acoustics (sound through air) signature state of the entity.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^15, 2^15-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the time stamped <code>acousticSignatureIndex</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< short > getAcousticSignatureIndexTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>alternateEntityType</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The category of entity to be used when viewed by entities on the 'opposite' side.</i>
      *
      * @return true if <code>alternateEntityType</code> is available.
      */
      LIBAPI virtual bool hasAlternateEntityType() = 0;

      /**
      * Gets the value of the <code>alternateEntityType</code> attribute.
      *
      * <br>Description from the FOM: <i>The category of entity to be used when viewed by entities on the 'opposite' side.</i>
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      *
      * @return the <code>alternateEntityType</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::EntityTypeStruct getAlternateEntityType()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>alternateEntityType</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The category of entity to be used when viewed by entities on the 'opposite' side.</i>
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>alternateEntityType</code> attribute.
      */
      LIBAPI virtual DevStudio::EntityTypeStruct getAlternateEntityType(DevStudio::EntityTypeStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>alternateEntityType</code> attribute.
      * <br>Description from the FOM: <i>The category of entity to be used when viewed by entities on the 'opposite' side.</i>
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      *
      * @return the time stamped <code>alternateEntityType</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::EntityTypeStruct > getAlternateEntityTypeTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>articulatedParametersArray</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Identification of the visible parts, and their states, of the entity which are capable of independent motion.</i>
      *
      * @return true if <code>articulatedParametersArray</code> is available.
      */
      LIBAPI virtual bool hasArticulatedParametersArray() = 0;

      /**
      * Gets the value of the <code>articulatedParametersArray</code> attribute.
      *
      * <br>Description from the FOM: <i>Identification of the visible parts, and their states, of the entity which are capable of independent motion.</i>
      * <br>Description of the data type from the FOM: <i>Dynamic array of ArticulatedParameterStruct elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
      *
      * @return the <code>articulatedParametersArray</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::vector<DevStudio::ArticulatedParameterStruct > getArticulatedParametersArray()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>articulatedParametersArray</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Identification of the visible parts, and their states, of the entity which are capable of independent motion.</i>
      * <br>Description of the data type from the FOM: <i>Dynamic array of ArticulatedParameterStruct elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>articulatedParametersArray</code> attribute.
      */
      LIBAPI virtual std::vector<DevStudio::ArticulatedParameterStruct > getArticulatedParametersArray(std::vector<DevStudio::ArticulatedParameterStruct > defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>articulatedParametersArray</code> attribute.
      * <br>Description from the FOM: <i>Identification of the visible parts, and their states, of the entity which are capable of independent motion.</i>
      * <br>Description of the data type from the FOM: <i>Dynamic array of ArticulatedParameterStruct elements, may also contain no elements. The array is encoded without array length, containing only the elements.</i>
      *
      * @return the time stamped <code>articulatedParametersArray</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::vector<DevStudio::ArticulatedParameterStruct > > getArticulatedParametersArrayTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>camouflageType</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The type of camouflage in use (if any).</i>
      *
      * @return true if <code>camouflageType</code> is available.
      */
      LIBAPI virtual bool hasCamouflageType() = 0;

      /**
      * Gets the value of the <code>camouflageType</code> attribute.
      *
      * <br>Description from the FOM: <i>The type of camouflage in use (if any).</i>
      * <br>Description of the data type from the FOM: <i>Camouflage type</i>
      *
      * @return the <code>camouflageType</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::CamouflageEnum::CamouflageEnum getCamouflageType()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>camouflageType</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The type of camouflage in use (if any).</i>
      * <br>Description of the data type from the FOM: <i>Camouflage type</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>camouflageType</code> attribute.
      */
      LIBAPI virtual DevStudio::CamouflageEnum::CamouflageEnum getCamouflageType(DevStudio::CamouflageEnum::CamouflageEnum defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>camouflageType</code> attribute.
      * <br>Description from the FOM: <i>The type of camouflage in use (if any).</i>
      * <br>Description of the data type from the FOM: <i>Camouflage type</i>
      *
      * @return the time stamped <code>camouflageType</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::CamouflageEnum::CamouflageEnum > getCamouflageTypeTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>damageState</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The state of damage of the entity.</i>
      *
      * @return true if <code>damageState</code> is available.
      */
      LIBAPI virtual bool hasDamageState() = 0;

      /**
      * Gets the value of the <code>damageState</code> attribute.
      *
      * <br>Description from the FOM: <i>The state of damage of the entity.</i>
      * <br>Description of the data type from the FOM: <i>Damaged appearance</i>
      *
      * @return the <code>damageState</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::DamageStatusEnum::DamageStatusEnum getDamageState()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>damageState</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The state of damage of the entity.</i>
      * <br>Description of the data type from the FOM: <i>Damaged appearance</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>damageState</code> attribute.
      */
      LIBAPI virtual DevStudio::DamageStatusEnum::DamageStatusEnum getDamageState(DevStudio::DamageStatusEnum::DamageStatusEnum defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>damageState</code> attribute.
      * <br>Description from the FOM: <i>The state of damage of the entity.</i>
      * <br>Description of the data type from the FOM: <i>Damaged appearance</i>
      *
      * @return the time stamped <code>damageState</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::DamageStatusEnum::DamageStatusEnum > getDamageStateTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>engineSmokeOn</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Whether the entity's engine is generating smoke or not.</i>
      *
      * @return true if <code>engineSmokeOn</code> is available.
      */
      LIBAPI virtual bool hasEngineSmokeOn() = 0;

      /**
      * Gets the value of the <code>engineSmokeOn</code> attribute.
      *
      * <br>Description from the FOM: <i>Whether the entity's engine is generating smoke or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>engineSmokeOn</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual bool getEngineSmokeOn()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>engineSmokeOn</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Whether the entity's engine is generating smoke or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>engineSmokeOn</code> attribute.
      */
      LIBAPI virtual bool getEngineSmokeOn(bool defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>engineSmokeOn</code> attribute.
      * <br>Description from the FOM: <i>Whether the entity's engine is generating smoke or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>engineSmokeOn</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< bool > getEngineSmokeOnTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>firePowerDisabled</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Whether the entity's main weapon system has been disabled or not.</i>
      *
      * @return true if <code>firePowerDisabled</code> is available.
      */
      LIBAPI virtual bool hasFirePowerDisabled() = 0;

      /**
      * Gets the value of the <code>firePowerDisabled</code> attribute.
      *
      * <br>Description from the FOM: <i>Whether the entity's main weapon system has been disabled or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>firePowerDisabled</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual bool getFirePowerDisabled()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>firePowerDisabled</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Whether the entity's main weapon system has been disabled or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>firePowerDisabled</code> attribute.
      */
      LIBAPI virtual bool getFirePowerDisabled(bool defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>firePowerDisabled</code> attribute.
      * <br>Description from the FOM: <i>Whether the entity's main weapon system has been disabled or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>firePowerDisabled</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< bool > getFirePowerDisabledTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>flamesPresent</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Whether the entity is on fire (with visible flames) or not.</i>
      *
      * @return true if <code>flamesPresent</code> is available.
      */
      LIBAPI virtual bool hasFlamesPresent() = 0;

      /**
      * Gets the value of the <code>flamesPresent</code> attribute.
      *
      * <br>Description from the FOM: <i>Whether the entity is on fire (with visible flames) or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>flamesPresent</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual bool getFlamesPresent()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>flamesPresent</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Whether the entity is on fire (with visible flames) or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>flamesPresent</code> attribute.
      */
      LIBAPI virtual bool getFlamesPresent(bool defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>flamesPresent</code> attribute.
      * <br>Description from the FOM: <i>Whether the entity is on fire (with visible flames) or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>flamesPresent</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< bool > getFlamesPresentTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>forceIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The identification of the force that the entity belongs to.</i>
      *
      * @return true if <code>forceIdentifier</code> is available.
      */
      LIBAPI virtual bool hasForceIdentifier() = 0;

      /**
      * Gets the value of the <code>forceIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>The identification of the force that the entity belongs to.</i>
      * <br>Description of the data type from the FOM: <i>Force ID</i>
      *
      * @return the <code>forceIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::ForceIdentifierEnum::ForceIdentifierEnum getForceIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>forceIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The identification of the force that the entity belongs to.</i>
      * <br>Description of the data type from the FOM: <i>Force ID</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>forceIdentifier</code> attribute.
      */
      LIBAPI virtual DevStudio::ForceIdentifierEnum::ForceIdentifierEnum getForceIdentifier(DevStudio::ForceIdentifierEnum::ForceIdentifierEnum defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>forceIdentifier</code> attribute.
      * <br>Description from the FOM: <i>The identification of the force that the entity belongs to.</i>
      * <br>Description of the data type from the FOM: <i>Force ID</i>
      *
      * @return the time stamped <code>forceIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::ForceIdentifierEnum::ForceIdentifierEnum > getForceIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>hasAmmunitionSupplyCap</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Whether the entity has the capability to supply other entities with ammunition.</i>
      *
      * @return true if <code>hasAmmunitionSupplyCap</code> is available.
      */
      LIBAPI virtual bool hasHasAmmunitionSupplyCap() = 0;

      /**
      * Gets the value of the <code>hasAmmunitionSupplyCap</code> attribute.
      *
      * <br>Description from the FOM: <i>Whether the entity has the capability to supply other entities with ammunition.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>hasAmmunitionSupplyCap</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual bool getHasAmmunitionSupplyCap()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>hasAmmunitionSupplyCap</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Whether the entity has the capability to supply other entities with ammunition.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>hasAmmunitionSupplyCap</code> attribute.
      */
      LIBAPI virtual bool getHasAmmunitionSupplyCap(bool defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>hasAmmunitionSupplyCap</code> attribute.
      * <br>Description from the FOM: <i>Whether the entity has the capability to supply other entities with ammunition.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>hasAmmunitionSupplyCap</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< bool > getHasAmmunitionSupplyCapTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>hasFuelSupplyCap</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Whether the entity has the capability to supply other entities with fuel or not.</i>
      *
      * @return true if <code>hasFuelSupplyCap</code> is available.
      */
      LIBAPI virtual bool hasHasFuelSupplyCap() = 0;

      /**
      * Gets the value of the <code>hasFuelSupplyCap</code> attribute.
      *
      * <br>Description from the FOM: <i>Whether the entity has the capability to supply other entities with fuel or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>hasFuelSupplyCap</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual bool getHasFuelSupplyCap()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>hasFuelSupplyCap</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Whether the entity has the capability to supply other entities with fuel or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>hasFuelSupplyCap</code> attribute.
      */
      LIBAPI virtual bool getHasFuelSupplyCap(bool defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>hasFuelSupplyCap</code> attribute.
      * <br>Description from the FOM: <i>Whether the entity has the capability to supply other entities with fuel or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>hasFuelSupplyCap</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< bool > getHasFuelSupplyCapTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>hasRecoveryCap</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Whether the entity has the capability to recover other entities or not.</i>
      *
      * @return true if <code>hasRecoveryCap</code> is available.
      */
      LIBAPI virtual bool hasHasRecoveryCap() = 0;

      /**
      * Gets the value of the <code>hasRecoveryCap</code> attribute.
      *
      * <br>Description from the FOM: <i>Whether the entity has the capability to recover other entities or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>hasRecoveryCap</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual bool getHasRecoveryCap()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>hasRecoveryCap</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Whether the entity has the capability to recover other entities or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>hasRecoveryCap</code> attribute.
      */
      LIBAPI virtual bool getHasRecoveryCap(bool defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>hasRecoveryCap</code> attribute.
      * <br>Description from the FOM: <i>Whether the entity has the capability to recover other entities or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>hasRecoveryCap</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< bool > getHasRecoveryCapTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>hasRepairCap</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Whether the entity has the capability to repair other entities or not.</i>
      *
      * @return true if <code>hasRepairCap</code> is available.
      */
      LIBAPI virtual bool hasHasRepairCap() = 0;

      /**
      * Gets the value of the <code>hasRepairCap</code> attribute.
      *
      * <br>Description from the FOM: <i>Whether the entity has the capability to repair other entities or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>hasRepairCap</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual bool getHasRepairCap()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>hasRepairCap</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Whether the entity has the capability to repair other entities or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>hasRepairCap</code> attribute.
      */
      LIBAPI virtual bool getHasRepairCap(bool defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>hasRepairCap</code> attribute.
      * <br>Description from the FOM: <i>Whether the entity has the capability to repair other entities or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>hasRepairCap</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< bool > getHasRepairCapTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>immobilized</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Whether the entity is immobilized or not.</i>
      *
      * @return true if <code>immobilized</code> is available.
      */
      LIBAPI virtual bool hasImmobilized() = 0;

      /**
      * Gets the value of the <code>immobilized</code> attribute.
      *
      * <br>Description from the FOM: <i>Whether the entity is immobilized or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>immobilized</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual bool getImmobilized()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>immobilized</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Whether the entity is immobilized or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>immobilized</code> attribute.
      */
      LIBAPI virtual bool getImmobilized(bool defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>immobilized</code> attribute.
      * <br>Description from the FOM: <i>Whether the entity is immobilized or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>immobilized</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< bool > getImmobilizedTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>infraredSignatureIndex</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Index used to obtain the infra-red signature state of the entity.</i>
      *
      * @return true if <code>infraredSignatureIndex</code> is available.
      */
      LIBAPI virtual bool hasInfraredSignatureIndex() = 0;

      /**
      * Gets the value of the <code>infraredSignatureIndex</code> attribute.
      *
      * <br>Description from the FOM: <i>Index used to obtain the infra-red signature state of the entity.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^15, 2^15-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the <code>infraredSignatureIndex</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual short getInfraredSignatureIndex()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>infraredSignatureIndex</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Index used to obtain the infra-red signature state of the entity.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^15, 2^15-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>infraredSignatureIndex</code> attribute.
      */
      LIBAPI virtual short getInfraredSignatureIndex(short defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>infraredSignatureIndex</code> attribute.
      * <br>Description from the FOM: <i>Index used to obtain the infra-red signature state of the entity.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^15, 2^15-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the time stamped <code>infraredSignatureIndex</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< short > getInfraredSignatureIndexTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>isConcealed</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Whether the entity is concealed or not.</i>
      *
      * @return true if <code>isConcealed</code> is available.
      */
      LIBAPI virtual bool hasIsConcealed() = 0;

      /**
      * Gets the value of the <code>isConcealed</code> attribute.
      *
      * <br>Description from the FOM: <i>Whether the entity is concealed or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>isConcealed</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual bool getIsConcealed()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>isConcealed</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Whether the entity is concealed or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>isConcealed</code> attribute.
      */
      LIBAPI virtual bool getIsConcealed(bool defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>isConcealed</code> attribute.
      * <br>Description from the FOM: <i>Whether the entity is concealed or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>isConcealed</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< bool > getIsConcealedTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>liveEntityMeasuredSpeed</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The entity's own measurement of speed (e.g. air speed for aircraft).</i>
      *
      * @return true if <code>liveEntityMeasuredSpeed</code> is available.
      */
      LIBAPI virtual bool hasLiveEntityMeasuredSpeed() = 0;

      /**
      * Gets the value of the <code>liveEntityMeasuredSpeed</code> attribute.
      *
      * <br>Description from the FOM: <i>The entity's own measurement of speed (e.g. air speed for aircraft).</i>
      * <br>Description of the data type from the FOM: <i>Velocity/Speed measured in decimeter per second. [unit: decimeter per second (dm/s), resolution: 1, accuracy: perfect]</i>
      *
      * @return the <code>liveEntityMeasuredSpeed</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual unsigned short getLiveEntityMeasuredSpeed()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>liveEntityMeasuredSpeed</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The entity's own measurement of speed (e.g. air speed for aircraft).</i>
      * <br>Description of the data type from the FOM: <i>Velocity/Speed measured in decimeter per second. [unit: decimeter per second (dm/s), resolution: 1, accuracy: perfect]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>liveEntityMeasuredSpeed</code> attribute.
      */
      LIBAPI virtual unsigned short getLiveEntityMeasuredSpeed(unsigned short defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>liveEntityMeasuredSpeed</code> attribute.
      * <br>Description from the FOM: <i>The entity's own measurement of speed (e.g. air speed for aircraft).</i>
      * <br>Description of the data type from the FOM: <i>Velocity/Speed measured in decimeter per second. [unit: decimeter per second (dm/s), resolution: 1, accuracy: perfect]</i>
      *
      * @return the time stamped <code>liveEntityMeasuredSpeed</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< unsigned short > getLiveEntityMeasuredSpeedTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>marking</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>A unique marking or combination of characters used to distinguish the entity from other entities.</i>
      *
      * @return true if <code>marking</code> is available.
      */
      LIBAPI virtual bool hasMarking() = 0;

      /**
      * Gets the value of the <code>marking</code> attribute.
      *
      * <br>Description from the FOM: <i>A unique marking or combination of characters used to distinguish the entity from other entities.</i>
      * <br>Description of the data type from the FOM: <i>Character set used in the marking and the string of characters to be interpreted for display.</i>
      *
      * @return the <code>marking</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::MarkingStruct getMarking()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>marking</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>A unique marking or combination of characters used to distinguish the entity from other entities.</i>
      * <br>Description of the data type from the FOM: <i>Character set used in the marking and the string of characters to be interpreted for display.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>marking</code> attribute.
      */
      LIBAPI virtual DevStudio::MarkingStruct getMarking(DevStudio::MarkingStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>marking</code> attribute.
      * <br>Description from the FOM: <i>A unique marking or combination of characters used to distinguish the entity from other entities.</i>
      * <br>Description of the data type from the FOM: <i>Character set used in the marking and the string of characters to be interpreted for display.</i>
      *
      * @return the time stamped <code>marking</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::MarkingStruct > getMarkingTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>powerPlantOn</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Whether the entity's power plant is on or not.</i>
      *
      * @return true if <code>powerPlantOn</code> is available.
      */
      LIBAPI virtual bool hasPowerPlantOn() = 0;

      /**
      * Gets the value of the <code>powerPlantOn</code> attribute.
      *
      * <br>Description from the FOM: <i>Whether the entity's power plant is on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>powerPlantOn</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual bool getPowerPlantOn()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>powerPlantOn</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Whether the entity's power plant is on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>powerPlantOn</code> attribute.
      */
      LIBAPI virtual bool getPowerPlantOn(bool defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>powerPlantOn</code> attribute.
      * <br>Description from the FOM: <i>Whether the entity's power plant is on or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>powerPlantOn</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< bool > getPowerPlantOnTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>propulsionSystemsData</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The basic operating data of the propulsion systems aboard the entity.</i>
      *
      * @return true if <code>propulsionSystemsData</code> is available.
      */
      LIBAPI virtual bool hasPropulsionSystemsData() = 0;

      /**
      * Gets the value of the <code>propulsionSystemsData</code> attribute.
      *
      * <br>Description from the FOM: <i>The basic operating data of the propulsion systems aboard the entity.</i>
      * <br>Description of the data type from the FOM: <i>A set of Propulsion System Data descriptions.</i>
      *
      * @return the <code>propulsionSystemsData</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::vector<DevStudio::PropulsionSystemDataStruct > getPropulsionSystemsData()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>propulsionSystemsData</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The basic operating data of the propulsion systems aboard the entity.</i>
      * <br>Description of the data type from the FOM: <i>A set of Propulsion System Data descriptions.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>propulsionSystemsData</code> attribute.
      */
      LIBAPI virtual std::vector<DevStudio::PropulsionSystemDataStruct > getPropulsionSystemsData(std::vector<DevStudio::PropulsionSystemDataStruct > defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>propulsionSystemsData</code> attribute.
      * <br>Description from the FOM: <i>The basic operating data of the propulsion systems aboard the entity.</i>
      * <br>Description of the data type from the FOM: <i>A set of Propulsion System Data descriptions.</i>
      *
      * @return the time stamped <code>propulsionSystemsData</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::vector<DevStudio::PropulsionSystemDataStruct > > getPropulsionSystemsDataTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>radarCrossSectionSignatureIndex</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Index used to obtain the radar cross section signature state of the entity.</i>
      *
      * @return true if <code>radarCrossSectionSignatureIndex</code> is available.
      */
      LIBAPI virtual bool hasRadarCrossSectionSignatureIndex() = 0;

      /**
      * Gets the value of the <code>radarCrossSectionSignatureIndex</code> attribute.
      *
      * <br>Description from the FOM: <i>Index used to obtain the radar cross section signature state of the entity.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^15, 2^15-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the <code>radarCrossSectionSignatureIndex</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual short getRadarCrossSectionSignatureIndex()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>radarCrossSectionSignatureIndex</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Index used to obtain the radar cross section signature state of the entity.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^15, 2^15-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>radarCrossSectionSignatureIndex</code> attribute.
      */
      LIBAPI virtual short getRadarCrossSectionSignatureIndex(short defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>radarCrossSectionSignatureIndex</code> attribute.
      * <br>Description from the FOM: <i>Index used to obtain the radar cross section signature state of the entity.</i>
      * <br>Description of the data type from the FOM: <i>Integer in the range [-2^15, 2^15-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
      *
      * @return the time stamped <code>radarCrossSectionSignatureIndex</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< short > getRadarCrossSectionSignatureIndexTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>smokePlumePresent</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Whether the entity is generating smoke or not (intentional or unintentional).</i>
      *
      * @return true if <code>smokePlumePresent</code> is available.
      */
      LIBAPI virtual bool hasSmokePlumePresent() = 0;

      /**
      * Gets the value of the <code>smokePlumePresent</code> attribute.
      *
      * <br>Description from the FOM: <i>Whether the entity is generating smoke or not (intentional or unintentional).</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>smokePlumePresent</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual bool getSmokePlumePresent()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>smokePlumePresent</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Whether the entity is generating smoke or not (intentional or unintentional).</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>smokePlumePresent</code> attribute.
      */
      LIBAPI virtual bool getSmokePlumePresent(bool defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>smokePlumePresent</code> attribute.
      * <br>Description from the FOM: <i>Whether the entity is generating smoke or not (intentional or unintentional).</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>smokePlumePresent</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< bool > getSmokePlumePresentTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>tentDeployed</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Whether the entity has deployed tent or not.</i>
      *
      * @return true if <code>tentDeployed</code> is available.
      */
      LIBAPI virtual bool hasTentDeployed() = 0;

      /**
      * Gets the value of the <code>tentDeployed</code> attribute.
      *
      * <br>Description from the FOM: <i>Whether the entity has deployed tent or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the <code>tentDeployed</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual bool getTentDeployed()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>tentDeployed</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Whether the entity has deployed tent or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @param defaultValue default value
      *
      * @return the <code>tentDeployed</code> attribute.
      */
      LIBAPI virtual bool getTentDeployed(bool defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>tentDeployed</code> attribute.
      * <br>Description from the FOM: <i>Whether the entity has deployed tent or not.</i>
      * <br>Description of the data type from the FOM: <i></i>
      *
      * @return the time stamped <code>tentDeployed</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< bool > getTentDeployedTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>trailingEffectsCode</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The type and size of any trail that the entity is making.</i>
      *
      * @return true if <code>trailingEffectsCode</code> is available.
      */
      LIBAPI virtual bool hasTrailingEffectsCode() = 0;

      /**
      * Gets the value of the <code>trailingEffectsCode</code> attribute.
      *
      * <br>Description from the FOM: <i>The type and size of any trail that the entity is making.</i>
      * <br>Description of the data type from the FOM: <i>Size of trailing effect</i>
      *
      * @return the <code>trailingEffectsCode</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::TrailingEffectsCodeEnum::TrailingEffectsCodeEnum getTrailingEffectsCode()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>trailingEffectsCode</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The type and size of any trail that the entity is making.</i>
      * <br>Description of the data type from the FOM: <i>Size of trailing effect</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>trailingEffectsCode</code> attribute.
      */
      LIBAPI virtual DevStudio::TrailingEffectsCodeEnum::TrailingEffectsCodeEnum getTrailingEffectsCode(DevStudio::TrailingEffectsCodeEnum::TrailingEffectsCodeEnum defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>trailingEffectsCode</code> attribute.
      * <br>Description from the FOM: <i>The type and size of any trail that the entity is making.</i>
      * <br>Description of the data type from the FOM: <i>Size of trailing effect</i>
      *
      * @return the time stamped <code>trailingEffectsCode</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::TrailingEffectsCodeEnum::TrailingEffectsCodeEnum > getTrailingEffectsCodeTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>vectoringNozzleSystemData</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The basic operational data for the vectoring nozzle systems aboard the entity.</i>
      *
      * @return true if <code>vectoringNozzleSystemData</code> is available.
      */
      LIBAPI virtual bool hasVectoringNozzleSystemData() = 0;

      /**
      * Gets the value of the <code>vectoringNozzleSystemData</code> attribute.
      *
      * <br>Description from the FOM: <i>The basic operational data for the vectoring nozzle systems aboard the entity.</i>
      * <br>Description of the data type from the FOM: <i>A set of Vectoring Nozzle System Data description.</i>
      *
      * @return the <code>vectoringNozzleSystemData</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual std::vector<DevStudio::VectoringNozzleSystemDataStruct > getVectoringNozzleSystemData()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>vectoringNozzleSystemData</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The basic operational data for the vectoring nozzle systems aboard the entity.</i>
      * <br>Description of the data type from the FOM: <i>A set of Vectoring Nozzle System Data description.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>vectoringNozzleSystemData</code> attribute.
      */
      LIBAPI virtual std::vector<DevStudio::VectoringNozzleSystemDataStruct > getVectoringNozzleSystemData(std::vector<DevStudio::VectoringNozzleSystemDataStruct > defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>vectoringNozzleSystemData</code> attribute.
      * <br>Description from the FOM: <i>The basic operational data for the vectoring nozzle systems aboard the entity.</i>
      * <br>Description of the data type from the FOM: <i>A set of Vectoring Nozzle System Data description.</i>
      *
      * @return the time stamped <code>vectoringNozzleSystemData</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< std::vector<DevStudio::VectoringNozzleSystemDataStruct > > getVectoringNozzleSystemDataTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>entityType</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The category of the entity.</i>
      *
      * @return true if <code>entityType</code> is available.
      */
      LIBAPI virtual bool hasEntityType() = 0;

      /**
      * Gets the value of the <code>entityType</code> attribute.
      *
      * <br>Description from the FOM: <i>The category of the entity.</i>
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      *
      * @return the <code>entityType</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::EntityTypeStruct getEntityType()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>entityType</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The category of the entity.</i>
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>entityType</code> attribute.
      */
      LIBAPI virtual DevStudio::EntityTypeStruct getEntityType(DevStudio::EntityTypeStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>entityType</code> attribute.
      * <br>Description from the FOM: <i>The category of the entity.</i>
      * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
      *
      * @return the time stamped <code>entityType</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::EntityTypeStruct > getEntityTypeTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>entityIdentifier</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>The unique identifier for the entity instance.</i>
      *
      * @return true if <code>entityIdentifier</code> is available.
      */
      LIBAPI virtual bool hasEntityIdentifier() = 0;

      /**
      * Gets the value of the <code>entityIdentifier</code> attribute.
      *
      * <br>Description from the FOM: <i>The unique identifier for the entity instance.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @return the <code>entityIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::EntityIdentifierStruct getEntityIdentifier()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>entityIdentifier</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>The unique identifier for the entity instance.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>entityIdentifier</code> attribute.
      */
      LIBAPI virtual DevStudio::EntityIdentifierStruct getEntityIdentifier(DevStudio::EntityIdentifierStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>entityIdentifier</code> attribute.
      * <br>Description from the FOM: <i>The unique identifier for the entity instance.</i>
      * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
      *
      * @return the time stamped <code>entityIdentifier</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::EntityIdentifierStruct > getEntityIdentifierTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>isPartOf</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Defines if the entity if a constituent part of another entity (denoted the host entity). If the entity is a constituent part of another entity then the HostEntityIdentifier shall be set to the EntityIdentifier of the host entity and the HostRTIObjectIdentifier shall be set to the RTI object instance ID of the host entity. If the entity is not a constituent part of another entity then the HostEntityIdentifier shall be set to 0.0.0 and the HostRTIObjectIdentifier shall be set to the empty string.</i>
      *
      * @return true if <code>isPartOf</code> is available.
      */
      LIBAPI virtual bool hasIsPartOf() = 0;

      /**
      * Gets the value of the <code>isPartOf</code> attribute.
      *
      * <br>Description from the FOM: <i>Defines if the entity if a constituent part of another entity (denoted the host entity). If the entity is a constituent part of another entity then the HostEntityIdentifier shall be set to the EntityIdentifier of the host entity and the HostRTIObjectIdentifier shall be set to the RTI object instance ID of the host entity. If the entity is not a constituent part of another entity then the HostEntityIdentifier shall be set to 0.0.0 and the HostRTIObjectIdentifier shall be set to the empty string.</i>
      * <br>Description of the data type from the FOM: <i>Defines the spatial relationship between two objects.</i>
      *
      * @return the <code>isPartOf</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::IsPartOfStruct getIsPartOf()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>isPartOf</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Defines if the entity if a constituent part of another entity (denoted the host entity). If the entity is a constituent part of another entity then the HostEntityIdentifier shall be set to the EntityIdentifier of the host entity and the HostRTIObjectIdentifier shall be set to the RTI object instance ID of the host entity. If the entity is not a constituent part of another entity then the HostEntityIdentifier shall be set to 0.0.0 and the HostRTIObjectIdentifier shall be set to the empty string.</i>
      * <br>Description of the data type from the FOM: <i>Defines the spatial relationship between two objects.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>isPartOf</code> attribute.
      */
      LIBAPI virtual DevStudio::IsPartOfStruct getIsPartOf(DevStudio::IsPartOfStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>isPartOf</code> attribute.
      * <br>Description from the FOM: <i>Defines if the entity if a constituent part of another entity (denoted the host entity). If the entity is a constituent part of another entity then the HostEntityIdentifier shall be set to the EntityIdentifier of the host entity and the HostRTIObjectIdentifier shall be set to the RTI object instance ID of the host entity. If the entity is not a constituent part of another entity then the HostEntityIdentifier shall be set to 0.0.0 and the HostRTIObjectIdentifier shall be set to the empty string.</i>
      * <br>Description of the data type from the FOM: <i>Defines the spatial relationship between two objects.</i>
      *
      * @return the time stamped <code>isPartOf</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::IsPartOfStruct > getIsPartOfTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>spatial</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Spatial state stored in one variant record attribute.</i>
      *
      * @return true if <code>spatial</code> is available.
      */
      LIBAPI virtual bool hasSpatial() = 0;

      /**
      * Gets the value of the <code>spatial</code> attribute.
      *
      * <br>Description from the FOM: <i>Spatial state stored in one variant record attribute.</i>
      * <br>Description of the data type from the FOM: <i>Variant Record for a single spatial attribute.</i>
      *
      * @return the <code>spatial</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::SpatialVariantStruct getSpatial()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>spatial</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Spatial state stored in one variant record attribute.</i>
      * <br>Description of the data type from the FOM: <i>Variant Record for a single spatial attribute.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>spatial</code> attribute.
      */
      LIBAPI virtual DevStudio::SpatialVariantStruct getSpatial(DevStudio::SpatialVariantStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>spatial</code> attribute.
      * <br>Description from the FOM: <i>Spatial state stored in one variant record attribute.</i>
      * <br>Description of the data type from the FOM: <i>Variant Record for a single spatial attribute.</i>
      *
      * @return the time stamped <code>spatial</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::SpatialVariantStruct > getSpatialTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
    
      /**
      * Returns true if the <code>relativeSpatial</code> attribute has received a value yet.
      *
      * <br>Description from the FOM: <i>Relative spatial state stored in one variant record attribute.</i>
      *
      * @return true if <code>relativeSpatial</code> is available.
      */
      LIBAPI virtual bool hasRelativeSpatial() = 0;

      /**
      * Gets the value of the <code>relativeSpatial</code> attribute.
      *
      * <br>Description from the FOM: <i>Relative spatial state stored in one variant record attribute.</i>
      * <br>Description of the data type from the FOM: <i>Variant Record for a single spatial attribute.</i>
      *
      * @return the <code>relativeSpatial</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::SpatialVariantStruct getRelativeSpatial()
         THROW_SPEC (HlaValueNotSetException) = 0;

      /**
      * Gets the value of the <code>relativeSpatial</code> attribute, or <code>defaultValue</code> if value was not set.
      *
      * <br>Description from the FOM: <i>Relative spatial state stored in one variant record attribute.</i>
      * <br>Description of the data type from the FOM: <i>Variant Record for a single spatial attribute.</i>
      *
      * @param defaultValue default value
      *
      * @return the <code>relativeSpatial</code> attribute.
      */
      LIBAPI virtual DevStudio::SpatialVariantStruct getRelativeSpatial(DevStudio::SpatialVariantStruct defaultValue) = 0;

      /**
      * Gets the time stamped value of the <code>relativeSpatial</code> attribute.
      * <br>Description from the FOM: <i>Relative spatial state stored in one variant record attribute.</i>
      * <br>Description of the data type from the FOM: <i>Variant Record for a single spatial attribute.</i>
      *
      * @return the time stamped <code>relativeSpatial</code> attribute.
      *
      * @throws HlaValueNotSetException unchecked exception that is thrown if no value is available.
      */
      LIBAPI virtual DevStudio::HlaTimeStamped< DevStudio::SpatialVariantStruct > getRelativeSpatialTimeStamped()
         THROW_SPEC (HlaValueNotSetException) = 0;
   };
}
#endif
