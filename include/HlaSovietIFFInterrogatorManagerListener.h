/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLASOVIETIFFINTERROGATORMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLASOVIETIFFINTERROGATORMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaSovietIFFInterrogator.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaSovietIFFInterrogator instances.
    */
    class HlaSovietIFFInterrogatorManagerListener {

    public:

        LIBAPI virtual ~HlaSovietIFFInterrogatorManagerListener() {}

        /**
        * This method is called when a new HlaSovietIFFInterrogator instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param sovietIFFInterrogator the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaSovietIFFInterrogatorDiscovered(HlaSovietIFFInterrogatorPtr sovietIFFInterrogator, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaSovietIFFInterrogator instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param sovietIFFInterrogator the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaSovietIFFInterrogatorInitialized(HlaSovietIFFInterrogatorPtr sovietIFFInterrogator, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaSovietIFFInterrogatorManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param sovietIFFInterrogator the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaSovietIFFInterrogatorInInterest(HlaSovietIFFInterrogatorPtr sovietIFFInterrogator, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaSovietIFFInterrogatorManagerListener instance goes out of interest.
        *
        * @param sovietIFFInterrogator the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaSovietIFFInterrogatorOutOfInterest(HlaSovietIFFInterrogatorPtr sovietIFFInterrogator, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaSovietIFFInterrogator instance is deleted.
        *
        * @param sovietIFFInterrogator the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaSovietIFFInterrogatorDeleted(HlaSovietIFFInterrogatorPtr sovietIFFInterrogator, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaSovietIFFInterrogatorManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaSovietIFFInterrogatorManagerListener::Adapter : public HlaSovietIFFInterrogatorManagerListener {

    public:
        LIBAPI virtual void hlaSovietIFFInterrogatorDiscovered(HlaSovietIFFInterrogatorPtr sovietIFFInterrogator, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaSovietIFFInterrogatorInitialized(HlaSovietIFFInterrogatorPtr sovietIFFInterrogator, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaSovietIFFInterrogatorInInterest(HlaSovietIFFInterrogatorPtr sovietIFFInterrogator, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaSovietIFFInterrogatorOutOfInterest(HlaSovietIFFInterrogatorPtr sovietIFFInterrogator, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaSovietIFFInterrogatorDeleted(HlaSovietIFFInterrogatorPtr sovietIFFInterrogator, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
