/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAMUNITIONMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLAMUNITIONMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaMunition.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaMunition instances.
    */
    class HlaMunitionManagerListener {

    public:

        LIBAPI virtual ~HlaMunitionManagerListener() {}

        /**
        * This method is called when a new HlaMunition instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param munition the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaMunitionDiscovered(HlaMunitionPtr munition, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaMunition instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param munition the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaMunitionInitialized(HlaMunitionPtr munition, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaMunitionManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param munition the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaMunitionInInterest(HlaMunitionPtr munition, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaMunitionManagerListener instance goes out of interest.
        *
        * @param munition the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaMunitionOutOfInterest(HlaMunitionPtr munition, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaMunition instance is deleted.
        *
        * @param munition the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaMunitionDeleted(HlaMunitionPtr munition, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaMunitionManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaMunitionManagerListener::Adapter : public HlaMunitionManagerListener {

    public:
        LIBAPI virtual void hlaMunitionDiscovered(HlaMunitionPtr munition, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaMunitionInitialized(HlaMunitionPtr munition, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaMunitionInInterest(HlaMunitionPtr munition, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaMunitionOutOfInterest(HlaMunitionPtr munition, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaMunitionDeleted(HlaMunitionPtr munition, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
