/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAINTERACTIONLISTENER_H
#define DEVELOPER_STUDIO_HLAINTERACTIONLISTENER_H

#include <DevStudio/datatypes/AcknowledgeFlagEnum.h>
#include <DevStudio/datatypes/AcknowledgementProtocolEnum.h>
#include <DevStudio/datatypes/AcquisitionTypeEnum.h>
#include <DevStudio/datatypes/ActionEnum.h>
#include <DevStudio/datatypes/ActionResultEnum.h>
#include <DevStudio/datatypes/ArticulatedParameterStruct.h>
#include <DevStudio/datatypes/ArticulatedParameterStructLengthlessArray.h>
#include <DevStudio/datatypes/AttributeValuePairStruct.h>
#include <DevStudio/datatypes/AttributeValuePairStructArray1Plus.h>
#include <DevStudio/datatypes/AudioDataTypeStruct.h>
#include <DevStudio/datatypes/BreachStruct.h>
#include <DevStudio/datatypes/BreachStructLengthlessArray.h>
#include <DevStudio/datatypes/BreachableSegmentStruct.h>
#include <DevStudio/datatypes/BreachableSegmentStructLengthlessArray.h>
#include <DevStudio/datatypes/BreachedStatusEnum.h>
#include <DevStudio/datatypes/ChemicalContentEnum.h>
#include <DevStudio/datatypes/ClockTimeStruct.h>
#include <DevStudio/datatypes/ClockTimeStructLengthlessArray.h>
#include <DevStudio/datatypes/CollisionTypeEnum.h>
#include <DevStudio/datatypes/DamageStatusEnum.h>
#include <DevStudio/datatypes/DatumIdentifierEnum.h>
#include <DevStudio/datatypes/DatumIdentifierLengthlessArray.h>
#include <DevStudio/datatypes/DepthMeterFloat32LengthlessArray.h>
#include <DevStudio/datatypes/DetonationResultCodeEnum.h>
#include <DevStudio/datatypes/EntityCoordinateVectorStruct.h>
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EntityTypeStruct.h>
#include <DevStudio/datatypes/EnvironmentObjectTypeStruct.h>
#include <DevStudio/datatypes/EventIdentifierStruct.h>
#include <DevStudio/datatypes/EventTypeEnum.h>
#include <DevStudio/datatypes/ExhaustSmokeStruct.h>
#include <DevStudio/datatypes/ExhaustSmokeStructLengthlessArray.h>
#include <DevStudio/datatypes/FederateIdentifierStruct.h>
#include <DevStudio/datatypes/FixedDatumStruct.h>
#include <DevStudio/datatypes/FixedDatumStructLengthlessArray.h>
#include <DevStudio/datatypes/ForceIdentifierEnum.h>
#include <DevStudio/datatypes/FortificationsEnum.h>
#include <DevStudio/datatypes/FuseTypeEnum.h>
#include <DevStudio/datatypes/GeographicShapeStruct.h>
#include <DevStudio/datatypes/MineDielectricDifferenceLengthlessArray.h>
#include <DevStudio/datatypes/MineFusingStruct.h>
#include <DevStudio/datatypes/MineFusingStructLengthlessArray.h>
#include <DevStudio/datatypes/MineIdentifierLengthlessArray.h>
#include <DevStudio/datatypes/MinefieldLaneMarkerStruct.h>
#include <DevStudio/datatypes/MinefieldLaneMarkerStructLengthlessArray.h>
#include <DevStudio/datatypes/MinefieldPaintSchemeEnum.h>
#include <DevStudio/datatypes/MinefieldPaintSchemeLengthlessArray.h>
#include <DevStudio/datatypes/MinefieldSensorTypeEnum.h>
#include <DevStudio/datatypes/MinefieldSensorTypeLengthlessArray.h>
#include <DevStudio/datatypes/MissingRecordNumbersLengthlessArray1Plus.h>
#include <DevStudio/datatypes/OrientationStruct.h>
#include <DevStudio/datatypes/OrientationStructLengthlessArray.h>
#include <DevStudio/datatypes/PerimeterPointStruct.h>
#include <DevStudio/datatypes/PerimeterPointStructLengthlessArray.h>
#include <DevStudio/datatypes/PositionStruct.h>
#include <DevStudio/datatypes/RTIobjectIdArray.h>
#include <DevStudio/datatypes/RecordSetStruct.h>
#include <DevStudio/datatypes/RecordSetStructArray1Plus.h>
#include <DevStudio/datatypes/RelativePositionStruct.h>
#include <DevStudio/datatypes/RepairResultEnum.h>
#include <DevStudio/datatypes/RepairTypeEnum.h>
#include <DevStudio/datatypes/RequestStatusEnum.h>
#include <DevStudio/datatypes/ResponseFlagEnum.h>
#include <DevStudio/datatypes/ServiceTypeEnum.h>
#include <DevStudio/datatypes/ShellTypeEnum.h>
#include <DevStudio/datatypes/SignalDataLengthlessArray1Plus.h>
#include <DevStudio/datatypes/SpotCategoryEnum.h>
#include <DevStudio/datatypes/StopFreezeReasonEnum.h>
#include <DevStudio/datatypes/SupplyStruct.h>
#include <DevStudio/datatypes/SupplyStructLengthlessArray.h>
#include <DevStudio/datatypes/TacticalDataLinkTypeEnum.h>
#include <DevStudio/datatypes/TemperatureDegreeCelsiusFloat32LengthlessArray.h>
#include <DevStudio/datatypes/TransferTypeEnum.h>
#include <DevStudio/datatypes/UnsignedInteger8LengthlessArray.h>
#include <DevStudio/datatypes/UserProtocolEnum.h>
#include <DevStudio/datatypes/VariableDatumStruct.h>
#include <DevStudio/datatypes/VariableDatumStructArray.h>
#include <DevStudio/datatypes/VelocityVectorStruct.h>
#include <DevStudio/datatypes/WarheadTypeEnum.h>
#include <DevStudio/datatypes/WorldLocationStruct.h>
#include <DevStudio/datatypes/WorldLocationStructLengthlessArray.h>
#include <string>
#include <vector>

#include <DevStudio/HlaTimeStamp.h>
#include <DevStudio/HlaLogicalTime.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

   /**
   * Listener for all global interactions.
   */

   class HlaInteractionListener {
   public:

      LIBAPI virtual ~HlaInteractionListener() {}

      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void hLAinteractionRoot(
          bool local,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.Collision</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void collision(
          bool local,
          HlaCollisionParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.Collision.CollisionElastic</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void collisionElastic(
          bool local,
          HlaCollisionElasticParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.EnvironmentObjectTransaction</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void environmentObjectTransaction(
          bool local,
          HlaEnvironmentObjectTransactionParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.EnvironmentObjectTransaction.ArealObjectTransaction</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void arealObjectTransaction(
          bool local,
          HlaArealObjectTransactionParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.EnvironmentObjectTransaction.ArealObjectTransaction.OtherArealObjectTransaction</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void otherArealObjectTransaction(
          bool local,
          HlaOtherArealObjectTransactionParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.EnvironmentObjectTransaction.ArealObjectTransaction.MinefieldObjectTransaction</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void minefieldObjectTransaction(
          bool local,
          HlaMinefieldObjectTransactionParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.EnvironmentObjectTransaction.LinearObjectTransaction</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void linearObjectTransaction(
          bool local,
          HlaLinearObjectTransactionParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.EnvironmentObjectTransaction.LinearObjectTransaction.BreachableLinearObjectTransaction</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void breachableLinearObjectTransaction(
          bool local,
          HlaBreachableLinearObjectTransactionParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.EnvironmentObjectTransaction.LinearObjectTransaction.BreachObjectTransaction</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void breachObjectTransaction(
          bool local,
          HlaBreachObjectTransactionParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.EnvironmentObjectTransaction.LinearObjectTransaction.ExhaustSmokeObjectTransaction</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void exhaustSmokeObjectTransaction(
          bool local,
          HlaExhaustSmokeObjectTransactionParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.EnvironmentObjectTransaction.LinearObjectTransaction.OtherLinearObjectTransaction</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void otherLinearObjectTransaction(
          bool local,
          HlaOtherLinearObjectTransactionParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.EnvironmentObjectTransaction.LinearObjectTransaction.MinefieldLaneMarkerObjectTransaction</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void minefieldLaneMarkerObjectTransaction(
          bool local,
          HlaMinefieldLaneMarkerObjectTransactionParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.EnvironmentObjectTransaction.PointObjectTransaction</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void pointObjectTransaction(
          bool local,
          HlaPointObjectTransactionParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.EnvironmentObjectTransaction.PointObjectTransaction.BreachablePointObjectTransaction</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void breachablePointObjectTransaction(
          bool local,
          HlaBreachablePointObjectTransactionParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.EnvironmentObjectTransaction.PointObjectTransaction.BurstPointObjectTransaction</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void burstPointObjectTransaction(
          bool local,
          HlaBurstPointObjectTransactionParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.EnvironmentObjectTransaction.PointObjectTransaction.CraterObjectTransaction</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void craterObjectTransaction(
          bool local,
          HlaCraterObjectTransactionParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.EnvironmentObjectTransaction.PointObjectTransaction.OtherPointObjectTransaction</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void otherPointObjectTransaction(
          bool local,
          HlaOtherPointObjectTransactionParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.EnvironmentObjectTransaction.PointObjectTransaction.RibbonBridgeObjectTransaction</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void ribbonBridgeObjectTransaction(
          bool local,
          HlaRibbonBridgeObjectTransactionParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.EnvironmentObjectTransaction.PointObjectTransaction.StructureObjectTransaction</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void structureObjectTransaction(
          bool local,
          HlaStructureObjectTransactionParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.MinefieldQuery</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void minefieldQuery(
          bool local,
          HlaMinefieldQueryParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.MinefieldData</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void minefieldData2(
          bool local,
          HlaMinefieldData2ParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.MinefieldResponseNACK</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void minefieldResponseNACK(
          bool local,
          HlaMinefieldResponseNACKParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.RadioSignal</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void radioSignal(
          bool local,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.RadioSignal.EncodedAudioRadioSignal</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void encodedAudioRadioSignal(
          bool local,
          HlaEncodedAudioRadioSignalParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.RadioSignal.RawBinaryRadioSignal</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void rawBinaryRadioSignal(
          bool local,
          HlaRawBinaryRadioSignalParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.RadioSignal.DatabaseIndexRadioSignal</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void databaseIndexRadioSignal(
          bool local,
          HlaDatabaseIndexRadioSignalParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.RadioSignal.ApplicationSpecificRadioSignal</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void applicationSpecificRadioSignal(
          bool local,
          HlaApplicationSpecificRadioSignalParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.AcousticTransient</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void acousticTransient(
          bool local,
          HlaAcousticTransientParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.WeaponFire</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void weaponFire(
          bool local,
          HlaWeaponFireParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.MunitionDetonation</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void munitionDetonation(
          bool local,
          HlaMunitionDetonationParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.RepairComplete</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void repairComplete(
          bool local,
          HlaRepairCompleteParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.RepairResponse</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void repairResponse(
          bool local,
          HlaRepairResponseParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.ResupplyCancel</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void resupplyCancel(
          bool local,
          HlaResupplyCancelParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.ResupplyOffer</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void resupplyOffer(
          bool local,
          HlaResupplyOfferParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.ResupplyReceived</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void resupplyReceived(
          bool local,
          HlaResupplyReceivedParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.ServiceRequest</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void serviceRequest(
          bool local,
          HlaServiceRequestParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.Acknowledge</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void acknowledge(
          bool local,
          HlaAcknowledgeParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.Acknowledge.AcknowledgeR</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void acknowledgeR(
          bool local,
          HlaAcknowledgeRParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.ActionRequest</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void actionRequest(
          bool local,
          HlaActionRequestParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.ActionRequest.ActionRequestR</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void actionRequestR(
          bool local,
          HlaActionRequestRParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.ActionResponse</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void actionResponse(
          bool local,
          HlaActionResponseParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.ActionResponse.ActionResponseR</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void actionResponseR(
          bool local,
          HlaActionResponseRParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.Comment</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void comment(
          bool local,
          HlaCommentParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.CreateEntity</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void createEntity(
          bool local,
          HlaCreateEntityParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.CreateEntity.CreateEntityR</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void createEntityR(
          bool local,
          HlaCreateEntityRParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.Data</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void data(
          bool local,
          HlaDataParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.Data.DataR</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void dataR(
          bool local,
          HlaDataRParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.DataQuery</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void dataQuery(
          bool local,
          HlaDataQueryParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.DataQuery.DataQueryR</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void dataQueryR(
          bool local,
          HlaDataQueryRParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.EventReport</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void eventReport(
          bool local,
          HlaEventReportParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.RemoveEntity</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void removeEntity(
          bool local,
          HlaRemoveEntityParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.RemoveEntity.RemoveEntityR</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void removeEntityR(
          bool local,
          HlaRemoveEntityRParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.SetData</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void setData(
          bool local,
          HlaSetDataParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.SetData.SetDataR</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void setDataR(
          bool local,
          HlaSetDataRParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.StartResume</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void startResume(
          bool local,
          HlaStartResumeParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.StartResume.StartResumeR</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void startResumeR(
          bool local,
          HlaStartResumeRParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.StopFreeze</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void stopFreeze(
          bool local,
          HlaStopFreezeParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.StopFreeze.StopFreezeR</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void stopFreezeR(
          bool local,
          HlaStopFreezeRParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.RecordR</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void recordR(
          bool local,
          HlaRecordRParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.RecordQueryR</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void recordQueryR(
          bool local,
          HlaRecordQueryRParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.SetRecordR</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void setRecordR(
          bool local,
          HlaSetRecordRParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.TransferControl</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void transferControl(
          bool local,
          HlaTransferControlParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.AttributeChangeRequest</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void attributeChangeRequest(
          bool local,
          HlaAttributeChangeRequestParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.AttributeChangeRequest.AttributeChangeRequestR</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void attributeChangeRequestR(
          bool local,
          HlaAttributeChangeRequestRParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.AttributeChangeResult</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void attributeChangeResult(
          bool local,
          HlaAttributeChangeResultParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.AttributeChangeResult.AttributeChangeResultR</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void attributeChangeResultR(
          bool local,
          HlaAttributeChangeResultRParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.CreateObjectRequest</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void createObjectRequest(
          bool local,
          HlaCreateObjectRequestParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.CreateObjectRequest.CreateObjectRequestR</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void createObjectRequestR(
          bool local,
          HlaCreateObjectRequestRParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.CreateObjectResult</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void createObjectResult(
          bool local,
          HlaCreateObjectResultParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.CreateObjectResult.CreateObjectResultR</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void createObjectResultR(
          bool local,
          HlaCreateObjectResultRParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.RemoveObjectRequest</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void removeObjectRequest(
          bool local,
          HlaRemoveObjectRequestParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.RemoveObjectRequest.RemoveObjectRequestR</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void removeObjectRequestR(
          bool local,
          HlaRemoveObjectRequestRParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.RemoveObjectResult</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void removeObjectResult(
          bool local,
          HlaRemoveObjectResultParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.RemoveObjectResult.RemoveObjectResultR</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void removeObjectResultR(
          bool local,
          HlaRemoveObjectResultRParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.ActionRequestToObject</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void actionRequestToObject(
          bool local,
          HlaActionRequestToObjectParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.ActionRequestToObject.ActionRequestToObjectR</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void actionRequestToObjectR(
          bool local,
          HlaActionRequestToObjectRParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.ActionResponseFromObject</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void actionResponseFromObject(
          bool local,
          HlaActionResponseFromObjectParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.ActionResponseFromObject.ActionResponseFromObjectR</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void actionResponseFromObjectR(
          bool local,
          HlaActionResponseFromObjectRParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.interactionRoot</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void interactionRoot(
          bool local,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.SpotReport</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void spotReport(
          bool local,
          HlaSpotReportParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;
      /**
      * This method is invoked when an HLA <code>HLAinteractionRoot.SpotReport.FireSpotReport</code> interaction is received,
      * (or is sent locally).
      * @param local <code>true</code> if the interaction was sent locally <code>false</code> otherwise
      * @param parameters contains value for the parameters
      * @param timeStamp the time when the interaction was sent
      * @param logicalTime the logical time when the interaction was sent
      */
      LIBAPI virtual void fireSpotReport(
          bool local,
          HlaFireSpotReportParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) = 0;

      class Adapter;
   };

   /**
   * An adapter class that implements the HlaInteractionListener interface with empty methods.
   * It might be used as a base class for a listener.
   */
   class HlaInteractionListener::Adapter : public HlaInteractionListener {
   public:

      LIBAPI virtual void hLAinteractionRoot(
          bool local,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void collision(
          bool local,
          HlaCollisionParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void collisionElastic(
          bool local,
          HlaCollisionElasticParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void environmentObjectTransaction(
          bool local,
          HlaEnvironmentObjectTransactionParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void arealObjectTransaction(
          bool local,
          HlaArealObjectTransactionParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void otherArealObjectTransaction(
          bool local,
          HlaOtherArealObjectTransactionParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void minefieldObjectTransaction(
          bool local,
          HlaMinefieldObjectTransactionParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void linearObjectTransaction(
          bool local,
          HlaLinearObjectTransactionParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void breachableLinearObjectTransaction(
          bool local,
          HlaBreachableLinearObjectTransactionParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void breachObjectTransaction(
          bool local,
          HlaBreachObjectTransactionParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void exhaustSmokeObjectTransaction(
          bool local,
          HlaExhaustSmokeObjectTransactionParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void otherLinearObjectTransaction(
          bool local,
          HlaOtherLinearObjectTransactionParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void minefieldLaneMarkerObjectTransaction(
          bool local,
          HlaMinefieldLaneMarkerObjectTransactionParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void pointObjectTransaction(
          bool local,
          HlaPointObjectTransactionParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void breachablePointObjectTransaction(
          bool local,
          HlaBreachablePointObjectTransactionParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void burstPointObjectTransaction(
          bool local,
          HlaBurstPointObjectTransactionParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void craterObjectTransaction(
          bool local,
          HlaCraterObjectTransactionParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void otherPointObjectTransaction(
          bool local,
          HlaOtherPointObjectTransactionParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void ribbonBridgeObjectTransaction(
          bool local,
          HlaRibbonBridgeObjectTransactionParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void structureObjectTransaction(
          bool local,
          HlaStructureObjectTransactionParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void minefieldQuery(
          bool local,
          HlaMinefieldQueryParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void minefieldData2(
          bool local,
          HlaMinefieldData2ParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void minefieldResponseNACK(
          bool local,
          HlaMinefieldResponseNACKParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void radioSignal(
          bool local,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void encodedAudioRadioSignal(
          bool local,
          HlaEncodedAudioRadioSignalParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void rawBinaryRadioSignal(
          bool local,
          HlaRawBinaryRadioSignalParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void databaseIndexRadioSignal(
          bool local,
          HlaDatabaseIndexRadioSignalParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void applicationSpecificRadioSignal(
          bool local,
          HlaApplicationSpecificRadioSignalParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void acousticTransient(
          bool local,
          HlaAcousticTransientParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void weaponFire(
          bool local,
          HlaWeaponFireParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void munitionDetonation(
          bool local,
          HlaMunitionDetonationParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void repairComplete(
          bool local,
          HlaRepairCompleteParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void repairResponse(
          bool local,
          HlaRepairResponseParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void resupplyCancel(
          bool local,
          HlaResupplyCancelParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void resupplyOffer(
          bool local,
          HlaResupplyOfferParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void resupplyReceived(
          bool local,
          HlaResupplyReceivedParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void serviceRequest(
          bool local,
          HlaServiceRequestParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void acknowledge(
          bool local,
          HlaAcknowledgeParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void acknowledgeR(
          bool local,
          HlaAcknowledgeRParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void actionRequest(
          bool local,
          HlaActionRequestParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void actionRequestR(
          bool local,
          HlaActionRequestRParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void actionResponse(
          bool local,
          HlaActionResponseParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void actionResponseR(
          bool local,
          HlaActionResponseRParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void comment(
          bool local,
          HlaCommentParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void createEntity(
          bool local,
          HlaCreateEntityParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void createEntityR(
          bool local,
          HlaCreateEntityRParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void data(
          bool local,
          HlaDataParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void dataR(
          bool local,
          HlaDataRParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void dataQuery(
          bool local,
          HlaDataQueryParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void dataQueryR(
          bool local,
          HlaDataQueryRParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void eventReport(
          bool local,
          HlaEventReportParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void removeEntity(
          bool local,
          HlaRemoveEntityParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void removeEntityR(
          bool local,
          HlaRemoveEntityRParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void setData(
          bool local,
          HlaSetDataParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void setDataR(
          bool local,
          HlaSetDataRParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void startResume(
          bool local,
          HlaStartResumeParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void startResumeR(
          bool local,
          HlaStartResumeRParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void stopFreeze(
          bool local,
          HlaStopFreezeParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void stopFreezeR(
          bool local,
          HlaStopFreezeRParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void recordR(
          bool local,
          HlaRecordRParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void recordQueryR(
          bool local,
          HlaRecordQueryRParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void setRecordR(
          bool local,
          HlaSetRecordRParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void transferControl(
          bool local,
          HlaTransferControlParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void attributeChangeRequest(
          bool local,
          HlaAttributeChangeRequestParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void attributeChangeRequestR(
          bool local,
          HlaAttributeChangeRequestRParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void attributeChangeResult(
          bool local,
          HlaAttributeChangeResultParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void attributeChangeResultR(
          bool local,
          HlaAttributeChangeResultRParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void createObjectRequest(
          bool local,
          HlaCreateObjectRequestParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void createObjectRequestR(
          bool local,
          HlaCreateObjectRequestRParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void createObjectResult(
          bool local,
          HlaCreateObjectResultParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void createObjectResultR(
          bool local,
          HlaCreateObjectResultRParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void removeObjectRequest(
          bool local,
          HlaRemoveObjectRequestParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void removeObjectRequestR(
          bool local,
          HlaRemoveObjectRequestRParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void removeObjectResult(
          bool local,
          HlaRemoveObjectResultParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void removeObjectResultR(
          bool local,
          HlaRemoveObjectResultRParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void actionRequestToObject(
          bool local,
          HlaActionRequestToObjectParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void actionRequestToObjectR(
          bool local,
          HlaActionRequestToObjectRParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void actionResponseFromObject(
          bool local,
          HlaActionResponseFromObjectParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void actionResponseFromObjectR(
          bool local,
          HlaActionResponseFromObjectRParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void interactionRoot(
          bool local,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void spotReport(
          bool local,
          HlaSpotReportParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
      LIBAPI virtual void fireSpotReport(
          bool local,
          HlaFireSpotReportParametersPtr parameters,
          HlaTimeStampPtr timeStamp,
          HlaLogicalTimePtr logicalTime
      ) {}
   };

}
#endif
