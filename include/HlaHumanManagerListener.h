/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAHUMANMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLAHUMANMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaHuman.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaHuman instances.
    */
    class HlaHumanManagerListener {

    public:

        LIBAPI virtual ~HlaHumanManagerListener() {}

        /**
        * This method is called when a new HlaHuman instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param human the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaHumanDiscovered(HlaHumanPtr human, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaHuman instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param human the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaHumanInitialized(HlaHumanPtr human, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaHumanManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param human the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaHumanInInterest(HlaHumanPtr human, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaHumanManagerListener instance goes out of interest.
        *
        * @param human the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaHumanOutOfInterest(HlaHumanPtr human, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaHuman instance is deleted.
        *
        * @param human the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaHumanDeleted(HlaHumanPtr human, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaHumanManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaHumanManagerListener::Adapter : public HlaHumanManagerListener {

    public:
        LIBAPI virtual void hlaHumanDiscovered(HlaHumanPtr human, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaHumanInitialized(HlaHumanPtr human, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaHumanInInterest(HlaHumanPtr human, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaHumanOutOfInterest(HlaHumanPtr human, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaHumanDeleted(HlaHumanPtr human, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
