/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLAAGGREGATEENTITYUPDATER_H
#define DEVELOPER_STUDIO_HLAAGGREGATEENTITYUPDATER_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <boost/noncopyable.hpp>

#include <DevStudio/datatypes/AggregateMarkingStruct.h>
#include <DevStudio/datatypes/AggregateStateEnum.h>
#include <DevStudio/datatypes/DimensionStruct.h>
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/EntityTypeStruct.h>
#include <DevStudio/datatypes/ForceIdentifierEnum.h>
#include <DevStudio/datatypes/FormationEnum.h>
#include <DevStudio/datatypes/IsPartOfStruct.h>
#include <DevStudio/datatypes/RTIobjectIdArray.h>
#include <DevStudio/datatypes/SilentAggregateStruct.h>
#include <DevStudio/datatypes/SilentAggregateStructLengthlessArray.h>
#include <DevStudio/datatypes/SilentEntityStruct.h>
#include <DevStudio/datatypes/SilentEntityStructLengthlessArray.h>
#include <DevStudio/datatypes/SpatialVariantStruct.h>
#include <DevStudio/datatypes/VariableDatumStruct.h>
#include <DevStudio/datatypes/VariableDatumStructLengthlessArray.h>
#include <string>
#include <vector>

#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaException.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaBaseEntityUpdater.h>

namespace DevStudio {

    /**
    * Updater used to update attribute values.
    */
    class HlaAggregateEntityUpdater : public HlaBaseEntityUpdater {

    public:

    LIBAPI virtual ~HlaAggregateEntityUpdater() {}

    /**
    * Set the aggregateMarking for this update.
    * <br>Description from the FOM: <i>A unique marking or combination of characters used to distinguish the aggregate from other aggregates.</i>
    * <br>Description of the data type from the FOM: <i>Unique marking associated with an aggregate.</i>
    *
    * @param aggregateMarking the new aggregateMarking
    */
    LIBAPI virtual void setAggregateMarking(const DevStudio::AggregateMarkingStruct& aggregateMarking) = 0;

    /**
    * Set the aggregateState for this update.
    * <br>Description from the FOM: <i>An indicator of the extent of association of objects form an operating group.</i>
    * <br>Description of the data type from the FOM: <i>Aggregate state</i>
    *
    * @param aggregateState the new aggregateState
    */
    LIBAPI virtual void setAggregateState(const DevStudio::AggregateStateEnum::AggregateStateEnum& aggregateState) = 0;

    /**
    * Set the dimensions for this update.
    * <br>Description from the FOM: <i>The size of the area covered by the units in the aggregate.</i>
    * <br>Description of the data type from the FOM: <i>Bounding box in X,Y,Z axis.</i>
    *
    * @param dimensions the new dimensions
    */
    LIBAPI virtual void setDimensions(const DevStudio::DimensionStruct& dimensions) = 0;

    /**
    * Set the entityIdentifiers for this update.
    * <br>Description from the FOM: <i>The identification of entities that are contained within the aggregate.</i>
    * <br>Description of the data type from the FOM: <i>Set of ID's of registered object instances.</i>
    *
    * @param entityIdentifiers the new entityIdentifiers
    */
    LIBAPI virtual void setEntityIdentifiers(const std::vector<std::string >& entityIdentifiers) = 0;

    /**
    * Set the forceIdentifier for this update.
    * <br>Description from the FOM: <i>The identification of the force that the aggregate belongs to.</i>
    * <br>Description of the data type from the FOM: <i>Force ID</i>
    *
    * @param forceIdentifier the new forceIdentifier
    */
    LIBAPI virtual void setForceIdentifier(const DevStudio::ForceIdentifierEnum::ForceIdentifierEnum& forceIdentifier) = 0;

    /**
    * Set the formation for this update.
    * <br>Description from the FOM: <i>The category of positional arrangement of the entities within the aggregate.</i>
    * <br>Description of the data type from the FOM: <i>Formation</i>
    *
    * @param formation the new formation
    */
    LIBAPI virtual void setFormation(const DevStudio::FormationEnum::FormationEnum& formation) = 0;

    /**
    * Set the numberOfSilentEntities for this update.
    * <br>Description from the FOM: <i>The number of elements in the SilentEntities list.</i>
    * <br>Description of the data type from the FOM: <i>Integer in the range [-2^15, 2^15-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
    *
    * @param numberOfSilentEntities the new numberOfSilentEntities
    */
    LIBAPI virtual void setNumberOfSilentEntities(const short& numberOfSilentEntities) = 0;

    /**
    * Set the numberOfVariableDatums for this update.
    * <br>Description from the FOM: <i>The number of records in the VariableDatums structure.</i>
    * <br>Description of the data type from the FOM: <i>Integer in the range [0, 2^32-1]. [unit: NA, resolution: 1, accuracy: perfect]</i>
    *
    * @param numberOfVariableDatums the new numberOfVariableDatums
    */
    LIBAPI virtual void setNumberOfVariableDatums(const unsigned int& numberOfVariableDatums) = 0;

    /**
    * Set the silentAggregates for this update.
    * <br>Description from the FOM: <i>The numbers and types, of silent aggregates contained in the aggregate. Silent aggregates are sub-aggregates that are in the aggregate, but that are not separately represented in the virtual world.</i>
    * <br>Description of the data type from the FOM: <i>Set of silent aggregates (aggregates not registered in the federation).</i>
    *
    * @param silentAggregates the new silentAggregates
    */
    LIBAPI virtual void setSilentAggregates(const std::vector<DevStudio::SilentAggregateStruct >& silentAggregates) = 0;

    /**
    * Set the silentEntities for this update.
    * <br>Description from the FOM: <i>The numbers and types, of silent entities in the aggregate. Silent entities are entities that are in the aggregate, but that are not separately represented in the virtual world.</i>
    * <br>Description of the data type from the FOM: <i>A set of silent entities (entities not registered in the federation).</i>
    *
    * @param silentEntities the new silentEntities
    */
    LIBAPI virtual void setSilentEntities(const std::vector<DevStudio::SilentEntityStruct >& silentEntities) = 0;

    /**
    * Set the subAggregateIdentifiers for this update.
    * <br>Description from the FOM: <i>The identifications of aggregates represented in the virtual world that are contained in the aggregate.</i>
    * <br>Description of the data type from the FOM: <i>Set of ID's of registered object instances.</i>
    *
    * @param subAggregateIdentifiers the new subAggregateIdentifiers
    */
    LIBAPI virtual void setSubAggregateIdentifiers(const std::vector<std::string >& subAggregateIdentifiers) = 0;

    /**
    * Set the variableDatums for this update.
    * <br>Description from the FOM: <i>Extra data that describes the aggregate.</i>
    * <br>Description of the data type from the FOM: <i>Set of additional data associated with an aggregate.</i>
    *
    * @param variableDatums the new variableDatums
    */
    LIBAPI virtual void setVariableDatums(const std::vector<DevStudio::VariableDatumStruct >& variableDatums) = 0;

    /**
    * Set the entityType for this update.
    * <br>Description from the FOM: <i>The category of the entity.</i>
    * <br>Description of the data type from the FOM: <i>Type of entity. Based on the Entity Type record as specified in IEEE 1278.1-1995 section 5.2.16.</i>
    *
    * @param entityType the new entityType
    */
    LIBAPI virtual void setEntityType(const DevStudio::EntityTypeStruct& entityType) = 0;

    /**
    * Set the entityIdentifier for this update.
    * <br>Description from the FOM: <i>The unique identifier for the entity instance.</i>
    * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
    *
    * @param entityIdentifier the new entityIdentifier
    */
    LIBAPI virtual void setEntityIdentifier(const DevStudio::EntityIdentifierStruct& entityIdentifier) = 0;

    /**
    * Set the isPartOf for this update.
    * <br>Description from the FOM: <i>Defines if the entity if a constituent part of another entity (denoted the host entity). If the entity is a constituent part of another entity then the HostEntityIdentifier shall be set to the EntityIdentifier of the host entity and the HostRTIObjectIdentifier shall be set to the RTI object instance ID of the host entity. If the entity is not a constituent part of another entity then the HostEntityIdentifier shall be set to 0.0.0 and the HostRTIObjectIdentifier shall be set to the empty string.</i>
    * <br>Description of the data type from the FOM: <i>Defines the spatial relationship between two objects.</i>
    *
    * @param isPartOf the new isPartOf
    */
    LIBAPI virtual void setIsPartOf(const DevStudio::IsPartOfStruct& isPartOf) = 0;

    /**
    * Set the spatial for this update.
    * <br>Description from the FOM: <i>Spatial state stored in one variant record attribute.</i>
    * <br>Description of the data type from the FOM: <i>Variant Record for a single spatial attribute.</i>
    *
    * @param spatial the new spatial
    */
    LIBAPI virtual void setSpatial(const DevStudio::SpatialVariantStruct& spatial) = 0;

    /**
    * Set the relativeSpatial for this update.
    * <br>Description from the FOM: <i>Relative spatial state stored in one variant record attribute.</i>
    * <br>Description of the data type from the FOM: <i>Variant Record for a single spatial attribute.</i>
    *
    * @param relativeSpatial the new relativeSpatial
    */
    LIBAPI virtual void setRelativeSpatial(const DevStudio::SpatialVariantStruct& relativeSpatial) = 0;

    /**
    * Send all the attributes.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate()
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

    /**
    * Send all the attributes with a specified timestamp.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @param timestamp timestamp to send for this update
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate(HlaTimeStampPtr timestamp)
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

    /**
    * Send all the attributes with a specified timestamp.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @param logicalTime logical time to send for this update
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaInvalidLogicalTimeException if the logical time is not valid.
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate(HlaLogicalTimePtr logicalTime)
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaInvalidLogicalTimeException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

    /**
    * Send all the attributes with a specified timestamp.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @param timestamp timestamp to send for this update
    * @param logicalTime logical time to send for this update
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaInvalidLogicalTimeException if the logical time is not valid.
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate(HlaTimeStampPtr timestamp, HlaLogicalTimePtr logicalTime)
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaInvalidLogicalTimeException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;
    };
}
#endif
