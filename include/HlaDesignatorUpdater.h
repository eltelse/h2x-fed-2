/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLADESIGNATORUPDATER_H
#define DEVELOPER_STUDIO_HLADESIGNATORUPDATER_H

#ifdef _WIN32
#pragma warning( disable : 4290)
#endif

#include <boost/noncopyable.hpp>

#include <DevStudio/datatypes/AccelerationVectorStruct.h>
#include <DevStudio/datatypes/DeadReckoningAlgorithmEnum.h>
#include <DevStudio/datatypes/DesignatorCodeEnum.h>
#include <DevStudio/datatypes/DesignatorCodeNameEnum.h>
#include <DevStudio/datatypes/EntityIdentifierStruct.h>
#include <DevStudio/datatypes/RelativePositionStruct.h>
#include <DevStudio/datatypes/WorldLocationStruct.h>
#include <string>

#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaException.h>
#include <DevStudio/HlaLibSettings.h>
#include <DevStudio/HlaEmbeddedSystemUpdater.h>

namespace DevStudio {

    /**
    * Updater used to update attribute values.
    */
    class HlaDesignatorUpdater : public HlaEmbeddedSystemUpdater {

    public:

    LIBAPI virtual ~HlaDesignatorUpdater() {}

    /**
    * Set the codeName for this update.
    * <br>Description from the FOM: <i>The code name of the designator system.</i>
    * <br>Description of the data type from the FOM: <i>Designator code name</i>
    *
    * @param codeName the new codeName
    */
    LIBAPI virtual void setCodeName(const DevStudio::DesignatorCodeNameEnum::DesignatorCodeNameEnum& codeName) = 0;

    /**
    * Set the designatedObjectIdentifier for this update.
    * <br>Description from the FOM: <i>The object instance ID of the entity that is currently being designated (if any).</i>
    * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
    *
    * @param designatedObjectIdentifier the new designatedObjectIdentifier
    */
    LIBAPI virtual void setDesignatedObjectIdentifier(const std::string& designatedObjectIdentifier) = 0;

    /**
    * Set the designatorCode for this update.
    * <br>Description from the FOM: <i>The designator code being used by the designating entity.</i>
    * <br>Description of the data type from the FOM: <i>Designator code</i>
    *
    * @param designatorCode the new designatorCode
    */
    LIBAPI virtual void setDesignatorCode(const DevStudio::DesignatorCodeEnum::DesignatorCodeEnum& designatorCode) = 0;

    /**
    * Set the designatorEmissionWavelength for this update.
    * <br>Description from the FOM: <i>The wavelength of the designator system.</i>
    * <br>Description of the data type from the FOM: <i>Wavelength expressed in micrometer. [unit: micron, resolution: NA, accuracy: perfect]</i>
    *
    * @param designatorEmissionWavelength the new designatorEmissionWavelength
    */
    LIBAPI virtual void setDesignatorEmissionWavelength(const float& designatorEmissionWavelength) = 0;

    /**
    * Set the designatorOutputPower for this update.
    * <br>Description from the FOM: <i>The output power of the designator system.</i>
    * <br>Description of the data type from the FOM: <i>The unit of power is the watt (W), which is equal to one joule per second. [unit: watt (W), resolution: NA, accuracy: perfect]</i>
    *
    * @param designatorOutputPower the new designatorOutputPower
    */
    LIBAPI virtual void setDesignatorOutputPower(const float& designatorOutputPower) = 0;

    /**
    * Set the designatorSpotLocation for this update.
    * <br>Description from the FOM: <i>The location, in the world coordinate system, of the designator spot.</i>
    * <br>Description of the data type from the FOM: <i>The location of an object in the world coordinate system, as specified in IEEE Std 1278.1-1995 section 1.3.2.</i>
    *
    * @param designatorSpotLocation the new designatorSpotLocation
    */
    LIBAPI virtual void setDesignatorSpotLocation(const DevStudio::WorldLocationStruct& designatorSpotLocation) = 0;

    /**
    * Set the deadReckoningAlgorithm for this update.
    * <br>Description from the FOM: <i>Dead reckoning algorithm used by the issuing object.</i>
    * <br>Description of the data type from the FOM: <i>Dead-reckoning algorithm</i>
    *
    * @param deadReckoningAlgorithm the new deadReckoningAlgorithm
    */
    LIBAPI virtual void setDeadReckoningAlgorithm(const DevStudio::DeadReckoningAlgorithmEnum::DeadReckoningAlgorithmEnum& deadReckoningAlgorithm) = 0;

    /**
    * Set the relativeSpotLocation for this update.
    * <br>Description from the FOM: <i>The location of the designator spot, relative to the object being designated (if any).</i>
    * <br>Description of the data type from the FOM: <i>Relative position in right-handed Cartesian coordinates.</i>
    *
    * @param relativeSpotLocation the new relativeSpotLocation
    */
    LIBAPI virtual void setRelativeSpotLocation(const DevStudio::RelativePositionStruct& relativeSpotLocation) = 0;

    /**
    * Set the spotLinearAccelerationVector for this update.
    * <br>Description from the FOM: <i>The rate of change in linear velocity of the designator spot over time.</i>
    * <br>Description of the data type from the FOM: <i>The magnitude of the change in linear velocity over time.</i>
    *
    * @param spotLinearAccelerationVector the new spotLinearAccelerationVector
    */
    LIBAPI virtual void setSpotLinearAccelerationVector(const DevStudio::AccelerationVectorStruct& spotLinearAccelerationVector) = 0;

    /**
    * Set the entityIdentifier for this update.
    * <br>Description from the FOM: <i>The EntityIdentifier of the object which this embedded system is a part of.</i>
    * <br>Description of the data type from the FOM: <i>Unique, exercise-wide identification of the entity, or a symbolic group address referencing multiple entities or a simulation application. Based on the Entity Identifier record as specified in IEEE 1278.1-1995 section 5.2.14.</i>
    *
    * @param entityIdentifier the new entityIdentifier
    */
    LIBAPI virtual void setEntityIdentifier(const DevStudio::EntityIdentifierStruct& entityIdentifier) = 0;

    /**
    * Set the hostObjectIdentifier for this update.
    * <br>Description from the FOM: <i>The RTI object instance ID of the object of which this embedded system is part of.</i>
    * <br>Description of the data type from the FOM: <i>An RTI object instance identification string.</i>
    *
    * @param hostObjectIdentifier the new hostObjectIdentifier
    */
    LIBAPI virtual void setHostObjectIdentifier(const std::string& hostObjectIdentifier) = 0;

    /**
    * Set the relativePosition for this update.
    * <br>Description from the FOM: <i>The position of the embedded system, relative to the host object's position.</i>
    * <br>Description of the data type from the FOM: <i>Relative position in right-handed Cartesian coordinates.</i>
    *
    * @param relativePosition the new relativePosition
    */
    LIBAPI virtual void setRelativePosition(const DevStudio::RelativePositionStruct& relativePosition) = 0;

    /**
    * Send all the attributes.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate()
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

    /**
    * Send all the attributes with a specified timestamp.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @param timestamp timestamp to send for this update
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate(HlaTimeStampPtr timestamp)
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

    /**
    * Send all the attributes with a specified timestamp.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @param logicalTime logical time to send for this update
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaInvalidLogicalTimeException if the logical time is not valid.
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate(HlaLogicalTimePtr logicalTime)
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaInvalidLogicalTimeException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;

    /**
    * Send all the attributes with a specified timestamp.
    * Note that the <code>Updater</code> can not be reused after <code>sendUpdate</code> has been called.
    *
    * @param timestamp timestamp to send for this update
    * @param logicalTime logical time to send for this update
    *
    * @throws HlaNotConnectedException if the federate is not connected
    * @throws HlaInternalException on internal errors
    * @throws HlaRtiException on exception from the RTI
    * @throws HlaAttributeNotOwnedException if a changed attribute is not owned
    * @throws HlaUpdaterReusedException if the Updater is reused
    * @throws HlaObjectInstanceIsRemovedException if the object instance is removed
    * @throws HlaInvalidLogicalTimeException if the logical time is not valid.
    * @throws HlaSaveInProgressException if a federation save is in progress
    * @throws HlaRestoreInProgressException if a federation restore is in progress
    */
    LIBAPI virtual void sendUpdate(HlaTimeStampPtr timestamp, HlaLogicalTimePtr logicalTime)
        THROW_SPEC (HlaNotConnectedException, HlaAttributeNotOwnedException, HlaUpdaterReusedException, HlaInternalException, HlaRtiException, HlaObjectInstanceIsRemovedException, HlaInvalidLogicalTimeException, HlaSaveInProgressException, HlaRestoreInProgressException) = 0;
    };
}
#endif
