/*
 * DO NOT EDIT!
 * 
 * Automatically generated source code by Pitch Developer Studio
 * Licensed to Rony Levin, Israel MoD
 *
 * Copyright (C) 2006-2018 Pitch Technologies AB. All rights reserved.
 * Use is subject to license terms.
 */
#ifndef DEVELOPER_STUDIO_HLABASEENTITYMANAGERLISTENER_H
#define DEVELOPER_STUDIO_HLABASEENTITYMANAGERLISTENER_H

#include <boost/noncopyable.hpp>

#include <DevStudio/HlaBaseEntity.h>
#include <DevStudio/HlaPointers.h>
#include <DevStudio/HlaLibSettings.h>

namespace DevStudio {

    /**
    * Listener for changes of HlaBaseEntity instances.
    */
    class HlaBaseEntityManagerListener {

    public:

        LIBAPI virtual ~HlaBaseEntityManagerListener() {}

        /**
        * This method is called when a new HlaBaseEntity instance is discovered.
        * A discovered instance is initially within interest.
        *
        * @param baseEntity the object which is discovered
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaBaseEntityDiscovered(HlaBaseEntityPtr baseEntity, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaBaseEntity instance is initialized.
        * An instance is initialized when it has been discovered and all attribute marked with
        * <i>initialized</i> has a value.
        *
        * @param baseEntity the object which is initialized
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time when the update was initiated
        */
        LIBAPI virtual void hlaBaseEntityInitialized(HlaBaseEntityPtr baseEntity, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        /**
        * This method is called when a HlaBaseEntityManagerListener instance comes within interest.
        * A discovered instance is initially within interest, so this will not
        * be called at the time of discovery.
        *
        * @param baseEntity the object which has come into interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaBaseEntityInInterest(HlaBaseEntityPtr baseEntity, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaBaseEntityManagerListener instance goes out of interest.
        *
        * @param baseEntity the object which has gone out of interest
        * @param timeStamp the time when the update was initiated
        */
        LIBAPI virtual void hlaBaseEntityOutOfInterest(HlaBaseEntityPtr baseEntity, HlaTimeStampPtr timeStamp) = 0;

        /**
        * This method is called when a HlaBaseEntity instance is deleted.
        *
        * @param baseEntity the object to be deleted
        * @param timeStamp the time when the update was initiated
        * @param logicalTime the logical time for the delete
        */
        LIBAPI virtual void hlaBaseEntityDeleted(HlaBaseEntityPtr baseEntity, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) = 0;

        class Adapter;
        class InstanceListener;
        class InstanceValueListener;
        class InstanceOwnershipListener;
    };

    /**
    * An adapter class that implements the HlaBaseEntityManagerListener interface with empty methods.
    * It might be used as a base class for a listener.
    */
    class HlaBaseEntityManagerListener::Adapter : public HlaBaseEntityManagerListener {

    public:
        LIBAPI virtual void hlaBaseEntityDiscovered(HlaBaseEntityPtr baseEntity, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaBaseEntityInitialized(HlaBaseEntityPtr baseEntity, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}
        LIBAPI virtual void hlaBaseEntityInInterest(HlaBaseEntityPtr baseEntity, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaBaseEntityOutOfInterest(HlaBaseEntityPtr baseEntity, HlaTimeStampPtr timeStamp) {}
        LIBAPI virtual void hlaBaseEntityDeleted(HlaBaseEntityPtr baseEntity, HlaTimeStampPtr timeStamp, HlaLogicalTimePtr logicalTime) {}

    };
}
#endif
